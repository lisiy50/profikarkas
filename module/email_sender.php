<?php

if (isset($_POST['g-recaptcha-response'])) {
	$captcha = $_POST['g-recaptcha-response'];
} else {
	$captcha = false;
}

if (!$captcha) {
	//Do something with error
} else {
	$secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
	$response = file_get_contents(
		"https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
	);
	// use json_decode to extract json response
	$response = json_decode($response);

	if ($response->success === false) {
		//Do something with error
	}
}

//... The Captcha is valid you can continue with the rest of your code
//... Add code to filter access using $response . score
if ($response->success==true && $response->score <= 0.5) {
	//Do something to denied access
}

//send to zohocrm
$url = 'https://profikarkas.com.ua/zohocrm/application_module.php';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_GET));
curl_exec($ch);
curl_close($ch);
//end send to zohocrm

$email = "info@profikarkas.com.ua";
//$email = "info@cheport.com.ua";
$subject = $_GET['subject'];
$message = 'Телефон: '.$_GET['phone'].'
'.'Удобное время: c'.$_GET['time-from'] .'до '.$_GET['time-to'];

mail($email,$subject,$message);

echo 'ok';