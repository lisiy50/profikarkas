$(function() {


// replace img by background
function ibg() {
	$.each($('.ibg'), function (index, val) {
		if ($(this).find('img').length > 0) {
			$(this).css('background-image', 'url("' + $(this).find('img').attr('src') + '")');
		}
	});
}

ibg();


// language switcher
var $currLang;
var $currLangLink;

if ($(window).width() > 767) {
	$('.header_lang_btn').on('click', function(e){
		e.preventDefault();
		$currLang = $(this).text();
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.header_lang_inner').slideUp();
		} else {
			$(this).addClass('active');
			$('.header_lang_inner').slideDown();
		}
	});

	$('.header_lang_link').on('click', function(e){
		e.preventDefault();
		var $val = $(this).text();
		$('.header_lang_btn').text($val).removeClass('active');
		$(this).text($currLang);
		$('.header_lang_inner').slideUp();
	});
}


// add shadow to header
$(window).on('scroll', function(){

	if ($(this).scrollTop() > 500 ){
		$('.page-header, .header-bottom').addClass('scroll');
	} else {
		$('.page-header, .header-bottom').removeClass('scroll');
	}

});

if($(window).scrollTop() > 500 ){
	$('.page-header, .header-bottom').addClass('scroll');
}


// Перенос информации из одной части хедера в другую
$(window).resize(function(){
	adaptiveHeader();
});

adaptiveHeader();

function adaptiveHeader() {
	var w = $(window).outerWidth();
	var h = $(window).outerHeight();
	replaceHeader(w, h);
}

function replaceHeader(w, h) {

	var pageHeader = $('.page-header');
	var headerTop = $('.header-top');
	var headerTopInner = $('.header-top-inner');
	var headerBottom = $('.header-bottom-inner');
	var headerLang = $('.header_lang');

	if(w < 768){
		if(!headerTop.hasClass('mobile')) {
			headerTop.addClass('mobile').appendTo(headerBottom);
		}
	} else {
		if(headerTop.hasClass('mobile')) {
			headerTop.removeClass('mobile').prependTo(pageHeader);
		}
	}
	if(w < 768){
		if(!headerLang.hasClass('mobile')) {
			headerLang.addClass('mobile').appendTo(headerBottom);
		}
	} else {
		if(headerLang.hasClass('mobile')) {
			headerLang.removeClass('mobile').prependTo(headerTopInner);
		}
	}
}


// Mobile menu
$('.menu-toggle').on('click', function(e){
	e.preventDefault();
	if ($(this).hasClass('is-active')) {
		$(this).removeClass('is-active');
		$('.header-bottom').removeClass('active');
	} else {
		$(this).addClass('is-active');
		$('.header-bottom').addClass('active');
	}
});


// modal callback
$('.call-back').on('click', function(e){
	e.preventDefault();
	$('.overlay').fadeIn();
	$('#modal-callback').fadeIn();
	$('body').addClass('modal-open');
});


//order callback
$('.js-house-order-btn').on('click', function(e){
	e.preventDefault();
	$('.overlay').fadeIn();
	$('#modal-order').fadeIn();
	$('body').addClass('modal-open');
});


//close modal
$('.popup-close').on('click', function(e){
	e.preventDefault();
	$(this).parents('.popup').fadeOut();
	$('.overlay').fadeOut();
	$('body').removeClass('modal-open');
});

$('.overlay').on('click', function(e){
	e.preventDefault();
	$('.popup').fadeOut();
	$(this).fadeOut();
	$('body').removeClass('modal-open');
});


// video label slider
$('.video-label-wrapper').slick({
	dots: false,
	arrows: false,
	infinite: true,
	speed: 300,
	slidesToShow: 1,
	adaptiveHeight: true,
	autoplay: true,
	autoplaySpeed: 5000,
});


$('.seo-slider').slick({
	dots: true,
	arrows: true,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 4000,
	pauseOnHover: false,
	responsive: [
		{
			breakpoint: 768,
			settings: {
				arrows: false,
				dots: false,
			}
		}
	]
});


$('.ajax-form').submit(function (e) {
	 e.preventDefault();
		var form = $(this);
		$.ajax({
				method: "GET",
				url: 'email_sender.php',
				data: form.serialize(),
				success: function (resp) {
						$('.ajax-form').trigger('reset');
						$('#modal-order').fadeOut();
						$('.overlay').fadeIn();
						$('#modal-success').fadeIn();
						$('body').addClass('modal-open');
						setTimeout(function(){
							$('.overlay').fadeOut();
							$('#modal-success').fadeOut();
							$('body').removeClass('modal-open');
						}, 5000);
				},
				error: function (xhr, str) {
						console.log(xhr);
				}
		});

		return false;
});

});