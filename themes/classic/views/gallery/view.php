<?php
$detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>

<div class="gallery_slider">
    <div class="slider" id="gallery-carousel">
        <?php foreach($model->images as $image):?>
            <a class="slide" href="#" id="i<?php echo $image->id;?>" style="width: 800px;">
                <img src="<?php echo $image->getImageUrl('more');?>">
                <?php if($image->description):?>
                    <div class="slide_text"><?php echo $image->description;?></div>
                <?php endif;?>
            </a>
        <?php endforeach;?>
    </div>

    <a href="#" class="slider_prev"></a>
    <a href="#" class="slider_next"></a>

    <?php if (!$isMobile || $isTablet): ?>

    <div class="project_carusel gallery_carusel clearfix">
        <a href="#" class="back pull-left" id="gallery-thumbs-prev"></a>
        <div style="width: 732px;height: 60px;" class="pull-left">
            <div class="pull-left carusel" style="width: 732px;height: 60px" id="gallery-thumbs">
                <?php foreach($model->images as $i=>$image):?>
                    <a href="#i<?php echo $image->id;?>" class="<?php echo ($i == 0)?'selected':'';?>"><img src="<?php echo $image->getImageUrl('mini');?>"></a>
                <?php endforeach;?>
            </div>
        </div>
        <a href="#" class="next pull-left" id="gallery-thumbs-next"></a>
    </div>

    <?php endif ?>

</div>

<div class="gallery-text">
    <?php if (Yii::app()->language == 'ru'): ?>
        <?php echo $model->description;?>
    <?php elseif (Yii::app()->language == 'uk'): ?>
        <?php echo $model->description_uk;?>
    <?php endif; ?>
</div>

<?php

package('carouFredSel');

$script = <<<JS
$('#carousel span').append('<img src="img/gui/carousel_glare.png" class="glare" />');
$('#thumbs a').append('<img src="img/gui/carousel_glare_small.png" class="glare" />');

$('#gallery-carousel').carouFredSel({
 responsive: true,
    width: '100%',
    height: 'variable',
    items: {
        visible: 1,
        height: 'variable'
    },
    circular: false,
    auto: false,
    scroll: {
        fx: 'directscroll'
    },
    next:'.slider_next',
    prev:'.slider_prev'
});

$('#gallery-thumbs').carouFredSel({
responsive: true,
circular: true,
infinite: false,
auto: false,
prev: '#gallery-thumbs-prev',
next: '#gallery-thumbs-next',
items: {
    visible: {
        min: 3,
        max: 9
    },
    width: 105,
    height: 60
}
});

$('#gallery-thumbs a').click(function() {
$('#gallery-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
$('#gallery-carousel a').removeClass('selected');
$(this).addClass('selected');
return false;
});
JS;
cs()->registerScript('gallery-view', $script);

?>
