<?php
$detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>

<div class="reviews_text">
    <h1 class="name">
        <?php if (Yii::app()->language == 'ru'): ?>
            <?php echo $model->title; ?>
        <?php elseif (Yii::app()->language == 'uk'): ?>
            <?php echo $model->title_uk; ?>
        <?php endif; ?>
    </h1>
    <?php if (Yii::app()->language == 'ru'): ?>
        <?php echo $model->description; ?>
    <?php elseif (Yii::app()->language == 'uk'): ?>
        <?php echo $model->description_uk; ?>
    <?php endif; ?>
</div>

<?php if($model->images):?>
    <div class="reviews_slider">
        <div class="slider" id="review-carousel">
            <?php foreach($model->images as $k=>$image):?>
                <a class="slide" href="#" id="i<?php echo $image->id;?>">
                    <span style="display: inline-block;position: relative;">
                        <?php if($model->project_name):?>
                            <div class="z10img">
                                <?php if (Yii::app()->language == 'ru'): ?>
                                    <?php echo $model->project_name;?>
                                <?php elseif (Yii::app()->language == 'uk'): ?>
                                    <?php echo $model->project_name_uk;?>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <div class="z10img"><?= t('Индивидуальный проект') ?></div>
                        <?php endif ?>
                        <img src="<?php echo $image->getImageUrl('more');?>" alt="Проект дома <?php echo $model->title ?> иллюстрация <?php echo $k+1;?>">
                    </span>
                    <?php if($image->description):?>
                        <div class="slide_text"><?php echo $image->description;?></div>
                    <?php endif;?>
                </a>
            <?php endforeach;?>
        </div>

        <a href="#" class="slider_prev"></a>
        <a href="#" class="slider_next"></a>

        <?php if (!$isMobile || $isTablet): ?>

        <div class="project_carusel clearfix ">
            <?php if(count($model->images) > 1): ?>
            <a href="#" class="back pull-left" id="review-thumbs-prev"></a>
            <?php endif ?>
            <?php if($model->getVideos()):?>
                <div class="video_block pull-left" style="width: <?php $model->getCountVideos() * 105;?>px;height: 60px;">
                    <?php foreach($model->getVideos() as $video):?>
                        <a href="<?php echo $video['videoLink'];?>" class="review-video various fancybox.iframe">
                            <?php echo $video['miniImageHtml'];?>
                            <img src="<?php echo app()->theme->baseUrl;?>/img/carusel_video_img.png" class="video_play_img">
                        </a>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
            <?php if(count($model->images) > 1): ?>
            <div style="width: <?php echo (735 - ($model->getCountVideos() * 105));?>px;height: 60px;" class="pull-left">
                <div class="pull-left carusel" style="width: <?php echo (732 - ($model->getCountVideos() * 105));?>px;height: 60px" id="review-thumbs">
                    <?php foreach($model->images as $i=>$image):?>
                        <a href="#i<?php echo $image->id;?>" class="<?php echo ($i == 0)?'selected':'';?>"><img src="<?php echo $image->getImageUrl('mini');?>" alt="Проект дома <?php echo $model->title ?> иллюстрация <?php echo $i+1;?>"></a>
                    <?php endforeach;?>
                </div>
            </div>
            <a href="#" class="next pull-left" id="review-thumbs-next"></a>
            <?php endif ?>
        </div>

        <?php endif ?>
    </div>
<?php elseif($model->getCountVideos()):?>
    <div class="reviews_video">
        <?php foreach($model->getVideos() as $video):?>
        <a href="<?php echo $video['videoLink'];?>" class="review-video various fancybox.iframe">
            <?php if($model->project_name):?>
                <div class="z10img">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php echo $model->project_name; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php echo $model->project_name_uk; ?>
                    <?php endif; ?>
                </div>
            <?php endif;?>
            <?php echo $video['largeImageHtml'] ?>
            <img class="play" src="<?php echo app()->theme->baseUrl;?>/img/play2.png">
            <img class="play hover" src="<?php echo app()->theme->baseUrl;?>/img/play2_hover.png">
        </a>
        <?php endforeach;?>
    </div>
<?php endif;?>

<div class="clearfix link_wrap"><a href="<?php echo url('review/index');?>" class="link"><?= t('Вернуться') ?></a></div>




<?php
package('fancyboxHelpers');
package('carouFredSel');

$script = <<<JS
$('.review-video').fancybox();

$('#carousel span').append('<img src="img/gui/carousel_glare.png" class="glare" />');
$('#thumbs a').append('<img src="img/gui/carousel_glare_small.png" class="glare" />');
JS;

$script .= <<<JS

function sliderButtonPos(item) {
    var width = item.outerWidth();
    var h = item.outerHeight();
    if (width) {
        var space = (800 - width) / 2;
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    } else {
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    }
}

$('#review-carousel').carouFredSel({
    responsive: true,
    width: '100%',
    height: 'variable',
    items: {
        visible: 1,
        height: 'variable'
    },
    circular: false,
    auto: false,
     scroll: {
        fx: 'directscroll',
        onBefore: function( data ) {
            var item = data.items.visible.find('img,iframe');
            sliderButtonPos(item);
        }
    },
    onCreate: function(data) {
        var item = $(this).find('img,iframe');
        sliderButtonPos(item);
    },
    next:'.slider_next',
    prev:'.slider_prev'
});

$('#review-thumbs').carouFredSel({
    responsive: false,
    circular: true,
    infinite: false,
    auto: false,
    prev: '#review-thumbs-prev',
    next: '#review-thumbs-next',
    items: {
        visible: {
            min: 3,
            max: 9
        },
        width: 105,
        height: 60
    }
});

$('#review-thumbs a').click(function() {
    $('#review-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
    $('#review-carousel a').removeClass('selected');
    $(this).addClass('selected');
    return false;
});
JS;

cs()->registerScript('review-view', $script);
?>

<style type="text/css">
    <?php if($model->h1_size):?>
    .reviews_text h1, .reviews_text h1 span {
        font-size: <?php echo $model->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h2_size):?>
    .reviews_text h2, .reviews_text h2 span {
        font-size: <?php echo $model->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h3_size):?>
    .reviews_text h3, .reviews_text h3 span {
        font-size: <?php echo $model->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h4_size):?>
    .reviews_text h4, .reviews_text h4 span {
        font-size: <?php echo $model->h4_size?>px !important;
    }
    <?php endif?>
</style>
