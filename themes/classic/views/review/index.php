<div class="catalog clearfix" id="portfolio-catalog">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<div class="review_article clearfix" style="margin-top: 60px;">

    <?php if (Yii::app()->language == 'ru'): ?>
        <?php echo TextContent::get('review-index'); ?>
    <?php elseif (Yii::app()->language == 'uk'): ?>
        <?php echo TextContent::getUk('review-index'); ?>
    <?php endif; ?>

</div>

<?php
$script = <<<JS
$('#portfolio-catalog').on('click', '.portfolio-pagination .next a', function(){
    $(this).addClass('disabled');
    $('.items-loader').show();
    $.get($(this).attr('href'), {}, function(html){
        $('#portfolio-pager, .items-loader').remove();
        $('#portfolio-catalog').append(html);
    });
    return false;
});
JS;
cs()->registerScript('review-index', $script);
?>
