<?php
foreach($dataProvider->data as $portfolioItem){
    $this->renderPartial('_view', array('model'=>$portfolioItem, ));
}
?>

<div id="portfolio-pager">
    <?php $this->widget('BootPager', array(
        'pages'=>$dataProvider->pagination,
        'nextPageLabel' => t('Ещё отзывы'),
        'prevPageLabel' => '',
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'htmlOptions' => array(
            'class' => 'portfolio-pagination',
        ),
    )); ?>
</div>

<div class="items-loader" style="display:none;">
    <div  style="width: 100%">
        <div class="items-loader-button" style="width: 127px;margin-bottom: -34px;display: block;float: right;font-weight: bold;line-height: 44px;padding: 0 20px;text-align: center;text-decoration: none;">
            <img src="<?php echo app()->theme->baseUrl?>/img/items-loader.gif">
        </div>
    </div>
</div>
