<?php
/**
 * @var $message Swift_Mime_Message
 */
?>
<?php $this->renderPartial('/layouts/mail_header', array('message'=>$message));?>

<div>
    <?php echo t('Для восстановления пароля перейдите по ссылке') ?>:
    <a href="<?php echo app()->createAbsoluteUrl('user/restorePassword', array('key'=>$key))?>"><?php echo app()->createAbsoluteUrl('user/restorePassword', array('key'=>$key))?></a>
</div>

<?php $this->renderPartial('/layouts/mail_footer', array('message'=>$message));?>
