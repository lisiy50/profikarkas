    <?php if($order->payment_status==Order::PAYMENT_STATUS_SUCCESS): ?>
        <h3>Ваш заказ успешно оплачен</h3>
    <?php else: ?>
        <h3>Ваш заказ еще не оплачен</h3>
    <?php endif; ?>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th></th>
            <th>Товар</th>
            <th>Цена</th>
            <th>Количество</th>
            <th>Итого</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($order->products as $i=>$product): ?>
        <tr>
            <td width="100">
                <a class="thumbnail" href="<?php echo $product->url; ?>"><img style="max-height: 100px; max-width: 150px" src="<?php echo $product->getImageUrl('small'); ?>"></a>
            </td>
            <td  style="vertical-align: middle;">
                <a href="<?php echo $product->url; ?>"><h4 style="text-align: center;"><?php echo $product->name; ?></h4></a>
            </td>
            <td width="50" style="vertical-align: middle; white-space: nowrap;">
                <?php echo Yii::app()->priceFormatter->format($product->price); ?>
            </td>
            <td width="50" style="vertical-align: middle; white-space: nowrap; text-align: center;">
                <?php echo $product->quantity; ?> шт.
            </td>
            <td width="50" style="vertical-align: middle; white-space: nowrap;">
                <?php echo Yii::app()->priceFormatter->format($product->sumPrice); ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php if($order->delivery): ?>
        <tr>
            <td></td>
            <td colspan="3" style="vertical-align: middle;">
                <?php echo $order->delivery->name; ?>
            </td>
            <td style="vertical-align: middle;">
                <?php echo Yii::app()->priceFormatter->format($order->delivery->priceTo($order->cost)); ?>
            </td>
        </tr>
        <?php endif; ?>
        </tbody>
    </table>

    <h3>Итого: <span id="subtotal_price"><?php echo Yii::app()->priceFormatter->format($order->cost); ?></span></h3>

<?php echo CHtml::beginForm('', 'post', array('id'=>'payment-form')); ?>

<?php foreach($order->payments as $payment): ?>

    <label class="radio payment-label">
        <?php echo CHtml::activeRadioButton($order, 'payment_id', array('value'=>$payment->id, 'uncheckValue'=>null)); ?>
        <strong><?php echo $payment->name; ?></strong>
        <p class="help-block">
            <?php echo $payment->description; ?>
        </p>
    </label>

<?php endforeach; ?>

<?php echo CHtml::endForm(); ?>

<?php if($order->getPayHandler()): ?>
    <?php $order->renderPayForm(); ?>
<?php else: ?>
    <h4>Для уточнения процедуры оплаты свяжитесь с нами</h4>
<?php endif; ?>

<?php
$script = <<<JS
$('.payment-label').change(function(){
    $('#payment-form').submit();
})
JS;
cs()->registerScript('order-pay', $script);
?>
