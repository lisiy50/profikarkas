<h3>Оформление заказа</h3>

<?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => 'products-form',
    'action' => $this->createUrl('order/update')
)); ?>
<table class="table">
    <thead>
        <tr>
            <th></th>
            <th>Товар</th>
            <th>Цена</th>
            <th>Количество</th>
            <th>Итого</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (Yii::app()->cart->products as $i => $product): ?>
        <tr>
            <td>
                <a style="width: 150px;" class="thumbnail" href="<?php echo $product->url; ?>">
                    <img style="max-height: 100px; max-width: 150px" src="<?php echo $product->getImageUrl('small'); ?>">
                </a>
            </td>
            <td style="vertical-align: middle;">
                <a href="<?php echo $product->url; ?>"><h4 style="text-align: center;"><?php echo $product->name; ?></h4>
                </a>
                <?php echo $product->summary; ?>
            </td>
            <td style="vertical-align: middle;">
                <?php echo Yii::app()->priceFormatter->format($product->price); ?>
            </td>
            <td style="vertical-align: middle;" nowrap="nowrap">
                <?php echo $form->textField($product, "[$i]quantity", array('onchange' => "$('#products-form').submit();", 'style' => 'margin-top:8px;width:20px;')); ?>
                <span class="help-inline">шт.</span>
            </td>
            <td style="vertical-align: middle;" nowrap="nowrap">
                <?php echo Yii::app()->priceFormatter->format($product->sumPrice); ?>
            </td>
            <td style="vertical-align: middle;">
                <a title="убрать из корзины" href="<?php echo $this->createUrl('remove', array('id' => $product->id)); ?>">
                    <i class="icon-remove"></i>
                </a>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4">Доставка: <span id="delivery-name"><?php echo $order->delivery?$order->delivery->name:''; ?></span></td>
            <td id="delivery-price"><?php echo Mz::price($order->delivery?$order->delivery->priceTo($cart->cost):0); ?></td>
            <td></td>
        </tr>
    </tbody>
    <tfoot>
    <tr>
        <td><h3>Итого:</h3></td>
        <td colspan="5"><h3 id="total-price" style="text-align: right"><?php echo Mz::price( $order->delivery?$cart->cost+$order->delivery->priceTo($cart->cost):$cart->cost ); ?></h3></td>
    </tr>
    </tfoot>
</table>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('BootActiveForm', array(
    'id'=>'order-form',
    'action'=>$this->createUrl('index'),
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
)); ?>

<?php echo $form->errorSummary($order); ?>

<h3>Доставка</h3>
<?php foreach(Delivery::model()->findAll() as $delivery): ?>
<label class="radio delivery-label" data-price="<?php echo $delivery->priceInfo($cart->cost); ?>" data-total-price="<?php echo Mz::price($cart->cost+$delivery->priceTo($cart->cost)); ?>" data-payments="<?php echo implode(',',$delivery->paymentIds); ?>" data-name="<?php echo $delivery->name; ?>">
    <?php echo $form->radioButton($order, 'delivery_id', array(
        'value'=>$delivery->id,
        'uncheckValue'=>null,
    )); ?>
    <strong><?php echo $delivery->name; ?> (<?php echo $delivery->priceInfo($cart->cost); ?>)</strong>
    <p class="help-block">
        <?php echo $delivery->description; ?>
    </p>
</label>
<?php endforeach; ?>

<?php
echo $form->dropDownListRow($order, 'payment_id', $order->getPaymentList(), array('prompt'=>'', 'class'=>'input-xlarge'))."\n";

echo $form->textFieldRow($order, 'name', array('class'=>'input-xlarge'))."\n";

echo $form->textFieldRow($order, 'email', array('class'=>'input-xlarge'))."\n";

echo $form->textFieldRow($order, 'phone', array('class'=>'input-xlarge'))."\n";

echo $form->textAreaRow($order, 'address', array('rows'=>2, 'class'=>'input-xxlarge'))."\n";
?>

<?php echo CHtml::submitButton('Заказать', array('class'=>"btn btn-large btn-success")); ?>

<?php $this->endWidget(); ?>

<?php
$script = <<<JS
$('#order-form .delivery-label').click(function(){
    var delivery=$(this);

    var payments=delivery.data('payments').split(',');
    $('#Order_payment_id option').each(function() {
        if(this.value!='' && $.inArray(this.value, payments)==-1) {
            $(this).hide();
            if(this.selected)
                $('#Order_payment_id').val('').change();
        } else {
            $(this).show();
        }
    });
    $('#delivery-name').closest('tr').show();
    $('#delivery-name').html(delivery.data('name'));
    $('#total-price').html(delivery.data('total-price'));
    $('#delivery-price').html(delivery.data('price'));
});
$('.delivery-label:has(:radio:checked)').click();
if($('.delivery-label:has(:radio:checked)').length==0) {
    $('#delivery-name').closest('tr').hide();
}
JS;
cs()->registerScript('order-index', $script);
?>
