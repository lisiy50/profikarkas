<style type="text/css">
    span.required {
        color: #ff0000;
    }
</style>
<?php
Yii::app()->getClientScript()->registerScript('delivery-payment', "
    $('.delivery-radio').click(function(){
        var el=$(this);
        el.find(':radio').attr('checked', true);
        $('#total_price').html(el.attr('price'));
        var payment_ids=el.attr('payment_ids').split(',');
        $('#Order_payment_id option').each(function() {
            if(this.value!='' && $.inArray(this.value, payment_ids)==-1) {
                $(this).hide();
                if(this.selected) {
                    $('#Order_payment_id').val('').change();
                }
            } else {
                $(this).show();
            }
        });
    });
    $('.delivery-radio:has(:radio:checked)').click();
");
?>
<div class="row">
    <div class="span">
        <h3>Доставка</h3>
    </div>
</div>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'order-form',
    'action'=>$this->createUrl('index'),
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
)); ?>

<?php if($order->hasErrors()):?>
<div class="row margin20">
    <div class="span alert alert-error nomargin_bottom">
        <?php echo $form->errorSummary($order); ?>
    </div>
</div>
<?php endif;?>

<?php foreach(Delivery::model()->findAll() as $delivery): ?>
<div class="row margin20">
    <div class="span">
        <label class="btn radio inline" price="<?php echo Yii::app()->priceFormatter->format(Yii::app()->cart->cost+$delivery->priceTo(Yii::app()->cart->cost)); ?>" payment_ids="<?php echo implode(',',$delivery->paymentIds); ?>">
            <?php echo $form->radioButton($order, 'delivery_id', array('value'=>$delivery->id, 'uncheckValue'=>null, 'class'=>"nomargin_left",)); ?>
            <span class="help-inline" style="margin-top: -4px;">
                <strong style="white-space: nowrap;" class="popover_element" rel="popover" data-content="<?php echo $delivery->description; ?>" data-original-title="<?php echo $delivery->name; ?> (<?php echo Yii::app()->priceFormatter->format($delivery->price); ?><?php if($delivery->free_from):?> бесплатно от <?php echo Yii::app()->priceFormatter->format($delivery->free_from); ?><?php endif; ?>)">
                    <?php echo $delivery->name; ?> (<?php echo Yii::app()->priceFormatter->format($delivery->price); ?><?php if($delivery->free_from):?> бесплатно от <?php echo Yii::app()->priceFormatter->format($delivery->free_from); ?><?php endif; ?>)
                </strong>
            </span>
        </label>
    </div>
</div>
<?php endforeach; ?>

<?php
$script = <<<JS
 $('.popover_element').popover();
JS;
cs()->registerScript('order-_form', $script);
?>

<div class="row margin20">
    <div class="span">
        <h3 class="alert alert-info nomargin_bottom">Итого с доставкой: <?php echo Yii::app()->priceFormatter->format($order->delivery?Yii::app()->cart->cost+$order->delivery->priceTo(Yii::app()->cart->cost):Yii::app()->cart->cost ); ?></h3>
    </div>
</div>
<div class="row margin20">
    <div class="span">
        <h3>Адрес получателя</h3>
    </div>
</div>

<div class="row">
    <div class="span">
        <?php echo $form->labelEx($order, 'payment_id'); ?>
        <?php echo $form->dropDownList($order, 'payment_id', CHtml::listData(Payment::model()->findAll(), 'id', 'name'), array('empty'=>'')); ?>
        <?php echo $form->error($order, 'payment_id', array('class'=>"label label-important")); ?><p></p>
    </div>
</div>

<div class="row">
    <div class="span">
        <?php echo $form->labelEx($order, 'name'); ?>
        <?php echo $form->textField($order, 'name'); ?>
        <?php echo $form->error($order, 'name', array('class'=>"label label-important")); ?><p></p>
    </div>
</div>

<div class="row">
    <div class="span">
        <?php echo $form->labelEx($order, 'email'); ?>
        <?php echo $form->textField($order, 'email'); ?>
        <?php echo $form->error($order, 'email', array('class'=>"label label-important")); ?><p></p>
    </div>
</div>

<div class="row">
    <div class="span">
        <?php echo $form->labelEx($order, 'phone'); ?>
        <?php echo $form->textField($order, 'phone'); ?>
        <?php echo $form->error($order, 'phone', array('class'=>"label label-important")); ?><p></p>
    </div>
</div>

<div class="row">
    <div class="span">
        <?php echo $form->labelEx($order, 'address'); ?>
        <?php echo $form->textArea($order, 'address', array('cols'=>40, 'rows'=>4)); ?>
        <?php echo $form->error($order, 'address', array('class'=>"label label-important")); ?><p></p>
    </div>
</div>

<div class="row">
    <div class="span3">

        <?php echo CHtml::submitButton('Заказать', array('class'=>"btn btn-large fill")); ?>
    </div>
</div>

<?php $this->endWidget(); ?>