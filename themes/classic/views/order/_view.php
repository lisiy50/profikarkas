<?php if(Yii::app()->cart->isEmpty): ?>

<a href="#"><div class="cart">
    <div>КОРЗИНА:</div>
    <div>нет товаров</div>
</div></a>

<?php else: ?>

<a href="<?php echo Yii::app()->createUrl('order/index') ?>"><div class="cart">
    <div>КОРЗИНА:</div>
    <div><?php echo Yii::t('app', '{n} товар|{n} товара|{n} товаров|{n} товар', Yii::app()->cart->itemsCount); ?>  <?php echo Mz::price(Yii::app()->cart->cost); ?></div>
</div></a>

<?php endif; ?>