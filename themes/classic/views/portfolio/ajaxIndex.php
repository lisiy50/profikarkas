<?php
/**
 * @var $this PortfolioController
 * @var $dataProvider CActiveDataProvider
 */
$lastCrossLink = 0;
if(isset($_GET['lastCrossLink'])){
    $lastCrossLink = $_GET['lastCrossLink'];
}
$crossLinks = CrossLink::getNextItems($lastCrossLink);
foreach($dataProvider->data as $k=>$portfolioItem){
    $this->renderPartial('_view', array('model'=>$portfolioItem, ));
    if($k == 2 && isset($crossLinks[0])){
        $this->renderPartial('//crossLink/_portfolioCrossLink', array('crossLink'=>$crossLinks[0]));
    }
    if($k == 4 && isset($crossLinks[0])){
        $this->renderPartial('//crossLink/_portfolioCrossLink', array('crossLink'=>$crossLinks[1]));
    }
}
?>

<div id="portfolio-pager">
    <?php $this->widget('BootPager', array(
        'pages'=>$dataProvider->pagination,
        'nextPageLabel' => t('Ещё работы'),
        'prevPageLabel' => '',
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'htmlOptions' => array(
            'class' => 'portfolio-pagination',
        ),
    )); ?>
</div>

<div class="items-loader" style="display:none;">
    <div  style="width: 100%">
        <div class="items-loader-button" style="width: 127px;margin-bottom: -34px;display: block;float: right;font-weight: bold;line-height: 44px;padding: 0 20px;text-align: center;text-decoration: none;">
            <img src="<?php echo app()->theme->baseUrl?>/img/items-loader.gif">
        </div>
    </div>
</div>
