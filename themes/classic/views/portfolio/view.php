<?php
/* @var Portfolio $model */
?>
<?php $detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>
<div class="structured_top clearfix">
    <a href="<?php echo url('portfolio/index');?>" class="pull-left"><?= t('Вернуться к списку проектов') ?></a>
    <div class="pull-right">
        <?php if($model->getPrevProject()):?>
        <a href="<?php echo $model->getPrevProject()->getUrl();?>" class="back"><?= t('Предыдущий') ?></a>
        <?php endif;?>
        <?php if($model->getNextProject()):?>
        <a href="<?php echo $model->getNextProject()->getUrl();?>" class="next"><?= t('Следующий') ?></a>
        <?php endif;?>
    </div>
</div>

<?php if ($model->show_albom == Portfolio::STATUS_ENABLED): ?>
<?php
$imagesArr = $model->images;
krsort($imagesArr);
?>
<div class="structured_center">
    <div class="slider" id="portfolio-carousel">

        <?php foreach($imagesArr as $k=>$image):?>

            <span class="slide" id="i<?php echo $image->id;?>">
                <span style="display: inline-block;position: relative;">
                    <h1 class="z10img">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php echo $model->project_name; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php echo $model->project_name_uk; ?>
                        <?php endif; ?>
                    </h1>
                    <img  src="<?php echo $image->getImageUrl('more');?>" alt="Проект дома <?php echo $model->project_name ?> иллюстрация <?php echo $k+1;?>">
                </span>
                <?php if($image->description):?>
                <div class="slide_text"><?php echo $image->description;?></div>
                <?php endif;?>



                <?php if($k == 0 && $model->video_walk_url):?>
                    <a class="go_to_video_walk" href="<?php echo $model->video_walk_url; ?>" target="_blank">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl?>/img/go_to_video_walk.png" alt="">
                    </a>
                <?php endif; ?>

            </span>

            <?php if ($k == 0 && $model->project): ?>
                <?php foreach($model->project->projectVideo1->getVideos() as $k=>$video):?>
                    <span class="slide" id="v<?php echo $k;?>">
                        <?php echo $video->getVideoInsertHtml();?>
                    </span>
                <?php endforeach;?>
            <?php endif ?>

        <?php endforeach;?>
    </div>

    <a href="#" class="slider_prev"></a>
    <a href="#" class="slider_next"></a>

    <?php if (!$isMobile || $isTablet): ?>
        <div class="project_carusel clearfix">
        <a href="#" class="back pull-left" id="portfolio-thumbs-prev"></a>
        <?php if($model->getVideos()):?>
            <div class="video_block pull-left" style="width: <?php $model->getCountVideos() * 105;?>px;height: 60px;">
                <?php foreach($model->getVideos() as $video):?>
                    <a href="<?php echo $video['videoLink'];?>" class="portfolio-video various fancybox.iframe">
                        <?php echo $video['miniImageHtml'];?>
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/carusel_video_img.png" class="video_play_img">
                    </a>
                <?php endforeach;?>
            </div>
        <?php endif;?>

        <?php if(count($imagesArr) > 1): ?>
        <div style="width: <?php echo (945 - ($model->getCountVideos() * 105));?>px;height: 60px;" class="pull-left">
            <div class="pull-left carusel" style="width: <?php echo (945 - ($model->getCountVideos() * 105));?>px;height: 60px" id="portfolio-thumbs">

                <?php foreach($imagesArr as $i=>$image):?>
                    <a href="#i<?php echo $image->id;?>" class="<?php echo ($i == 0)?'selected':'';?>"><img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('mini');?>" alt="Проект дома <?php echo $model->project_name ?> иллюстрация <?php echo $i+1;?>"></a>

                    <?php if ($i == 0 && $model->project): ?>
                        <?php foreach($model->project->projectVideo1->getVideos() as $k=>$video):?>
                            <a href="#v<?php echo $k;?>"><?php echo $video->getVideoThumb();?></a>
                        <?php endforeach;?>
                    <?php endif ?>

                <?php endforeach;?>
            </div>
        </div>
        <?php endif ?>

        <a href="#" class="next pull-left" id="portfolio-thumbs-next"></a>
    </div>
    <?php endif ?>

</div>
<?php endif;?>

<?php if ($model->id == 34): ?>
    <div class="invitation"></div>
<?php endif; ?>

<div class="structured_bottom row">

    <div class="specification <?php echo trim(strip_tags($model->description)) ? 'col-md-3 col-md-push-9' : 'col-md-12'?>">
        <div class="specification-tit">
            <?= t('Проект') ?>:
            <?php
                if ($model->getProjectUrl()) {
                    echo CHtml::link($model->project_name, $model->getProjectUrl());
                } else {
                    if (Yii::app()->language == 'ru') {
                        echo $model->project_name;
                    } elseif (Yii::app()->language == 'uk') {
                        echo $model->project_name_uk;
                    }
                }
            ?>
        </div>
        <div class="specification-item">
            <?php if($model->gross_area):?>
                <strong><?= t('Общая площадь') ?>:</strong><br>
                <?php echo $model->gross_area;?> m<sup>2</sup>
            <?php endif;?>
        </div>
        <div class="specification-item">
            <?php if($model->construction_place):?>
                <strong><?= t('Место строительства') ?>:</strong><br>
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php echo $model->construction_place; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php echo $model->construction_place_uk; ?>
                <?php endif; ?>
            <?php endif;?>
        </div>
        <div class="specification-item">
            <?php if($model->workload):?>
                <strong><?= t('Заказанный объем работ') ?>:</strong><br>
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php echo $model->workload; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php echo $model->workload_uk; ?>
                <?php endif; ?>
            <?php endif;?>
        </div>
        <div class="specification-item">
            <?php if($model->turnaround_time):?>
                <strong><?= t('Сроки выполнения работ') ?>:</strong><br>
                <?php echo $model->turnaround_time;?>
            <?php endif;?>
        </div>
        <div class="specification-item">
            <?php if($model->period_of_works):?>
                <strong><?= t('Период выполнения работ') ?>:</strong><br>
                <?php echo $model->period_of_works;?>
            <?php endif;?>
        </div>
    </div>

    <?php if(trim(strip_tags($model->description))):?>
        <div class="description col-md-9 col-md-pull-3 text-justify">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $model->description; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php echo $model->description_uk; ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>

<?php if ($model->id != 34): ?>
    <div class="sign-up-for-viewing"></div>
<?php endif; ?>

<div class="block_social_networks">
    <?php echo $this->renderPartial('//layouts/share_buttons');?>
</div>

<?php
package('fancyboxHelpers');
package('carouFredSel');

$script = <<<JS

$('.portfolio-video').fancybox({
    autoResize: true,
    autoCenter: true
});

JS;

    $script .= <<<JS


    
function sliderButtonPos(item) {
    var width = item.outerWidth();
    var h = item.outerHeight();
    if (width) {
        var space = (1000 - width) / 2;
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    } else {
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    }
}

setTimeout(initSlider, 2000);

function initSlider() {
    $('#portfolio-carousel').carouFredSel({
        responsive: true,
        width: '100%',
        height: 'variable',
        items: {
            visible: 1,
            height: 'variable'
        },
        circular: false,
        auto: false,
        scroll: {
            fx: 'directscroll',
            onBefore: function( data ) {
                var item = data.items.visible.find('img,iframe');
                sliderButtonPos(item);
            }
        },
        onCreate: function(data) {
            var item = $(this).find('img,iframe');
            sliderButtonPos(item);
        },
        next:'.slider_next',
        prev:'.slider_prev'
    });

    $('#portfolio-thumbs').carouFredSel({
        circular: true,
        infinite: false,
        auto: false,
        prev: '#portfolio-thumbs-prev',
        next: '#portfolio-thumbs-next',
        items: {
            visible: {
                max: 9
            },
            width: 105,
            height: 60
        },
    });
}
    


$('#portfolio-thumbs a').click(function() {
    $('#portfolio-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
    $('#portfolio-carousel a').removeClass('selected');
    $(this).addClass('selected');
    return false;
});

    $.post('site/signUpForViewing', function(html){
        $('.sign-up-for-viewing').html(html);
    });

    $('.sign-up-for-viewing').on('submit', '#SignUpForViewing-form', function () {
        $('.sign-up-for-viewing-form-loader').show();
        $.post($.createUrl('site/signUpForViewing'), $(this).serialize(), function (html) {
            $('#sign-up-for-viewing').remove();
            $('.sign-up-for-viewing').html(html);
        });
        return false;
    });
    
    $.post('site/signUpForDemoHouse', function(html) {
        $('.invitation').html(html);
        $('#SignUpForDemoHouse_subject').val('Запрос на поездку в демо-дом. Альбом демо-дом');
    });

    $('.invitation').on('submit', '#signUpForDemoHouseForm', function () {
        $.post($.createUrl('site/signUpForDemoHouse'), $(this).serialize(), function (html) {
            $('#invitationInner').remove();
            $('.invitation').html(html);
        });
        return false;
    });
    
    $(document).keydown(function(event) {
        var keycode = event.keyCode || event.which;
        if (keycode == '37') {
            $('.slider_prev').click();
        } else if (keycode == '39') {
            $('.slider_next').click();
        }
    });
JS;

cs()->registerScript('portfolio-view', $script);

//cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/sign.up.for.viewing.js');
?>

<style type="text/css">
    <?php if($model->h1_size):?>
    .description h1, .description h1 span {
        font-size: <?php echo $model->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h2_size):?>
    .description h2, .description h2 span {
        font-size: <?php echo $model->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h3_size):?>
    .description h3, .description h3 span {
        font-size: <?php echo $model->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h4_size):?>
    .description h4, .description h4 span {
        font-size: <?php echo $model->h4_size?>px !important;
    }
    <?php endif?>
</style>


