<div class="goods_wrap">
    <?php if($model->image): ?>
    <a href="<?php echo $model->getUrl();?>" class="img_wrap">
        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $model->getImageUrl('small');?>" alt="Проект дома <?php echo $model->project_name ?> иллюстрация 1">
    </a>
    <?php elseif($model->firstImage): ?>
    <a href="<?php echo $model->getUrl();?>" class="img_wrap">
        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $model->firstImage->getImageUrl('small');?>" alt="Проект дома <?php echo $model->project_name ?> иллюстрация 1">
    </a>
    <?php endif;?>
    <a href="<?php echo $model->getUrl();?>" class="goods_wrap_bottom clearfix">
        <?php
        $z_nameStyle = '';
        if(strpos($model->project_name,'Z')===false && strpos($model->project_name,'z')===false)
            $z_nameStyle = 'font-size:12px;max-width:140px;line-height:15px;padding-top:3px;height:36px;';
        ?>
        <div class="z_name" style="<?php echo $z_nameStyle;?>">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $model->project_name; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php echo $model->project_name_uk; ?>
            <?php endif; ?>
        </div>
        <div class="goods_title">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $model->construction_place; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php echo $model->construction_place_uk; ?>
            <?php endif; ?>
        </div>
    </a>
</div>
