<?php
/**
 * @var CActiveDataProvider $dataProvider
 */
?>

<div class="catalog clearfix" id="portfolio-catalog">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<div class="invitation invitation-sm invitation-index" style="margin-bottom: 20px;"></div>

<div class="portfolio-catalog-text clearfix">

    <?php if (Yii::app()->language == 'ru'): ?>
        <?php echo config('portfolio_text'); ?>
    <?php elseif (Yii::app()->language == 'uk'): ?>
        <?php echo config('portfolio_text_uk'); ?>
    <?php endif; ?>

</div>

<div class="sign-up-for-viewing sign-up-for-viewing-narrow"></div>


<?php

$script = <<<JS

    $('#portfolio-catalog').on('click', '.portfolio-pagination .next a', function(){
        var lastCrossLink;
        var id = ($('.portfolio_cross_link:last').data('id'));
        lastCrossLink = id;
        $(this).addClass('disabled');
        $('.items-loader').show();
        $.get($(this).attr('href'), lastCrossLink, function(html){
            $('#portfolio-pager, .items-loader').remove();
            $('#portfolio-catalog').append(html);
            addCrossLinksSpace();
            init();
        });
        return false;
    });

    $.post('site/signUpForViewing', function(html){
        $('.sign-up-for-viewing').html(html);
    });

    $('.sign-up-for-viewing').on('submit', '#SignUpForViewing-form', function () {
        $('.sign-up-for-viewing-form-loader').show();
        $.post($.createUrl('site/signUpForViewing'), $(this).serialize(), function (html) {
            $('#sign-up-for-viewing').remove();
            $('.sign-up-for-viewing').html(html);
        });
        return false;
    });
    
    $.post('site/signUpForDemoHouse', function(html) {
        $('.invitation').html(html);
        $('#SignUpForDemoHouse_subject').val('Запрос на поездку в демо-дом. Мы построили');
    });

    $('.invitation').on('submit', '#signUpForDemoHouseForm', function () {
        $.post($.createUrl('site/signUpForDemoHouse'), $(this).serialize(), function (html) {
            $('#invitationInner').remove();
            $('.invitation').html(html);
        });
        return false;
    });

JS;
cs()->registerScript('portfolio-index', $script);

//cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/sign.up.for.viewing.js');
