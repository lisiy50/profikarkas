<?php
/* @var $crossLink CrossLink */

if (Yii::app()->language == 'ru') {
    $nameArr = explode('|', $crossLink->name);
} elseif (Yii::app()->language == 'uk') {
    $nameArr = explode('|', $crossLink->name_uk);
}


$style = '';
if($crossLink->image)
    $style = "background-image: url({$crossLink->getImageUrl('project')})";
?>

<div class="best_build pull-left whats_good" style="<?php echo $style;?>" data-id="<?php echo $crossLink->id ?>">
    <?php foreach($nameArr as $k=>$namePart):?>
        <div class="last_news_p<?php echo ($k>=(ceil(count($nameArr)/2)) && (floor(count($nameArr)/2)) != 0)?2:1;?>"><?php echo $namePart; ?></div>
    <?php endforeach;?>
    <a href="<?php echo $crossLink->getUrl(); ?>" class="last_news_wrap_link"><?= t('Подробнее') ?></a>
</div>
