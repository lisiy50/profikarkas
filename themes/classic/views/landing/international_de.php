<?php
/**
 * @var LandingController $this
 * @var $baseInformation Landing
 * @var $textInformation LandingText
 */

cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/scripts-int.min.js');
cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/landing_de.js');
cs()->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/css/styles-int.min.css');

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Prefab houses, Prefab houses Ukraine, Prefabricated houses, Prefabricated houses Ukraine" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/logo_small.png" media="nope!" onload="this.media='all'" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" media="nope!" onload="this.media='all'" rel="stylesheet">
    <title><?= $baseInformation->meta_title_ru ?></title>
</head>
<body>
<header class="header" role="banner">
    <div class="container">
        <h1 class="header__title header__title--german">
            <?= $baseInformation->title_ru ?>
        </h1>
    </div>
    <div class="header__nav">
        <span class="header__nav-btn">De</span>
        <div class="header__nav-inner">
            <a href="<?php echo Yii::app()->getHomeUrl(); ?>" class="header__nav-link">Ua</a>
            <a href="/en" class="header__nav-link">En</a>
            <a href="/fr" class="header__nav-link">Fr</a>
        </div>
    </div>
</header>
<main class="main">
    <section class="video">
        <div class="container">
            <div class="video__inner">
                <p class="video__text video__text--german">
                    <?php if (strip_tags($textInformation[57]['text_ru'])): ?>
                        <?= $textInformation[57]['text_ru'] ?>
                    <?php endif; ?>
                </p>
                <div class="video__video">
                    <iframe width="854" height="480" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://www.youtube.com/embed/6gbRxIBjDAk?rel=0&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--certificate" style="background-position: 50% 2.25em;">
        <div class="container">
            <h2 class="title-primary title-primary--fixed">
                Über uns
            </h2>
            <div class="section__inner section__inner--cooperate" style="border-bottom: 2px solid #000000;">
                <?php if (strip_tags($textInformation[77]['text_ru'])): ?>
                    <?= $textInformation[77]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--cooperate">
        <div class="container">
            <h2 class="title-secondary">
                Ihre Vorteile:
            </h2>
            <div class="section__inner section__inner--cooperate">
                <?php if (strip_tags($textInformation[79]['text_ru'])): ?>
                    <?= $textInformation[79]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--materials" style="background-position: 50% 50%;">
        <div class="container">
            <h2 class="title-primary title-primary--fixed">
                <?php if (strip_tags($textInformation[58]['text_ru'])): ?>
                    <?= $textInformation[58]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--materials">
                <p class="section__text">
                    <?php if (strip_tags($textInformation[59]['text_ru'])): ?>
                        <?= $textInformation[59]['text_ru'] ?>
                    <?php endif; ?>
                </p>
                <?php if (strip_tags($textInformation[60]['text_ru'])): ?>
                    <?= $textInformation[60]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary">
                <?php if (strip_tags($textInformation[61]['text_ru'])): ?>
                    <?= $textInformation[61]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--design">
                <?php if (strip_tags($textInformation[62]['text_ru'])): ?>
                    <?= $textInformation[62]['text_ru'] ?>
                <?php endif; ?>
            </div>
            <div class="slider">
                <div class="slider__gallery" id="projectsGallery">
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-01.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-02.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-03.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-04.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-05.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-06.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-07.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-08.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-09.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-10.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                </div>
                <div class="slider__thumbs" id="projectsThumbs">
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-01.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-02.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-03.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-04.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-05.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-06.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-07.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-08.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-09.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-10.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="kits">
        <div class="container">
            <h2 class="title-primary">
                <?php if (strip_tags($textInformation[63]['text_ru'])): ?>
                    <?= $textInformation[63]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="kits__inner">
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[64]['text_ru'])): ?>
                            <?= $textInformation[64]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-0.png" alt="Exterior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[65]['text_ru'])): ?>
                            <?= $textInformation[65]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-1.png" alt="Exterior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[66]['text_ru'])): ?>
                            <?= $textInformation[66]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-2.png" alt="Interior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[67]['text_ru'])): ?>
                            <?= $textInformation[67]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic" style="margin-top: initial;">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-3.png" alt="Intermediate floors" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[68]['text_ru'])): ?>
                            <?= $textInformation[68]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-4.png" alt="Roofs" class="kits__img">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary">Möchten Sie sich ansehen, was wir schon gebaut haben?</h2>
            <div class="slider">
                <div class="slider__gallery" id="housesGallery">
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-01.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-02.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-03.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-04.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-05.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-06.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-07.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-081.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-09.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-10.jpg" alt="">
                    </div>
                </div>
                <div class="slider__thumbs" id="housesThumbs">
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-01.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-02.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-03.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-04.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-05.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-06.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-07.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-081.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-09.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-10.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="video video--white">
        <div class="container">
            <h2 class="title-primary">Bewertung des Eigentümers des Hauses Lyon, Frankreich</h2>
            <div class="video__inner video__inner--white">
                <div class="video__video">
                    <iframe width="854" height="480" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://www.youtube.com/embed/KJSMwKNL3WA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--certificate">
        <div class="container">
            <h2 class="title-primary">
                <?php if (strip_tags($textInformation[69]['text_ru'])): ?>
                    <?= $textInformation[69]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--certificate">
                <div class="section__item section__item--text">
                    <p class="section__item-text">
                        <?php if (strip_tags($textInformation[70]['text_ru'])): ?>
                            <?= $textInformation[70]['text_ru'] ?>
                        <?php endif; ?>
                    </p>
                    <p class="section__item-text section__item-text--selected">
                        <?php if (strip_tags($textInformation[71]['text_ru'])): ?>
                            <?= $textInformation[71]['text_ru'] ?>
                        <?php endif; ?>
                    </p>
                </div>
                <div class="section__item section__item--pic">
                    <img class="section__item-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/certificate.jpg" alt="Certificate Passive House Institute">
                </div>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary">
                <?php if (strip_tags($textInformation[72]['text_ru'])): ?>
                    <?= $textInformation[72]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--design">
                <?php if (strip_tags($textInformation[73]['text_ru'])): ?>
                    <?= $textInformation[73]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--cooperate">
        <div class="container">
            <h2 class="title-primary">
                Wie kann man mit uns zusammenarbeiten?
            </h2>
            <div class="section__inner section__inner--cooperate">
                <?php if (strip_tags($textInformation[74]['text_ru'])): ?>
                    <?= $textInformation[74]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary title-secondary--small">
                <span class="title-secondary__item">Kontakte</span>
            </h2>
            <div class="section__inner section__inner--address">
                <address class="section__address">
                    <p class="section__address-item">
                        <span>Mobil:</span>
                        <span>
                            <a href="tel:<?= str_replace([' ', '-', '(', ')'], '', config('contact_phone_int')) ?>">
                                <?= config('contact_phone_int') ?>
                            </a>
                            -
                            <a href="https://t.me/MykolaKudinenko">Telegram</a>,
                            <a href="viber://chat/?number=%2B<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">Viber</a>,
                            <a href="https://wa.me/<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">
                                WhatsApp
                            </a>
                        </span>
                    </p>
                    <p class="section__address-item">
                        <span>Email:</span>
                        <span><a href="mailto:<?= config('contact_email') ?>"><?= config('contact_email') ?></a></span>
                    </p>
                </address>
            </div>
            <div style="padding: 0 25px 25px; background-color: #ffffff;text-align: center;">
                Company details: Limited Liability Company 'Profibudcomplect' //
                Registered office: 02000, Kyiv, Prospect Grigorenka, 38-А, apt. 157 //
                EDRPOU (National State Registry of Ukrainian Enterprises and Organizations) 38657557 //
                Acc. UA343510050000026001610250000 // Bank: PAO "UkrSibbank" Kharkiv // MFO code (sort code) 351005
            </div>
        </div>
    </section>
    <section class="form">
		<div id="form-container"></div>
    </section>
</main>
<script src="https://www.google.com/recaptcha/api.js?render=6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
            document.getElementById('g-recaptcha-response').value=token;
        });
    });
</script>
</body>
</html>
