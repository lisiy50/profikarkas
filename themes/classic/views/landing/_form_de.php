<?php
/**
 * @var LandingController $this
 * @var LandingDeForm $model
 */
?>

<div class="container">
    <div class="form__inner">

        <?php if(Yii::app()->user->hasFlash('success')): ?>
			<p class="form__title">
				<span class="form__title-item"><?php echo Yii::app()->user->getFlash('success'); ?></span>
			</p>
        <?php else: ?>
			<p class="form__title">
				<span class="form__title-item">Bei aufkommenden Fragen oder für zusätzliche</span>
				<span class="form__title-item">Informationen, bitte, kontaktieren Sie mit uns.</span>
			</p>

			<div class="form__wrap">

                <?php
                /* @var BootActiveForm $form */
                $form=$this->beginWidget('BootActiveForm', array(
                    'enableClientValidation'=>false,
                    'action' => url('landing/ajaxDeForm'),
                    'id' => 'LandingDeForm-form',
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                    ),
                ));
                ?>


				<?php if ($model->hasErrors()): ?>
                    <?php echo $form->errorSummary($model)?>
				<?php endif; ?>

                    <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                    <input type="hidden" name="action" value="validate_captcha">
					<div class="form__group">
                        <?php echo $form->textField($model, 'name', array('class'=>'form__control', 'placeholder' => 'Name *', 'required' => 'required'));?>
					</div>
					<div class="form__group form__group--group">
						<div class="form__group-item">

                            <?php echo $form->textField($model, 'country', array('class'=>'form__control', 'placeholder' => 'Land *', 'required' => 'required'));?>

						</div>
						<div class="form__group-item">
                            <?php echo $form->textField($model, 'city', array('class'=>'form__control', 'placeholder' => 'Stadt *', 'required' => 'required'));?>
						</div>
					</div>
					<div class="form__group">
                        <?php echo $form->emailField($model, 'email', array('class'=>'form__control', 'placeholder' => 'E-Mail-Adresse *', 'required' => 'required'));?>
					</div>
					<div class="form__group">
                        <?php echo $form->textArea($model, 'text', array('class'=>'form__control', 'placeholder' => 'Text *', 'required' => 'required'));?>
					</div>
					<div class="form__group">
                        <label class="form__btn-upload" for="LandingDeForm_file">
                            <?php echo $form->fileField($model, 'file', array('class'=>'', 'style'=>'display:none', 'onchange' => '$("#uploadFile").html(this.files[0].name)')); ?>
                            <span class="form__btn-upload-plus">+</span> angehängte Datei
                        </label>
                        <span class='form__label-upload' id="uploadFile"></span>
					</div>

					<div class="form__btn-wrap">
                        <?php echo CHtml::submitButton(t('Absenden'), array('class'=>"form__btn"));?>
					</div>

                    <p class="form__hint">Felder mit* sollen unbedingt ausgefüllt werden.</p>

                <?php $this->endWidget();?>

			</div>

        <?php endif; ?>

    </div>
</div>
