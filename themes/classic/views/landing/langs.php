<?php

echo 'Язык приложения: ' . Yii::app()->language;
echo '<br>';
echo 'Пример перевода: ' . t('Получилось');
echo '<br>';

?>

<a href="<?= Yii::app()->createUrl('landing/hotels') ?>">Ссылка: <?= Yii::app()->createUrl('landing/hotels') ?></a>
<br>
<div>Переключатель языков</div>
<a href="<?php echo Yii::app()->createUrl($this->route, CMap::mergeArray($_GET, array('lang'=>'uk'))); ?>">Uk</a>
<a href="<?php echo Yii::app()->createUrl($this->route, CMap::mergeArray($_GET, array('lang'=>'ru'))); ?>">Ru</a>
<a href="<?php echo Yii::app()->createUrl($this->route, CMap::mergeArray($_GET, array('lang'=>'en'))); ?>">En</a>
<a href="<?php echo Yii::app()->createUrl($this->route, CMap::mergeArray($_GET, array('lang'=>'de'))); ?>">De</a>
<a href="<?php echo Yii::app()->createUrl($this->route, CMap::mergeArray($_GET, array('lang'=>'fr'))); ?>">Fr</a>

