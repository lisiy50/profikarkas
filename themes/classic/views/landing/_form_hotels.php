<?php
/**
 * @var LandingController $this
 * @var LandingEnForm $model
 */
?>

<?php if (Yii::app()->user->hasFlash('success')): ?>
    <p class="request__success">
        <span class="request__success-item">
            <?= Yii::app()->user->getFlash('success'); ?>
        </span>
    </p>
<?php else: ?>
    <h2 class="request__title">
        <?php if (Yii::app()->language == 'ru'): ?>
            Готовы ответить на ваши вопросы
        <?php elseif (Yii::app()->language == 'uk'): ?>
            Готові відповісти на ваші питання
        <?php endif; ?>
    </h2>
    <p class="request__subtitle">
        <?php if (Yii::app()->language == 'ru'): ?>
            Заполните форму, мы перезвоним в ближайшее время
        <?php elseif (Yii::app()->language == 'uk'): ?>
            Заповніть форму, ми зателефонуємо найближчим часом
        <?php endif; ?>
    </p>


    <?php
    /* @var BootActiveForm $form */
    $form = $this->beginWidget('BootActiveForm', [
        'enableClientValidation'=>false,
        'action' => url('landing/hotelsForm'),
        'id' => 'hotelsForm',
        'htmlOptions' => [
            'enctype' => 'multipart/form-data',
        ],
    ]);

    ?>

    <div class="request__inner">
        <?php if ($model->hasErrors()): ?>
            <?= $form->errorSummary($model) ?>
        <?php endif; ?>

        <p class="request__inner-title">
            <?php if (Yii::app()->language == 'ru'): ?>
                Свяжитесь с нами
            <?php elseif (Yii::app()->language == 'uk'): ?>
                Зв'яжіться з нами
            <?php endif; ?>
        </p>

        <input type="hidden" id="g-recaptcha-response2" name="g-recaptcha-response">
        <input type="hidden" name="action" value="validate_captcha">

        <div class="request__group-wrap">
            <div class="request__group request__group--sm">
                <?= $form->textField($model, 'name', ['class'=>'request__control', 'placeholder' => t('Имя'), 'required' => 'required']); ?>
            </div>
            <div class="request__group request__group--sm">
                <?= $form->emailField($model, 'email', ['class'=>'request__control', 'placeholder' => 'E-mail', 'required' => 'required']); ?>
            </div>
            <div class="request__group request__group--sm">
                <?= $form->textField($model, 'phone', ['class'=>'request__control', 'placeholder' => t('Телефон'), 'required' => 'required']); ?>
            </div>
        </div>
        <div class="request__group">
            <?= $form->textArea($model, 'text', ['class'=>'request__control request__control--textarea', 'placeholder' => t('Комментарий')]); ?>
        </div>
        <div class="request__group request__group--file">
            <label class="request__btn-upload" for="LandingHotelsForm_file">
                <?= $form->fileField($model, 'file', ['class' => '', 'style'=>'display: none', 'onchange' => '$("#uploadFile").html(this.files[0].name)']); ?>
                <span class="request__btn-upload-icon">
                    <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.60876 3.34562H4.78282C5.0829 3.34562 5.32619 3.09794 5.32619 2.79775C5.32619 2.49756 5.0829 2.25 4.78282 2.25H2.60876C2.3087 2.25 2.06519 2.49759 2.06519 2.79775C2.06517 3.09792 2.30867 3.34562 2.60876 3.34562Z"/>
                        <path d="M2.60876 5.73431H7.1739C7.47418 5.73431 7.71749 5.48661 7.71749 5.18644C7.71749 4.88624 7.47418 4.63867 7.1739 4.63867H2.60876C2.3087 4.63867 2.06519 4.88626 2.06519 5.18644C2.06517 5.48663 2.30867 5.73431 2.60876 5.73431Z"/>
                        <path d="M7.39136 6.81836H2.60876C2.3087 6.81836 2.06519 7.06017 2.06519 7.36031C2.06519 7.6605 2.3087 7.90241 2.60876 7.90241H7.39138C7.69145 7.90241 7.93498 7.66052 7.93498 7.36031C7.93496 7.06017 7.69145 6.81836 7.39136 6.81836Z"/>
                        <path d="M7.27547 0H1.76319C1.09056 0 0.547852 0.545189 0.547852 1.21759V8.77852C0.547852 9.45104 1.09056 10 1.76319 10H8.237C8.90962 10 9.45231 9.45102 9.45231 8.77852V2.23033L7.27547 0ZM8.237 9.12592H1.76319C1.57023 9.12592 1.41018 8.9716 1.41018 8.77854V1.21759C1.41018 1.02455 1.57023 0.862328 1.76319 0.862328H6.85323V1.84108C6.85323 2.27138 7.2019 2.62148 7.6321 2.62589L8.58998 2.63567V8.77854C8.58998 8.9716 8.42995 9.12592 8.237 9.12592Z"/>
                    </svg>
                </span> <?= t('Прикрепить файл') ?>
            </label>
            <span class='request__label-upload' id="uploadFile"></span>
        </div>

        <div class="request__btn-wrap">
            <?= CHtml::submitButton(t('Отправить заявку'), ['class' => 'request__btn', 'id' => 'hotelsFormSend']); ?>
        </div>

    </div>

    <?php $this->endWidget();?>

<?php endif; ?>
