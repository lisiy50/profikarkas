<?php
/**
 * @var LandingController $this
 * @var Review[] $reviews
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="<?php echo Yii::app()->seo->metaKeywords; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo Yii::app()->seo->metaDescription; ?>">

    <meta property="og:image" content="https://profikarkas.com.ua/homepage/images/metaimage.jpg" />
    <meta property="og:image:secure_url" content="https://profikarkas.com.ua/homepage/images/metaimage.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="164" />

    <title><?php echo Yii::app()->seo->metaTitle; ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo_small.png" media="nope!" onload="this.media='all'" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl; ?>/homepage/css/styles1.min.css"/>

    <!-- Google Tag Manager -->
    <script async defer>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MBGQ5T');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body>

<script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'ru', parsetags: 'explicit'}
</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe loading="lazy" src="https://www.googletagmanager.com/ns.html?id=GTM-MBGQ5T"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="pre">
    <div class="cube-all">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div>
</div>

<div id="fb-root"></div>
<script async defer>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=567891339972064";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<header>
    <div class="header_wrap">
        <div class="first">
            <ul>
                <li>
                    <div class="header_lang">
                        <span class="header_lang_btn">UA</span>
                        <div class="header_lang_inner">
                            <a class="header_lang_link" href="<?= Yii::app()->createUrl('en'); ?>">EN</a>
                            <a class="header_lang_link" href="<?= Yii::app()->createUrl('de'); ?>">DE</a>
                            <a class="header_lang_link" href="<?= Yii::app()->createUrl('fr'); ?>">FR</a>
                        </div>
                    </div>
                </li>
                <li>
                    <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                        <a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a>
                    <?php endforeach;?>
                </li>
                <li>
                    <a href="mailto:<?php echo config('contact_email'); ?>"
                       onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'E-mail', {nonInteraction: true});">
                        <?php echo config('contact_email'); ?>
                    </a>
                </li>
                <li><a class="call_back" href="javascript:void(0)"><?= t('заказать обратный звонок')?></a></li>
                <li>
					<form action="<?php echo Yii::app()->createUrl('/search/index')?>">
						<input type="text"
                               name="q"
                               value="<?php  echo isset($_GET['q']) ? $_GET['q'] : ''; ?>"
                               placeholder="<?= t('поиск проекта по названию') ?>"
                               style="border: none; margin: 0; outline: none;">
						<button type="submit" style="display: none;"></button>
					</form>
                </li>
                <li><a href="<?php echo Yii::app()->createUrl('site/login'); ?>"><?php echo t('вход')?></a></li>
            </ul>
        </div>
    </div>
    <div id="order1" class="popup">
        <div class="popup_wrap">
            <div class="popup_logo">
                <a href="<?php echo Yii::app()->homeUrl; ?>"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo_menu.png" alt="<?php echo t('логотип')?>"></a>
                <div class="close_popup">
                    <a href="javascript:void(0)"></a>
                </div>
            </div>
            <div class="popup_flex">
                <div class="containerp">
                    <div class="card">
                        <?php $this->actionRecall(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="order2" class="popup">
        <div class="popup_wrap">
            <div class="popup_logo">
                <a href="<?php echo Yii::app()->homeUrl; ?>"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo_menu.png" alt="<?php echo t('логотип')?>"></a>
                <div class="close_popup">
                    <a href="javascript:void(0)"></a>
                </div>
            </div>
            <div class="popup_flex">
                <div class="containerp">
                    <div class="card">
                        <form>
                            <div class="input-container">
                                <input type="#{type}" id="#{label}" required="required"/>
                                <label for="#{label}"><?php echo t('Ваше имя')?></label>
                                <div class="bar"></div>
                            </div>
                            <div class="input-container">
                                <input type="#{type}" id="#{label}" required="required"/>
                                <label for="#{label}"><?php echo t('Контактный телефон')?></label>
                                <div class="bar"></div>
                            </div>
                            <div class="input-container">
                                <input type="#{type}" id="#{label}" required="required"/>
                                <label for="#{label}"><?php echo t('Ваш e-mail')?></label>
                                <div class="bar"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <a href="#" class="button_popup"><span><?php echo t('Отправить заявку')?></span></a>
            </div>
        </div>
    </div>
</header>
<header>
    <div class="wrap_nav">
        <div class="wrap_content">
            <nav class="navigation">
                <a class="navigation__logo" href="<?php echo Yii::app()->homeUrl; ?>">
                    <svg class="navigation__logo-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="240px" height="57px" viewBox="0 0 356.6 84.9" xml:space="preserve">
                        <style type="text/css">
                            /*.navigation__logo-svg-path2 {fill:#B72226; }*/
                        </style>
                        <polygon fill="#000000" points="33.2,9.9 26.5,9.9 26.5,21.6 33.2,16.3 "/>
                        <rect x="63.9" y="27.8" fill="#ffffff" width="17.2" height="4.6"/>
                        <path class="navigation__logo-svg-path2" d="M89.9,28.1H74.5l-7.6,35.6h10.8l2.3-11h6.3c3.8,0,7.6-0.3,10.8-2.4c3.6-2.3,5.8-6.6,5.8-11.4 C103,30.5,97.3,28.1,89.9,28.1 M86.8,44.3h-5l1.6-7.5H88c2.5,0,4.4,0.5,4.4,3.3C92.4,43.7,89.9,44.3,86.8,44.3"/>
                        <polygon fill="#000000" points="77.8,25.5 57.7,0 57,0.6 57,0.5 0,45.3 6,45.3 57.1,5.1 73.2,25.5 74.5,25.5"/>
                        <polygon fill="#000000" points="75.2,28.1 75.3,28.1 75.2,28.1"/>
                        <polygon fill="#B72226" points="71.8,28.2 32.2,28.2 10.6,45.3 17.4,45.3 68.2,45.3"/>
                        <polygon fill="#000000" points="71.9,27.9 56.7,8.7 32.2,28.2 71.8,28.2"/>
                        <polygon fill="#fec52c" points="68.2,45.3 17.4,45.3 17.4,63.7 64.3,63.7 64.4,63.2"/>
                        <polygon class="navigation__logo-svg-path3" points="82.1,55.4 82.2,55.3 82.1,55.3"/>
                        <path class="navigation__logo-svg-path2" d="M124.9,37.5c-1.4-0.3-2-0.4-3.1-0.4c-3.9,0-6,1.8-8.2,4.7l0.9-4h-9.1h-0.4l-2.1,10l-3.3,15.8h9.8l2.4-11.5	c0.9-4.1,2.7-6.6,7.4-6.6c1.3,0,2.3,0.2,3.7,0.6L124.9,37.5z"/>
                        <path class="navigation__logo-svg-path2" d="M138.1,37.2c-9.5,0-15.5,6.5-15.5,15.7c0,8.2,5.6,11.6,12.9,11.6c9.5,0,15.5-6.5,15.5-15.8 C151,40.8,145.4,37.2,138.1,37.2 M135.9,57.6c-2.3,0-3.5-1.5-3.5-4.3c0-3.1,1.3-9.3,5.4-9.3c2.5,0,3.5,2,3.5,4.3 C141.2,51.5,140,57.6,135.9,57.6"/>
                        <path class="navigation__logo-svg-path2" d="M172.4,37.9h-5.3l0.2-0.8c0.5-1.9,1.1-2.4,3.8-2.4c0.5,0,1,0,1.6,0h0.6l1.4-6.6c-2.3-0.2-3.1-0.2-5.1-0.2 c-7,0-10.1,1.5-11.8,8.6l-0.3,1.3h-4l-1.4,6.1h4.1l-4.1,19.7h9.9l4.1-19.7h5L172.4,37.9z"/>
                        <polygon class="navigation__logo-svg-path2" points="187.6,28.1 177.7,28.1 176.3,35 186.2,35"/>
                        <polygon class="navigation__logo-svg-path2" points="175.7,37.9 170.2,63.7 180.1,63.7 185.5,37.9"/>
                        <path class="navigation__logo-svg-path5" d="M225.7,28.1h-14.3l-7.1,6.6c-1.9,1.9-4.1,3.9-5.8,5.9c0.6-1.9,1.1-3.8,1.5-5.8l1.4-6.8h-10.9l-7.6,35.6H194l2.3-10.5 l3.9-3.4l6.5,13.9H219l-10.3-21L225.7,28.1z"/>
                        <path class="navigation__logo-svg-path5" d="M237.1,37.1c-6.3,0-12.2,1.4-13.5,8.7h9.3c0.7-2.1,1.7-2.7,3.8-2.7c2.1,0,3.3,0.8,3.3,2.1c0,3-3.2,2.8-8.1,3.1	c-7.3,0.6-11.8,3-11.8,8.9c0,4.6,3.2,7.3,8.3,7.3c3.5,0,6.4-1.2,8.3-3.4c-0.1,1-0.1,1.2-0.1,1.6c0,0.3,0,0.5,0.1,0.9h9.9 c-0.3-0.8-0.3-1.3-0.3-2.3c0-1.5,0.3-3,0.6-4.5l1.8-8.4c0.5-2.3,0.7-3.4,0.7-4.5C249.4,39.4,245.2,37.1,237.1,37.1 M238.1,53.8 c-0.8,3-1.8,5-5.2,5c-1.8,0-2.9-0.9-2.9-2.4c0-2,1.5-2.7,3.2-3c1.7-0.3,3.8-0.1,5.3-1.2L238.1,53.8z"/>
                        <path class="navigation__logo-svg-path5" d="M274.3,37.5c-1.4-0.3-1.9-0.4-3-0.4c-3.9,0-6,1.8-8.2,4.7l0.9-4h-9.4l-5.5,25.8h9.8l2.4-11.5c0.9-4.1,2.7-6.6,7.4-6.6 c1.3,0,2.3,0.2,3.7,0.6L274.3,37.5z"/>
                        <path class="navigation__logo-svg-path5" d="M304.3,37.9h-11.8l-5.9,6c-0.5,0.5-0.8,0.9-1.8,1.9c0.7-2.4,0.9-3.2,1-3.9l3-13.8h-9.8l-7.6,35.6h9.8l1.6-7.5l2.3-1.9 l3.5,9.4h10.8l-6.6-15.5L304.3,37.9z"/>
                        <path class="navigation__logo-svg-path5" d="M317.3,37.1c-6.3,0-12.2,1.4-13.5,8.7h9.3c0.7-2.1,1.7-2.7,3.8-2.7c2.1,0,3.3,0.8,3.3,2.1c0,3-3.2,2.8-8.1,3.1	c-7.3,0.6-11.8,3-11.8,8.9c0,4.6,3.2,7.3,8.3,7.3c3.5,0,6.4-1.2,8.3-3.4c-0.1,1-0.1,1.2-0.1,1.6c0,0.3,0,0.5,0.1,0.9h9.9 c-0.3-0.8-0.3-1.3-0.3-2.3c0-1.5,0.3-3,0.7-4.5l1.8-8.4c0.5-2.3,0.7-3.4,0.7-4.5C329.6,39.4,325.5,37.1,317.3,37.1 M318.3,53.8 c-0.8,3-1.8,5-5.2,5c-1.9,0-2.9-0.9-2.9-2.4c0-2,1.5-2.7,3.2-3c1.7-0.3,3.8-0.1,5.3-1.2L318.3,53.8z"/>
                        <path class="navigation__logo-svg-path5" d="M356.6,45.5v-0.8c0-5-3.9-7.4-11.8-7.4c-6.1,0-12.9,1.9-12.9,9.1c0,3.8,2.3,6.1,7,7c4,0.8,6.6,0.9,6.6,2.8	c0,1.3-1.2,2.1-3.3,2.1c-2.5,0-3.6-0.9-3.6-2.8v-0.3h-9.1c0,0.5-0.1,0.6-0.1,0.9c0,6.9,6.8,8.4,12.3,8.4c6.2,0,13.4-2.3,13.4-9.5 c0-6.4-5.1-7.2-10.1-8c-1.2-0.2-3.2-0.4-3.2-2c0-1.2,1.1-2,2.8-2c2,0,3,0.7,3,2.3c0,0.1,0,0.3-0.1,0.4H356.6z"/>
                        <path fill="#58595b" d="M189,84.9l0.6-3H183l-0.6,3h-2.6l1.1-5h1.2c0.9-1.1,2.1-4.1,2.6-6.2l0.4-1.8h8l-1.7,8h1.3l-1.1,5H189z M189.9,74h-2.6l-0.1,0.3c-0.5,2.1-1.5,4.6-2.3,5.7h3.7L189.9,74z"/>
                        <path fill="#58595b" d="M207.7,76c0,3.5-2.2,6.3-5.9,6.3c-2.8,0-4.6-1.6-4.6-4.4c0-3.6,2.4-6.3,5.8-6.3C206,71.6,207.7,73.2,207.7,76 M200,77.9c0,1.5,0.8,2.3,2.1,2.3c2.1,0,2.9-2.6,2.9-4.3c0-1.4-0.7-2.3-2-2.3C201.3,73.6,200,75.5,200,77.9"/>
                        <polygon fill="#58595b" points="221,82 222.7,74.3 222.6,74.3 219,82 216.4,82 216.1,74.3 216,74.3 214.4,82 211.8,82 213.9,71.9 217.9,71.9 218.2,79.2 218.3,79.2 221.7,71.9 225.8,71.9 223.7,82"/>
                        <path fill="#58595b" d="M235.6,71.6c2.6,0,4.1,0.9,4.1,2.5c0,0.5-0.1,1-0.3,1.8l-0.8,3.6c-0.2,0.8-0.2,1.2-0.2,1.7 c0,0.3,0,0.4,0.1,0.7h-2.7c0-0.2,0-0.2,0-0.4c0-0.2,0-0.4,0-0.6c-0.8,0.9-1.9,1.3-3.2,1.3c-2,0-3.2-1.1-3.2-2.8	c0-2.6,2.3-3.3,4.8-3.5c1.8-0.1,2.8-0.1,2.8-1.5c0-0.7-0.6-1.1-1.7-1.1c-1.1,0-1.7,0.4-2,1.5h-2.6 C231.2,72.3,233.2,71.6,235.6,71.6 M234.7,77.6c-1.3,0.1-2.6,0.3-2.6,1.6c0,0.8,0.6,1.3,1.5,1.3c1.4,0,2.3-1,2.6-2.4l0.2-1 C236.1,77.5,235.3,77.5,234.7,77.6"/>
                        <polygon fill="#58595b" points="263.3,82 264.2,77.7 260.3,77.7 259.4,82 256.6,82 258.8,71.9 261.5,71.9 260.7,75.6 264.6,75.6 265.4,71.9 268.1,71.9 266,82"/>
                        <path fill="#58595b" d="M277.9,71.6c2.6,0,4.1,0.9,4.1,2.5c0,0.5-0.1,1-0.3,1.8l-0.8,3.6c-0.2,0.8-0.2,1.2-0.2,1.7 c0,0.3,0,0.4,0.1,0.7h-2.7c0-0.2,0-0.2,0-0.4c0-0.2,0-0.4,0-0.6c-0.8,0.9-1.9,1.3-3.2,1.3c-2,0-3.2-1.1-3.2-2.8	c0-2.6,2.3-3.3,4.8-3.5c1.8-0.1,2.8-0.1,2.8-1.5c0-0.7-0.6-1.1-1.7-1.1c-1.1,0-1.7,0.4-2,1.5H273C273.5,72.3,275.5,71.6,277.9,71.6 M277,77.6c-1.3,0.1-2.6,0.3-2.6,1.6c0,0.8,0.6,1.3,1.5,1.3c1.4,0,2.3-1,2.6-2.4l0.2-1C278.4,77.5,277.6,77.5,277,77.6"/>
                        <path fill="#58595b" d="M305.8,71.9c1.6,0,3.7,0.1,3.7,2.3c0,1.2-0.7,2.1-1.9,2.4c0.9,0.3,1.4,1,1.4,2c0,1.4-0.9,2.7-2.3,3.1 c-0.6,0.2-1.1,0.2-2.3,0.2H299l2.1-10H305.8z M302,80.1h2.1c1,0,2.2-0.1,2.2-1.4c0-1.1-1.1-1.1-1.9-1.1h-1.9L302,80.1z M302.9,75.8 h2.1c0.9,0,1.8-0.1,1.8-1.2c0-0.9-0.9-0.9-1.5-0.9h-1.9L302.9,75.8z"/>
                        <path fill="#58595b" d="M318.3,82.3c-2.9,0-4.6-1.6-4.6-4.5c0-3.6,2.4-6.2,5.7-6.2c2.8,0,4.5,1.7,4.5,4.4c0,0.6,0,0.9-0.2,1.6h-7.4 c0,0.2,0,0.3,0,0.5c0,1.4,0.8,2.3,2.1,2.3c1,0,1.7-0.5,2.1-1.4h2.7C322.6,81.3,320.7,82.3,318.3,82.3 M321.3,75.9v-0.3 c0-1.3-0.7-2.1-2-2.1c-1.3,0-2.3,0.9-2.7,2.4H321.3z"/>
                        <polygon fill="#58595b" points="335.9,71.9 339.4,71.9 334.8,75.9 337.5,82 334.4,82 332.7,77.7 331.2,79 330.5,82 327.8,82 330,71.9 332.7,71.9 331.9,75.6"/>
                        <path fill="#58595b" d="M348,71.6c2.6,0,4.1,0.9,4.1,2.5c0,0.5-0.1,1-0.3,1.8l-0.8,3.6c-0.2,0.8-0.2,1.2-0.2,1.7c0,0.3,0,0.4,0.1,0.7 h-2.7c0-0.2,0-0.2,0-0.4c0-0.2,0-0.4,0-0.6c-0.8,0.9-1.9,1.3-3.2,1.3c-2,0-3.2-1.1-3.2-2.8c0-2.6,2.3-3.3,4.8-3.5 c1.8-0.1,2.8-0.1,2.8-1.5c0-0.7-0.6-1.1-1.7-1.1c-1.1,0-1.7,0.4-2,1.5h-2.6C343.6,72.3,345.6,71.6,348,71.6 M347.1,77.6	c-1.3,0.1-2.6,0.3-2.6,1.6c0,0.8,0.6,1.3,1.5,1.3c1.4,0,2.3-1,2.6-2.4l0.2-1C348.5,77.5,347.7,77.5,347.1,77.6"/>
                    </svg>
                </a>
                <ul class="first_appear">
                    <?php foreach(MenuItem::model()->findAll('parent_id=1') as $menuItem):?>
                        <li class="margin_formenu">
                            <a href="<?= $menuItem->url ?>">
                                <span><?= $menuItem->name ?></span>
                            </a>
                        </li>
                    <?php endforeach;?>
                </ul>
                <div class="start_line_first"></div>
                <div class="start_line_second"></div>
            </nav>
            <div class="aditional"></div>
        </div>
    </div>
</header>

<div class="lines">
    <svg class="house_line1">
        <line data-0="stroke-dashoffset:400;" data-100-start="stroke-dashoffset:0;" x1="3" y1="0" x2="3" y2="400" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="400"/>
    </svg>
    <svg class="house_line2">
        <line data-100-start="stroke-dashoffset:125;" data-150-start="stroke-dashoffset:0;" x1="123" y1="2" x2="0" y2="2" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="125"/>
    </svg>
    <svg class="house_line3">
        <line data-150-start="stroke-dashoffset:195;" data-250-start="stroke-dashoffset:0;" x1="195" y1="2" x2="0" y2="2" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="195"/>
    </svg>
    <svg class="house_line4">
        <line data-250-start="stroke-dashoffset:92;" data-300-start="stroke-dashoffset:0;" x1="92" y1="2" x2="0" y2="2" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="92"/>
    </svg>
    <svg class="house_line5">
        <line data-300-start="stroke-dashoffset:385;" data-450-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="385" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="385"/>
        <line data-450-start="stroke-dashoffset:157;" data-500-start="stroke-dashoffset:0;" x1="2" y1="385" x2="2" y2="542" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="157"/>
    </svg>
    <svg class="house_line6">
        <line data-600-start="stroke-dashoffset:625;" data-950-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="625" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="625"/>
        <line data-950-start="stroke-dashoffset:400;" data-1400-start="stroke-dashoffset:0;" x1="2" y1="625" x2="2" y2="1025" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="400"/>
    </svg>
    <svg class="house_line7">
        <line data-1600-start="stroke-dashoffset:118;" data-1700-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="118" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="118"/>
        <line data-1800-start="stroke-dashoffset:521;" data-2200-start="stroke-dashoffset:0;" x1="2" y1="303" x2="2" y2="824" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="521"/>
    </svg>
    <svg class="house_line8">
        <line data-2300-start="stroke-dashoffset:500;" data-2500-start="stroke-dashoffset:0;" x1="4" y1="8" x2="347" y2="237" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="500" />
        <line data-2500-start="stroke-dashoffset:822;" data-3000-start="stroke-dashoffset:0;" x1="346" y1="236" x2="346" y2="822" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="822" />
    </svg>
    <svg class="house_line9">
        <line data-3250-start="stroke-dashoffset:2205;" data-5200-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="2205" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="2205" />
    </svg>
    <svg class="house_line10" style="display: none;">
        <line data-5321-start="stroke-dashoffset:364;" data-5400-start="stroke-dashoffset:0;" x1="0" y1="2" x2="363" y2="2" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="364" />
        <line data-5400-start="stroke-dashoffset:485;" data-5600-start="stroke-dashoffset:0;" x1="361" y1="2" x2="361" y2="485" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="485" />
        <line data-5600-start="stroke-dashoffset:210;" data-5700-start="stroke-dashoffset:0;" x1="363" y1="485" x2="155" y2="485" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="210" />
        <line data-5700-start="stroke-dashoffset:35;" data-5750-start="stroke-dashoffset:0;" x1="157" y1="485" x2="157" y2="520" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="35" />
    </svg>
    <svg class="house_line11">
        <line data-5900-start="stroke-dashoffset:110;" data-6000-start="stroke-dashoffset:0;" x1="220" y1="20" x2="220" y2="130" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="110" />
        <line data-6000-start="stroke-dashoffset:55;" data-6050-start="stroke-dashoffset:0;" x1="220" y1="225" x2="220" y2="280" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="55" />
        <line data-6050-start="stroke-dashoffset:610;" data-6300-start="stroke-dashoffset:0;" x1="218" y1="280" x2="822" y2="280" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-6300-start="stroke-dashoffset:55;" data-6350-start="stroke-dashoffset:0;" x1="820" y1="280" x2="820" y2="335" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="55" />
        <line data-6350-start="stroke-dashoffset:43;" data-6400-start="stroke-dashoffset:0;" x1="820" y1="427" x2="820" y2="470" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="43" />
        <line data-6400-start="stroke-dashoffset:610;" data-6550-start="stroke-dashoffset:0;" x1="820" y1="468" x2="220" y2="468" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-6550-start="stroke-dashoffset:51;" data-6600-start="stroke-dashoffset:0;" x1="220" y1="466" x2="220" y2="517" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="51" />
        <line data-6600-start="stroke-dashoffset:65;" data-6650-start="stroke-dashoffset:0;" x1="220" y1="605" x2="220" y2="670" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="65" />
        <line data-6650-start="stroke-dashoffset:610;" data-6900-start="stroke-dashoffset:0;" x1="220" y1="668" x2="820" y2="668" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-6750-start="stroke-dashoffset:610;" data-6900-start="stroke-dashoffset:0;" x1="220" y1="668" x2="820" y2="668" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-6900-start="stroke-dashoffset:55;" data-6950-start="stroke-dashoffset:0;" x1="820" y1="666" x2="820" y2="721" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="55" />
        <line data-6950-start="stroke-dashoffset:70;" data-7000-start="stroke-dashoffset:0;" x1="820" y1="815" x2="820" y2="880" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="70" />
        <line data-7000-start="stroke-dashoffset:610;" data-7150-start="stroke-dashoffset:0;" x1="820" y1="878" x2="220" y2="878" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-7150-start="stroke-dashoffset:69;" data-7200-start="stroke-dashoffset:0;" x1="220" y1="876" x2="220" y2="945" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="69" />
        <line data-7200-start="stroke-dashoffset:68;" data-7250-start="stroke-dashoffset:0;" x1="220" y1="1032" x2="220" y2="1100" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="68" />
        <line data-7250-start="stroke-dashoffset:610;" data-7400-start="stroke-dashoffset:0;" x1="218" y1="1100" x2="820" y2="1100" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-7400-start="stroke-dashoffset:57;" data-7450-start="stroke-dashoffset:0;" x1="820" y1="1098" x2="820" y2="1155" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="57" />
        <line data-7450-start="stroke-dashoffset:75;" data-7500-start="stroke-dashoffset:0;" x1="820" y1="1245" x2="820" y2="1320" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="75" />
        <line data-7500-start="stroke-dashoffset:610;" data-7650-start="stroke-dashoffset:0;" x1="820" y1="1318" x2="220" y2="1318" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-7650-start="stroke-dashoffset:69;" data-7700-start="stroke-dashoffset:0;" x1="220" y1="1316" x2="220" y2="1385" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="69" />
        <line data-7700-start="stroke-dashoffset:60;" data-7750-start="stroke-dashoffset:0;" x1="220" y1="1475" x2="220" y2="1535" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="60" />
        <line data-7750-start="stroke-dashoffset:610;" data-7900-start="stroke-dashoffset:0;" x1="218" y1="1535" x2="820" y2="1535" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-7900-start="stroke-dashoffset:67;" data-7950-start="stroke-dashoffset:0;" x1="820" y1="1533" x2="820" y2="1580" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="67" />
        <line data-7950-start="stroke-dashoffset:55;" data-8000-start="stroke-dashoffset:0;" x1="820" y1="1665" x2="820" y2="1720" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="55" />
        <line data-8000-start="stroke-dashoffset:610;" data-8150-start="stroke-dashoffset:0;" x1="820" y1="1718" x2="220" y2="1718" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="610" />
        <line data-8150-start="stroke-dashoffset:94;" data-8200-start="stroke-dashoffset:0;" x1="220" y1="1716" x2="220" y2="1810" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="94" />
    </svg>
    <svg class="house_line12">
        <line data-9600-start="stroke-dashoffset:113;" data-9650-start="stroke-dashoffset:0;" x1="256" y1="0" x2="256" y2="113" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="113"/>
        <line data-9700-start="stroke-dashoffset:424;" data-9850-start="stroke-dashoffset:0;" x1="350" y1="158" x2="774" y2="158" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="424"/>
        <line data-9850-start="stroke-dashoffset:424;" data-9900-start="stroke-dashoffset:0;" x1="773" y1="158" x2="1073" y2="348" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="424"/>
        <line data-9900-start="stroke-dashoffset:592;" data-10100-start="stroke-dashoffset:0;" x1="1028" y1="320" x2="1028" y2="912" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="592"/>
    </svg>
    <svg class="house_line13">
        <line data-10200-start="stroke-dashoffset:745;" data-10700-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="745" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="745"/>
    </svg>
    <svg class="house_line14">
        <line data-11800-start="stroke-dashoffset:30;" data-11850-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="30" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="30"/>
        <line data-11800-start="stroke-dashoffset:205;" data-11900-start="stroke-dashoffset:0;" x1="2" y1="323" x2="2" y2="528" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="205"/>
        <line data-11900-start="stroke-dashoffset:85;" data-12000-start="stroke-dashoffset:0;" x1="2" y1="528" x2="2" y2="613" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="85"/>
    </svg>
    <svg class="house_line15">
        <line data-12200-start="stroke-dashoffset:621;" data-12300-start="stroke-dashoffset:0;" x1="4" y1="2" x2="625" y2="2" style="stroke: #ffde17;stroke-width: 4px;" stroke-dasharray="621"/>
    </svg>
</div>
<div class="lines_under lines">
    <svg class="house_under_line1">
        <line data-500-start="stroke-dashoffset:756;" data-600-start="stroke-dashoffset:0;" x1="0" y1="0" x2="756" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="756"/>
    </svg>
    <svg class="house_under_line2">
        <line data-1500-start="stroke-dashoffset:545;" data-1600-start="stroke-dashoffset:0;" x1="545" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="545"/>
    </svg>
    <svg class="house_under_line3">
        <line data-2180-start="stroke-dashoffset:482;" data-2300-start="stroke-dashoffset:0;" x1="0" y1="0" x2="482" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="482"/>
    </svg>
    <svg class="house_under_line4">
        <line data-3000-start="stroke-dashoffset:914;" data-3250-start="stroke-dashoffset:0;" x1="914" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="914"/>
    </svg>
    <svg class="house_under_line5">
        <line data-4800-start="stroke-dashoffset:825;" data-4950-start="stroke-dashoffset:0;" x1="0" y1="0" x2="825" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="825"/>
    </svg>
    <svg class="house_under_line6" style="display: none;">
        <line data-5200-start="stroke-dashoffset:911;" data-5500-start="stroke-dashoffset:0;" x1="0" y1="0" x2="911" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="911"/>
    </svg>
    <svg class="house_under_line7">
        <line data-5300-start="stroke-dashoffset:911;" data-5550-start="stroke-dashoffset:0;" x1="455" y1="0" x2="81" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="911"/>
        <line data-5300-start="stroke-dashoffset:911;" data-5550-start="stroke-dashoffset:0;" x1="455" y1="0" x2="911" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="911"/>
    </svg>
    <svg class="house_under_line8">
        <line data-6120-start="stroke-dashoffset:600;" data-6250-start="stroke-dashoffset:0;" x1="0" y1="0" x2="600" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="600"/>
    </svg>
    <svg class="house_under_line9">
        <line data-8200-start="stroke-dashoffset:747;" data-8300-start="stroke-dashoffset:0;" x1="0" y1="0" x2="747" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="747"/>
    </svg>
    <svg class="house_under_line10">
        <line data-9650-start="stroke-dashoffset:747;" data-9750-start="stroke-dashoffset:0;" x1="0" y1="0" x2="747" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="747"/>
    </svg>
    <svg class="house_under_line11">
        <line data-10100-start="stroke-dashoffset:1020;" data-10200-start="stroke-dashoffset:0;" x1="1026" y1="0" x2="6" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="1020"/>
    </svg>
    <svg class="house_under_line12">
        <line data-10700-start="stroke-dashoffset:571;" data-10800-start="stroke-dashoffset:0;" x1="0" y1="0" x2="571" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="571"/>
    </svg>
    <svg class="house_under_line13">
        <line data-10800-start="stroke-dashoffset:1255;" data-11700-start="stroke-dashoffset:0;" x1="2" y1="0" x2="2" y2="1255" style="stroke: #000;stroke-width: 4px;" stroke-dasharray="1255"/>
    </svg>
    <svg class="house_under_line14">
        <line data-11700-start="stroke-dashoffset:452;" data-11800-start="stroke-dashoffset:0;" x1="452" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="452"/>
    </svg>
</div>

<div class="menu_collapse_768">
    <div class="yel">
        <p><?= t('Меню') ?></p>
        <div class="burger"></div>
    </div>
    <ul>
        <li class="logo99">
            <a href="<?php echo Yii::app()->homeUrl; ?>"><img loading="lazy" class="logo992" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo_menu.png" alt="<?php echo t('логотип')?>"></a>
        </li>
        <?php foreach(MenuItem::model()->findAll('parent_id=1') as $menuItem):?>
            <li>
                <a href="<?= $menuItem->url ?>">
                    <span><?= $menuItem->name ?></span>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
    <ul class="visible-xs black">
        <li><a href="<?php echo Yii::app()->createUrl('site/login'); ?>"><?= t('Вход')?></a></li>
        <li><a href="mailto:<?php echo config('contact_email'); ?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'E-mail', {nonInteraction: true});"><?php echo config('contact_email'); ?></a></li>
        <li>
            <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                <a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a>
            <?php endforeach;?>
        </li>
        <li><a class="call_back" href="#"><?= t('заказать обратный звонок')?></a></li>
    </ul>
    <div class="lang-link-wrap">
        <a class="lang-link" href="<?= Yii::app()->createUrl('en'); ?>">En</a>
        <a class="lang-link" href="<?= Yii::app()->createUrl('de'); ?>">De</a>
        <a class="lang-link" href="<?= Yii::app()->createUrl('fr'); ?>">Fr</a>
    </div>
</div>

<div class="back">
</div>

<div id="overlay"></div>

<section class="blocks">
    <div class="wrap">
        <div class="house">
            <img loading="lazy" class="logo768" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo.png" alt="<?= t('логотип')?>">
            <div class="path">
                <div class="title">
                    <h1><?= t('Сборка дома') ?></h1><br>
                    <span><?= t('за 1 день') ?></span>
                </div>
                <div>
                    <h2><?= t('заводское производство')?></h2>
                    <h2><?= t('немецкое качество')?></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="id1">
    <div class="container second">
        <div class="col-xs-3 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-2"><span>01</span>
        </div>
        <div class="col-xs-8 col-xs-offset-1 col-sm-5 col-sm-offset-1 col-md-5 col-xs-offset-0 col-lg-5 col-lg-offset-0">
            <h3><?= t('При строительстве')?><br><b><?= t('каркасного дома')?></b> <?= t('вы')?><br> <?= t('столкнетесь с вопросами')?>:</h3>
        </div>

    </div>
</section>

<section>
    <div class="container third">
        <div class="hidden-xs col-sm-7 col-md-6 col-md-offset-1 col-lg-5 col-lg-offset-1 previmg-slide">
            <div class="for_user previewImageShow">
            </div>
        </div>
        <div class="col-xs-9 col-xs-offset-3  col-sm-1 col-sm-offset-0 col-md-2 col-lg-2 let1 outerdiv ">
            <div class="small">
                <h5 class="right"><?= t('не знаете какой дом выбрать?')?></h5>
                <svg class="active_slide">
                    <line data-675-start="stroke-dashoffset:240;" data-700-start="stroke-dashoffset:200;" x1="240" y1="0" x2="0" y2="0" style="display: none;stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                    <line data-675-start="stroke-dashoffset:240;" data-700-start="stroke-dashoffset:0;" x1="240" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                </svg>
                <span>1</span>
                <p class="right_text"><?= t('Не можете определиться с проектом? Хотите быстро, качественно и бюджетно?')?></p>
                <div class="for_user previewImage">
                    <div class="image_768 image_768_new" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/homepage/images/img1v3.jpg);"></div>
                    <picture>
                        <img loading="lazy" class="image_768 f2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/question.webp">
                    </picture>
                </div>
            </div>
            <div class="small">
                <h5 class="right"><?= t('на сколько затянется строительство')?></h5>
                <svg class="active_slide">
                    <line data-750-start="stroke-dashoffset:240;" data-800-start="stroke-dashoffset:200;" x1="240" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                    <line data-750-start="stroke-dashoffset:240;" data-800-start="stroke-dashoffset:0;" x1="240" y1="0" x2="0" y2="0" style="display: none;stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                </svg>
                <span class="hidden-xs">3</span>
                <span class="hidden-sm hidden-md hidden-lg">2</span>
                <p class="right_text"><?= t('Можно строить 3 месяца, а можно 4 года.')?> <?= t('Как не попасть на долгострой?')?></p>
                <div class="for_user previewImage">
                    <picture>
                        <img loading="lazy" class="image_768" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/breakdown.webp">
                    </picture>
                    <picture>
                        <img loading="lazy" class="image_768 f2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/man333.webp">
                    </picture>
                </div>
            </div>
            <div class="small height_change">
                <h5 class="right padd"><?= t('будет ли дом высоко качества')?></h5>
                <svg class="active_slide">
                    <line data-800-start="stroke-dashoffset:240;" data-850-start="stroke-dashoffset:200;" x1="240" y1="0" x2="0" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                    <line data-800-start="stroke-dashoffset:240;" data-850-start="stroke-dashoffset:0;" x1="240" y1="0" x2="0" y2="0" style="display: none;stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                </svg>
                <span class="hidden-xs">5</span>
                <span class="hidden-sm hidden-md hidden-lg">3</span>
                <p class="right_text"><?= t('Как проконтролировать качество работ?')?> <?= t('Как убедиться в том, что дом будет надежным и долговечным?')?></p>
                <div class="for_user previewImage">
                    <picture>
                        <img loading="lazy" class="image_768" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/house12345.webp">
                    </picture>
                    <picture>
                        <img loading="lazy" class="image_768 f2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/man333333.webp">
                    </picture>
                </div>
            </div>
            <img loading="lazy" class="line_first" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line.png">
        </div>
        <div class="col-xs-9 col-xs-offset-3  col-sm-1 col-sm-offset-0 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1 let2 outerdiv outerdiv-2">
            <div class="small small_second">
                <h5 class="paddl"><?= t('вырастет ли стоимость во время строительства')?></h5>
                <svg class="active_slide">
                    <line data-750-start="stroke-dashoffset:240;" data-800-start="stroke-dashoffset:200;" x1="0" y1="0" x2="240" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                    <line data-750-start="stroke-dashoffset:240;" data-800-start="stroke-dashoffset:0;" x1="0" y1="0" x2="240" y2="0" style="display: none;stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                </svg>
                <span class="hidden-xs sher">2</span>
                <span class="hidden-sm hidden-md hidden-lg">4</span>
                <p class="biger_width">
                    <?= t('Придется ли вам доплачивать во время строительства?')?>
                    <?= t('Все ли учтено в смете?')?>
                    <?= t('Нет ли в смете лишних позиций?')?></p>
                <div class="for_user previewImage">
                    <div class="image_768 image_768_new" style="background-image: url(<?php echo Yii::app()->baseUrl; ?>/homepage/images/image-01.jpg);"></div>
                    <picture>
                        <img loading="lazy" class="image_768 f2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/man33.webp">
                    </picture>
                </div>
            </div>
            <div class="small">
                <h5><?= t('сложно выбрать подрядчика')?></h5>
                <svg class="active_slide">
                    <line data-825-start="stroke-dashoffset:240;" data-875-start="stroke-dashoffset:200;" x1="0" y1="0" x2="240" y2="0" style="stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                    <line data-825-start="stroke-dashoffset:240;" data-875-start="stroke-dashoffset:0;" x1="0" y1="0" x2="240" y2="0" style="display: none;stroke: #ffde17;stroke-width: 44px;" stroke-dasharray="240"/>
                </svg>
                <span class="hidden-xs sher">4</span>
                <span class="hidden-sm hidden-md hidden-lg">5</span>
                <p class="biger_width">
                    <?= t('Не знаете, как убедиться в надежности подрядчика, в его профессионализме?')?>
                    <?= t('В компании не слышат ваших пожеланий?')?></p>
                <div class="for_user previewImage">
                    <picture>
                        <img loading="lazy" class="image_768" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/house1234.webp">
                    </picture>
                    <picture>
                        <img loading="lazy" class="image_768 f2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/man3333.webp">
                    </picture>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="id6">
    <div class="container fourth">
        <div class="hidden-xs col-sm-2 col-sm-offset-0 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1 fourth_768">
            <picture>
                <img loading="lazy" class="hidden-xs for_smn" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/man.webp">
            </picture>
        </div>
        <div class="col-xs-9 col-sm-6 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1 fouth-title">
            <h4><?= t('под видом каркасного<br>дома<b> вам могут продать</b><br>сип панельный')?></h4>
            <span class="hidden-xs"><?= t('чем это опасно')?></span>
        </div>
        <div class="col-xs-2 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1"><span>02</span>
        </div>
    </div>
    <div class="container five">
        <div class="col-xs-12 col-sm-2 col-sm-offset-0 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1 five_768">
            <span class="visible-xs word"><?= t('мнение инженера')?></span>
            <div class="opstup">
                <picture>
                    <img loading="lazy" class="visible-xs image320" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/image320.png">
                </picture>
                <p><?= t('Дом в первую очередь должен быть безопасным, надежным и качественным.')?>
					<?= t('Поэтому никогда не думал работать с sip панелями из-за ключевых особенностей технологии')?>:</p>
                <ol>
                    <li><?= t('токсичности пенополистирола')?></li>
                    <li><?= t('недостаточной прочности каркаса')?>,</li>
                    <li><?= t('наличия мостиков холода')?>.</li>
                </ol>
                <p><?= t('Каркасная технология обеспечивает отличные качество и надежность, гарантирует безопасность')?></p>
            </div>
        </div>
        <div class="col-sm-8 col-sm-offset-1 col-md-7 col-md-offset-1 col-lg-7 col-lg-offset-1">
            <div class="image_bg">
                <img loading="lazy" class="path_second" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line2.png">
                <span><?= t('мнение инженера')?></span>
                <div>
                    <h5><?= t('каркасный')?></h5>
                    <ul>
                        <li><?= t('безопасность')?></li>
                        <br>
                        <li><?= t('хорошая звукоизоляция')?></li>
                        <br>
                        <li><?= t('долговечность')?></li>
                        <br>
                        <li><?= t('надежность')?></li>
                        <br>
                        <li><?= t('выгодная комплектация')?></li>
                        <br>
                        <li><?= t('высокая скорость монтажа')?></li>
                        <br>
                    </ul>
                </div>
                <div>
                    <h5><?= t('сип-панельный')?></h5>
                    <ul>
                        <li><?= t('опасность для здоровья')?></li>
                        <br>
                        <li><?= t('плохая звукоизоляция')?></li>
                        <br>
                        <li><?= t('неизвестный срок службы')?></li>
                        <br>
                        <li><?= t('меньшая надежность')?></li>
                        <br>
                        <li><?= t('неполная комплектация')?></li>
                        <br>
                        <li><?= t('более долгий срок монтажа')?></li>
                        <br>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="id7" class="pozz">
    <div class="container six">
        <div class="col-xs-4 col-sm-2 col-sm-offset-3 col-md-2 col-md-offset-3 col-lg-2 col-lg-offset-3"><span>03</span>
        </div>
        <div class="col-xs-7 col-sm-4 col-md-3 col-lg-3">
            <h5><?= t('Чем <b>уникальна</b><br>наша компания?')?></h5>
        </div>
    </div>
    <div class="container seven">
        <div class=" col-sm-3 col-sm-offset-2 col-md-3 col-md-offset-2 col-lg-3 col-lg-offset-3">
            <div class="shadow2"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/image-02.jpg"></div>
        </div>
        <div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-0">
            <h6><?= t('просто, прозрачно, профессионально!')?></h6>
        </div>
        <div class="col-sm-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-0 qqq">
            <span><b><?= t('Для вашего дома европейского качества мы применяем лучшую современную технологию строительства')?></b></span>
            <p><?= t('Благодаря заводской сборке стен строительство происходит просто — чем проще строительство, тем оно качественнее.')?></p>
            <p><?= t('Строим ваш дом быстро – мы ценим и ваше время, и свое.')?> </p>
        </div>
        <div class="col-sm-3 col-md-2 col-md-offset-0 col-lg-2 col-lg-offset-0 text">
            <p><?= t('Делаем строительство прозрачным для вас — согласовываем с вами концепцию дома во всех деталях, вы понимаете, за что вы платите на каждом этапе.')?></p>
            <p><?= t('Строим ваш дом качественно – чтобы он радовал надежностью и комфортом долгие годы.')?></p>
        </div>
        <img loading="lazy" class="line4" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line4.png">
    </div>
    <div class="back2"></div>
</section>

<div id="why_choose" style="position: relative; top: -10px; height: 0;"></div>

<section id="id2">
    <div class="pozz1">
        <div class="container seven1">
            <img loading="lazy" class="line5" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line5.png">
            <div class="col-xs-9 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <h5><?= t('<b>почему выбирают</b></br>наши каркасные дома')?></h5>
            </div>
            <div class="col-xs-2 col-sm-2 col-sm-offset-2 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
                <span>04</span></div>
        </div>
        <div class="eight">
            <?php $i = 1; foreach (Advantage::model()->limit(9)->findAll() as $advantage): ?>
                <div data-index="<?php echo $i; ?>" class="eight_big">
                    <div class="shadow"><img loading="lazy" alt="<?php echo $advantage->title; ?>" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $advantage->getImageUrl('small'); ?>"></div>
                    <div>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <h4><?php echo $advantage->title; ?></h4>
                            <span><?php echo $advantage->slogan; ?></span>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <h4><?php echo $advantage->title_uk; ?></h4>
                            <span><?php echo $advantage->slogan_uk; ?></span>
                        <?php endif; ?>
                    </div>
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <p><?php echo $advantage->annotation; ?></p>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php echo $advantage->annotation_uk; ?>
                    <?php endif; ?>
                    <a href="#"><span><?= t('читать полностью')?></span><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></a>
                </div>
                <?php $i++; ?>
            <?php endforeach; ?>
        </div>
        <div class="back3"></div>
    </div>
</section>

<section id="id_house">
    <div class="container nine">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-9 col-lg-offset-2">
            <h3><?= t('посмотрите как проходит монтаж дома за 1 день')?></h3>
        </div>
    </div>
    <div class="container ten">
        <div class="col-xs-11 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2">
            <div class="questions">
                <span><?= t('Оставьте заявку и мы пригласим вас на просмотр')?></span>
            </div>
            <form id="form" class="form3 form" action="<?php echo url('landing/ajaxContact'); ?>" method="post">
                <input type="hidden" id="g-recaptcha-response2" name="g-recaptcha-response">
                <input type="hidden" name="action" value="validate_captcha">
                <input type="hidden" name="form" value="<?= t('Запись на просмотр')?>">
                <input class="valid reset" type="text" name="name" placeholder="<?= t('ИМЯ')?>">
                <input class="valid reset" type="number" name="phone" placeholder="<?= t('ТЕЛЕФОН')?>">
                <input class="reset" type="email" name="email" placeholder="E-MAIL">
                <button class="button"><?= t('Отправить заявку')?></button>
            </form>
        </div>
        <div class="col-xs-11 col-sm-6  col-md-5 col-lg-5">
            <div class="video" onclick="playPause()" style="cursor: pointer">
                <iframe loading="lazy" width="525" height="295" src="" data-src="https://www.youtube.com/embed/61prQcGcKN4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section id="id3" style="display: none;">
    <div class="pozz4">
        <div class="container eleven">
            <div class="col-xs-3 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-2 col-lg-2 col-lg-offset-2">
                <span>05</span></div>
            <div class="col-xs-8 col-sm-4 col-md-3 col-lg-3">
                <h5><?= t('<b>Этапы </b><br> строительсвта<br> дома')?></h5>
            </div>
        </div>
        <div class="container twelve">
            <img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line6.png">
            <div class="col-xs-3 col-sm-1 col-sm-offset-1 col-md-1 col-md-offset-2 col-lg-1 col-lg-offset-2">
                <div class="something">
                    <span class="active">01</span>
                    <span>02</span>
                    <span>03</span>
                    <span>04</span>
                    <span>05</span>
                </div>
            </div>
            <div class="col-xs-8 col-sm-6 col-md-6 col-lg-5">
                <img loading="lazy" class="image_768" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/house5.jpg">
            </div>
            <div class="col-xs-7 col-xs-offset-1 col-sm-3 col-sm-offset-0 col-md-3 col-md-offset-0 col-lg-2 col-lg-offset-0">
                <h6><?= t('Подготовка к строительству')?></h6>
                <ul>
                    <li><?= t('приобретение участка')?></li>
                    <li><?= t('подключение коммуникации (вода, электричество)')?></li>
                    <li><?= t('выбор проекта и подписание договора на строительство')?></li>
                    <li><?= t('подготовка проектной документации')?></li>
                    <li><?= t('геологические и геодезические работы')?></li>
                </ul>
            </div>
        </div>
        <div class="back4"></div>
    </div>
</section>

<section id="id6">
    <div class="container thirteen">
        <div class="col-xs-6 col-xs-offset-3  col-sm-3 col-sm-offset-5 col-md-3 col-md-offset-5 col-lg-3 col-lg-offset-5">
            <h5><?= t('<b>Сколько стоит </b><br> такой дом?')?></h5>
        </div>
        <div class="col-xs-2 col-sm-2 col-sm-offset-1 col-md-2 col-lg-2 change_num">
            <span>05</span>
        </div>
    </div>
    <div class="container thirteen_1">
        <div class="col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 col-lg-11 col-lg-offset-0">
            <span class="mmm visible-xs"><a href="#"><?= t('фильтр')?></a></span>
            <ul class="hidden-xs">
                <li>
                    <?= t('Сортировать по')?>:
                    <input id="sort-by" type="hidden" value="">
                </li>
                <li id="sort-by-price">
                    <div></div>
                    <?= t('Цене')?>
                </li>
                <li id="sort-by-area">
                    <div></div>
                    <?= t('Квадратуре дома')?>
                </li>
                <li><a href="" id="showOnNeed"><?= t('Смотреть все проекты')?></a></li>
                <li><a  id="discardFiltr"><span><?= t('Сбросить фильтр')?></span><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></a></li>
                <li><span><?= t('от')?></span>
                    <div>
                        <form class="input_f" onsubmit="return false;"><input id="filter-area-from" type="number" value=""></form>
                    </div>
                    <span><?= t('до')?></span>
                    <div>
                        <form class="input_f" onsubmit="return false;"><input id="filter-area-till" type="number" value=""></form>
                    </div>
                    <span><?= t('м')?><sup>2</sup></span></li>
            </ul>
        </div>
    </div>
    <div class="container thirteen1">
        <div id="frame" class="thir" style="overflow: hidden;">
            <div class="slidee">
                <?php $this->actionAjaxProjects(); ?>
            </div>
            <div>
                <div class="scrollbar">
                    <div class="handle"></div>
                    <div class="controls center">
                        <button class="prev"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrowblackl.png"></button>
                        <button class="next"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrowblackr.png"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="new_menu">
    <div class="new_menu_wrap">
        <img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/logo_small.png" alt="<?= t('логотип')?>">
        <ul>
            <li><a href="#"><span><?= t('Вопросы при выборе')?></span></a></li>
            <li><a href="#"><span><?= t('ваши выгоды')?></span></a></li>
            <li><a href="#"><span><?= t('этапы строительства')?></span></a></li>
            <li><a href="#"><span><?= t('как мы работаем')?></span></a></li>
            <li><a href="#"><span><?= t('отзывы')?></span></a></li>
            <li><a href="<?= Yii::app()->createUrl('portfolio/index'); ?>"><span><?= t('готовые дома')?></span></a></li>
            <li><a href="<?= Yii::app()->createUrl('project/index'); ?>"><span><?= t('лучшие проекты')?></span></a></li>
            <li>
                <a href="<?= Yii::app()->createUrl('article/view', array('id' => 1, 'uri' => 'montaj-doma')); ?>">
                    <span><?= t('технология')?></span>
                </a>
            </li>
            <li><a href="<?= Yii::app()->createUrl('homekit/index'); ?>"><span><?= t('стоимость')?></span></a></li>
            <li>
                <a href="<?= Yii::app()->createUrl('article/view', array('id' => 5, 'uri' => 'video-po-algoritmu')); ?>">
                    <span><?= t('алгоритм строительства')?></span>
                </a>
            </li>
            <li>
                <a href="<?= Yii::app()->createUrl('article/view', array('id' => 9, 'uri' => 'krovelnyie-konstruktsii')); ?>">
                    <span><?= t('модульный дом')?></span>
                </a>
            </li>
            <li><a href="<?= Yii::app()->createUrl('site/contact'); ?>"><span><?= t('о нас')?></span></a></li>
        </ul>
    </div>
</section>

<section id="id4" class="new_menu_appear">
    <img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line8.png">
    <div class="pozz5">
        <div class="container fourteen">
            <img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line7.png">
            <div class="col-xs-4 col-sm-3 col-sm-offset-1 col-md-2 col-md-offset-2 col-lg-2 col-lg-offset-2 fourteen-06">
                <span>06</span></div>
            <div class="col-xs-8 col-sm-5 col-md-4 col-lg-4">
                <h5><?= t('<b>8 причин </b>почему стоит <br> сотрудничать с нами')?></h5>
            </div>
        </div>
        <div class="container fourteen1 fourteen2">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-3">
                <div class="pozit">
                    <div class="visible-xs ooo"><img loading="lazy" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img loading="lazy" class="visible-xs show_item item1" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/glass.png" alt="<?= t('лупа')?>">
                    <h4><?= t('Финансовая прозрачность')?></h4>
                    <img loading="lazy" class="odd" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/one.png">
                    <p class="height_1"><?= t('Все суммы, предварительно согласованные с вами, фиксируются в договоре, смете и графике оплат.')?></p>
					<p class="height_1_fix hidden-sm"><?= t('В процессе строительства они не меняется.')?></p>
                </div>
            </div>
            <div class="col-sm-2  col-md-1 col-lg-1">
                <img class="hidden-xs show_item item1" src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/glass.png" alt="<?= t('лупа')?>">
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-1 col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-3">
                <div><img class="hidden-xs show_item item2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/doc1.png" alt="<?= t('лупа')?>"></div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 col-lg-5 col-lg-offset-0">
                <div class="pozit">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item2" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/doc1.png" alt="<?= t('лупа')?>">
                    <h4><?= t('Контроль строительства')?></h4>
                    <img class="even" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/two.png">
                    <p class="height_2"><?= t('Смета позволяет контролировать объемы работ и материалов.')?>
						<?= t('Каждый этап строительства вы принимаете по акту приема-передачи.')?></p>
                </div>
            </div>
        </div>
        <div class="back5"></div>
    </div>
    <div class="pozz7">
        <div class="container fourteen1">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-3">
                <div class="pozit plus1">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item3" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/puzzle.png" alt="<?= t('пазл')?>">
                    <h4><?= t('Удобство оплат')?></h4>
                    <img class="odd plus_smth" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/three.png">
                    <p class="height_3"><?= t('Вы оплачиваете строительства дома частями.')?>
						<?= t('Вам не нужно вносить всю сумму сразу.')?>
						<?= t('График платежей согласовываем вместе с вами, учитывая ваши пожелания.')?></p>
                </div>
            </div>
            <div class="col-sm-2 col-md-1 col-lg-1">
                <img class="hidden-xs show_item item3" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/puzzle.png" alt="<?= t('пазл')?>">
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-sm-1 col-sm-offset-2 col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-3">
                <div><img class="hidden-xs show_item item4" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/key.png" alt="<?= t('ключ')?>"></div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 col-lg-5 col-lg-offset-0">
                <div class="pozit">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item4" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/key.png" alt="<?= t('ключ')?>">
                    <h4><?= t('Любой обьем работ')?></h4>
                    <img class="even plus_snt" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/four.png">
                    <p class="height_4"><?= t('Вы заказываете объем работ, нужный именно вам: от изготовления домокомплекта до строительства дома под ключ.')?>
						<?= t('Мы готовы выполнить любые работы: от устройства скважины до ландшафтного дизайна.')?></p>
                </div>
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-3">
                <div class="pozit plus2">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item5" src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/hhouse.png" alt="<?= t('пазл')?>">
                    <h4><?= t('Эксклюзивные архитектурные решения')?></h4>
                    <img class="odd plus_smthin" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/five.png">
                    <p class="height_5"><?= t('Мы являемся эксклюзивным представителем известного архитектурного бюро Z500 в сфере каркасного домостроения.')?>
                        <?= t('При строительстве с нами любой проект Z500 вы получите в подарок.')?></p>
                </div>
            </div>
            <div class="col-sm-2 col-md-1 col-lg-1">
                <img class="hidden-xs show_item item5" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/hhouse.png" alt="<?= t('пазл')?>">
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-sm-1 col-sm-offset-2  col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-3">
                <div><img class="hidden-xs show_item item6" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/location.png" alt="<?= t('ключ')?>"></div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 col-lg-5 col-lg-offset-0">
                <div class="pozit plus3">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item6" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/location.png" alt="<?= t('ключ')?>">
                    <h4><?= t('Опыт строительства')?></h4>
                    <img class="even plus_gny" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/six.png">
                    <p class="otstup_text height_6"><?= t('Более 50 построенных домов')?><br>
						<?= t('Более 7 лет на рынке')?><br>
						<?= t('Около 10 домов могут возводятся одновременно Собственный архитектурный, конструкторский, инженерный отделы.')?></p>
                </div>
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-3">
                <div class="pozit plus4">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item7" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/clock.png" alt="<?= t('пазл')?>">
                    <h4><?= t('Короткие сроки строительства')?></h4>
                    <img class="odd plus_smthinf" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/seven.png">
                    <p class="height_7"><?= t('Немецкая каркасная технология позволяет построить дом под ключ за 3 месяца.')?>
                        <?= t('Благодаря отсутствию мокрых работ, строительство сборных домов можно проводить в любое время года, в том числе зимой.')?></p>
                </div>
            </div>
            <div class="col-sm-2  col-md-1  col-lg-1">
                <img class="hidden-xs show_item item7" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/clock.png" alt="<?= t('пазл')?>">
            </div>
        </div>
        <div class="container fourteen1">
            <div class="col-xs-10 col-xs-offset-1 col-sm-1 col-sm-offset-2 col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-3">
                <div><img class="hidden-xs show_item item8" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/doc.png" alt="<?= t('ключ')?>"></div>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 col-lg-5 col-lg-offset-0">
                <div class="pozit plus5">
                    <img class="lineu" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line9.png">
                    <div class="visible-xs ooo"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow_top.png"></div>
                    <img class="visible-xs show_item item8" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/doc.png" alt="<?= t('ключ')?>">
                    <h4><?= t('10 летняя гарантия на построеный дом')?></h4>
                    <img class="even eight" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/eight.png">
                    <p class="otstup_text2"><?= t('По завершении строительства вы получите письменную гарантию на домокомплект - 10 лет, прописанную в договоре.')?></p>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="id8">
    <div class="pozz6">
        <div class="container fifteen">
            <div class="col-xs-3 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-2 col-lg-2 col-lg-offset-2">
                <span>07</span></div>
            <div class="col-xs-6 col-sm-5 col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1">
                <h5><?= t('Этапы<br><b>сотрудничества</b><br>с нами')?></h5>
            </div>
        </div>
        <div class="container fifteen1">
            <div class="col-xs-12 col-sm-12  col-md-11 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <div class="video2">
                    <iframe loading="lazy" width="560" height="315" src="" data-src="https://www.youtube.com/embed/hmdc9Evcp-g?rel=0&amp;showinfo=0"
                            frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="container fifteen2">
            <div class="col-xs-12 col-sm-12 col-md-11 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <div class="block1">
                    <div class="yellow1">
                        <span>1</span>
                        <h6 class="hidden-xs"><?= t('выбор<br>проекта')?></h6>
                        <h6 class="visible-xs"><?= t('выбор проекта')?></h6>
                    </div>
                    <div class="picture shadow4">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/algorithm-1.jpg" alt="<?= t('договор')?>">
                    </div>
                    <p><?= t('Любой проект с нашего сайта или сайта нашего партнера Z500 вы получите в подарок!')?>
                        <?= t('Можем разработать и индивидуальный проект.')?></p>
                </div>
                <div class="block1">
                    <div class="yellow1">
                        <span>2</span>
                        <h6 class="hidden-xs"><?= t('обсуждение<br>условий')?></h6>
                        <h6 class="visible-xs"><?= t('обсуждение условий')?></h6>
                    </div>
                    <div class="picture shadow4">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/algorithm-2.jpg" alt="<?= t('договор')?>">
                    </div>
                    <p><?= t('Поможем определиться с комплектацией дома, учитывая ваши потребности и пожелания;')?>
                        <?= t('Сделаем предварительный просчет стоимости строительства.')?></p>
                </div>
                <div class="block1">
                    <div class="yellow1">
                        <span>3</span>
                        <h6 class="hidden-xs"><?= t('Подписание<br>договора')?></h6>
                        <h6 class="visible-xs"><?= t('Подписание договора')?></h6>
                    </div>
                    <div class="picture shadow4">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/algorithm-3.jpg" alt="<?= t('договор')?>">
                    </div>
                    <p><?= t('Договор содержит смету, календарный график работ, график оплат.')?>
                        <?= t('Предоплату вы вносите только за материалы, и частично за работы 1-го этапа строительных работ.')?>
                    </p>
                </div>
                <div class="block1">

                    <div class="yellow1">
                        <span>4</span>
                        <h6 class="hidden-xs"><?= t('строительтво<br>дома')?></h6>
                        <h6 class="visible-xs"><?= t('строительтво дома')?></h6>
                    </div>
                    <div class="picture shadow4">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/algorithm-4.jpg" alt="<?= t('договор')?>">
                    </div>
                    <p><?= t('Проводим строительные работы согласно договору.')?>
                        <?= t('Вы сами можете регулировать скорость строительства в зависимости от ваших финансовых возможностей.')?></p>
                </div>
                <div class="block1">
                    <div class="yellow1">
                        <span>5</span>
                        <h6 class="twolayers"><?= t('Завершение<br>строительства')?></h6>
                    </div>
                    <div class="picture shadow4">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/algorithm-5.jpg" alt="<?= t('договор')?>">
                    </div>
                    <p><?= t('Подписываем акты выполненных работ, вручаем гарантийный сертификат и ключи от вашего дома.')?></p>
                </div>
            </div>
        </div>
        <div class="container fifteen3">
            <div class="col-sm-7 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1">
                <div class="house10">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/house10.jpg" alt="<?= t('дом')?>">
                </div>
            </div>
            <div class="col-xs-11 col-sm-5 col-md-4  col-lg-4">
                <div class="questions">
                    <span><?= t('Приезжайте к нам<br> в демо-дом<br> на чашечку кофе :-)')?></span>
                </div>
                <form class="form_op form" action="<?php echo url('landing/ajaxContact') ?>" method="post">
                    <input type="hidden" id="g-recaptcha-response3" name="g-recaptcha-response">
                    <input type="hidden" name="action" value="validate_captcha">
                    <input type="hidden" name="form" value="<?= t('Запрос на поездку в демо-дом. Главная')?>">
                    <input type="text" class="valid reset" name="name" placeholder="<?= t('ИМЯ')?>">
                    <input type="number" class="valid reset" name="phone" placeholder="<?= t('ТЕЛЕФОН')?>">
                    <input class="reset" type="email" name="email" placeholder="E-MAIL">
                    <button class="button"><span><?= t('Отправить заявку')?></span><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></button>
                </form>
            </div>
        </div>
        <div class="back6"></div>
    </div>
</section>

<section id="id5">
    <div class="pozz7">
        <div class="back7"></div>
        <div class="container sixteen">
            <div class="col-xs-3 col-xs-offset-1 col-sm-1 col-sm-offset-2 col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-3 title-8">
                <span>08</span></div>
            <div class="col-xs-8 col-sm-4 col-md-3 col-lg-3">
                <h5><?= t('<b>отзывы</b><br>о нашей работе')?></h5>
            </div>
        </div>
        <div class="container sixteen1">
            <img class="lkj" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line11.png">
            <div class="second_slider">

                <?php foreach ($reviews as $review): ?>
                    <?php $this->renderPartial('_review', array('model' => $review)); ?>
                <?php endforeach; ?>

            </div>
        </div>
</section>

<section class="hidden-xs">
    <div class="container seventeen">
        <div class="col-sm-3 col-sm-offset-5 col-md-2 col-md-offset-6 col-lg-2 col-lg-offset-6">
            <h5><?= t('<b>отзывы</b><br>поставщиков')?></h5>
        </div>
        <div class="col-sm-1 col-lg-1">
            <span>09</span></div>
    </div>
    <div class="container seventeen1">
        <img class="img_left" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line.png">
        <div class="col-sm-10  col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="first_slider">

                <?php foreach (ReviewSupplier::model()->findAll() as $reviewSupplier): ?>
                <div class="feedback">
                    <div class="feedback_image">
                        <img alt="<?php echo $reviewSupplier->name; ?>" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $reviewSupplier->getImageUrl('small'); ?>">
                    </div>

                    <?php if (Yii::app()->language == 'ru'): ?>
                        <h4><?php echo $reviewSupplier->product ?></h4>
                        <h5><?php echo $reviewSupplier->name ?></h5>
                        <p>
                            <?php echo $reviewSupplier->content; ?>
                        </p>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <h4><?php echo $reviewSupplier->product_uk ?></h4>
                        <h5><?php echo $reviewSupplier->name_uk ?></h5>
                        <p>
                            <?php echo $reviewSupplier->content_uk; ?>
                        </p>
                    <?php endif; ?>

                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>

<section>
    <div class="pozz8">
        <div class="container eighteen hidden-xs">
            <div class="col-sm-1 col-sm-offset-2 col-md-1 col-md-offset-3 col-lg-1 col-lg-offset-2">
                <span class="hidden-xs">10</span></div>

        </div>
        <div class="container eighteen1">
            <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 give_margin">
                <div class="container visible-xs kotp">
                    <div class="col-xs-8 col-sm-3 col-sm-offset-5 col-md-2 col-md-offset-6 col-lg-2 col-lg-offset-6">
                        <h5><?= t('Контакты')?></h5>
                    </div>
                    <div class="col-xs-3 col-sm-1 col-lg-1">
                        <span class="hidden-xs">10</span><span class="visible-xs">09</span></div>
                </div>
                <h6 class="hidden-xs"><?= t('Контакты')?></h6>
                <div class="left">
                    <span class="text_m"><?= t('Вы можете нам написать,мы<br> как минимум поможем<br>советом')?></span>
                    <form class="form2 form" method="post" action="<?php echo url('landing/ajaxContact') ?>">
                        <input type="hidden" id="g-recaptcha-response4" name="g-recaptcha-response">
                        <input type="hidden" name="action" value="validate_captcha">
                        <input type="hidden" name="form" value="<?= t('Контакты')?>">
                        <input type="text" class="valid reset" name="name" placeholder="<?= t('ИМЯ')?>">
                        <input type="number" class="valid reset" name="phone" placeholder="<?= t('ТЕЛЕФОН')?>">
                        <input type="email" name="email" class="reset" placeholder="E-MAIL">
                        <textarea class="reset" name="comment" placeholder="<?= t('Наши дома отапливать дешевле, чем квартиру в несколько раз...')?>"></textarea>
                        <button class="button"><span><?= t('Задать вопрос')?></span><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></button>
                    </form>
                    <h5><?= t('Будьте на связи!')?></h5>
                    <p><?= t('Адрес')?>: <span class="desc_right"><?= config('contact_address') ?></span></p>
                    <p><?= t('Телефон')?>: <span class="desc_right"><a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a></span></p>
                    <p>Email: <span class="desc_right"><?php echo config('contact_email'); ?></span></p>
                    <p>Web: <span class="desc_right">profikarkas.com.ua</span></p>
                    <p>Skype: <span class="desc_right">profikarkas</span></p>
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-sm-5 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0 eighteen2">
                <h6><?= t('Последние статьи')?></h6>
                <a class="button blog hidden-xs" href="<?php echo url('usefularticle/index')?>">
                    <span> <?= t('перейти в блог')?></span>
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png">
                </a>
                <div class="eighteen_scroll" data-mcs-theme="dark">
                    <div class="direction">

                        <?php foreach (UsefulArticle::model()->limit(5)->findAll(array("condition"=>"status_main = 1")) as $usefulArticle): ?>

                            <div class="article">
                                <div class="date">
                                    <p><?php echo Yii::app()->dateFormatter->format('dd.MM.yyyy', $usefulArticle->create_time); ?></p>
                                    <div></div>
                                </div>

                                <?php if (Yii::app()->language == 'ru'): ?>
                                    <h5 class="visible-xs"><?php echo $usefulArticle->title; ?></h5>
                                    <div class="article_photo">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $usefulArticle->getImageUrl('small');?>">
                                    </div>
                                    <h5 class="hidden-xs"><?php echo $usefulArticle->title; ?></h5>
                                    <p class="cvbcb">
                                        <?php echo $usefulArticle->description ? explode("|", wordwrap(strip_tags($usefulArticle->description), 120, "|"))[0] . '...' : ''; ?>
                                    </p>
                                <?php elseif (Yii::app()->language == 'uk'): ?>
                                    <h5 class="visible-xs"><?php echo $usefulArticle->title_uk; ?></h5>
                                    <div class="article_photo">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $usefulArticle->getImageUrl('small');?>">
                                    </div>
                                    <h5 class="hidden-xs"><?php echo $usefulArticle->title_uk; ?></h5>
                                    <p class="cvbcb">
                                        <?php echo $usefulArticle->description_uk ? explode("|", wordwrap(strip_tags($usefulArticle->description_uk), 120, "|"))[0] . '...' : ''; ?>
                                    </p>
                                <?php endif; ?>


                                <a class="readmore" href="<?php echo $usefulArticle->getUrl();?>"><span><?= t('читать полностью')?></span><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></a>
                            </div>

                        <?php endforeach; ?>

                        <div class="article visible-xs artic">
                            <img class="imagebook" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/book.png">
                            <a class="readmore" href="<?php echo url('usefularticle/index'); ?>"><span><?= t('перейти в блог')?></span><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/arrow.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="back8"></div>
    </div>
</section>


<section class="hidden-xs">
    <div class="container eighteen3">
        <div class="col-sm-2 col-sm-offset-2 col-md-2 col-md-offset-2 col-lg-2 col-lg-offset-2 eighteen4">
            <div class="director">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/director.jpg">
            </div>
            <h5><?= t('Николай Кудиненко')?></h5>
            <h6><?= t('Директор компании')?></h6>
        </div>
        <div class="col-sm-7 col-sm-offset-0 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
            <h3><?= t('слово директора')?></h3>
            <p><?= t('Все должно выполняться просто, осмысленно и эффективно.')?>
                <?= t('Немецкая технология строительства каркасных домов идеально отражает эту философию, поэтому воплотить ее в жизнь здесь в Украине так ценно и важно для меня.')?>
				<br><br>
                <?= t('Благодаря заводской сборке каркасных стен, современному проектированию, строительство происходит максимально просто, быстро и качественно.')?>
                <?= t('Дом получается энергоэффективным, комфортным, надежным и долговечным.')?>
				<br><br>
                <?= t('Сам строительный процесс мы делаем для вас прозрачным и понятным: вы знаете, за что вы платите, выбираете комплектацию дома с учетом желаний и возможностей.')?>
				<br><br>
				<b style="color:#000"><?= t('Профикаркас — это комфорт превыше ожиданий.')?></b>
                <?= t('И ваши улыбки в новом доме — лучшая награда за наш труд!')?>
			</p>
        </div>
    </div>
</section>

<footer>
    <div class="footer_inner">
        <div class="footer_wrap">
            <img class="pre" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/line12.png">
            <ul>
                <li>
                    <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                        <a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a>
                    <?php endforeach;?>
                </li>
                <li><?php echo strtoupper(config('contact_email')); ?></li>
                <br class="visible-xs">
                <li class="social_item"><a href="https://www.facebook.com/Profikarkas?ref=hl" class="social_link fb"></a></li>
                <li class="social_item"><a href="https://www.instagram.com/profikarkas_ua/" class="social_link inst"></a></li>
                <li class="social_item"><a href="https://www.youtube.com/channel/UCCI9ZJ_tUFptZFpX-JBnoOA" class="social_link yt"></a></li>
            </ul>
            <div class="social">

            </div>
        </div>
    </div>

    <div class="container container_footer">
        <div class="footer__data">
            <div class="footer__data-item">
                <div class="footer__data-title">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Реквизиты
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Реквізити
                    <?php endif; ?>
                </div>
                <div class="footer__data-text">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Общество с ограниченной ответственностью «ПРОФІБУДКОМПЛЕКТ» // Юридический адрес: 02000,г. Киев,
                        пр-т. Григоренка, 38-А, кв. 157 // ЕГРПОУ 38657557 // Р/р UA343510050000026001610250000 //
                        Банк: ПАО "УкрСиббанк" г. Харьков // МФО 351005
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Товариство з обмеженою відповідальністю «ПРОФІБУДКОМПЛЕКТ» // Юридична адреса: 02000, м. Київ,
                        пр-т. Григоренка, 38-А, кв. 157 // ЄДРПОУ 38657557 // Р/р UA343510050000026001610250000 //
                        Банк: ПАТ "УкрСиббанк" м. Харків // МФО 351005
                    <?php endif; ?>
                </div>
            </div>
            <div class="footer__data-item">
                <div class="footer__data-title">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Наши партнеры
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Наші партнери
                    <?php endif; ?>
                </div>
                <div class="footer__data-gallery">
                    <div class="footer__data-gallery-inner" id="partnerGallery">
                        <div class="footer__data-gallery-item">
                            <a href="https://zs6.com.ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-zelena-skelia.png"
                                     alt="Zelena Skelia logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://pragma.ua/ru/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-pragma.png"
                                     alt="Pragma logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="http://aqua-basis.com.ua/ru/home/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-aqua-basis.png"
                                     alt="Aqua Basis logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.kantal.com.ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-kantal.jpg"
                                     alt="Kantal logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.hormann.ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-hormann.png"
                                     alt="Hormann logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.schiedel.com/ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-schiedel.png"
                                     alt="Schiedel logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.isover.ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-isover.png"
                                     alt="Isover logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.korsa.ua/ " class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-korsa.png"
                                     alt="Korsa logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                        <div class="footer__data-gallery-item">
                            <a href="https://www.velux.ua/" class="footer__data-link" target="_blank">
                                <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-velux.png"
                                     alt="Velux logo"
                                     class="footer__data-img">
                            </a>
                        </div>
                    </div>
                    <div class="footer__data-arrow footer__data-arrow_prev"></div>
                    <div class="footer__data-arrow footer__data-arrow_next"></div>
                </div>
            </div>
        </div>
        <div class="footer_menu">
            <?php $this->renderPartial('_footer_menu'); ?>
        </div>
        <p class="footer_copyright">Copyright © <?php echo date('Y') ?> ProfiKarkas All Rights Reserved.</p>
    </div>
</footer>
<div class="overflow_lb">
    <div class="lb_content">
        <?php $i = 1; foreach (Advantage::model()->limit(9)->findAll() as $advantage): ?>
            <div data-index="<?php echo $i; ?>" class="lb">
                <div class="text_wrap">
                    <div class="text_header">
                        <span class="num"><?php echo $i . '/' . count(Advantage::model()->limit(9)->findAll()); ?></span>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <span class="title"><?php echo $advantage->title; ?></span>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <span class="title"><?php echo $advantage->title_uk; ?></span>
                        <?php endif; ?>

                    </div>
                    <div class="text_content">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php echo $advantage->content; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php echo $advantage->content_uk; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>

        <div class="close_lb"></div>
        <div class="nav_lb">
            <div class="lb_prev"></div>
            <div class="lb_next"></div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmW_-RuGD1yj9q698TbsscijaCcXibBVU" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ" async defer></script>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/homepage/js/scripts-b.min.js" async defer></script>

<script>

    function init() {
        var lazyElements = document.querySelectorAll("iframe[data-src], img[data-src]");
        for (var i = 0; i < lazyElements.length; i++) {
            if(lazyElements[i].getAttribute('data-src')) {
                lazyElements[i].setAttribute('src', lazyElements[i].getAttribute('data-src'));
            }
        }

        grecaptcha.ready(function() {
            grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
                document.getElementById('g-recaptcha-response').value=token;
                document.getElementById('g-recaptcha-response2').value=token;
                document.getElementById('g-recaptcha-response3').value=token;
                document.getElementById('g-recaptcha-response4').value=token;
            });
        });
    }
    document.addEventListener('DOMContentLoaded', function () {
        window.setTimeout('init()', 6000);
    });
    function initMap() {
        var styles = [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 65
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": "50"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffde17 "
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "30"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "40"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffff00 "
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -97
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            }
        ];

        var styledMap = new google.maps.StyledMapType(styles, {
            name: 'Styled Map'
        });

        var mapOptions = {
            zoom: 14,
            mapTypeControl: false,
            center: new google.maps.LatLng(50.428542, 30.531771),
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        var companyImage = new google.maps.MarkerImage('images/locat.png',
            new google.maps.Size(42,57),
            new google.maps.Point(0,0),
            new google.maps.Point(42,57)
        );

        var companyPos = new google.maps.LatLng(50.428528, 30.531782);
        var companyMarker = new google.maps.Marker({
            position: companyPos,
            map: map,
            icon: companyImage,
            zIndex: 3
        });
    }
    window.setTimeout('initMap()', 6000);
</script>

</body>
</html>
