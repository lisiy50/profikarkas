<?php
/* @var $baseInformation Landing */
/* @var $textInformation LandingText */
?>
<section class="banner">
    <div class="container container--banner">
        <div class="decor decor--banner"></div>
    </div>
    <div class="container container--content">
        <div class="decor decor--stage-left"></div>
    </div>
    <div class="container container--banner">
        <div class="banner__inner">
            <picture class="banner__pic">
                <source media="(max-width: 414px)" srcset="<?= Yii::app()->theme->baseUrl ?>/img/hotels/banner-main-2-xs.jpg">
                <source media="(max-width: 767px)" srcset="<?= Yii::app()->theme->baseUrl ?>/img/hotels/banner-main-2-sm.jpg">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/banner-main-2.jpg" alt="" class="banner__img">
            </picture>
            <div class="banner__content">
                <h1 class="banner__title">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[2]['text_ru'])): ?>
                            <?= $textInformation[2]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[2]['text_ua'])): ?>
                            <?= $textInformation[2]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </h1>
                <p class="banner__slogan">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[1]['text_ru'])): ?>
                            <?= $textInformation[1]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[1]['text_ua'])): ?>
                            <?= $textInformation[1]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="container container--content">
        <div class="decor decor--slogan"></div>
    </div>
</section>

<section class="stage">
    <div class="container container--content">
        <div class="decor decor--stage"></div>
        <div class="stage__inner">
            <h2 class="stage__title">
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php if (strip_tags($textInformation[3]['text_ru'])): ?>
                        <?= $textInformation[3]['text_ru'] ?>
                    <?php endif; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php if (strip_tags($textInformation[3]['text_ua'])): ?>
                        <?= $textInformation[3]['text_ua'] ?>
                    <?php endif; ?>
                <?php endif; ?>
            </h2>
            <p class="stage__slogan">
                <span class="stage__slogan-inner">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[4]['text_ru'])): ?>
                            <?= $textInformation[4]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[4]['text_ua'])): ?>
                            <?= $textInformation[4]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
            </p>
            <section class="stage__content">
                <div class="stage__content-top">
                    <div class="stage__content-list-wrap">
                        <p class="stage__content-list-title">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                <?php if (strip_tags($textInformation[5]['text_ru'])): ?>
                                    <?= $textInformation[5]['text_ru'] ?>
                                <?php endif; ?>
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                <?php if (strip_tags($textInformation[5]['text_ua'])): ?>
                                    <?= $textInformation[5]['text_ua'] ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </p>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php if (strip_tags($textInformation[6]['text_ru'])): ?>
                                <?= $textInformation[6]['text_ru'] ?>
                            <?php endif; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php if (strip_tags($textInformation[6]['text_ua'])): ?>
                                <?= $textInformation[6]['text_ua'] ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="stage__content-gallery">
                        <div class="stage__content-gallery-inner" id="stageGallery">
                            <div class="stage__content-gallery-item">
                                <div class="stage__content-gallery-video">
                                    <div class="stage__content-gallery-video-inner">
                                        <iframe width="1280"
                                                height="720"
                                                src="" data-src="https://www.youtube.com/embed/61prQcGcKN4?modestbranding=1&autohide=1&showinfo=0&controls=0&rel=0"
                                                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="stage__content-gallery-item">
                                <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/stage-1.jpg" alt="" class="stage__content-gallery-img">
                            </div>
                            <div class="stage__content-gallery-item">
                                <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/stage-2.jpg" alt="" class="stage__content-gallery-img">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="stage__content-bottom">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[7]['text_ru'])): ?>
                            <?= $textInformation[7]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[7]['text_ua'])): ?>
                            <?= $textInformation[7]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="stage__content-thumbs-wrap">
                        <p class="stage__content-thumbs-title">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                <?php if (strip_tags($textInformation[8]['text_ru'])): ?>
                                    <?= $textInformation[8]['text_ru'] ?>
                                <?php endif; ?>
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                <?php if (strip_tags($textInformation[8]['text_ua'])): ?>
                                    <?= $textInformation[8]['text_ua'] ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </p>
                        <div class="stage__content-thumbs">
                            <div class="stage__content-thumbs-inner" id="stageThumbs">
                                <div class="stage__content-thumbs-item">
                                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/stage-th-0.jpg" alt="" class="stage__content-thumbs-img">
                                </div>
                                <div class="stage__content-thumbs-item">
                                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/stage-th-1.jpg" alt="" class="stage__content-thumbs-img">
                                </div>
                                <div class="stage__content-thumbs-item">
                                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/stage-th-2.jpg" alt="" class="stage__content-thumbs-img">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p style="margin-top: .125rem; margin-bottom: 0; line-height: 1.11111111;">
                    <small>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php if (strip_tags($textInformation[18]['text_ru'])): ?>
                                <?= $textInformation[18]['text_ru'] ?>
                            <?php endif; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php if (strip_tags($textInformation[18]['text_ua'])): ?>
                                <?= $textInformation[18]['text_ua'] ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </small>
                </p>
            </section>
        </div>
    </div>
</section>

<section class="standart">
    <div class="container container--content">
        <div class="standart__inner">
            <p class="standart__slogan">
                <span class="standart__slogan-inner">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[9]['text_ru'])): ?>
                            <?= $textInformation[9]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[9]['text_ua'])): ?>
                            <?= $textInformation[9]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
            </p>
            <h2 class="standart__title">
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php if (strip_tags($textInformation[10]['text_ru'])): ?>
                        <?= $textInformation[10]['text_ru'] ?>
                    <?php endif; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php if (strip_tags($textInformation[10]['text_ua'])): ?>
                        <?= $textInformation[10]['text_ua'] ?>
                    <?php endif; ?>
                <?php endif; ?>
            </h2>
            <div class="standart__content">
                <picture class="standart__content-pic">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/image-standart.jpg" alt="" class="standart__content-img">
                </picture>
                <ul class="standart__content-list">
                    <li class="standart__content-list-item">
                        <span class="standart__content-list-icon">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/standart-list-1.svg" alt="">
                        </span>
                        <span class="standart__content-list-text">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Профессиональное проектирование:<br>
                                с применением европейских правил энергоэффективности, соответствие украинским ДБН
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Професійне проєктування:<br>
                                із застосуванням європейських правил енергоефективності, відповідність українським ДБН
                            <?php endif; ?>
                        </span>
                    </li>
                    <li class="standart__content-list-item">
                        <span class="standart__content-list-icon">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/standart-list-2.svg" alt="">
                        </span>
                        <span class="standart__content-list-text">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Материалы проверенных производителей: соответствие стандартов ISO, EC
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Матеріали перевірених виробників: відповідність стандартів ISO, EC
                            <?php endif; ?>
                        </span>
                    </li>
                    <li class="standart__content-list-item">
                        <span class="standart__content-list-icon">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/standart-list-3.svg" alt="">
                        </span>
                        <span class="standart__content-list-text">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Профильные специалисты<br> для каждого этапа работ. Авторский надзор
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Профільні спеціалісти<br> для кожного етапу робіт. Авторський нагляд
                            <?php endif; ?>
                        </span>
                    </li>
                    <li class="standart__content-list-item">
                        <span class="standart__content-list-icon">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/standart-list-4.svg" alt="">
                        </span>
                        <span class="standart__content-list-text">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Опыт строительства<br> в Украине и Европе более 10 лет
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Досвід будівництва<br> в Україні та Європі більше 10 років
                            <?php endif; ?>
                        </span>
                    </li>
                    <li class="standart__content-list-item">
                        <span class="standart__content-list-icon">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/standart-list-5.svg" alt="">
                        </span>
                        <span class="standart__content-list-text">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Гарантия на построенное здание 10 лет
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Гарантія на побудований будинок 10 років
                            <?php endif; ?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="energy">
    <div class="container container--content">
        <div class="energy__header">
            <h2 class="energy__title">
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php if (strip_tags($textInformation[11]['text_ru'])): ?>
                        <?= $textInformation[11]['text_ru'] ?>
                    <?php endif; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php if (strip_tags($textInformation[11]['text_ua'])): ?>
                        <?= $textInformation[11]['text_ua'] ?>
                    <?php endif; ?>
                <?php endif; ?>
            </h2>
            <p class="energy__slogan">
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php if (strip_tags($textInformation[12]['text_ru'])): ?>
                        <?= $textInformation[12]['text_ru'] ?>
                    <?php endif; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php if (strip_tags($textInformation[12]['text_ua'])): ?>
                        <?= $textInformation[12]['text_ua'] ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </div>
        <div class="decor decor--energy"></div>
        <div class="energy__inner">
            <p class="energy__inner-title">
                <?php if (Yii::app()->language == 'ru'): ?>
                    <?php if (strip_tags($textInformation[13]['text_ru'])): ?>
                        <?= $textInformation[13]['text_ru'] ?>
                    <?php endif; ?>
                <?php elseif (Yii::app()->language == 'uk'): ?>
                    <?php if (strip_tags($textInformation[13]['text_ua'])): ?>
                        <?= $textInformation[13]['text_ua'] ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php if (strip_tags($textInformation[14]['text_ru'])): ?>
                    <?= $textInformation[14]['text_ru'] ?>
                <?php endif; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php if (strip_tags($textInformation[14]['text_ua'])): ?>
                    <?= $textInformation[14]['text_ua'] ?>
                <?php endif; ?>
            <?php endif; ?>
            <p class="energy__inner-notice">
                <span class="energy__inner-notice-text">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[15]['text_ru'])): ?>
                            <?= $textInformation[15]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[15]['text_ua'])): ?>
                            <?= $textInformation[15]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </span>
            </p>
            <div class="energy__content">
                <p class="energy__content-descr">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[16]['text_ru'])): ?>
                            <?= $textInformation[16]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[16]['text_ua'])): ?>
                            <?= $textInformation[16]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </p>
                <p class="energy__content-notice">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php if (strip_tags($textInformation[17]['text_ru'])): ?>
                            <?= $textInformation[17]['text_ru'] ?>
                        <?php endif; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php if (strip_tags($textInformation[17]['text_ua'])): ?>
                            <?= $textInformation[17]['text_ua'] ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </p>
            </div>
            <p style="margin-top: .25rem;">
                <small>
                    <em>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            * - Данный расчет является приблизительным. Точный можно сделать на основании проекта дома, региона строительства.
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            * - Цей розрахунок є приблизним. Точний можна зробити на основі проєкту будинку, регіону будівництва.
                        <?php endif; ?>
                    </em>
                </small>
            </p>
        </div>
    </div>
</section>

<section class="hotel">
    <div class="container container--content">
        <div class="hotel__inner">
            <h2 class="hotel__title">
                <span class="hotel__title-inner"><?= t('Пример проекта гостиницы') ?></span>
            </h2>
            <div class="hotel__content">
                <div class="hotel__content-pic">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/hotel-3.jpg"
                         alt="Пример проекта гостиницы"
                         class="hotel__content-img">
                    <div class="hotel__content-pic-inner">
                        <div class="hotel__content-price">
                            <?= t('Стоимость') ?>
                            <span>3 500 000* ₴</span>
                        </div>
                        <div class="hotel__content-term">
                            <?= t('Срок готовности') ?>
                            <span>3 <?= t('месяца') ?></span>
                        </div>
                    </div>
                </div>
                <div class="hotel__content-area">
                    <span class="hotel__content-area-inner">s: <strong>257 м<sup>2</sup></strong></span>
                </div>
                <div class="hotel__content-plan">
                    <a class="hotel__content-plan-pic" href="<?= Yii::app()->theme->baseUrl ?>/pdf/plan-1.pdf" title="Планировка 1-й этаж" target="_blank">
                        <picture>
                            <source media="(max-width: 767px)" srcset="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-1-sm.jpg">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-1.jpg" alt="" class="hotel__content-plan-img">
                        </picture>
                    </a>
                    <a class="hotel__content-plan-pic" href="<?= Yii::app()->theme->baseUrl ?>/pdf/plan-1.pdf" title="Планировка 2-й этаж" target="_blank">
                        <picture>
                            <source media="(max-width: 767px)" srcset="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-2-sm.jpg">
                            <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-2.jpg" alt="" class="hotel__content-plan-img">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="hotel__subcontent">
                <div class="hotel__subcontent-inner">
                    <p class="hotel__subcontent-title"><?= t('стоимость включает в себя:') ?></p>
                    <ul class="hotel__subcontent-list">
                        <li class="hotel__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Производство конструкций здания на заводе (полностью готовые стеновые панели с окнами,
                                черновым фасадом, черновой внутренней отделкой, перекрытия, кровельные фермы)
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Виробництво конструкцій будівлі на заводі (повністю готові стінові панелі з вікнами,
                                чорновим фасадом, чорновим внутрішнім оздобленням, перекриття, покрівельні ферми)
                            <?php endif; ?>
                        </li>
                        <li class="hotel__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Монтаж домокомплекта
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Монтаж домокомплекту
                            <?php endif; ?>
                        </li>
                        <li class="hotel__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Кровельные работы
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Покрівельні роботи
                            <?php endif; ?>
                        </li>
                        <li class="hotel__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Фасадные работы
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Фасадні роботи
                            <?php endif; ?>
                        </li>
                    </ul>
                    <p style="margin-top: -.5rem;"><em>* -
                            <?php if (Yii::app()->language == 'ru'): ?>
                                согласно смете от
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                відповідно кошторису від
                            <?php endif; ?>
                            15.07.2019</em></p>
                    <div class="hotel__subcontent-btn-wrap">
                        <a href="" class="hotel__subcontent-btn" style="display: none;"><?= t('Больше деталей') ?></a>
                        <span class="hotel__subcontent-area">s: <strong>257 м<sup>2</sup></strong></span>
                    </div>

                </div>
                <div class="hotel__subcontent-video">
                    <div class="hotel__subcontent-video-inner">
                        <iframe width="1237"
                                height="696" src="" data-src="https://www.youtube.com/embed/tKI-hG3b6V0?modestbranding=1&autohide=1&showinfo=0&controls=0&rel=0"
                                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
                <div class="hotel__subcontent-link-wrap" style="display: none;">
                    <a href="" class="hotel__subcontent-link"><?= t('Больше деталей') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="modular">
    <div class="container container--content">
        <div class="modular__inner">
            <h2 class="modular__title">
                <span class="modular__title-inner">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Базы отдыха на основе модульных домов
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Бази відпочинку на основі модульних будинків
                    <?php endif; ?>
                </span>
            </h2>
            <div class="modular__content">
                <div class="modular__content-pic">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/modular-1.jpg" alt="" class="modular__content-img">
                    <div class="modular__content-pic-inner">
                        <div class="modular__content-price">
                            <?= t('Стоимость') ?>
                            <span>350 000 ₴</span>
                        </div>
                        <div class="modular__content-term">
                            <?= t('Срок готовности') ?>
                            <span>1 <?= t('месяц') ?></span>
                        </div>
                    </div>
                    <?php if (false): ?>
                    <a class="modular__content-plan" href="<?= Yii::app()->theme->baseUrl ?>/pdf/plan-2.pdf" title="Планировка земельного участка" target="_blank">
                        <img src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-3.jpg" alt="Планировка земельного участка" class="modular__content-plan-img">
                    </a>
                    <?php endif; ?>
                </div>
                <div class="modular__content-area">
                    <span class="modular__content-area-inner">s: <strong>257 м<sup>2</sup></strong></span>
                </div>
            </div>
            <div class="modular__subcontent">
                <?php if (false): ?>
                <a class="modular__subcontent-plan modular__subcontent-plan--sm" target="_blank" title="Планировка земельного участка" href="<?= Yii::app()->theme->baseUrl ?>/pdf/plan-2.pdf">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-3-sm.jpg" alt="Планировка" class="modular__subcontent-plan-img">
                </a>
                <?php endif; ?>

                <a class="modular__subcontent-plan" target="_blank" title="Планировка" href="<?= Yii::app()->theme->baseUrl ?>/pdf/plan-2.pdf">
                    <picture>
                        <source media="(max-width: 767px)" srcset="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-3-sm.jpg">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/plan-thumb-3.jpg" alt="Планировка" class="modular__subcontent-plan-img">
                    </picture>
                </a>
                <div class="modular__subcontent-inner">
                    <p class="modular__subcontent-title"><?= t('Стоимость включает в себя:') ?></p>
                    <ul class="modular__subcontent-list">
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Модульный дом
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Модульний будинок
                            <?php endif; ?>
                        </li>
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Произведенный и собранный на заводе
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Вироблений та зібраний на заводі
                            <?php endif; ?>
                        </li>
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Включая кровлю
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Включаючи покрівлю
                            <?php endif; ?>
                        </li>
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Внешнюю и внутреннюю отделку под ключ
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Зовнішнє та внутрішнє оздоблення під ключ
                            <?php endif; ?>
                        </li>
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Разведенная электрика с розетками и выключателями
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Розведена електрика з розетками та вимикачами
                            <?php endif; ?>
                        </li>
                        <li class="modular__subcontent-list-item">
                            <?php if (Yii::app()->language == 'ru'): ?>
                                Оборудованный санузел (бойлером, душевой кабиной, унитазом)
                            <?php elseif (Yii::app()->language == 'uk'): ?>
                                Обладнаний санвузол (бойлер, душова кабіна, унітаз)
                            <?php endif; ?>
                        </li>
                    </ul>
                    <div class="modular__subcontent-btn-wrap">
                        <a href="https://profikarkas.com.ua/module" class="modular__subcontent-btn"><?= t('Больше деталей') ?></a>
                        <span class="modular__subcontent-area">s: <strong>29,4 м<sup>2</sup></strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="request">
    <div class="container container--content">
        <div class="request__wrap" id="formContainer"></div>
    </div>
</section>

<section class="services">
    <div class="container container--content">
        <div class="services__inner">
            <h2 class="services__title"><span class="services__title-inner">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Мы выполняем
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Ми виконуємо
                    <?php endif; ?>
                </span></h2>
            <div class="services__content">
                <picture class="services__content-pic">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/image-services.png" alt="" class="services__content-img">
                </picture>
                <ul class="services__content-list">
                    <li class="services__content-list-item">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Проектирование
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Проєктування
                        <?php endif; ?>
                    </li>
                    <li class="services__content-list-item">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Достроительная подготовка территории - геология, геодезия, другие работы
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Добудівельна підготовка території – геологія, геодезія, інші роботи
                        <?php endif; ?>
                    </li>
                    <li class="services__content-list-item">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Строительство гостиниц, баз отдыха
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Будівництво готелів, баз відпочинку
                        <?php endif; ?>
                    </li>
                    <li class="services__content-list-item">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Ввод в эксплуатацию
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Уведення в експлуатацію
                        <?php endif; ?>
                    </li>
                    <li class="services__content-list-item">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Ландшафтный дизайн
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Ландшафтний дизайн
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="excellence">
    <div class="container container--content">
        <div class="decor decor--excellence"></div>
        <div class="excellence__header">
            <h2 class="excellence__title">
                <span class="excellence__title-inner">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Преимущества работы с нами
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Переваги роботи з нами
                    <?php endif; ?>
                </span>
            </h2>
        </div>
        <div class="excellence__content excellence__content--top">
            <div class="excellence__content-inner">
                <div class="excellence__item">
                    <div class="excellence__item-pic excellence__item-pic--shadow">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-1.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Финансовая<br> прозрачность
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Фінансова<br> прозорість
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Все суммы, предварительно согласованные с вами, фиксируются в договоре, смете и графике оплат
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Усі суми, попередньо узгоджені з вами, фіксуються в договорі, кошторисі та графіку оплат
                        <?php endif; ?>
                    </p>
                </div>
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-2.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Контроль<br> качества
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Контроль<br> якості
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Авторский надзор на ключевых этапах. Каждый этап строительства вы принимаете по акту приема-передачи
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Авторський нагляд на ключових етапах. Кожен етап будівництва ви приймаєте за актом приймання-передачі
                        <?php endif; ?>
                    </p>
                </div>
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-3.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Удобный график<br> оплат
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Зручний графік<br> оплат
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            График платежей согласовываем вместе с вами, учитывая ваши пожелания
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Графік платежів погоджуємо разом з вами з огляду на ваші побажання
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="excellence__content excellence__content--medium">
            <div class="excellence__content-inner">
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-4.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Вы заказываете объем работ, нужный именно вам
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Ви замовляєте обсяг робіт, потрібний саме вам
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            От изготовления домокомплекта до строительства дома под ключ. Мы готовы выполнить любые работы:
                            от устройства скважины до ландшафтного дизайна для Киева и Киевской области. Объемы работ для
                            каждого региона оговариваются индивидуально.
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Від виготовлення домокомплекту до будівництва будинку під ключ. Ми готові виконати будь-які роботи:
                            від влаштування свердловини до ландшафтного дизайну для Києва та Київської області. Обсяги
                            робіт кожного регіону обговорюються індивідуально.
                        <?php endif; ?>
                    </p>
                </div>
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-5.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Работа на прямую с производителем
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Робота напряму з виробником
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Собственное производство для каркасных зданий фабричной сборки, собственный проектный отдел -
                            это гарантия сроков строительства, гарантия качества.
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Власне виробництво каркасних будівель фабричного збирання, власний проєктний відділ –
                            це гарантія термінів будівництва, гарантія якості.
                        <?php endif; ?>
                    </p>
                </div>
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-6.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Гарантия на построенные здания
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Гарантія на збудовані будівлі
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            По завершении строительства вы получите письменную гарантию на домокомплект - 10 лет,
                            прописанную в договоре.
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Після завершення будівництва ви отримаєте письмову гарантію на домокомплект – 10 років,
                            прописану у договорі.
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="excellence__content excellence__content--bottom">
            <div class="excellence__content-inner">
                <div class="excellence__item">
                    <div class="excellence__item-pic">
                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/excellence-icon-7.svg" alt="" class="excellence__item-img">
                    </div>
                    <p class="excellence__item-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Выделенный проектный менеджер
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Виділений проєктний менеджер
                        <?php endif; ?>
                    </p>
                    <p class="excellence__item-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Гарантия быстрого решения любого вопроса во время строительства. Понимание ваших потребностей,
                            подбор оптимальных решений для вас. - это экономия вашего времени и финансов.
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Гарантія швидкого вирішення будь-якого питання під час будівництва. Розуміння ваших потреб,
                            вибір оптимальних рішень для вас - це економія вашого часу та фінансів.
                        <?php endif; ?>
                    </p>
                </div>
                <div class="excellence__descr">
                    <div class="excellence__descr-top">
                        <div class="excellence__descr-top-inner">
                            <span>
                                <?php if (Yii::app()->language == 'ru'): ?>
                                    Почему лучше с нами
                                <?php elseif (Yii::app()->language == 'uk'): ?>
                                    Чому краще з нами
                                <?php endif; ?>
                            </span>
                        </div>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Подбор оптимальных решений исходя из ваших реалий
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Підбір оптимальних рішень виходячи з ваших реалій
                        <?php endif; ?>
                        <br>
                        <span>=</span>
                    </div>
                    <div class="excellence__descr-bottom">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Экономия вашего времени
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Економія вашого часу
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="excellence__footer">
            <div class="excellence__slogan">
                <div class="excellence__slogan-text">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        Вы захотели,<br>
                        <span>мы построили</span>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        Ви захотіли,<br>
                        <span>ми побудували</span>
                    <?php endif; ?>
                </div>
                <div class="excellence__slogan-icon">
                    <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/icon-quote.svg" alt="" class="excellence__slogan-img">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contacts">
    <div class="container container--content">
        <div class="decor decor--contacts"></div>
        <div class="contacts__wrap">
            <div class="contacts__inner">
                <div class="contacts__form">
                    <p class="contacts__form-title"><?= t('Контакты') ?></p>
                    <p class="contacts__form-notice">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Вы можете нам написать,<br> мы как минимум поможем советом
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Ви можете нам написати,<br> ми як мінімум допоможемо порадою
                        <?php endif; ?>
                    </p>
                    <div class="contacts__form-inner">
                        <form class="form2 form" method="post" action="<?= url('landing/hotelsContact') ?>">
                            <input type="hidden" id="g-recaptcha-response3" name="g-recaptcha-response">
                            <input type="hidden" name="action" value="validate_captcha">
                            <input type="hidden" name="form" value="Страница коммерческой недвижимости. Форма контактов.">
                            <div class="contacts__form-group">
                                <input type="text" class="valid reset contacts__form-control" name="name" placeholder="<?= t('ИМЯ') ?>">
                            </div>
                            <div class="contacts__form-group">
                                <input type="number" class="valid reset contacts__form-control" name="phone" placeholder="<?= t('ТЕЛЕФОН') ?>">
                            </div>
                            <div class="contacts__form-group">
                                <input type="email" name="email" class="reset contacts__form-control" placeholder="E-MAIL">
                            </div>
                            <div class="contacts__form-group">
                                <textarea class="reset contacts__form-control contacts__form-control--textarea"
                                          name="comment"
                                          placeholder="<?= t('Наши дома отапливать дешевле, чем квартиру в несколько раз...') ?>"></textarea>
                            </div>
                            <div class="contacts__form-btn-wrap">
                                <button class="button contacts__form-btn">
                                    <span class="button contacts__form-btn-text"><?= t('Задать вопрос') ?></span>
                                    <span class="button contacts__form-btn-arrow">
                                        <img  src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= Yii::app()->theme->baseUrl ?>/img/hotels/icon-arrow.svg" alt="">
                                    </span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <address class="contacts__address">

                    <p class="contacts__address-title"><?= t('Будьте на связи!') ?></p>

                    <ul class="contacts__address-list">
                        <li class="contacts__address-item">
                            <strong><?= t('Адрес') ?>:</strong> <?= config('contact_address') ?>
                        </li>
                        <li class="contacts__address-item">
                            <strong><?= t('Телефон') ?>:</strong>
                            <?php foreach (explode(',', config('contact_phone')) as $phoneNumber): ?>
                                <a href="tel:+38<?= preg_replace('#[^\d]*#', '', $phoneNumber) ?>">
                                    <?= $phoneNumber ?>
                                </a>
                            <?php endforeach;?>
                        </li>
                        <li class="contacts__address-item">
                            <strong>Email:</strong>
                            <a href="mailto:<?= config('contact_email') ?>">
                                <?= config('contact_email') ?>
                            </a>
                        </li>
                        <li class="contacts__address-item">
                            <strong>Skype:</strong> <a href="skype:profikarkas?chat">profikarkas</a>
                        </li>
                    </ul>
                    <div class="contacts__map" id="map"></div>
                </address>
            </div>
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php if (strip_tags($baseInformation->text_ru)): ?>
                    <article class="contacts__descr">
                        <h1 class="contacts__descr-title"><?= $baseInformation->title_ru ?></h1>
                        <div class="contacts__descr-inner">
                            <?= $baseInformation->text_ru ?>
                        </div>
                    </article>
                <?php endif; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php if (strip_tags($baseInformation->text_ua)): ?>
                    <article class="contacts__descr">
                        <h1 class="contacts__descr-title"><?= $baseInformation->title_ua ?></h1>
                        <div class="contacts__descr-inner">
                            <?= $baseInformation->text_ua ?>
                        </div>
                    </article>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="decor decor--contacts-bottom"></div>
    </div>
</section>

<script>

    function init() {
        var lazyElements = document.querySelectorAll("iframe[data-src], img[data-src]");
        for (var i = 0; i < lazyElements.length; i++) {
            if(lazyElements[i].getAttribute('data-src')) {
                lazyElements[i].setAttribute('src', lazyElements[i].getAttribute('data-src'));
            }
        }
    }
    document.addEventListener('DOMContentLoaded', function () {
        window.setTimeout('init()', 4000);
    });
</script>
