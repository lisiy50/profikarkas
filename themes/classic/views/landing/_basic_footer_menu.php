<?php foreach (MenuItem::model()->findAll('parent_id = 2') as $key => $menuItem): ?>
    <div class="footer__menu-item">
        <a class="footer__menu-item-title" href="<?= $menuItem->url ?>" class=""><?= $menuItem->name ?></a>
        <ul class="footer__submenu">
            <?php foreach ($menuItem->children as $child): ?>
                <li class="footer__submenu-item">
                    <?php if ($child->children): ?>
                        <div class="footer__submenu-children"><?= $child->name ?></div>
                        <?php $subMenuHtmlArr = []; ?>
                        <?php
                        foreach ($child->children as $child1) {
                            $subMenuHtmlArr[] = '<a class="footer__submenu-link" href="' . $child1->url. '">' . $child1->name . '</a>';
                        }
                        echo implode(', ', $subMenuHtmlArr);
                        ?>
                    <?php else: ?>
                        <a class="footer__submenu-link" href="<?= $child->url ?>"><?= $child->name ?></a>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endforeach; ?>
