<?php
/**
 * @var LandingController $this
 * @var Review $model
 */
?>

<div>
    <div class="hidden-xs col-sm-6 col-sm-offset-1 col-md-5 col-md-offset-2 col-lg-5 col-lg-offset-2">
        <div class="picture_left">
            <?php if($model->getProject()): ?>
                <?php foreach ($model->getProject()->iVisualizations as $i => $image): if ($i >= 2) break;?>
                    <div class="left_2"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('facadeSmall'); ?>"></div>
                <?php endforeach; ?>
                <?php foreach ($model->getProject()->iLayouts as $image): ?>
                    <div class="left_2"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('facadeSmall'); ?>"></div>
                <?php break; endforeach; ?>
            <?php else: ?>
                <?php foreach ($model->images as $k=>$image): ?>
                    <?php if($k < 3): ?>
                        <div class="left_2"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('more');?>" alt="Проект дома <?php echo $model->title ?>"></div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="house11" style="overflow:hidden; background-image: url('<?php echo $model->images ? $model->images[0]->getImageUrl('more') : '';?>'); background-repeat: no-repeat; background-position: 50% 50%; background-size: 100%">
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/house13.png" alt="<?= t('дом')?>" style="opacity: 0;"><!-- эта скрытая картинка нужна, чтобы  не перепиcывать стили для мобильных -->
        </div>

        <div class="description">
            <?php if($model->getProject()): ?>
                <?php if($model->getProject()->net_area): ?>
                    <p><?= t('Площадь дома')?>:<span class="desc_right"><?php echo $model->getProject()->net_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
                <?php if($model->getProject()->minimum_building_area): ?>
                    <p><?= t('Площадь застройки')?>:<span class="desc_right"><?php echo $model->getProject()->minimum_building_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
                <?php if($model->getProject()->cubage): ?>
                    <p><?= t('Кубатура')?>:<span class="desc_right"><?php echo $model->getProject()->cubage; ?> <?= t('м')?><sup>3</sup></span></p>
                <?php endif; ?>
                <?php if($model->getProject()->house_height): ?>
                    <p><?= t('Высота')?>:<span class="desc_right"><?php echo $model->getProject()->house_height; ?> <?= t('м')?></span></p>
                <?php endif; ?>
                <?php if($model->getProject()->roof_angle): ?>
                    <p><?= t('Угол наклона кровли')?>:<span class="desc_right"><?php echo $model->getProject()->roof_angle; ?> °</span></p>
                <?php endif; ?>
                <?php if($model->getProject()->roof_area): ?>
                    <p><?= t('Площадь крыши')?>:<span class="desc_right"><?php echo $model->getProject()->roof_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
            <?php else: ?>
                <?php if($model->project_area): ?>
                    <p><?= t('Площадь дома')?>:<span class="desc_right"><?php echo $model->project_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
                <?php if($model->project_built_area): ?>
                    <p><?= t('Площадь застройки')?>:<span class="desc_right"><?php echo $model->project_built_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
                <?php if($model->project_volume): ?>
                    <p><?= t('Кубатура')?>:<span class="desc_right"><?php echo $model->project_volume; ?> <?= t('м')?><sup>3</sup></span></p>
                <?php endif; ?>
                <?php if($model->project_height): ?>
                    <p><?= t('Высота')?>:<span class="desc_right"><?php echo $model->project_height; ?> <?= t('м')?></span></p>
                <?php endif; ?>
                <?php if($model->project_roof_angle): ?>
                    <p><?= t('Угол наклона кровли')?>:<span class="desc_right"><?php echo $model->project_roof_angle; ?> °</span></p>
                <?php endif; ?>
                <?php if($model->project_roof_area): ?>
                    <p><?= t('Площадь крыши')?>:<span class="desc_right"><?php echo $model->project_roof_area; ?> <?= t('м')?><sup>2</sup></span></p>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-3 marperson">
        <div class="person">
            <div class="person_photo">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $model->getImageUrl('thumb'); ?>">
                <p><?php echo $model->client_post; ?></p>
            </div>
            <div class="desc_text" <?php echo !$model->getCountVideos() ? 'style="height:255px;"' : ''; ?>>
                <div>
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <span><?php echo $model->client_name; ?></span>
                        <p><?php echo $model->getShortDescription(); ?></p>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <span><?php echo $model->client_name_uk; ?></span>
                        <p><?php echo $model->getShortDescriptionUk(); ?></p>
                    <?php endif; ?>
                    <?php if($model->client_facebook): ?>
                        <a class="facebook" href="<?php echo $model->client_facebook; ?>"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/homepage/images/facey.png"></a>
                    <?php endif; ?>
                </div>
            </div>
            <a class="read_more" href="<?php echo $model->url; ?>"><?= t('Читать больше...')?></a>

            <?php if ($model->getCountVideos()): ?>
                <div class="video3">
                    <iframe loading="lazy" width="273" height="144" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $model->videos[0]['videoLink']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
