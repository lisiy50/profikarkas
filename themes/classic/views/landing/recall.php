<?php
/**
 * @var LandingController $this
 * @var RecallForm $model
 */
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <div class="" style="color: #ffde17">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php else: ?>

    <?php
    /** @var CActiveForm $form */
    $form=$this->beginWidget('CActiveForm', array(
        'enableClientValidation'=>true,
        'action' => url('landing/recall'),
        'id' => 'landing_recall',
        'htmlOptions'=>array(
            'class'=>'form order_call',
        ),
    ));
    ?>
    <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
    <input type="hidden" name="action" value="validate_captcha">
<div class="input-container">
    <?php echo $form->textField($model, 'name', array('required' => 'required', 'autocomplete' => 'off')); ?>
    <label for="RecallForm_name"><?= t('Ваше имя') ?></label>
    <div class="bar" <?php echo $model->hasErrors('name') ? 'style="background-color:red"' : '' ; ?>></div>
</div>
<div class="input-container">
    <?php echo $form->textField($model, 'phone', array('required' => 'required', 'autocomplete' => 'off')); ?>
    <label for="RecallForm_phone"><?= t('Контактный телефон') ?></label>
    <div class="bar" <?php echo $model->hasErrors('phone') ? 'style="background-color:red"' : '' ; ?>></div>
</div>
<div class="input-container">
    <?php echo $form->textField($model, 'email', array('required' => 'required', 'autocomplete' => 'off')); ?>
    <label for="RecallForm_email"><?= t('Ваш e-mail') ?></label>
    <div class="bar" <?php echo $model->hasErrors('email') ? 'style="background-color:red"' : '' ; ?>></div>
</div>

<?php echo CHtml::submitButton(t('Заказать звонок'), array('class'=>"button_popup")); ?>

<?php $this->endWidget(); ?>

<?php endif; ?>

