<?php
/**
 * @var LandingController $this
 * @var $projects
 * @var $baseInformation Landing
 * @var $textInformation LandingText
 */

cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/scripts-int-2.min.js');
cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/landing_en.js');
cs()->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/css/styles-int-3.min.css');

?>

<style>
    html {
        scroll-behavior: smooth;
    }

    .slick-slide {
        height: initial !important;
    }
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Prefab houses, Prefab houses Ukraine, Prefabricated houses, Prefabricated houses Ukraine" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/logo_small.png" media="nope!" onload="this.media='all'" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" media="nope!" onload="this.media='all'" rel="stylesheet">
    <title><?= $baseInformation->meta_title_ru ?></title>
</head>
<body>
<header class="mod-header" role="banner">
    <div class="mod-header__banner">
        <div class="container">
            <div class="mod-header__banner-inner">
                <div class="mod-header__btn">
                    <span class="mod-header__btn-item"></span>
                    <span class="mod-header__btn-item"></span>
                    <span class="mod-header__btn-item"></span>
                </div>
                <div class="mod-header__email">
                    <a class="mod-header__email-link" href="mailto:<?= config('contact_email') ?>">
                        <?= config('contact_email') ?>
                    </a>
                </div>
                <p class="mod-header__slogan">
                    <?php if (strip_tags($textInformation[20]['text_ru'])): ?>
                        <?= $textInformation[20]['text_ru'] ?>
                    <?php endif; ?>
                    <a class="mod-header__slogan-link" href="#partnerships">Press here for more details</a>.</p>
                <div class="mod-header__contacts">
                    <div class="mod-header__phone">
                        <a class="mod-header__phone-link" href="tel:<?= str_replace([' ', '-', '(', ')'], '', config('contact_phone_int')) ?>">
                            <?= config('contact_phone_int') ?>
                        </a>
                    </div>
                    <div class="mod-header__messengers">
                        <a class="mod-header__messengers-link mod-header__messengers-link--whatsapp" href="https://wa.me/<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">
                            <svg width="1.25em" height="1.25em" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                                <rect height="512" id="rect2987" rx="64" ry="64" style="fill:#65bc54;fill-opacity:1;fill-rule:nonzero;stroke:none" width="512" x="0" y="0"/>
                                <path d="m 456,250.85266 c 0,107.60904 -87.9126,194.8442 -196.36397,194.8442 -34.43066,0 -66.77677,-8.80168 -94.9199,-24.24162 L 56.000005,456 91.437745,351.45584 C 73.559715,322.08872 63.265025,287.65523 63.265025,250.85124 63.265025,143.23516 151.18049,56 259.63463,56 368.0874,56.001 456,143.23657 456,250.85266 z M 259.63603,87.03196 c -91.04092,0 -165.093965,73.49248 -165.093965,163.8207 0,35.84056 11.683465,69.04162 31.446055,96.04529 l -20.62177,60.83151 63.44285,-20.16403 c 26.07126,17.11323 57.29196,27.09805 90.82543,27.09805 91.02965,0 165.09396,-73.48543 165.09396,-163.81224 0,-90.3268 -74.06292,-163.81928 -165.09256,-163.81928 z m 99.15526,208.68972 c -1.20989,-1.98879 -4.4185,-3.18602 -9.22424,-5.5706 -4.81705,-2.3874 -28.48964,-13.94551 -32.894,-15.53429 -4.41845,-1.59301 -7.63122,-2.39304 -10.83838,2.38458 -3.20432,4.79028 -12.42856,15.53429 -15.24273,18.72031 -2.80853,3.19166 -5.60863,3.59026 -10.42569,1.20003 -4.80578,-2.38739 -20.32177,-7.4284 -38.70826,-23.70215 -14.30749,-12.65815 -23.96978,-28.2854 -26.77831,-33.07147 -2.80854,-4.77903 -0.2972,-7.3622 2.10993,-9.73975 2.16626,-2.14796 4.81423,-5.58186 7.22416,-8.36364 2.40712,-2.79447 3.20715,-4.78184 4.80861,-7.96926 1.61272,-3.18884 0.80002,-5.97485 -0.3986,-8.3707 -1.20286,-2.38317 -10.83274,-25.88955 -14.84415,-35.449 -4.01138,-9.55947 -8.0115,-7.96646 -10.82568,-7.96646 -2.80996,0 -6.01569,-0.40002 -9.22987,-0.40002 -3.20997,0 -8.42703,1.19864 -12.83562,5.97344 -4.41001,4.78325 -16.84138,16.33291 -16.84138,39.83365 0,23.50497 17.24279,46.21133 19.65273,49.39594 2.40431,3.17756 33.28838,52.9721 82.21811,72.10228 48.94802,19.11328 48.94802,12.74407 57.77365,11.937 8.81437,-0.78735 28.46992,-11.54403 32.48832,-22.70072 4.0086,-11.14964 4.0086,-20.71896 2.8114,-22.70917 z" style="fill:#ffffff;fill-rule:evenodd"/>
                            </svg>
                        </a>
                        <a class="mod-header__messengers-link mod-header__messengers-link--telegram" href="https://t.me/MykolaKudinenko">
                            <svg width="1.25em" height="1.25em" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                <g transform="translate(297.22034,0)">
                                    <rect height="512" rx="64" ry="64" style="fill:#64a9dc; fill-opacity:1; fill-rule:nonzero; stroke:none" width="512" x="-297.22034" y="5.6843419e-014"/>
                                    <path d="M 127.88103,94.198045 -218.4667,227.78507 c -23.63879,9.48433 -23.4986,22.66901 -4.30871,28.54816 l 86.38017,26.96101 33.05108,101.33207 c 4.018041,11.09077 2.03732,15.48953 13.683731,15.48953 8.98786,0 12.97504,-4.09778 17.98087,-8.98786 3.1833,-3.11513 22.08378,-21.49087 43.18752,-42.00936 l 89.85158,66.38643 c 16.5339,9.12161 28.47227,4.39617 32.58935,-15.35577 L 152.92948,122.20987 C 158.96812,98.000015 143.69981,87.018565 127.88103,94.198045 z M -122.83885,277.09998 71.862531,154.2616 c 9.71969,-5.89458 18.63166,-2.72542 11.31455,3.76981 l -166.71528,150.41932 -6.49136,69.23533 -32.809291,-100.58608 z" style="fill:#ffffff"/>
                                </g>
                            </svg>
                        </a>
                        <a class="mod-header__messengers-link mod-header__messengers-link--viber" href="viber://chat/?number=%2B<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">
                            <svg width="1.25em" height="1.25em" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                <rect height="512" rx="64" ry="64" width="512" fill="#7b519d"/>
                                <g fill="#fff" fill-rule="evenodd">
                                    <path d="M421.915 345.457c-12.198-9.82-25.233-18.634-38.064-27.638-25.59-17.973-48.996-19.37-68.091 9.546-10.723 16.234-25.734 16.945-41.43 9.823-43.27-19.62-76.683-49.85-96.255-93.83-8.658-19.458-8.544-36.903 11.713-50.665 10.725-7.278 21.53-15.89 20.666-31.793-1.128-20.736-51.475-90.033-71.357-97.347-8.227-3.027-16.42-2.83-24.79-.017-46.62 15.678-65.93 54.019-47.437 99.417 55.17 135.442 152.26 229.732 285.91 287.282 7.62 3.277 16.085 4.587 20.371 5.763 30.428.306 66.073-29.01 76.367-58.104 9.911-27.99-11.035-39.1-27.603-52.437zM272.06 77.439c97.707 15.025 142.768 61.485 155.21 159.895 1.154 9.09-2.232 22.768 10.737 23.02 13.554.259 10.288-13.217 10.402-22.316 1.146-92.684-79.669-178.606-173.524-181.774-7.081 1.019-21.733-4.883-22.647 10.988-.609 10.7 11.727 8.942 19.822 10.187z"/>
                                    <path d="M291.172 104.422c-9.398-1.132-21.805-5.56-24.001 7.48-2.293 13.687 11.535 12.297 20.42 14.286 60.346 13.487 81.358 35.451 91.294 95.311 1.451 8.727-1.432 22.31 13.399 20.059 10.991-1.674 7.021-13.317 7.94-20.118.487-57.47-48.758-109.778-109.052-117.018z"/>
                                    <path d="M296.713 151.416c-6.273.155-12.43.834-14.736 7.538-3.463 10.02 3.822 12.409 11.237 13.6 24.755 3.974 37.783 18.571 40.256 43.257.668 6.7 4.92 12.129 11.392 11.365 8.969-1.07 9.78-9.053 9.505-16.634.443-27.734-30.904-59.79-57.654-59.126z"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mod-header__menu">
        <div class="container">
            <div class="mod-header__menu-wrap">
                <ul class="mod-header__menu-inner">
                    <li class="mod-header__menu-item">
                        <a href="#what_we_produce" class="mod-header__menu-link">What we produce</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#about_us" class="mod-header__menu-link">About us</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#benefits" class="mod-header__menu-link">Your benefits</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#materials" class="mod-header__menu-link">Materials we use</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#projects" class="mod-header__menu-link">Projects and costs</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#housekits" class="mod-header__menu-link">Housekits</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#built_houses" class="mod-header__menu-link">Built houses</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#feedback" class="mod-header__menu-link">Feedback</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#energoeffeciency" class="mod-header__menu-link">Energoeffeciency</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#partnerships" class="mod-header__menu-link">Details of partnerships</a>
                    </li>
                    <li class="mod-header__menu-item">
                        <a href="#contacts" class="mod-header__menu-link">Contacts</a>
                    </li>
                </ul>
                <div class="mod-header__nav">
                    <span class="mod-header__nav-btn">
                        <svg height="48px" width="64px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 480">
                         <defs id="defs3">
                          <clipPath id="clipPath8673" clipPathUnits="userSpaceOnUse">
                           <rect id="rect8675" fill-opacity="0.67" height="512" width="682.67" y="-.0000028613" x="-85.333"/>
                          </clipPath>
                         </defs>
                         <g id="flag" clip-path="url(#clipPath8673)" transform="matrix(.93750 0 0 .93750 80 .0000026825)">
                          <g id="g578" stroke-width="1pt" transform="matrix(17.067 0 0 17.067 -256 -0.0000024)">
                           <rect id="rect124" height="30" width="60" y="0" x="0" fill="#006"/>
                           <g id="g584">
                            <path id="path146" d="m0 0v3.3541l53.292 26.646h6.708v-3.354l-53.292-26.646h-6.708zm60 0v3.354l-53.292 26.646h-6.708v-3.354l53.292-26.646h6.708z" fill="#fff"/>
                            <path id="path136" d="m25 0v30h10v-30h-10zm-25 10v10h60v-10h-60z" fill="#fff"/>
                            <path id="path141" d="m0 12v6h60v-6h-60zm27-12v30h6v-30h-6z" fill="#c00"/>
                            <path id="path150" d="m0 30 20-10h4.472l-20 10h-4.472zm0-30 20 10h-4.472l-15.528-7.7639v-2.2361zm35.528 10 20-10h4.472l-20 10h-4.472zm24.472 20-20-10h4.472l15.528 7.764v2.236z" fill="#c00"/>
                           </g>
                          </g>
                         </g>
                        </svg>
                    </span>
                    <div class="mod-header__nav-inner">
                        <a href="<?php echo Yii::app()->getHomeUrl(); ?>" class="mod-header__nav-link">
                            <svg height="48px" width="64px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 480">
                                <g id="flag" fill-rule="evenodd" stroke-width="1pt" transform="matrix(1.25 0 0 .9375 0 -.0000018493)">
                                    <rect id="rect171" height="512" width="512" y=".0000024116" x="0" fill="#ffff0b"/>
                                    <rect id="rect403" height="256" width="512" y=".0000024116" x="0" fill="#268cff"/>
                                </g>
                            </svg>
                        </a>
                        <a href="/de" class="mod-header__nav-link">
                            <svg height="48px" width="64px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 480">
                                <g id="flag" fill-rule="evenodd" stroke-width="1pt" transform="matrix(.60207 0 0 .75259 0 -.000028157)">
                                    <rect id="rect171" height="212.6" width="1063" y="425.2" x="0" fill="#ffe600"/>
                                    <rect id="rect256" height="212.6" width="1063" y="0.000038" x="0" fill="#000000"/>
                                    <rect id="rect255" height="212.6" width="1063" y="212.6" x="0" fill="#f00"/>
                                </g>
                            </svg>
                        </a>
                        <a href="/fr" class="mod-header__nav-link">
                            <svg height="48px" width="64px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 480">
                                <g id="g3627" fill-rule="evenodd" stroke-width="1pt" transform="scale(1.25 .9375)">
                                    <rect id="rect171" height="512" width="512" y="7.4219e-7" x="0" fill="#fff"/>
                                    <rect id="rect403" height="512" width="170.67" y="7.4219e-7" x="0" fill="#00267f"/>
                                    <rect id="rect135" height="512" width="170.67" y="7.4219e-7" x="341.33" fill="#f31830"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
<main class="main mod-main">
    <div class="mod-header__base">
        <div class="container">
            <div class="mod-header__base-inner">
                <h1 class="mod-header__title">
                    <?= $baseInformation->title_ru ?>
                </h1>
            </div>
        </div>
    </div>
    <section class="video">
        <span class="anchor" id="what_we_produce"></span>
        <div class="container">
            <div class="video__inner">
                <p class="video__text video__text--mod">
                    <?php if (strip_tags($textInformation[21]['text_ru'])): ?>
                        <?= $textInformation[21]['text_ru'] ?>
                    <?php endif; ?>
                </p>
                <div class="video__video">
                    <iframe width="854" height="480" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://www.youtube.com/embed/CfJUxWYl1Cg?rel=0&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--certificate" style="background-position: 50% 2.25em;">
        <span class="anchor" id="about_us"></span>
        <div class="container">
            <h2 class="title-primary title-primary--fixed">
                About Us
            </h2>
            <div class="section__inner section__inner--cooperate" style="border-bottom: 2px solid #000000;">
                <?php if (strip_tags($textInformation[75]['text_ru'])): ?>
                    <?= $textInformation[75]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--cooperate">
        <span class="anchor" id="benefits"></span>
        <div class="container">
            <h2 class="title-secondary">
                Your benefits:
            </h2>
            <div class="section__inner section__inner--cooperate">
                <?php if (strip_tags($textInformation[76]['text_ru'])): ?>
                    <?= $textInformation[76]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--materials" style="background-position: 0 50%;">
        <span class="anchor" id="materials"></span>
        <div class="container">
            <h2 class="title-primary">
                <?php if (strip_tags($textInformation[22]['text_ru'])): ?>
                    <?= $textInformation[22]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--materials">
                <p class="section__text">
                    <?php if (strip_tags($textInformation[23]['text_ru'])): ?>
                        <?= $textInformation[23]['text_ru'] ?>
                    <?php endif; ?>
                </p>
                <?php if (strip_tags($textInformation[24]['text_ru'])): ?>
                    <?= $textInformation[24]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary">
                <?php if (strip_tags($textInformation[25]['text_ru'])): ?>
                    <?= $textInformation[25]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--design">
                <?php if (strip_tags($textInformation[26]['text_ru'])): ?>
                    <?= $textInformation[26]['text_ru'] ?>
                <?php endif; ?>
            </div>
            <div class="slider">
                <div class="slider__gallery" id="projectsGallery">
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-01.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-02.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-03.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-04.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-05.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-06.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-07.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-08.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-09.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-10.jpg" alt="">
                        <span class="slider__logo"></span>
                    </div>
                </div>
                <div class="slider__thumbs" id="projectsThumbs">
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-01.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-02.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-03.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-04.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-05.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-06.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-07.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-08.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-09.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/project-thumb-10.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="projects">
        <span class="anchor" id="projects"></span>
        <div class="container">
            <div class="projects__inner">
                <h2 class="title-secondary">Projects and costs</h2>
                <div class="projects__carousel">
                    <div class="projects__carousel-inner" id="projectsCarousel">
                        <?php
                        $total = count($projects);
                        ?>
                        <?php foreach ($projects as $key => $project): ?>
                            <?php
                            $workScopePrice = WorkScopePrice::model()->findByAttributes(array('home_kit_id' => 2, 'work_scope_id' => 10, 'project_id' => $project->id));
                            $modulo = ($key + 1) % 2;
                            ?>
                            <?php if ($modulo): ?>
                                <div class="projects__carousel-item">
                                <div class="projects__carousel-item-inner">
                            <?php endif; ?>
                            <div class="projects__carousel-subitem">
                                <div class="projects__carousel-subitem-content">
                                    <div class="projects__carousel-subitem-title">
                                        <?= $project->getName() ?>
                                    </div>
                                    <div class="projects__carousel-subitem-price">
                                        <?= $workScopePrice ? price('{price}', $workScopePrice->price) . ' €' : ''; ?>
                                    </div>
                                    <div class="projects__carousel-subitem-area">
                                        <?= $project->net_area ?> m<sup>2</sup>
                                    </div>
                                    <div class="projects__carousel-subitem-plans">
                                        <?php foreach ($project->iLayouts as $i => $image): if ($i >= 2) break; ?>
                                            <div class="projects__carousel-subitem-plans-pic">
                                                <img src="<?php echo $image->getImageUrl('thumb'); ?>"
                                                     alt="The layout of <?= $project->getName() ?> house"
                                                     class="projects__carousel-subitem-plans-img">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="projects__carousel-subitem-pic">
                                    <img src="<?= $project->getFirstImageUrl() ? $project->getFirstImageUrl()->getImageUrl('landing'):''; ?>"
                                         alt="<?= $project->getName() ?>"
                                         class="projects__carousel-subitem-img">
                                    <a href="<?= $project->url ?>" class="projects__carousel-subitem-link">
                                                <span class="projects__carousel-subitem-link-inner">
                                                    <?= t('More details') ?>
                                                </span>
                                        <span class="projects__carousel-subitem-link-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.876" height="16" viewBox="0 0 9.876 16">
                                              <defs>
                                                <style>
                                                  .cls-1 { fill: #fff; fill-rule: evenodd;}
                                                </style>
                                              </defs>
                                              <path class="cls-1" d="M912.057,9332.89l6.114,6.11-6.114,6.11,1.887,1.89,8-8-8-8Z" transform="translate(-912.062 -9331)"/>
                                            </svg>
                                        </span>
                                    </a>
                                </div>

                            </div>
                            <?php if (!$modulo || $key + 1 == $total): ?>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <span class="projects__carousel-arrow projects__carousel-arrow--prev"></span>
                    <span class="projects__carousel-arrow projects__carousel-arrow--next"></span>
                </div>
            </div>
        </div>
    </section>
    <section class="kits">
        <span class="anchor" id="housekits"></span>
        <div class="container">
            <h2 class="title-primary">
                <?php if (strip_tags($textInformation[27]['text_ru'])): ?>
                    <?= $textInformation[27]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="kits__inner">
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[28]['text_ru'])): ?>
                            <?= $textInformation[28]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-0.png" alt="Exterior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[29]['text_ru'])): ?>
                            <?= $textInformation[29]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-1.png" alt="Exterior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[30]['text_ru'])): ?>
                            <?= $textInformation[30]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-2.png" alt="Interior walls" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[31]['text_ru'])): ?>
                            <?= $textInformation[31]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic" style="margin-top: initial;">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-3.png" alt="Intermediate floors" class="kits__img">
                    </div>
                </div>
                <div class="kits__item">
                    <div class="kits__text">
                        <?php if (strip_tags($textInformation[32]['text_ru'])): ?>
                            <?= $textInformation[32]['text_ru'] ?>
                        <?php endif; ?>
                    </div>
                    <div class="kits__pic">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/kit-4.png" alt="Roofs" class="kits__img">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <span class="anchor" id="built_houses"></span>
        <div class="container">
            <h2 class="title-secondary">Do you want to see what we have already built?</h2>
            <div class="slider">
                <div class="slider__gallery" id="housesGallery">
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-01.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-02.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-03.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-04.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-05.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-06.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-07.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-081.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-09.jpg" alt="">
                    </div>
                    <div class="slider__item">
                        <img class="slider__img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-10.jpg" alt="">
                    </div>
                </div>
                <div class="slider__thumbs" id="housesThumbs">
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-01.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-02.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-03.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-04.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-05.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-06.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-07.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-081.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-09.jpg" alt="">
                    </div>
                    <div class="slider__thumb">
                        <img class="slider__thumb-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/slides/house-thumb-10.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="video video--white">
        <span class="anchor" id="feedback"></span>
        <div class="container">
            <h2 class="title-primary">Feedback from the house owner Lyon, France</h2>
            <div class="video__inner video__inner--white">
                <div class="video__video">
                    <iframe width="854" height="480" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://www.youtube.com/embed/hzRJ-B-snvY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--certificate">
        <span class="anchor" id="energoeffeciency"></span>
        <div class="container">
            <h2 class="title-primary">
                <?php if (strip_tags($textInformation[33]['text_ru'])): ?>
                    <?= $textInformation[33]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--certificate">
                <div class="section__item section__item--text">
                    <p class="section__item-text">
                        <?php if (strip_tags($textInformation[34]['text_ru'])): ?>
                            <?= $textInformation[34]['text_ru'] ?>
                        <?php endif; ?>
                    </p>
                    <p class="section__item-text section__item-text--selected">
                        <?php if (strip_tags($textInformation[35]['text_ru'])): ?>
                            <?= $textInformation[35]['text_ru'] ?>
                        <?php endif; ?>
                    </p>
                </div>
                <div class="section__item section__item--pic">
                    <img class="section__item-img" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/certificate.jpg" alt="Certificate Passive House Institute">
                </div>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <div class="container">
            <h2 class="title-secondary">
                <?php if (strip_tags($textInformation[36]['text_ru'])): ?>
                    <?= $textInformation[36]['text_ru'] ?>
                <?php endif; ?>
            </h2>
            <div class="section__inner section__inner--design">
                <?php if (strip_tags($textInformation[37]['text_ru'])): ?>
                    <?= $textInformation[37]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--cooperate">
        <span class="anchor" id="partnerships"></span>
        <div class="container">
            <h2 class="title-primary">
                How to cooperate with us:
            </h2>
            <div class="section__inner section__inner--cooperate">
                <?php if (strip_tags($textInformation[38]['text_ru'])): ?>
                    <?= $textInformation[38]['text_ru'] ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="section section--design">
        <span class="anchor" id="contacts"></span>
        <div class="container">
            <h2 class="title-secondary title-secondary--small">
                <span class="title-secondary__item">Contacts</span>
            </h2>
            <div class="section__inner section__inner--address">
                <address class="section__address">
                    <p class="section__address-item">
                        <span>Phone:</span>
                        <span>
                            <a href="tel:<?= str_replace([' ', '-', '(', ')'], '', config('contact_phone_int')) ?>">
                                <?= config('contact_phone_int') ?>
                            </a>
                            -
                            <a href="https://t.me/MykolaKudinenko">Telegram</a>,
                            <a href="viber://chat/?number=%2B<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">Viber</a>,
                            <a href="https://wa.me/<?= str_replace(['+', ' ', '(', ')'], '', config('contact_phone_int')) ?>">
                                WhatsApp
                            </a>
                        </span>
                    </p>
                    <p class="section__address-item">
                        <span>Email:</span>
                        <span><a href="mailto:<?= config('contact_email') ?>"><?= config('contact_email') ?></a></span>
                    </p>
                </address>
            </div>
            <div style="padding: 0 25px 25px; background-color: #ffffff;text-align: center;">
                Company details: Limited Liability Company 'Profibudcomplect' //
                Registered office: 02000, Kyiv, Prospect Grigorenka, 38-А, apt. 157 //
                EDRPOU (National State Registry of Ukrainian Enterprises and Organizations) 38657557 //
                Acc. UA343510050000026001610250000 // Bank: PAO "UkrSibbank" Kharkiv // MFO code (sort code) 351005
            </div>
        </div>
    </section>
    <section class="form">
		<div id="form-container"></div>
    </section>
</main>
<script src="https://www.google.com/recaptcha/api.js?render=6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
            document.getElementById('g-recaptcha-response').value=token;
        });
    });
</script>
</body>
</html>
