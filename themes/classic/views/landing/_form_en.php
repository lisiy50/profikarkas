<?php
/**
 * @var LandingController $this
 * @var LandingEnForm $model
 */
?>

<div class="container">
    <div class="form__inner">

        <?php if(Yii::app()->user->hasFlash('success')): ?>
			<p class="form__title">
				<span class="form__title-item"><?php echo Yii::app()->user->getFlash('success'); ?></span>
			</p>
        <?php else: ?>
			<p class="form__title">
				<span class="form__title-item">If you have any questions or need additional</span>
				<span class="form__title-item"> information, please, feel free to contact us.</span>
			</p>

			<div class="form__wrap">

                <?php
                /* @var BootActiveForm $form */
                $form=$this->beginWidget('BootActiveForm', array(
                    'enableClientValidation'=>false,
                    'action' => url('landing/ajaxEnForm'),
                    'id' => 'LandingEnForm-form',
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                    ),
                ));
                ?>

				<?php if ($model->hasErrors()): ?>
                    <?php echo $form->errorSummary($model)?>
				<?php endif; ?>

                    <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                    <input type="hidden" name="action" value="validate_captcha">

					<div class="form__group">
                        <?php echo $form->textField($model, 'name', array('class'=>'form__control', 'placeholder' => 'Name *', 'required' => 'required'));?>
					</div>
					<div class="form__group form__group--group">
						<div class="form__group-item">

                            <?php echo $form->textField($model, 'country', array('class'=>'form__control', 'placeholder' => 'Country *', 'required' => 'required'));?>

						</div>
						<div class="form__group-item">
                            <?php echo $form->textField($model, 'city', array('class'=>'form__control', 'placeholder' => 'City *', 'required' => 'required'));?>
						</div>
					</div>
					<div class="form__group">
                        <?php echo $form->emailField($model, 'email', array('class'=>'form__control', 'placeholder' => 'Email *', 'required' => 'required'));?>
					</div>
					<div class="form__group">
                        <?php echo $form->textArea($model, 'text', array('class'=>'form__control', 'placeholder' => 'Text *', 'required' => 'required'));?>
					</div>
					<div class="form__group">
                        <label class="form__btn-upload" for="LandingEnForm_file">
                            <?php echo $form->fileField($model, 'file', array('class'=>'', 'style'=>'display:none', 'onchange' => '$("#uploadFile").html(this.files[0].name)')); ?>
                            <span class="form__btn-upload-plus">+</span> Attach file
                        </label>
                        <span class='form__label-upload' id="uploadFile"></span>
					</div>

					<div class="form__btn-wrap">
                        <?php echo CHtml::submitButton(t('Send'), array('class'=>"form__btn"));?>
					</div>

                    <p class="form__hint">Fields marked with an * are required.</p>

                <?php $this->endWidget();?>

			</div>

        <?php endif; ?>

    </div>
</div>
