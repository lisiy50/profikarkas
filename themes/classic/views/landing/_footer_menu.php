<?php foreach (MenuItem::model()->findAll('parent_id = 2') as $key => $menuItem): ?>
    <div  class="footer_menu_item">
        <a href="<?php echo $menuItem->url;?>" class=""><?php echo $menuItem->name;?></a>
        <ul class="">
            <?php foreach($menuItem->children as $child):?>
                <li>
                    <?php if($child->children): ?>
                        <a class="hasChildren"><?php echo $child->name;?></a>
                        <br>
                        <?php $subMenuHtmlArr = array();?>
                        <?php
                        foreach ($child->children as $child1) {
                            $subMenuHtmlArr[] = '<a href="'.$child1->url.'">'.$child1->name.'</a>';
                        }
                        echo implode(', ', $subMenuHtmlArr);
                        ?>
                    <?php else: ?>
                        <a href="<?php echo $child->url;?>"><?php echo $child->name;?></a>
                    <?php endif ?>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endforeach; ?>

