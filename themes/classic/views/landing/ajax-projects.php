<?php
/**
 * @var LandingController $this
 * @var CActiveDataProvider $dataProvider
 * @var Project $model
 * @var null|integer $itemWidth
 */
?>

<?php /** @var Project $project */?>

<?php foreach ($dataProvider->data as $i => $project): ?>
    <?php $workScopePrice = WorkScopePrice::model()->findByAttributes(array('home_kit_id' => 2, 'work_scope_id' => 1, 'project_id' => $project->id)); ?>
    <?php if ($i != 0 && $i % 2 == 0): ?>
    </div>
    <?php endif; ?>
    <?php if ($i % 2 == 0): ?>
    <div class="<?php $i == 0 ? 'active curr' : ''; ?>" style="<?php echo $itemWidth ? "width:{$itemWidth};" : ''; ?>">
    <?php endif; ?>
    <?php $clases = $i % 2 > 0 ? 'col-xs-11 col-xs-offset-1 col-sm-11 col-sm-offset-0 col-md-11 col-md-offset-0 col-md-offset-1 col-lg-11 col-lg-offset-0' : 'col-xs-11 col-xs-offset-1 col-sm-11 col-sm-offset-0 col-md-11 col-md-offset-0 col-lg-11 col-lg-offset-0 shadow3_768'; ?>
    <div class="<?php echo $clases; ?>">
        <div class="shadow3">
            <div class="gray">
                <h6><?php echo $project->getName() ?></h6>
                <span><?php echo $workScopePrice ? price('{price} {short}.', $workScopePrice->price) : ''; ?></span>
                <p><?php echo $project->net_area;?> м 2</p>

                <div class="clearfix" style="width:300px;position:relative;z-index:1;">
                    <?php foreach ($project->iLayouts as $i => $image): if ($i >= 2) break; ?>
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('thumb'); ?>" alt="Планировака дома <?php echo $project->getName(); ?>" style="float:left;">
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="house_slider">
                <div>
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $project->getFirstImageUrl()?$project->getFirstImageUrl()->getImageUrl('landing'):'';?>" alt="<?php echo $project->getName(); ?>">
                </div>
                <a href="<?php echo $project->url; ?>"> <span><?= t('подробнее') ?></span>
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->baseUrl; ?>/images/arrow.png">
                </a>
            </div>
        </div>
    </div>

<?php endforeach; ?>
</div>
