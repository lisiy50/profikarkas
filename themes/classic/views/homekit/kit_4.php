<div class="kit">
    <div class="kit-inner" id="kit_4">

        <div class="kit-content">

            <div class="kit-content-inner tab-content">
                <h4 class="kit-content-inner-title">ДОМОКОМПЛЕКТ СТАНДАРТ + ФАСАД</h4>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix in active" id="kit_content_41">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content51.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Внешние стены</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: 42px;">
                                <li class="kit-content-list-item">
                                    OSB
                                    <svg width="348" height="14">
                                        <rect x="0" y="7" width="348" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Деревянный каркас
                                    <svg width="320" height="14">
                                        <rect x="0" y="7" width="320" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>150 мм</span>
                                    <svg width="280" height="14">
                                        <rect x="0" y="7" width="280" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Паробарьер
                                    <svg width="252" height="14">
                                        <rect x="0" y="7" width="252" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Обрешетка
                                    <svg width="48" height="14">
                                        <rect x="0" y="7" width="48" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>50 мм</span>
                                    <svg width="176" height="14">
                                        <rect x="0" y="7" width="176" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="147" height="14">
                                        <rect x="0" y="7" width="147" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Закладные под электрику
                                    <svg width="119" height="14">
                                        <rect x="0" y="7" width="119" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>50 мм</span>
                                    <svg width="374" height="14">
                                        <rect x="0" y="7" width="374" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Отделка фасада
                                    <svg width="388" height="14">
                                        <rect x="0" y="7" width="388" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_42">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content52.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Внутренние стены</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: 0;">
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="374" height="14">
                                        <rect x="0" y="7" width="374" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Стойки каркаса
                                    <svg width="340" height="14">
                                        <rect x="0" y="7" width="340" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Минеральная вата <span>100 мм (150 мм)</span>
                                    <svg width="280" height="14">
                                        <rect x="0" y="7" width="280" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="220" height="14">
                                        <rect x="0" y="7" width="220" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Закладные под электрику
                                    <svg width="127" height="14">
                                        <rect x="0" y="7" width="127" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_43">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content43_1.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Перекрытие</div>
                            <ul class="kit-content-list list-unstyled">
                                <li class="kit-content-list-item">
                                    ОСП
                                    <svg width="236" height="14">
                                        <rect x="0" y="7" width="236" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Брус перекрытия
                                    <svg width="267" height="14">
                                        <rect x="0" y="7" width="267" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>100 мм</span>
                                    <svg width="168" height="14">
                                        <rect x="0" y="7" width="168" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Пароизоляция
                                    <svg width="142" height="14">
                                        <rect x="0" y="7" width="142" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Обрешетка
                                    <svg width="117" height="14">
                                        <rect x="0" y="7" width="117" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_44">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content54.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Кровля</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: -5px;">
                                <li class="kit-content-list-item">
                                    Битумная черепица
                                    <svg width="291" height="14">
                                        <rect x="0" y="7" width="291" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Пароизоляция
                                    <svg width="300" height="14">
                                        <rect x="0" y="7" width="300" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    ОСП
                                    <svg width="225" height="14">
                                        <rect x="0" y="7" width="225" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата
                                    <svg width="320" height="14">
                                        <rect x="0" y="7" width="320" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Подкладочный ковер
                                    <svg width="71" height="14">
                                        <rect x="0" y="7" width="71" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Капельник
                                    <svg width="60" height="14">
                                        <rect x="0" y="7" width="60" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Пароизоляция
                                    <svg width="244" height="31">
                                        <line x1="100%" y1="24" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Фермы (брус стропильной системы)
                                    <svg width="128" height="52">
                                        <line x1="100%" y1="45" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Водосточный желоб
                                    <svg width="75" height="40">
                                        <line x1="100%" y1="33" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Перфорированый софит
                                    <svg width="162" height="88">
                                        <line x1="100%" y1="81" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Лобовая доска
                                    <svg width="104" height="73">
                                        <line x1="100%" y1="66" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="kit-nav">
            <div class="kit-nav-pic">
                <img class="kit-nav-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-nav-4-lg.png">
            </div>
            <ul class="kit-nav-inner list-unstyled" role="tablist">
                <li role="presentation" class="kit-nav-item kit-nav-item-51 active">
                    <a class="kit-nav-lnk" href="#kit_content_41" aria-controls="kit_content_41" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">1</span> Внешние стены
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-52">
                    <a class="kit-nav-lnk" href="#kit_content_42" aria-controls="kit_content_42" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">2</span> Внутренние стены
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-53">
                    <a class="kit-nav-lnk" href="#kit_content_43" aria-controls="kit_content_43" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">3</span> Перекрытие
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-54">
                    <a class="kit-nav-lnk" href="#kit_content_44" aria-controls="kit_content_44" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">4</span> Кровля
                    </a>
                </li>
            </ul>
        </div>


    </div>
</div>
