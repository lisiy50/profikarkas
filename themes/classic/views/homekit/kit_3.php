<div class="kit">
    <div class="kit-inner" id="kit_3">

        <div class="kit-content">

            <div class="kit-content-inner tab-content">
                <h4 class="kit-content-inner-title">ДОМОКОМПЛЕКТ СТАНДАРТ</h4>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix in active" id="kit_content_31">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content11.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Внешние стены</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: 124px;">
                                <li class="kit-content-list-item">
                                    OSB
                                    <svg width="373" height="14">
                                        <rect x="0" y="7" width="373" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Деревянный каркас
                                    <svg width="346" height="14">
                                        <rect x="0" y="7" width="346" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>150 мм</span>
                                    <svg width="304" height="14">
                                        <rect x="0" y="7" width="304" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Паробарьер
                                    <svg width="274" height="14">
                                        <rect x="0" y="7" width="274" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Обрешетка
                                    <svg width="56" height="14">
                                        <rect x="0" y="7" width="56" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item" style="padding-top: 18px;">
                                    Утеплитель минеральная вата <span>50 мм</span>
                                    <svg width="195" height="14">
                                        <rect x="0" y="7" width="195" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="158" height="14">
                                        <rect x="0" y="7" width="158" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Закладные под электрику
                                    <svg width="130" height="14">
                                        <rect x="0" y="7" width="130" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_32">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content52.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Внутренние стены</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: 0;">
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="374" height="14">
                                        <rect x="0" y="7" width="374" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Стойки каркаса
                                    <svg width="340" height="14">
                                        <rect x="0" y="7" width="340" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Минеральная вата <span>100 мм (150 мм)</span>
                                    <svg width="280" height="14">
                                        <rect x="0" y="7" width="280" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Гипсокартон
                                    <svg width="220" height="14">
                                        <rect x="0" y="7" width="220" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Закладные под электрику
                                    <svg width="127" height="14">
                                        <rect x="0" y="7" width="127" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_33">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content43_1.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Перекрытие</div>
                            <ul class="kit-content-list list-unstyled">
                                <li class="kit-content-list-item">
                                    ОСП
                                    <svg width="236" height="14">
                                        <rect x="0" y="7" width="236" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Брус перекрытия
                                    <svg width="267" height="14">
                                        <rect x="0" y="7" width="267" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата <span>100 мм</span>
                                    <svg width="168" height="14">
                                        <rect x="0" y="7" width="168" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Пароизоляция
                                    <svg width="142" height="14">
                                        <rect x="0" y="7" width="142" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Обрешетка
                                    <svg width="117" height="14">
                                        <rect x="0" y="7" width="117" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"></circle>
                                        <circle r="3" cx="7" cy="7" fill="#888888"></circle>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="kit-content-item tab-pane fade clearfix" id="kit_content_34">
                    <div class="kit-content-pic">
                        <div class="kit-content-pic-inner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-content34_1.png" alt="" class="kit-content-img">
                        </div>
                    </div>
                    <div class="kit-content-body">
                        <div class="kit-content-body-inner">
                            <div class="kit-content-title">Кровля</div>
                            <ul class="kit-content-list list-unstyled" style="margin-bottom: 110px;">
                                <li class="kit-content-list-item">
                                    ОСП
                                    <svg width="320" height="14">
                                        <rect x="0" y="7" width="320" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Фермы (брус стропильной системы)
                                    <svg width="324" height="14">
                                        <rect x="0" y="7" width="324" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Битумная черепица
                                    <svg width="156" height="14">
                                        <rect x="0" y="7" width="156" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Утеплитель минеральная вата
                                    <svg width="304" height="14">
                                        <rect x="0" y="7" width="304" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Пароизоляция
                                    <svg width="157" height="14">
                                        <rect x="0" y="7" width="157" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Подкладочный ковер
                                    <svg width="62" height="34">
                                        <line x1="100%" y1="27" x2="7" y2="7" stroke="#888888" stroke-width="1"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Капельник
                                    <svg width="49" height="37">
                                        <line x1="100%" y1="30" x2="7" y2="7" stroke="#888888" stroke-width="1" />
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                                <li class="kit-content-list-item">
                                    Лобовая доска
                                    <svg width="78" height="14">
                                        <rect x="0" y="7" width="78" height="1" fill="#888888" style="shape-rendering:optimizeSpeed;"/>
                                        <circle r="7" cx="7" cy="7" fill="#fec22e"/>
                                        <circle r="3" cx="7" cy="7" fill="#888888"/>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="kit-nav">
            <div class="kit-nav-pic">
                <img class="kit-nav-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-nav-3-lg.png">
            </div>
            <ul class="kit-nav-inner list-unstyled" role="tablist">
                <li role="presentation" class="kit-nav-item kit-nav-item-51 active">
                    <a class="kit-nav-lnk" href="#kit_content_31" aria-controls="kit_content_31" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">1</span> Внешние стены
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-52">
                    <a class="kit-nav-lnk" href="#kit_content_32" aria-controls="kit_content_32" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">2</span> Внутренние стены
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-53">
                    <a class="kit-nav-lnk" href="#kit_content_33" aria-controls="kit_content_33" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">3</span> Перекрытие
                    </a>
                </li>
                <li role="presentation" class="kit-nav-item kit-nav-item-54">
                    <a class="kit-nav-lnk" href="#kit_content_34" aria-controls="kit_content_34" role="tab" data-toggle="tab">
                        <span class="kit-nav-num">4</span> Кровля
                    </a>
                </li>
            </ul>
        </div>


    </div>
</div>
