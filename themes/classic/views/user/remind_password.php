<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 23.07.14
 * Time: 13:53
 */

/**
 * @var $remindPassword RemindPasswordForm
 */

$formId = get_class($remindPassword).'-form';
?>
<div class="auth auth_password">

    <h3><?= t('Восстановление пароля') ?></h3>

    <div class="auth_wrap">
        <?php if(user()->hasFlash('success')):?>
            <div class="">
                <?php echo user()->getFlash('success')?>
            </div>
        <?php else:?>
            <?php $form = $this->beginWidget('BootActiveForm', array(
                'id' => $formId,
                'enableAjaxValidation'=>false,
                'enableClientValidation'=>false,
                'htmlOptions' => array(
                    'action' => 'user/remindPassword'
                ),
            ))?>


            <div class="control-group">
                <?php echo $form->textFieldRow($remindPassword, 'email', array(
                    'class' => '',
                ));?>
            </div>


            <?php echo CHtml::submitButton(t('Отправить'), array('class'=>'send_remind_password'));?>

            <?php $this->endWidget();?>

            <script type="text/javascript">
                <?php if(app()->request->isAjaxRequest):?>
                $('#<?php echo $formId?>').on('submit', function(){
                    $.fancybox.showLoading();
                    $.post('user/remindPassword', $(this).serialize(), function(html){
                        $.fancybox.hideLoading();
                        $('.fancybox-inner').html(html);
                    });
                    return false;
                });
                <?php endif;?>
            </script>
        <?php endif;?>
    </div>
</div>
