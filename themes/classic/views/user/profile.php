<?php
/**
 * @var $user User
 */
$formId = get_class($user).'-form';
?>
<div class="page_container user_profile_form">

    <div class="content">
        <a href="<?php echo url('insuranceIssue/index')?>" class="profile_tab"><?php echo t('Приобретенные полисы')?></a>
        <a href="#" class="profile_tab active"><?php echo t('Настройки профиля')?></a>
        <div class="profile">
            <?php $form = $this->beginWidget('BootActiveForm', array(
                'id' => $formId,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'htmlOptions' => array(
                    'action' => 'site/login'
                ),
            ));
            /* @var $form BootActiveForm */
            ?>

            <div class="row">
                <div class="col-sm-12">
                    <?php if(user()->hasFlash('success')):?>
                        <div class="bg-success" style="padding: 15px;">
                            <?php echo user()->getFlash('success');?>
                        </div>
                    <?php elseif(user()->hasFlash('error')):?>
                        <div class="bg-danger" style="padding: 15px;">
                            <?php echo user()->getFlash('error');?>
                        </div>
                    <?php endif;?>
                </div>
                <div class="col-sm-6">
                    <div class="profile_description">
                        <?php echo t('Изменение данных пользователя')?>
                    </div>

                    <?php echo $form->textFieldRow($user, 'name', array(
                        'class' => '',
                    ));?>
                    <?php echo $form->textFieldRow($user, 'surname', array(
                        'class' => '',
                    ));?>
                    <?php echo $form->widgetFieldRow($user, 'birthday', 'zii.widgets.jui.CJuiDatePicker', array(
                        'language' => app()->language,
                        'name'=>'birthday',
                        // additional javascript options for the date picker plugin
                        'options'=>array(
                            'yearRange' => date('Y', strtotime('-120 year')).':'.date('Y'),
                            'dateFormat'=>'yy-mm-dd',
                            'changeMonth'=>true,
                            'changeYear'=>true,
                            'htmlOptions' => array(
                                'placeholder' => t('гггг-мм-дд'),
                            ),
                        ),
                    )); ?>
                    <?php echo $form->dropDownListRow($user, 'gender', $user->getGenders(), array('prompt' => '',)); ?>
                    <?php echo $form->textFieldRow($user, 'passport_series', array(
                        'class' => '',
                    ));?>
                    <?php echo $form->textFieldRow($user, 'passport_no', array(
                        'class' => '',
                    ));?>

                </div>

                <div class="col-sm-6">
                    <div class="profile_description"><?php echo t('Данные для доставки оригиналов полисов')?></div>
                    <?php echo $form->textFieldRow($user, 'fio', array(
                        'class' => '',
                    ));?>
                    <?php echo $form->textFieldRow($user, 'address', array(
                        'class' => '',
                    ));?>

                    <div class="profile_description"><?php echo t('Дополнительные параметры')?></div>

                    <?php echo $form->textFieldRow($user, 'phone', array(
                        'class' => '',
                        'placeholder'=>'380671234567',
//                        'length'=>12,
                    )); ?>

                    <?php echo $form->checkBoxRow($user, 'subscribe', array('uncheckValue'=>User::STATUS_DISABLED))?>

                    <a class="simple_link" href="<?php echo url('user/changePassword')?>"><?php echo t('Изменить пароль')?></a>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <?php echo CHtml::submitButton(t('Сохранить'), array('class'=>'profile_save_data form_button', 'style'=>'margin: 0 auto;'));?>
                </div>
            </div>

            <?php $this->endWidget();?>
        </div>
    </div>
</div>