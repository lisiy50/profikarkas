<?php
$formId = get_class($model) . '-form';
?>

<?php if(user()->hasFlash('success')):?>

    <div class="auth">
    <div class="auth_wrap">
    <div class="register">

    <div class="auth_title"><?php echo t('Регистрация аккаунта')?></div>

    <div class="create_success">
        <?php echo user()->getFlash('success')?>
    </div>
    </div>
    </div>
    </div>
<?php else:?>

<div class="auth">
    <div class="auth_wrap">
        <div class="register">

            <h3 class="auth_title"><?php echo t('Регистрация аккаунта')?></h3>

            <div class="auth_register clearfix">
                <?php echo t('У Вас уже есть аккаунт?')?>
                <a href="<?php echo url('/site/login')?>" id="auth"><?php echo t('Вход на сайт')?></a>
            </div>

            <?php $form = $this->beginWidget('BootActiveForm', array(
                'id' => $formId,
                'enableAjaxValidation' => true,
            ));
            /**
             * @var $form BootActiveForm
             */

        //    echo $form->errorSummary($model);

            ?>

            <input type="hidden" id="g-recaptcha-response4" name="g-recaptcha-response">
            <input type="hidden" name="action" value="validate_captcha">

            <?php echo $form->textFieldRow($model, 'username')?>

            <?php echo $form->textFieldRow($model, 'fio')?>

            <?php echo $form->passwordFieldRow($model, 'password', array(
                'value' => '',
            ));

            echo $form->passwordFieldRow($model, 'rPassword', array(
                'value' => '',
            ));

            echo CHtml::submitButton(t('Зарегистрироваться'), array('class'=>'auth_btn'));

            $this->endWidget(); ?>

            <h4><?= t('Авторизация через социальные сети') ?></h4>

            <div id="uLogindc6ae8b3" data-ulogin="display=panel;fields=first_name,last_name,email,nickname,photo;lang=ru;providers=facebook,google,twitter;redirect_uri=<?php echo Yii::app()->createAbsoluteUrl('site/ulogin') ?>"></div>
            <?php cs()->registerScriptFile('//ulogin.ru/js/ulogin.js')?>

        </div>
    </div>
</div>

<script>

    <?php if(app()->request->isAjaxRequest):?>
    $('#<?php echo $formId?>').submit(function(){
        $.fancybox.showLoading();
        $.post('user/create', $(this).serialize(), function(html){
            $.fancybox.hideLoading();
            $('.fancybox-inner').html(html);
        })
        return false;
    });
    <?php endif;?>
</script>
<?php endif;?>
