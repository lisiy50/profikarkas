<?php if(Yii::app()->user->isGuest): ?>

<div class="enter">
    <a href="<?php echo Yii::app()->createUrl('site/login'); ?>"><?= t('Вход') ?></a>
    /
    <a href="<?php echo Yii::app()->createUrl('user/register'); ?>"><?= t('Регистрация') ?></a>
</div>

<?php else: ?>

<div class="enter">
    Привет, <?php echo Yii::app()->user->name; ?>!

    <a href="<?php echo Yii::app()->createUrl('user/update'); ?>"><?= t('Профиль') ?></a>
    /
    <a href="<?php echo Yii::app()->createUrl('site/logout'); ?>"><?= t('Выход') ?></a>
</div>

<?php endif; ?>
