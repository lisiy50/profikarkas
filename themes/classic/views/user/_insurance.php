<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 11/24/14
 * Time: 4:59 PM
 *
 * @var $data InsuranceIssue
 * @var $person InsuranceIssuePerson
 */
?>
<div class="row">
    <div class="col-md-4">

        <?php echo CHtml::link(CHtml::image($data->insurance->insurer->getImageUrl(), $data->insurance->insurer->name) . ' ' . $data->insurance->insurer->name, $data->insurance->insurer->web_site, array('title'=>$data->insurance->insurer->name, 'class'=>'btn', 'target'=>'_blank'))?>

        <table class="table table-hover">
            <tbody>
            <?php if($data->price):?>
            <tr>
                <td><?php echo t('Цена') ?></td>
                <td><?php echo $data->price ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->delivery_price):?>
            <tr>
                <td><?php echo t('Цена доставки') ?></td>
                <td><?php echo $data->delivery_price ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->delivery_type):?>
            <tr>
                <td><?php echo t('Тип доставки') ?></td>
                <td><?php echo $data->delivery_type ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->delivery_fio):?>
            <tr>
                <td><?php echo t('ФИО') ?></td>
                <td><?php echo $data->delivery_fio ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->delivery_address):?>
            <tr>
                <td><?php echo t('Адрес доставки') ?></td>
                <td><?php echo $data->delivery_address ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->delivery_phone):?>
            <tr>
                <td><?php echo t('Телефон') ?></td>
                <td><?php echo $data->delivery_phone ?></td>
            </tr>
            <?php endif;?>
            <?php if($data->create_time):?>
            <tr>
                <td><?php echo t('Дата покупки') ?></td>
                <td><?php echo app()->dateFormatter->formatDateTime($data->create_time) ?></td>
            </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        <div class="row">
            <?php foreach($data->persons(array('order'=>'insurer DESC')) as $person): ?>
                <div class="col-md-12">

                    <div class="panel <?php echo $person->insurer ? 'panel-danger' : 'panel-info' ?>">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $person->first_name ?> <?php echo $person->last_name ?></h3>
                        </div>
                        <div class="panel-body">
                            <?php if($person->insurer):?>

                            <?php endif;?>
                            <?php echo t('Дата рождения') ?>: <?php echo $person->birthday ?>
                            <br/>
                            <?php if($person->identific):?>
                                <?php echo t('ИИН') ?>: <?php echo $person->identific ?>
                                <br/>
                            <?php endif;?>
                            <?php if($person->passport):?>
                                <?php echo t('Серия и номер пасспорта') ?>: <?php echo $person->passport ?>
                                <br/>
                            <?php endif;?>
                            <?php if($person->email):?>
                                <?php echo t('E-mail') ?>: <?php echo $person->email ?>
                                <br/>
                            <?php endif;?>
                            <?php if($person->phone):?>
                                <?php echo t('Телефон') ?>: <?php echo $person->phone ?>
                                <br/>
                            <?php endif;?>
                        </div>
                    </div>



                </div>
            <?php endforeach;?>
        </div>

    </div>
</div>