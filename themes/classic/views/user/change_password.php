<?php
/**
 * @var $user User
 */
$formId = get_class($user).'-form';
?>
<div class="page_container">

    <div class="content user_profile_form">

        <?php if(user()->hasFlash('success')): ?>
            <div class="alert alert-success">
                <?php echo user()->getFlash('success') ?>
            </div>
        <?php endif ?>

        <div>
            <?php $form = $this->beginWidget('BootActiveForm', array(
                'id' => $formId,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'htmlOptions' => array(
                    'action' => 'site/login'
                ),
            ))?>

            <h3 class="profile_description">
                <?php echo t('Изменение пароля пользователя')?>
            </h3>

            <?php echo $form->passwordFieldRow($user, 'oldPassword', array(
                'class' => '',
                'value' => '',
            ));?>

            <?php echo $form->passwordFieldRow($user, 'password', array(
                'class' => '',
                'value' => '',
            ));?>
            <?php echo $form->passwordFieldRow($user, 'rPassword', array(
                'class' => '',
                'value' => '',
            ));?>

            <?php echo CHtml::submitButton(t('Сохранить'), array('class'=>'profile_save_data form_button'));?>

            <?php $this->endWidget();?>
        </div>
    </div>
</div>
