<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 23.07.14
 * Time: 15:49
 */

/**
 * @var $user User
 */
$formId = get_class($user).'-form';
?>

<?php if(user()->hasFlash('error')):?>
    <div class="">
        <?php echo user()->getFlash('error')?>
    </div>
<?php elseif(user()->hasFlash('success')):?>
    <div class="page_container">
        <div class="content">
            <?php echo user()->getFlash('success')?>
        </div>
    </div>
<?php else:?>
    <div class="page_container">
        <div class="content restore_password">
            <?php $form = $this->beginWidget('BootActiveForm', array(
                'id' => $formId,
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'htmlOptions' => array(
                    'action' => '/user/restorePassword'
                ),
            ))?>

            <h3><?= t('Введите новый пароль') ?></h3>

            <?php echo $form->passwordFieldRow($user, 'password', array(
                'value' => '',
            ));?>

            <?php echo $form->passwordFieldRow($user, 'rPassword', array(
                'value' => '',
            ));?>

            <?php echo CHtml::submitButton(t('Отправить'), array('class'=>'send_remind_password'));?>

            <?php $this->endWidget();?>
        </div>
    </div>
<?php endif;?>
