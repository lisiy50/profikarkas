<div class="row">
    <h3 class="span pull-left"><?= t('Результаты поиска') ?></h3>
    <div class="span pull-right">
        <?= t('Сортировать по') ?>:
        <?php echo $dataProvider->sort->link('name', t('названию')); ?> |
        <?php echo $dataProvider->sort->link('create_time', t('дате')); ?> |
        <?php echo $dataProvider->sort->link('price', t('цене')); ?>
    </div>
</div>

<?php foreach($dataProvider->data as $product):?>
    <?php $this->renderPartial('/product/_view', array('product'=>$product)); ?>
<?php endforeach; ?>
<?php if($dataProvider->itemCount==0): ?>
    <h4><?= t('Поиск не дал результатов.') ?></h4>
<?php endif; ?>


<div class="pagination">
    <?php $this->widget('BootPager', array(
         'pages'=>$dataProvider->pagination,
    )); ?>
</div>
