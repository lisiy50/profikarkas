<?php
/**
 * @var SearchController $this
 * @var Project[] $projects
 * @var Portfolio[] $portfolioItems
 */
?>

<div class="search">
    <h1><?= t('Результаты поиска') ?> "<?php echo isset($_GET['q']) ? $_GET['q'] : ''?>"</h1>

    <hr>

    <?php foreach ($projects as $project): ?>
        <div class="row item">
            <div class="col-sm-3">
                <a href="<?php echo $project->getUrl()?>">
                    <img src="<?php echo $project->getFirstImageUrl()?$project->getFirstImageUrl()->getImageUrl('small'):'';?>" class="img-responsive" alt="Проект дома <?php echo $project->symbol ?> иллюстрация 1">
                </a>
            </div>
            <div class="col-sm-9">
                <h4>
                    <a href="<?php echo $project->getUrl()?>">
						<strong><?= t('Проект') ?> <?php echo $project->getName();?></strong>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php echo $project->title; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php echo $project->title_uk; ?>
                        <?php endif; ?>
                    </a>
                </h4>

                <div class="description">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php echo strip_tags(str_replace('><', '> <', $project->description)) ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php echo strip_tags(str_replace('><', '> <', $project->description_uk)) ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    <?php endforeach; ?>

    <?php foreach ($portfolioItems as $portfolioItem):?>
        <div class="row item">
            <div class="col-sm-3">
                <a href="<?php echo $portfolioItem->getUrl()?>">
                <?php if($portfolioItem->image): ?>
                    <img src="<?php echo $portfolioItem->getImageUrl('small');?>" class="img-responsive"  alt="Проект дома <?php echo $portfolioItem->project_name ?> иллюстрация 1">
                <?php elseif($portfolioItem->firstImage): ?>
                    <img src="<?php echo $portfolioItem->firstImage->getImageUrl('small');?>" class="img-responsive"  alt="Проект дома <?php echo $portfolioItem->project_name ?> иллюстрация 1">
                <?php endif;?>
                </a>
            </div>
            <div class="col-sm-9">
                <h4>
                    <a href="<?php echo $portfolioItem->getUrl()?>">
                        <strong><?php echo $portfolioItem->project_name ?></strong>
                        <?php if (Yii::app()->language == 'ru'): ?>
                            <?php echo $portfolioItem->construction_place; ?>
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            <?php echo $portfolioItem->construction_place_uk; ?>
                        <?php endif; ?>
                        <?php echo $portfolioItem->gross_area;?>m<sup>2</sup>
                    </a>
                </h4>

                <div class="description">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php echo strip_tags(str_replace('><', '> <', $portfolioItem->description)) ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php echo strip_tags(str_replace('><', '> <', $portfolioItem->description_uk)) ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    <?php endforeach;?>
</div>


