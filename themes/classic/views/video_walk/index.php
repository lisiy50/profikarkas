<?php
/**
 * @var CActiveDataProvider $dataProvider
 */
?>

<div class="catalog clearfix" id="portfolio-catalog">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<div class="portfolio-catalog-text clearfix">

    <?php echo TextContent::get('walks-index'); ?>

</div>
