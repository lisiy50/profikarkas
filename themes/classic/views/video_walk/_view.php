<?php
/**
 * @var FrontendController $this
 * @var VideoWalk $model
 */
?>
<div class="goods_wrap">
    <?php if($model->image): ?>
    <a href="<?php echo $model->getUrl();?>" class="img_wrap" target="_blank" rel="nofollow">
        <img src="<?php echo $model->getImageUrl('default');?>" alt="<?php echo $model->name ?> Видеопрогулка 1">
    </a>
    <?php endif;?>
    <a href="<?php echo $model->getUrl();?>" class="goods_wrap_bottom clearfix" target="_blank" rel="nofollow">
        <?php if($model->projectModels): ?>
        <div class="z_name">
            <?php foreach ($model->projectModels as $projectModel) {
                $names[] = $projectModel->getName();
            }?>
            <?php echo implode(', ', $names); ?>
        </div>
        <?php endif; ?>
        <div class="goods_title">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $model->name; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php echo $model->name_uk; ?>
            <?php endif; ?>
        </div>
    </a>
</div>
