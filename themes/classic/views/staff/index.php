<div class="about_us_wrap">
    <ul class="list-unstyled list-inline about_nav">
        <li class="<?php echo (!isset($_GET['Staff']['post']))?'current':'';?>"><a href="<?php echo app()->createUrl('staff/index');?>">Все</a></li>
        <li class="<?php echo (isset($_GET['Staff']['post']) && $_GET['Staff']['post'] == 'manager')?'current':'';?>"><a href="<?php echo app()->createUrl('staff/index', array('Staff'=>array('post'=>'manager')));?>">Менеджеры</a></li>
        <li class="<?php echo (isset($_GET['Staff']['post']) && $_GET['Staff']['post'] == 'marketer')?'current':'';?>"><a href="<?php echo app()->createUrl('staff/index', array('Staff'=>array('post'=>'marketer')));?>">Маркетологи</a></li>
        <li class="<?php echo (isset($_GET['Staff']['post']) && $_GET['Staff']['post'] == 'technicians')?'current':'';?>"><a href="<?php echo app()->createUrl('staff/index', array('Staff'=>array('post'=>'technicians')));?>">Технические специалисты</a></li>
    </ul>

    <div class="slogan">
        <span>“</span>Наша миссия заключается в том, чтобы принести максимум хорошего европейского опыта коттеджного строительства в Украину.<span>“</span>
    </div>
    <div class="about_staff pull-left clearfix" style="width: 810px;">
        <?php $this->beginWidget('Masonry', array(
            'options'=>array(
                'isResizable'=>'false',
                'singleMode'=>'.staff_item',
            )
        ));?>

        <?php foreach($dataProvider->data as $staff):?>
            <div class="staff_item staff_<?php echo $staff->image_size;?>">
                <a href="#">
                    <img src="<?php echo $staff->getImageUrl($staff->image_size.'Gray');?>">
                    <img class="img_hover" src="<?php echo $staff->getImageUrl($staff->image_size);?>">
                    <div class="hover_name">
                        <?php echo $staff->name;?>
                        <span><?php echo $staff->post_text;?></span>
                    </div>
                </a>
            </div>
        <?php endforeach;?>

        <?php if(false):?>
        <div class="staff_item staff_medium">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color5.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color5.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_small">
            <a href="#">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color2.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color2.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_small">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color3.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color3.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_medium">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color6.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color6.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_large">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color1.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color1.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_small">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color4.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color4.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>

        <div class="staff_item staff_small">
            <a href="#" class="">
                <img src="<?php echo app()->theme->baseUrl;?>/pic/img_no_color7.png">
                <img class="img_hover" src="<?php echo app()->theme->baseUrl;?>/pic/img_color7.png">
                <div class="hover_name">
                    Александр Александрович
                    <span>менеджер проектов</span>
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php $this->endWidget();?>
    </div>
</div>