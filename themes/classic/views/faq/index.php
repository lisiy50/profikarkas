<div class="faq_block">
    <ul>
        <?php foreach($rootedFaq as $item):?>
            <li class="faq-item inactive">
                <a href="#" class="inactive" id="<?php echo app()->translitFormatter->formatUrl($item->name);?>"><?php echo $item->name;?></a>
                <div class="faq-text" style="display: none;">
                    <?php echo $item->content;?>
                </div>
                <?php if($item->children){
                    $this->renderPartial('items', array('items'=>$item->children));
                };?>
            </li>
        <?php endforeach;?>
    </ul>
</div>

<?php
$script = <<<JS
$('body').on('click', '.faq_block li.faq-item >a.inactive', function(){
    var liContainer = $(this).closest('li');
    var siblins = liContainer.siblings();
    siblins.removeClass('active').addClass('inactive').find('li').removeClass('active').addClass('inactive');
    siblins.find('a').removeClass('active').addClass('inactive');
    liContainer.siblings().removeClass('active').addClass('inactive').find('ul.faq-items, div.faq-text').slideUp();
    $(this).closest('li').addClass('active').removeClass('inactive');
    $(this).closest('li').find('>ul, >div.faq-text').slideDown();

    $(this).removeClass('inactive').addClass('active');
    return false;
});
$('body').on('click', '.faq_block li.faq-item >a.active', function(){
    var liContainer = $(this).closest('li');
    var siblins = liContainer.siblings();
    liContainer.removeClass('inactive').addClass('active').find('ul.faq-items, div.faq-text').slideUp();
    $(this).closest('li').addClass('inactive').removeClass('active');

    $(this).removeClass('active').addClass('inactive');
    liContainer.find('a').removeClass('active').addClass('inactive');
    liContainer.find('li').removeClass('active').addClass('inactive');
    return false;
});

$(document.location.hash).parents('li').find('>a').click();
JS;
cs()->registerScript('faq-index', $script);
?>
