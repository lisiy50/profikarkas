<ul style="display: none;" class="faq-items">
    <?php foreach($items as $item):?>
        <li class="faq-item inactive">
            <a href="#" class="inactive" id="<?php echo app()->translitFormatter->formatUrl($item->name);?>"><?php echo $item->name;?></a>
            <div class="faq-text" style="display: none;">
                <?php echo $item->content;?>
            </div>
            <?php if($item->children){
                $this->renderPartial('items', array('items'=>$item->children));
            };?>
        </li>
    <?php endforeach;?>
</ul>