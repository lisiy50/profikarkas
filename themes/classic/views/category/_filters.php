<?php /*
  *
  * $category - категория
  * $sort - обьект для генерации сортировки
  *
  * */ ?>
<form action="" method="get">

<div class="filters">
    <?php foreach($filters as $filter): ?>
        <?php echo $filter->createField(); ?>
    <?php endforeach; ?>

    <button type="submit" class="btn"><?= t('ГОТОВО') ?></button>

</div>

    <div class="sort">
        <?php echo $sort->link('name', t('По названию')); ?> |
        <?php echo $sort->link('price', t('По цене')); ?> |
        <?php echo $sort->link('priority', t('По популярности')); ?>
    </div>

</form>
