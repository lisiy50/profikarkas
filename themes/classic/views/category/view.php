<?php
$this->layout='//layouts/column1';
 /*
  *
  * $category - категория
  * $dataProvider - товары текущей категории
  *
  * */ ?>

<div class="content">
    <div class="content_l">
        <div class="content_l_head"><?= t('КАТЕГОРИИ') ?></div>
        <?php $this->widget('Menu', array(
            'items'=>Category::model()->in_menu()->rooted()->findAll(),
            'nesting'=>2,
            'criteria'=>array('scopes'=>array('in_menu')),
        )); ?>
        <div class="content_l_head"><?= t('ПРОИЗВОДИТЕЛИ') ?></div>
        <?php $this->widget('Menu', array(
            'items'=>Brand::model()->findAll(),
            'nesting'=>1,
        )); ?>
    </div>
    <div class="content_r">

        <?php $this->widget('BootBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?>

        <?php
        if($category->hasChildren) {
            $this->renderPartial('_children', array(
                'children'=>$category->children,
                'category'=>$category,
            ));
        } elseif($category->hasProducts) {
            $this->renderPartial('_products', array(
                'dataProvider'=>$dataProvider,
                'category'=>$category,
            ));
        } else { ?>
            <h3>Категория пуста.</h3>
        <?php } ?>

    </div>
</div>


