<?php /*
  *
  * $category - категория
  * $dataProvider - товары текущей категории
  *
  * */ ?>

<?php $this->renderPartial('_filters', array(
    'sort'=>$dataProvider->sort,
    'filters'=>$category->filters,
    'dataProvider'=>$dataProvider,
)); ?>

<div class="products_container">
    <?php foreach($dataProvider->data as $product): ?>
    <?php $this->renderPartial('/product/_view', array('product'=>$product)); ?>
    <?php endforeach; ?>
</div>

<div style="clear: both;"></div>
    <div class="pagination">
        <?php $this->widget('BootPager', array(
        'pages'=>$dataProvider->pagination,
    )); ?>
    </div>

