<?php /*
  *
  * $category - категория
  * $children - подкатегории текущей категории
  *
  * */ ?>
<div class="products_container">

<?php foreach($children as $i=>$category): ?>
<div class="category">
    <div class="category_bg">
        <div class="category_head"><a href="<?php echo $category->url; ?>"><?php echo $category->name; ?></a></div>
        <?php if($category->image): ?>
        <div class="category_pic"><a href="<?php echo $category->url; ?>"><img src="<?php echo $category->getImageUrl('small'); ?>"></a></div>
        <?php endif; ?>
    </div>
    <?php if($category->hasChildren): ?>
    <div class="category_text">
        <?php foreach($category->children as $child): ?>
        <a href="<?php echo $child->url; ?>"><?php echo $child->name; ?></a>,
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>

<?php if($i%3==2): ?>
    <b class="clearb"></b>
<?php endif; ?>

<?php endforeach; ?>

</div>