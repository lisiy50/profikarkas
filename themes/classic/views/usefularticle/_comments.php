<?php
/**
 * @var Controller $this
 * @var UsefulArticle $model
 */
?>

<div id="comment-container">
    <?php $this->widget('CommentsWidget', array('owner' => $model, 'comment' => new Comment()));?>
</div>

