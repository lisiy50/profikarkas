<?php if(Yii::app()->compare->isEmpty): ?>

        <h3><?= t('Товаров нет в сравнении') ?></h3>

<?php else: ?>

        <a href="<?php echo $this->createUrl('compare/index'); ?>">
            <?= t('В сравнении') ?> <?php echo Yii::t('app', '{n} товар|{n} товара|{n} товаров|{n} товар', Yii::app()->compare->count); ?>
        </a>

<?php endif; ?>
