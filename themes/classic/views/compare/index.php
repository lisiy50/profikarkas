<?php /*
  *
  * $products - товары что участвуют в сравнении
  *
  * */ ?>

<h3><?= t('Сравнение товаров') ?></h3>

<?php if(Yii::app()->compare->isEmpty): ?>
    <strong><?= t('Товаров нет в сравнении') ?></strong>
<?php else: ?>

    <?php foreach(Yii::app()->compare->getProductsGroupByCategories() as $pack): list($category, $products)=$pack; ?>

        <?php if(Yii::app()->compare->getCategoriesCount()>1): ?>
            <strong><?= t('Сравнение товаров категории') ?> <?php echo $category->name; ?></strong>
        <?php endif; ?>

    <div style="width: 700px; overflow: auto;">
        <table class="table table-striped table-condensed" width="<?php echo 200*count($products)+150; ?>">
            <thead>
                <tr>
                    <th></th>
                    <?php foreach($products as $product): ?>
                    <th width="200"><a href="<?php echo $product->url; ?>"><?php echo $product->brand->name; ?> <?php echo $product->name; ?></a></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <?php foreach($products as $product): ?>
                    <td><img src="<?php echo $product->getImageUrl('small'); ?>"></td>
                    <?php endforeach; ?>
                </tr>

                <tr>
                    <td>Цена</td>
                    <?php foreach($products as $product): ?>
                    <td><?php echo Yii::app()->priceFormatter->format($product->price); ?></td>
                    <?php endforeach; ?>
                </tr>

                <tr>
                    <td>Кратко</td>
                    <?php foreach($products as $product): ?>
                    <td><?php echo $product->summary; ?></td>
                    <?php endforeach; ?>
                </tr>

                <?php foreach($category->getFeatures(Feature::IN_COMPARE) as $feature): ?>

                <tr>
                    <td><?php echo $feature->name; ?></td>
                    <?php foreach($products as $product): ?>
                    <td><?php echo $product->getHasFeatureValue($feature->id)?$product->getFeatureValue($feature->id).' '.$feature->unit:'-'; ?></td>
                    <?php endforeach; ?>
                </tr>

                <?php endforeach; ?>

                <tr>
                    <td></td>
                    <?php foreach($products as $product): ?>
                    <td><a href="<?php echo $this->createUrl('remove', array('id'=>$product->id)); ?>"><?= t('Убрать из сравнения') ?></a></td>
                    <?php endforeach; ?>
                </tr>

            </tbody>
        </table>
    </div>
    <?php endforeach; ?>
<?php endif; ?>
