<?php if(!isset($options)) $options = array();?>

<div class="social_news">
    <ul class="list-unstyled list-inline social_group_switcher">
        <li class="current">
            <a href="#group-fb" class="social_group_switcher_item">
                <img src="<?php echo app()->theme->baseUrl;?>/img/social_fb.png">
            </a>
        </li>
        <li>
            <a href="#group-inst" class="social_group_switcher_item">
                <img src="<?php echo app()->theme->baseUrl;?>/img/social_inst.png">
            </a>
        </li>
    </ul>

    <div class="group_item" id="group-fb">
        <?php
        isset($options['width'])?$width=$options['width']:$width=265;
        isset($options['height'])?$height=$options['height']:$height=400;
        $colorscheme='light';
        $show_faces='true';
        $header='true';
        $stream='false';
        $show_border='false';
        $group_id='Profikarkas';

        $htmlOptions = array(
            'class'=>'fb-like-box',
            'data-href'=>'https://www.facebook.com/'.$group_id,
            'data-width'=>$width,
            'data-height'=>$height,
            'data-colorscheme'=>$colorscheme,
            'data-show-faces'=>$show_faces,
            'data-header'=>$header,
            'data-stream'=>$stream,
            'data-show-border'=>$show_border,
            'data-show-posts'=>'false',
        );
        echo CHtml::tag('div', $htmlOptions, '', true);
        ?>
    </div>

    <div class="group_item" id="group-inst" style="">

        <iframe src="//widget.stapico.ru/?q=profikarkas_ua&s=100&w=3&h=2&b=0&p=5&effect=0" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:348px; height: 355px" ></iframe>

    </div>

</div>

<?php
$script = <<<JS
$('#group-fb').show();
$('.social_group_switcher_item').click(function(){
    $('.social_group_switcher li').removeClass('current');
    $('.social_news .group_item').hide();

    $(this).closest('li').addClass('current');
    $('div'+$(this).attr('href')).show();

    return false;
})
JS;
cs()->registerScript('lauouts-social_groups', $script);
?>
