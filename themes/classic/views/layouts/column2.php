<?php $this->beginContent('//layouts/main'); ?>
    <div class="content_left pull-left">
        <ul class="list-unstyled content_nav">
            <?php foreach(MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->menu_id)) as $item):?>
                <?php
                $currentClass = '';
                if( !(app()->request->url=='/' || app()->request->url=='/profikarkas/') && strstr(app()->request->url, $item->url)!==false){
                    $currentClass = ' current';
                }
                $css = '';
                if ($item->id == 116) {
                    $css = 'color: #ba3133;font-weight: bolder;';
                }
                if ($item->id == 58 || $item->id == 114) {
                    continue;
                }
                ?>
                <li data-id="<?php echo $item->id;?>" data-parent_id="<?php echo $item->parent_id;?>" class="id<?php echo $item->id; echo $currentClass;?>">
                    <a data-id="<?php echo $item->id;?>" style="<?php echo $css; ?>" href="<?php echo $item->url;?>" <?php echo $item->link_attributes; ?>><?php echo $item->name;?></a>
                </li>
            <?php endforeach;?>
        </ul>
        <?php //$this->widget('Menu', array('items'=>MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->menu_id)), 'htmlOptions'=>array('class'=>'list-unstyled content_nav')));?>
    </div>
    <div class="content_right pull-left">
        <?php echo $content;?>
    </div>
<?php $this->endContent(); ?>
