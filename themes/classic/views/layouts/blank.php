<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="ru">
    <title><?php echo Yii::app()->seo->metaTitle; ?></title>
    <meta name="description" content="<?php echo Yii::app()->seo->metaDescription; ?>">
    <meta name="keywords" content="<?php echo Yii::app()->seo->metaKeywords; ?>">
    <link rel="icon" href="<?php echo app()->baseUrl;?>/themes/classic/img/profikarkas.ico" type="image/x-icon">

    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.less" rel="stylesheet/less" type="text/css">
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/less.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/accounting.js" type="text/javascript"></script>

    <?php echo $this->additionalHeaderTags ?>
</head>
<body>

<?php echo $content;?>

</body>
</html>