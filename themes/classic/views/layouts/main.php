<!DOCTYPE html>
<html lang="ru-UA">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="ru">
    <title><?php echo Yii::app()->seo->metaTitle; ?></title>
    <meta name="description" content="<?php echo Yii::app()->seo->metaDescription; ?>">
    <meta name="keywords" content="<?php echo Yii::app()->seo->metaKeywords; ?>">

    <meta property="og:image" content="https://profikarkas.com.ua/homepage/images/metaimage.jpg" />
    <meta property="og:image:secure_url" content="https://profikarkas.com.ua/homepage/images/metaimage.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="164" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="<?php echo app()->baseUrl;?>/themes/classic/img/profikarkas.ico" type="image/x-icon">

<!--    --><?php //cs()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/accounting.js') ?>
    <?php echo $this->additionalHeaderTags ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->assetsUrl ?>/css/styles1.min.css" media="nope!" onload="this.media='all'"/>
</head>
<body>

<!-- Google Tag Manager -->

<noscript><iframe src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="//www.googletagmanager.com/ns.html?id=GTM-MBGQ5T"

                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script async defer>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

    })(window,document,'script','dataLayer','GTM-MBGQ5T');</script>

<!-- End Google Tag Manager -->

<div id="fb-root"></div>
<script async defer>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=567891339972064";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'ru', parsetags: 'explicit'}
</script>

<div class="contact-data">
    <span>&nbsp;&nbsp;&nbsp;web: <strong><a href="//profikarkas.com.ua">profikarkas.com.ua</a></strong></span>
    <span>&nbsp;&nbsp;&nbsp;email: <strong><?php echo config('contact_email') ?></strong></span>
    <span>&nbsp;&nbsp;&nbsp;tel: <strong><?php echo config('contact_phone') ?></strong></span>
</div>

<div class="header">
    <div class="container">
        <div class="header_top clearfix">
            <div class="header_switch_lang pull-left">
                <span><b>UA</b></span>
                <div class="header_switch_lang_inner">
                    <a href="<?php echo url('/en');?>"><b>EN</b></a>
                    <a href="<?php echo url('/de');?>"><b>DE</b></a>
                    <a href="<?php echo url('/fr');?>"><b>FR</b></a>
                </div>
            </div>
            <div class="header_tell pull-left">
                <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                    <span><a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a></span>
                <?php endforeach;?>
            </div>
            <div class="header_email pull-left">
                <a href="mailto:<?php echo config('contact_email');?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'E-mail', {nonInteraction: true});"><?php echo config('contact_email');?></a>
            </div>
            <div class="header_callback pull-left">
                <a href="<?php echo url('site/recall');?>" id="recall" class="various fancybox.ajax" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Call', {nonInteraction: true});"><?= t('Заказать обратный звонок') ?></a>
            </div>

            <div class="user-menu pull-right">
                <span data-info="<?php echo user()->id; ?>"><?= t('Вход') ?></span>
                <?php
                    $items = [];
                    if (user()->isGuest) {
                        $items[] = array('label' => t('Войти'), 'url' => array('/site/login'),);
                        $items[] = array('label' => t('Зарегистрироваться'), 'url' => array('/user/create'),);
                    } else {
                        $items[] = array('label' => t('Изменить пароль'), 'url' => array('/user/changePassword'),);
                        $items[] = array('label' => t('Выйти'), 'url' => array('/site/logout'),);
                    }
                    $this->widget('BootMenu', array(
                        'items' => $items,
                    ));
                ?>
            </div>

            <div class="form pull-right">
                <div class="form-group">
					<form action="<?php echo Yii::app()->createUrl('/search/index')?>">
						<input type="text" name="q" value="<?php  echo isset($_GET['q']) ? $_GET['q'] : ''; ?>" placeholder="<?= t('поиск проекта по названию') ?>">
						<button type="submit" style="display: none;"></button>
					</form>
                </div>
            </div>
        </div>

        <div class="header_center clearfix">
            <span>
                <a href="<?php echo app()->homeUrl;?>" class="logo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png"></a>
            </span>
        </div>

        <ul class="list-inline list-unstyled header_nav clearfix">
            <?php foreach(MenuItem::model()->findAll('parent_id=1') as $menuItem):?>
                <?php
                $currentClass = '';
                if( !(app()->request->url=='/' || app()->request->url=='/profikarkas/') && strstr($menuItem->url, app()->request->url)!==false){
                    $currentClass = ' current';
                }
                ?>
                <li data-id="<?php echo $menuItem->id;?>" class="id<?php echo $menuItem->id; echo $currentClass;?>">
                    <a data-id="<?php echo $menuItem->id;?>" href="<?php echo $menuItem->url;?>"><?php echo $menuItem->name;?></a>
                </li>
            <?php endforeach;?>
        </ul>

    </div>
</div>
<div class="content">
    <div class="container">
        <div class="content_wrap clearfix" id="<?php echo Yii::app()->controller->id.'-'.Yii::app()->controller->action->id ?>-page">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_wrap">
            <div class="footer__data">
                <div class="footer__data-item">
                    <div class="footer__data-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Реквизиты
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Реквізити
                        <?php endif; ?>
                    </div>
                    <div class="footer__data-text">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Общество с ограниченной ответственностью «ПРОФІБУДКОМПЛЕКТ» // Юридический адрес: 02000,г. Киев,
                            пр-т. Григоренка, 38-А, кв. 157 // ЕГРПОУ 38657557 // Р/р UA343510050000026001610250000 //
                            Банк: ПАО "УкрСиббанк" г. Харьков // МФО 351005
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Товариство з обмеженою відповідальністю «ПРОФІБУДКОМПЛЕКТ» // Юридична адреса: 02000, м. Київ,
                            пр-т. Григоренка, 38-А, кв. 157 // ЄДРПОУ 38657557 // Р/р UA343510050000026001610250000 //
                            Банк: ПАТ "УкрСиббанк" м. Харків // МФО 351005
                        <?php endif; ?>
                    </div>
                </div>
                <div class="footer__data-item">
                    <div class="footer__data-title">
                        <?php if (Yii::app()->language == 'ru'): ?>
                            Наши партнеры
                        <?php elseif (Yii::app()->language == 'uk'): ?>
                            Наші партнери
                        <?php endif; ?>
                    </div>
                    <div class="footer__data-gallery">
                        <div class="footer__data-gallery-inner" id="partnerGallery">
                            <div class="footer__data-gallery-item">
                                <a href="https://zs6.com.ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-zelena-skelia.png"
                                         alt="Zelena Skelia logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://pragma.ua/ru/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-pragma.png"
                                         alt="Pragma logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="http://aqua-basis.com.ua/ru/home/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-aqua-basis.png"
                                         alt="Aqua Basis logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://www.kantal.com.ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-kantal.jpg"
                                         alt="Kantal logo"
                                         class="footer__data-img">
                                </a>
                            </div>

                            <div class="footer__data-gallery-item">
                                <a href="https://www.hormann.ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-hormann.png"
                                         alt="Hormann logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://www.schiedel.com/ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-schiedel.png"
                                         alt="Schiedel logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://www.isover.ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-isover.png"
                                         alt="Isover logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://www.korsa.ua/ " class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-korsa.png"
                                         alt="Korsa logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                            <div class="footer__data-gallery-item">
                                <a href="https://www.velux.ua/" class="footer__data-link" target="_blank">
                                    <img src="<?= Yii::app()->theme->baseUrl ?>/img/logo-velux.png"
                                         alt="Velux logo"
                                         class="footer__data-img">
                                </a>
                            </div>
                        </div>
                        <div class="footer__data-arrow footer__data-arrow_prev"></div>
                        <div class="footer__data-arrow footer__data-arrow_next"></div>
                    </div>
                </div>
            </div>
            <?php if (Yii::app()->language != 'en'): ?>
            <table>
                <tr>
                    <td>
                        <div class="footer_content_wrap">
                            <div class="footer_title"><?= t('Контакты') ?></div>
                            <div class="contacts_block">
                                <div class="clearfix contacts">
                                    <div class="pull-left contacts_name"><?= t('Адрес') ?>: </div>
                                    <div class="pull-left contacts_contacts">
                                        <?= config('contact_address') ?>
                                    </div>
                                </div>
                                <div class="clearfix contacts">
                                    <div class="pull-left contacts_name"><?= t('Тел.') ?>: </div>
                                    <div class="pull-left contacts_contacts footer_phones">
                                        <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                                            <a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a><br/>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <div class="clearfix contacts">
                                    <div class="pull-left contacts_name">E-mail:</div>
                                    <div class="pull-left contacts_contacts">
                                        <a href="mailto:<?php echo config('contact_email');?>"
                                           onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'E-mail', {nonInteraction: true});">
                                            <?php echo config('contact_email');?>
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix contacts">
                                    <div class="pull-left contacts_name">Web:</div>
                                    <div class="pull-left contacts_contacts">
                                        <a href="//profikarkas.com.ua">profikarkas.com.ua</a>
                                    </div>
                                </div>
                                <div class="social_block_title"><?= t('Мы в соцсетях') ?>:</div>
                                <ul class="list-inline list-unstyled social_block">
                                    <li><a href="https://www.facebook.com/Profikarkas?ref=hl" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/social_fb.png"></a></li>
                                    <li><a href="https://www.instagram.com/profikarkas_ua/" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/social_inst.png"></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCCI9ZJ_tUFptZFpX-JBnoOA" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/social_yt.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </td>

                    <?php foreach(MenuItem::model()->findAll('parent_id = 2') as $menuItem):?>
                    <td>
                        <div  class="footer_content_wrap">
                            <a href="<?php echo $menuItem->url;?>" class="footer_title"><?php echo $menuItem->name;?></a>
                            <ul class="footer_nav list-unstyled">
                                <?php foreach($menuItem->children as $child):?>
                                    <li>
                                        <?php if($child->children): ?>
                                            <a class="hasChildren"><?php echo $child->name;?></a>
                                            <br>
                                            <?php $subMenuHtmlArr = array();?>
                                            <?php
                                            foreach ($child->children as $child1) {
                                                $subMenuHtmlArr[] = '<a href="'.$child1->url.'">'.$child1->name.'</a>';
                                            }
                                            echo implode(', ', $subMenuHtmlArr);
                                            ?>
                                        <?php else: ?>
                                            <a href="<?php echo $child->url;?>"><?php echo $child->name;?></a>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </td>
                    <?php endforeach;?>
                </tr>
            </table>
            <?php endif; ?>
        </div>
        <div class="footer_copyright">© <?php echo date('Y');?>. ProfiKarkas. All rights reserved</div>
    </div>
</div>

<div class="contact-data">
    <span>&nbsp;&nbsp;&nbsp;web: <strong><a href="//profikarkas.com.ua">profikarkas.com.ua</a></strong></span>
    <span>&nbsp;&nbsp;&nbsp;email: <strong><?php echo config('contact_email') ?></strong></span>
    <span>&nbsp;&nbsp;&nbsp;tel: <strong><?php echo config('contact_phone') ?></strong></span>
</div>

<script src="https://www.google.com/recaptcha/api.js?render=6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ" async defer></script>

<?php

package('fancyboxHelpers');

$script = <<<JS

function initCaptcha() {
    grecaptcha.ready(function() {
        grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
            document.getElementById('g-recaptcha-response4').value=token;
        });
    });
}

window.setTimeout('initCaptcha()', 4000);

$('#recall').fancybox({
    closeBtn: false,
    autoResize: true,
    autoCenter: true
});

if( $(window).width() <= 1024 ) {
    $(".user-menu").on("click", function() {
      $(".user-menu .nav").slideToggle();
    });
    if ($(window).width() > 767) {
         $(".header_switch_lang").on("click", function() {
         $(".header_switch_lang_inner").slideToggle();
    });   
    }
} else {
    $(".user-menu").hover(function() {
      $(".user-menu .nav").slideToggle();
    });

    $(".header_switch_lang").hover(function() {
      $(".header_switch_lang_inner").slideToggle();
    });
    
    $("#partnerGallery").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 1000,
        cssEase: "cubic-bezier(.165, .84, .44, 1)",
        infinite: true,
        autoplaySpeed: 6000,
        arrows: true,
        nextArrow: ".footer__data-arrow_next",
        prevArrow: ".footer__data-arrow_prev",
        dots: false,
    });
}

JS;

cs()->registerScript('layouts-main-racall', $script);

?>

<script type="text/javascript" async defer>
    var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"5cf15ba4bd2c6baf98d70582526cdb6035d50baf84ca45317d29b6cb07880619", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
</script>

</body>

<style>
    .zsiq_theme1 .zsiq_flt_rel {
        background-color: #FFD300 !important;
    }
    #zsiq_maintitle {
        position: relative;
        height: 45px;
        color: transparent;
    }
    #zsiq_maintitle:after {
        position: absolute;
        top: 12px;
        bottom: 0;
        left: 0;
        right: 0;
        font-size: 14px;
        color: #000000;
        line-height: 15px;
        content: 'Отвечаем на Ваши вопросы он-лайн';
    }
    #titlediv > div[title*='Оставьте ваше сообщение']:after {
        top: -3px;
        white-space: normal;
        content: 'Оставьте ваше сообщение, мы ответим в рабочее время на e-mail при первой же возможности.';
    }
    #zsiq_byline {
        display: none;
    }
    .zsiq_theme1 div.zsiq_cnt {
        left: -280px;
        width: 270px;
    }
    @media screen and (max-width: 414px) {
        .zsiq_theme1 div.zsiq_cnt {
            left: -225px;
            width: 215px;
        }
    }
</style>

</html>
