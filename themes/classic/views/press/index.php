<div class="catalog clearfix" id="portfolio-catalog">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<?php
$script = <<<JS
$('#portfolio-catalog').on('click', '.portfolio-pagination .next a', function(){
    var lastCrossLink = {}
    var id = ($('.portfolio_cross_link:last').data('id'));
    lastCrossLink.lastCrossLink = id;
    $.get($(this).attr('href'), lastCrossLink, function(html){
        $('#portfolio-pager').remove();
        $('#portfolio-catalog').append(html);
        addCrossLinksSpace();
    });
    return false;
});
JS;
cs()->registerScript('press-index', $script);
?>
