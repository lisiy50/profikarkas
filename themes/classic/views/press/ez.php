<?php
package('fancyboxHelpers');
package('elevateZoom3');
?>
<?php
$detect = new MobileDetect();
$isMobile = $detect->isMobile();
?>

<div class="wide-page">

    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
//    'homeLink' => array('label' => 'Домой', 'url' => Yii::app()->homeUrl),
        'links' => $this->breadcrumbs,
    )); ?>

    <div class="press_text">
        <h1 class="name"><?php echo $model->title;?></h1>
        <?php echo $model->description;?>
    </div>


<?php foreach ($model->images as $image): ?>
    <img style="border:1px solid #e8e8e6;" id="zoom_03" src="<?php echo $image->getImageUrl('large');?>"
         data-zoom-image="<?php echo $image->getImageUrl('original');?>"
         width="968"  />
<?php break; endforeach ?>

<div class="videos">
    <?php if($model->getVideos()):?>
        <div class="video_block pull-left" style="width: <?php $model->getCountVideos() * 105;?>px;height: 60px;">
            <?php foreach($model->getVideos() as $video):?>
                <a href="<?php echo $video['videoLink'];?>" class="press-video various fancybox.iframe">
                    <?php echo $video['miniImageHtml'];?>
                    <img src="<?php echo app()->theme->baseUrl;?>/img/carusel_video_img.png" class="video_play_img">
                </a>
            <?php endforeach;?>
        </div>
    <?php endif;?>
</div>

<div id="gallery_01" style="width:500px;float:left; ">

<?php foreach ($model->images as $image): ?>
    <a  href="#" class="elevatezoom-gallery active" data-update="" data-image="<?php echo $image->getImageUrl('large');?>"
        data-zoom-image="<?php echo $image->getImageUrl('original');?>">
        <img src="<?php echo $image->getImageUrl('mini');?>" width="102"  /></a>
<?php endforeach ?>

</div>

<div class="clearfix link_wrap"><a href="<?php echo url('press/index');?>" class="link"><?= t('Вернуться') ?></a></div>

</div>

<?php
$script = <<<JS

$('.press-video').fancybox();

    $(document).ready(function () {
        $("#zoom_03").elevateZoom({
        zoomType : "inner",
        gallery:'gallery_01',
        cursor: 'pointer',
        galleryActiveClass: "active",
        imageCrossfade: true,
        loadingIcon: "http://www.elevateweb.co.uk/spinner.gif"
        });

        $("#zoom_03").bind("click", function(e) {
            var ez =   $('#zoom_03').data('elevateZoom');
            ez.closeAll(); //NEW: This function force hides the lens, tint and window

            var gl = [];
            ez.getGalleryList().forEach(function(item, i, arr) {
                    gl[i] = {
                        href: item.href,
                        prevEffect		: 'none',
                        nextEffect		: 'none',
                        closeBtn		: false,
                        mouseWheel: false,
                        helpers		: {
                            buttons	: {}
                        }
                    }
            });
            $.fancybox(gl);
            console.log(gl);
            return false;
        });

    });

JS;

cs()->registerScript('press-view', $script);
?>
