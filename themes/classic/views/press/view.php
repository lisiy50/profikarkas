<?php
$detect = new MobileDetect();
$isMobile = $detect->isMobile();
?>

<div class="wide-page">

    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
//    'homeLink' => array('label' => 'Домой', 'url' => Yii::app()->homeUrl),
        'links' => $this->breadcrumbs,
    )); ?>

    <div class="press_text">
        <h1 class="name"><?php echo $model->title;?></h1>
        <?php echo $model->description;?>
    </div>

    <?php if($model->getVideos()):?>
        <div class="video_block">
            <?php foreach($model->getVideos() as $video):?>
                <a href="<?php echo $video['videoLink'];?>" class="press-video various fancybox.iframe">
                    <?php echo $video['miniImageHtml'];?>
                    <img src="<?php echo app()->theme->baseUrl;?>/img/carusel_video_img.png" class="video_play_img">
                </a>
            <?php endforeach;?>
        </div>
    <?php endif;?>

    <div class="clearfix"></div>
    <br>

    <div class="openseadragon_wrapper">
        <div id="openseadragon1" style="width: 1000px; height: 800px;"></div>
    </div>

    <div class="clearfix link_wrap"><a href="<?php echo url('press/index');?>" class="link"><?= t('Вернуться') ?></a></div>

</div>


<?php
package('openseadragon');
package('fancybox');

$openseadragonAssetesPath = cs()->getPackageBaseUrl('openseadragon');

$titleSources = array();
foreach ($model->images as $image) {
    $titleSources[] = $image->getImageUrl('original').'.dzi';
}
$titleSources = '"'.implode('","', $titleSources).'"';

$script = <<<JS
var viewer = OpenSeadragon({
        id: "openseadragon1",
        showNavigator: true,
        autoHideControls:  false,
        prefixUrl:       "{$openseadragonAssetesPath}/images/",
        tileSources:     [
            {$titleSources}
        ],
        sequenceMode: true,
        //preserveViewport: true,
        mouseNavEnabled: false

    });
    $('.press-video').fancybox();

    $("#openseadragon1").click(function(){
        viewer.setMouseNavEnabled(true);
    });
    $("body").click(function(event){
        if ($(event.target).closest("#openseadragon1").length === 0) {
            viewer.setMouseNavEnabled(false);
        }
    });
JS;

cs()->registerScript('press-view', $script);
?>
