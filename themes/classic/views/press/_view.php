<?php
/**
 * @var $model UsefulArticle
 */
?>

<div class="goods_wrap goods_press_wrap <?php echo (!$model->firstImage&&$model->getCountVideos())?'europe_video':'';?>">
    <a href="<?php echo $model->getPressUrl();?>" class="img_wrap">
        <?php if($model->image):?>
            <img src="<?php echo $model->getImageUrl('small');?>">
        <?php elseif($model->getCountVideos()):?>
            <?php echo $model->videos[0]['largeImageHtml'];?>
            <img class="zoom" src="<?php echo app()->theme->baseUrl;?>/img/europe_play.png">
            <img class="zoom hover_zoom" src="<?php echo app()->theme->baseUrl;?>/img/europe_play2.png">
        <?php endif;?>
    </a>
    <a href="<?php echo $model->getPressUrl();?>" class="goods_wrap_bottom clearfix">
        <div class="goods_title"><?php echo $model->title;?></div>
    </a>
</div>