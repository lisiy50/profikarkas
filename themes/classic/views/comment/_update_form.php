<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 9/15/16
 * Time: 4:40 PM
 *
 * @var Comment $model
 */
?>

<form action="<?php echo url('/comment/update', array('id' => $model->id)) ?>" method="post" class="comment-update-form">

    <div class="comment-update-form-label text-left"><?= t('Редактировать комментарий') ?></div>

    <?php echo CHtml::hiddenField('id', $model->id) ?>

    <?php echo CHtml::textArea('body', $model->body, array('style'=>'width:800px;height:300px;resize:none;')) ?>

    <div class="comment-update-form-btn text-right">
        <button type="submit">Отправить</button>
    </div>

</form>

<?php
$js = <<<JS
$('.comment-update-form').submit(function() {
    $.post($(this).attr('action'), $(this).serialize(), function() {
      $('.comment_view#c{$model->id} .comments-text').html($('.comment-update-form textarea').val());
      $.fancybox.close();
    })
    return false;
});
JS;
?>
<script type="text/javascript">
    <?php echo $js?>
</script>
