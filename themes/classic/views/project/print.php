<?php
/**
 * @var $project Project
 * @var WorkScopePrice $workScopePrice
 */
$workScopePrice = WorkScopePrice::model()->findByAttributes(array('home_kit_id' => 1, 'work_scope_id' => 1, 'project_id' => $project->id));
?>

<div class="print_wrap">
    <div class="home_projects_center clearfix">
    <div class="left">
        <div class="z10img"><?php echo $project->getName();?></div>
        <div id="project-slider">
            <div class="slider clearfix" id="project-carousel">
                <?php foreach($project->getImagesByType('visualization', 8) as $k=>$image):?>
                    <a href="#" class="slide" id="i<?php echo $k;?>">
                        <img src="<?php echo $image->getImageUrl('large');?>">
                    </a>
                <?php break; endforeach;?>
            </div>
            <div class="project_carusel clearfix">
                <div class="pull-left carusel" style="width: 718px;height: 50px" id="project-thumbs">
                    <?php foreach($project->getImagesByType('visualization', 8) as $k=>$image):?>
                        <a href="#i<?php echo $k;?>"><img src="<?php echo $image->getImageUrl('thumb');?>"></a>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <div id="project-videos" style="display: none;">
            <div class="slider" id="project-video-carousel">
                <?php foreach($project->getVideos() as $k=>$video):?>
                    <div class="item" style="display:<?php echo $k==0?'block':'none';?>;" id="v<?php echo $k;?>">
                        <?php echo $video->getVideoInsertHtml();?>
                    </div>
                <?php endforeach;?>
            </div>
            <div class="project_carusel clearfix">
                <div class="pull-left carusel" style="width: 718px;height: 50px" id="project-video-thumbs">
                    <?php foreach($project->getVideos() as $k=>$video):?>
                        <a href="#v<?php echo $k;?>"><?php echo $video->getVideoThumb();?></a>
                    <?php endforeach;?>
                </div>
            </div>
        </div>

    </div>
</div>
    <div class="home_projects_bottom">

    <div id="project-tab1" class="project-tab main_info">
        <div class="projects_info clearfix">
            <div class="left pull-left">
                <?php
                foreach($project->getImagesByType('layout') as $layout):
                    if(strpos($layout->getImageUrl(), 'dzialka')) continue;
                    ?>
                    <div class="plan_wrap">
                        <!--                <div class="plan_name">Первый этаж  56,5 m<sup>2</sup></div>-->
                        <img src="<?php echo $layout->getImageUrl();?>">
                    </div>
                <?php endforeach;?>
            </div>
            <div class="right pull-left">
                <div class="area_block_wrap clearfix">
                    <div class="area_block">
                        <?= t('Жилая площадь') ?>
                        <span><?php echo $project->usable_area_by_order;?> m<sup>2</sup></span>
                    </div>
                    <div class="area_block" style="margin: 0">
                        <?= t('Общая площадь') ?>
                        <span><?php echo $project->net_area;?> m<sup>2</sup></span>
                    </div>
                </div>
                <table class="table_project" style="width: 100%" >

                    <?php if($workScopePrice): ?>
                    <tr>
                        <td><?= t('Стоимость домокомплекта от') ?></td>
                        <td id="homeKitMinPrice"><?php echo price('{price} {short}.', $workScopePrice->price);?></td>
                    </tr>
                    <?php endif ?>

                    <tr>
                        <td><?= t('Подвал') ?></td>
                        <td><?php echo $project->getBasementArea();?></td>
                    </tr>
                    <tr>
                        <td><?= t('Площадь застройки') ?></td>
                        <td><?php echo $project->minimum_building_area;?> m<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td><?= t('Кубатура') ?></td>
                        <td><?php echo $project->cubage;?> m<sup>3</sup></td>
                    </tr>
                    <tr>
                        <td><?= t('Высота') ?></td>
                        <td><?php echo $project->house_height;?> m</td>
                    </tr>
                    <tr>
                        <td><?= t('Угол наклона кровли') ?></td>
                        <td><?php echo $project->roof_angle;?>°</td>
                    </tr>
                    <tr>
                        <td><?= t('Площадь крыши') ?></td>
                        <td><?php echo $project->roof_area;?> m<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td><?= t('Минимальные размеры участка') ?></td>
                        <td><?= t('шир.') ?> <?php echo $project->plot_width;?>m × <?= t('длина') ?> <?php echo $project->plot_length;?>m</td>
                    </tr>
                </table>

                <div class="right_border pull-left"></div>
                <div class="project_z10 pull-left"><strong><?= t('Проект') ?> <?php echo $project->getName();?></strong>  <?php echo $project->title;?></div>
                <div class="expand_description pull-left">
                    <?php echo $project->description;?>
                </div>


            </div>
        </div>

        <div class="facades">
            <div class="facades_name"><?= t('Фасады') ?></div>
            <ul class="facades_wrap list-unstyled list-inline">
                <?php foreach($project->getImagesByType('facade') as $image):?>
                    <li><a href="<?php echo $image->getImageUrl();?>" class="fancybox" rel="fasade"><img src="<?php echo $image->getImageUrl('facadeSmall');?>"></a></li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>

    <div id="project-tab4" class="project-tab prices">
        <?php $homeKits = HomeKit::model()->findAll();?>
        <div class="project_cost_wrap">
            <div class="project_cost_name"><?= t('Стоимость домокомплекта') ?></div>
            <div class="project_cost_table"><table style="width: 100%">
                    <thead>
                    <tr>
                        <td></td>
                        <?php foreach($homeKits as $homeKit):?>
                            <td><?= t('ДОМОКОМПЛЕКТ') ?> “<?php echo mb_strtoupper($homeKit->name, 'UTF8');?>”</td>
                        <?php endforeach;?>
                    </tr>
                    </thead>
                    <tbody>


                    <?php foreach(WorkScope::model()->findAll() as $k=>$workScope): ?>
                        <?php
                        $checked = !($k>1 && $k!=(count($workScopes)-1)) || isset($_GET['workScope-'.$workScope->id]);
                        ?>
                        <tr class="<?php echo $checked?'':'checkbox_off';?> <?php echo $k==0?'first_tr':'';?>">
                            <td class="<?php echo $k==0?'first_td':'';?>">
                                <div class="fire_fox_fix">
                                    <a href="#" class="checkbox_table <?php echo $k!=0?'project-price-checkbox':'';?>">
                                        <img src="<?php echo app()->theme->baseUrl?>/img/<?php echo $checked?'check_on.png':'check_off.png';?>"/>
                                    </a>
                                    <?php if($k == 0){
                                        echo t('Домокомплект');
                                    } else {
                                        echo $workScope->name;
                                    }?>
                                    <div class="drop_menu_table" style="display: none;"><?php echo $workScope->description;?></div>
                                </div>

                            </td>
                            <?php foreach($homeKits as $col=>$homeKit):?>
                                <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id'=>$project->id,'work_scope_id'=>$workScope->id,'home_kit_id'=>$homeKit->id));?>
                                <?php if($k<=1):?>
                                    <td data-price="<?php echo $workScopePrice->price;?>" class="work-scope-price-col<?php echo $col;?>">
                                        <?php echo price('{price}', $workScopePrice->price);?>
                                    </td>
                                <?php else:?>
                                    <td colspan="4" data-price="<?php echo $workScopePrice->price;?>" class="work-scope-price">
                                        <?php echo price('{price}', $workScopePrice->price);?>
                                    </td>
                                    <?php break; endif;?>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="in_total">
                        <td><?= t('ИТОГО') ?></td>
                        <?php foreach($homeKits as $k=>$homeKit):?>
                            <td class="price" id="homeKit-summ<?php echo $k;?>"></td>
                        <?php endforeach;?>
                    </tr>
                    </tbody>

                </table></div>

        </div>

    </div>

<?php
$priceAccuracy = config('price_accuracy');
$themeBaseUrl = app()->theme->baseUrl;
$script = <<<JS
function round(a,b) {
    b=b || 0;
    return Math.round(a*Math.pow(10,b))/Math.pow(10,b);
}


$('.project_cost_table a.plus_table').click(function(){
    $(this).closest('td').toggleClass('active').find('div.drop_menu_table').toggle();
    return false;
});

/*
 * count summ
 * */
function countSumm(col){
    var summ = 0;
    $('.work-scope-price-col'+col).each(function(i, el){
        if(!$(el).closest('tr').hasClass('checkbox_off'))
            summ += $(el).data('price');
    });
    $('.work-scope-price').each(function(i, el){
        if(!$(el).closest('tr').hasClass('checkbox_off'))
            summ += $(el).data('price');
    });
    return accounting.formatNumber(summ, {$priceAccuracy}, " ", ',')
}
function renderSumm(){
    $('#homeKit-summ0').html(countSumm(0));
    $('#homeKit-summ1').html(countSumm(1));
    $('#homeKit-summ2').html(countSumm(2));
    $('#homeKit-summ3').html(countSumm(3));
}
renderSumm();

$('.project_cost_table a.checkbox_table').click(function(){
    return false;
});

$('.project_cost_table a.project-price-checkbox').click(function(){
    var tr = $(this).closest('tr');
    tr.toggleClass('checkbox_off');
    if(tr.hasClass('checkbox_off'))
        tr.find('.project-price-checkbox img').attr('src', '{$themeBaseUrl}/img/check_off.png');
    else
        tr.find('.project-price-checkbox img').attr('src', '{$themeBaseUrl}/img/check_on.png');
    renderSumm();
    return false;
});
/*
 * end
 * */

window.print();
JS;
cs()->registerScript('project-print', $script);
?>

    </div>
</div>
