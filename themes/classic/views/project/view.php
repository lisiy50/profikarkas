<?php
/**
 * @var Project $project
 * @var PackagingHomeKit[] $packagingHomeKits
 */

$detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();

?>

<style>
    .tabs li>a div {
        font-size: 13px;
    }
    @media screen and (max-width: 767px) {
        .tabs li a div {
            font-size: 11px;
        }
    }

</style>

<div class="structured_top clearfix">
    <?php if (Yii::app()->language == 'en'): ?>
        <a class="pull-left" href="<?= url('project/index');?>" style="width: initial; margin-right: 20px;">
            Go to the full list of projects (in ukrainian language)
        </a>
        <a class="pull-left" href="/en#projects">
            Go back
        </a>
    <?php else: ?>
        <a href="<?php echo url('project/index');?>" class="pull-left"><?= t('Вернуться к списку проектов') ?></a>
    <?php endif; ?>
    <div class="pull-right">
        <?php if($project->getPrevProject()):?>
        <a href="<?php echo $project->getPrevProject()->getUrl();?>" class="back"><?= t('Предыдущий') ?></a>
        <?php endif;?>
        <?php if($project->getNextProject()):?>
        <a href="<?php echo $project->getNextProject()->getUrl();?>" class="next"><?= t('Следующий') ?></a>
        <?php endif;?>
    </div>
</div>

<div class="home_projects_center clearfix">
    <div class="left">
        <h1 class="z10img"><?php echo $project->getName();?></h1>
        <div id="project-slider">

            <div class="slider" id="project-carousel">
                <?php foreach($project->iVisualizations as $k=>$image):?>
                    <a href="#" class="slide" id="i<?php echo $k;?>">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('large');?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация <?php echo $k+1;?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Img', 'Click', <?php echo $image->getImageUrl('large');?>, {nonInteraction: true});">
                    </a>
                <?php endforeach;?>

                <?php foreach($project->projectVideo1->getVideos() as $k=>$video):?>
                    <a href="#" class="slide" id="v<?php echo $k;?>">
                        <?php echo $video->getVideoInsertHtml();?>
                    </a>
                <?php endforeach;?>

                <?php if($project->getNewsImages()):?>
                    <?php foreach($project->getNewsImages() as $k1=>$newsImage):?>
                        <a href="#" class="slide" id="n<?php echo $k1;?>">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $newsImage->getImageUrl('project');?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация <?php echo $k1+$k+2;?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Img', 'Click', <?php echo $newsImage->getImageUrl('project');?>, {nonInteraction: true});">
                        </a>
                    <?php endforeach;?>
                <?php endif;?>

                <?php if($project->getPortfolioImages()):?>
                    <?php foreach($project->getPortfolioImages() as $k2=>$portfolioImage):?>
                        <a href="#" class="slide" id="p<?php echo $k2;?>">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $portfolioImage->getImageUrl('more');?>" alt="<?php echo $project->symbol ?> иллюстрация <?php echo $k2+$k1+$k+3;?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Img', 'Click', <?php echo $portfolioImage->getImageUrl('more');?>, {nonInteraction: true});">
                        </a>
                    <?php endforeach;?>
                <?php endif;?>

                <?php foreach($project->iInteriors as $k3=>$image):?>
                    <a href="#" class="slide" id="in<?php echo $k3;?>">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('large');?>" alt="<?php echo $project->getName();?> интерьер <?php echo ($k+1)?>" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Img', 'Click', <?php echo $image->getImageUrl('large');?>, {nonInteraction: true});">
                    </a>
                <?php endforeach;?>

            </div>

            <a href="#" class="slider_prev"></a>
            <a href="#" class="slider_next"></a>

            <?php if (!$isMobile || $isTablet): ?>

                <div class="project_carusel clearfix">
                <a href="#" class="back pull-left" id="project-thumbs-prev"></a>

                    <?php if($project->getVideoWalks()):?>
                    <div class="pull-left videowalks-mini" style="width: <?php echo count($project->getVideoWalks())*214;?>px;">
                        <?php foreach ($project->getVideoWalks() as $k=>$videoWalk): ?>
                            <a href="<?php echo $videoWalk->getUrl(); ?>" class="slide" target="_blank" rel="nofollow" style="width: 210px;">
                                <div class="video-walk-label">
                                    <?= t('Видео прогулка по дому') ?>
                                </div>
                                <div class="video-walk-image">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $videoWalk->getImageUrl('mini');?>" alt="<?php echo $videoWalk->name ?> Видеопрогулка <?php echo $k+1;?>">
                                    <div class="video-walk-icon">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/360.png">
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <?php $caruselWidth = 856-count($project->getVideoWalks())*214; ?>

                    <div class="pull-left" style="width:<?php echo $caruselWidth; ?>px;">
                        <div class="pull-left carusel" style="width: <?php echo $caruselWidth; ?>px;height: 60px" id="project-thumbs">
                            <?php foreach($project->iVisualizations as $k=>$image):?>
                                <a href="#i<?php echo $k;?>"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('thumb');?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация <?php echo $k+1;?>"></a>
                            <?php endforeach;?>
                            <?php foreach($project->projectVideo1->getVideos() as $k=>$video):?>
                                <a href="#v<?php echo $k;?>"><?php echo $video->getVideoThumb();?></a>
                            <?php endforeach;?>
                            <?php if($project->getNewsImages()):?>
                                <?php foreach($project->getNewsImages() as $k1=>$newsImage):?>
                                    <a href="#n<?php echo $k1;?>"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $newsImage->getImageUrl('mini');?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация <?php echo $k1+$k+2;?>"></a>
                                <?php endforeach;?>
                            <?php endif;?>
                            <?php if($project->getPortfolioImages()):?>
                                <?php foreach($project->getPortfolioImages() as $k2=>$portfolioImage):?>
                                    <a href="#p<?php echo $k2;?>"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $portfolioImage->getImageUrl('mini');?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация <?php echo $k2+$k1+$k+3;?>"></a>
                                <?php endforeach;?>
                            <?php endif;?>
                            <?php foreach($project->iInteriors as $k3=>$image):?>
                                <a href="#in<?php echo $k3;?>" >
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('interiorSmall');?>"  alt="<?php echo $project->getName();?> интерьер <?php echo ($k+1)?>">
                                </a>
                            <?php endforeach;?>
                        </div>
                    </div>

                <a href="#" class="next pull-left" id="project-thumbs-next"></a>
            </div>
            <?php endif ?>
        </div>
        <div id="project-videos" style="display: none;">
            <div class="slider" id="project-video-carousel">
                <?php foreach($project->getVideos() as $k=>$video):?>
                    <div class="item" style="display:<?php echo $k==0?'block':'none';?>;" id="v<?php echo $k;?>">
                        <?php echo $video->getVideoInsertHtml();?>
                    </div>
                <?php endforeach;?>
            </div>
            <div class="project_carusel clearfix">
                <div class="pull-left carusel" style="width: 856px;height: 60px" id="project-video-thumbs">
                    <?php foreach($project->getVideos() as $k=>$video):?>
                        <a href="#v<?php echo $k;?>"><?php echo $video->getVideoThumb();?></a>
                    <?php endforeach;?>
                </div>
            </div>
        </div>

    </div>
    <div class="right">
        <ul class="list-unstyled home_projects_right_nav pull-right">
            <?php if($project->videoWalks): ?>
            <li><a class="colored" href="<?php echo $project->videoWalks[0]->url?>" id="wideo-walks"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/360.png"><?= t('Видео прогулка') ?></a></li>
            <?php endif; ?>
            <li class="<?= $project->videoWalks ? '' : 'current'?>"><a href="#project-slider" id="project-slider-tab"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/photo_icon.png"><?= t('Фото') ?></a></li>
            <?php if($project->getVideos()):?>
            <li><a href="#project-videos" id="project-videos-tab"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/video_img.png"><?= t('Видео') ?></a></li>
            <?php endif;?>
            <?php if(config('project_house_equipment_link_visibility') && Yii::app()->language != 'en'):?>
            <li><a href="<?php echo url('equiphouses/index');?>" target="_blank"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/house_img.png"><?= t('Оснащение дома') ?></a></li>
            <?php endif;?>
            <?php if(config('project_faq_link_visibility') && Yii::app()->language != 'en'):?>
            <li><a href="<?php echo url('faq/index');?>" target="_blank"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/question_img.png"><?= t('Вопросы и ответы') ?></a></li>
            <?php endif;?>
            <?php if(config('project_mounting_video_link_visibility') && Yii::app()->language != 'en'):?>
            <li><a href="<?php echo url('article/view', array('id'=>1, 'uri'=>'montaj-doma'));?>" target="_blank"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/mounting_video_img.png"><?= t('Видео о монтаже') ?></a></li>
            <?php endif;?>
            <?php if(config('project_building_steps_link_visibility') && Yii::app()->language != 'en'):?>
            <li><a href="<?php echo url('article/view', array('id'=>5, 'uri'=>'video-po-algoritmu'));?>" target="_blank"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/building_steps_img.png"><?= t('Этапы строительства') ?></a></li>
            <?php endif;?>
        </ul>
    </div>

    <div class="favorites_wrap">
        <ul class="list-inline list-unstyled pull-left">
<!--            <li><a href="#"><img class="normal" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="--><?php //echo app()->theme->baseUrl;?><!--/img/heart_img.png"><img class="hover" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="--><?php //echo app()->theme->baseUrl;?><!--/img/heart_img_hover.png">  избранное</a></li>-->
            <li><a id="print_project" href="#"><img class="normal" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/print_img.png"><img class="hover" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/print_img_hover.png"><?= t('Печать') ?></a></li>
            <li><a href="<?php echo url('project/recomend', array('id'=>$project->id));?>" class="various fancybox.ajax" id="recomend-project" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Recommend', {nonInteraction: true});"><img class="normal" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/envelope_img.png"><img class="hover" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo app()->theme->baseUrl;?>/img/envelope_img_hover.png"><?= t('Порекомендовать') ?></a></li>
        </ul>
    </div>

</div>

<div class="home_projects_bottom" style="position: relative;">
    <ul class="list-unstyled list-inline tabs clearfix" id="project-tabs">
        <li class="current"><a href="#project-tab1" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'General', {nonInteraction: true});"><div><?= t('Общие данные') ?></div><div class="alt"><?= t('Параметры') ?></div></a></li>
        <?php if (false): ?>
        <li><a href="#project-tab2"><div><?= t('Варианты') ?> <span>0</span></div><div class="alt"><?= t('Другие версии') ?></div></a></li>
        <?php endif; ?>
        <?php if(count($project->iInteriors)): ?>
        <li><a href="#project-tab3" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'Interiors', {nonInteraction: true});"><div><?= t('Интерьеры') ?></div></a></li>
        <?php endif ?>

        <?php if (! in_array($project->id, [153, 154, 155])): ?>
            <?php if (Yii::app()->language != 'en'): ?>
            <li><a href="#project-tab4" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'Price', {nonInteraction: true});"><div><?= t('Стоимость строительства') ?></div></a></li>
            <?php endif; ?>
        <?php endif; ?>

        <?php if (Yii::app()->language != 'en'): ?>
            <li><a href="#project-tab5"><div><?= t('Почему мы не строим') ?></div><div class=""><?= t('из sip панелей') ?></div></a></li>
        <?php endif; ?>

        <li>
            <a href="#project-tab6"
               onclick="ga('create', 'UA-52477514-1', 'auto'); ga('send', 'event', 'Tab', 'Click', 'Export price', {nonInteraction: true});">
                <div>Export price</div>
            </a>
        </li>
        <li>
            <a href="#project-tab7"
               onclick="ga('create', 'UA-52477514-1', 'auto'); ga('send', 'event', 'Tab', 'Click', 'FAQ', {nonInteraction: true});">
                <div><?= t('Часто задаваемые') ?></div><div><?= t('вопросы') ?></div>
            </a>
        </li>
        <?php if (false): ?>
        <li><a class="tab_btn" id="" href="<?= Yii::app()->createUrl('article/view', array('id' => 30, 'uri' => 'iq-energy')); ?>"><div>Получите до 9000 €</div><div class="">от IQ Energy</div></a></li>
        <li><a id="project_comments" href="#project-tab_comments"><div>Комментарии</div><div class="alt2"><?php echo $project->countComments; ?></div></a></li>
        <?php endif; ?>
    </ul>

    <div id="project-tab1" class="project-tab">
        <?php $this->renderPartial('tabs/tab1', array('project'=>$project));?>
    </div>
    <?php if (false): ?>
    <div id="project-tab2">
        <?php $this->renderPartial('tabs/tab2', array('project'=>$project));?>
    </div>
    <?php endif; ?>
    <div id="project-tab3" class="project-tab hidden">
        <?php $this->renderPartial('tabs/tab3', array('project'=>$project));?>
    </div>
    <div id="project-tab4" class="project-tab hidden">
        <?php $this->renderPartial('tabs/tab4', array('project'=>$project, 'homeKits'=> $homeKits, 'packagingHomeKits' => $packagingHomeKits));?>
    </div>
    <?php if (true): ?>
    <div id="project-tab5" class="project-tab hidden">
        <?php $this->renderPartial('tabs/tab5');?>
    </div>
    <?php endif; ?>
    <div id="project-tab6" class="project-tab hidden">
        <?php $this->renderPartial('tabs/tab6', array(
            'project' => $project,
            'homeKits' => $homeKits,
            'packagingHomeKits' => $packagingHomeKits
        ));?>
    </div>
    <div id="project-tab7" class="project-tab hidden">
        <?php $this->renderPartial('tabs/tab7'); ?>
    </div>
</div>

<?php if (Yii::app()->language != 'en'): ?>
<div class="invitation"></div>
<?php endif; ?>

<?php if (false): ?>
<div id="project-tab_comments" style="margin-top: 45px;">
    <?php $this->renderPartial('tabs/tab_comments', array('project' => $project));?>
</div>
<?php endif; ?>

<div class="block_social_networks">
<!--    <a class="comment_link" href="#project-tab_comments">Комментировать</a>-->
    <?php $this->renderPartial('//layouts/share_buttons');?>
</div>

<?php
package('fancyboxHelpers');
package('carouFredSel');
?>

<?php

$projectName = $project->getName();

$script = <<<JS

    $('.fancybox').fancybox({
        autoResize: true,
        autoCenter: true
    });
    
    $('#recomend-project').fancybox({
        closeBtn: false,
        autoResize: true,
        autoCenter: true
    });
    
    $('#project-slider-tab, #project-videos-tab').click(function(){
        $('#project-slider, #project-videos').hide();
        $('#project-slider-tab, #project-videos-tab').closest('li').removeClass('current');
        $(this).closest('li').addClass('current');
        $($(this).attr('href')).show();
        return false;
    })
    
    $('#project-tabs a').not('.tab_btn').click(function(){
        $('#project-tabs li').removeClass('current');
        $(this).closest('li').addClass('current');
        $('.project-tab').addClass('hidden');
        var tabSelector = $(this).attr('href');
        $(tabSelector).removeClass('hidden');
    });
    
    var selector = '#project-tabs a[href="'+window.location.hash+'"]';
    $(selector).click();

JS;

$script .= <<<JS

setTimeout(initSlider, 2000);

function initSlider() {
    $('#project-carousel').carouFredSel({
        responsive: true,
        width: '100%',
        height: 'variable',
        items: {
            visible: 1,
            height: 'variable'
        },
        circular: false,
        auto: false,
        scroll: {
            fx: 'directscroll'
        },
        next:'.slider_next',
        prev:'.slider_prev'
    });
    
    $('#project-thumbs').carouFredSel({
        circular: true,
        infinite: false,
        auto: false,
        prev: '#project-thumbs-prev',
        next: '#project-thumbs-next',
        items: {
            visible: {
                min: 8,
                max: 8
            },
            width: 107,
            height: 60
        }
    });
}

    $('#project-thumbs a').click(function() {
        $('#project-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
        $('#project-carousel a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });

    $.post('site/signUpForDemoHouse', function(html) {
        $('.invitation').html(html);
        $('#SignUpForDemoHouse_subject').val('Запрос на поездку в демо-дом. Страница проекта $projectName');
    });

    $('.invitation').on('submit', '#signUpForDemoHouseForm', function () {
        $.post($.createUrl('site/signUpForDemoHouse'), $(this).serialize(), function (html) {
            $('#invitationInner').remove();
            $('.invitation').html(html);
        });
        return false;
    });
    
JS;

$script .= <<<JS

    $('#project-video-thumbs a').click(function(){
        $('#project-video-carousel .item').hide();
        $($(this).attr('href')).show();
        return false;
    });
    
    $('#print_project').click(function(){
        window.print();
        return false;
    });
    
    $('#project-description iframe').wrap('<div class="project-description-video" style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;"></div>').css({
        'position': 'absolute',
        'top': '0',
        'left': '0',
        'bottom': '0',
        'right': '0',
        'width': '100%',
        'height': '100%'});

JS;

$script .= <<<JS
    
    $(document).ready(function(){
        ga('create', 'UA-52477514-1', 'auto');
    })

JS;

cs()->registerScript('project-view', $script);

cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/sign.up.for.consultation.js');


?>

<style type="text/css">
    <?php if($project->h1_size):?>
    .project-description h1, .project-description h1 span {
        font-size: <?php echo $project->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($project->h2_size):?>
    .project-description h2, .project-description h2 span {
        font-size: <?php echo $project->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($project->h3_size):?>
    .project-description h3, .project-description h3 span {
        font-size: <?php echo $project->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($project->h4_size):?>
    .project-description h4, .project-description h4 span {
        font-size: <?php echo $project->h4_size?>px !important;
    }
    <?php endif?>
</style>
