<?php
/**
 * @var Controller $this
 * @var Project $project
 */
?>

<div id="comment-container">
    <?php $this->widget('CommentsWidget', array('owner' => $project, 'comment' => new Comment()));?>
</div>

