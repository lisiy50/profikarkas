<?php
/**
 * @var $project Project
 */
?>

<div class="projects_info clearfix">

    <div class="left pull-left">
        <?php
        foreach($project->iLayouts as $k=>$layout):
        ?>
            <div class="plan_wrap">
                <img class="img-responsive" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $layout->getImageUrl('origin');?>" alt="<?php echo $project->getName();?> планировка <?php echo ($k+1)?>">
            </div>
        <?php endforeach;?>
    </div>
    <div class="right pull-left">
        <div class="area_block">
            <?= t('Общая площадь') ?>
            <span><?php echo $project->net_area; ?> m<sup>2</sup></span>
        </div>
        <?php if (false): ?>
        <div class="area_block">
            <?= t('Площадь дома по внешним габаритам') ?>
            <span><?php echo $project->external_dimensions_area; ?> m<sup>2</sup></span>
        </div>
        <?php endif; ?>

        <table class="table_project pull-left" style="width: 100%" >
            <?php /* @var WorkScopePrice $workScopePrice */ ?>
            <?php /* @var WorkScopePrice $euroWorkScopePrice */ ?>
            <?php
            $euroWorkScopePrice = WorkScopePrice::model()->findByAttributes(array('home_kit_id' => 2, 'work_scope_id' => 10, 'project_id' => $project->id));
            ?>
            <?php if($workScopePrice = WorkScopePrice::model()->findByAttributes(array('home_kit_id' => 2, 'work_scope_id' => 1, 'project_id' => $project->id))): ?>
                <?php if (Yii::app()->language != 'en'): ?>
                    <tr>
                        <td><?= t('Стоимость домокомплекта от') ?></td>
                        <td id="homeKitMinPrice"><?= price('{price}', $workScopePrice->price) . ' ' . t('грн.'); ?>
                            <?= CHtml::link(t('Подробнее'), $project->getUrl(array('#' => 'project-tab4')), array('style' => 'font-weight:bold;text-decoration:underline;float:right;color:#333;', 'class' => 'go-to-price-tab'))?>
                            <br>
                            <?php echo config('price_notice_default_1')?></td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td><?= t('Стоимость домокомплекта от') ?></td>
                        <td id="homeKitMinPrice">
                            <?= price('{price}', $euroWorkScopePrice->price) . ' €'; ?>
                            <br>
                            <?= config('price_notice_default_1')?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (config('show_actual_cost')): ?>
                <tr>
                    <td colspan="2"><a href="#" id="request-price" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Actual price', {nonInteraction: true});"><?php echo config('price_notice_default_2')?></a></td>
                </tr>
                <?php endif;?>
            <?php elseif ($project->id == 155): ?>
                <tr>
                    <td><?= t('Стоимость') ?></td>
                    <td id="homeKitMinPrice">400 000 <?= t('грн.') ?></td>
                </tr>
            <?php elseif ($project->id == 154): ?>
                <tr>
                    <td><?= t('Стоимость') ?></td>
                    <td id="homeKitMinPrice">350 000 <?= t('грн.') ?></td>
                </tr>
            <?php elseif ($project->id == 153): ?>
                <tr>
                    <td><?= t('Стоимость') ?></td>
                    <td id="homeKitMinPrice">300 000 <?= t('грн.') ?></td>
                </tr>
            <?php endif;?>
            <tr>
                <td><?= t('Подвал') ?></td>
                <td><?php echo $project->getBasementArea();?></td>
            </tr>
            <tr>
                <td><?= t('Площадь застройки') ?></td>
                <td><?php echo $project->minimum_building_area;?> m<sup>2</sup></td>
            </tr>
            <tr>
                <td><?= t('Кубатура') ?></td>
                <td><?php echo $project->cubage;?> m<sup>3</sup></td>
            </tr>
            <tr>
                <td><?= t('Высота') ?></td>
                <td><?php echo $project->house_height;?> m</td>
            </tr>
            <tr>
                <td><?= t('Угол наклона кровли') ?></td>
                <td><?php echo $project->roof_angle;?>°</td>
            </tr>
            <tr>
                <td><?= t('Площадь крыши') ?></td>
                <td><?php echo $project->roof_area;?> m<sup>2</sup></td>
            </tr>
            <?php if (Yii::app()->language != 'en'): ?>
            <tr>
                <td><?= t('Минимальные размеры участка') ?></td>
                <td><?= t('шир.') ?> <?php echo $project->plot_width;?>m × <?= t('длина') ?> <?php echo $project->plot_length;?>m</td>
            </tr>
            <?php endif; ?>
        </table>
        <?php foreach($project->iAreas as $k=>$layout):?>
            <div class="plan_wrap">
                <!--                <div class="plan_name">Первый этаж  56,5 m<sup>2</sup></div>-->
                <img src="<?php echo $layout->getImageUrl('origin');?>" alt="<?php echo $project->getName();?> участок <?php echo ($k+1)?>" style="max-width: 100%">
            </div>
        <?php  endforeach;?>
        <div class="right_border pull-left"></div>
        <h2 class="project_z10 pull-left"><strong><?= t('Проект') ?> <?php echo $project->getName();?></strong>

            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $project->title; ?>
            <?php elseif (Yii::app()->language == 'uk'): ?>
                <?php echo $project->title_uk; ?>
            <?php endif; ?>

        </h2>

        <?php if (Yii::app()->language != 'en'): ?>
            <div class="expand_description pull-left">
                <div class="project-description" id="project-description" style="">
                    <?php if (Yii::app()->language == 'ru'): ?>
                        <?php echo $project->description; ?>
                    <?php elseif (Yii::app()->language == 'uk'): ?>
                        <?php echo $project->description_uk;?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>


        <?php $this->renderPartial('//layouts/social_groups', array('options'=>array('width'=>365, 'height'=>254)));?>


    </div>
</div>

<div class="facades">
    <div class="facades_name"><?= t('Фасады') ?></div>
    <ul class="facades_wrap list-unstyled list-inline">
        <?php foreach($project->iFacades as $k=>$image):?>
        <li><a href="<?php echo $image->getImageUrl('origin');?>" class="fancybox" rel="fasade"><img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $image->getImageUrl('facadeSmall');?>" alt="<?php echo $project->getName();?> фасад <?php echo ($k+1)?>"></a></li>
        <?php endforeach;?>
    </ul>
</div>

<?php if($project->getSimilars()): ?>

<div class="similar-projects">
    <div class="facades_name"><?= t('Похожие проекты') ?></div>
    <ul class="similar-projects-wrap list-unstyled list-inline">
        <?php foreach($project->getSimilars() as $similarProject):?>
            <?php /* @var Project $similarProject*/ ?>
            <li>
                <a class="clearfix" title="<?php echo $similarProject->getName() ?>" href="<?php echo $similarProject->getUrl();?>">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $similarProject->getFirstImageUrl()?$similarProject->getFirstImageUrl()->getImageUrl('small'):'' ?>" alt="<?php echo $similarProject->getName();?>">

                    <div class="name"><?php echo $similarProject->getName();?></div>
                    <div class="square"><?php echo $similarProject->net_area;?>m<sup>2</sup></div>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>

<?php endif ?>

<?php
$script = <<<JS
//        $('#expand-description').click(function(){
//            $('#project-description').toggle();
//            return false;
//        });

var min = 999999999;
$('.project_cost_table .first_tr td[data-price]').each(function(i, el){
if(parseInt($(el).data('price')) < min)
min = parseInt($(el).data('price'));
});
//        $('#homeKitMinPrice').html(accounting.formatNumber(min, 2, " ", ','));

$('#request-price').click(function(){
    $('a[href="#project-tab4"]').click();
    $('#show-send-link').click();
    return false;
});

$('.go-to-price-tab').click(function(){
    $('#project-tabs a[href="#project-tab4"]').click();
})
JS;
cs()->registerScript('project-tab1', $script);
?>
