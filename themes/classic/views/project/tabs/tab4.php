<?php
/**
 * @var $project Project
 * @var PackagingHomeKit[] $packagingHomeKits
 * @var HomeKit[] $homeKits
 */
?>
<script>
    function printCss() {
        $('.project_cost_wrap').append("<style>@media print{.cost-table{padding-top:20px!important;}.cost-var-head,.cost-head{position: static;}.no-print,.cost-feature-img,.zsiq_flt_rel,#zsiq_maintitle,#zsiq_maintitle:after,#titlediv > div[title*='Оставьте ваше сообщение']:after,#zsiq_byline,.zsiq_theme1 div.zsiq_cnt,.similar-projects,.facades,.invitation,.home_projects_center,.projects_info{display:none;}}</style>");
    }
</script>

<div class="project_cost_wrap">
    <div class="project_cost_name no-print">
        <h2 style="margin-bottom: 22px;"><?= t('Стоимость строительства дома') ?> <?php echo $project->getName() ?></h2> <span style="font-weight: normal;font-size: 14px;"><?php // echo config('price_notice_default_1')?></span>
    </div>

    <div class="cost-page-block no-print">
        <ul class="cost-page-list gray-1">
            <li><?= t('Мы изготавливаем на заводе и монтируем каркасно-панельные дома с утеплением из минеральной ваты.') ?></li>
            <li><?= t('Мы работаем по всей Украине и Европе.') ?></li>
            <li><?= t('При производстве и монтаже домов мы соблюдаем требования Института Пассивного Дома г.Дармштад (Германия) (минимизация тепловых мостов, паронепроницаемость конструкций).') ?></li>
        </ul>
        <div class="cost-page-inner gray-3">
            <div class="cost-page-video-wrap">
                <div class="cost-page-video">
                    <iframe width="525" height="295" src="" data-src="https://www.youtube.com/embed/bPEA_aj_kso?rel=0&autoplay=1&mute=1&loop=1&playlist=bPEA_aj_kso" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="cost-page-pic" style="background: #ffffff;">
                <?php if (false): ?>
                    <div class="cost-page-notice">
                        Как выглядит<br>
                        дом изнутри<br>
                        разрезы стен
                    </div>
                    <div class="cost-page-img-wrap">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="  data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-nav-5-lg.png" alt="Как выглядит дом изнутри. Разрезы стен" class="cost-page-img">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if(config('show_actual_cost')): ?>
    <a href="#" id="show-send-link" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Actual price', {nonInteraction: true});">Актуальную стоимость <span>запрашивайте у менеджера</span></a>
    <?php endif;?>

    <?php $homeKits = HomeKit::model()->findAll(); ?>
    <?php $workScopes = WorkScope::model()->findAll(); ?>

    <section class="cost">

        <div class="cost-table">

            <div class="cost-head">
                <div class="cost-head-inner">
                    <div class="cost-tr cost-head-name">
                        <div class="cost-td cost-feature title gray-1"><?= t('КОМПЛЕКТАЦИИ') ?></div>
                        <div class="cost-td cost-title cost-name gray-2"><?= t('Домокомплект<br> "под ключ"<br> с монтажом') ?></div>
                        <div class="cost-td cost-title cost-name gray-3"><?= t('Стандарт') ?></div>
                        <div class="cost-td cost-title cost-name gray-4"><?= t('Стандарт +<br> отделка') ?></div>
                        <div class="cost-td cost-title cost-name gray-5"><?= t('Максимум') ?></div>
                    </div>
                    <?php if (false): ?>
                    <div class="cost-tr cost-head-num cost-head-num-desktop no-print">
                        <div class="cost-td cost-title-icon-feature cost-feature">
                            <div class="cost-title-icon-feature-inner">
                                <span class="cost-title-icon-feature-notice">Нажмите на картинку, чтобы увидеть</span>
                                <span class="cost-title-icon-feature-title">КАК ВЫГЛЯДИТ ДОМОКОМПЛЕКТ</span>
                            </div>
                        </div>
                        <div class="cost-td cost-title cost-title-icon">
                            <a href="<?php echo url('homekit/kit1')?>" class="cost-title-icon-link various fancybox.ajax">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/kit-icon-1.jpg" alt="" class="kit-icon-img">
                            </a>
                        </div>
                        <div class="cost-td cost-title cost-title-icon">
                            <a href="<?php echo url('homekit/kit2')?>" class="cost-title-icon-link various fancybox.ajax">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/kit-icon-2.jpg" alt="" class="kit-icon-img">
                            </a>
                        </div>
                        <div class="cost-td cost-title cost-title-icon">
                            <a href="<?php echo url('homekit/kit3')?>" class="cost-title-icon-link various fancybox.ajax">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/kit-icon-3.jpg" alt="" class="kit-icon-img">
                            </a>
                        </div>
                        <div class="cost-td cost-title cost-title-icon">
                            <a href="<?php echo url('homekit/kit4')?>" class="cost-title-icon-link various fancybox.ajax">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/kit-icon-4.jpg" alt="" class="kit-icon-img">
                            </a>
                        </div>
                        <div class="cost-td cost-title cost-title-icon">
                            <a href="<?php echo url('homekit/kit5')?>" class="cost-title-icon-link various fancybox.ajax">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/kit-icon-5.jpg" alt="" class="kit-icon-img">
                            </a>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="cost-tr cost-head-num cost-head-num-mobile no-print">
                <div class="cost-td cost-feature"><?= t('Комплектации') ?></div>
                <div class="cost-td cost-title">1</div>
                <div class="cost-td cost-title">2</div>
                <div class="cost-td cost-title">3</div>
                <div class="cost-td cost-title">4</div>
            </div>

            <div class="cost-var no-print">
                <div class="cost-tr cost-var-head cost-var-head-project">
                    <div class="cost-td cost-feature"><strong><?= t('Цена за домокомплект') ?></strong></div>
                    <?php foreach($homeKits as $key => $homeKit): ?>
                        <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id' => $project->id, 'work_scope_id' => $workScopes[0]['id'], 'home_kit_id' => $homeKit->id)); ?>
                        <?php if ($key != 3): ?>
                            <div class="cost-td"><strong><?php echo price('{price}', $workScopePrice->price); ?></strong></div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="cost-td"><strong><?= t('по запросу') ?></strong></div>
                </div>
                <div class="cost-var-body">
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Сроки проектирования') ?></span>
                            <span class="cost-feature-small"><?= t('Сроки проектирования') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"><?= t('2-8 недель') ?></div>
                        <div class="cost-td gray-3"><?= t('2-8 недель') ?></div>
                        <div class="cost-td gray-4"><?= t('2-8 недель') ?></div>
                        <div class="cost-td gray-5"><?= t('2-8 недель') ?></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Сроки производства') ?></span>
                            <span class="cost-feature-small"><?= t('Сроки производства') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"><?= t('2-5 недель') ?></div>
                        <div class="cost-td gray-3"><?= t('2-5 недель') ?></div>
                        <div class="cost-td gray-4"><?= t('2-5 недель') ?></div>
                        <div class="cost-td gray-5"><?= t('2-5 недель') ?></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Сроки монтажа домокомплекта на фундамент') ?></span>
                            <span class="cost-feature-small"><?= t('Сроки монтажа домокомплекта на фундамент') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td red-1"><?= t('2-5 дней') ?></div>
                        <div class="cost-td red-2"><?= t('2-5 дней') ?></div>
                        <div class="cost-td red-3"><?= t('2-5 дней') ?></div>
                        <div class="cost-td red-4"><?= t('2-5 дней') ?></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Другие монтажно-строительные работы') ?></span>
                            <span class="cost-feature-small"><?= t('Другие монтажно-строительные работы') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"><?= t('2 недели') ?></div>
                        <div class="cost-td gray-3"><?= t('3-4 недели') ?></div>
                        <div class="cost-td gray-4"><?= t('4-6 недель') ?></div>
                        <div class="cost-td gray-5"><?= t('от 2 месяцев') ?></div>
                    </div>
                    <?php if (false): ?>
                        <div class="cost-tr cost-subtitle">
                            <div class="cost-td cost-feature cost-feature-name">
                                Фундамент
                            </div>
                            <div class="cost-td"></div>
                            <div class="cost-td"></div>
                            <div class="cost-td"></div>
                            <div class="cost-td"></div>
                        </div>
                        <div class="cost-tr">
                            <div class="cost-td gray-1 cost-feature cost-feature-name">
                                <span class="cost-feature-full">Бетонный монолотный фундамент с утепленной плитой пола</span>
                                <span class="cost-feature-small">Бетонный монолотный фундамент с утепленной плитой пола</span>
                                <a class="cost-feature-link" href="">?</a>
                            </div>
                            <div class="cost-td gray-2 cost-check-icon"></div>
                            <div class="cost-td gray-3 cost-check-icon"></div>
                            <div class="cost-td gray-4 cost-check-icon"></div>
                            <div class="cost-td gray-5 cost-check-icon"></div>
                        </div>
                    <?php endif; ?>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Домокомплект') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"><?= t('толщина стены<br> 245 мм') ?></div>
                        <div class="cost-td"><?= t('толщина стены<br> 257 мм') ?></div>
                        <div class="cost-td"><?= t('толщина стены<br> 257 мм') ?></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Сборка стеновых панелей и панелей перекрытия на производстве (до 8м в длину)') ?></span>
                            <span class="cost-feature-small"><?= t('Сборка стеновых панелей и панелей перекрытия на производстве (до 8м в длину)') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-full">
                                <?= t('Наружные стены толщиной 245 мм, высотой 2800мм и R = 5 (U = 0,2 Вт / м2*К) состоящие из:') ?>
                                <ol>
                                    <li><?= t('наружный утеплитель 50мм') ?></li>
                                    <li><?= t('деревянный каркас из СЕ сертифицированной констукционной древесины KVH сечением 60 * 140мм с минераловатным утеплителем 140мм') ?></li>
                                    <li><?= t('ОСБ 12мм') ?></li>
                                    <li><?= t('паробарьерная пленка') ?></li>
                                    <li><?= t('внутренний слой утеплителя 40 мм с каналами для разводки электричества') ?></li>
                                </ol>
                            </div>
                            <div class="cost-feature-small">
                                <?= t('Наружные стены толщиной 245 мм, высотой 2800мм и R = 5 (U = 0,2 Вт / м2*К) состоящие из:') ?>
                                <ol>
                                    <li><?= t('наружный утеплитель 50мм') ?></li>
                                    <li><?= t('деревянный каркас из СЕ сертифицированной констукционной древесины KVH сечением 60 * 140мм с минераловатным утеплителем 140мм') ?></li>
                                    <li><?= t('ОСБ 12мм') ?></li>
                                    <li><?= t('паробарьерная пленка') ?></li>
                                    <li><?= t('внутренний слой утеплителя 40 мм с каналами для разводки электричества') ?></li>
                                </ol>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Комплект внутренних несущих стен: каркас из сертифицированного евро бруса 60*140') ?></span>
                            <span class="cost-feature-small"><?= t('Комплект внутренних несущих стен: каркас из сертифицированного евро бруса 60*140') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Межэтажные перекрытия: сертифицированный евро бруса 60*200 с ОСП 22 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Межэтажные перекрытия: сертифицированный евро бруса 60*200 с ОСП 22 мм') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Конструкция кровли: фермы Mitek или стропильная система из сертифицированного евро бруса 60*200') ?></span>
                            <span class="cost-feature-small"><?= t('Конструкция кровли: фермы Mitek или стропильная система из сертифицированного евро бруса 60*200') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Кровельное покрытие') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Металлочерепица, включая монтажные материалы') ?></span>
                            <span class="cost-feature-small"><?= t('Металлочерепица, включая монтажные материалы') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Окна') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Энергоэффективные окна с "теплым" монтажом') ?></span>
                            <span class="cost-feature-small"><?= t('Энергоэффективные окна с "теплым" монтажом') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Монтажно-строительные работы') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж домокомплекта за 2-5 дней, другие монтажно-строительные работы') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж домокомплекта за 2-5 дней, другие монтажно-строительные работы') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Утепление наружного контура дома') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Утепление наружных стен внутри каркаса базальтовой ватой 140 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Утепление наружных стен внутри каркаса базальтовой ватой 140 мм') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Наружное утепление наружных стен утеплителем 50 мм (основа для отделки фасада декоративной штукатуркой)') ?></span>
                            <span class="cost-feature-small"><?= t('Наружное утепление наружных стен утеплителем 50 мм (основа для отделки фасада декоративной штукатуркой)') ?></span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Внутреннее утепление наружных стен 40 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Внутреннее утепление наружных стен 40 мм') ?></span>
                        </div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Утепление кровли 250 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Утепление кровли 250 мм') ?></span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Фасады') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Наружная декоративная отделка (силикат-силиконовая штукатурка Ceresit "барашек" в два цвета)') ?></span>
                            <span class="cost-feature-small"><?= t('Наружная декоративная отделка (силикат-силиконовая штукатурка Ceresit "барашек" в два цвета)') ?></span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Водосточная система пластиковая') ?></span>
                            <span class="cost-feature-small"><?= t('Водосточная система пластиковая') ?></span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Наружные оконные отливы металличесие матовые') ?></span>
                            <span class="cost-feature-small"><?= t('Наружные оконные отливы металличесие матовые') ?></span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Подшив свеса крыши перфорированными пластиковыми соффитами') ?></span>
                            <span class="cost-feature-small"><?= t('Подшив свеса крыши перфорированными пластиковыми соффитами') ?></span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Металлическая входная дверь') ?></span>
                            <span class="cost-feature-small"><?= t('Металлическая входная дверь') ?></span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Звукоизоляция внутренних конструкций дома') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Звукоизоляции межэтажного перекрытия Isover 100 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Звукоизоляции межэтажного перекрытия Isover 100 мм') ?></span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Звукоизоляция внутренних стен Isover 150 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Звукоизоляция внутренних стен Isover 150 мм') ?></span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Коммуникации') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Разводка электрики: выводы электрокабеля под розетки и выключатели') ?></span>
                            <span class="cost-feature-small"><?= t('Разводка электрики: выводы электрокабеля под розетки и выключатели') ?></span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Разводка металлопластиковых труб отопления') ?></span>
                            <span class="cost-feature-small"><?= t('Разводка металлопластиковых труб отопления') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Разводка вентиляции: вытяжные вентиляторы из санузлов на улицу, отдельные вентканалы, согласно проекта.') ?></span>
                            <span class="cost-feature-small"><?= t('Разводка вентиляции: вытяжные вентиляторы из санузлов на улицу, отдельные вентканалы, согласно проекта.') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Разводка водопровода и канализации: выводы труб под сантехнические приборы (110 мм для унитаза и 50 мм для остальной сантехники)') ?></span>
                            <span class="cost-feature-small"><?= t('Разводка водопровода и канализации: выводы труб под сантехнические приборы (110 мм для унитаза и 50 мм для остальной сантехники)') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            <?= t('Внутренние отделочные работы') ?>
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Гипсокартонный потолок на металлическом каркасе') ?></span>
                            <span class="cost-feature-small"><?= t('Гипсокартонный потолок на металлическом каркасе') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Обшивка наружных стен гипсокартоном') ?></span>
                            <span class="cost-feature-small"><?= t('Обшивка наружных стен гипсокартоном') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Обшивка внутренних несущих стен гипсокартоном') ?></span>
                            <span class="cost-feature-small"><?= t('Обшивка внутренних несущих стен гипсокартоном') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Внутренние гипсокартонные перегородки на металлическом каркасе со звукоизоляцией') ?></span>
                            <span class="cost-feature-small"><?= t('Внутренние гипсокартонные перегородки на металлическом каркасе со звукоизоляцией') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Оконные откосы из гипсокартона') ?></span>
                            <span class="cost-feature-small"><?= t('Оконные откосы из гипсокартона') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Гидроизоляция бетонного пола') ?></span>
                            <span class="cost-feature-small"><?= t('Гидроизоляция бетонного пола') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Утепление бетонного пола 1-го этажа полистиролом 50 мм') ?></span>
                            <span class="cost-feature-small"><?= t('Утепление бетонного пола 1-го этажа полистиролом 50 мм') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Стяжка пола 1-го этажа под финишное покрытие') ?></span>
                            <span class="cost-feature-small"><?= t('Стяжка пола 1-го этажа под финишное покрытие') ?></span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>

                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Внутренняя декоративная отделка стен') ?></span>
                            <span class="cost-feature-small"><?= t('Внутренняя декоративная отделка стен') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж оконных подоконников из ПВХ') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж оконных подоконников из ПВХ') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Укладка ламината на пол') ?></span>
                            <span class="cost-feature-small"><?= t('Укладка ламината на пол') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Финишная отделка потолков') ?></span>
                            <span class="cost-feature-small"><?= t('Финишная отделка потолков') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Гидроизоляция мокрых зон в санузлах') ?></span>
                            <span class="cost-feature-small"><?= t('Гидроизоляция мокрых зон в санузлах') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Укладка плитки в санузлах') ?></span>
                            <span class="cost-feature-small"><?= t('Укладка плитки в санузлах') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж внутренних дверей') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж внутренних дверей') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж плинтусов') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж плинтусов') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж электрофурнитуры') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж электрофурнитуры') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж светильников') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж светильников') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж электробойлера 100л и электроконвекторов') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж электробойлера 100л и электроконвекторов') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж сантехприборов и смесителей') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж сантехприборов и смесителей') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж мебели в санузлах') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж мебели в санузлах') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full"><?= t('Монтаж вентиляторов в санузлах') ?></span>
                            <span class="cost-feature-small"><?= t('Монтаж вентиляторов в санузлах') ?></span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5 cost-check-icon"></div>
                    </div>
                </div>
            </div>

            <?php if (false): ?>
            <div class="cost-var no-print">
                <div class="cost-tr cost-var-head cost-var-head-project">
                    <div class="cost-td cost-feature"><?php echo $workScopes[1]['name']; ?></div>
                    <?php foreach($homeKits as $key => $homeKit): ?>
                        <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id' => $project->id, 'work_scope_id' => $workScopes[1]['id'], 'home_kit_id' => $homeKit->id)); ?>
                        <?php if ($key == 0 || $key == 1): ?>
                            <div class="cost-td"><?php echo str_replace(' ', '', price('{price}', $workScopePrice->price)); ?></div>
                        <?php else: ?>
                            <div class="cost-td">по запросу</div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="cost-td">по запросу</div>
                </div>
                <div class="cost-var-body">
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект базовых наружных стен высотой <strong>2885 мм</strong> с утеплением из базальтовой ваты 200мм и смонтированным гипсокартоном</span>
                            <span class="cost-feature-small">Наружные стены</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект внутренних стен со звукоизоляцией 100мм и смонтированным гипсокартоном</span>
                            <span class="cost-feature-small">Внутренние стены</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Межэтажные перекрытия (без звукоизоляции)</span>
                            <span class="cost-feature-small">Межэтажные перекрытия</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Кровельная система (стропила или стропильные фермы)</span>
                            <span class="cost-feature-small">Кровельная система</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж закладных под электропроводку в стенах</span>
                            <span class="cost-feature-small">Закладные под электрику</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект монтажных материалов</span>
                            <span class="cost-feature-small">Монтажные материалы</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-2 width-5 show_slider_1">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж домокомплекта за 1-3 дня</span>
                            <span class="cost-feature-small">Монтаж домокомплекта</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж кровельного покрытия - <strong>металлочерепица 0.45мм (Польша)</strong></span>
                            <span class="cost-feature-small">Кровельное покрытие</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-2 width-4 show_slider_2">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Утепление кровли, включая пароизоляцию <strong>Изовер Профи 200мм</strong></span>
                            <span class="cost-feature-small">Утепление кровли</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Утепление/звукоизоляция межэтажного перекрытия</span>
                            <span class="cost-feature-small">Утепление перекрытия</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Энергоэффективные белые <strong>окна Рехау Евро 70мм с энергоэффективным двухкамерным стеклопакетом 40мм с заполнением аргоном (4i - 14Ar - 4 - 14Ar - 4i)</strong></span>
                            <span class="cost-feature-small">Энергоэфф. окна</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">"Теплый" монтаж окон с тепло и парозащитой монтажного шва</span>
                            <span class="cost-feature-small">"Теплый" монтаж окон</span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-2 width-3 show_slider_3">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Фасадное утепление и отделка: <strong>утеплитель Neopor 50 mm, черновая отделка Baumit BauContact, декоративная штукатурка Baumit Stellartop</strong></span>
                            <span class="cost-feature-small">Фасадное утепление и отделка</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Внешние откосы</span>
                            <span class="cost-feature-small">Внешние откосы</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Водосточная система <strong>PVC (белая / коричневая)</strong></span>
                            <span class="cost-feature-small">Водосточная система</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Оконные отливы металлические (белые / коричневые)</span>
                            <span class="cost-feature-small">Оконные отливы</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Подшива свеса крыши: <strong>перфорированный софит PVC (белый / коричневый)</strong></span>
                            <span class="cost-feature-small">Подшива свеса крыши</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Внутренние откосы вокруг окон</span>
                            <span class="cost-feature-small">Внутренние откосы</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-2 width-2 show_slider_4">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж электрики</span>
                            <span class="cost-feature-small">Монтаж электрики</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж вентиляции</span>
                            <span class="cost-feature-small">Монтаж вентиляции</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж водопровода и канализации</span>
                            <span class="cost-feature-small">Монтаж водопровода</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж системы отопления</span>
                            <span class="cost-feature-small">Монтаж системы отопления</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Подшивка потолков гипсокартоном</span>
                            <span class="cost-feature-small">Подшивка потолков гипсокартоном</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Стяжки полов под финишное покрытие</span>
                            <span class="cost-feature-small">Стяжки полов под финишное покрытие</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Дополнительная звукоизоляция второго этажа от ударного шума (опция)</span>
                            <span class="cost-feature-small">Дополнительная звукоизоляция второго этажа от ударного шума (опция)</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-2 width-1 show_slider_5">Как выглядит домокомплект</a>
                    </div>
                </div>
            </div>

            <div class="cost-var no-print">
                <div class="cost-tr cost-var-head cost-var-head-project">
                    <div class="cost-td cost-feature"><?php echo $workScopes[2]['name']; ?></div>
                    <?php foreach($homeKits as $key => $homeKit): ?>
                        <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id' => $project->id, 'work_scope_id' => $workScopes[2]['id'], 'home_kit_id' => $homeKit->id)); ?>
                        <?php if ($key == 0 || $key == 1): ?>
                            <div class="cost-td"><?php echo str_replace(' ', '', price('{price}', $workScopePrice->price)); ?></div>
                        <?php else: ?>
                            <div class="cost-td">по запросу</div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="cost-td">по запросу</div>
                </div>
                <div class="cost-var-body">
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект базовых наружных стен высотой <strong>2885 мм</strong> с утеплением из базальтовой ваты 200мм и смонтированным гипсокартоном</span>
                            <span class="cost-feature-small">Наружные стены</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект внутренних стен со звукоизоляцией 100мм и смонтированным гипсокартоном</span>
                            <span class="cost-feature-small">Внутренние стены</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Межэтажные перекрытия (без звукоизоляции)</span>
                            <span class="cost-feature-small">Межэтажные перекрытия</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Кровельная система (стропила или стропильные фермы)</span>
                            <span class="cost-feature-small">Кровельная система</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж закладных под электропроводку в стенах</span>
                            <span class="cost-feature-small">Закладные под электрику</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Комплект монтажных материалов</span>
                            <span class="cost-feature-small">Монтажные материалы</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-3 width-5 show_slider_1">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж домокомплекта за 1-3 дня</span>
                            <span class="cost-feature-small">Монтаж домокомплекта</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж кровельного покрытия - <strong>битумная черепица Технониколь Кантри</strong></span>
                            <span class="cost-feature-small">Кровельное покрытие</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-3 width-4 show_slider_2">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Утепление кровли, включая пароизоляцию <strong>Изовер Профи 300мм</strong></span>
                            <span class="cost-feature-small">Утепление кровли</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Утепление/звукоизоляция межэтажного перекрытия</span>
                            <span class="cost-feature-small">Утепление перекрытия</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Энергоэффективные <strong>окна с наружной ламинацией (под дерево) Рехау Евро 70мм с энергоэффективным двухкамерным стеклопакетом 40мм с заполнением аргоном (4i - 14Ar - 4 - 14Ar - 4i), наружная ламинация или белые окна Рехау Генео 85мм с энергоэффективным двухкамерным стеклопакетом 44мм с заполнением аргоном (4i - 16Ar - 4 - 16Ar - 4i)</strong></span>
                            <span class="cost-feature-small">Энергоэфф. окна</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                            <span class="cost-feature-full">"Теплый" монтаж окон с тепло и парозащитой монтажного шва</span>
                            <span class="cost-feature-small">"Теплый" монтаж окон</span>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-3 width-3 show_slider_3">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Фасадное утепление и отделка: <strong>базальтовая вата Paroc 100мм с черновой отделкой Baumit ProContact, декоративная штукатурка Baumit Stellartop</strong></span>
                            <span class="cost-feature-small">Фасадное утепление и отделка</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Внешние откосы</span>
                            <span class="cost-feature-small">Внешние откосы</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Водосточная система <strong>Profil (цвет на выбор)</strong></span>
                            <span class="cost-feature-small">Водосточная система</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Оконные отливы <strong>алюминиевые(цвет на выбор)</strong></span>
                            <span class="cost-feature-small">Оконные отливы</span>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Подшива свеса крыши: <strong>перфорированный софит PVC (белый / коричневый)</strong></span>
                            <span class="cost-feature-small">Подшива свеса крыши</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-tr-bottom">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Внутренние откосы вокруг окон</span>
                            <span class="cost-feature-small">Внутренние откосы</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-3 width-2 show_slider_4">Как выглядит домокомплект</a>
                    </div>
                    <div class="cost-tr cost-tr-top">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж электрики</span>
                            <span class="cost-feature-small">Монтаж электрики</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж вентиляции</span>
                            <span class="cost-feature-small">Монтаж вентиляции</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж водопровода и канализации</span>
                            <span class="cost-feature-small">Монтаж водопровода</span>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Монтаж системы отопления</span>
                            <span class="cost-feature-small">Монтаж системы отопления</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Подшивка потолков гипсокартоном</span>
                            <span class="cost-feature-small">Подшивка потолков гипсокартоном</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Стяжки полов под финишное покрытие</span>
                            <span class="cost-feature-small">Стяжки полов под финишное покрытие</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Дополнительная звукоизоляция второго этажа от ударного шума (опция)</span>
                            <span class="cost-feature-small">Дополнительная звукоизоляция второго этажа от ударного шума (опция)</span>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                        <a href="" class="cost-btn cost-btn-mobile cost-btn-mobile-3 width-1 show_slider_5">Как выглядит домокомплект</a>
                    </div>
                </div>
            </div>

            <div class="cost-var">
                <div class="cost-tr cost-var-head cost-var-head-project">
                    <div class="cost-td cost-feature">EU export option. Euro**, exw.</div>
                    <?php foreach($homeKits as $key => $homeKit): ?>
                        <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id' => $project->id, 'work_scope_id' => $workScopes[3]['id'], 'home_kit_id' => $homeKit->id)); ?>
                        <?php if ($key == 0 || $key == 1): ?>
                            <div class="cost-td"><?php echo str_replace(' ', '', price('{price}', $workScopePrice->price)); ?></div>
                        <?php else: ?>
                            <div class="cost-td">by request</div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="cost-td">by request</div>
                </div>
                <div class="cost-var-body">
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>External walls</strong> with thickness 320mm and height 2800mm, and U = 0.15 W/K*m2.
                            External wall consists of:<br>
                            - <em>Neopor</em> (by BASF) external insulation 100mm protected by <em>Baumit</em> reinforcement mortar with glass mesh<br>
                            - OSB<br>
                            - Wood studs 145*45 with stone wool insulation <em>Paroc</em> 35kg/m3 (wood studs 195*45 on request)<br>
                            - Vapor barrier 0.15mm<br>
                            - Wood battens 45*45mm with glass wood insulation <em>Isover</em> Profi<br>
                            - Reinforced drywall <em>Knauf</em> Diamant
                        </span>
                            <span class="cost-feature-small">External walls</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-01.png" alt="External walls" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Internal walls</strong> consisting of<br>
                            - Reinforced drywall <em>Knauf</em> Diamant on both sides<br>
                            - Wood studs 95*45 with acoustic glass wool insulation <em>Isover</em>
                        </span>
                            <span class="cost-feature-small">Internal walls</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-02.png" alt="Internal walls" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Intermediate floor</strong> consisting of<br>
                            - OSB<br>
                            - wood beams 200mm
                        </span>
                            <span class="cost-feature-small">Intermediate floor</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-03.png" alt="Intermediate floor" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof trusses</strong> Mitek for pitched roofs
                        </span>
                            <span class="cost-feature-small">Roof trusses</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-04.png" alt="Roof trusses" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof trusses Mitek</strong><br>
                            OR<br>
                            <strong>Roof panels</strong> with rafters 240mm for flat roofs
                        </span>
                            <span class="cost-feature-small">Roof trusses Mitek</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-05.png" alt="Roof trusses Mitek" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof panels</strong> with rafters 200mm for insulated pitched roof
                        </span>
                            <span class="cost-feature-small">Roof panels</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-06.png" alt="Roof panels" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Electricity channels</strong> inside external and internal walls
                        </span>
                            <span class="cost-feature-small">Electricity channels</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-07.png" alt="Electricity channels" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Protection of external insulation with <em>Baumit</em> <strong>reinforcement mortar</strong>
                            and glass mesh
                        </span>
                            <span class="cost-feature-small">Reinforcement mortar</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-08.png" alt="Reinforcement mortar" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            White PVC <strong>energy efficient windows</strong> with triple glass 44mm (4i - 16Ar - 4 -
                            16Ar - 4i) and window frame 70mm. U window = 1.2 W/K*m2.<br>
                            - Color windows on request,<br>
                            - PVC windows with window frame 85mm and U = 0.85W/K*m2 on request.
                        </span>
                            <span class="cost-feature-small">Energy efficient windows</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-09.png" alt="Energy efficient windows" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Windows installation according to EU energy-efficient standards (triple insulation from
                            <em>illbruck</em>)
                        </span>
                            <span class="cost-feature-small">Windows installation</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-10.png" alt="Windows installation" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Set of installation materials</strong> (metal corners, ankers, wood screws 8mm,
                            nails, wood etc.)
                        </span>
                            <span class="cost-feature-small">Set of installation materials</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-11.png" alt="Set of installation materials" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-1">*</div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Housekit (walls, intermediate floors, and roof structures) installation
                            <a href="https://www.youtube.com/watch?v=CfJUxWYl1Cg" target="_blank">Youtube link to video of housekit installation</a>.
                        </span>
                            <span class="cost-feature-small">Housekit</span>
                            <div class="cost-feature-img" style="max-width: 160px; min-height: 84px;">
                                <iframe width="160" height="98" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://www.youtube.com/embed/CfJUxWYl1Cg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Metal <strong>tiles</strong><br>
                            OR<br>
                            bitumen shingles for pitched roof (Ceramic tiles on request) Screen reader support enabled.
                        </span>
                            <span class="cost-feature-small">Tiles</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-12.png" alt="Tiles" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Bitumen membrane</strong> for flat roof (PVC membrane on request)
                        </span>
                            <span class="cost-feature-small">Bitumen membrane</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-13.png" alt="Bitumen membrane" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof diffusion membrane</strong> <em>Isover</em> for protecting roof insulation and
                            wood impregnated <strong>battens</strong> (30x50 and 50x50) for tiles
                        </span>
                            <span class="cost-feature-small">Roof diffusion membrane</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-14.png" alt="Roof diffusion membrane" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-2">*</div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof insulation</strong> <em>Isover</em> Profi 300mm (U = 0.15 W/K*m2)
                            <strong>Vapour barrier</strong> with tapes for protecting roof insulation<br>
                            <strong>Wood battens</strong> 30x80mm
                        </span>
                            <span class="cost-feature-small">Roof insulation</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-15-1.png" alt="Roof insulation" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Acoustic insulation</strong> glass wool 100mm in intermediate floor
                        </span>
                            <span class="cost-feature-small">Acoustic insulation</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-16.png" alt="Acoustic insulation" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            PVC <strong>overhangs</strong> with metal planks installation on fascia under gutter
                        </span>
                            <span class="cost-feature-small">Overhangs</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-17.png" alt="Overhangs" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            PVC <strong>gutter</strong><br>
                            (metal gutter on request)
                        </span>
                            <span class="cost-feature-small">PVC gutter</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-18.png" alt="PVC gutter" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            nice-looking special PVC <strong>gutter</strong> for flat roofs(hidden gutter for flat
                            roofs on request)
                        </span>
                            <span class="cost-feature-small">Special PVC gutter</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-19.png" alt="Special PVC gutter" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-3 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            External metal <strong>window sills</strong>
                        </span>
                            <span class="cost-feature-small">External metal window sills</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-20.png" alt="External metal window sills" class="img-responsive">
                            </picture>
                        </div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-3">*</div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Finish stucco</strong> <em>Baumit</em> StellarporTop 2mm on walls<br>
                                - ceramic tiles on facade on request<br>
                                - wood siding on facade on request
                        </span>
                            <span class="cost-feature-small">Finish stucco</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-21.png" alt="Finish stucco" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Painting exterior wood columns and beams with lasur
                        </span>
                            <span class="cost-feature-small">Painting exterior</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-22.png" alt="Painting exterior" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">*</div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Additional <strong>insulation</strong> in roof or walls
                        </span>
                            <span class="cost-feature-small">Insulation</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-15-1.png" alt="Insulation" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Acoustic floating floor system</strong> consisting of mineral wool 30mm (180kg/m3)
                            and gypsum fibreboard <em>Knauf</em> 20mm on intermediate floors for additional sound
                            insulation
                        </span>
                            <span class="cost-feature-small">Acoustic floating floor system</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-23.png" alt="Acoustic floating floor system" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roof windows</strong> <em>Velux</em> Optima
                        </span>
                            <span class="cost-feature-small">Roof windows</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-24.png" alt="Roof windows" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Roller shutters</strong> with electric motorization management
                        </span>
                            <span class="cost-feature-small">Roller shutters</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-25.png" alt="Roller shutters" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Slide window systems</strong> on exit to terrace
                        </span>
                            <span class="cost-feature-small">Slide window systems</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-26.png" alt="Slide window systems" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            External sun shading system for windows (<strong>raffstores</strong>) with electric
                            motorization management
                        </span>
                            <span class="cost-feature-small">External sun shading system</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-27.png" alt="External sun shading system" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Entrance door</strong> <em>Hormann</em> Thermo46 (Hormann Thermo 65 or PVC doors on
                            request)
                        </span>
                            <span class="cost-feature-small">Entrance door</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/euro-kit-28.png" alt="Entrance door" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <em>Hormann</em> <strong>garage insulated door</strong> with Comunello automation (Alutech
                            garage inslulated doors on request)
                        </span>
                            <span class="cost-feature-small">Garage insulated door</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-29.png" alt="Garage insulated door" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            <strong>Wicket</strong> in garage door(Alutech solution here is much cheaper vs Hormann)
                        </span>
                            <span class="cost-feature-small">Wicket</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-30.png" alt="Wicket" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Ceramic chimney system <em>Schiedel</em> UNI for fireplace
                        </span>
                            <span class="cost-feature-small">Ceramic chimney system</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-31.png" alt="Ceramic chimney system" class="img-responsive">
                            </picture>
                            <a class="cost-feature-link cost-feature-link-light" href="">?</a>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-5 cost-feature cost-feature-name">
                        <span class="cost-feature-full">
                            Wood pergola for sun protection
                        </span>
                            <span class="cost-feature-small">Wood pergola for sun protection</span>
                            <picture class="cost-feature-img">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_euro-kit-32.png" alt="Wood pergola for sun protection" class="img-responsive">
                            </picture>
                        </div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5"></div>
                        <div class="cost-td gray-5">*</div>
                    </div>
                    <div class="cost-tr cost-print text-right">
                        <input type="button" value="Print" onclick="printCss(); window.print();" class="btn-print no-print">
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <?php if (false): ?>
        <p class="no-print"><em>Prices are actual for <?php echo config('price_notice_default_1') ?></em></p>
        <p class="no-print"><em>* - Цены указаны в гривнах на <?php echo config('price_notice_default_1') ?>.</em></p>
        <p class="no-print"><em>** - Оплата производится в гривне по курсу НБУ на день оплаты.</em></p>
        <?php endif; ?>

        <div class="cost-list no-print">
            <p style="margin-bottom: 35px;"><strong><?= t('Стоимость фундамента составляет 2200-2500 грн./м<sup>2</sup> общей площади дома,
            дополнительно к указанной стоимости строительства.') ?></strong></p>
            <h4 class="cost-list-descr"><?= t('Обязательные работы') ?></h4>
            <h5 class="cost-list-sub-descr"><?= t('просчитываются дополнительно согласно ТЗ, составленного с заказчиком:') ?></h5>
            <ul class="cost-list-inner">
                <li><?= t('подготовительные работы на участке,') ?></li>
                <li><?= t('земляные работы,') ?></li>
                <li><?= t('скважина, септик,') ?></li>
                <li><?= t('доставка домокомплекта, аренда техники, лессов для монтажа домокомплекта.') ?></li>
            </ul>
            <h4 class="cost-list-descr"><?= t('Дополнительные опции') ?></h4>
            <h5 class="cost-list-sub-descr"><?= t('согласовываются и просчитываются индивидуально:') ?></h5>
            <ul class="cost-list-inner">
                <li><?= t('дымоходы и камин,') ?></li>
                <li><?= t('входные навесы, террасы, перголы,') ?></li>
                <li><?= t('дополнительная звукоизоляция от ударного шума,') ?></li>
                <li><?= t('внутренняя лестница,') ?></li>
                <li><?= t('инженерное оборудование в доме,') ?></li>
                <li><?= t('ограждение и пол балконов,') ?></li>
                <li><?= t('мансардные окна.') ?></li>
            </ul>
        </div>

        <?php if (false): ?>
        <div class="kit-slider-wrap no-print" id="slider_5">
            <div class="kit-slider-overlay"></div>
            <div class="kit-slider">
                <div class="kit-slider-inner" id="kit_slider_5">
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Домокомплект Максимум</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-51.jpg" alt="">
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внешние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-52.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 56px; left: 90px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">OSB</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 68px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Деревянный каркас</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 126px; left: 93px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>150 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 192px; left: 116px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Паробарьер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 156px; left: 238px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 220px; left: 162px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 280px; left: 234px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 254px; left: 203px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 240px; left: 32px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 282px; left: 20px;">
                                    <span class="kit-hamburger-number">10</span>
                                    <span class="kit-hamburger-desc right">Отделка фасада</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внутренние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-53.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 40px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 50px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Стойки каркаса</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 130px; left: 82px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Минеральная вата <span>100 мм (150 мм)</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 152px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 202px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Перекрытие</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-54.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 94px; left: 118px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left" style="width: 96px;">Черновая стяжка пола</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 124px; left: 136px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 150px; left: 114px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Звукоизоляция от ударного шума <span>30 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 124px; left: 194px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 156px; left: 26px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc right">Брус перекрытия</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 168px; left: 178px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>100 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 164px; left: 258px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 196px; left: 208px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 236px; left: 194px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Кровля</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-55.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 100px; left: 136px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc right">Битумная черепица</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 176px; left: 178px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 68px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc right">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 144px; left: 62px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right" style="width: 120px;">Утеплитель минеральная вата</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 160px; left: 242px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc left">Подкладочный ковер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 194px; left: 244px;">
                                    <span class="kit-hamburger-number">10</span>
                                    <span class="kit-hamburger-desc left">Капельник</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 186px; left: 24px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 174px; left: 88px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right" style="width: 146px;">Фермы (брус стропильной системы)</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 248px; left: 244px;">
                                    <span class="kit-hamburger-number">11</span>
                                    <span class="kit-hamburger-desc left">Водосточный желоб</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 250px; left: 178px;">
                                    <span class="kit-hamburger-number">12</span>
                                    <span class="kit-hamburger-desc left">Перфорированый софит</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 274px; left: 216px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Лобовая доска</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 210px; left: 122px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="" class="kit-slider-close">✕</a>
            </div>
        </div>

        <div class="kit-slider-wrap no-print" id="slider_4">
            <div class="kit-slider-overlay"></div>
            <div class="kit-slider">
                <div class="kit-slider-inner" id="kit_slider_4">
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Домокомплект Стандарт + Фасад</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-41.jpg" alt="">
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внешние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-52.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 56px; left: 90px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">OSB</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 68px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Деревянный каркас</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 126px; left: 93px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>150 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 192px; left: 116px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Паробарьер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 156px; left: 238px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 220px; left: 162px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 280px; left: 234px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 254px; left: 203px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 240px; left: 32px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 282px; left: 20px;">
                                    <span class="kit-hamburger-number">10</span>
                                    <span class="kit-hamburger-desc right">Отделка фасада</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внутренние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-53.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 40px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 50px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Стойки каркаса</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 130px; left: 82px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Минеральная вата <span>100 мм (150 мм)</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 152px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 202px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Перекрытие</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-44.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 110px; left: 138px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 148px; left: 32px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Брус перекрытия</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 184px; left: 84px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>100 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 158px; left: 246px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 206px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Кровля</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-55.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 100px; left: 136px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc right">Битумная черепица</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 176px; left: 178px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 68px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc right">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 144px; left: 62px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right" style="width: 120px;">Утеплитель минеральная вата</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 160px; left: 242px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc left">Подкладочный ковер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 194px; left: 244px;">
                                    <span class="kit-hamburger-number">10</span>
                                    <span class="kit-hamburger-desc left">Капельник</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 186px; left: 24px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 174px; left: 88px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right" style="width: 146px;">Фермы (брус стропильной системы)</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 248px; left: 244px;">
                                    <span class="kit-hamburger-number">11</span>
                                    <span class="kit-hamburger-desc left">Водосточный желоб</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 250px; left: 178px;">
                                    <span class="kit-hamburger-number">12</span>
                                    <span class="kit-hamburger-desc left">Перфорированый софит</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 274px; left: 216px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Лобовая доска</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 210px; left: 122px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="" class="kit-slider-close">✕</a>
            </div>
        </div>

        <div class="kit-slider-wrap no-print" id="slider_3">
            <div class="kit-slider-overlay"></div>
            <div class="kit-slider">
                <div class="kit-slider-inner" id="kit_slider_3">
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Домокомплект Стандарт</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-31.jpg" alt="">
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внешние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-32.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 48px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">OSB</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 52px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Деревянный каркас</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 126px; left: 80px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>150 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 192px; left: 104px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Паробарьер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 152px; left: 238px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 220px; left: 146px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 280px; left: 234px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 254px; left: 198px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внутренние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-53.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 40px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 50px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Стойки каркаса</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 130px; left: 82px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Минеральная вата <span>100 мм (150 мм)</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 152px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 202px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Перекрытие</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-44.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 110px; left: 138px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 148px; left: 32px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Брус перекрытия</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 184px; left: 84px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>100 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 158px; left: 246px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 206px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Кровля</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-35.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 82px; left: 72px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 172px; left: 86px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Фермы (брус стропильной системы)</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 96px; left: 146px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc right">Битумная черепица</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 146px; left: 68px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right" style="width: 120px;">Утеплитель минеральная вата</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 172px; left: 178px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 158px; left: 252px;">
                                    <span class="kit-hamburger-number">9</span>
                                    <span class="kit-hamburger-desc left">Подкладочный ковер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 196px; left: 252px;">
                                    <span class="kit-hamburger-number">10</span>
                                    <span class="kit-hamburger-desc left">Капельник</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 258px; left: 224px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Лобовая доска</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 184px; left: 28px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 212px; left: 132px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Обрешетка</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="" class="kit-slider-close">✕</a>
            </div>
        </div>

        <div class="kit-slider-wrap no-print" id="slider_2">
            <div class="kit-slider-overlay"></div>
            <div class="kit-slider">
                <div class="kit-slider-inner" id="kit_slider_2">
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Домокомплект с Монтажем и Черепицей</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-21.jpg" alt="">
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внешние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-32.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 48px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">OSB</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 52px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Деревянный каркас</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 126px; left: 80px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>150 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 192px; left: 104px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Паробарьер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 152px; left: 238px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 220px; left: 146px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 280px; left: 234px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 254px; left: 198px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внутренние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-53.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 40px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 50px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Стойки каркаса</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 130px; left: 82px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Минеральная вата <span>100 мм (150 мм)</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 152px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 202px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Перекрытие</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-24.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 140px; left: 138px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 194px; left: 162px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc left">Брус перекрытия</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Кровля</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-25.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 82px; left: 72px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 174px; left: 86px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Фермы (брус стропильной системы)</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 96px; left: 146px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc right">Битумная черепица</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 172px; left: 178px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc left">Пароизоляция</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 158px; left: 248px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Подкладочный ковер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 248px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Капельник</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 264px; left: 218px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc left">Лобовая доска</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="" class="kit-slider-close">✕</a>
            </div>
        </div>

        <div class="kit-slider-wrap no-print" id="slider_1">
            <div class="kit-slider-overlay"></div>
            <div class="kit-slider">
                <div class="kit-slider-inner" id="kit_slider_1">
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Домокомплект без Монтажа</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-11.jpg" alt="">
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внешние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-32.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 48px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">OSB</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 52px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Деревянный каркас</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 126px; left: 80px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Утеплитель минеральная вата <span>150 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 192px; left: 104px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Паробарьер</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 152px; left: 238px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Обрешетка</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 220px; left: 146px;">
                                    <span class="kit-hamburger-number">6</span>
                                    <span class="kit-hamburger-desc left">Утеплитель минеральная вата <span>50 мм</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 280px; left: 234px;">
                                    <span class="kit-hamburger-number">7</span>
                                    <span class="kit-hamburger-desc left">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 254px; left: 198px;">
                                    <span class="kit-hamburger-number">8</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Внутренние стены</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-53.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 40px; left: 82px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 84px; left: 50px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc right">Стойки каркаса</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 130px; left: 82px;">
                                    <span class="kit-hamburger-number">3</span>
                                    <span class="kit-hamburger-desc right">Минеральная вата <span>100 мм (150 мм)</span></span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 200px; left: 152px;">
                                    <span class="kit-hamburger-number">4</span>
                                    <span class="kit-hamburger-desc right">Гипсокартон</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 232px; left: 202px;">
                                    <span class="kit-hamburger-number">5</span>
                                    <span class="kit-hamburger-desc left">Закладные под электрику</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Перекрытие</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-24.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 140px; left: 138px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left">ОСП</span>
                                </li>
                                <li class="kit-hamburger-item" style="top: 194px; left: 162px;">
                                    <span class="kit-hamburger-number">2</span>
                                    <span class="kit-hamburger-desc left">Брус перекрытия</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kit-slider-item">
                        <div class="kit-slider-item-title">Кровля</div>
                        <div class="kit-slider-item-img">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/homekit/_kit-slide-15_1.jpg" alt="">
                            <ul class="kit-hamburger">
                                <li class="kit-hamburger-item" style="top: 108px; left: 212px;">
                                    <span class="kit-hamburger-number">1</span>
                                    <span class="kit-hamburger-desc left" style="width: 142px;">Кровельная система (стропила или стропильные фермы), крепежные элементы</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="" class="kit-slider-close">✕</a>
            </div>
        </div>
        <?php endif; ?>
    </section>

    <div id="cost-form-wrapper no-print" style="display: none;">
        <?php $this->renderPartial('/project/_send_cost_form', array('project'=>$project));?>
    </div>

<!--    <div class="inline-recall-form">-->
<!--        --><?php //$this->renderPartial('recall_for_crm');?>
<!--    </div>-->

</div>

<?php

$priceAccuracy = config('price_accuracy');
$themeBaseUrl = app()->theme->baseUrl;
$project_cost_form_request_link = app()->createAbsoluteUrl('project/view', array('id'=>$project->id, 'uri'=>app()->translitFormatter->formatUrl($project->name)));
$print_project_link = url('project/print', array('id'=>$project->id));

$script = <<<JS

function round(a,b) {
    b=b || 0;
    return Math.round(a*Math.pow(10,b))/Math.pow(10,b);
}

$('#project_cost_form input').change(function(){
    var requestStr = $('#project_cost_form').serialize();
    var requestLink = '{$project_cost_form_request_link}?' + requestStr + '#project-tab4';
    $('#ProjectCostForm_link').val( requestLink );
    $('#print_project').attr('href', '{$print_project_link}' + '?' + requestStr);
});

$('#project_cost_form table tr:first-child input').change();

$('#show-send-link').click(function(){
    $(this).next().show();
    $(this).hide();
    return false;
});

$('#cost-form-wrapper').on('submit', '#send-price-from', function(){
    $(this).find('.recall-send').attr('disabled', 'disabled').val('идет отправка...');
    $.post($(this).attr('action'), $(this).serialize(), function(html){
        $('#cost-form-wrapper').html(html);
    });
    return false;
});

$('.project_cost_wrap').on('click', '#recall-cancel', function(){
    $('#show-send-link').show();
    $('#show-send-link').next().hide();
    return false;
});

JS;

cs()->registerScript('project-tab4', $script);

?>
