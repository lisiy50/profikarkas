<div class="other_versions clearfix">
    <div class="pull-left versions">
        <a href="#" class="img_wrap"><img src="<?php echo app()->theme->baseUrl;?>/pic/version.png"></a>
        <a href="#" class="version_bottom clearfix">
            <div class="z10 pull-left">Z10</div>
            <div class="square pull-left">97m2</div>
        </a>
        <div class="name"><strong>Проект Z10</strong>— бориспольское направление</div>
        <table>
            <tr>
                <td>Площадь застройки:</td>
                <td>71,7 m²</td>
            </tr>
            <tr>
                <td>Площадь по полу:</td>
                <td>134,46 m²</td>
            </tr>
            <tr>
                <td>Габариты дома:</td>
                <td>шир. 16,36м × дл. 16,58м</td>
            </tr>
        </table>
    </div>
    <div class="pull-left versions">
        <a href="#" class="img_wrap"><img src="<?php echo app()->theme->baseUrl;?>/pic/version2.png"></a>
        <a href="#" class="version_bottom clearfix">
            <div class="z10 pull-left">Z10</div>
            <div class="square pull-left">97m2</div>
        </a>
        <div class="name"><strong>Проект Z10</strong>— бориспольское направление</div>
        <table>
            <tr>
                <td>Площадь застройки:</td>
                <td>71,7 m²</td>
            </tr>
            <tr>
                <td>Площадь по полу:</td>
                <td>134,46 m²</td>
            </tr>
            <tr>
                <td>Габариты дома:</td>
                <td>шир. 16,36м × дл. 16,58м</td>
            </tr>
        </table>
    </div>
    <div class="pull-left versions">
        <a href="#" class="img_wrap"><img src="<?php echo app()->theme->baseUrl;?>/pic/version3.png"></a>
        <a href="#" class="version_bottom clearfix">
            <div class="z10 pull-left">Z10</div>
            <div class="square pull-left">97m2</div>
        </a>
        <div class="name"><strong>Проект Z10</strong>— бориспольское направление</div>
        <table>
            <tr>
                <td>Площадь застройки:</td>
                <td>71,7 m²</td>
            </tr>
            <tr>
                <td>Площадь по полу:</td>
                <td>134,46 m²</td>
            </tr>
            <tr>
                <td>Габариты дома:</td>
                <td>шир. 16,36м × дл. 16,58м</td>
            </tr>
        </table>
    </div>
</div>