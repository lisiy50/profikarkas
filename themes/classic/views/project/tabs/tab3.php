<?php
/**
 * @var $project Project
 */
?>

<div class="interior_wrap">
    <div class="interior">
        <h2 class="name"><?= t('Интерьеры дома') ?> <?php echo $project->getName() ?></h2>
        <div class="block clearfix">
            <?php foreach($project->iInteriors as $k=>$image):?>
                <a href="<?php echo $image->getImageUrl('origin');?>" class="fancybox" rel="interior">
                    <img src="<?php echo $image->getImageUrl('interiorSmall');?>"  alt="<?php echo $project->getName();?> интерьер <?php echo ($k+1)?>">
                </a>
            <?php endforeach;?>
        </div>
    </div>
</div>
