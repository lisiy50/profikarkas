<?php
/**
 * @var $project Project
 * @var PackagingHomeKit[] $packagingHomeKits
 * @var HomeKit[] $homeKits
 */
?>
<script>
    function printCss() {
        $('.project_cost_wrap').append("<style>@media print{.cost-table{padding-top:20px!important;}.cost-var-head,.cost-head{position: static;}.no-print,.cost-feature-img,.zsiq_flt_rel,#zsiq_maintitle,#zsiq_maintitle:after,#titlediv > div[title*='Оставьте ваше сообщение']:after,#zsiq_byline,.zsiq_theme1 div.zsiq_cnt,.similar-projects,.facades,.invitation,.home_projects_center,.projects_info{display:none;}}</style>");
    }
</script>

<div class="project_cost_wrap">
    <div class="project_cost_name no-print">
        <h2 style="margin-bottom: 22px;">House building cost <?php echo $project->getName() ?></h2>
    </div>

    <?php if (config('show_actual_cost')): ?>
    <a href="#" id="show-send-link" onclick="ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Actual price', {nonInteraction: true});">
        Актуальную стоимость <span>запрашивайте у менеджера</span>
    </a>
    <?php endif;?>

    <?php $homeKits = HomeKit::model()->findAll(); ?>
    <?php $workScopes = WorkScope::model()->findAll(); ?>

    <section class="cost">

        <div class="cost-table">

            <div class="cost-head">
                <div class="cost-head-inner">
                    <div class="cost-tr cost-head-name">
                        <div class="cost-td cost-feature title gray-1"></div>
                        <div class="cost-td cost-title cost-name cost-name-export gray-2">
                            Housekit set №1<br>
                            Housekit with installation including windows, roof, external insulation ready for facade works
                        </div>
                        <div class="cost-td cost-title cost-name cost-name-export gray-3">
                            Housekit set №2<br>
                            Housekit with installation + facade works
                        </div>
                        <div class="cost-td cost-title cost-name cost-name-export gray-4">
                            Housekit set №3<br>
                            Housekit with preparation for interior finishing works
                        </div>
                    </div>
                </div>
            </div>

            <div class="cost-tr cost-head-num cost-head-num-mobile no-print">
                <div class="cost-td cost-feature"></div>
                <div class="cost-td cost-title">№1</div>
                <div class="cost-td cost-title">№2</div>
                <div class="cost-td cost-title">№3</div>
            </div>

            <div class="cost-var no-print">
                <div class="cost-tr cost-var-head cost-var-head-project">
                    <div class="cost-td cost-feature"><strong>EU export option. EUR, exw.</strong></div>
                    <?php foreach ($homeKits as $key => $homeKit): ?>
                        <?php
                        $workScopePrice = WorkScopePrice::model()->findByPk(array(
                            'project_id' => $project->id,
                            'work_scope_id' => $workScopes[4]['id'],
                            'home_kit_id' => $homeKit->id
                        )); ?>
                        <?php if ($key != 3): ?>
                            <div class="cost-td">
                                <strong><?php echo price('{price}', $workScopePrice->price); ?></strong>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <div class="cost-var-body">
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-subtable">
                                <span class="cost-subtable-title">
                                    1m<sup>2</sup> (of internal area)
                                </span>
                                <span class="cost-subtable-value">EUR/m<sup>2</sup> (for information purpose only)</span>
                            </span>
                        </div>
                        <?php foreach ($homeKits as $key => $homeKit): ?>
                            <?php
                            $workScopePrice = WorkScopePrice::model()->findByPk(array(
                                'project_id' => $project->id,
                                'work_scope_id' => $workScopes[4]['id'],
                                'home_kit_id' => $homeKit->id
                            )); ?>
                            <?php if ($key != 3): ?>
                                <div class="cost-td gray-<?php echo $key + 2; ?>">
                                    <?php echo round($workScopePrice->price / $project->net_area); ?>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-subtable">
                                <span class="cost-subtable-title cost-subtable-title-offset gray-1">
                                    Terms of works for house &#60;200m2. For houses 200m2+ terms of
                                    works are a subject for negotiation.
                                </span>
                                <span class="cost-subtable-value">
                                    Project works (Development of production and installation drawings)
                                </span>
                            </span>
                        </div>
                        <div class="cost-td gray-2">2-8 weeks</div>
                        <div class="cost-td gray-3">2-8 weeks</div>
                        <div class="cost-td gray-4">2-8 weeks</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-subtable">
                                <span class="cost-subtable-title gray-1"></span>
                                <span class="cost-subtable-value">Housekit production</span>
                            </span>
                        </div>
                        <div class="cost-td gray-2">2-5 weeks</div>
                        <div class="cost-td gray-3">2-5 weeks</div>
                        <div class="cost-td gray-4">2-5 weeks</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-subtable">
                                <span class="cost-subtable-title gray-1"></span>
                                <span class="cost-subtable-value">Housekit installation</span>
                            </span>
                        </div>
                        <div class="cost-td red-1">2-5 days</div>
                        <div class="cost-td red-2">2-5 days</div>
                        <div class="cost-td red-3">2-5 days</div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-subtable">
                                <span class="cost-subtable-title gray-1"></span>
                                <span class="cost-subtable-value">Other construction works (roof, facade, etc.)</span>
                            </span>
                        </div>
                        <div class="cost-td gray-2">2 weeks</div>
                        <div class="cost-td gray-3">3-4 weeks</div>
                        <div class="cost-td gray-4">4-6 weeks</div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Housekit
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Wall panels and intermediate floor panels production (up to
                                8m long) at Profikarkas factory</span>
                            <span class="cost-feature-small">Wall panels and intermediate floor panels production (up to
                                8m long) at Profikarkas factory</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-full">
                                <strong>External walls</strong>  with thickness 257mm, height 2800mm and U = 0.2
                                W/K*m<sup>2</sup> (R=5) consist of:
                                <ol>
                                    <li>52mm wood fiber external insulation</li>
                                    <li>wood frame from CE certified С24 construction wood studs KVH 60*140mm with stone
                                        wool insulation 35kg/m<sup>3</sup></li>
                                    <li>OSB 12mm</li>
                                    <li>vapour barrier film</li>
                                    <li>40mm wood fiber internal insulation with channels for electricity</li>
                                </ol>
                                <i style="display: inline-block; margin-top: 5px;">- walls with different structure, more insulation, lower U value, etc.
                                    and on request</i>
                            </div>
                            <div class="cost-feature-small">
                                <strong>External walls</strong>  with thickness 257mm, height 2800mm and U = 0.2
                                W/K*m<sup>2</sup> (R=5) consist of:
                                <ol>
                                    <li>52mm wood fiber external insulation</li>
                                    <li>wood frame from CE certified С24 construction wood studs KVH 60*140mm with stone
                                        wool insulation 35kg/m<sup>3</sup></li>
                                    <li>OSB 12mm</li>
                                    <li>vapour barrier film</li>
                                    <li>40mm wood fiber internal insulation with channels for electricity</li>
                                </ol>
                                <i style="display: inline-block; margin-top: 5px;">- walls with different structure, more insulation, lower U value, etc.
                                    and on request</i>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Set of internal load-bearing (structure) walls from CE
                                certified С24 construction wood studs KVH 60*140mm</span>
                            <span class="cost-feature-small">Set of internal load-bearing (structure) walls from CE
                                certified С24 construction wood studs KVH 60*140mm</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-full">
                                Wood roof structure:<br>
                                wood roof trusses with nailed plates Mitek (www.mitek.de) OR<br>
                                wood frame from CE certified C24 construction wood beams KVH 60*200mm.<br>
                                Wood battens for tiles and roof membranes are included (delivered loose)<br><br>
                                Roof parapets for flat roofs (if applicable) consist of
                                <ol>
                                    <li> OSB 12mm</li>
                                    <li> wood frame from CE certified construction wood studs KVH 60*100mm</li>
                                    <li> 52mm wood fiber external insulation (delivered loose)</li>
                                </ol>
                            </div>
                            <div class="cost-feature-small">
                                Wood roof structure:<br>
                                wood roof trusses with nailed plates Mitek (www.mitek.de) OR<br>
                                wood frame from CE certified C24 construction wood beams KVH 60*200mm.<br>
                                Wood battens for tiles and roof membranes are included (delivered loose)<br><br>
                                Roof parapets for flat roofs (if applicable) consist of
                                <ol>
                                    <li> OSB 12mm</li>
                                    <li> wood frame from CE certified construction wood studs KVH 60*100mm</li>
                                    <li> 52mm wood fiber external insulation (delivered loose)</li>
                                </ol>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-full">
                                Intermediate floor panels (if applicable) consist of
                                <ol>
                                    <li> OSB 22mm</li>
                                    <li> wood frame from CE certified C24 construction wood beams KVH 60*200mm</li>
                                    <li> acoustic glass wool insulation Isover Profi 100mm (delivered loose)</li>
                                    <li> Wood battens 30x50mm (delivered loose)</li>
                                </ol>
                            </div>
                            <div class="cost-feature-small">
                                Intermediate floor panels (if applicable) consist of
                                <ol>
                                    <li> OSB 22mm</li>
                                    <li> wood frame from CE certified C24 construction wood beams KVH 60*200mm</li>
                                    <li> acoustic glass wool insulation Isover Profi 100mm (delivered loose)</li>
                                    <li> Wood battens 30x50mm (delivered loose)</li>
                                </ol>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Concrete foundation
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Has to be provided by buyer according to dimensions provided
                                by ProfiKarkas</span>
                            <span class="cost-feature-small">Has to be provided by buyer according to dimensions
                                provided by ProfiKarkas</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3"></div>
                        <div class="cost-td gray-4"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Windows
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                Dark grey (Antracite) or white outside and white inside PVC <strong>energy efficient
                                windows</strong> with triple glass 44mm (4i - 16 - 4 - 16 - 4i) and window frame 70mm. U
                                window = 1.2 W/K*m<sup>2</sup>.<br>
                                Windows installation according to EU energy-efficient standards (triple protection).<br>
                                <i style="display: inline-block; margin-top: 5px;">- Color windows on request,</i>
                                <i style="display: inline-block; margin-top: 5px;">- PVC windows with window frame 85mm and U = 0.85W/K*m<sup>2</sup> on
                                    request</i>
                            </span>
                            <span class="cost-feature-small">
                                Dark grey (Antracite) or white outside and white inside PVC <strong>energy efficient
                                windows</strong> with triple glass 44mm (4i - 16 - 4 - 16 - 4i) and window frame 70mm. U
                                window = 1.2 W/K*m<sup>2</sup>.<br>
                                Windows installation according to EU energy-efficient standards (triple protection).<br>
                                <i style="display: inline-block; margin-top: 5px;">- Color windows on request,</i>
                                <i style="display: inline-block; margin-top: 5px;">- PVC windows with window frame 85mm and U = 0.85W/K*m<sup>2</sup> on
                                request</i>
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Roof tiles
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-full">
                                Metal roof tiles 0.45mm dark grey (antracite RAL7024) color matt for pitched roof OR<br>
                                PVC membrane with XPS slope insulation 50-150mm for flat roofs (the most durable
                                solution for flat roofs)
                            </div>
                            <div class="cost-feature-small">
                                Metal roof tiles 0.45mm dark grey (antracite RAL7024) color matt for pitched roof OR<br>
                                PVC membrane with XPS slope insulation 50-150mm for flat roofs (the most durable
                                solution for flat roofs)
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Housekit with roof tiles installation
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <div class="cost-feature-video" style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
                                <iframe style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/g3JW18J56wY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Thermal insulation of exterior building envelope
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Thermal insulation of external walls between wall studs with
                                stone wool 140mm</span>
                            <span class="cost-feature-small">Thermal insulation of external walls between wall studs
                                with stone wool 140mm</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Outside thermal insulation of external wall studs with wood
                                fibre insulation 50mm (suitable for facade finishing decorative plaster)</span>
                            <span class="cost-feature-small">Outside thermal insulation of external wall studs with wood
                                fibre insulation 50mm (suitable for facade finishing decorative plaster)</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Internal thermal insulation of external walls with wood
                                fibre insulation 40mm</span>
                            <span class="cost-feature-small">Internal thermal insulation of external walls with wood
                                fibre insulation 40mm</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2 cost-check-icon"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-1 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Thermal insulation of roof with Isover Profi 250 mm,
                                including vapor barrier and wood battens 30x50</span>
                            <span class="cost-feature-small">Thermal insulation of roof with Isover Profi 250 mm,
                                including vapor barrier and wood battens 30x50</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>

                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Facades and exterior house decoration
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Protection of external insulation with reinforcement mortar
                                and glass mesh with special profiles on edges and connections.</span>
                            <span class="cost-feature-small">Protection of external insulation with reinforcement mortar
                                and glass mesh with special profiles on edges and connections.</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                <strong>Finish plaster</strong> (two colors) on walls<br>
                                <i style="display: inline-block; margin-top: 5px;">- ceramic tiles on facade on request</i>
                                <i style="display: inline-block; margin-top: 5px;">- wood siding on facade on request</i>
                            </span>
                            <span class="cost-feature-small">
                                <strong>Finish plaster</strong> (two colors) on walls
                                <i style="display: inline-block; margin-top: 5px;">- ceramic tiles on facade on request</i>
                                <i style="display: inline-block; margin-top: 5px;">- wood siding on facade on request</i>
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Painting exterior wood columns and beams with lasur</span>
                            <span class="cost-feature-small">Painting exterior wood columns and beams with lasur</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                Dark grey (antricite RAL7024) or white PVC overhangs with metal planks installation on
                                fascia under gutter for pitched roofs OR<br>
                                Metal copings on parapet walls for flat roofs
                            </span>
                            <span class="cost-feature-small">
                                Dark grey (antricite RAL7024) or white PVC overhangs with metal planks installation on
                                fascia under gutter for pitched roofs OR<br>
                                Metal copings on parapet walls for flat roofs
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                Dark grey (Antracite) or white PVC gutter
                            </span>
                            <span class="cost-feature-small">
                                Dark grey (Antracite) or white PVC gutter
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-2 cost-feature cost-feature-name">
                            <span class="cost-feature-full">External metal window sills according to windows color.</span>
                            <span class="cost-feature-small">External metal window sills according to windows color.</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-2"></div>
                        <div class="cost-td gray-3 cost-check-icon"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>

                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Sound insulation of internal house structures
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">sound insulation of intermediate floors with Isover 100mm +
                                wood 30x50</span>
                            <span class="cost-feature-small">sound insulation of intermediate floors with Isover 100mm +
                                wood 30x50</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">on request</div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">sound insulation of internal walls with Isover 100 or 140mm</span>
                            <span class="cost-feature-small">sound insulation of internal walls with Isover 100 or 140mm</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">on request</div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>

                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Internal engineering systems
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of monolith copper electric cables (3x2.5 for
                                electric sockets and 3x1.5 for lighting) with fire resistant envelope in flexible wiring
                                plastic pipes (3 sockets, 1 switch and 1 light point in each room)</span>
                            <span class="cost-feature-small">Installation of monolith copper electric cables (3x2.5 for
                                electric sockets and 3x1.5 for lighting) with fire resistant envelope in flexible wiring
                                plastic pipes (3 sockets, 1 switch and 1 light point in each room)</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of ventilation: exhaust air ducts from WCs to
                                the street and individual air ducts according to the project.</span>
                            <span class="cost-feature-small">Installation of ventilation: exhaust air ducts from WCs to
                                the street and individual air ducts according to the project.</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of plastic water pipes (hot and cold water)</span>
                            <span class="cost-feature-small">Installation of plastic water pipes (hot and cold water)</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of sewage pipes - 110mm pipes for toilets and
                                50mm for other devices (showers, sinks, etc.) inside house above basement</span>
                            <span class="cost-feature-small">Installation of sewage pipes - 110mm pipes for toilets and
                                50mm for other devices (showers, sinks, etc.) inside house above basement</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>

                    <div class="cost-tr cost-subtitle">
                        <div class="cost-td cost-feature cost-feature-name">
                            Internal plasterboard and screed works
                        </div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                        <div class="cost-td"></div>
                    </div>

                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of plasterboard ceiling on metal frame</span>
                            <span class="cost-feature-small">Installation of plasterboard ceiling on metal frame</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                Installation of green plasterboard Knauf with improved moisture resistance on external
                                and internal load-bearing walls
                                <i style="display: inline-block; margin-top: 5px;">- new generation of reinforced with glass fiber plasterboard Knauf Diamond
                                    (DFH2IR) with improved fire, moisture and sound resistance on request.</i>
                            </span>
                            <span class="cost-feature-small">
                                Installation of green plasterboard Knauf with improved moisture resistance on external
                                and internal load-bearing walls
                                <i style="display: inline-block; margin-top: 5px;">- new generation of reinforced with glass fiber plasterboard Knauf Diamond
                                    (DFH2IR) with improved fire, moisture and sound resistance on request.</i>
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">on request</div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">
                                Internal partition walls (wood frame 50x80 + sound insulation Isover Profi + green
                                plasterboard Knauf with improved moisture resistance)
                                <i style="display: inline-block; margin-top: 5px;">- new generation of reinforced with glass fiber plasterboard Knauf Diamond
                                    (DFH2IR) with improved fire, moisture and sound resistance on request.</i>
                            </span>
                            <span class="cost-feature-small">
                                Internal partition walls (wood frame 50x80 + sound insulation Isover Profi + green
                                plasterboard Knauf with improved moisture resistance)
                                <i style="display: inline-block; margin-top: 5px;">- new generation of reinforced with glass fiber plasterboard Knauf Diamond
                                    (DFH2IR) with improved fire, moisture and sound resistance on request.</i>
                            </span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">on request</div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Installation of plasterboard on window slopes</span>
                            <span class="cost-feature-small">Installation of plasterboard on window slopes</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4">on request</div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Waterproofing of concrete slab on the ground</span>
                            <span class="cost-feature-small">Waterproofing of concrete slab on the ground</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Floor insulation EPS 50mm under screed</span>
                            <span class="cost-feature-small">Floor insulation EPS 50mm under screed</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                    <div class="cost-tr">
                        <div class="cost-td gray-4 cost-feature cost-feature-name">
                            <span class="cost-feature-full">Screed on ground floor</span>
                            <span class="cost-feature-small">Screed on ground floor</span>
                            <a class="cost-feature-link" href="">?</a>
                        </div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4"></div>
                        <div class="cost-td gray-4 cost-check-icon"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="cost-list no-print">
            <?php if (false): ?>
            <p style="margin-bottom: 35px;font-size: 16px;font-style: italic;">
                Offer is valid till <?= config('price_notice_default_1') ?>
            </p>
            <?php endif; ?>
            <h4 class="cost-list-descr">IMPORTANT NOTE</h4>
            <h5 class="cost-list-sub-descr">For housekit installation customer has to provide:</h5>
            <ul class="cost-list-inner">
                <li>autocrane 30-60t</li>
                <li>construction light scaffolding (like for facade works) around house</li>
                <li>electricity 220-240v 5kw</li>
                <li>toilet at construction site</li>
                <li>solid (gravel or asphalt) road for trucks to construction site</li>
                <li>appartment 1km around construction site with kitchen and 6 separate beds for construction team</li>
            </ul>
            <h4 class="cost-list-descr">DELIVERY COST</h4>
            <p class="">~70m<sup>2</sup> of housekit / truck + sometimes (depends on total house area) additional 1
                truck for things you want to buy additionally to Housekit set #1 (facade materials, plasterboard for
                ceiling, garage door, additional insulation, etc). Cost of 1 truck is ~1eur/km on land + 350-500euros
                for customs agency fee and insurance + cost of ferry if it is needed for delivery.</p>
        </div>
    </section>
</div>

