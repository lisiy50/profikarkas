<div class="recomend-form-wrapper">
    <h3><?=t('Порекомендовать проект знакомому')?></h3>

    <div class="recomend-project-info">
        <div class="recomend-project-image">
            <img width="148" style="float: left;" src="<?php echo $projectModel->getFirstImageUrl()?$projectModel->getFirstImageUrl()->getImageUrl('small'):'';?>">
            <div class="recomend-project-symbol">
                <?php echo $projectModel->symbol;?>
            </div>
        </div>
        <strong>Проект <?php echo $projectModel->symbol;?></strong> <?php echo $projectModel->title;?>
    </div>

    <?php if(Yii::app()->user->hasFlash('contact')): ?>
        <div class="alert alert-success">
            <?php echo Yii::app()->user->getFlash('contact'); ?>
        </div>
    <?php else: ?>

        <?php $form=$this->beginWidget('BootActiveForm', array(
            'enableClientValidation'=>true,
            'action' => url('project/recomend', array('id'=>$projectModel->id)),
            'id' => 'write_the_company_form',
            'type' => 'horizontal',
            'htmlOptions' => array(
                'onsubmit' => "ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Recommend', {nonInteraction: true});",
            ),
        )); ?>

        <?php if(false && $model->hasErrors()):?>
            <?php echo $form->errorSummary($model); ?>
        <?php endif;?>

        <div class="recomend-project-form-row">
            <?php
            echo $form->labelEx($model, 'email', array('class'=>''));
            echo $form->textField($model, 'email', array('class'=>'', 'placeholder'=>'name@profikarkas.com'))."\n";
            ?>
        </div>
        <div class="recomend-project-form-row">
            <?php
            echo $form->labelEx($model, 'body', array('class'=>''));
            echo $form->textArea($model, 'body', array('class'=>'', 'style'=>'resize:vertical;', 'placeholder'=>t('введите Ваше сообщение...')))."\n";
            ?>
        </div>
        <div class="recomend-project-form-row">
            <?php
            echo $form->labelEx($model, 'senderEmail', array('class'=>''));
            echo $form->textField($model, 'senderEmail', array('class'=>''))."\n";
            ?>
        </div>

        <span class="recomend-project-required-fields">
            * <?= t('обязательные данные') ?>
        </span>

        <div class="recomend-project-chekbox-wrapper">
            <?php echo $form->checkBox($model, 'copy', array('style'=>'display:none;'));?>
            <label for="RecomendProjectForm_copy">
                <div class="recomend-project-chekbox"></div>
                <?php $label = $model->attributeLabels(); echo $label['copy'];?>
            </label>
        </div>

        <div class="recomend-project-chekbox-wrapper">
            <?php echo $form->checkBox($model, 'agreement', array('style'=>'display:none;'));?>
            <label for="RecomendProjectForm_agreement">
                <div class="recomend-project-chekbox"></div>
                <?php $label = $model->attributeLabels(); echo $label['agreement'];?>
            </label>
        </div>

        <span class="recomend-project-agreement">
            <?= t('Компания Профикаркас гарантирует конфиденциальность введенной Вами информации') ?>
        </span>

        <?php if(false && CCaptcha::checkRequirements() && Yii::app()->config['contact_use_captcha']): ?>
            <div class="control-group">
                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model, 'verifyCode', array('class'=>'input-small')); ?>
                <span class="help-block"><?=t('Пожалуйста, введите буквы, изображенные на картинке выше.')?></span>
            </div>
        <?php endif; ?>

        <?php echo CHtml::submitButton(t('Отправить сообщение'), array('class'=>"recomend-project-send-btn")); ?>
        <?php //echo CHtml::link(t('X'), 'javascript:$.fancybox.close();',array('class'=>"recomend-close")); ?>

        <?php $this->endWidget(); ?>

    <?php endif; ?>

    <script type="text/javascript">
        jQuery(function(){
            $('#write_the_company_form').submit(function(){
                $.fancybox.showLoading();
                $.post($(this).attr('action'), $(this).serialize(), function(html){
                    $.fancybox.hideLoading();
                    $('.fancybox-inner').html(html);
                })
                return false;
            });

            $('#RecomendProjectForm_copy').change(function(){
                if($(this).is(':checked')){
                    $(this).closest('.recomend-project-chekbox-wrapper').find('.recomend-project-chekbox').addClass('active');
                } else {
                    $(this).closest('.recomend-project-chekbox-wrapper').find('.recomend-project-chekbox').removeClass('active');
                }
            });

            $('#RecomendProjectForm_agreement').change(function(){
                if($(this).is(':checked')){
                    $(this).closest('.recomend-project-chekbox-wrapper').find('.recomend-project-chekbox').addClass('active');
                    $('.recomend-project-send-btn').addClass('active').removeAttr('disabled');
                } else {
                    $(this).closest('.recomend-project-chekbox-wrapper').find('.recomend-project-chekbox').removeClass('active');
                    $('.recomend-project-send-btn').removeClass('active').attr('disabled', 'true');
                }
            });
            $('#RecomendProjectForm_agreement, #RecomendProjectForm_copy').change();
        })
    </script>
</div>
