<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 11/15/14
 * Time: 4:55 PM
 * @var $form BootActiveForm
 * @var $model ProjectCostForm
 * @var $project Project
 */
if(!isset($model))
    $model = new ProjectCostForm();
if(isset($project))
    $model->project_name = $project->getName();
?>
<?php if($message):?>
    <div class="alert alert-success" role="alert"><?php echo $message ?></div>
<?php else: ?>
    <?php
    $form=$this->beginWidget('BootActiveForm', array(
    //    'enableClientValidation'=>true,
        'enableAjaxValidation'=>false,
        'action' => url('project/sendPrice'),
        'id' => 'send-price-from',
        'htmlOptions' => array(
            'onclick' => "ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Actual price', {nonInteraction: true});",
        ),
    ));
    ?>
    <?php //echo $form->errorSummary($model);?>
    <div class="label-message"><?= t('Выберите, пожалуйста, тип домокомплекта стоимось которого вы хотите узнать:') ?> <?php echo $form->dropDownList($model, 'type', $model->getTypeList())."\n";?></div>
    <div class="label-message"><?= t('Введите адрес электронной почты, на который будет отправлен просчет:') ?></div>

    <?php echo $form->hiddenField($model, 'link', array('style'=>'width:100%;border:0;background:none;','readonly'=>true));?>


    <div class="clearfix">
        <div class="pull-left">
            <?php //echo $form->textField($model, 'email', array('class'=>'form-control email-field', 'placeholder'=>'email'))."\n";?>
            <?php echo $form->textFieldRow($model, 'email', array('class'=>'form-control email-field'))."\n";?>
        </div>
        <div class="pull-left">
            <?php echo CHtml::submitButton(t('Отправить'), array('class'=>"recall-send"));?> <?php echo CHtml::submitButton(t('Отменить'), array('id'=>"recall-cancel"));?>
        </div>
    </div>

    <?php echo $form->hiddenField($model, 'project_name')."\n";?>


    <?php $this->endWidget();?>
<?php endif ?>
