<div class="form_wrap clearfix">
    <?php $this->renderPartial('_search', array('projectModel'=>$projectModel, 'dataProvider'=>$dataProvider));?>
</div>

<div class="catalog clearfix" id="project-catalog" style="width: 1000px;">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<div class="invitation" style="margin-bottom: 20px;"></div>

<?php if(!app()->request->queryString):?>
    <div class="clearfix">
        <?php
        if(Yii::app()->controller->action->id=='dachi'){

            if (Yii::app()->language == 'ru') {
                echo config('product_categiry_dachi');
            } elseif (Yii::app()->language == 'uk') {
                echo config('product_categiry_dachi_uk');
            }

        } elseif(Yii::app()->controller->action->id=='zagorodnie') {

            if (Yii::app()->language == 'ru') {
                echo config('product_categiry_zagorodnie');
            } elseif (Yii::app()->language == 'uk') {
                echo config('product_categiry_zagorodnie_uk');
            }

        } else {

            if (Yii::app()->language == 'ru') {
                echo config('product_text');
            } elseif (Yii::app()->language == 'uk') {
                echo config('product_text_uk');
            }
        }
        ?>
    </div>
<?php endif;?>

<?php
$script = <<<JS

    $('#project-catalog').on('click', '.project-pagination .next a', function(){
        var lastCrossLink = {};
        var id = ($('.whats_good:last').data('id'));
        lastCrossLink.lastCrossLink = id;
        $(this).addClass('disabled');
        $('.items-loader').show();
        $.get($(this).attr('href'), lastCrossLink, function(html){
            $('#project-pager, .items-loader').remove();
            $('#project-catalog').append(html);
            addCrossLinksSpace();
            init();
        });
        return false;
    });

    $.post('site/signUpForDemoHouse', function(html) {
        $('.invitation').html(html);
        $('#SignUpForDemoHouse_subject').val('Запрос на поездку в демо-дом. Каталог проектов');
    });

    $('.invitation').on('submit', '#signUpForDemoHouseForm', function () {
        $.post($.createUrl('site/signUpForDemoHouse'), $(this).serialize(), function (html) {
            $('#invitationInner').remove();
            $('.invitation').html(html);
        });
        return false;
    });

JS;

cs()->registerScript('project-index', $script);

cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/sign.up.for.consultation.js');
?>
