<?php
/**
 * @var $project Project
 * @var WorkScopePrice $workScopePrice
 */
$workScopePrice = $project->getLowWorkScopePrice();
?>
<div class="best_build pull-left">
    <a href="<?php echo $project->getUrl();?>" class="img_wrap">
        <?php if($project->getVideoWalks()): ?>
            <div class="has-video-walk">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/img/360big.png">
                <?= t('Видео прогулка по дому') ?>
            </div>
        <?php endif; ?>
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?php echo $project->getFirstImageUrl()?$project->getFirstImageUrl()->getImageUrl('small'):'';?>" alt="Проект дома <?php echo $project->symbol ?> иллюстрация 1">
    </a>
    <a href="<?php echo $project->getUrl();?>" class="best_build_bottom clearfix">
        <div class="pull-left cat_name"><?php echo $project->getName();?></div>
        <div class="pull-left square_m"><?php echo $project->net_area;?>m<sup>2</sup></div>
        <?php if($workScopePrice): ?>
        <div style="display: none;">
            <?php
            var_dump($workScopePrice);
            ?>
        </div>
        <div class="pull-left name" title="<?php echo $project->title;?>"><span><?= t('домокомплект от') ?></span> <?php echo price('{price} {short}.', $workScopePrice->price);?></div>
        <?php elseif($project->id == 155): ?>
            <div class="pull-left name" title="<?php echo $project->title;?>"> 700 000 <?= t('грн.') ?></div>
        <?php elseif($project->id == 154): ?>
            <div class="pull-left name" title="<?php echo $project->title;?>"> 600 000 <?= t('грн.') ?></div>
        <?php elseif($project->id == 153): ?>
            <div class="pull-left name" title="<?php echo $project->title;?>"> 500 000 <?= t('грн.') ?></div>
        <?php endif ?>
    </a>
</div>
