<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => get_class($projectModel) . '-search-form',
    'method' => 'get',
    'action' => url(Yii::app()->controller->id.'/'.Yii::app()->controller->action->id),
));
?>
    <div class="form_block pull-left">
        <div class="form_name"><?= t('Площадь') ?></div>
        <?php
        $min_val = 30;
        $max_val = 250;
        $from=isset($_GET['Project']['net_area']['from'])?$_GET['Project']['net_area']['from']:$min_val;
        $till=isset($_GET['Project']['net_area']['till'])?$_GET['Project']['net_area']['till']: $max_val;
        $this->widget('zii.widgets.jui.CJuiSlider', array(
            'options'=>array(
                'range'=>true,
                'min'=>$min_val,
                'max'=>$max_val,
                'values'=>array($from, $till),
                'slide'=>"js:function(event, ui){
                                $('#Project_net_area_from').val(ui.values[0]);
                                $('#Project_net_area_till').val(ui.values[1]);
                            }",
                'change' => 'js:function( event, ui ){ $("#Project-search-form").submit(); }',
            ),
            'htmlOptions'=>array(
                'id'=>'Project_net_area_slider',
            )
        ));
        ?>
        <div class="for_cost">
            от
            <?php
            $val = $from;
            isset($_GET['Project']['net_area']['from']) && $val=$_GET['Project']['net_area']['from'];
            echo CHtml::textField('Project[net_area][from]', $val, array(
                'size'=>3,
                'placeholder'=>$min_val,
                'onchange'=>"$('#Project_net_area_slider').slider('values', [$('#Project_net_area_from').val(), $('#Project_net_area_till').val()])",
            ));
            ?>

            до
            <?php
            $val = $till;
            isset($_GET['Project']['net_area']['till']) && $val=$_GET['Project']['net_area']['till'];
            echo CHtml::textField('Project[net_area][till]', $val, array(
                'size'=>3,
                'placeholder'=>'250',
                'onchange'=>"$('#Project_net_area_slider').slider('values', [$('#Project_net_area_from').val(), $('#Project_net_area_till').val()])",
            ));
            ?>
            m<sup>2</sup>
        </div>

    </div>
    <div class="form_block pull-left">

        <div class="form_name"><?= t('Категории') ?>:</div>
        <a class="filter-link" href="<?php echo Yii::app()->controller->action->id=='dachi' ? url('project/index') : url('project/dachi')?>">
            <?php echo CHtml::checkBox('dachi', Yii::app()->controller->action->id=='dachi', array('class'=>'linkInput'));?> <?= t('Дачные дома') ?>
        </a>
        <br>
        <a class="filter-link" href="<?php echo Yii::app()->controller->action->id=='zagorodnie' ? url('project/index') : url('project/zagorodnie')?>">
            <?php echo CHtml::checkBox('zagorodnie', Yii::app()->controller->action->id=='zagorodnie', array('class'=>'linkInput'));?> <?= t('Загородные дома') ?>
        </a>
        <div class="checkbox" style="margin-top: 0; padding-left: 17px;">
            <label>
                <?php echo CHtml::checkBox('Project[has_wideo_walks]', isset($projectModel->attributes['has_wideo_walks'])&&$projectModel->attributes['has_wideo_walks'] == 1, array('value'=>1));?>
                <?= t('С видео прогулками') ?>
            </label>
        </div>
    </div>
    <div class="form_block pull-left">
        <div class="form_name"><?= t('Этажность') ?>:</div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[floors][1]', isset($projectModel->attributes['floors'][1])&&$projectModel->attributes['floors'][1]==1, array('value'=>1));?>
                <?= t('Одноэтажный') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[floors][2]', isset($projectModel->attributes['floors'][2])&&$projectModel->attributes['floors'][2]==2, array('value'=>2));?>
                <?= t('Двухэтажный') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[attic_floor]', isset($projectModel->attributes['attic_floor'])&&$projectModel->attributes['attic_floor']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
                <?= t('С мансардным этажом') ?>
            </label>
        </div>
    </div>
    <div class="form_block pull-left">
        <div class="form_name"><?= t('Гараж') ?>:</div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[cars_in_garage][1]', isset($projectModel->attributes['cars_in_garage'][1])&&$projectModel->attributes['cars_in_garage'][1]==1, array('value'=>1));?>
                <?= t('Одна машина') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[cars_in_garage][2]', isset($projectModel->attributes['cars_in_garage'][2])&&$projectModel->attributes['cars_in_garage'][2]==2, array('value'=>2));?>
                <?= t('Две машины') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[cars_in_garage][0]', isset($projectModel->attributes['cars_in_garage'][0])&&$projectModel->attributes['cars_in_garage'][0]=='0', array('value'=>'0'));?>
                <?= t('Без гаража') ?>
            </label>
        </div>
    </div>
    <div class="form_block pull-left">
        <div class="form_name"><?= t('Количество комнат') ?>:</div>
        <label class="checkbox-inline">
            <?php echo CHtml::checkBox('Project[rooms][2]', isset($projectModel->attributes['rooms']['2'])&&$projectModel->attributes['rooms']['2']==2, array('value'=>2));?> 2
        </label>
        <label class="checkbox-inline">
            <?php echo CHtml::checkBox('Project[rooms][3]', isset($projectModel->attributes['rooms']['3'])&&$projectModel->attributes['rooms']['3']==3, array('value'=>3));?> 3
        </label>
        <label class="checkbox-inline">
            <?php echo CHtml::checkBox('Project[rooms][4]', isset($projectModel->attributes['rooms']['4'])&&$projectModel->attributes['rooms']['4']==4, array('value'=>4));?> 4
        </label>
        <label class="checkbox-inline">
            <?php echo CHtml::checkBox('Project[rooms][5]', isset($projectModel->attributes['rooms']['5'])&&$projectModel->attributes['rooms']['5']==5, array('value'=>5));?> 5+
        </label>
        <div class="checkbox" style="margin-top: 3px;">
            <label>
                <?php echo CHtml::checkBox('Project[first_floor_room]', isset($projectModel->attributes['first_floor_room'])&&$projectModel->attributes['first_floor_room']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
                <?= t('Комната на 1-ом этаже') ?>
            </label>
        </div>
    </div>
    <div class="form_block pull-left">
        <div class="form_name"><?= t('Тип крыши') ?>:</div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[roof_2_slope]', isset($projectModel->attributes['roof_2_slope'])&&$projectModel->attributes['roof_2_slope']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
                <?= t('2-скатная') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[roof_many_slope]', isset($projectModel->attributes['roof_many_slope'])&&$projectModel->attributes['roof_many_slope']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
                <?= t('многоскатная') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[roof_4_slope]', isset($projectModel->attributes['roof_4_slope'])&&$projectModel->attributes['roof_4_slope']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
                <?= t('4-скатная') ?>
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?php echo CHtml::checkBox('Project[roof_flat]', isset($projectModel->attributes['roof_flat'])&&$projectModel->attributes['roof_flat']==LibraryRecord::STATUS_ENABLED, array('value'=>LibraryRecord::STATUS_ENABLED));?>
            <?= t('плоская') ?>
            </label>
        </div>
    </div>

    <div class="reset_options  pull-left">

        <div class="sort pull-left">
            <?php if(@$_GET[$dataProvider->sort->sortVar]=='net_area'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('net_area'=>true))?>" class="asc"><?= t('По площади') ?></a>
            <?php elseif(@$_GET[$dataProvider->sort->sortVar]=='net_area.desc'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('net_area'=>false))?>" class="desc"><?= t('По площади') ?></a>
            <?php else:?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('net_area'=>false))?>"><?= t('По площади') ?></a>
            <?php endif;?>
            <span class="divider"></span>
            <?php if(@$_GET[$dataProvider->sort->sortVar]=='price'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('price'=>true))?>" class="asc"><?= t('По цене') ?></a>
            <?php elseif(@$_GET[$dataProvider->sort->sortVar]=='price.desc'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('price'=>false))?>" class="desc"><?= t('По цене') ?></a>
            <?php else:?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('price'=>false))?>"><?= t('По цене') ?></a>
            <?php endif;?>
            <span class="divider"></span>
            <?php if(@$_GET[$dataProvider->sort->sortVar]=='symbol_sort_letter-symbol_sort_number'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('symbol_sort_letter'=>true, 'symbol_sort_number'=>true))?>" class="asc"><?= t('По названию') ?></a>
            <?php elseif(@$_GET[$dataProvider->sort->sortVar]=='symbol_sort_letter.desc-symbol_sort_number.desc'):?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('symbol_sort_letter'=>false, 'symbol_sort_number'=>false))?>" class="desc"><?= t('По названию') ?></a>
            <?php else:?>
                <a rel=”nofollow” href="<?php echo $dataProvider->sort->createUrl($this, array('symbol_sort_letter'=>false, 'symbol_sort_number'=>false))?>"><?= t('По названию') ?></a>
            <?php endif;?>
        </div>
        <a rel=”nofollow” href="<?php echo url('project/index', array('resetSearch'=>'1'));?>"><?= t('Сбросить все параметры') ?></a>
    </div>
    <?php echo CHtml::submitButton('', array('style'=>'font-size:0;width:0;height:0;border:0;line-height:0;margin: 0;padding: 0;'));?>
<?php $this->endWidget();?>

<?php
$script = <<<JS
$('#Project-search-form input[class!="linkInput"]').change(function(){
    $('#Project-search-form').submit();
});
JS;
cs()->registerScript('project-_search', $script);
?>
