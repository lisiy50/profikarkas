<div class="goods_wrap">
    <?php if($model->firstImage):?>
    <a href="<?php echo $model->getUrl();?>" class="img_wrap">
        <img src="<?php echo $model->firstImage->getImageUrl('small');?>">
    </a>
    <?php endif;?>
    <a href="<?php echo $model->getUrl();?>" class="goods_wrap_bottom clearfix">
        <?php
        $z_nameStyle = '';
        if(strpos($model->project_name,'Z')===false && strpos($model->project_name,'z')===false)
            $z_nameStyle = 'font-size:12px;max-width:140px;line-height:15px;padding-top:3px;height:36px;';
        ?>
        <div class="z_name" style="<?php echo $z_nameStyle;?>"><?php echo $model->project_name;?> </div>
        <div class="goods_title"><?php echo $model->construction_place;?></div>
    </a>
</div>