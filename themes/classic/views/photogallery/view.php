<?php
/* @var $model PhotoGallery */
?>
<div class="structured_top clearfix">
    <a href="<?php echo url('photoGallery/index');?>" class="pull-left"><?= t('Вернуться к списку фотогалерей') ?></a>
    <div class="pull-right">
        <?php if($model->getPrevProject()):?>
        <a href="<?php echo $model->getPrevProject()->getUrl();?>" class="back"><?= t('Предыдущая') ?></a>
        <?php endif;?>
        <?php if($model->getNextProject()):?>
        <a href="<?php echo $model->getNextProject()->getUrl();?>" class="next"><?= t('Предыдущая') ?></a>
        <?php endif;?>
    </div>
</div>
<?php if($model->show_albom == PhotoGallery::STATUS_ENABLED):?>
<div class="structured_center">
    <div class="slider" id="portfolio-carousel">
        <?php foreach($model->images as $image):?>
            <a class="slide" href="#" id="i<?php echo $image->id;?>">
                <span style="display: inline-block;position: relative;">
                    <div class="z10img"><?php echo $model->project_name;?></div>
                    <img src="<?php echo $image->getImageUrl('more');?>">
                </span>
                <?php if($image->description):?>
                <div class="slide_text"><?php echo $image->description;?></div>
                <?php endif;?>
            </a>
        <?php endforeach;?>
    </div>
    <a href="#" class="slider_prev"></a>
    <a href="#" class="slider_next"></a>
    <div class="project_carusel clearfix">
        <a href="#" class="back pull-left" id="portfolio-thumbs-prev"></a>
        <?php if($model->getVideos()):?>
            <div class="video_block pull-left" style="width: <?php $model->getCountVideos() * 105;?>px;height: 60px;">
                <?php foreach($model->getVideos() as $video):?>
                    <a href="<?php echo $video['videoLink'];?>" class="portfolio-video various fancybox.iframe">
                        <?php echo $video['miniImageHtml'];?>
                        <img src="<?php echo app()->theme->baseUrl;?>/img/carusel_video_img.png" class="video_play_img">
                    </a>
                <?php endforeach;?>
            </div>
        <?php endif;?>
        <div style="width: <?php echo (945 - ($model->getCountVideos() * 105));?>px;height: 60px;" class="pull-left">
            <div class="pull-left carusel" style="width: <?php echo (945 - ($model->getCountVideos() * 105));?>px;height: 60px" id="portfolio-thumbs">
                <?php foreach($model->images as $i=>$image):?>
                    <a href="#i<?php echo $image->id;?>" class="<?php echo ($i == 0)?'selected':'';?>"><img src="<?php echo $image->getImageUrl('mini');?>"></a>
                <?php endforeach;?>
            </div>
        </div>
        <a href="#" class="next pull-left" id="portfolio-thumbs-next"></a>
    </div>
</div>
<?php endif;?>
<div class="structured_bottom clearfix">
    <div class="pull-left left">
        <table>
            <tbody>
            <col width="180"/>
            <col/>
            <tr>
                <td>Проект: </td>
                <td>
                    <?php
                        if($model->getProjectUrl()){
                            echo CHtml::link($model->project_name, $model->getProjectUrl());
                        } else {
                            echo $model->project_name;
                        }
                    ?>
                </td>
            </tr>
            <?php if($model->gross_area):?>
            <tr>
                <td><?= t('Общая площадь') ?>: </td>
                <td><?php echo $model->gross_area;?> m<sup>2</sup></td>
            </tr>
            <?php endif;?>
            <?php if($model->construction_place):?>
            <tr>
                <td><?= t('Место строительства') ?>: </td>
                <td><?php echo $model->construction_place;?></td>
            </tr>
            <?php endif;?>
            <?php if($model->workload):?>
            <tr>
                <td><?= t('Заказанный объем работ') ?>: </td>
                <td><?php echo $model->workload;?></td>
            </tr>
            <?php endif;?>
            <?php if($model->turnaround_time):?>
            <tr>
                <td><?= t('Сроки выполнения работ') ?>: </td>
                <td><?php echo $model->turnaround_time;?></td>
            </tr>
            <?php endif;?>
            <?php if($model->period_of_works):?>
            <tr>
                <td><?= t('Период выполнения работ') ?>: </td>
                <td><?php echo $model->period_of_works;?></td>
            </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
    <div class="pull-left right">
        <div class="description">
            <?php echo $model->description;?>
        </div>
    </div>

    <div class="block_social_networks">
        <?php echo $this->renderPartial('//layouts/share_buttons');?>
    </div>

</div>

<?php
package('fancyboxHelpers');
package('carouFredSel');
?>



<?php
$script = <<<JS
$('.portfolio-video').fancybox({
    autoResize: true,
    autoCenter: true
});

$('#portfolio-carousel').carouFredSel({
    responsive: false,
    circular: false,
    auto: false,
    items: {
        visible: 1,
        width: 1000,
        height: 550
    },
    scroll: {
        fx: 'directscroll'
    },
    next:'.slider_next',
    prev:'.slider_prev'
});

$('#portfolio-thumbs').carouFredSel({
    circular: true,
    infinite: false,
    auto: false,
    prev: '#portfolio-thumbs-prev',
    next: '#portfolio-thumbs-next',
    items: {
        visible: {
            max: 9
        },
        width: 105,
        height: 60
    }
});

$('#portfolio-thumbs a').click(function() {
    $('#portfolio-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
    $('#portfolio-carousel a').removeClass('selected');
    $(this).addClass('selected');
    return false;
});
JS;
cs()->registerScript('photogallery-view', $script);
?>
