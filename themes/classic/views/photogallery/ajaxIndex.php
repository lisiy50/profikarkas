<?php
$lastCrossLink = 0;
if(isset($_GET['lastCrossLink'])){
    $lastCrossLink = $_GET['lastCrossLink'];
}
$crossLinks = CrossLink::getNextItems($lastCrossLink);
foreach($dataProvider->data as $k=>$portfolioItem){
    $this->renderPartial('_view', array('model'=>$portfolioItem, ));
    if($k == 2){
        $this->renderPartial('//crossLink/_portfolioCrossLink', array('crossLink'=>$crossLinks[0]));
    }
    if($k == 4){
        $this->renderPartial('//crossLink/_portfolioCrossLink', array('crossLink'=>$crossLinks[1]));
    }
}
?>

<div id="portfolio-pager">
    <?php $this->widget('BootPager', array(
        'pages'=>$dataProvider->pagination,
        'nextPageLabel' => t('Ещё работы'),
        'prevPageLabel' => '',
        'firstPageLabel' => '',
        'lastPageLabel' => '',
        'htmlOptions' => array(
            'class' => 'portfolio-pagination',
        ),
    )); ?>
</div>
