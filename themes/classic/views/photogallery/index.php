<?php
/**
 * @var $this PhotoGalleryController
 * @var $dataProvider CActiveDataProvider
 */
?>

<div class="catalog clearfix" id="photoGallery-catalog">

    <?php $this->renderPartial('ajaxIndex', array('dataProvider'=>$dataProvider));?>

</div>

<?php
$script = <<<JS
$('#photoGallery-catalog').on('click', '.photoGallery-pagination .next a', function(){
    var lastCrossLink = {}
    var id = ($('.portfolio_cross_link:last').data('id'));
    lastCrossLink.lastCrossLink = id;
    $.get($(this).attr('href'), lastCrossLink, function(html){
        $('#photoGallery-pager').remove();
        $('#photoGallery-catalog').append(html);
    });
    return false;
});
JS;
cs()->registerScript('photogallery-index', $script);
?>
