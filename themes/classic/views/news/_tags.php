<div class="content_l_head">ТЕГИ</div>
<ul>
<?php foreach(Tag::model()->findAll() as $tag): ?>
    <li><a href="<?php echo Yii::app()->createUrl('news/archive', array('tag'=>$tag->name)); ?>"><?php echo $tag->name; ?></a></li>
<?php endforeach; ?>
</ul>