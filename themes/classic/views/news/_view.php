<div class="news_block clearfix">
    <img src="<?php echo $news->getImageUrl('small');?>" class="pull-left">
    <div class="pull-left news_content">
        <div class="news_date"><?php echo Yii::app()->dateFormatter->format('dd.MM.yyyy', $news->publish_date); ?></div>
        <h3 class="news_title">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $news->title; ?>
            <?php elseif (Yii::app()->language == 'uk' && strip_tags($news->title_uk)): ?>
                <?php echo $news->title_uk; ?>
            <?php else: ?>
                <?php echo $news->title; ?>
            <?php endif; ?>
        </h3>
        <div class="news_annotation">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $news->annotation; ?>
            <?php elseif (Yii::app()->language == 'uk' && strip_tags($news->annotation_uk)): ?>
                <?php echo $news->annotation_uk; ?>
            <?php else: ?>
                <?php echo $news->annotation; ?>
            <?php endif; ?>
        </div>
        <div class="date_link_wrap clearfix">
            <a href="<?php echo $news->getUrl();?>" class="pull-right"><?= t('Подробнее') ?></a>
        </div>
    </div>
</div>
