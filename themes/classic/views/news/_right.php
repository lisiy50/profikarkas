<?php if(false): ?>
<div class="newsletter">

    <?php if(user()->hasFlash('success')):?>
        <?php echo user()->getFlash('success');?>
    <?php else:?>

        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => array('news/right'),
            'enableClientValidation' => true,
            'htmlOptions' => array(
                'id' => 'Subscriber-form',
            ),
        ));?>
            <span>Новостная рассылка</span>
            <div>
                <?php
                echo $form->textField($subscriber, 'name', array('placeholder'=>'Имя и Фамилия...'));
                echo $form->error($subscriber, 'name');
                ?>
            </div>
            <?php
            echo $form->textField($subscriber, 'email', array('placeholder'=>'E-mail...'));
            echo $form->error($subscriber, 'email');
            ?>
            <button type="submit">Подписаться</button>
        <?php $this->endWidget();?>
    <?php endif;?>
</div>
<div class="news_border"></div>
<?php endif ?>

<?php $this->renderPartial('//layouts/social_groups', array('options'=>array('width'=>200, 'height'=>300)));?>

<?php
$script = <<<JS
$('#Subscriber-form').submit(function(){
    $.post($.createUrl('news/right'), $('#Subscriber-form').serialize(), function(html){
        $('.news_right').html(html);
    });
    return false;
})
JS;
cs()->registerScript('news-_right', $script);
?>
