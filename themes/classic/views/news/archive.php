<?php /*
  *
  * $dataProvider - новости
  *
  * */ ?>

<div class="news_left pull-left">

    <?php foreach($dataProvider->data as $news): ?>
        <?php $this->renderPartial('_view', array('news'=>$news)); ?>
    <?php endforeach; ?>


</div>
<div class="news_right">
    <?php $this->actionRight();?>
</div>
<div class="pagination_news pull-left">

    <?php $this->widget('BootPager', array(
        'pages'=>$dataProvider->pagination,
        'htmlOptions'=>array(
            'class' => 'pagination',
        ),
    )); ?>
</div>