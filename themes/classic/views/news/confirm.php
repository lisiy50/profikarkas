<?php /*
  *
  * $news - новость
  *
  * */
?>


<div class="news_left pull-left">
    <div class="news_block news_more clearfix">
        <h3 class="news_title"><?= t('Вы успешно подписались на новостную рассылку.') ?></h3>
    </div>
</div>
<div class="news_right">
    <?php $this->actionRight();?>
</div>
