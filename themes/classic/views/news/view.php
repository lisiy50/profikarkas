<?php
$detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>

<div class="news_left pull-left">
    <div class="news_block news_more clearfix">
        <h1 class="news_title">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $news->title; ?>
            <?php elseif (Yii::app()->language == 'uk' && strip_tags($news->title_uk)): ?>
                <?php echo $news->title_uk; ?>
            <?php else: ?>
                <?php echo $news->title; ?>
            <?php endif; ?>
        </h1>
        <div class="news_date"><?php echo Yii::app()->dateFormatter->format('dd.MM.yyyy', $news->publish_date); ?></div>

        <?php if($news->images):?>
            <div class="news_slider">
                <div class="slider" id="news-carousel">
                    <?php foreach($news->images as $image):?>
                        <a class="slide" href="#" id="i<?php echo $image->id;?>" style="display:inline-block;text-align:center;height: 380px;">
                            <img src="<?php echo $image->getImageUrl('more');?>" style="max-height:380px;vertical-align:middle;">
                            <?php if($image->description):?>
                                <div class="slide_text"><?php echo $image->description;?></div>
                            <?php endif;?>
                        </a>
                    <?php endforeach;?>
                </div>

                <a href="#" class="slider_prev"></a>
                <a href="#" class="slider_next"></a>

                <?php if (!$isMobile || $isTablet): ?>

                <div class="project_carusel clearfix">
                    <a href="#" class="back pull-left" id="news-thumbs-prev"></a>
                    <div style="width: 530px;height: 60px;" class="pull-left">
                        <div class="pull-left carusel" style="width: 530px;height: 60px" id="news-thumbs">
                            <?php foreach($news->images as $i=>$image):?>
                                <a href="#i<?php echo $image->id;?>" class="<?php echo ($i == 0)?'selected':'';?>">
                                    <img src="<?php echo $image->getImageUrl('mini');?>" style="width: 100px;">
                                </a>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <a href="#" class="next pull-left" id="news-thumbs-next"></a>
                </div>

                <?php endif ?>

            </div>
        <?php elseif($news->image):?>
            <img src="<?php echo $news->getImageUrl('large');?>" class="news-image">
        <?php endif;?>
        <div class="news_text">
            <?php if (Yii::app()->language == 'ru'): ?>
                <?php echo $news->content; ?>
            <?php elseif (Yii::app()->language == 'uk' && strip_tags($news->content_uk)): ?>
                <?php echo $news->content_uk; ?>
            <?php else: ?>
                <?php echo $news->content; ?>
            <?php endif; ?>
        </div>
        <br>
        <div class="block_social_networks" >
            <?php echo $this->renderPartial('//layouts/share_buttons');?>
        </div>

        <a href="<?php echo url('news/archive');?>" class="return_btn pull-right"><?= t('Вернуться к списку новостей') ?></a>
    </div>
</div>
<div class="news_right">
    <?php $this->actionRight();?>
</div>

<div style="display: none;width: 300px;">
    <?php foreach($news->images as $k=>$image):?>
        <?php echo CHtml::link($k, $image->getImageUrl('large'));?> |
    <?php endforeach;?>
</div>

<?php

package('carouFredSel');
$script = <<<JS
$('#carousel span').append('<img src="img/gui/carousel_glare.png" class="glare" />');
$('#thumbs a').append('<img src="img/gui/carousel_glare_small.png" class="glare" />');

function sliderButtonPos(item) {
    var width = item.outerWidth();
    var h = item.outerHeight();
    if (width) {
        var space = (586 - width) / 2;
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    } else {
        $('.slider_prev').animate({'left': space + 'px', 'height': h+'px'});
        $('.slider_next').animate({'right': space + 'px', 'height': h+'px'});
    }
}

$('#news-carousel').carouFredSel({
    responsive: true,
    width: '100%',
    height: 'variable',
    items: {
        visible: 1,
        height: 'variable'
    },
    circular: false,
    auto: false,
     scroll: {
        fx: 'directscroll',
        onBefore: function( data ) {
            var item = data.items.visible.find('img,iframe');
            sliderButtonPos(item);
        }
    },
    onCreate: function(data) {
        var item = $(this).find('img,iframe');
        sliderButtonPos(item);
    },
    next:'.slider_next',
    prev:'.slider_prev'
});

$('#news-thumbs').carouFredSel({
//            responsive: true,
    circular: true,
    infinite: false,
    auto: false,
    prev: '#news-thumbs-prev',
    next: '#news-thumbs-next',
    items: {
        visible: {
            min: 3,
            max: 9
        },
        width: 105,
        height: 60
    }
});

$('#news-thumbs a').click(function() {
    $('#news-carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
    $('#news-carousel a').removeClass('selected');
    $(this).addClass('selected');
    return false;
});
JS;
cs()->registerScript('news-view', $script);

?>
