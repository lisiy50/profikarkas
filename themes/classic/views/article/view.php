<?php /* @var Article $article - статья */ ?>

<div class="block_social_networks" >
    <?php echo $this->renderPartial('//layouts/share_buttons');?>
    <?php if ($article->id == 30): ?>
        <a href="" id="historyLinkMobile"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/arrow_history_mobile.png"></a>
    <?php endif;?>
</div>

<div class="article reviews_text" style="<?php echo !$article->getShowSideMenu()?'width:1000px;':'';?>" <?php echo $article->id == 30 ? 'id="iqEnergy"': '';?>>
    <?php if (Yii::app()->language == 'ru'): ?>
        <?php echo $article->content; ?>
    <?php elseif (Yii::app()->language == 'uk'): ?>
        <?php echo $article->content_uk; ?>
    <?php endif; ?>
</div>

<style>
    <?php if($article->h1_size):?>
    .article h1, .article h1 span {
        font-size: <?php echo $article->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h2_size):?>
    .article h2, .article h2 span {
        font-size: <?php echo $article->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h3_size):?>
    .article h3, .article h3 span {
        font-size: <?php echo $article->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h4_size):?>
    .article h4, .article h4 span {
        font-size: <?php echo $article->h4_size?>px !important;
    }
    <?php endif?>
</style>

<?php if (false && $article->id == 15): ?>
    <div class="clearfix"></div>

    <div class="openseadragon_wrapper">
        <div id="openseadragon1" style="width: 814px; height: 700px;"></div>
    </div>
<?php endif; ?>

<?php

if ($article->id == 30) {
    cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/jquery.scrollTo.min.js');

    echo '<a href="" id="historyLink">' . t('Вернуться к проекту') . '</a>';
}

if ($article->id == 15) {
    package('openseadragon');

    $openseadragonAssetesPath = cs()->getPackageBaseUrl('openseadragon');
    $modelWithImages = UsefulArticle::model()->findByPk(45);

    $titleSources = array();

    foreach ($modelWithImages->images as $image) {
        $titleSources[] = $image->getImageUrl('original').'.dzi';
    }
    $titleSources = '"'.implode('","', $titleSources).'"';

    $script = <<<JS

    var viewer = OpenSeadragon({
        id: "openseadragon1",
        showNavigator: true,
        autoHideControls:  false,
        prefixUrl: "{$openseadragonAssetesPath}/images/",
        tileSources: [{$titleSources}],
        sequenceMode: true,
        mouseNavEnabled: false
    });

    $("#openseadragon1").click(function () {
        viewer.setMouseNavEnabled(true);
    });
    
    $("body").click(function (event) {
        if ($(event.target).closest("#openseadragon1").length === 0) {
            viewer.setMouseNavEnabled(false);
        }
    });
JS;

    cs()->registerScript('press-view', $script);
}

?>





