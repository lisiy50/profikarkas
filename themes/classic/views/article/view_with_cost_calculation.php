<?php /* @var Article $article - статья */ ?>

<div class="block_social_networks" >
    <?php echo $this->renderPartial('//layouts/share_buttons');?>
</div>

<div class="article reviews_text" style="<?php echo !$article->getShowSideMenu()?'width:1000px;':'';?>">
    <?php echo $article->content; ?>
</div>

<?php cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/cost.calculation.js'); ?>
<div id="cost-calculation"></div>

<style type="text/css">
    <?php if($article->h1_size):?>
    .article h1, .article h1 span {
        font-size: <?php echo $article->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h2_size):?>
    .article h2, .article h2 span {
        font-size: <?php echo $article->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h3_size):?>
    .article h3, .article h3 span {
        font-size: <?php echo $article->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($article->h4_size):?>
    .article h4, .article h4 span {
        font-size: <?php echo $article->h4_size?>px !important;
    }
    <?php endif?>
</style>
