<div class="europe_build">
    <?php if(false && $model->image):?>
        <a href="<?php echo $model->getImageUrl('large');?>" class="fancybox">
            <img src="<?php echo $model->getImageUrl('medium');?>">
        </a>
    <?php endif;?>
    <div class="title"><?php echo $model->title;?></div>
    <?php echo $model->description;?>
    <div class="clearfix link_wrap"><a href="javascript:window.history.back();" class="link"><?= t('Вернуться') ?></a></div>
</div>

<?php package('fancyboxHelpers');?>



<?php
$script = <<<JS
$('.fancybox').fancybox({
    autoResize: true,
    autoCenter: true
});
JS;
cs()->registerScript('buildInEurope-view', $script);
?>

<style type="text/css">
    <?php if($model->h1_size):?>
    .europe_build h1, .europe_build h1 span {
        font-size: <?php echo $model->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h2_size):?>
    .europe_build h2, .europe_build h2 span {
        font-size: <?php echo $model->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h3_size):?>
    .europe_build h3, .europe_build h3 span {
        font-size: <?php echo $model->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h4_size):?>
    .europe_build h4, .europe_build h4 span {
        font-size: <?php echo $model->h4_size?>px !important;
    }
    <?php endif?>
</style>

