<div class="goods_wrap">
    <a href="<?php echo $model->getUrl();?>" class="img_wrap">

        <?php if($model->image):?>
            <?php if($model->getHasVideoInDescription() && $model->image):?>
                <img src="<?php echo $model->getImageUrl('small');?>">
                <img class="zoom" src="<?php echo app()->theme->baseUrl;?>/img/europe_play.png">
                <img class="zoom hover_zoom" src="<?php echo app()->theme->baseUrl;?>/img/europe_play2.png">
            <?php elseif($model->image):?>
                <img src="<?php echo $model->getImageUrl('small');?>">
            <?php endif;?>
        <?php endif;?>

    </a>
    <a href="<?php echo $model->getUrl();?>" class="goods_wrap_bottom clearfix">
        <div class="goods_title"><?php echo $model->title;?></div>
    </a>
</div>