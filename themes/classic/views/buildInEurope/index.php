<div class="catalog clearfix" id="portfolio-catalog">

    <?php
    foreach($dataProvider->data as $portfolioItem){
        $this->renderPartial('_view', array('model'=>$portfolioItem, ));
    }
    ?>

    <div id="portfolio-pager">
<!--        <div class="more_work pull-left"><a href="#">Ещё работы</a></div>-->
        <?php $this->widget('BootPager', array(
            'pages'=>$dataProvider->pagination,
            'nextPageLabel' => t('Ещё работы'),
            'prevPageLabel' => '',
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'htmlOptions' => array(
                'class' => 'portfolio-pagination',
            ),
        )); ?>
    </div>

</div>

<?php
$script = <<<JS
$('#portfolio-catalog').on('click', '.portfolio-pagination .next a', function(){
    $(this).addClass('disabled');
    $('.items-loader').show();
    $.get($(this).attr('href'), {}, function(html){
        $('#portfolio-pager, .items-loader').remove();
        $('#portfolio-catalog').append(html);
    });
    return false;
});
JS;
cs()->registerScript('buildInEurope-index', $script);
?>
