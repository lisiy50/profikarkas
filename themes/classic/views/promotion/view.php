<div class="row margin20">
    <div class="span">
        <h3><?php echo $promotion->title; ?></h3>
    </div>
</div>
<div class="row margin20">
    <div class="span">
        <?php echo $promotion->content; ?>
    </div>
</div>
<div class="row margin20">
    <div class="span">
        <h3>Товары что участвуют в акции:</h3>
    </div>
</div>
<div class="row margin20">
    <div class="span">
        <?php foreach($promotion->products as $product): ?>
            <?php $this->renderPartial('//product/_view', array('product'=>$product)); ?>
        <?php endforeach; ?>
    </div>
</div>



