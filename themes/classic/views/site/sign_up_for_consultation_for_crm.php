<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 9/14/15
 * Time: 10:04 AM
 *
 * @var SiteController $this
 * @var SignUpForConsultation $model
 */
?>

<div class="sign-up-for-consultation-bg navbar-fixed-bottom" id="sign-up-for-consultation">
    <div class="sign-up-for-consultation-wrapper container clearfix">

        <div class="sign-up-for-consultation-info clearfix">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/model.png">
            <div class="sign-up-for-consultation-title"><?= t('Запишитесь на БЕСПЛАТНУЮ<br> консультацию!!!') ?></div>
            <div class="sign-up-for-consultation-txt"><?= t('Наши эксперты ответят на все ваши вопросы.') ?></div>
        </div>
        <div class="sign-up-for-consultation-form" style="position: relative;">

            <!-- Note :
   - You can modify the font style and form style to suit your website.
   - Code lines with comments “Do not remove this code”  are required for the form to work properly, make sure that you do not remove these lines of code.
   - The Mandatory check script can modified as to suit your business needs.
   - It is important that you test the modified form before going live.-->
            <div id='crmWebToEntityForm' style='width:342px;margin:auto;'>
                <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
                <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1793916000000212180 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

                    <!-- Do not remove this code. -->
                    <input type='text' style='display:none;' name='xnQsjsdp' value='b53510900833b00d856eda3c57a59c3cedb92ced4709ccb4a4b7b30341314ba2'/>
                    <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
                    <input type='text' style='display:none;' name='xmIwtLD' value='d68653f57ab87d0c591fae8452485d076f46da3beca44df2b0ec32ea98be84be'/>
                    <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>

                    <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;profikarkas.com.ua' />
                    <!-- Do not remove this code. -->
                    <style>
                        tr , td {
                            padding:6px;
                            border-spacing:0px;
                            border-width:0px;
                        }
                    </style>
                    <table style='width:342px;background-color:transparent;color:black'>

                        <tr style="display: none;"><td colspan='2' style='text-align:left;color:black;font-family:Arial;font-size:14px;'>
                                <strong>&#x437;&#x430;&#x44f;&#x432;&#x43a;&#x430; &#x43d;&#x430; &#x431;&#x435;&#x441;&#x43f;&#x43b;&#x430;&#x442;&#x43d;&#x443;&#x44e; &#x43a;&#x43e;&#x43d;&#x441;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x446;&#x438;&#x44e;</strong></td></tr>

                        <tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:50%'>&#x411;&#x440;&#x435;&#x43d;&#x434;</td><td style='width:250px;'>
                                <select style='width:250px;' name='LEADCF11'>
                                    <option value='-None-'>-None-</option>
                                    <option value='z500'>z500</option>
                                    <option selected value='Profikarkas'>Profikarkas</option>
                                    <option value='&#x414;&#x440;&#x443;&#x433;&#x43e;&#x435;'>Другое</option>
                                </select></td></tr>

                        <tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:50%'>&#x418;&#x441;&#x442;&#x43e;&#x447;&#x43d;&#x438;&#x43a;</td><td style='width:250px;'>
                                <select style='width:250px;' name='LEADCF1'>
                                    <option value='-None-'>-None-</option>
                                    <option selected value='&#x421;&#x430;&#x439;&#x442;&#x20;Profikarkas'>Сайт Profikarkas</option>
                                    <option value='&#x420;&#x424;&#x20;&#x421;&#x430;&#x439;&#x442;'>РФ Сайт</option>
                                    <option value='&#x423;&#x43a;&#x440;&#x20;&#x421;&#x430;&#x439;&#x442;&#x20;'>Укр Сайт</option>
                                    <option value='KZ&#x20;&#x421;&#x430;&#x439;&#x442;'>KZ Сайт</option>
                                    <option value='&#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x43b;&#x20;&#x434;&#x438;&#x43b;&#x435;&#x440;'>передал дилер</option>
                                    <option value='&#x43f;&#x435;&#x440;&#x435;&#x434;&#x430;&#x43b;&#x438;&#x20;&#x43f;&#x43e;&#x43b;&#x44f;&#x43a;&#x438;'>передали поляки</option>
                                    <option value='&#x414;&#x440;&#x443;&#x433;&#x43e;&#x435;'>Другое</option>
                                </select></td></tr>

                        <tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:50%'>&#x41c;&#x435;&#x442;&#x43e;&#x434; &#x43f;&#x435;&#x440;&#x432;&#x43e;&#x43d;&#x430;&#x447;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x439; &#x43a;&#x43e;&#x43c;&#x43c;&#x443;&#x43d;&#x438;&#x43a;&#x430;&#x446;&#x438;&#x438;</td><td style='width:250px;'>
                                <select style='width:250px;' name='LEADCF2'>
                                    <option value='-None-'>-None-</option>
                                    <option value='&#x442;&#x435;&#x43b;&#x435;&#x444;&#x43e;&#x43d;'>телефон</option>
                                    <option value='e-mail'>e-mail</option>
                                    <option value='&#x436;&#x438;&#x432;&#x43e;&#x441;&#x430;&#x439;&#x442;'>живосайт</option>
                                    <option value='&#x43a;&#x43e;&#x43c;&#x43c;&#x435;&#x43d;&#x442;&#x430;&#x440;&#x438;&#x439;'>комментарий</option>
                                    <option value='&#x432;&#x43e;&#x43f;&#x440;&#x43e;&#x441;&#x20;&#x43f;&#x43e;&#x20;&#x43f;&#x440;&#x43e;&#x435;&#x43a;&#x442;&#x443;'>вопрос по проекту</option>
                                    <option value='&#x437;&#x430;&#x43a;&#x430;&#x437;&#x20;&#x441;&#x20;&#x441;&#x430;&#x439;&#x442;&#x430;'>заказ с сайта</option>
                                    <option value='Skype'>Skype</option>
                                    <option value='&#x441;&#x43e;&#x446;&#x20;&#x441;&#x435;&#x442;&#x438;'>соц сети</option>
                                    <option value='&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x20;&#x437;&#x430;&#x44f;&#x432;&#x43a;&#x438;&#x20;&#x437;&#x430;&#x43f;&#x440;&#x43e;&#x441;&#x430;&#x20;&#x437;&#x432;&#x43e;&#x43d;&#x43a;&#x430;'>форма заявки запроса звонка</option>
                                    <option selected value='&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x20;&#x431;&#x435;&#x441;&#x43f;&#x43b;&#x430;&#x442;&#x43d;&#x43e;&#x439;&#x20;&#x43a;&#x43e;&#x43d;&#x441;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x446;&#x438;&#x438;'>форма бесплатной консультации</option>
                                    <option value='&#x444;&#x43e;&#x440;&#x43c;&#x430;&#x20;&#x437;&#x430;&#x44f;&#x432;&#x43a;&#x430;&#x20;&#x43d;&#x430;&#x20;&#x43f;&#x440;&#x43e;&#x441;&#x43c;&#x43e;&#x442;&#x440;'>форма заявка на просмотр</option>
                                </select></td></tr>

                        <tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:50%'>&#x421;&#x442;&#x440;&#x430;&#x43d;&#x430;2</td><td style='width:250px;'>
                                <select style='width:250px;' name='LEADCF10'>
                                    <option value='-None-'>-None-</option>
                                    <option value='&#x420;&#x424;'>РФ</option>
                                    <option selected value='&#x423;&#x43a;&#x440;&#x430;&#x438;&#x43d;&#x430;'>Украина</option>
                                    <option value='&#x411;&#x435;&#x43b;&#x430;&#x440;&#x443;&#x441;&#x44c;'>Беларусь</option>
                                    <option value='&#x41a;&#x430;&#x437;&#x430;&#x445;&#x441;&#x442;&#x430;&#x43d;'>Казахстан</option>
                                    <option value='&#x434;&#x440;&#x443;&#x433;&#x43e;&#x435;'>другое</option>
                                </select></td></tr>

                        <tr>
                            <!--<td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>
                                &#x418;&#x43c;&#x44f; &#x424;&#x430;&#x43c;&#x438;&#x43b;&#x438;&#x44f;
                                <span style='color:red;'>*</span>
                            </td>-->
                            <td colspan='2' style="padding: 0;">
                                <input class="sign-up-for-consultation-input" type='text' maxlength='80' name='Last Name' placeholder="Имя"/>
                            </td>
                        </tr>

                        <tr>
                            <!--<td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>
                                &#x422;&#x435;&#x43b;&#x435;&#x444;&#x43e;&#x43d;
                                <span style='color:red;'>*</span>
                            </td>-->
                            <td colspan='2' style="padding: 0;">
                                <input class="sign-up-for-consultation-input" type='text' maxlength='30' name='Phone' placeholder="Номер телефона"/>
                            </td>
                        </tr>

                        <tr style="padding: 0;">
                            <td colspan='2' style="padding: 0;">
                                <input class="sign-up-for-consultation-send" style="width: 167px;" type='submit' value='Отправить' />
                                <input class="sign-up-for-consultation-send" style="width: 167px;margin-left: 4px;" type='reset' value='Отмена' />
                            </td>
                        </tr>
                    </table>
                    <script>
                        var mndFileds=new Array('Last Name','Phone');
                        var fldLangVal=new Array('Имя Фамилия','Телефон');
                        var name='';
                        var email='';

                        function checkMandatory() {
                            for(i=0;i<mndFileds.length;i++) {
                                var fieldObj=document.forms['WebToLeads1793916000000212180'][mndFileds[i]];
                                if(fieldObj) {
                                    if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
                                        if(fieldObj.type =='file')
                                        {
                                            alert('Please select a file to upload');
                                            fieldObj.focus();
                                            return false;
                                        }
                                        alert(fldLangVal[i] +' cannot be empty');
                                        fieldObj.focus();
                                        return false;
                                    }  else if(fieldObj.nodeName=='SELECT') {
                                        if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
                                            alert(fldLangVal[i] +' cannot be none');
                                            fieldObj.focus();
                                            return false;
                                        }
                                    } else if(fieldObj.type =='checkbox'){
                                        if(fieldObj.checked == false){
                                            alert('Please accept  '+fldLangVal[i]);
                                            fieldObj.focus();
                                            return false;
                                        }
                                    }
                                    try {
                                        if(fieldObj.name == 'Last Name') {
                                            name = fieldObj.value;
                                        }
                                    } catch (e) {}
                                }
                            }
                        }

                    </script>
                </form>
            </div>

        </div>

        <?php echo CHtml::link(t('Закрыть'), '#',array('class'=>"sign-up-for-consultation-close"));?>

    </div>
</div>
