<?php
/**
 * @var SiteController $this
 * @var SignUpForConsultation $model
 */
?>

<div class="invitation-inner" id="invitationInner">
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="invitation-confirm-wrapper">
            <p class="invitation-confirm">
                <?php echo Yii::app()->user->getFlash('success'); ?>
                <a class="invitation-confirm-close" href="#">✕</a>
            </p>
        </div>
    <?php else: ?>
        <div class="invitation-bg">
            <p class="invitation-title"><?= t('Приезжайте к нам в демо-дом на чашечку <span>кофе :-)</span>') ?></p>
            <ul class="invitation-list">
                <li class="invitation-list-item"><?= t('Почувствуйте себя хозяином') ?></li>
                <li class="invitation-list-item"><?= t('Получите ответы на важные вопросы') ?></li>
            </ul>
            <div class="invitation-form">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'enableClientValidation' => false,
                    'action' => url('site/signUpForDemoHouse'),
                    'id' => 'signUpForDemoHouseForm',
                    'htmlOptions' => array(
                        'onclick' => "ga('create', 'UA-52477514-1', 'auto'); ga('send', 'event', 'Form', 'Submit', 'Free consultation', {nonInteraction: true});",
                    ),
                )); ?>
                <input type="hidden" id="g-recaptcha-response2" name="g-recaptcha-response">
                <input type="hidden" name="action" value="validate_captcha">
                <?php echo $form->hiddenField($model, 'subject', array('value' => '')); ?>
                <div class="invitation-form-group">
                    <?php echo $form->textField($model, 'name', array('class' => 'invitation-form-control', 'placeholder' => t('Имя...'))); ?>
                    <?php echo $form->error($model, 'name', array('class' => 'invitation-form-help')); ?>
                </div>
                <div class="invitation-form-group">
                    <?php echo $form->textField($model, 'phone', array('class' => 'invitation-form-control', 'placeholder' => '0501234567')); ?>
                    <?php echo $form->error($model, 'phone', array('class' => 'invitation-form-help')); ?>
                </div>
                <div class="invitation-form-btn-wrapper">
                    <?php echo CHtml::submitButton(t('Записаться на экскурсию'), array('class' => 'invitation-form-btn')); ?>
                </div>

                <?php $this->endWidget();?>
            </div>
        </div>
    <?php endif; ?>
</div>

<script>
    function initCaptcha2() {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
                document.getElementById('g-recaptcha-response2').value=token;
            });
        });
    }

    window.setTimeout('initCaptcha2()', 4000);
</script>
