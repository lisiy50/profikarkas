<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 8/3/15
 * Time: 3:22 PM
 */

$width = 240;
$height = 285;
?>

<div id="social-poup-wrapper" style='display: none;opacity: 0;'></div>

<div class="social-poup" style='opacity: 0;'>

    <div class="social-poup-header">
        <span><?= t('Подпишитесь на наши') ?></span>
        <span><?= t('страницы в социальных сетях!') ?></span>
        <div id="social-poup-close"></div>
    </div>

    <div class="social-poup-text"><?= t('И вы будете первыми узнавать о наших новинках!') ?></div>

    <div class="group-container">

        <div class="group-container-header">
            <img src="<?php echo app()->theme->baseUrl?>/img/face_book_v2.png" /> Facebook
        </div>

        <div
            class="fb-page"
            data-href="https://www.facebook.com/Profikarkas"
            data-small-header="false"
            data-adapt-container-width="true"
            data-hide-cover="false"
            data-show-facepile="true"
            data-show-posts="true"
            data-width="<?=$width;?>"
            data-height="<?=$height?>">
        </div>

        <script type="text/javascript">
            FB.XFBML.parse();
        </script>

    </div>

    <div class="group-container">

        <div class="group-container-header">
            <img src="<?php echo app()->theme->baseUrl?>/img/social_inst_v2.png" /> Instagram
        </div>

        <a href="https://www.instagram.com/profikarkas_ua/" target="_blank" style="display: block; cursor: pointer;">
            <img class="img-responsive" src="<?php echo app()->theme->baseUrl?>/img/social-popup-inst.jpg" />
        </a>
    </div>

    <div class="social-poup-subscribed"><span><?= t('Спасибо, я уже подписан') ?></span></div>

</div>
