<?php
/**
 * @var SiteController $this
 * @var CostCalculationForm $model
 */
?>

<div class="cost-calculation-bg">
    <div class="cost-calculation-wrapper">

        <div class="cost-calculation-form clearfix">

            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="cost-calculation-required-info">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php else: ?>

                <?php $form=$this->beginWidget('BootActiveForm', array(
                    'enableClientValidation'=>false,
                    'action' => url('site/costCalculation'),
                    'id' => 'CostCalculationForm-form',
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'onsubmit' => "ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Ferma', {nonInteraction: true});",
                    ),
                ));
                /* @var BootActiveForm $form */
                ?>

                <div class="cost-calculation-item">
                    <?php echo $form->textFieldRow($model, 'name', array('class'=>'cost-calculation-input'))."\n";?>
                </div>
                <div class="cost-calculation-item mod-1 pull-left">
                    <?php echo $form->textFieldRow($model, 'email', array('class'=>'cost-calculation-input'))."\n";?>
                </div>
                <div class="cost-calculation-item mod-1 pull-right">
                    <?php echo $form->textFieldRow($model, 'phone', array('class'=>'cost-calculation-input'))."\n";?>
                </div>
                <div class="cost-calculation-item mod-2">
                    <?php echo $form->labelEx($model, 'file')?><?php echo $form->fileField($model, 'file', array('class'=>'cost-calculation-input'));?><?php echo $form->error($model, 'file')?>
                </div>

                <br>
                <div class="cost-calculation-required-info pull-left">
                    <?= t('* - поля, обязательные для заполнения') ?>
                    <br>
                    <?= t('** - план кровли вашего дома с размерами,<br> который содержится в архитектурной части проекта.') ?>
                </div>

                <?php echo CHtml::submitButton(t('Отправить'), array('class'=>"cost-calculation-send pull-right"));?>


                <?php $this->endWidget();?>

            <?php endif; ?>


        </div>

    </div>
</div>
