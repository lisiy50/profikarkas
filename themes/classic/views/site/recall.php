<?php
/**
 * @var RecallForm $model
 */
?>


<div class="recall-wrapper" style="width: 380px;">
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <div class="alert">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php else: ?>

        <br>
        <strong style="font-family: Arial;"><?= t('Заказ на обратный звонок ПрофиКаркас') ?></strong>

        <br>

        <?php
        /** @var CActiveForm $form */
        $form=$this->beginWidget('CActiveForm', array(
            'enableClientValidation'=>true,
            'action' => url('site/recall'),
            'id' => 'recall_form',
            'htmlOptions' => array(
                'onsubmit' => "ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Call', {nonInteraction: true});",
            ),
        ));
        ?>
        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
        <input type="hidden" name="action" value="validate_captcha">
        <table style="text-align: left; width: 100%">
            <tr>
                <td width="120">
                    <?php echo $form->labelEx($model, 'name', array('style' => 'padding-top:15px;')); ?>
                </td>
                <td>
                    <?php echo $form->textField($model, 'name', array('class'=>'recall-input', 'placeholder'=> t('Ваше имя'), 'style' => 'width:100%')); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'phone', array('style' => 'padding-top:15px;')); ?>
                </td>
                <td>
                    <?php echo $form->textField($model, 'phone', array('class'=>'recall-input', 'placeholder'=>'Ваш номер телефона', 'style' => 'width:100%')); ?>
                </td>
            </tr>
            <tr>
                <td width="120">
                    <?php echo $form->labelEx($model, 'email', array('style' => 'padding-top:15px;')); ?>
                </td>
                <td>
                    <?php echo $form->textField($model, 'email', array('class'=>'recall-input', 'placeholder'=>'Ваш Email', 'style' => 'width:100%')); ?>
                </td>
            </tr>
        </table>

        <?php echo CHtml::submitButton(t('Отправить'), array('class' => "recall-send")); ?>
        <?php echo CHtml::link('X', '#' ,array('class' => "recall-close")); ?>

    <?php $this->endWidget(); ?>
    <?php endif; ?>
</div>

<script>
    function initCaptchaRecall() {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
                document.getElementById('g-recaptcha-response').value=token;
            });
        });
    }

    window.setTimeout('initCaptchaRecall()', 6000);
</script>

<script type="text/javascript">
    $(document).on("click", "#recall_form .recall-send", function() {
        var form = $("#recall_form");
        $.ajax({
            url: "site/recall",
            type: "post",
            data: form.serialize(),
            success: function(html) {
                console.log(html);
                // $.fancybox.hideLoading();
                $(".fancybox-inner").html(html);
            },
            error: function(html) {
                alert('bad request');
            }
        });
        return false;
    });

    $(document).on("click", "#recall_form .recall-close" , function() {
        $.fancybox.close();
        return false;
    });
</script>
