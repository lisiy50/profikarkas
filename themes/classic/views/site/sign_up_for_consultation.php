<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 9/14/15
 * Time: 10:04 AM
 *
 * @var SiteController $this
 * @var SignUpForConsultation $model
 */
?>

<div class="sign-up-for-consultation-bg navbar-fixed-bottom" id="sign-up-for-consultation">
    <div class="sign-up-for-consultation-wrapper container clearfix">

        <div class="sign-up-for-consultation-info clearfix">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/model.png">
            <div class="sign-up-for-consultation-title"><?= t('Запишитесь на БЕСПЛАТНУЮ<br> консультацию!!!') ?></div>
            <div class="sign-up-for-consultation-txt"><?= t('Наши эксперты ответят на все ваши вопросы.') ?></div>
        </div>
        <div class="sign-up-for-consultation-form" style="position: relative;">

            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php else: ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'enableClientValidation'=>false,
                    'action' => url('site/signUpForConsultation'),
                    'id' => 'SignUpForConsultation-form',
                    'htmlOptions' => array(
                        'onclick' => "ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Free consultation', {nonInteraction: true});",
                    ),
                ));
                /* @var CActiveForm $form */
                ?>

                <input type="hidden" id="g-recaptcha-response5" name="g-recaptcha-response">
                <input type="hidden" name="action" value="validate_captcha">

                <?php echo $form->textField($model, 'name', array('class'=>'sign-up-for-consultation-input', 'placeholder'=>'Имя'))."<br>\n";?>
                <?php echo $form->textField($model, 'phone', array('class'=>'sign-up-for-consultation-input', 'placeholder'=>'Номер телефона'))."<br>\n";?>

                <?php echo CHtml::submitButton(t('Отправить'), array('class'=>"sign-up-for-consultation-send"));?>


                <?php $this->endWidget();?>

            <?php endif; ?>

            <div class="sign-up-for-consultation-form-loader" style="display:none; position:absolute;top: 0;left: 0;width: 100%;height: 100%;background-color: rgba(0, 0, 0, .5)">

            </div>

        </div>

        <?php echo CHtml::link(t('Закрыть'), '#',array('class'=>"sign-up-for-consultation-close"));?>

    </div>
</div>

<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
            document.getElementById('g-recaptcha-response5').value=token;
        });
    });
</script>
