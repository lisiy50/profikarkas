<h3><?= t('Авторизация') ?></h3>

<?php
$form=$this->beginWidget('BootActiveForm', array(
    'id'=>'login-form',
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
));

echo $form->textFieldRow($login,'username');

echo $form->passwordFieldRow($login,'password');

echo $form->checkBoxRow($login,'rememberMe');

echo CHtml::submitButton(t('Войти'), array('class'=>"btn btn-large"));
?>

<a href="<?php echo url('/user/remindPassword'); ?>"><?= t('Забыли пароль?') ?></a>

<?php $this->endWidget(); ?>

<h4><?= t('Авторизация через социальные сети') ?></h4>

<div id="uLogindc6ae8b3" data-ulogin="display=panel;fields=first_name,last_name,email,nickname,photo;lang=ru;providers=facebook,google,twitter;redirect_uri=<?php echo Yii::app()->createAbsoluteUrl('site/ulogin') ?>"></div>
<?php cs()->registerScriptFile('//ulogin.ru/js/ulogin.js')?>
