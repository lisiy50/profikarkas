<?php $detect = new MobileDetect();
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>

<!--<div class="slider_wrap">-->
<!--    <div class="slider" id="main_page_slider">-->
<!--        --><?php //foreach(SliderItem::model()->findAll() as $sliderItem):?>
<!--        <div class="slide">-->
<!--            <a href="--><?php //echo $sliderItem->url;?><!--">-->
<!--                <img src="--><?php //echo $sliderItem->getImageUrl();?><!--">-->
<!--            </a>-->
<!--        </div>-->
<!--        --><?php //endforeach;?>
<!--    </div>-->
<!--    <div class="slider_nav_wrap">-->
<!--        <div class="slider_nav" id="main_page_slider_nav">-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<?php
//package('carouFredSel');
//
//$script = <<<JS
//$('#main_page_slider').carouFredSel({
//    responsive: true,
//    pagination: '#main_page_slider_nav',
//    scroll: {
//        fx: 'cover-fade'
//    }
//})
//JS;
//cs()->registerScript('site-index-carousel', $script);
//?>

<div class="video_wrap" onclick="playPause()" style="cursor: pointer">
    <video id="video" controls poster="<?php echo Yii::app()->theme->baseUrl; ?>/img/video_poster.jpg" preload="auto" width=1000 height=563>
        <source src="<?php echo Yii::app()->theme->baseUrl; ?>/img/video_profikarkas.mp4" type="video/mp4">
        Ваш броузер не поддерживает тег video
    </video>
    <?php if (!$isMobile && !$isTablet): ?>
        <div class="video_icon" id="video_icon"></div>
    <?php endif ?>
</div>

<script>
    var myVideo = document.getElementById("video");

    function playPause() {
        if (myVideo.paused) {
            myVideo.play();
            document.getElementById("video_icon").style.display='none';
        } else {
            myVideo.pause();
            document.getElementById("video_icon").style.display='block';
        }
    }
</script>

<div class="main_page_text" style="">
    <div class="first_part">
        <h1><span style="color:#ba3133;">Комфортный, качественный дом в короткие сроки — это реально!</span></h1>

        &nbsp; &nbsp; <p>Как хочется каждому из нас, чтобы каркасный дом был комфортный и долговечный, чтобы строительство дома проходило по условленному графику, по фиксированной цене. А в реальности мы больше всего боимся, что строительство дома обернется непомерными хлопотами и высокими финансовыми рисками.</p>
    </div>
        <div class="second_part">
        <p>А хотели бы вы построить дом, который кроме описанных преимуществ, приносил бы еще и финансовую выгоду каждый год?
            Задумывались
            ли вы над тем, что преимущества каркасного дома можно измерить в денежном эквиваленте?</p>

        <p><b>Это вполне реально!</b></p><h4><span style="color:#ba3133;"><b>С каркасным домом, построенным в нашей компании, вы
                    получите</b></span><span style="line-height:20px;font-size:15px;color:rgb(51,51,51);"></span>
        </h4>
        <ul>
            <li><span style="line-height:1.45em;"><b><span style="color:#ba3133;">Экономию бюджета</span> </b><span
                        style="color:#e36c09;"></span></span><span style="line-height:1.45em;">на этапе строительства, при высоком качестве результата</span>
                <ol>
                    <li><span style="line-height:1.45em;">Благодаря</span>
                        <ul>
                            <li><span style="color:rgb(119,119,119);font-style:italic;line-height:1.45em;">более легкой конструкции каркасного дома,</span><span
                                    style="font-size:15px;"></span>
                            </li>
                            <li><span style="color:rgb(119,119,119);font-style:italic;line-height:1.45em;">а значит и более простому типу фундамента,</span>
                            </li>
                            <li><span style="color:rgb(119,119,119);font-style:italic;line-height:1.45em;">современному подходу к проектированию от компании Профикаркас</span><br>
                                <!--<img  src="/storage/images/30f2dc519c981642e9613e0d4b499d66.jpg"
                                      alt="30f2dc519c981642e9613e0d4b499d66.jpg"
                                      style="font-size: 15px; float: none; margin: 20px 0px; width: 127.015px; height: 64px;margin-left: 10px;">-->

                                <span style="color: #ba3133;margin-left: 10px;"><b>Вы получите</b></span>
                            </li>
                            <li><span style="line-height:1.45em;color: #777;font-style:italic;">уменьшение количества строй материалов на заливку
                                        фундамента на 30%-50%.</span>
                            </li>
                            <li><span style="line-height:1.45em;color: #777;font-style:italic;">уменьшение стоимости такого фундамента в 1,5-2 раза, по
                                        сравнению с фундаментом<br>для этого же дома, возводимого по другим технологии.</span></li>
                        </ul>
                    </li>
                    <li><span
                            style="line-height:1.45em;">Благодаря использованию немецкой каркасной технологии от Профикаркас</span>
                </ol>
                    <!--<img src="/storage/images/ca80117125d8db583562bfb91fdaf005.jpg" style="width: 127.015px; height: 64px;margin-left: 90px;
margin-top: 20px;
margin-bottom: 20px;"
                        alt="ca80117125d8db583562bfb91fdaf005.jpg">-->
                    <span style="color: #ba3133;margin-left: 90px;"><b>Вы получите</b></span>
<p>
                <span style="line-height:1.45em;color: #777;padding-left: 80px;font-style: italic;display: block;">
                        Экономию бюджета на стеновых материалах 15-40%<br>(по сравнению с традиционными материалами).</span>
</p>

                <p>В результате вы получите каркасный дом, цена которого будет ниже, чем при строительстве такого же дома по другой технологии на 15-30%.
                </p>
            </li>
        </ul>


        <br>
        <ul>
            <li><p><b style="line-height:1.45em;"><span
                        style="color:#ba3133;">Экономию в процессе эксплуатации.</span></b></p>
<p style="line-height:1.45em;">Для каркаса вашего дома мы используем только качественные строительные материалы:</p>
                <p>
                <ul>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">древесину высшего сорта высушенную до 12% влажности,</span></i></span>
                    </li>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">минеральную вату Rockwool,</span></i></span>
                    </li>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">материалы только сертифицированных производителей,</span></i></span>
                    </li>
                    <li><span style="line-height:1.45em;"><i><span
                                    style="color:#747474;">качественные отделочные материалы.</span></i></span></li>
                </ul>
                </p>


                <!--<img src="/storage/images/c5e0a814f77f9302ae6f6743fdbf4757.jpg" style="width:127.015px;height:64px;margin-left: 90px;
margin-top: 20px;
margin-bottom: 20px;"
                        alt="c5e0a814f77f9302ae6f6743fdbf4757.jpg">-->
                <p><span style="color: #ba3133;margin-left: 50px;"><b>Благодаря этому вы получите</b></span></p>

                <p style="padding-left: 40px;color:rgb(119,119,119);font-style:italic;"><span
                            style="line-height:20px;">длительный срок эксплуатации дома — около 100 лет.</span>

                </p>








            </li>
            <li>

                <p></p><b style="line-height:1.45em;"><span style="color:rgb(186,49,51);"><b style="line-height:1.45em;">Экономию на отоплении дома.</b></span></b></p>

                <p>Дома построенные по каркасной технологии очень теплые,</p>

                <!--<p><img src="/storage/images/17c138b0a4165c674d160a8b7a5d8643.jpg"
                        style="line-height: 1.45em; width: 127.015px; height: 64px;margin-left: 90px;
margin-top: 20px;
margin-bottom: 20px;" alt="17c138b0a4165c674d160a8b7a5d8643.jpg"></p>-->
                <p><span style="color: #ba3133;margin-left: 50px;"><b>Благодаря этому вы получите</b></span></p>

                <p><i><span
                                style="color:#747474;padding-left: 40px">экономию на отоплении дома 46%,</span></i><br><span
                        style="color:rgb(119,119,119);font-style:italic;line-height:1.45em;padding-left: 40px"> по сравнению с аналогичным домом из газобетона.</span>
                </p>
            </li>
        </ul>




        <p>Сравните потребление газа для дома <a href="http://profikarkas.com.ua/project/6-z10#.VA3Nwvl_tyU"
                                                 target="_blank">Z10</a>: </p>
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td><p>Материал внешних<span style="line-height:1.45em;">стен</span></p></td>
                <td>
                    <p style="text-align:center;">домокомплекта стандарт
                        <br> (деревянный каркас + утеплитель 200 мм)</p>
                </td>
                <td><p style="text-align:center;">газобетон<br><span style="line-height:1.45em;">(толщина внешних стен 375 мм + утеплитель 50 мм)</span>
                    </p></td>
            </tr>
            <tr>
                <td><p>Потребление газа за отопительный
                        сезон на сумму</p></td>
                <td><p style="text-align:center;">9119,65 грн.*</p></td>
                <td>
                    <p style="text-align:center;">16929,63 грн.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Разница</p>
                </td>
                <td>
                    <p style="text-align:center;"><b>Дешевле на 7809,98 грн, —46%! </b></p>
                </td>
                <td>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="clearfix"></div>
        <p style="float: right;">* — Если учитывать цены на газ по состоянию на 01.09.14.</p>
        <div class="clearfix"></div>

        <p><span style="font-size:15px;line-height:1.45em;">Кроме финансовой выгоды вы получите дополнительные приятные бонусы, от вашего дома, которые очень трудно измерить, но важность которых трудно переоценить:</span>
        </p>
        <ul>
            <li><span style="color:#ba3133;"><b>Надежность и высокое качество вашего дома</b>. </span>
                <ul>
                    <li><span style="line-height:1.45em;color:rgb(119,119,119);font-style:italic;">Мы ведем строительство каркасно-панельных домов по немецкой технологии, которая используется в Западной Европе более 100 лет, пользуется широкой популярностью во многих европейских странах, постоянно совершенствуется.</span>

                    </li>
                    <li><span style="line-height:1.45em;color:rgb(119,119,119);font-style:italic;">При строительстве дома мы применяем только сертифицированные высококачественные материалы.</span>
                    </li>
                    <li><span style="line-height:1.45em;color:rgb(119,119,119);font-style:italic;">Основную работу по строительству вашего дома мы производим в заводских условиях, что гарантирует точность изготовления конструкций, благоприятный режим температуры и влажности.</span>
                    </li>
                    <li><span style="line-height:1.45em;color:rgb(119,119,119);font-style:italic;">На строительной площадке квалифицированные специалисты монтируют из готовых конструкций в единое целое будущие сборные дома. Киев и область основной регион нашего строительства. Но мы готовы смонтировать ваш домокомплект в любом уголке нашей страны.</span>
                    </li>
                </ul>
            </li>
        </ul>
        <!--<p><img src="/storage/images/d2f7f5d63935afcf04e4947b3628a21b.jpg" style="width:127.015384615385px;height:64px;margin-left: 130px;
margin-top: 20px;
margin-bottom: 20px;"
                alt="d2f7f5d63935afcf04e4947b3628a21b.jpg"></p>-->
        <p><span style="color: #ba3133;margin-left: 90px;"><b>Благодаря этому вы получите</b></span></p>


        <p style="padding-left: 80px;color:#777;"><i>Надежный и качественный дом!</i></p>
        <ul>
            <li><span style="color:#ba3133;"><b style="line-height:1.45em;">Короткие сроки.</b></span>


                <p>
                    Немецкая каркасная технология позволяет возводить сборные дома в очень короткие сроки.
                </p>
                <p>
                <ul>
                    <li><span style="line-height:1.45em;">От заливки фундамента до конца внутренних отделочных работ проходит меньше полугода.</span><span
                            style="line-height:1.45em;"></span></li>
                    <li><span
                            style="line-height:1.45em;">Монтаж деревянного домокомплекта на участке занимает всего 1-2 недели.</span>
                    </li>
                </ul>
                </p>
                <!--<p><img src="/storage/images/9d2e3d506b870dfcfacc35b59a959796.jpg" style="width: 127.015384615385px; height: 64px;margin-left: 90px;
margin-top: 20px;
margin-bottom: 20px;"
                        alt="9d2e3d506b870dfcfacc35b59a959796.jpg"></p>-->
                <p><span style="color: #ba3133;margin-left: 50px;"><b>Благодаря этому вы получите</b></span></p>

                <p style="padding-left: 40px"><span style="line-height:1.45em;color: #777;"><i>Экономию своего времени!</i></span></p>






            </li>
        </ul>

        <ul>
            <li><span style="color:#ba3133;"><b style="line-height:1.45em;">Здоровый и комфортный микроклимат</b><span
                        style="line-height:1.45em;">.</span></span>


            <p>
                Каркасные дома возводятся из натуральных, экологически чистых материалов:
            </p>
                <p>
                <ul>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">дерево, из которого производится каркас дома,</span></i></span><span
                            style="line-height:1.45em;">    </span></li>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">минеральная вата Rockwool, которой заполняется каркас,</span></i></span><span
                            style="line-height:1.45em;">    </span></li>
                    <li><span style="line-height:1.45em;"><i><span style="color:#747474;">он является отличным и утеплителем, и шумоизолятором.</span></i></span><span
                            style="font-size:15px;">       </span></li>
                </ul>
                </p>

                <!--<p>
                    <img src="/storage/images/6903daae2c190fdb38df4c661f799320.jpg"
                         style="width:127.015384615385px;height:64px;margin-left: 90px;
margin-top: 20px;
margin-bottom: 20px;"
                         alt="6903daae2c190fdb38df4c661f799320.jpg"></p>-->
                <p><span style="color: #ba3133;margin-left: 50px;"><b>Благодаря этому вы получите</b></span></p>
                <p>
                    <ul>
                        <li><i><span style="line-height:1.45em;color: #777;">здоровый, комфортный микроклимат в доме,</span></i>
                        </li>
                        <li><i style="line-height:1.45em;color: #777;">отличную шумоизоляцию стен дома, лучшую чем у каменных стен.</i>
                        </li>
                    </ul>
                </p>


            </li>
        </ul>
        <p>И это еще не все! Кроме того, что наши дома являются выгодными и комфортными для вас,
            сотрудничество с нашей компанией имеет ряд неоспоримых выгод:</p>



        <p><span style="line-height:1.45em;"><span style="color:#ba3133;"><b>1. </b><span
                        style="font-weight:bold;">Широкий выбор проектов + проект дома в подарок!</span></span></span><span
                style="line-height:1.45em;">   </span></p>
        <ul style="
padding-left: 0 !important;">
            <ul>
                <li><span style="line-height:1.45em;">На нашем сайте представлено более 100 проектов домов, которые мы сможем построить для вас, от традиционных до современных, различной площади и этажности. Можем, также, рекомендовать типовые проекты коттеджей нашего партнера — архитектурного бюро Z500, каталог проектов которого
насчитывает более 700 проектов.</span><span style="font-size:15px;">   </span></li>
                <li><span style="line-height:1.45em;">Заказав строительство
дома в нашей компании, вы получите любой типовой </span><b style="line-height:1.45em;">проект Z500 стоимостью от 5400
                        грн. в подарок!</b><span style="font-size:15px;"> </span></li>
                <li><span style="line-height:1.45em;">При строительстве с нами
адаптация фундамента (стоимостью 2100 грн.) для вашего участка также
производится бесплатно.</span><span style="font-size:15px;">   </span></li>
            </ul>
        </ul>
        <p>Согласитесь, приятный финансовый бонус;). </p>

        <p>Более того любой проект дома Z500 при необходимости может быть изменен под ваши индивидуальные потребности!</p>

        <p><b style="line-height:1.45em;"><span style="color:#ba3133;">2.  Вариативность комплектации.</span></b></p>

        <p>Для вашего удобства мы разработали несколько типов комплектации каркаса дома:<span
                style="line-height:20px;font-size:15px;">       </span></p>
        <ul style="
padding-left: 0 !important;
">
            <ul>
                <li><span style="line-height:1.45em;">для самостоятельной достройки,</span></li>
                <li><span style="line-height:1.45em;">для дачи,</span><span style="line-height:1.45em;">   </span>
                </li>
                <li><span style="line-height:1.45em;">два варианта дома для круглогодичного проживания.</span></li>
            </ul>
        </ul>
        <p>Вы сможете быстро сориентироваться, что вы получите в результате, быстро выбрать наиболее
            подходящий тип каркаса.</p>

        <p><span style="color:#ba3133;"><b>3. Качественное строительство</b>. </span></p>

        <p>Каркасные дома (в Киеве и в любом другом регионе) гарантируют высокое качества домокомплекта благодаря: <span
                style="line-height:1.45em;"> </span></p>
        <ul>
            <ul style="
padding-left: 0 !important;
">
                <li><span style="line-height:1.45em;">заводскому производству конструкций дома,</span><span
                        style="font-size:15px;"></span></li>
                <li><span style="line-height:1.45em;">автоматизированному процессу производства домокомплекта,</span><span
                        style="font-size:15px;">   </span></li>
                <li><span
                        style="line-height:1.45em;">европейскому оборудованию, которым оснащено наше производство,</span><span
                        style="font-size:15px;"> </span></li>
                <li><span style="line-height:1.45em;">квалифицированным строителям.</span></li>
            </ul>
        </ul>
        <p>За счет этих преимуществ у нас есть возможность</p>
        <ul class="list-unstyled">
            <li>
                <ul>
                    <li><span style="line-height:1.45em;">максимально точно выполнить все проектные требования,</span><span
                            style="font-size:15px;">     </span></li>
                    <li><span style="line-height:1.45em;">свести к минимуму человеческий фактор.</span><span
                            style="line-height:1.45em;"> </span></li>
                </ul>
            </li>
        </ul>
        <p><span style="line-height:1.45em;">Что очень важно при строительстве дома по каркасной технологии.</span></p>

        <p>Монтаж домокомплекта на участке, также, производят профессиональные бригады, которые владеют всеми особенностями
            технологического процесса.</p>

        <p><span style="color:#ba3133;"><b>4.</b><b></b>
</span><b><span style="color:#ba3133;">Финансовая безопасность</span> </b>— строительство дома по гарантированной
            стоимости.</p>

        <p>Заключив договор сотрудничества с нашей компанией, вы сможете быть уверены, что <span>стоимость каркасного дома, зафиксированная в договоре, не изменится с течением времени. К дополнительным строительным работам (например, отделка фасада, разводка коммуникаций), которые вы можете заказать, прилагается детальная смета, в которой вы увидите, сколько и каких материалов будет израсходовано,
какой объем работ будет сделан, какова их стоимость.</span></p>

        <p><span style="color:#ba3133;"><b>5.</b><b></b>
<b>Уверенность в сроках.</b></span></p>

        <p>
            Благодаря налаженной работе с проверенными подрядчиками, вы получите готовый сборный дом в сроки, которые мы четко фиксируем в договоре.
        </p>

        <p><span style="color:#ba3133;"><b>6.</b><b></b>
<b>Дом под ключ — страховка от очень многих хлопот и рисков</b>.</span></p>

        <p>В нашей компании вы сможете заказать <span>строительство каркасных домов под ключ. Один
подрядчик – это один ответственный за все строительство, в случае оплошности он
сам ее исправит, не оправдываясь ошибками предыдущих строителей. Заключив
договор с одной компанией, вам не придется искать подрядчиков на разные виды
работ, контролировать их деятельность, покупать и доставлять строительные
материалы. Вы сэкономите очень много времени и сил, передав все хлопоты
строительного процесса одной организации. Вам останется только ввезти мебель в
новый дом! </span></p>

        <p>
            Наша компания ведет строительство каркасных домов в Киеве и области. В других регионах мы готовы выполнить для вас монтаж домокомплекта коттеджа.
        </p>

        <p><span style="color:#ba3133;"><b>7.</b><b></b>
<b>Экономия времени</b></span>, благодаря возможности строительства каркасных домов круглый год!</p>

        <p>
            <span style="line-height:1.45em;">Благодаря отсутствию мокрых работ, строительство сборных домов можно проводить в любое время года, в том числе зимой. Вы сможете въехать в готовый дом весной, не тратя драгоценное летнее время на стройку.</span>
        </p>

        <p><b><span style="color:#ba3133;">8.
Гарантия на построенный дом.</span></b></p>

        <p>По завершении строительства вы получите письменную гарантию на домокомплект, прописанную в договоре — 10 лет.</p>

        <p><i>Здесь мы перечислили самые основные преимущества, которые вы получите, работая с нами, на самом деле их
                больше. Для более близкого знакомства с нашей компанией заходите в гости — мы всегда вам рады!</i></p>
        </div>

</div>
