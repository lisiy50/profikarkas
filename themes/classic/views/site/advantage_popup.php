<div id="advantage-popup-wrapper" style='display: none; opacity: 0;'></div>

<div class="advantage-popup" style='opacity: 0;'>
    <span class="advantage-popup-close"></span>
    <div class="advantage-popup-inner">
        <p class="advantage-popup-text">
            <?= t('Узнайте, какие еще<br> <b>выгоды</b> вы получите<br> при строительстве<br> <b>каркасного дома</b>') ?>
        </p>
        <a href="http://profikarkas.com.ua#why_choose" class="advantage-popup-link"><?= t('Интересно') ?></a>
    </div>
</div>
