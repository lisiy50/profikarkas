<div class="contact_page clearfix">
    <div class="pull-left">
        <?php echo Yii::app()->config['contact_map']; ?>
    </div>
    <div class="pull-right contacts_container">
        <div class="contacts_slogan">
            <?= t('Заезжайте к нам в гости!') ?>
            <br>
            <br>
            <?= t('Будем рады вас увидеть и ответить на ваши вопросы.') ?>
        </div>

        <div class="contacts_adress"><?= config('contact_address')?></div>
        <div class="contacts_tell">
            Тел:
            <?php foreach(explode(',', config('contact_phone')) as $phoneNumber):?>
                <a href="tel:+38<?php echo preg_replace('#[^\d]*#', '', $phoneNumber) ?>"><?php echo $phoneNumber ?></a><br/>
            <?php endforeach;?>
        </div>
        <div class="contacts_mail">Email: info@profikarkas.com.ua</div>
        <div class="contacts_www">Web: www.profikarkas.com.ua</div>
<!--        <div class="contacts_skype">profikarkas</div>-->
        <ul class="list-inline list-unstyled social_block contacts_social_block">
            <li>
                <a href="https://www.facebook.com/Profikarkas?ref=hl" target="_blank">
                    <img style="width: 38px; height: 38px;" src="<?= Yii::app()->theme->baseUrl; ?>/img/social_fb.png">
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/profikarkas_ua/" target="_blank">
                    <img style="width: 38px; height: 38px;" src="<?= Yii::app()->theme->baseUrl; ?>/img/social_inst.png">
                </a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCCI9ZJ_tUFptZFpX-JBnoOA" target="_blank">
                    <img style="width: 38px; height: 38px;" src="<?= Yii::app()->theme->baseUrl; ?>/img/social_yt.png">
                </a>
            </li>
        </ul>
    </div>
</div>
