<h3>Карта сайта</h3>

<?php
$this->widget('Menu', array(
    'items' => MenuItem::model()->findAll('parent_id=1'),
    'nesting' => 3
));
?>

<?php
$this->widget('Menu', array(
    'items' => Category::model()->rooted()->findAll(),
    'nesting' => 6
));
?>

<?php
$this->widget('Menu', array(
    'items' => News::model()->limit(50)->findAll(),
    'template'=>'<a href="{url}">{title}</a>',
));
?>

<?php
$this->widget('Menu', array(
    'items' => Article::model()->limit(50)->findAll(),
    'template'=>'<a href="{url}">{title}</a>',
));
?>