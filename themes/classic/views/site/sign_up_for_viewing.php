<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 9/14/15
 * Time: 10:04 AM
 *
 * @var SiteController $this
 * @var SignUpForViewing $model
 */
?>

<div class="sign-up-for-viewing-bg navbar-fixed-bottom" id="sign-up-for-viewing">
    <div class="sign-up-for-viewing-wrapper container clearfix">

        <div class="sign-up-for-viewing-info">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/model.png">
            <div class="sign-up-for-viewing-title"><?= t('Запишитесь на просмотр!') ?></div>
            <div class="sign-up-for-viewing-txt"><?= t('Вы можете лично осмотреть дом, который сейчас строится. Для записи на просмотр введите запрашиваемые данные, и мы свяжемся с вами для уточнения деталей проведения экскурсии.') ?></div>
            <div class="sign-up-for-viewing-add"><?= t('<span>Данила</span><br> Руководитель отдела по работе с клиентами') ?></div>
        </div>
        <div class="sign-up-for-viewing-form" style="position: relative;">

            <?php if(Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php else: ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'enableClientValidation'=>false,
                    'action' => url('site/signUpForViewing'),
                    'id' => 'SignUpForViewing-form'
                ));
                /* @var CActiveForm $form */
                ?>
                <input type="hidden" id="g-recaptcha-response3" name="g-recaptcha-response">
                <input type="hidden" name="action" value="validate_captcha">
                <?php echo $form->textField($model, 'name', array('class'=>'sign-up-for-viewing-input', 'placeholder'=> t('Имя')))."<br>\n";?>
                <?php echo $form->textField($model, 'phone', array('class'=>'sign-up-for-viewing-input', 'placeholder'=> t('Номер телефона')))."<br>\n";?>
                <?php echo $form->textField($model, 'email', array('class'=>'sign-up-for-viewing-input', 'placeholder'=> t('Ваш E-mail')))."<br>\n";?>


                <?php echo CHtml::submitButton(t('Отправить'), array('class'=>"sign-up-for-viewing-send"));?>


                <?php $this->endWidget();?>

            <?php endif; ?>

            <div class="sign-up-for-viewing-form-loader" style="display:none; position:absolute;top: 0;left: 0;width: 100%;height: 100%;background-color: rgba(0, 0, 0, .5)">

            </div>

        </div>

        <?php echo CHtml::link('Закрыть', '#',array('class'=>"sign-up-for-viewing-close"));?>

    </div>
</div>

<script>
    function initCaptcha3() {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LepOLcZAAAAAOoPzQwsZWq45vQeqT8aaPpl1IYZ', {action: 'validate_captcha'}).then(function(token) {
                document.getElementById('g-recaptcha-response3').value=token;
            });
        });
    }

    window.setTimeout('initCaptcha3()', 4000);
</script>
