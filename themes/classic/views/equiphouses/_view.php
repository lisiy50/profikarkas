<div class="content_wrap clearfix equip_houses_content" id="t<?php echo $model->id;?>" style="<?php echo $k!=0?'display: none;':'';?>">
    <div style="float: left;">
        <?php if($model->image):?>
        <div class="pull-left img_wrap"><img src="<?php echo $model->getImageUrl('large');?>"></div>
        <div class="pull-left type_block">
            <?php echo $model->right_description;?>
        </div>
        <?php else:?>
            <?php echo $model->right_description;?>
        <?php endif;?>
    </div>
    <div class="equip_houses_bottom" style="float: left;">
        <?php echo $model->description;?>
    </div>
</div>
