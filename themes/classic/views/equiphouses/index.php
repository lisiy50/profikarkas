<ul class="list-unstyled list-inline equip_houses_nav clearfix" id="equip-houses-tabs">

    <?php foreach($models as $k => $model):?>
        <li class="<?php echo $k==0?'current':'';?>"><a href="#t<?php echo $model->id;?>"><?php echo $model->name;?></a></li>
    <?php endforeach;?>

</ul>

<?php foreach($models as $k => $model):?>
    <?php $this->renderPartial('_view', array('model'=>$model, 'k'=>$k));?>
<?php endforeach;?>

<?php
$script = <<<JS
$('#equip-houses-tabs a').click(function(){
    $('#equip-houses-tabs >li').removeClass('current');
    $(this).closest('li').addClass('current');
    $('.equip_houses_content').hide();
    $( $(this).attr('href') ).show();


    $('#equip-houses-tabs >li').css('background', 'url(<?php echo app()->theme->baseUrl;?>/img/border_header_top.png) right 50% no-repeat');
    $(this).closest('li').prev().css('background', 'none');
    return false;
});

$('#equip-houses-tabs >li').hover(function(){
    $(this).prev().css('background', 'none');
}, function(){
    if(!$(this).hasClass('current'))
        $(this).prev().css('background', 'url(<?php echo app()->theme->baseUrl;?>/img/border_header_top.png) right 50% no-repeat');
})
$('#equip-houses-tabs >li.current').prev().css('background', 'none');
JS;
cs()->registerScript('equiphouses-index', $script);
?>
