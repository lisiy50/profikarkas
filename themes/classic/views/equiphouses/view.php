<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 6/18/15
 * Time: 11:26 AM
 *
 * @var array $models - array of EquipHouses
 * @var EquipHouses $equipHouses
 */
?>

<ul class="list-unstyled list-inline equip_houses_nav clearfix" id="equip-houses-tabs">

    <?php foreach($models as $k => $model):?>
        <?php /* @var EquipHouses $equipHouses */?>
        <li class="<?php echo ($equipHouses->id == $model->id)?'current':'';?>">
            <a href="<?php echo $model->getUrl() ?>"><?php echo $model->name;?></a>
        </li>
    <?php endforeach;?>

</ul>

<div class="content_wrap clearfix equip_houses_content">
    <div style="float: left;">
        <?php if($equipHouses->image):?>
            <div class="pull-left img_wrap"><img src="<?php echo $equipHouses->getImageUrl('large');?>"></div>
            <div class="pull-left type_block">
                <?php echo $equipHouses->right_description;?>
            </div>
        <?php else:?>
            <?php echo $equipHouses->right_description;?>
        <?php endif;?>
    </div>
    <div class="equip_houses_bottom" style="float: left;">
        <?php echo $equipHouses->description;?>
    </div>
</div>

<style type="text/css">
    <?php if($model->h1_size):?>
    .equip_houses_bottom h1, .equip_houses_bottom h1 span {
        font-size: <?php echo $model->h1_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h2_size):?>
    .equip_houses_bottom h2, .equip_houses_bottom h2 span {
        font-size: <?php echo $model->h2_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h3_size):?>
    .equip_houses_bottom h3, .equip_houses_bottom h3 span {
        font-size: <?php echo $model->h3_size?>px !important;
    }
    <?php endif?>
    <?php if($model->h4_size):?>
    .equip_houses_bottom h4, .equip_houses_bottom h4 span {
        font-size: <?php echo $model->h4_size?>px !important;
    }
    <?php endif?>
</style>
