<style>
    body {
        display: flex;
        flex-direction: column;
        height: 100vh;
        min-height: 100vh;
    }
    .content {
        display: flex;
        flex: 1 0 auto;
    }
</style>

<section class="faq-page">
    <h1 class="faq-page__title"><?= t('Часто задаваемые вопросы') ?></h1>
    <div class="faq-page__wrapper">
        <?php echo $tpl ?>
    </div>
</section>





<?php

$script = <<<JS

$("#faqs").dcAccordion();

JS;

cs()->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.frontend.assets')).'/js/jquery.accordion.js');
cs()->registerScript('questions-index', $script);

?>

