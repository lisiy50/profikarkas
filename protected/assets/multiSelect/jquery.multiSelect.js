(function( $ ) {

    var requestIndex = 0;

    $.widget( "mz.multiSelect", {

        options: {
            template: '<tr><td style="width: 13px;">{checkBox}</td><td>{label}</td></tr>',
            source: null,
            resultElement: null,
            delay: 300,
            minLength: 1,
            itemTag: 'tr',
            requestData:function(){return {};}
        },

        pending: 0,

        _create: function() {
            this.element
                .addClass( "ui-autocomplete-input" )
                .attr( "autocomplete", "off" );

            this.result=$(this.options.resultElement);

            this._on( this.element, {
                keydown: function( event ) {
                    this._searchTimeout( event );
                },
                input: function( event ) {
                    this._searchTimeout( event );
                }
            })

            $(document).on('change', this.options.resultElement + ' :checkbox', function(){
                if($(this).is(':checked')) {
                    $(this).closest('tr').addClass('info');
                } else {
                    $(this).closest('tr').removeClass('info');
                }
            }).find(this.options.resultElement + ' :checkbox').change();

            this._initSource();
        },

        _searchTimeout: function( event ) {
            clearTimeout( this.searching );
            this.searching = this._delay(function() {
                // only search if the value has changed
                if ( this.term !== this._value() ) {
                    this.search( null, event );
                }
            }, this.options.delay );
        },

        search: function( value, event ) {
            value = value != null ? value : this._value();

            // always save the actual value, not the one passed as an argument
            this.term = this._value();

            if ( value.length < this.options.minLength ) {
                return;
            }

            if ( this._trigger( "search", event ) === false ) {
                return;
            }

            return this._search( value );
        },

        _search: function( value ) {
            this.pending++;
            this.element.addClass( "ui-autocomplete-loading" );

            var data=this.options.requestData();
            data.term=value;

            this.source(data, this._response() );
        },

        _response: function() {
            var that = this,
                index = ++requestIndex;

            return function( content ) {
                if ( index === requestIndex ) {
                    that._trigger( "response", null, { content: content } );
                    that._render(content);
                }

                that.pending--;
                if ( !that.pending ) {
                    that.element.removeClass( "ui-autocomplete-loading" );
                }
            };
        },

        _render: function(items){
            var that=this;

            $.each(this.result.find(this.options.itemTag), function() {
                if(!$(':checkbox', this).is(':checked'))
                    $(this).remove();
            })

            $.each( items, function( index, item ) {
                that._renderItem( item );
            });
        },

        _renderItem: function(item){
            var content=this._renderTemplate(item);
            return $(content).appendTo(this.result);
        },

        _renderTemplate: function(item){
            var template=this.options.template;
            for(var name in item) {
                while(template.indexOf('{'+name+'}')!=-1) {
                    template=template.replace('{'+name+'}', item[name]);
                }
            }

            return template;
        },

        _initSource: function() {
            var array, url,
                that = this;
            if ( $.isArray(this.options.source) ) {
                array = this.options.source;
                this.source = function( request, response ) {
                    response( $.ui.autocomplete.filter( array, request.term ) );
                };
            } else if ( typeof this.options.source === "string" ) {
                url = this.options.source;
                this.source = function( request, response ) {
                    if ( that.xhr ) {
                        that.xhr.abort();
                    }
                    that.xhr = $.ajax({
                        url: url,
                        data: request,
                        dataType: "json",
                        success: function( data ) {
                            response( data );
                        },
                        error: function() {
                            response( [] );
                        }
                    });
                };
            } else {
                this.source = this.options.source;
            }
        },

        _setOption: function( key, value ) {
            $.Widget.prototype._setOption.apply( this, arguments );
        },

        _destroy: function() {
            clearTimeout( this.searching );
            this.element
                .removeClass( "ui-autocomplete-input" )
                .removeAttr( "autocomplete" );
        },

        _value: function() {
            return this.element.val.apply( this.element, arguments );
        }
    });
}( jQuery ) );