<?php
class LibraryRecord extends ActiveRecord
{

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    public $status=self::STATUS_ENABLED;
    public $show_albom=self::STATUS_ENABLED;

    public function getDbCriteria($createIfNull = true)
    {
        if (IS_FRONTEND) {
            $criteria = parent::getDbCriteria(true);
            $alias = $this->getTableAlias(false, false);
            $criteria->addCondition("$alias.status=" . self::STATUS_ENABLED);
        } else {
            $criteria = parent::getDbCriteria($createIfNull);
        }
        return $criteria;
    }

    public function getIsVisible()
    {
        return ($this->status == self::STATUS_ENABLED);
    }

    public function getStatusIcon()
    {
        switch ($this->status) {
            case self::STATUS_ENABLED:
                return '<i class="icon-eye-open"></i>';
            case self::STATUS_DISABLED:
                return '<i class="icon-eye-close"></i>';
        }
    }

    public function getUrl($params=array()) {
        if(IS_BACKEND)
            $urlManager=Yii::app()->frontendUrlManager;
        else
            $urlManager=Yii::app()->urlManager;

        $route=lcfirst(get_class($this)).'/view';

        return $urlManager->createUrl($route, CMap::mergeArray(array('id'=>$this->primaryKey), $params));
    }
}