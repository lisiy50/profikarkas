<?php

/**
 * Class ActiveRecord
 */
class ActiveRecord extends CActiveRecord
{
    public $multyLangFields = [];

    private $_created=false;

    public function __get($name)
    {
        if (parent::__isset($name)) {
            return parent::__get($name);
        }

        if (in_array($name, $this->multyLangFields)) {
            return parent::__get($name . '_' . Yii::app()->language);
        }

        if (strpos($name, 'has') === 0) {
            $attr = 'count' . ucfirst(substr($name, 3));
            return $this->{$attr} > 0;
        }

        if (strpos($name, 'count') === 0) {
            $attr = substr($name, 5);
            $attr[0] = strtolower($attr[0]);

            if (isset($this->{$attr}) === false)
                return false;

            $arr = $this->{$attr};
            return is_array($arr) ? count($arr) : 0;
        }

        return parent::__get($name);
    }

    public function __isset($name) {
        if (parent::__isset($name)) {
            return true;
        }

        if (in_array($name, $this->multyLangFields)) {
            return true;
        }

        if (strpos($name, 'has') === 0 || strpos($name, 'count') === 0) {
            return true;
        }

        return false;
    }

    public function limit($limit)
    {
        $this->getDbCriteria()->mergeWith(array('limit' => $limit));
        return $this;
    }

    public function order($order)
    {
        $this->getDbCriteria()->mergeWith(array('order' => $order));
        return $this;
    }

    public function where($col, $val)
    {
        $this->getDbCriteria()->compare($col, $val);
        return $this;
    }

    public function condition($condition)
    {
        $this->getDbCriteria()->addCondition($condition);
        return $this;
    }

    public function like($col, $val)
    {
        $this->getDbCriteria()->addSearchCondition($col, $val);
        return $this;
    }

    protected function beforeCreate() {
        $this->_created=true;

        if($this->hasEventHandler('onBeforeCreate'))
        {
            $event=new CModelEvent($this);
            $this->onBeforeCreate($event);
            return $event->isValid;
        }
        else
            return true;
    }

    protected function afterCreate()
    {
        if($this->hasEventHandler('onAfterCreate'))
            $this->onAfterSave(new CEvent($this));
    }

    protected function beforeSave() {
        if(parent::beforeSave()) {
            if($this->isNewRecord)
                return $this->beforeCreate();
            else
                return true;
        } else
            return false;
    }

    protected function afterSave() {
        if($this->_created)
            $this->afterCreate();

        parent::afterSave();
    }

    public function onBeforeCreate($event)
    {
        $this->raiseEvent('onBeforeCreate',$event);
    }

    public function onAfterCreate($event) {
        $this->raiseEvent('onAfterCreate',$event);
    }

    public function getClassId()
    {
        return strtolower(get_class($this));
    }

}