<?php

class Controller extends CController
{

    public $breadcrumbs = array();
    public $assetsUrl;

    public function init()
    {
        $config = require Yii::getPathOfAlias('application.data.handlers') . '.php';
        foreach ($config as $event => $handlers) {
            if ($this->hasEvent($event)) {
                foreach ($handlers as $handler) {
                    $this->attachEventHandler($event, $handler);
                }
            }
        }

        if (isset($_POST['PHPSESSID'])) {
            Yii::app()->session->close();
            Yii::app()->session->sessionID = $_POST['PHPSESSID'];
            Yii::app()->session->init();
        }

        parent::init();
    }

    protected function registerScripts()
    {
        if (!app()->request->isAjaxRequest) {
            $cs = Yii::app()->getClientScript();

//            $cs->registerPackage('modernizr');

            $homeUrl = CJavaScript::encode(rtrim(Yii::app()->homeUrl, '/'));
            $cs->registerScript('CreateUrl', "
            $.createUrl=function(route, params){
                var url = $homeUrl + '/' + route;
                if(params)
                   url+='?' + $.param(params)
                return url;
            }
            $.ajaxPrefilter(function(options) {
                if(options.crossDomain==false && options.url.slice(0,5)!='http:' && options.url.slice(0,1)!='/') {
                    options.url = $homeUrl + '/' + options.url;
                }
            });
        ");
        }
    }

    protected function beforeRender($view)
    {
        if (parent::beforeRender($view)) {
            $this->registerScripts();

            return true;
        } else {
            return false;
        }
    }

    public function loadModel($id)
    {
        $class=substr(get_class($this),0,-10);
        $model=CActiveRecord::model($class)->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    // игнорируем относительные пути в модуле
    public function createUrl($route,$params=array(),$ampersand='&')
    {
        if($route==='')
            $route=$this->getId().'/'.$this->getAction()->getId();
        elseif(strpos($route,'/')===false)
            $route=$this->getId().'/'.$route;

        return Yii::app()->createUrl(trim($route,'/'),$params,$ampersand);
    }

}
