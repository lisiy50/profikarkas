<?php
class FrontendController extends Controller {
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/default';

    public $aux_view;

    public $aux_data;

    public $menu_id;

    public $highlight;

    public $additionalHeaderTags = '';

    public function init()
    {
        if(Yii::app()->user->hasState('referer')==false) {
            Yii::app()->user->setState('referer', CArray::get($_SERVER, 'HTTP_REFERER', ''));
        }

        $hostInfo = app()->request->hostInfo;
        $redirect = false;
        if(stripos(app()->request->hostInfo, 'http://www.') === 0){
            $hostInfo = str_replace('http://www.', 'http://', app()->request->hostInfo);
            $redirect = true;
        }

        $route = rtrim(str_replace(array('/index.php', '/index', '//'), array('', '', '/'), app()->request->url), '/');
        if(app()->request->url != '/' && !strstr(app()->request->url, $route)){
            $redirect = true;
        }

        if($redirect){
            $this->redirect($hostInfo . $route, true, 301);
        }

        parent::init();
    }

    public function processOutput($output)
    {
        $output=parent::processOutput($output);
        $output=preg_replace('/(<\\/body\s*>)/is',Yii::app()->config['counters'].'$1',$output,1);
        return $output;
    }

    protected function registerScripts()
    {
        if (!app()->request->isAjaxRequest) {
            $dir = Yii::getPathOfAlias('application.frontend.assets');
            $this->assetsUrl = Yii::app()->assetManager->publish($dir);

            $cs = app()->clientScript;

//            $cs->registerScriptFile('//connect.facebook.net/ru_RU/all.js#xfbml=1');

            $cs->registerScript('DisplayMessage', "
            window.displayMessage=function(text, type) {
                $.jGrowl(text, { theme: type });
            }
            $(document).ajaxError(function(){
                console.log('При ajax запросе произошла ошибка.');
            })
        ");

//            $cs->registerCssFile(Yii::app()->baseUrl . '/css/slick-theme.css');
//            $cs->registerCssFile(Yii::app()->baseUrl . '/css/slick.css');
//            $cs->registerCssFile($this->assetsUrl.'/css/style.css?p8');
//            $cs->registerCssFile($this->assetsUrl . '/css/styles.min.css');
//            $cs->registerCssFile($this->assetsUrl.'/css/print.css', 'print');

            package('cookie');

//            $cs->registerScriptFile($this->assetsUrl . '/js/accounting.js');
            $cs->registerScriptFile($this->assetsUrl . '/js/accounting.min.js');
            $cs->registerScriptFile(Yii::app()->baseUrl . '/front_end/js/parts/slick.min.js');
            $cs->registerScriptFile($this->assetsUrl . '/js/main.js');
//            $cs->registerScriptFile($this->assetsUrl . '/js/main.min.js');

            $cs->registerPackage('bootstrap3');

            $this->highlightMenu();
        }

        parent::registerScripts();
    }

    public function renderAux() {
        if($this->aux_view) {
            $this->renderPartial($this->aux_view, $this->aux_data);
        }
    }

    public function setAuxData($view, $data=array()) {
        $this->aux_view=$view;
        $this->aux_data=$data;
    }

    public function getViewFile($viewName)
    {
        if(($theme=Yii::app()->getTheme())!==null && ($viewFile=$theme->getViewFile($this,$viewName))!==false)
            return $viewFile;
        $moduleViewPath=$basePath=Yii::app()->getViewPath();
        if(($module=$this->getModule())!==null)
            $moduleViewPath=$module->getViewPath();
        return $this->resolveViewFile($viewName,$this->getViewPath(),$basePath,$moduleViewPath);
    }

    public function highlightMenu($highlight = NULL){
        if(!$highlight)
            $highlight = $this->highlight;
        if($highlight){
            cs()->registerScript('highlightMenu', "
            $('.content_nav li.id$highlight').addClass('current');
            $('.header_nav li.id'+$('.content_nav li.id$highlight').data('parent_id')).addClass('current').prev().css('background', 'none');
            $('.header_nav li.id$highlight').addClass('current').prev().css('background', 'none');
        ");
        }
    }

}
