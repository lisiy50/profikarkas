<?php
class BackendController extends Controller
{

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '/layouts/column2';
    public $icon = 'database';
    private $_package;
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    private $_topMenu;

    public function getTopMenu()
    {
        if ($this->_topMenu === null) {
            $items = include Yii::getPathOfAlias('application.backend.data.topMenu') . '.php';

            foreach(Yii::app()->packages as $package)
                $items=CMap::mergeArray($items, $package->getTopMenu());

            $this->_topMenu=$this->normalizeItems($items);
        }

        return $this->_topMenu;
    }

    public function setTopMenu($items)
    {
        $this->_topMenu = $items;
    }

    protected function normalizeItems($items) {
        $order=5;
        if(!is_array($items))
            return array();
        foreach($items as $i=>$item)
        {
            if(!is_array($item))
                $items[$i]=array('divider'=>true);

            if(!isset($item['label']))
                $items[$i]['label']=$i;

            if(isset($item['items']))
                $items[$i]['items']=$this->normalizeItems($item['items']);

            if(!isset($item['order']))
                $items[$i]['order']=$order++;
        }

        usort($items, array($this, 'sortItems'));

        $items=array_values($items);

        return $items;
    }

    public function sortItems($a, $b) {
        if($a['order']==$b['order'])
            return 0;

        return $a['order']>$b['order']?1:-1;
    }

    protected function registerScripts()
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerPackage('bootstrap');
        $cs->registerPackage('jgrowl');
        $cs->registerPackage('jquery.ui');

        $dir = Yii::getPathOfAlias('application.backend.assets');
        $this->assetsUrl = Yii::app()->assetManager->publish($dir);

        $cs->registerScriptFile($this->assetsUrl . '/js/main.js');

        $viewJs=$this->getViewPath() . DS . 'js' . DS . $this->action->id . '.js';
        if (file_exists($viewJs)) {
            $cs->registerScriptFile(CHtml::asset($viewJs));
        }

        $cs->registerScript('init', "
            window.displayMessage=function(text, type) {
                $.jGrowl(text, { theme: type });
            }
            $(document).ajaxError(function(){
                $.jGrowl('При ajax запросе произошла ошибка. Перезагрузите страницу', { theme: 'error' });
            })
        ");

        if ($message=Yii::app()->user->getFlash('success')) {
            $message=CJavaScript::encode($message);
            $cs->registerScript('CallDisplayMessage', "displayMessage($message, 'success')");
        } else if ($message=Yii::app()->user->getFlash('error')) {
            $message=CJavaScript::encode($message);
            $cs->registerScript('CallDisplayMessage', "displayMessage($message, 'error')");
        }

        parent::registerScripts();
    }

    public function getViewPath()
    {
        if(($package=$this->getPackage())===null)
            $package=Yii::app();
        return $package->getViewPath().DIRECTORY_SEPARATOR.$this->getId();
    }

    public function getPackage() {
        return $this->_package;
    }

    public function setPackage(BasePackage $package) {
        $this->_package=$package;
    }

}
