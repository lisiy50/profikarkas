<?php
abstract class BasePackage extends CComponent {

    private $_id;
    private $_basePath;
    private $_viewPath;
    private $_controllerPath;
    private $_controllerMap;

    public $behaviors=array();

    public $import=array();

    public function __construct($config=null)
    {
        if(is_string($config))
            $config=require($config);
        if(isset($config['basePath']))
        {
            $this->setBasePath($config['basePath']);
            unset($config['basePath']);
        }

        $id=$config['id'];

        Yii::setPathOfAlias($id,$this->getBasePath());

        $this->preinit();

        $this->configure($config);

        $this->attachBehaviors($this->behaviors);

        foreach($this->import as $alias)
            Yii::import($alias);

        $this->init();
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id=$id;
    }

    public function init() {

    }

    public function preinit() {

    }

    public function getControllerMap() {
        if($this->_controllerMap===null) {
            $path=$this->getControllerPath();
            if(!file_exists($path))
                return array();

            $fileNames=scandir($path);
            foreach($fileNames as $fileName) {
                if(strpos($fileName, 'Controller.php')!==false) {
                    $name=substr($fileName,0,-14);
                    $name[0]=strtolower($name[0]);

                    $this->_controllerMap[$name]=array(
                        'class'=>$this->getId().'.controllers.'.substr($fileName,0,-4),
                        'package'=>$this,
                    );
                }
            }
        }

        return $this->_controllerMap;
    }

    /**
     * Returns the root directory of the module.
     * @return string the root directory of the module. Defaults to the directory containing the module class.
     */
    public function getBasePath()
    {
        if($this->_basePath===null)
        {
            $class=new ReflectionClass(get_class($this));
            $this->_basePath=dirname($class->getFileName());
        }
        return $this->_basePath;
    }

    public function getViewPath()
    {
        if($this->_viewPath!==null)
            return $this->_viewPath;
        else
            return $this->_viewPath=$this->getBasePath().DIRECTORY_SEPARATOR.'views';
    }

    /**
     * @return string the directory that contains the controller classes. Defaults to 'moduleDir/controllers' where
     * moduleDir is the directory containing the module class.
     */
    public function getControllerPath()
    {
        if($this->_controllerPath!==null)
            return $this->_controllerPath;
        else
            return $this->_controllerPath=$this->getBasePath().DIRECTORY_SEPARATOR.'controllers';
    }

    public function configure($config)
    {
        if(is_array($config))
        {
            foreach($config as $key=>$value)
                $this->$key=$value;
        }
    }

    public function getTopMenu() {
        return array();
    }

}