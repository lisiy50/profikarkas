<?php

/**
 * This is the model class for table "{{home_kit_relation}}".
 *
 * The followings are the available columns in table '{{home_kit_relation}}':
 * @property integer $home_kit_id
 * @property integer $packaging_home_kit_id
 * @property string $value
 * @property integer $price
 */
class HomeKitRelation extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return HomeKitRelation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{home_kit_relation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('home_kit_id, packaging_home_kit_id', 'required'),
			array('home_kit_id, packaging_home_kit_id, price', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('home_kit_id, packaging_home_kit_id, value, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'homeKit' => array(self::BELONGS_TO, 'HomeKit', 'home_kit_id'),
            'packagingHomeKit' => array(self::BELONGS_TO, 'PackagingHomeKit', 'packaging_home_kit_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'home_kit_id' => 'Домокомплект',
			'packaging_home_kit_id' => 'Комплектация домокомплекта',
			'value' => 'Значение',
			'price' => 'Цена',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('home_kit_id',$this->home_kit_id);
		$criteria->compare('packaging_home_kit_id',$this->packaging_home_kit_id);
		$criteria->compare('value',$this->value);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}