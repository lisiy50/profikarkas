<?php

/**
 * This is the model class for table "{{equip_houses}}".
 *
 * The followings are the available columns in table '{{equip_houses}}':
 * @property integer $id
 * @property string $name
 * @property string $name_uk
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_de
 * @property string $right_description
 * @property string $right_description_uk
 * @property string $right_description_en
 * @property string $right_description_fr
 * @property string $right_description_de
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_fr
 * @property string $description_de
 * @property string $image
 * @property integer $status
 * @property integer $watermark
 * @property string $create_time
 * @property string $update_time
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 */
class EquipHouses extends LibraryRecord
{

    public $watermark = self::STATUS_ENABLED;

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 70, 70, 'resize' => 'fill', 'required' => 'thumb.jpg'),
            'large' => array('storage/equipHouses', 1000, 800, 'resize'=>'max', 'prefix' => 'large_'),
            'original' => array('storage/news', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->watermark == EquipHouses::STATUS_ENABLED){
            $images['large'] = array('storage/equipHouses', 1000, 800, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }

        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'equipHouses/view',
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'name',
                'images' => $images,
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @return EquipHouses the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{equip_houses}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'filter', 'filter'=>'strip_tags'),
//            array('content','filter','filter'=>array($obj=new EHtmlPurifier(),'purify')),
            array('name, status', 'required'),
            array('status, watermark', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('name, name_uk, name_en, name_fr, name_de', 'length', 'max'=>255),
            array('right_description, right_description_uk, right_description_en, right_description_fr, right_description_de, description, description_uk, description_en, description_fr, description_de', 'safe'),
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
            array('image', 'unsafe'),
            array('h1_size,h2_size,h3_size,h4_size', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, status, watermark', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название (Русский)',
            'name_uk' => 'Название (Украинский)',
            'name_en' => 'Название (Английский)',
            'name_fr' => 'Название (Французский)',
            'name_de' => 'Название (Немецкий)',
            'right_description' => 'Описание справа (Русский)',
            'right_description_uk' => 'Описание справа (Украинский)',
            'right_description_en' => 'Описание справа (Английский)',
            'right_description_fr' => 'Описание справа (Французский)',
            'right_description_de' => 'Описание справа (Немецкий)',
            'image' => 'Изображение',
            'status' => 'Включено',
            'watermark' => 'Водяной знак',
            'description' => 'Содержание (Русский)',
            'description_uk' => 'Содержание (Украинский)',
            'description_en' => 'Содержание (Английский)',
            'description_fr' => 'Содержание (Французский)',
            'description_de' => 'Содержание (Немецкий)',
            'create_time' => 'Добавлена',
            'update_time' => 'Изменена',
            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('watermark',$this->watermark);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'update_time DESC',
            ),
        ));
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->name);
        return strtolower(parent::getUrl($params));
    }

}
