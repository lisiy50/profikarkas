<?php

/**
 * RecomendProjectForm class.
 * RecomendProjectForm is the data structure for keeping
 * contact form data. It is used by the 'writeTheCompany' action of 'SiteController'.
 */
class RecomendProjectForm extends CFormModel
{
    public $email;
    public $body;
    public $senderEmail;
    public $copy;
    public $agreement;
//    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('email, agreement', 'required', 'message'=>t('Обязательно для заполнения')),
            array('body','safe'),
            array('email, senderEmail', 'email'),
            array('agreement', 'in', 'range'=>array(1)),
            array('copy, agreement', 'in', 'range'=>array(0, 1)),
//            array('verifyCode', 'captcha', 'allowEmpty'=>!Yii::app()->config['contact_use_captcha'] && !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'email'=>t('Адрес эл. почты знакомого'),
            'body'=>t('Ваш комментарий'),
            'senderEmail'=>t('Ваш адрес эл. почты'),
            'copy'=>t('отправьте мне копию эл. сообщения'),
            'agreement'=>t('я согласен на обработку данных компанией Профикаркас'),
//            'verifyCode'=>t('Проверочный код'),
        );
    }
}