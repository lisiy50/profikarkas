<?php
class RemindPasswordForm extends CFormModel
{
	public $email;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('email', 'required'),
            array('email', 'emailExists'),
            array('email', 'email'),
		);
	}
	public function attributeLabels()
	{
		return array(
            'email'=>'E-mail',
		);
	}

    public function emailExists($attribute = 'email', $params = array()){
        if($this->getUserByEmail()===null)
            $this->addError($attribute, t('Пользователя с таким E-mail не существует.'));
    }

    /**
     * @param null $email
     * @return User|null
     */
    public function getUserByEmail($email = null){
        if($email)
            return User::model()->findByAttributes(array('username'=>$email));
        if($this->email)
            return User::model()->findByAttributes(array('username'=>$this->email));
        return null;
    }
}