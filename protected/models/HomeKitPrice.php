<?php

/**
 * Class HomeKitPrice
 */

class HomeKitPrice extends CComponent {

    const ONE_C_PRICE = 'v';
    const ONE_C_NAME = 'p';
    const ONE_C_VARIANT = 's';
    const ONE_C_HOME_KIT = 'n';
    const ONE_C_WORK_SCOPE = 'l';

    public static $required_home_kits = [1, 2, 3, 4];
    public static $required_work_scopes = ['A', 'B', 'C', 'D', 'F'];

    private $_data = [];

    private function getPath()
    {
        return Yii::getPathOfAlias('application') . DS . '..' . DS . 'storage' . DS . 'homeKitPrice';
    }

    private function isDataExists()
    {
        return file_exists($this->getPath());
    }

    public function setData($data)
    {
        while(!file_put_contents($this->getPath(), $data)) {}
        return true;
    }

    public function getData()
    {
        if ($this->isDataExists()) {
            $this->_data = json_decode(substr(file_get_contents($this->getPath()), 3), true);
        }
        return $this->_data;
    }

    private static function getAllProjects()
    {
        $arr = [];
        $projects = Yii::app()->db->createCommand()
            ->select('id, symbol, variation_symbol')
            ->from('tbl_project')
            ->queryAll();
        foreach ($projects as $project) {
            $name = strtolower(trim($project['symbol']));
            if ($project['variation_symbol'] == true) {
                $name .= strtolower(trim($project['variation_symbol']));
            }
            $arr[$name] = $project['id'];
        }
        return $arr;
    }

    public static function isProject($name)
    {
        return array_key_exists($name, self::getAllProjects());
    }

    public static function getProjectId($name)
    {
        return self::getAllProjects()[$name];
    }

    private static function getIDs($table)
    {
        $arr = [];
        $items = Yii::app()->db->createCommand()
            ->select('id')
            ->from($table)
            ->queryAll();
        foreach ($items as $item) {
            $arr[] = $item['id'];
        }
        return $arr;
    }

    private static function getMappedIDs($arr1, $arr2)
    {
        $arr = [];
        $mapped = array_map(null, $arr1, $arr2);
        foreach ($mapped as $item) {
            $arr[$item[1]] = $item[0];
        }
        return $arr;
    }

    public static function getHomeKitsIDs()
    {
        return self::getMappedIDs(self::getIDs('tbl_home_kit'), self::$required_home_kits);
    }

    public static function getWorkScopesIDs()
    {
        return self::getMappedIDs(self::getIDs('tbl_work_scope'), self::$required_work_scopes);
    }
}
