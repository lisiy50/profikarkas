<?php

/**
 * This is the model class for table "{{project}}".
 *
 * The followings are the available columns in table '{{project}}':
 * @property integer $id
 * @property integer $z500_id_primary
 * @property string $symbol
 * @property string $symbol_sort_letter
 * @property integer $symbol_sort_number
 * @property string $variation_symbol
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $title_de
 * @property string $title_fr
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_de
 * @property string $description_fr
 * @property double $house_height
 * @property double $living_room_height
 * @property double $cubage
 * @property double $minimum_building_area
 * @property integer $roof_2_slope
 * @property integer $roof_4_slope
 * @property integer $roof_many_slope
 * @property integer $roof_flat
 * @property double $roof_angle
 * @property double $roof_angle1
 * @property double $roof_area
 * @property double $roof_canopy1
 * @property double $roof_canopy2
 * @property double $roof_canopy3
 * @property double $roof_canopy4
 * @property double $net_area
 * @property double $usable_area
 * @property double $usable_area_by_order
 * @property double $basement_area
 * @property double $external_dimensions_area
 * @property double $garage_area
 * @property double $cars_in_garage
 * @property double $loft_area
 * @property double $plot_width
 * @property double $plot_length
 * @property double $facade_width
 * @property double $facade_height
 * @property string $video_ids
 * @property string $video1_ids
 * @property string $overlap
 * @property string $applying
 * @property string $facade
 * @property string $wall_material
 * @property string $roof_material
 * @property string $heating
 * @property integer $floors
 * @property integer $attic_floor
 * @property integer $rooms
 * @property integer $first_floor_room
 * @property integer $status
 * @property integer $show_on_landing
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $priority
 * @property integer $price
 * @property file $csvFile
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 * @property integer $has_wideo_walks
 * @property string $z500_url_key
 *
 * @property array $similars - array of Project models
 * @property Comment[] $comments
 * @property ProjectImage[] $images
 * @property ProjectImage[] $iVisualizations
 * @property ProjectImage[] $iInteriors
 * @property ProjectImage[] $iFacades
 * @property ProjectImage[] $iLayouts
 * @property ProjectImage[] $iAreas
 *
 * @property  VideoWalk[] $videoWalks
 */
class Project extends LibraryRecord
{

    public $priority = 25;

    private $_nextProjectModel = false;
    private $_prevProjectModel = false;

    public $workScopePrice = false;

    public $csvFile;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project}}';
	}

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        $scopes = array(
            'order' => "$alias.priority DESC, create_time DESC",
        );

        return $scopes;
    }

    public function scopes(){
        return array(
            'directOrder' => array(
                'order' => 'priority DESC, create_time DESC',
            ),
            'reverseOrder' => array(
                'order' => 'priority, create_time',
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
//        roof_2_slope roof_4_slope roof_many_slope roof_flat
//        floors attic_floor rooms first_floor_room

		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('symbol', 'required'),
			array('symbol_sort_number, floors, attic_floor, rooms, first_floor_room, roof_2_slope, roof_4_slope, roof_many_slope, roof_flat, status, show_on_landing, create_time, update_time, price, h1_size,h2_size,h3_size,h4_size,has_wideo_walks', 'numerical', 'integerOnly'=>true),
			array('house_height, living_room_height, minimum_building_area, roof_angle, roof_angle1, roof_area, roof_canopy1, roof_canopy2, roof_canopy3, roof_canopy4, net_area, usable_area, usable_area_by_order, basement_area, external_dimensions_area, garage_area, cars_in_garage, loft_area, plot_width, plot_length, facade_width, facade_height, cubage', 'numerical'),
			array('symbol, symbol_sort_letter', 'length', 'max'=>10),
			array('variation_symbol, z500_url_key', 'length', 'max'=>20),
			array('title, title_uk, title_en, title_de, title_fr', 'length', 'max'=>200),
			array('video_ids, video1_ids', 'length', 'max'=>500),
			array('description, description_uk, description_en, description_de, description_fr, overlap, applying, facade, wall_material, roof_material, heating', 'safe'),
//            array('z500_id_primary', 'unique'),
            array('priority', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>99),
            array('workScopePrice', 'safe'),
            array('csvFile', 'file', 'types'=>'csv', 'on'=>'csvImport'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, z500_id_primary, symbol, symbol_sort_letter, symbol_sort_number, variation_symbol, title, title_uk, title_en, title_de, title_fr, description, description_uk, description_en, description_de, description_fr, house_height, living_room_height, minimum_building_area, roof_angle, roof_angle1, roof_area, roof_canopy1, roof_canopy2, roof_canopy3, roof_canopy4, net_area, usable_area, usable_area_by_order, basement_area, external_dimensions_area, garage_area, cars_in_garage, loft_area, plot_width, plot_length, facade_width, facade_height, overlap, applying, facade, wall_material, roof_material, heating, status, show_on_landing, create_time, update_time, priority, has_wideo_walks', 'safe', 'on'=>'search'),

            array('symbol', 'symbolValidator'),
		);
	}


    public function symbolValidator($attribute,$params)
    {
        if ($this->symbol) {
            if ($this->isNewRecord)
                $exists = Project::model()->exists('symbol=:symbol AND (variation_symbol = :variation_symbol)', array(':symbol' => $this->symbol, ':variation_symbol' => $this->variation_symbol));
            else
                $exists = Project::model()->exists('symbol=:symbol AND (variation_symbol = :variation_symbol) AND id<>:id', array(':symbol' => $this->symbol, ':variation_symbol' => $this->variation_symbol, ':id' => $this->id));
            if ($exists) {
                $this->addError('symbol','Проект уже существует.');
            }
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'news'=>array(self::HAS_MANY, 'News', 'project_id'),
            'portfolios'=>array(self::HAS_MANY, 'Portfolio', 'project_id'),
            'comments'=>array(self::HAS_MANY, 'Comment', 'owner_id', 'condition' => 'comments.owner = :owner AND comments.parent_id IS NULL', 'params' => array(':owner' => __CLASS__)),
            'countComments'=>array(self::STAT, 'Comment', 'owner_id', 'condition' => 'owner = :owner', 'params' => array(':owner' => __CLASS__)),

            'images'=>array(self::HAS_MANY, 'ProjectImage', 'project_id'),
            'iVisualizations'=>array(self::HAS_MANY, 'ProjectImage', 'project_id', 'condition' => 'type = :type', 'params' => array(':type' => ProjectImage::TYPE_VISUALIZATION)),
            'iInteriors'=>array(self::HAS_MANY, 'ProjectImage', 'project_id', 'condition' => 'type = :type', 'params' => array(':type' => ProjectImage::TYPE_INTERIOR)),
            'iFacades'=>array(self::HAS_MANY, 'ProjectImage', 'project_id', 'condition' => 'type = :type', 'params' => array(':type' => ProjectImage::TYPE_FACADE)),
            'iLayouts'=>array(self::HAS_MANY, 'ProjectImage', 'project_id', 'condition' => 'type = :type', 'params' => array(':type' => ProjectImage::TYPE_LAYOUT)),
            'iAreas'=>array(self::HAS_MANY, 'ProjectImage', 'project_id', 'condition' => 'type = :type', 'params' => array(':type' => ProjectImage::TYPE_AREA)),
		);
	}

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'project/view',
                'params' => array('id' => $this->id),
            ),
//            'projectImage' => array(
//                'class' => 'ProjectImageBehavior',
//            ),
            'projectVideo' => array(
                'class' => 'VideoBehavior',
                'video_ids_attr' => 'video_ids',
            ),
            'projectVideo1' => array(
                'class' => 'VideoBehavior',
                'video_ids_attr' => 'video1_ids',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'z500_id_primary' => 'Z500 Id Primary',
			'symbol' => 'Symbol',
			'variation_symbol' => 'Variation Symbol',
			'title' => 'Название (Русский)',
			'title_uk' => 'Название (Украинский)',
			'title_en' => 'Название (Английский)',
			'title_de' => 'Название (Немецкий)',
			'title_fr' => 'Название (Французский)',
			'description' => 'Описание (Русский)',
            'description_uk' => 'Описание (Украинский)',
            'description_en' => 'Описание (Английский)',
            'description_de' => 'Описание (Немецкий)',
            'description_fr' => 'Описание (Французский)',
			'house_height' => 'Высота дома',
			'living_room_height' => 'Living Room Height',
			'minimum_building_area' => 'Минимальная площадь застройки',
			'roof_angle' => 'Roof Angle',
			'roof_angle1' => 'Roof Angle1',
			'roof_area' => 'Площадь крыши',
			'roof_canopy1' => 'Roof Canopy1',
			'roof_canopy2' => 'Roof Canopy2',
			'roof_canopy3' => 'Roof Canopy3',
			'roof_canopy4' => 'Roof Canopy4',
			'net_area' => 'Net Area',
			'usable_area' => 'Usable Area',
			'usable_area_by_order' => 'Usable Area By Order',
			'basement_area' => 'Площадь подвала (значение = площадь, -1 = отсутствует, -2 = по желанию клиента)',
			'external_dimensions_area' => 'Площадь дома по внешним габаритам',
			'garage_area' => 'Площадь гаража',
			'cars_in_garage' => 'Машин в гараже',
			'loft_area' => 'Loft Area',
			'plot_width' => 'Ширина участка',
			'plot_length' => 'Высота участка',
			'facade_width' => 'Facade Width',
			'facade_height' => 'Facade Height',
			'video_ids' => 'ID видео youtube и vimeo через ";"',
			'video1_ids' => 'ID видео youtube и vimeo через ";" в слайдере',
			'overlap' => 'Overlap',
			'applying' => 'Applying',
			'facade' => 'Facade',
			'wall_material' => 'Wall Material',
			'roof_material' => 'Roof Material',
			'heating' => 'Heating',

            'floors' => 'Количество этажей',
            'attic_floor' => 'Мансарда',
            'rooms' => 'Количество комнат',
            'first_floor_room' => 'Комната на первом этаже',

            'roof_2_slope'=>'Двускатная крыша',
            'roof_4_slope'=>'Четырех скатная крыша',
            'roof_many_slope'=>'Многоскатная крыша',
            'roof_flat'=>'Плоская крыша',

            'cubage' => 'Кубатура',

			'status' => 'Status',
			'show_on_landing' => 'Показывать на лендинге',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
            'priority' => 'Приоритет',
            'price' => 'Цена',
            'csvFile' => 'CSV файл',

            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',

            'z500_url_key' => 'Ключ проекта z500 (должен совпадать с URL)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        if(is_array($this->usable_area_by_order) && isset($this->usable_area_by_order['from']) && isset($this->usable_area_by_order['till'])) {
            $criteria->compare('usable_area_by_order','>='.$this->usable_area_by_order['from']);
            $criteria->compare('usable_area_by_order','<='.$this->usable_area_by_order['till']);
        } else {
            $criteria->compare('usable_area_by_order',$this->usable_area_by_order);
        }

        if(is_array($this->net_area) && isset($this->net_area['from']) && isset($this->net_area['till'])) {
            $criteria->compare('net_area','>='.$this->net_area['from']);
            $criteria->compare('net_area','<='.$this->net_area['till']);
        } else {
            $criteria->compare('net_area',$this->net_area);
        }

        $criteria->compare('rooms',$this->rooms);
        $criteria->compare('symbol',$this->symbol);

        $criteria->compare('garage_area', $this->garage_area);
        $criteria->compare('cars_in_garage', $this->cars_in_garage);
        $criteria->compare('first_floor_room', $this->first_floor_room);
        $criteria->compare('attic_floor', $this->attic_floor);

        $roofSlipeCirteria = new CDbCriteria();
        $roofSlipeCirteria->compare('roof_2_slope', $this->roof_2_slope, false, 'OR');
        $roofSlipeCirteria->compare('roof_4_slope', $this->roof_4_slope, false, 'OR');
        $roofSlipeCirteria->compare('roof_many_slope', $this->roof_many_slope, false, 'OR');
        $roofSlipeCirteria->compare('roof_flat', $this->roof_flat, false, 'OR');
        $criteria->mergeWith($roofSlipeCirteria);

        $criteria->compare('id',$this->id);
        $criteria->compare('symbol',$this->symbol,true);
        $criteria->compare('variation_symbol',$this->variation_symbol,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('title_uk',$this->title_uk,true);
        $criteria->compare('title_en',$this->title_en,true);
        $criteria->compare('title_de',$this->title_de,true);
        $criteria->compare('title_fr',$this->title_fr,true);
        $criteria->compare('floors', $this->floors);
		$criteria->compare('status',$this->status);
		$criteria->compare('show_on_landing',$this->show_on_landing);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('has_wideo_walks',$this->has_wideo_walks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /*
     * @return array of overlap materials
     * */
    public function getOverlaps(){
        return array(
            1=>'часторебристый',
            2=>'железобетонный',
            3=>'по деревянным балкам',
            4=>'монолитное перекрытие',
        );
    }

    /*
     * @return array of building applying
     * */
    public function getApplyings(){
        return array(
            1=>'по деревянным балкам',
            2=>'офия',
            3=>'современный',
            4=>'для бизнеса',
            5=>'Два входа',
            6=>'для двух семей',
            7=>'Вход через гараж',
            8=>'дачный',
        );
    }

    /*
     * @return array of building facade
     * */
    public function getFacades(){
        return array(
            1=>'эркер',
            2=>'боковая терасса',
            3=>'навес',
            4=>'стеклянные конструкции большой площади',
            5=>'зимний сад как ваирант',
            6=>'изменение месторасоложения терассы как вариант',
            7=>'компактные очертания',
        );
    }

    /*
     * @return array of building wall materials
     * */
    public function getWallMaterials(){
        return array(
            1=>'газобетон',
            2=>'керамические блоки',
            3=>'силикатные блоки',
            4=>'деревянный каркас',
            5=>'брусья',
        );
    }

    /*
     * @return array of building wall materials
     * */
    public function getRoofMaterials(){
        return array(
            1=>'керамическая черепица',
            2=>'металлочерепица',
            3=>'цементно-песчаная черепица',
            4=>'ПВХ мембрана',
            5=>'листовое железо',
            6=>'гонт',
            7=>'плоский',
            8=>'зеленая крыша',
        );
    }

    /*
     * @return array of building wall materials
     * */
    public function getHeatings(){
        return array(
            1=>'эко-орешек',
            2=>'твердое топливо',
            3=>'газ',
            4=>'масло',
            5=>'тепловой насос',
            6=>'электрическое',
            7=>'солнечный',
            8=>'камин на пелеты',
        );
    }

    /*
     * @return array of building wall materials
     * */
    public function getCarsInGarage(){
        return array(
            '0'=>'0',
            '1.0'=>'1',
            '1.5'=>'1+',
            '2.0'=>'2',
            '2.5'=>'2+',
            '3.0'=>'3',
            '3.5'=>'3+',
        );
    }

    /*
     * virtual DB fields
     * */
    public function setOverlap(array $val){
        $this->_overlap = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getOverlap(){
        return explode(';', $this->_overlap);
    }

    public function setApplying(array $val){
        $this->_applying = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getApplying(){
        return explode(';', $this->_applying);
    }

    public function setFacade(array $val){
        $this->_facade = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getFacade(){
        return explode(';', $this->_facade);
    }

    public function setWall_material(array $val){
        $this->_wall_material = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getWall_material(){
        return explode(';', $this->_wall_material);
    }

    public function setRoof_material(array $val){
        $this->_roof_material = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getRoof_material(){
        return explode(';', $this->_roof_material);
    }

    public function setHeating(array $val){
        $this->_heating = implode(';', $val);
    }
    /*
     * @return array
     * */
    public function getHeating(){
        return explode(';', $this->_heating);
    }

    public function getName(){
        return $this->symbol.(!empty($this->variation_symbol)?$this->variation_symbol:'');
    }
    /*
     * END virtual DB fields
     * */

    public function getUrl($params = array()){
        if(!isset($params['uri']))
            $params['uri'] = strtolower(app()->translitFormatter->formatUrl($this->getName()));
        return parent::getUrl($params);
    }

    public function getBasementArea(){
//        значение = площадь, -1 = отсутствует, -2 = по желанию клиента
        switch($this->basement_area){
            case '-2':
                return t('по желанию клиента');
            case '-1':
            case '0':
                return t('отсутствует');
            default:
                return $this->basement_area;

        }
    }

    public function getFirstImageUrl(/*$type = 'visualization'*/) {
        if ($this->iVisualizations) {
            return $this->iVisualizations[0];
        }
        return null;
    }

    public function getNextProject(){
        if($this->_nextProjectModel === false){
            $this->_nextProjectModel = self::model()->find('priority <= ? AND create_time < ?', array($this->priority, $this->create_time));
        }
        return $this->_nextProjectModel;
    }

    public function getPrevProject(){
        if($this->_prevProjectModel === false)
            $this->_prevProjectModel = self::model()->resetScope()->reverseOrder()->find('priority >= ? AND create_time > ?', array($this->priority, $this->create_time));
        return $this->_prevProjectModel;
    }

    protected function afterSave(){
        parent::afterSave();

        if($this->workScopePrice && is_array($this->workScopePrice)){
            foreach($this->workScopePrice as $workScopePrice){
                $workScopePriceModel = WorkScopePrice::model()->findByPk(array('project_id'=>$this->id, 'home_kit_id'=>$workScopePrice['home_kit_id'], 'work_scope_id'=>$workScopePrice['work_scope_id']));
                if(!$workScopePrice['price'] && $workScopePriceModel){
                    $workScopePriceModel->delete();
                } elseif($workScopePrice['price']) {
                    if(!$workScopePriceModel)
                        $workScopePriceModel = new WorkScopePrice();
                    $workScopePriceModel->project_id = $this->id;
                    $workScopePriceModel->home_kit_id = $workScopePrice['home_kit_id'];
                    $workScopePriceModel->work_scope_id = $workScopePrice['work_scope_id'];
                    $workScopePriceModel->price = $workScopePrice['price'];
                    $workScopePriceModel->save();
                }

            }
        }
    }

    protected function beforeSave(){
        if(parent::beforeSave()){
            preg_match('#(z[a-z]*)([0-9]+)#i', $this->symbol, $pockets);
            $this->symbol_sort_letter = $pockets[1];
            $this->symbol_sort_number = $pockets[2];
            $this->price = (!$this->isNewRecord && !$this->price && $this->getLowWorkScopePrice()) ? $this->getLowWorkScopePrice()->price : null;
            return true;
        }
        return false;
    }

    public function getNewsImages()
    {
        if($this->news){
            $images = array();
            foreach($this->news as $news){
                $images = array_merge($images, $news->images(array(
                        'condition'=>'images.additional_option = :additional_option',
                        'params'=>array(
                            ':additional_option'=>ExtraImage::ADDITIONAL_OPTION_ENABLE,
                        )
                    ))
                );
            }
            return $images;
        }
        return false;
    }

    public function getPortfolioImages(){
        if($this->portfolios){
            $images = array();
            foreach($this->portfolios as $portfolio){
                $images = array_merge($images, $portfolio->images(array(
                        'condition'=>'images.additional_option = :additional_option',
                        'params'=>array(
                            ':additional_option'=>ExtraImage::ADDITIONAL_OPTION_ENABLE,
                        )
                    ))
                );
            }
            return $images;
        }
        return false;
    }

    /*
     * @return WorkScopePrice
     * */
    public function getLowWorkScopePrice(){
        return WorkScopePrice::model()->findByAttributes(array('project_id'=>$this->id,'work_scope_id'=>1,'home_kit_id'=>2));
    }

    private $_similar;

    public function getSimilars()
    {
        if (!$this->_similar) {
            $query = '';
            if ($this->cars_in_garage > 0) {
                $query .= ' AND cars_in_garage>0';
            } else {
                $query .= ' AND (cars_in_garage<=0 OR cars_in_garage IS NULL)';
            }

            if ($this->roof_2_slope == self::STATUS_ENABLED) {
                $order = 'IF(roof_2_slope = 1, 0, 1)';
            }
            if ($this->roof_4_slope == self::STATUS_ENABLED) {
                $order = 'IF(roof_4_slope = 1, 0, 1)';
            }
            if ($this->roof_many_slope == self::STATUS_ENABLED) {
                $order = 'IF(roof_many_slope = 1, 0, 1)';
            }
            if ($this->roof_flat == self::STATUS_ENABLED) {
                $order = 'IF(roof_flat = 1, 0, 1)';
            }

            $squareDiff = 20;

            $condition = array(
                'condition' => 'id <> :id AND
                (symbol LIKE :symbol
                OR
                net_area BETWEEN :min_net_area AND :max_net_area
                AND attic_floor = :attic_floor
                AND floors = :floors ' . $query .')',
                'params' => array(
                    ':id' => $this->id,
                    ':symbol' => $this->symbol,
                    ':min_net_area' => $this->net_area - $squareDiff,
                    ':max_net_area' => $this->net_area + $squareDiff,
                    ':attic_floor' => $this->attic_floor,
                    ':floors' => $this->floors,
                ),
            );
            if (isset($order) && $order) {
                $condition['order'] = $order;
            }

            $this->_similar = Project::model()->findAll($condition);








//            $this->_similar = Project::model()->findAll(array(
//                'condition' => 'id <> :id
//            AND net_area BETWEEN :min_net_area AND :max_net_area
//            AND facade_width BETWEEN :min_facade_width AND :max_facade_width
//            AND floors = :floors',
//                'order' => $order,
//                'params' => array(
//                    ':id' => $this->id,
//                    ':min_net_area' => $this->net_area - $this->net_area * 0.1,
//                    ':max_net_area' => $this->net_area + $this->net_area * 0.1,
//                    ':min_facade_width' => $this->facade_width - 1.5,
//                    ':max_facade_width' => $this->facade_width + 1.5,
//                    ':floors' => $this->floors,
//                ),
//            ));
        }
        return $this->_similar;
    }

    /**
     * @var VideoWalk[]
     */
    private $_videoWalks = array();

    /**
     * @return VideoWalk[]|static[]
     */
    public function getVideoWalks()
    {
        if (!$this->_videoWalks) {
            $this->_videoWalks = VideoWalk::model()->find()->condition("FIND_IN_SET('{$this->symbol}{$this->variation_symbol}', REPLACE(t.projects, ' ', ''))")->findAll();
        }
        return $this->_videoWalks;
    }


    /**
     * @return Z500Image[]
     */
    function getZ500Images()
    {
        if (!$this->z500_url_key) {
            return [];
        }
        return Z500Image::all($this->z500_url_key);
    }
}
