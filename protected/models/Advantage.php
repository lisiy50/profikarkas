<?php

/**
 * This is the model class for table "{{advantage}}".
 *
 * The followings are the available columns in table '{{advantage}}':
 * @property integer $id
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_de
 * @property string $slogan
 * @property string $slogan_uk
 * @property string $slogan_en
 * @property string $slogan_fr
 * @property string $slogan_de
 * @property string $annotation
 * @property string $annotation_uk
 * @property string $annotation_en
 * @property string $annotation_fr
 * @property string $annotation_de
 * @property string $content
 * @property string $content_uk
 * @property string $content_en
 * @property string $content_fr
 * @property string $content_de
 * @property string $image
 * @property string $image_main
 * @property integer $status
 * @property integer $position
 * @property integer $visibility
 * @property string $publish_date
 * @property string $create_time
 * @property string $update_time
 */
class Advantage extends LibraryRecord
{

    const VISIBILITY_DIRECT_ONLY = 1;
    const VISIBILITY_INDEX = 2;

    public $visibility = self::VISIBILITY_INDEX;

//    private $_oldTags;
//
//    public $search_tag;

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),

            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/advantage', 250, 190, 'resize' => 'fill', 'required' => 'small.jpg', 'prefix' => 'small_'),
//                    'large' => array('storage/advantage', 270, 315, 'resize'=>'fill', 'prefix' => 'large_'),
                ),
            ),

            'Image2UploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image_main',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb/icon', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/advantage', 250, 190, 'resize' => 'fill', 'required' => 'small.jpg', 'prefix' => 'small_'),
//                    'large' => array('storage/advantage', 1920, 340, 'resize'=>'width', 'prefix' => 'large_' /*'watermark'=>array('watermark.jpg', -10, -10, 'opacity'=>50)*/),
                ),
            ),

            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'advantage/view',
            ),
        );
    }

    public function init()
    {
        $this->publish_date = date('Y-m-d');
    }
    /**
     * Returns the static model of the specified AR class.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{advantage}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, slogan, annotation', 'filter', 'filter' => 'strip_tags'),
            array('content, content_uk, content_en, content_fr, content_de', 'safe'),
            array('visibility, title, status, publish_date', 'required'),
            array('visibility, status, position', 'numerical', 'integerOnly' => true),
            array('title, title_uk, title_en, title_fr, title_de, slogan, slogan_uk, slogan_en, slogan_fr, slogan_de, annotation, annotation_uk, annotation_en, annotation_fr, annotation_de', 'length', 'max' => 255),
            array('status', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('image, image_main', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('image, image_main', 'unsafe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, status, position, publish_date, create_time, update_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'title' => 'Название (Русский)',
            'title_uk' => 'Название (Украинский)',
            'title_en' => 'Название (Английский)',
            'title_fr' => 'Название (Французский)',
            'title_de' => 'Название (Немецкий)',
            'slogan' => 'Слоган (Русский)',
            'slogan_uk' => 'Слоган (Украинский)',
            'slogan_en' => 'Слоган (Английский)',
            'slogan_fr' => 'Слоган (Французский)',
            'slogan_de' => 'Слоган (Немецкий)',
            'annotation' => 'Аннотация (Русский)',
            'annotation_uk' => 'Аннотация (Украинский)',
            'annotation_en' => 'Аннотация (Английский)',
            'annotation_fr' => 'Аннотация (Французский)',
            'annotation_de' => 'Аннотация (Немецкий)',
            'content' => 'Содержание (Русский)<br><small><i>Изображения нужно расположить перед(!) текстом, который должен располагаться слева от этого изображения, и включить у изображения "обтекание текстом справа"</i></small>',
            'content_uk' => 'Содержание (Украинский)',
            'content_en' => 'Содержание (Английский)',
            'content_fr' => 'Содержание (Французский)',
            'content_de' => 'Содержание (Немецкий)',
            'image' => 'Изображение',
            'image_main' => 'Изображение в подробнее',
            'status' => 'Включен',
            'position' => 'Порядок отображения',
            'visibility' => 'Показывать',
            'publish_date' => 'Дата публикации',
            'create_time' => 'Дата добавления',
            'update_time' => 'Дата изменения',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

//        $scopes = array(
//            'order' => "$alias.publish_date DESC",
//        );
        $scopes = array(
            'order' => "$alias.position ASC, create_time DESC",
        );

        return $scopes;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('position', $this->position);

        if (is_array($this->publish_date) && isset($this->publish_date['from']) && isset($this->publish_date['till'])) {
            $criteria->compare('publish_date', '>=' . $this->publish_date['from']);
            $criteria->compare('publish_date', '<=' . $this->publish_date['till']);
        } else {
            $criteria->compare('publish_date', $this->publish_date, true);
        }

        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function last($limit = NULL)
    {
        $criteria['order'] = 'publish_date DESC';
        if ($limit)
            $criteria['limit'] = $limit;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getPosition()
    {
        return [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
        ];
    }

}
