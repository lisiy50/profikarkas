<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ProjectCostForm extends CFormModel
{
	public $link;
	public $email;
	public $project_name;
	public $type;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('email, link, project_name, type', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'link'=>'Сылка',
            'email'=>'E-mail',
            'project_name'=>'Проект',
            'type'=>'Тип',
		);
	}

	/**
	 * @return array
	 */
	public function getTypeList()
	{
		return CHtml::listData(HomeKit::model()->findAll(), 'name', 'name');
	}

	public function sendToUser()
	{
		$swift=Yii::app()->swiftMailer;
		$subject = 'Просчет стоимости домокомплекта ' . $this->project_name;
		$body = 'Здравствуйте!<br/>';
		$body .= 'Вы отправили нам запрос на просчет стоимости домокомплекта ' . $this->project_name . '. Мы сможем выполнить его в ближайшие дни, как только он будет готов, мы сразу же сообщим вам. <br/>';
		$body .= 'С уважением,<br/>';
		$body .= 'команда, Профикаркас<br/><br/>';
		$body .= '<a href="http://profikarkas.com.ua/"><img src="http://profikarkas.com.ua/themes/classic/img/logo.png"/></a><br/>';

		$message=$swift->newMessage($this->email, $subject, $body, array(
			'contentType'=>'text/html',
			'from' => array('info@profikarkas.com.ua'=>'Профикаркас'),
		));
		$swift->send($message);
	}

	public function sendToManager()
	{
		$swift=Yii::app()->swiftMailer;
		$subject = 'Просчет стоимости домокомплекта ' . $this->project_name;
		$body = 'Здравствуйте!<br/>';
		$body .= 'Перейдите, пожалуйста, по этой ссылке чтобы увидеть просчет стоимости домокомплекта '.$this->project_name.'. <br/>';
		$body .= CHtml::link($this->link, $this->link) . '<br/><br/>';
		$body .= 'Тип: ' . $this->type . '<br/><br/>';
		$body .= 'E-mail: ' . $this->email . $this->type . '<br/><br/>';
		$body .= '<a href="http://profikarkas.com.ua/"><img src="http://profikarkas.com.ua/themes/classic/img/logo.png"/></a><br/>';

		$message=$swift->newMessage('info@profikarkas.com.ua', $subject, $body, array(
			'contentType'=>'text/html',
		));
		return $swift->send($message);
	}
}