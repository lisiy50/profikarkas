<?php
class PhotoGalleryImage extends ExtraImage {

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'photo_gallery_'),
            'mini' => array('storage/photo_gallery', 102, 60, 'resize'=>'fill', 'prefix'=>'mini_'),
            'small' => array('storage/photo_gallery', 389, 204, 'prefix'=>'small_', 'resize'=>'fill'),
            'more' => array('storage/photo_gallery', 1000, 550, 'resize'=>'max', 'prefix'=>'more_'),
            'large' => array('storage/photo_gallery', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
            'original' => array('storage/photo_gallery', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->getOwner()->watermark == PhotoGallery::STATUS_ENABLED){
            $images['more'] = array('storage/photo_gallery', 1000, 550, 'resize'=>'max', 'prefix'=>'more_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['large'] = array('storage/photo_gallery', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> $images,
            ),
        );
    }

}