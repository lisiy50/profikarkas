<?php

/**
 * This is the model class for table "{{parsing_pages_html}}".
 *
 * The followings are the available columns in table '{{parsing_pages_html}}':
 * @property integer $id
 * @property integer $category_id
 * @property integer $page
 * @property string $html
 * @property integer $parsed
 */
class ParsingPagesHtml extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParsingPagesHtml the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{parsing_pages_html}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, page, html', 'required'),
			array('category_id, page, parsed', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, page, html, parsed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'page' => 'Page',
			'html' => 'Html',
			'parsed' => 'Parsed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('page',$this->page);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('parsed',$this->parsed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}