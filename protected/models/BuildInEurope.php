<?php

/**
 * This is the model class for table "{{build_in_europe}}".
 *
 * The followings are the available columns in table '{{build_in_europe}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 */
class BuildInEurope extends LibraryRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BuildInEurope the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{build_in_europe}}';
	}

    public function behaviors()
    {
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 70, 70, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/buildInEurope', 389, 204, 'prefix'=>'small_', 'resize'=>'fill'),
                    'medium' => array('storage/portfolio', 389, 389, 'prefix'=>'medium_',),
                    'large' => array('storage/portfolio', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
                ),
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'buildInEurope/view',
                'params' => array('id' => $this->id),
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, status', 'required'),
			array('status,h1_size,h2_size,h3_size,h4_size', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
            array('image', 'unsafe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, status, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заглавие',
			'description' => 'Описание',
			'status' => 'Запись активна',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'h1_size' => 'размер h1(28)',
			'h2_size' => 'размер h2(30)',
			'h3_size' => 'размер h3(24)',
			'h4_size' => 'размер h4(18)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->title);
        return parent::getUrl($params);
    }

    public function getHasVideoInDescription(){
        if(strstr($this->description, 'iframe'))
            return true;
        return false;
    }
}