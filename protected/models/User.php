<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $role
 * @property integer $group_id
 * @property string $salt
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $fio
 * @property string $comment
 * @property integer $status
 */
class User extends ActiveRecord
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    const ROLE_CLIENT = 'client';
    const ROLE_MANAGER = 'manager';
    const ROLE_CONTENT = 'content';
    const ROLE_ADMIN = 'admin';

    public $rPassword;
    public $status=self::STATUS_ENABLED;

    public $oldPassword;
    public $currentPasswordHash;

    /**
     * Returns the static model of the specified AR class.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // register, changePassword
            array('password, rPassword', 'required', 'on' => 'register, changePassword, restorePassword, ulogin'),
            array('password', 'length', 'min' => 6, 'on' => 'register, changePassword, restorePassword, ulogin'),
            array('password', 'length', 'max' => 128, 'on' => 'register, changePassword, restorePassword, ulogin'),
            array('rPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Не верное подтверждение пароля', 'on' => 'register, changePassword, restorePassword, ulogin'),
            array('password', 'filter', 'filter' => array($this, 'hashPassword'), 'on' => 'register, changePassword, restorePassword, ulogin'),
            // register
            array('username', 'required', 'on' => 'register'),
            array('username', 'length', 'min' => 3, 'on' => 'register'),
            array('username', 'length', 'max' => 128, 'on' => 'register'),
            array('username', 'email', 'on' => 'register'),
            array('username', 'unique', 'on' => 'register'),
            array('fio', 'required', 'on' => 'register'),
            // change password
            array('oldPassword', 'required', 'on' => 'changePassword'),
            array('oldPassword', 'filter', 'filter' => array($this, 'oldPasswordValidator'), 'on' => 'changePassword'),

            array('role, email, status', 'required'),
            array('fio, phone, comment, address', 'filter', 'filter' => 'strip_tags'),
            array('group_id, status', 'numerical', 'integerOnly' => true),
            array('status', 'in', 'range' => array(self::STATUS_ENABLED, self::STATUS_DISABLED)),
            array('group_id', 'exist', 'className' => 'Group', 'attributeName' => 'id', 'message' => 'Такой группы нету'),
            array('email', 'length', 'max' => 128),
            array('email', 'email'),
            array('email', 'unique'),
            array('role', 'in', 'range' => array('client', 'manager', 'content', 'admin')),
            array('fio, phone, address', 'length', 'max' => 255),
            array('comment', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, password, role, group_id, email, phone, address, comment, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orders' => array(self::HAS_MANY, 'Order', 'user_id'),
            'group' => array(self::BELONGS_TO, 'Group', 'group_id'),
            //'partner'=>array(self::HAS_ONE, 'Partner', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'rPassword' => 'Подтверждение пароля',
            'oldPassword' => 'Старый пароль',
            'role' => 'Права доступа',
            'fio' => 'Имя',
            'group_id' => 'Группа',
            'salt' => 'Salt',
            'email' => 'E-Mail',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'comment' => 'Коментарий',
            'status' => 'Активен',
            'authoriz_time' => 'Посещение',
            'create_time' => 'Зарегестрирован',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password) === $this->password;
    }

    public function oldPasswordValidator($password)
    {
        if ($this->hashPassword($password) !== $this->currentPasswordHash) {
            $this->addError('oldPassword', 'Старый пароль введен не верно');
        }
    }

    /**
     * Generates the password hash.
     * @param string password
     * @param string salt
     * @return string hash
     */
    public function hashPassword($password)
    {
        return md5($this->salt . $password);
    }

    public function getStatusIcon()
    {
        switch ($this->status) {
            case self::STATUS_ENABLED:
                return '<i class="icon-ok-sign"></i>';
            case self::STATUS_DISABLED:
                return '<i class="icon-ban-circle"></i>';
        }
    }

    public function getGroupList()
    {
        return CHtml::listData(Group::model()->findAll(), 'id', 'name');
    }

    /**
     * @return string
     * this key use for restore password
     */
    public function getKey(){
        return md5($this->password);
    }

    public static function getUserByKey($key){
        return self::model()->find('MD5(CONCAT(password)) = :key', array(':key'=>$key));
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub

        $this->currentPasswordHash = $this->password;
    }
}