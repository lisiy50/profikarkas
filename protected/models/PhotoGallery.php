<?php

/**
 * This is the model class for table "{{photo_gallery}}".
 *
 * The followings are the available columns in table '{{photo_gallery}}':
 * @property integer $id
 * @property string $project_name
 * @property string $construction_place
 * @property integer $gross_area
 * @property string $workload
 * @property string $turnaround_time
 * @property string $period_of_works
 * @property string $description
 *
 * @property string $video_code1
 * @property string $video_code2
 * @property string $video_code3
 * @property string $video_code4
 * @property string $video_code5
 * @property string $video_code6
 * @property integer $service1
 * @property integer $service2
 * @property integer $service3
 * @property integer $service4
 * @property integer $service5
 * @property integer $service6
 *
 * @property integer $status
 * @property integer $watermark
 * @property integer $show_albom
 * @property string $create_time
 * @property string $update_time
 */
class PhotoGallery extends LibraryRecord
{

    private $_videos = array();
    private $_nextProjectModel = false;
    private $_prevProjectModel = false;

    public $watermark = self::STATUS_ENABLED;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{photo_gallery}}';
	}

    public function behaviors(){
  	    return array(
  		    'CTimestampBehavior' => array(
  		  	    'class' => 'zii.behaviors.CTimestampBehavior',
  		  	    'createAttribute' => 'create_time',
  			    'updateAttribute' => 'update_time',
  		    ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'photoGallery/view',
            )
  	    );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('project_name, construction_place, turnaround_time, period_of_works, workload, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'strip_tags'),
            array('project_name, construction_place, turnaround_time, period_of_works, workload, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'trim'),
            array('gross_area', 'numerical', 'integerOnly'=>true),
            array('description','safe'),
			array('project_name', 'required'),
			array('project_name, construction_place, turnaround_time, period_of_works, workload', 'length', 'max'=>255),
			array('status, watermark, service1, service2, service3, service4, service5, service6, show_albom', 'numerical', 'integerOnly'=>true),
            array('status, watermark, show_albom', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_name, construction_place, status, watermark, show_albom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'firstImage'=>array(self::HAS_ONE, 'PhotoGalleryImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'images'=>array(self::HAS_MANY, 'PhotoGalleryImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'PhotoGalleryImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
        );
	}

    public function defaultScope(){
        return array(
            'order' => 'create_time DESC',
        );
    }

    public function scopes(){
        return array(
            'directOrder' => array(
                'order' => 'create_time DESC',
            ),
            'reverseOrder' => array(
                'order' => 'create_time',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_name' => 'Название проекта',
			'construction_place' => 'Место строительства',
			'gross_area' => 'Общая площадь (m2)',
			'workload' => 'Заказанный объем работ',
			'turnaround_time' => 'Сроки выполнения работ',
			'period_of_works' => 'Период выполнения работ',
			'description' => 'Описание',

//            'video_code1' => '',
//            'service1' => '',
//            'video_code2' => '',
//            'service2' => '',
//            'video_code3' => '',
//            'service3' => '',
//            'video_code4' => '',
//            'service4' => '',
//            'video_code5' => '',
//            'service5' => '',
//            'video_code6' => '',
//            'service6' => '',

			'status' => 'Включена',
			'watermark' => 'Водяной знак',
			'show_albom' => 'Показывать альбом',
			'create_time' => 'Добавлена',
			'update_time' => 'Изменена',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_name',$this->project_name,true);
		$criteria->compare('construction_place',$this->construction_place,true);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('show_albom',$this->show_albom);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    public function getNextProject(){
        if($this->_nextProjectModel === false)
            $this->_nextProjectModel = self::model()->find('create_time < ?', array($this->create_time));
        return $this->_nextProjectModel;
    }

    public function getPrevProject(){
        if($this->_prevProjectModel === false)
            $this->_prevProjectModel = self::model()->resetScope()->reverseOrder()->find('create_time > ?', array($this->create_time));
        return $this->_prevProjectModel;
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->project_name);
        return strtolower(parent::getUrl($params));
    }

    public function getVideos(){
        if(!$this->_videos){
            $i = 1;
            $videoCodeProperty = 'video_code'.$i;
            $videoSevice = 'service'.$i;
            while(isset($this->$videoCodeProperty) && !empty($this->$videoCodeProperty)){
                $image = $this->getVideoMiniImage($this->$videoCodeProperty, $this->$videoSevice);
                $this->_videos[] = array(
                    'service' => $this->$videoSevice,
                    'code' => $this->$videoCodeProperty,
//                'htmlCode' => $this->getVideoInsertHtml($this->$videoCodeProperty, $this->$videoSevice),
                    'miniImageHtml' => isset($image['mini'])?$image['mini']:false,
//                    'largeImageHtml' => isset($image['large'])?$image['large']:false,
                    'videoLink' => $this->getVideoLink($this->$videoCodeProperty, $this->$videoSevice),
                );
                $i++;
                $videoCodeProperty = 'video_code'.$i;
                $videoSevice = 'service'.$i;
            }
        }

        return $this->_videos;
    }

    public function getCountVideos(){
        return count($this->getVideos());
    }

    public function getVideoInsertHtml($code, $service = 1){
        switch ($service){
            case 1:
                return '<iframe width="420" height="315" src="//www.youtube.com/embed/'.$code.'" frameborder="0" allowfullscreen></iframe>';
            case 2:
                return '<iframe src="//player.vimeo.com/video/'.$code.'" width="500" height="282" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            default:
                return false;
        }
    }

    public function getVideoMiniImage($code, $service = 1){
        switch ($service){
            case 1:
                return array(
                    'mini' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">',
                    'large' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/0.jpg" src="//i1.ytimg.com/vi/'.$code.'/0.jpg" alt="" width="480" data-group-key="thumb-group-0">',
                );
                return '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">';
            case 2:
                $var = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$code.php"));
                if(isset($var[0])){
                    return array(
                        'mini' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_medium']), false, false),
                        'large' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_large'], 'style'=>'width:480px;'), false, false),
                    );
                }
            default:
                return false;
        }
    }

    public function getVideoLink($code, $service = 1){
        switch ($service){
            case 1:
                return 'https://youtube.googleapis.com/v/'.$code.'%26hl=pl%26fs=1%26rel=0%26loop=1';
            case 2:
                return 'http://player.vimeo.com/video/'.$code.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ff0179';
            default:
                return false;
        }
    }

    public function getProjectUrl(){
        $projectModel = Project::model()->findByAttributes(array('symbol'=>$this->project_name));
        if($projectModel)
            return $projectModel->getUrl();
        return false;
    }

}