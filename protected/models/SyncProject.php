<?php

class SyncProject extends Project
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SyncProject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getOverlapLink(){
        return array(
            'p_strop_gestozebrowy'=>1,
            'p_strop_zelbetowy'=>2,
            'p_strop_drewniany'=>3,
            'p_strop_wiazary'=>4,
        );
    }

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getApplyingLink(){
        return array(
            'p_drewniany'=>1,
            'p_rezydencja'=>2,
            'p_nowoczesny'=>3,
            'p_dlabiznesu'=>4,
            'p_dwa_wejscia'=>5,
            'p_dwurodzinny'=>6,
            'p_wejscie_przez_garaz'=>7,
            'p_letniskowy'=>8,
        );
    }

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getFacadeLink(){
        return array(
            'p_wykusz'=>1,
            'p_taras_boczny'=>2,
            'p_wiata'=>3,
            'p_duze_przeszklenia'=>4,
            'p_ogrod_zimowy_opcja'=>5,
            'p_przeniesienie_tarasu_opcja'=>6,
            'p_zwarta_bryla'=>7,
        );
    }

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getWallMaterialLink(){
        return array(
            'p_sciany_mur_gazobeton'=>1,
            'p_sciany_mur_ceramika'=>2,
            'p_sciany_mur_silikaty'=>3,
            'p_sciany_drew_szkielet'=>4,
            'p_sciany_drew_bale'=>5,
        );
    }

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getRoofMaterialLink(){
        return array(
            'p_dach_dachowka'=>1,
            'p_dach_blachodachowka'=>2,
            'p_dach_papa'=>3,
            'p_dach_folia'=>4,
            'p_dach_blacha'=>5,
            'p_dach_gont'=>6,
            'p_dach_plytki'=>7,
            'p_zielony_dach'=>8,
        );
    }

    /*
     * @return array
     * key - z500 field name
     * val - profikarkas val
     * */
    public function getHeatingLink(){
        return array(
            'p_ekogroszek'=>1,
            'p_paliwo_stale'=>2,
            'p_gaz'=>3,
            'p_olej'=>4,
            'p_pompa_ciepla'=>5,
            'p_elektryczne'=>6,
            'p_solary'=>7,
            'p_kominek_pelety'=>8,
        );
    }

    /*
     * key - z500 field name
     * val - profikarkas field name
     * */
    public function getFieldLink(){
        return array(
            'p_id_primary'=>'z500_id_primary',
            'p_symbol'=>'symbol',
            'p_kod'=>'variation_symbol',
            'p_tytul'=>'title',
            'p_krotki_opis'=>'description',
            'p_wysokosc'=>'house_height',
            'p_wysokosc_salonu'=>'living_room_height',
            'p_min_pow_zabudowy'=>'minimum_building_area',
            'p_dach'=>'roof_angle',
            'p_dach_kat2'=>'roof_angle1',
            'p_dach_pow'=>'roof_area',
            'p_wys_scianki_kol'=>'height_walls_of_the_col',
            'p_okap_1'=>'roof_canopy1',
            'p_okap_2'=>'roof_canopy2',
            'p_okap_3'=>'roof_canopy3',
            'p_okap_4'=>'roof_canopy4',
            'p_powierzchnia_netto'=>'net_area',
            'p_pow_uzytkowa'=>'usable_area',
            'p_pow_uzytkowa_rozp'=>'usable_area_by_order',
            'p_pow_piwnica'=>'basement_area',
            'p_pow_garaz'=>'garage_area',
            'p_garaz_stanowiska'=>'cars_in_garage',
            'p_pow_strych'=>'loft_area',
            'p_dzialka_szerokosc'=>'plot_width',
            'p_dzialka_dlugosc'=>'plot_length',
            'p_elewacja_szerokosc'=>'facade_width',
            'p_elewacja_dlugosc'=>'facade_height',

            'p_dwu_spadowy' => 'roof_2_slope',
            'p_kopertowy' => 'roof_4_slope',
            'p_wielospadowy' => 'roof_many_slope',
            'p_plaski' => 'roof_flat',

            'p_kubatura' => 'cubage',
        );
    }

    /*
     * @param array of fields from z500
     * */
    public function setAttributesFields(array $project){
        foreach($this->getFieldLink() as $z500Field=>$profikarkasField){
            if(!is_array($project[$z500Field]))
                $this->$profikarkasField = $project[$z500Field];
        }

        $z500_video_ids = array();
        if($project['p_youtube_id']) $z500_video_ids[] = $project['p_youtube_id'];
        if($project['p_vimeo_id_0']) $z500_video_ids[] = $project['p_vimeo_id_0'];
        if($project['p_vimeo_id_1']) $z500_video_ids[] = $project['p_vimeo_id_1'];
        if($project['p_vimeo_id_2']) $z500_video_ids[] = $project['p_vimeo_id_2'];
        if($project['p_vimeo_id_3']) $z500_video_ids[] = $project['p_vimeo_id_3'];
        if($project['p_vimeo_id_4']) $z500_video_ids[] = $project['p_vimeo_id_4'];
        $this->video_ids = implode(';', $z500_video_ids);

        $z500_overlap = array();
        foreach($this->overlapLink as $overlapLinkKey=>$overlapLinkVal){
            if($project[$overlapLinkKey]) $z500_overlap[] = $overlapLinkVal;
        }
        if($z500_overlap !== array())
            $this->overlap = $z500_overlap;

        $z500_applying = array();
        foreach($this->applyingLink as $applyingLinkKey=>$applyingLinkVal){
            if($project[$applyingLinkKey]) $z500_applying[] = $applyingLinkVal;
        }
        if($z500_applying !== array())
            $this->applying = $z500_applying;

        $z500_facade = array();
        foreach($this->facadeLink as $facadeLinkKey=>$facadeLinkVal){
            if($project[$facadeLinkKey]) $z500_facade[] = $facadeLinkVal;
        }
        if($z500_facade !== array())
            $this->facade = $z500_facade;

        $z500_wallMaterial = array();
        foreach($this->wallMaterialLink as $wallMaterialLinkKey=>$wallMaterialLinkVal){
            if($project[$wallMaterialLinkKey]) $z500_wallMaterial[] = $wallMaterialLinkVal;
        }
        if($z500_wallMaterial !== array())
            $this->wall_material = $z500_wallMaterial;

        $z500_roofMaterial = array();
        foreach($this->roofMaterialLink as $roofMaterialLinkKey=>$roofMaterialLinkVal){
            if($project[$roofMaterialLinkKey]) $z500_roofMaterial[] = $roofMaterialLinkVal;
        }
        if($z500_roofMaterial !== array())
            $this->roof_material = $z500_roofMaterial;

        $z500_heating = array();
        foreach($this->heatingLink as $heatingLinkKey=>$heatingLinkVal){
            if($project[$heatingLinkKey]) $z500_heating[] = $heatingLinkVal;
        }
        if($z500_heating !== array())
            $this->heating = $z500_heating;
    }
}