<?php

/**
 * This is the model class for table "{{cross_link}}".
 *
 * The followings are the available columns in table '{{cross_link}}':
 * @property integer $id
 * @property string $name
 * @property string $uri
 * @property string $image
 * @property boolean $status
 * @property string $create_time
 * @property string $update_time
 */
class CrossLink extends LibraryRecord
{

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'name',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'portfolio' => array('storage/cross_link', 389, 240, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'small_'),
                    'project' => array('storage/cross_link', 490, 291, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'small_'),
                ),
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @return CrossLink the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{cross_link}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, name_uk', 'filter', 'filter' => 'strip_tags'),
            array('status', 'numerical', 'integerOnly' => true),
            array('uri, name, name_uk', 'length', 'max' => 255),
            array('image', 'unsafe'),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('status', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, uri, status, publish_date, create_time, update_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название, разделитель: "|" (Русский)',
            'name_uk' => 'Название, разделитель: "|" (Украинский)',
            'uri' => 'URI',
            'url' => 'URL',
            'image' => 'Изображение',
            'status' => 'Опубликована',
            'create_time' => 'Добавлена',
            'update_time' => 'Изменена',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('uri', $this->uri, true);
        $criteria->compare('status', $this->status);

        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function getUrl($params = array())
    {
        if (strpos($this->uri, 'www.') === 0 || strpos($this->uri, 'http://') === 0) {
            return $this->uri;
        }

        $urlManager = (IS_BACKEND) ? Yii::app()->frontendUrlManager : Yii::app()->urlManager;
        if (empty($this->uri)) {
            return $urlManager->baseUrl;
        }

        $pars=parse_url($this->uri);

        if(empty($pars['path']) && !empty($pars['fragment'])) {
            return '#'.$pars['fragment'];
        }

        $route=empty($pars['path'])?'site/index':$pars['path'];
        $params=array();
        if(!empty($pars['query'])) {
            foreach(explode('&', $pars['query']) as $str) {
                list($key, $val)=explode('=', $str);
                $params[$key]=$val;
            }
        }
        if(!empty($pars['fragment'])) {
            $params['#']=$pars['fragment'];
        }

        return $urlManager->createUrl($route, $params);
    }

    public static function getRandItem($criteria = array('order'=>'id', 'limit'=>2)){
        return self::model()->findAll($criteria);
    }

    public static function getNextItems($lastId, $limit = 2){
        $items = self::model()->limit($limit)->order('id')->findAll('id>:id', array(':id'=>$lastId));
        if(count($items) < $limit){
            $newItems = self::model()->limit(($limit - count($items)))->findAll(array('order'=>'id'));
            $items = CMap::mergeArray($items, $newItems);
        }
        return $items;
    }

}
