<?php

/**
 * This is the model class for table "{{video_walk}}".
 *
 * The followings are the available columns in table '{{video_walk}}':
 * @property integer $id
 * @property string $name
 * @property string $name_uk
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_de
 * @property string $projects
 * @property string $image
 * @property string $path
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Project[] $projectModels
 */
class VideoWalk extends LibraryRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VideoWalk the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{video_walk}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('path', 'required'),
            array('status, create_time, update_time', 'numerical', 'integerOnly'=>true),
            array('name, name_uk, name_en, name_fr, name_de', 'length', 'max'=>255),
            array('projects, path', 'length', 'max'=>128),
            array('image', 'unsafe'),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, projects, image, path, status, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Заглавие (Русский)',
            'name_uk' => 'Заглавие (Украинский)',
            'name_en' => 'Заглавие (Английский)',
            'name_fr' => 'Заглавие (Французский)',
            'name_de' => 'Заглавие (Немецкий)',
            'projects' => 'Проекты через запятую',
            'image' => 'Изображение',
            'path' => 'Путь к видеопрогулке',
            'status' => 'Активна',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('projects',$this->projects,true);
        $criteria->compare('image',$this->image,true);
        $criteria->compare('path',$this->path,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_time',$this->create_time);
        $criteria->compare('update_time',$this->update_time);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'name',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'mini' => array('storage/video_walk', 102, 60, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'mini_'),
                    'default' => array('storage/video_walk', 389, 204, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'default_'),
                ),
            ),
        );
    }

    public function getUrl($params = array())
    {
        return app()->baseUrl . '/storage/walks/' . $this->path;
    }

    /**
     * @var Project[]
     */
    private $_projectModels = array();

    /**
     * @return Project[]
     */
    public function getProjectModels()
    {
        if (!$this->_projectModels && $this->projects) {
            $projects = explode(',', $this->projects);
            foreach ($projects as $project) {
                $temp = Project::model()->find()->where('CONCAT(symbol, variation_symbol)', $project)->find();
                if ($temp) {
                    $this->_projectModels[] = $temp;
                    unset($temp);
                }
            }
        }
        return $this->_projectModels;
    }

    function afterSave()
    {
        parent::afterSave();
        foreach ($this->projectModels as $projectModel) {
            $projectModel->has_wideo_walks = 1;
            $projectModel->update(array('has_wideo_walks'));
        }

    }
}
