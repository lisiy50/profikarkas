<?php

/**
 * This is the model class for table "{{faq_item}}".
 *
 * The followings are the available columns in table '{{faq_item}}':
 * @property integer $id
 * @property string $name_ru
 * @property string $name_uk
 * @property string $content_ru
 * @property string $content_uk
 * @property integer $parent_id
 * @property integer $level
 * @property integer $position
 * @property integer $status
 *
 */
class FaqItem extends ActiveRecord
{

    const ARTICLE = 1;

    public $multyLangFields = array(
        'name',
    );

    /**
     * Returns the static model of the specified AR class.
     * @return FaqItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{faq_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name_ru, name_uk', 'filter', 'filter' => 'strip_tags'),
            array('name_ru, name_uk', 'required'),
            array('parent_id, level, position, status', 'numerical', 'integerOnly' => true),
            array('name_ru, name_uk, content_ru, content_uk', 'safe'),
        );
    }

    public function scopes()
    {
        return array(
            'rooted' => array(
                'condition' => 'parent_id IS NULL'
            ),
            'article' => array(
                'condition' => 'parent_id='.self::ARTICLE
            ),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => 'position',
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'countChildren' => array(self::STAT, 'FaqItem', 'parent_id'),
            'children' => array(self::HAS_MANY, 'FaqItem', 'parent_id'),
            'parent' => array(self::BELONGS_TO, 'FaqItem', 'parent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name_ru' => 'Название (Русский)',
            'name_uk' => 'Название (Украинский)',
            'content_ru' => 'Контент (Русский)',
            'content_uk' => 'Контент (Украинский)',
            'parent_id' => 'Размещение',
            'level' => 'Вложенность',
            'position' => 'Порядок',
            'status' => 'Показывать на сайте',
        );
    }

    public function getText()
    {
        return CHtml::link(CHtml::encode($this->name_ru), array('update', 'id' => $this->id));
    }

    protected function beforeDelete()
    {
        if (parent::beforeDelete()) {
            self::model()->deleteAll('parent_id=?', array($this->id));
            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->parent === null || $this->parent->isNewRecord) {
                $this->level = 0;
            } else {
                $this->level = $this->parent->level + 1;
            }
            if ($this->isNewRecord) {
                $criteria = new CDbCriteria;
                $criteria->select = 'position';
                $criteria->order = 'position DESC';
                if ($this->parent_id) {
                    $criteria->condition = 'parent_id=:parent_id';
                    $criteria->params = array(':parent_id' => $this->parent_id);
                } else {
                    $criteria->condition = 'parent_id IS NULL';
                }
                $last = self::model()->find($criteria);
                $this->position = $last ? $last->position + 1 : 0;
            }
            return true;
        } else
            return false;
    }

    private $_neighbors;

    public function getNeighbors()
    {
        if ($this->_neighbors == null) {
            if ($this->isRooted) {
                $this->_neighbors = FaqItem::model()->rooted()->findAll();
            } else {
                $this->_neighbors = FaqItem::model()->findAllByAttributes(array(
                    'parent_id' => $this->parent_id,
                ));
            }
        }
        return $this->_neighbors;
    }

    public function getIsRooted()
    {
        return empty($this->parent_id);
    }

    public function getRooted()
    {
        $parent = $this;
        while ($parent->parent) {
            $parent = $parent->parent;
        }
        return $parent;
    }
}
