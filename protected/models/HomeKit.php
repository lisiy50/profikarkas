<?php

/**
 * This is the model class for table "{{home_kit}}".
 *
 * The followings are the available columns in table '{{home_kit}}':
 * @property integer $id
 * @property string $name
 * @property integer $price
 * @property string $description
 */
class HomeKit extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return HomeKit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{home_kit}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, price', 'required'),
			array('price', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
//            array('description', 'filter', 'filter'=>array($obj = new EHtmlPurifier(), 'purify')),
            array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'packagingHomeKits'=>array(self::MANY_MANY, 'PackagingHomeKit', '{{home_kit_relation}}(home_kit_id, packaging_home_kit_id)'),
            'homeKitRelations'=>array(self::HAS_MANY, 'HomeKitRelation', 'home_kit_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'price' => 'Базовая цена',
			'description' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}