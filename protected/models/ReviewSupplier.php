<?php

/**
 * This is the model class for table "{{review_supplier}}".
 *
 * The followings are the available columns in table '{{review_supplier}}':
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $name_uk
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_de
 * @property string $content
 * @property string $content_uk
 * @property string $content_en
 * @property string $content_fr
 * @property string $content_de
 * @property string $product
 * @property string $product_uk
 * @property string $product_en
 * @property string $product_fr
 * @property string $product_de
 * @property string $image
 * @property integer $status
 * @property integer $visibility
 * @property string $publish_date
 * @property string $create_time
 * @property string $update_time
 */
class ReviewSupplier extends LibraryRecord
{

    const VISIBILITY_DIRECT_ONLY = 1;
    const VISIBILITY_INDEX = 2;

    public $visibility = self::VISIBILITY_INDEX;

//    private $_oldTags;
//
//    public $search_tag;

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),

            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/advantage', 120, 120, 'required' => 'small.jpg', 'prefix' => 'small_'),
//                    'large' => array('storage/advantage', 270, 315, 'resize'=>'fill', 'prefix' => 'large_'),
                ),
            ),

            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'advantage/view',
            ),
        );
    }

    public function init()
    {
        $this->publish_date = date('Y-m-d');
    }

    /**
     * Returns the static model of the specified AR class.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{review_supplier}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, name, product, content, content_uk, content_en, content_fr, content_de', 'filter', 'filter' => 'strip_tags'),
            array('visibility, title, status, publish_date', 'required'),
            array('visibility, status', 'numerical', 'integerOnly' => true),
            array('title, name, name_uk, name_en, name_fr, name_de, product, product_uk, product_en, product_fr, product_de', 'length', 'max' => 255),
            array('status', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('image', 'unsafe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, status, publish_date, create_time, update_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'title' => 'Поставщик',
            'name' => 'Имя (Русский)',
            'name_uk' => 'Имя (Украинский)',
            'name_en' => 'Имя (Английский)',
            'name_fr' => 'Имя (Французский)',
            'name_de' => 'Имя (Немецкий)',
            'content' => 'Отзыв (Русский)',
            'content_uk' => 'Отзыв (Украинский)',
            'content_en' => 'Отзыв (Английский)',
            'content_fr' => 'Отзыв (Французский)',
            'content_de' => 'Отзыв (Немецкий)',
            'product' => 'Продукция (Русский)',
            'product_uk' => 'Продукция (Украинский)',
            'product_en' => 'Продукция (Английский)',
            'product_fr' => 'Продукция (Французский)',
            'product_de' => 'Продукция (Немецкий)',
            'image' => 'Изображение',
            'status' => 'Включен',
            'visibility' => 'Показывать',
            'publish_date' => 'Дата публикации',
            'create_time' => 'Дата добавления',
            'update_time' => 'Дата изменения',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        $scopes = array(
            'order' => "$alias.publish_date DESC",
        );

        return $scopes;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);

        if (is_array($this->publish_date) && isset($this->publish_date['from']) && isset($this->publish_date['till'])) {
            $criteria->compare('publish_date', '>=' . $this->publish_date['from']);
            $criteria->compare('publish_date', '<=' . $this->publish_date['till']);
        } else {
            $criteria->compare('publish_date', $this->publish_date, true);
        }

        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function last($limit = NULL)
    {
        $criteria['order'] = 'publish_date DESC';
        if ($limit)
            $criteria['limit'] = $limit;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

}
