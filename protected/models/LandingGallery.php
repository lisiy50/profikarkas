<?php

/**
 * This is the model class for table "{{landing_gallery}}".
 *
 * The followings are the available columns in table '{{landing_gallery}}':
 * @property integer $id
 * @property integer $landing_id
 * @property string $name
 * @property string $name_manager
 * @property integer $image_1_width_lg
 * @property integer $image_1_height_lg
 * @property integer $image_1_width_md
 * @property integer $image_1_height_md
 * @property integer $image_1_width_sm
 * @property integer $image_1_height_sm
 * @property integer $image_2_width_lg
 * @property integer $image_2_height_lg
 * @property integer $image_2_width_md
 * @property integer $image_2_height_md
 * @property integer $image_2_width_sm
 * @property integer $image_2_height_sm
 */
class LandingGallery extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{landing_gallery}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('landing_id, image_1_width_lg, image_1_height_lg, image_1_width_md, image_1_height_md, image_1_width_sm, image_1_height_sm, image_2_width_lg, image_2_height_lg, image_2_width_md, image_2_height_md, image_2_width_sm, image_2_height_sm', 'numerical', 'integerOnly'=>true),
			array('name, name_manager', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, landing_id, name, name_manager, image_1_width_lg, image_1_height_lg, image_1_width_md, image_1_height_md, image_1_width_sm, image_1_height_sm, image_2_width_lg, image_2_height_lg, image_2_width_md, image_2_height_md, image_2_width_sm, image_2_height_sm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'landing_id' => 'ID лендинга',
			'name' => 'Название',
			'name_manager' => 'Примечание',
			'image_1_width_lg' => 'Размер Max (ширина',
			'image_1_height_lg' => 'высота)',
			'image_1_width_md' => 'Размер Mid (ширина',
			'image_1_height_md' => 'высота)',
			'image_1_width_sm' => 'Размер Min (ширина',
			'image_1_height_sm' => 'высота)',
			'image_2_width_lg' => 'Размер Max (ширина',
			'image_2_height_lg' => 'высота)',
			'image_2_width_md' => 'Размер Mid (ширина',
			'image_2_height_md' => 'высота)',
			'image_2_width_sm' => 'Размер Min (ширина',
			'image_2_height_sm' => 'высота)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('landing_id',$this->landing_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_manager',$this->name_manager,true);
		$criteria->compare('image_1_width_lg',$this->image_1_width_lg);
		$criteria->compare('image_1_height_lg',$this->image_1_height_lg);
		$criteria->compare('image_1_width_md',$this->image_1_width_md);
		$criteria->compare('image_1_height_md',$this->image_1_height_md);
		$criteria->compare('image_1_width_sm',$this->image_1_width_sm);
		$criteria->compare('image_1_height_sm',$this->image_1_height_sm);
		$criteria->compare('image_2_width_lg',$this->image_2_width_lg);
		$criteria->compare('image_2_height_lg',$this->image_2_height_lg);
		$criteria->compare('image_2_width_md',$this->image_2_width_md);
		$criteria->compare('image_2_height_md',$this->image_2_height_md);
		$criteria->compare('image_2_width_sm',$this->image_2_width_sm);
		$criteria->compare('image_2_height_sm',$this->image_2_height_sm);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LandingGallery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
