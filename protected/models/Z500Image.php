<?php

/**
 * Class Z500Image
 */

class Z500Image {
    public $type;
    public $remotePath;

    /**
     * @param $key
     * @return Z500Image[]
     */
    static function all($key)
    {
        $url = "http://z500.com.ua/project/export-images/{$key}.html";

        $result = array();
        if (@$json = file_get_contents($url)) {
            foreach (json_decode($json) as $type => $images) {
                foreach ($images as $image) {
                    $obj = new Z500Image();
                    switch ($type) {
                        case 'visualizations':
                            $obj->type = ProjectImage::TYPE_VISUALIZATION;
                            break;
                        case 'interiors':
                            $obj->type = ProjectImage::TYPE_INTERIOR;
                            break;
                        case 'facades':
                            $obj->type = ProjectImage::TYPE_FACADE;
                            break;
                        case 'layouts':
                            $obj->type = ProjectImage::TYPE_LAYOUT;
                            break;
                        case 'areas':
                            $obj->type = ProjectImage::TYPE_AREA;
                            break;
                    }
                    $obj->remotePath = $image->path;
                    $result[] = $obj;
                }
            }
        }

        return $result;
    }

    function getUrl()
    {
        return "http://z500.com.ua/FilesZ500/res{$this->remotePath}";
    }

    function getFilename()
    {
        $file = new SplFileInfo($this->getUrl());
        return $file->getFilename();
    }
}
