<?php

class Config extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @return Config the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{config}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $html_purifier = array('filter' => array(new EHtmlPurifier, 'purify'));
        $product_order = array('range' => array('price', 'price DESC', 'name', 'hit DESC', 'browse DESC', 'priority DESC'));

        return array(
            'shop_name' => array(
                'filter' => array('filter' => 'strip_tags'),
                'required',
                'length' => array('max' => 64)
            ),
            'company' => array(
                'filter' => array('filter' => 'strip_tags'),
                'required',
                'length' => array('max' => 255)
            ),
            'admin_email' => array(
                'required',
                'email',
                'length' => array('max' => 255)
            ),
            'contact_email' => array(
                'required',
                'email',
                'length' => array('max' => 255)
            ),
            'contact_phone' => array(
                'length' => array('max' => 255)
            ),
            'contact_phone_int' => array(
                'length' => array('max' => 32)
            ),
            'contact_address' => array(
                'length' => array('max' => 500)
            ),
            'contact_address_uk' => array(
                'length' => array('max' => 500)
            ),
            'contact_address_en' => array(
                'length' => array('max' => 500)
            ),
            'contact_address_de' => array(
                'length' => array('max' => 500)
            ),
            'contact_address_fr' => array(
                'length' => array('max' => 500)
            ),
            'main_text' => array('safe'),
            'main_text_uk' => array('safe'),
            'main_text_en' => array('safe'),
            'main_text_de' => array('safe'),
            'main_text_fr' => array('safe'),
            'product_text' => array('safe'),
            'product_text_uk' => array('safe'),
            'product_text_en' => array('safe'),
            'product_text_de' => array('safe'),
            'product_text_fr' => array('safe'),
            'portfolio_text' => array('safe'),
            'portfolio_text_uk' => array('safe'),
            'portfolio_text_en' => array('safe'),
            'portfolio_text_de' => array('safe'),
            'portfolio_text_fr' => array('safe'),
            'project_price_tab_text' => array('safe'),
            'project_price_tab_text_uk' => array('safe'),
            'project_price_tab_text_en' => array('safe'),
            'project_price_tab_text_de' => array('safe'),
            'project_price_tab_text_fr' => array('safe'),
            'product_categiry_dachi' => array('safe'),
            'product_categiry_dachi_uk' => array('safe'),
            'product_categiry_dachi_en' => array('safe'),
            'product_categiry_dachi_de' => array('safe'),
            'product_categiry_dachi_fr' => array('safe'),
            'product_categiry_zagorodnie' => array('safe'),
            'product_categiry_zagorodnie_uk' => array('safe'),
            'product_categiry_zagorodnie_en' => array('safe'),
            'product_categiry_zagorodnie_de' => array('safe'),
            'product_categiry_zagorodnie_fr' => array('safe'),
            'contact_text' => array('safe'),
            'contact_text_uk' => array('safe'),
            'contact_text_en' => array('safe'),
            'contact_text_de' => array('safe'),
            'contact_text_fr' => array('safe'),
            'contact_map' => array('safe'),
            'contact_use_captcha' => array('required', 'boolean'),
            'counters' => array('safe'),
            'price_accuracy' => array(
                'required',
                'numerical' => array('integerOnly' => true),
            ),
            'currency_default' => array(
                'required',
                'numerical' => array('integerOnly' => true),
                'exist' => array('className' => 'Currency', 'attributeName' => 'id', 'message' => 'Такой валюты не существует'),
            ),
            'currency_basic' => array(
                'required',
                'numerical' => array('integerOnly' => true),
                'exist' => array('className' => 'Currency', 'attributeName' => 'id', 'message' => 'Такой валюты не существует'),
            ),
            'product_catalog_limit' => array(
                'required',
                'numerical' => array('integerOnly' => true),
            ),
            'product_catalog_order' => array(
                'required',
                'in' => $product_order
            ),
            'product_search_limit' => array(
                'required',
                'numerical' => array('integerOnly' => true),
            ),
            'product_search_order' => array(
                'required',
                'in' => $product_order
            ),
            'similar_price_accuracy' => array(
                'required',
                'numerical' => array('min' => 0.005, 'max' => 1),
            ),
            'news_catalog_limit' => array(
                'required',
                'numerical' => array('integerOnly' => true),
            ),
            'mailing_new_order_to_admin' => array(
                'required',
                'boolean'
            ),
            'mailing_new_order_to_user' => array(
                'required',
                'boolean'
            ),
            'mailing_new_order_subject' => array(
                'filter' => array('filter' => 'strip_tags'),
                'length' => array('max' => 255)
            ),
            'mailing_new_order_pattern' => array(
                'safe'
            ),
            'smsing_new_order_to_admin' => array(
                'required',
                'boolean'
            ),
            'smsing_new_order_phones' => array(
                'match' => array(
                    'pattern' => '#^\((068|039|050|067)\)[0-9]{3}-[0-9]{2}-[0-9]{2}(,\s*\((068|039|050|067)\)[0-9]{3}-[0-9]{2}-[0-9]{2})*$#',
                    'message' => 'Не верный формат записи телефонов'
                ),
            ),
            'payment_required' => array(
                'required',
                'boolean'
            ),
            'delivery_required' => array(
                'required',
                'boolean'
            ),
            'vkontakte_api_id' => array(
                'numerical' => array('integerOnly' => true),
                'length' => array('max' => 11),
            ),
            'vkontakte_poll_id' => array(
                'length' => array('max' => 32),
            ),
            'vkontakte_group_id' => array(
                'length' => array('max' => 11),
            ),
            'cackle_site_id' => array(
                'numerical' => array('integerOnly' => true),
                'length' => array('max' => 11),
            ),
            'disqus_site_shortname' => array(
                'length' => array(
                    'max' => 50,
                    'min'=>3,
                    'tooShort'=>'Названия сайта должно содержать минимум три симола.',
                    'tooLong'=>'Названия сайта должно содержать максимум 50 симолов.',
                ),
                'match' => array(
                    'pattern' => '#^[a-z0-9]+[a-z0-9\-]*[a-z0-9]+$#',
                    'message' => 'Название сайта должно состоять только с дефисов и буквенно-цифровых символов. Оно должно начинаться и заканчиваться с алфавитно-цифровых символов.'
                ),
            ),

            'enterprise_1c_login' => array(
                'required',
                'length' => array('min' => 3, 'max' => 32),
                'match' => array('pattern' => '#^[a-zA-Z]+[a-zA-Z0-9]*$#', 'message' => 'Логин должен состоять из латинских букв и цифр'),
            ),
            'enterprise_1c_password' => array(
                'required',
                'length' => array('min' => 6, 'max' => 255),
            ),
            'project_house_equipment_link_visibility' => array(
                'required',
                'boolean'
            ),
            'project_faq_link_visibility' => array(
                'required',
                'boolean'
            ),
            'project_mounting_video_link_visibility' => array(
                'required',
                'boolean'
            ),
            'project_building_steps_link_visibility' => array(
                'required',
                'boolean'
            ),
            'show_actual_cost' => array(
                'required',
                'boolean'
            ),
            'price_notice_default_1' => array(
                'length' => array('max' => 255),
            ),
            'price_notice_default_2_uk' => array(
                'length' => array('max' => 255),
            ),
            'price_notice_default_2_en' => array(
                'length' => array('max' => 255),
            ),
            'price_notice_default_2_de' => array(
                'length' => array('max' => 255),
            ),
            'price_notice_default_2_fr' => array(
                'length' => array('max' => 255),
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'shop_name' => 'Название интернет магазина',
            'company' => 'Полное наименование компании',
            'admin_email' => 'Основной E-mail',
            'contact_email' => 'Контактный E-mail',
            'contact_phone' => 'Контактный телефон (через запятую)',
            'contact_phone_int' => 'Контактный телефон для зарубежных лендингов',

            'contact_address' => 'Контактный адрес (Русский)',
            'contact_address_uk' => 'Контактный адрес (Украинский)',
            'contact_address_en' => 'Контактный адрес (Английский)',
            'contact_address_de' => 'Контактный адрес (Немецкий)',
            'contact_address_fr' => 'Контактный адрес (Французский)',

            'main_text' => 'Текст на главной (Русский)',
            'main_text_uk' => 'Текст на главной (Украинский)',
            'main_text_en' => 'Текст на главной (Английский)',
            'main_text_de' => 'Текст на главной (Немецкий)',
            'main_text_fr' => 'Текст на главной (Французский)',

            'product_text' => 'Текст в каталоге проектов (Русский)',
            'product_text_uk' => 'Текст в каталоге проектов (Украинский)',
            'product_text_en' => 'Текст в каталоге проектов (Английский)',
            'product_text_de' => 'Текст в каталоге проектов (Немецкий)',
            'product_text_fr' => 'Текст в каталоге проектов (Французский)',

            'portfolio_text' => 'Текст в портфолио (Русский)',
            'portfolio_text_uk' => 'Текст в портфолио (Украинский)',
            'portfolio_text_en' => 'Текст в портфолио (Английский)',
            'portfolio_text_de' => 'Текст в портфолио (Немецкий)',
            'portfolio_text_fr' => 'Текст в портфолио (Французский)',

            'project_price_tab_text' => 'Текст в проекте на вкладке цен (Русский)',
            'project_price_tab_text_uk' => 'Текст в проекте на вкладке цен (Украинский)',
            'project_price_tab_text_en' => 'Текст в проекте на вкладке цен (Английский)',
            'project_price_tab_text_de' => 'Текст в проекте на вкладке цен (Немецкий)',
            'project_price_tab_text_fr' => 'Текст в проекте на вкладке цен (Французский)',

            'product_categiry_dachi' => 'Текст в категории дачи (Русский)',
            'product_categiry_dachi_uk' => 'Текст в категории дачи (Украинский)',
            'product_categiry_dachi_en' => 'Текст в категории дачи (Английский)',
            'product_categiry_dachi_de' => 'Текст в категории дачи (Немецкий)',
            'product_categiry_dachi_fr' => 'Текст в категории дачи (Французский)',

            'product_categiry_zagorodnie' => 'Текст в категории загородных домов (Русский)',
            'product_categiry_zagorodnie_uk' => 'Текст в категории загородных домов (Украинский)',
            'product_categiry_zagorodnie_en' => 'Текст в категории загородных домов (Английский)',
            'product_categiry_zagorodnie_de' => 'Текст в категории загородных домов (Немецкий)',
            'product_categiry_zagorodnie_fr' => 'Текст в категории загородных домов (Французский)',

            'contact_text' => 'Текст в контактах (Русский)',
            'contact_text_uk' => 'Текст в контактах (Украинский)',
            'contact_text_en' => 'Текст в контактах (Английский)',
            'contact_text_de' => 'Текст в контактах (Немецкий)',
            'contact_text_fr' => 'Текст в контактах (Французский)',

            'contact_map' => 'Код карты в контактах',
            'contact_use_captcha' => 'Используется капча',
            'counters' => 'Коды счетчиков',
            'price_accuracy' => 'Точность цены',
            'currency_default' => 'Валюта по умолчанию',
            'currency_basic' => 'Базовая валюта',

            'product_show_absent' => 'Показывать отсутствующие товары',
            'product_catalog_limit' => 'Товаров на страницу',
            'product_catalog_order' => 'Сортировка по умолчанию',
            'product_search_limit' => 'Товаров на страницу',
            'product_search_order' => 'Сортировка по умолчанию',
            'similar_price_accuracy' => 'Похожие товары',
            'news_catalog_limit' => 'Новостей на страницу',

            'vkontakte_api_id' => 'Ключ API',
            'vkontakte_poll_id' => 'Идентификатор опроса',
            'vkontakte_group_id' => 'Идентификатор группы в контакте',
            'cackle_site_id' => 'ID сайта',
            'disqus_site_shortname' => 'Краткое название сайта',

            'mailing_new_order_to_admin' => 'Оповещать администратора на ел. почту при оформлении заказа',
            'mailing_new_order_to_user' => 'Оповещать клиента на ел. почту при оформлении заказа',
            'mailing_new_order_subject' => 'Тема сообщения которое получит пользователь оформив заказ',
            'mailing_new_order_pattern' => 'Шаблон сообщения которое получит пользователь оформив заказ',
            'payment_required' => 'Форма оплаты обязательна для заполнения',
            'delivery_required' => 'Способ доставки обязателен для заполнения',

            'smsing_new_order_to_admin' => 'Отправлять sms при оформлении заказа',
            'smsing_new_order_phones' => 'Телефоны на которые отправлять sms',
            'enterprise_1c_login' => 'Пользователь',
            'enterprise_1c_password' => 'Пароль',

            'project_house_equipment_link_visibility' => 'Отображать ссылку на оснащение дома',
            'project_faq_link_visibility' => 'Отображать ссылку на FAQ',
            'project_mounting_video_link_visibility' => 'Отображать ссылку на видео о монтаже',
            'project_building_steps_link_visibility' => 'Отображать ссылку на этапы строительства',
            'show_actual_cost' => 'Отображать запрос на стоимость',

            'price_notice_default_1' => 'Дата обновления прайса',
            'price_notice_default_2' => 'Примечание к цене (Русский)',
            'price_notice_default_2_uk' => 'Примечание к цене (Украинский)',
            'price_notice_default_2_en' => 'Примечание к цене (Английский)',
            'price_notice_default_2_de' => 'Примечание к цене (Немецкий)',
            'price_notice_default_2_fr' => 'Примечание к цене (Французский)',
        );
    }

    public function getCurrencyList()
    {
        return CHtml::listData(Currency::model()->findAll(), 'id', 'name');
    }

    public function createValidators()
    {
        $validators = new CList;
        foreach ($this->rules() as $attribute => $rules) {
            foreach ($rules as $rule => $params) {
                if (is_int($rule)) {
                    $rule = $params;
                    $params = array();
                }
                $validators->add(CValidator::createValidator($rule, $this, $attribute, $params));
            }
        }
        return $validators;
    }
}
