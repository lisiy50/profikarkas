<?php

/**
 * This is the model class for table "{{project_image}}".
 *
 * The followings are the available columns in table '{{project_image}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $filename
 * @property integer $position
 * @property integer $type
 *
 * @method string getImageUrl(string $size = 'default')
 *
 */
class ProjectImage extends ActiveRecord {

    const TYPE_VISUALIZATION = 1,
        TYPE_FACADE = 3,
        TYPE_INTERIOR = 2,
        TYPE_LAYOUT = 4,
        TYPE_AREA = 5;

    /**
     * Returns the static model of the specified AR class.
     * @return ProjectImage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{project_image}}';
    }

    public function behaviors(){
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior2',
                'fileAttribute' => 'filename',
                'rootFolder' => 'storage/project',
                'images'=> array(
                    'tmb' => array('.tmb', 150, 150, 'resize'=>'fill'),
                    'thumb' => array('thumb', 102, 60, 'resize'=>'fill'),
                    'facadeSmall' => array('facadeSmall', 218, 131, 'resize'=>'fill'),
                    'facadeLarge' => array('facadeLarge', 1000, 1000, 'resize'=>'max'),
                    'interiorSmall' => array('interiorSmall', 300, 168, 'resize'=>'fill'),
                    'landing' => array('landing', 318, 241, 'resize'=>'fill'),
                    'small' => array('small', 490, 257, 'resize'=>'fill'),
                    'large' => array('large', 907, 550, 'resize'=>'fill', 'watermark' => array('project_watermark.png', 843, 15)),
                    'layout' => array('layout', 590, 1000, 'resize'=>'width'),
                    'area' => array('area', 360, 800, 'resize'=>'max'),

                    'origin' => array('origin', 1,1, 'resize' => false),
                ),
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('project_id', 'required'),
            array('position, project_id, type', 'numerical', 'integerOnly'=>true),
            array('filename', 'safe'),
//            array('filename', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
        );
    }

    public function getProject() {
        if(empty($this->project_id) )
            return null;

        return CActiveRecord::model('Project')->findByPk($this->project_id);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'filename' => 'Изображение',
            'project_id' => 'ID владельца',
            'position' => 'Порядок',
        );
    }

    public function defaultScope() {
        $alias=$this->getTableAlias(false,false);
        $scopes=array(
            'order'=>"$alias.position",
        );

        return $scopes;
    }

    static function typeList()
    {
        return [
            self::TYPE_VISUALIZATION => 'Визуализация',
            self::TYPE_INTERIOR => 'Интерьер',
            self::TYPE_FACADE => 'Фасад',
            self::TYPE_LAYOUT => 'Планировка',
            self::TYPE_AREA => 'Участок',
        ];

    }

    function getTypeList()
    {
        return self::typeList();
    }

}