<?php

class LandingDeForm extends CFormModel
{
	public $name;
	public $country;
	public $city;
	public $email;
	public $text;
	public $file;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email, country, city, text', 'required'),
			array('email', 'email'),
			array('file', 'file', 'maxSize' => 50 * 1000 * 1000, 'tooLarge' => 'Die Dateigröße "{File}" ist zu groß, sie sollte 50 MB nicht überschreiten.', 'allowEmpty'=>true),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name'=>'Name',
            'country'=>'Land',
            'city'=>'Stadt',
            'email'=>'E-Mail-Adresse',
			'text'=>'Text',
			'file'=>'File',
		);
	}
}
