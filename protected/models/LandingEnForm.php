<?php

class LandingEnForm extends CFormModel
{
	public $name;
	public $country;
	public $city;
	public $email;
	public $text;
	public $file;
//    public $subject;
//    public $comment;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email, country, city, text', 'required'),
			array('email', 'email'),
			array('file', 'file', 'maxSize' => 50 * 1000 * 1000, 'tooLarge' => 'File size "{file}" is too large, it must not exceed 50M.', 'allowEmpty'=>true),
//            array('subject, comment', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name' => 'Name',
            'country' => 'Country',
            'city' => 'City',
            'email' => 'Email',
			'text' => 'Text',
			'file' => 'File',
		);
	}
}
