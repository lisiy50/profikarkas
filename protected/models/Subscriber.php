<?php

/**
 * This is the model class for table "{{subscriber}}".
 *
 * The followings are the available columns in table '{{subscriber}}':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 */
class Subscriber extends ActiveRecord
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    public $status=self::STATUS_DISABLED;

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @return EquipHouses the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{subscriber}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'filter', 'filter'=>'strip_tags'),
//            array('content','filter','filter'=>array($obj=new EHtmlPurifier(),'purify')),
            array('name, status, email', 'required'),
            array('email', 'email'),
            array('email', 'unique'),
            array('status', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('name', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, email', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'status' => 'Активно',
            'create_time' => 'Добавлена',
            'update_time' => 'Изменена',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('email',$this->email, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'update_time DESC',
            ),
        ));
    }

}