<?php

/**
 * This is the model class for table "{{seo}}".
 *
 * The followings are the available columns in table '{{seo}}':
 * @property integer $id
 * @property string $route
 * @property string $entity
 * @property string $metaTitle
 * @property string $metaTitle_uk
 * @property string $metaTitle_en
 * @property string $metaTitle_de
 * @property string $metaTitle_fr
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $metaDescription_uk
 * @property string $metaDescription_en
 * @property string $metaDescription_de
 * @property string $metaDescription_fr
 */
class SEO extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SEO the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('metaTitle, metaKeywords, metaDescription, route', 'filter', 'filter'=>'strip_tags'),
            array('route', 'required'),
            array('metaTitle, metaTitle_uk, metaTitle_en, metaTitle_de, metaTitle_fr, metaKeywords', 'length', 'max'=>500),
            array('metaDescription, metaDescription_uk, metaDescription_en, metaDescription_de, metaDescription_fr', 'safe'),
            array('entity', 'numerical', 'integerOnly'=>true),
            array('metaTitle, metaTitle_uk, metaTitle_en, metaTitle_de, metaTitle_fr, metaKeywords, metaDescription, metaDescription_uk, metaDescription_en, metaDescription_de, metaDescription_fr, entity, route', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'route' => 'Route',
			'entity' => 'Связь',
			'metaTitle' => 'Мета-заголовок (Русский)',
			'metaTitle_uk' => 'Мета-заголовок (Украинский)',
			'metaTitle_en' => 'Мета-заголовок (Английский)',
			'metaTitle_de' => 'Мета-заголовок (Немецкий)',
			'metaTitle_fr' => 'Мета-заголовок (Французский)',
			'metaKeywords' => 'Meta Keywords',
			'metaDescription' => 'Мета-описание (Русский)',
            'metaDescription_uk' => 'Мета-описание (Украинский)',
            'metaDescription_en' => 'Мета-описание (Английский)',
            'metaDescription_de' => 'Мета-описание (Немецкий)',
            'metaDescription_fr' => 'Мета-описание (Французский)',
		);
	}

    public function findByRoute($route, $entity=null) {
        $criteria=new CDbCriteria;
        $criteria->compare('route', $route);
        if($entity)
            $criteria->compare('entity', $entity);
		return $this->query($criteria);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('route',$this->route);
		$criteria->compare('entity',$this->entity,true);
		$criteria->compare('metaTitle',$this->metaTitle,true);
		$criteria->compare('metaTitle_uk',$this->metaTitle_uk,true);
		$criteria->compare('metaTitle_en',$this->metaTitle_en,true);
		$criteria->compare('metaTitle_de',$this->metaTitle_de,true);
		$criteria->compare('metaTitle_fr',$this->metaTitle_fr,true);
		$criteria->compare('metaKeywords',$this->metaKeywords,true);
		$criteria->compare('metaDescription',$this->metaDescription,true);
		$criteria->compare('metaDescription_uk',$this->metaDescription_uk,true);
		$criteria->compare('metaDescription_en',$this->metaDescription_en,true);
		$criteria->compare('metaDescription_de',$this->metaDescription_de,true);
		$criteria->compare('metaDescription_fr',$this->metaDescription_fr,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

}
