<?php

/**
 * SignUpForDemoHouse class.
 * SignUpForDemoHouse is the data structure for keeping
 * sign up data. It is used by the 'signUpForDemoHouse' action of 'SiteController'.
 */
class SignUpForDemoHouse extends CFormModel
{
	public $name;
	public $email;
    public $phone;
    public $subject;
    public $comment;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('name, phone', 'required'),
            array('phone', 'match', 'pattern' => '/^(?=.*[0-9])[- +()0-9]+$/', 'message' => 'Поле Телефон содержит недопустимые символы.'),
			array('subject, email, comment', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name' => t('Имя'),
            'phone' => 'Телефон',
		);
	}
}
