<?php

/**
 * CostCalculationForm class.
 * CostCalculationForm is the data structure for keeping
 * recall form data. It is used by the 'recall' action of 'SiteController'.
 */
class CostCalculationForm extends CFormModel
{
	public $name;
	public $email;
	public $phone;
	public $file;
    public $subject;
    public $comment;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email', 'required'),
			array('phone', 'length', 'max' => 32),
			array('email', 'email'),
			array('file', 'file', 'maxSize' => 50 * 1000 * 1000, 'tooLarge' => 'Размер файла "{file}" слишком велик, он не должен превышать 50M.', 'allowEmpty'=>true),
            array('subject, comment', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name'=>'Имя',
            'phone'=>'Телефон',
            'email'=>'Email',
			'file'=>'Файл **',
		);
	}
}
