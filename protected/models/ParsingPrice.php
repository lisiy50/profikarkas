<?php

/**
 * This is the model class for table "{{parsing_price}}".
 *
 * The followings are the available columns in table '{{parsing_price}}':
 * @property integer $id
 * @property integer $page_id
 * @property integer $category_id
 * @property string $type
 * @property string $name
 * @property string $pro
 * @property integer $rating
 * @property string $city
 * @property integer $price
 * @property string $price_type
 * @property string $price_comment
 * @property string $date
 * @property string $link
 * @property integer $create_time
 * @property integer $update_time
 */
class ParsingPrice extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParsingPrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{parsing_price}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, category_id, type, name, rating, price, price_type, link', 'required'),
			array('page_id, category_id, rating, price', 'numerical', 'integerOnly'=>true),
			array('type, price_type, date', 'length', 'max'=>32),
			array('name, link', 'length', 'max'=>255),
			array('pro', 'length', 'max'=>16),
			array('city, price_comment', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, page_id, category_id, type, name, pro, rating, city, price, price_type, price_comment, date, link', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'category_id' => 'Category',
			'type' => 'Type',
			'name' => 'Name',
			'pro' => 'Pro',
			'rating' => 'Rating',
			'city' => 'City',
			'price' => 'Price',
			'price_type' => 'Price Type',
			'price_comment' => 'Price Comment',
			'date' => 'Date',
			'link' => 'Link',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('pro',$this->pro,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('price_type',$this->price_type,true);
		$criteria->compare('price_comment',$this->price_comment,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}