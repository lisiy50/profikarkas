<?php

/**
 * This is the model class for table "{{doc}}".
 *
 * The followings are the available columns in table '{{doc}}':
 * @property integer $id
 * @property string $contract
 * @property integer $date_day
 * @property string $date_moth
 * @property integer $date_year
 * @property string $client_name
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_issued
 * @property string $passport_date
 * @property string $address
 * @property string $project
 * @property string $phone
 * @property integer $price
 * @property string $price_text
 * @property string $signature
 * @property string $email
 * @property string $cover_up_date
 */
class Doc extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Doc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{doc}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_day, date_year, price', 'numerical', 'integerOnly'=>true),
			array('contract, date_moth, passport_series, passport_number, passport_date, project, phone, cover_up_date', 'length', 'max'=>50),
			array('client_name, passport_issued, address, price_text, signature', 'length', 'max'=>1024),
			array('email', 'length', 'max'=>256),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contract, date_day, date_moth, date_year, client_name, passport_series, passport_number, passport_issued, passport_date, address, project, phone, price, price_text, signature, email, cover_up_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contract' => 'Номер договора',
			'date_day' => 'Число (23)',
			'date_moth' => 'Месяц (апреля)',
			'date_year' => 'Год (2014)',
			'client_name' => 'ФИО (Смитт Джон Джонович)',
			'passport_series' => 'Серия паспорта',
			'passport_number' => 'Номер паспорта',
			'passport_issued' => 'Кем выдан паспорт',
			'passport_date' => 'Дата выдачи паспорта (31.12.2014)',
			'address' => 'Адрес',
			'project' => 'Название проекта (z1 lb)',
			'phone' => 'Телефонный номер',
			'price' => 'Цена (цыфры)',
			'price_text' => 'Цена (словами)',
			'signature' => 'Подпись клиента (Смитт Д. Д.)',
			'email' => 'Email (e@mail.com)',
			'cover_up_date' => 'Оплачивает до (31.12.2014)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contract',$this->contract,true);
		$criteria->compare('date_day',$this->date_day);
		$criteria->compare('date_moth',$this->date_moth,true);
		$criteria->compare('date_year',$this->date_year);
		$criteria->compare('client_name',$this->client_name,true);
		$criteria->compare('passport_series',$this->passport_series,true);
		$criteria->compare('passport_number',$this->passport_number,true);
		$criteria->compare('passport_issued',$this->passport_issued,true);
		$criteria->compare('passport_date',$this->passport_date,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('project',$this->project,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('price_text',$this->price_text,true);
		$criteria->compare('signature',$this->signature,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('cover_up_date',$this->cover_up_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}