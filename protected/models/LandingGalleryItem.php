<?php

/**
 * This is the model class for table "{{landing_gallery_item}}".
 *
 * The followings are the available columns in table '{{landing_gallery_item}}':
 * @property integer $id
 * @property integer $landing_gallery_id
 * @property string $image_1_url
 * @property string $image_2_url
 * @property string $video
 * @property string $title_ru
 * @property string $title_ua
 * @property string $text_ru
 * @property string $text_ua
 * @property integer $position
 * @property integer $status
 */
class LandingGalleryItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LandingGalleryItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{landing_gallery_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('landing_gallery_id', 'required'),
			array('landing_gallery_id, position, status', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ua', 'length', 'max'=>255),
			array('video, text_ru, text_ua', 'safe'),
//            array('image_1_url, image_2_url', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, landing_gallery_id, image_1_url, image_2_url, video, title_ru, title_ua, text_ru, text_ua, position, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'landing_gallery_id' => 'ID галереи',
			'image_1_url' => 'Изображение',
			'image_2_url' => 'Изображение (для мобильных)',
			'video' => 'Youtube код',
			'title_ru' => 'Заголовок (русский)',
			'title_ua' => 'Заголовок (украинский)',
			'text_ru' => 'Текст (русский)',
			'text_ua' => 'Текст (украинский)',
			'position' => 'Позиция',
			'status' => 'Показывать на сайте',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('landing_gallery_id',$this->landing_gallery_id);
		$criteria->compare('image_1_url',$this->image_1_url,true);
		$criteria->compare('image_2_url',$this->image_2_url,true);
		$criteria->compare('video',$this->video,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ua',$this->title_ua,true);
		$criteria->compare('text_ru',$this->text_ru,true);
		$criteria->compare('text_ua',$this->text_ua,true);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
