<?php

/**
 * This is the model class for table "{{extra_image}}".
 *
 * The followings are the available columns in table '{{extra_image}}':
 * @property integer $id
 * @property string $owner_model
 * @property integer $owner_id
 * @property string $filename
 * @property string $description
 * @property integer $position
 * @property integer $additional_option
 */
class ExtraImage extends ActiveRecord {

    const ADDITIONAL_OPTION_ENABLE = 1;
    const ADDITIONAL_OPTION_DISABLE = 2;

    /**
     * Returns the static model of the specified AR class.
     * @return Image the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{extra_image}}';
    }

    public function behaviors(){
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> array(
                    'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'extra_'),
                    'small' => array('storage/extra', 300, 300, 'prefix'=>'small_'),
                    'large' => array('storage/extra', 1280, 1024, 'prefix'=>'large_'),
                ),
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('description','safe'),
            array('owner_id, owner_model', 'required'),
            array('position, owner_id, additional_option', 'numerical', 'integerOnly'=>true),
            array('additional_option', 'in', 'range'=>array(self::ADDITIONAL_OPTION_DISABLE, self::ADDITIONAL_OPTION_ENABLE)),
            array('filename', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
        );
    }

    public function getOwner() {
        if(empty($this->owner_model) || empty($this->owner_id) )
            return null;

        return CActiveRecord::model($this->owner_model)->findByPk($this->owner_id);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'filename' => 'Изображение',
            'description' => 'Описание',
            'owner_id' => 'ID владельца',
            'owner_model' => 'Модель владельца',
            'position' => 'Порядок',
            'additional_option' => 'Дополнительная опция',
        );
    }

    public function defaultScope() {
        $alias=$this->getTableAlias(false,false);
        $scopes=array(
            'order'=>"$alias.position",
        );

        return $scopes;
    }

}
