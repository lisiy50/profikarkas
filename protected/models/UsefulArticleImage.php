<?php
class UsefulArticleImage extends ExtraImage {

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'useful_article_'),
            'mini' => array('storage/useful_article', 102, 60, 'resize'=>'fill', 'prefix'=>'mini_'),
            'small' => array('storage/useful_article', 389, 204, 'prefix'=>'small_', 'resize'=>'fill'),
            'more' => array('storage/useful_article', 1000, 2000, 'resize'=>'max', 'prefix'=>'more_'),
            'large' => array('storage/useful_article', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
            'original' => array('storage/useful_article', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->getOwner() && $this->getOwner()->watermark == Portfolio::STATUS_ENABLED){
            $images['more'] = array('storage/useful_article', 1000, 550, 'resize'=>'max', 'prefix'=>'more_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['large'] = array('storage/useful_article', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> $images,
            ),
        );
    }

}