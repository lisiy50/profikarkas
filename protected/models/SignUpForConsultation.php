<?php

/**
 * RecallForm class.
 * RecallForm is the data structure for keeping
 * recall form data. It is used by the 'recall' action of 'SiteController'.
 */
class SignUpForConsultation extends CFormModel
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $comment;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, phone', 'required'),
			array('email', 'email'),
            array('subject, comment', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name'=>'Имя',
            'phone'=>'Телефон',
//            'email'=>'Email',
		);
	}
}
