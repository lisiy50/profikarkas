<?php

/**
 * This is the model class for table "{{landing}}".
 *
 * The followings are the available columns in table '{{landing}}':
 * @property integer $id
 * @property string $name
 * @property string $title_ru
 * @property string $title_ua
 * @property string $text_ru
 * @property string $text_ua
 * @property string $meta_title_ru
 * @property string $meta_title_ua
 * @property string $meta_description_ru
 * @property string $meta_description_ua
 */
class Landing extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{landing}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title_ru, title_ua, meta_title_ru, meta_title_ua', 'length', 'max'=>255),
			array('text_ru, text_ua, meta_description_ru, meta_description_ua', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, title_ru, title_ua, text_ru, text_ua, meta_title_ru, meta_title_ua, meta_description_ru, meta_description_ua', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название лендинга',
			'title_ru' => 'Основной заголовок (русский)',
			'title_ua' => 'Основной заголовок (украинский)',
			'text_ru' => 'Seo текст (русский)',
			'text_ua' => 'Seo текст (украинский)',
			'meta_title_ru' => 'Мета заголовок (русский)',
			'meta_title_ua' => 'Мета заголовок (украинский)',
			'meta_description_ru' => 'Мета описание (русский)',
			'meta_description_ua' => 'Мета описание (украинский)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ua',$this->title_ua,true);
		$criteria->compare('text_ru',$this->text_ru,true);
		$criteria->compare('text_ua',$this->text_ua,true);
		$criteria->compare('meta_title_ru',$this->meta_title_ru,true);
		$criteria->compare('meta_title_ua',$this->meta_title_ua,true);
		$criteria->compare('meta_description_ru',$this->meta_description_ru,true);
		$criteria->compare('meta_description_ua',$this->meta_description_ua,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Landing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
