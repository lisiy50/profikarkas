<?php

/**
 * RecallForm class.
 * RecallForm is the data structure for keeping
 * recall form data. It is used by the 'recall' action of 'SiteController'.
 */
class RecallForm extends CFormModel
{
	public $form;
	public $name;
    public $phone;
    public $email;
	public $comment;
	public $subject;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, phone, email', 'required'),
			array('email', 'email'),
			array('comment, form, subject', 'safe')
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name'=> t('Имя'),
            'phone'=>'Телефон',
            'email'=>'E-mail',
            'comment'=>'Сообщение',
		);
	}
}
