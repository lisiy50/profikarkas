<?php

/**
 * This is the model class for table "{{staff}}".
 *
 * The followings are the available columns in table '{{staff}}':
 * @property integer $id
 * @property string $name
 * @property string $post_text
 * @property string $post
 * @property string $image
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 */
class Staff extends LibraryRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Staff the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{staff}}';
    }

    public function behaviors()
    {
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'name',
                'images' => array(
                    'thumb' => array('storage/.tmb', 70, 70, 'resize' => 'fill', 'required' => 'thumb.jpg'),

                    'small' => array('storage/staff', 186, 136, 'resize' => 'fill', 'required' => 'small.jpg', 'prefix' => 'small_'),
                    'smallGray' => array('storage/staff', 186, 136, 'resize' => 'fill', 'required' => 'small.jpg', 'gray'=>true, 'prefix' => 'smallGray_'),

                    'medium' => array('storage/staff', 186, 282, 'resize' => 'fill', 'required' => 'medium.jpg', 'prefix' => 'medium_'),
                    'mediumGray' => array('storage/staff', 186, 282, 'resize' => 'fill', 'required' => 'medium.jpg', 'gray'=>true, 'prefix' => 'mediumGray_'),

                    'large' => array('storage/staff', 388, 282, 'resize' => 'fill', 'required' => 'large.jpg', 'prefix' => 'large_'),
                    'largeGray' => array('storage/staff', 388, 282, 'resize' => 'fill', 'required' => 'large.jpg', 'gray'=>true, 'prefix' => 'largeGray_'),

                    'original' => array('storage/staff', 1280, 1024, 'resize'=>'max', 'prefix'=>'original_'),
                ),
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, post_text, post, status, image_size', 'required'),
            array('status', 'numerical', 'integerOnly'=>true),
            array('name, post_text, post', 'length', 'max'=>255),
            array('image_size', 'in', 'range'=>array_keys($this->getImageSizes())),
            array('post', 'in', 'range'=>array_keys($this->getPosts())),
            array('image', 'file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true),
            array('image', 'unsafe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, post_text, post, status, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Имя',
            'post_text' => 'Должность',
            'post' => 'Должность',
            'image' => 'Изображение',
            'image_size' => 'Размер изображения',
            'status' => 'Запись активна',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('post_text',$this->post_text,true);
        $criteria->compare('post',$this->post,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_time',$this->create_time);
        $criteria->compare('update_time',$this->update_time);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function getImageSizes(){
        return array(
            'small' => 'Маленькое',
            'medium' => 'Среднее',
            'large' => 'Большое',
        );
    }

    public function getPosts(){
        return array(
            'manager' => 'Менеджер',
            'marketer' => 'Маркетолог',
            'technicians' => 'Технический специалист',
        );
    }

    public function getPostName(){
        return $this->posts[$this->post];
    }
}