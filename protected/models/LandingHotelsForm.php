<?php

class LandingHotelsForm extends CFormModel
{
	public $name;
    public $email;
    public $phone;
	public $text;
	public $file;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return [
			['name, email, phone', 'required'],
			['email', 'email'],
			['file', 'file', 'maxSize' => 50 * 1000 * 1000, 'tooLarge' => 'Размер "{file}" не может превышать 50 Мб.', 'allowEmpty' => true],
            ['text', 'safe'],
		];
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'name' => 'Имя',
            'email' => 'E-mail',
			'phone' => 'Телефон',
			'text' => 'Комментарий',
			'file' => 'File',
		);
	}
}
