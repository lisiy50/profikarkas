<?php

/**
 * This is the model class for table "{{useful_article}}".
 *
 * The followings are the available columns in table '{{useful_article}}':
 * @property integer $id
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $title_de
 * @property string $title_fr
 * @property string $project_name
 * @property string $project_name_uk
 * @property string $project_name_en
 * @property string $project_name_de
 * @property string $project_name_fr
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_de
 * @property string $description_fr
 *
 * @property string $video_code1
 * @property string $video_code2
 * @property string $video_code3
 * @property string $video_code4
 * @property string $video_code5
 * @property string $video_code6
 * @property integer $service1
 * @property integer $service2
 * @property integer $service3
 * @property integer $service4
 * @property integer $service5
 * @property integer $service6
 *
 * @property integer $status
 * @property integer $status_main
 * @property integer $watermark
 * @property integer $section
 * @property string $create_time
 * @property string $update_time
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 * @property string $title_color
 *
 * @property Comment[] $comments
 */
class UsefulArticle extends LibraryRecord
{
    const SECTION_USEFUL_ARTICLE = 1;
    const SECTION_PRESS = 2;

    public $watermark = self::STATUS_ENABLED;

    private $_videos = array();
    private $_nextProjetcModel = false;
    private $_prevProjetcModel = false;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{useful_article}}';
	}

    public function behaviors(){
  	    return array(
  		    'CTimestampBehavior' => array(
  		  	    'class' => 'zii.behaviors.CTimestampBehavior',
  		  	    'createAttribute' => 'create_time',
  			    'updateAttribute' => 'update_time',
  		    ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/useful_article', 389, 204, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'small_'),
                    'original' => array('storage/useful_article', 770, 770, 'resize'=>'false', 'prefix' => 'original_' /*'watermark'=>array('watermark.jpg', -10, -10, 'opacity'=>50)*/),
                ),
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'usefulArticle/view',
            ),
            'imageResizeBehavior' => array(
                'class' => 'ImageResizeBehavior',
                'fields' => array('description'),
            ),
  	    );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, title_uk, title_en, title_de, title_fr, project_name, project_name_uk, project_name_en, project_name_de, project_name_fr, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'strip_tags'),
            array('title, title_uk, title_en, title_de, title_fr, project_name, project_name_uk, project_name_en, project_name_de, project_name_fr, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'trim'),
			array('description, description_uk, description_en, description_de, description_fr', 'safe'),
			array('title, section', 'required'),
			array('title, title_uk, title_en, title_de, title_fr, project_name, project_name_uk, project_name_en, project_name_de, project_name_fr', 'length', 'max'=>255),
			array('title_color', 'length', 'max'=>7),
			array('status, status_main, watermark, service1, service2, service3, service4, service5, service6, section, h1_size,h2_size,h3_size,h4_size', 'numerical', 'integerOnly'=>true),
            array('status, status_main, watermark', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('image', 'unsafe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, project_name, status, status_main, section', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'firstImage'=>array(self::HAS_ONE, 'UsefulArticleImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'images'=>array(self::HAS_MANY, 'UsefulArticleImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'UsefulArticleImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'comments'=>array(self::HAS_MANY, 'Comment', 'owner_id', 'condition' => 'comments.owner = :owner AND comments.parent_id IS NULL', 'params' => array(':owner' => __CLASS__)),
            'countComments'=>array(self::STAT, 'Comment', 'owner_id', 'condition' => 'owner = :owner AND parent_id IS NULL', 'params' => array(':owner' => __CLASS__)),
        );
	}

    public function defaultScope(){
        return array(
            'order' => 'create_time DESC',
        );
    }

    public function scopes(){
        return array(
            'directOrder' => array(
                'order' => 'create_time DESC',
            ),
            'reverseOrder' => array(
                'order' => 'create_time',
            ),
            'usefulArticles' => array(
                'condition' => 'section = :section OR section = ""',
                'params' => array(':section'=>self::SECTION_USEFUL_ARTICLE),
            ),
            'press' => array(
                'condition' => 'section = :section',
                'params' => array(':section'=>self::SECTION_PRESS),
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'title' => 'Заголовок (Русский)',
            'title_uk' => 'Заголовок (Украинский)',
            'title_en' => 'Заголовок (Английский)',
            'title_de' => 'Заголовок (Немецкий)',
            'title_fr' => 'Заголовок (Французский)',

            'project_name' => 'Название проекта (Русский)',
            'project_name_uk' => 'Название проекта (Украинский)',
            'project_name_en' => 'Название проекта (Английский)',
            'project_name_de' => 'Название проекта (Немецкий)',
            'project_name_fr' => 'Название проекта (Французский)',

            'description' => 'Содержание (Русский)',
            'description_uk' => 'Содержание (Украинский)',
            'description_en' => 'Содержание (Английский)',
            'description_de' => 'Содержание (Немецкий)',
            'description_fr' => 'Содержание (Французский)',

            'image' => 'Изображение',

            'watermark' => 'Водяной знак',
			'status' => 'Включена',
			'status_main' => 'Включена на главной',
            'section' => 'Раздел',
			'create_time' => 'Добавлена',
			'update_time' => 'Изменена',

            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',
            'title_color' => 'Цвет заглавия',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_uk',$this->title_uk,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('title_de',$this->title_de,true);
		$criteria->compare('title_fr',$this->title_fr,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('description_uk',$this->description_uk,true);
		$criteria->compare('description_en',$this->description_en,true);
		$criteria->compare('description_de',$this->description_de,true);
		$criteria->compare('description_fr',$this->description_fr,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('status_main',$this->status_main);
        $criteria->compare('section',$this->section);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    public function getNextProject(){
        if($this->_nextProjetcModel === false)
            $this->_nextProjetcModel = self::model()->find('create_time < ?', array($this->create_time));
        return $this->_nextProjetcModel;
    }

    public function getPrevProject(){
        if($this->_prevProjetcModel === false)
            $this->_prevProjetcModel = self::model()->resetScope()->reverseOrder()->find('create_time > ?', array($this->create_time));
        return $this->_prevProjetcModel;
    }



    public function getUrl($params=array()) {
        if(IS_BACKEND)
            $urlManager=Yii::app()->frontendUrlManager;
        else
            $urlManager=Yii::app()->urlManager;

        if (isset($params['route'])) {
            $route = $params['route'];
            unset($params['route']);
        } else {
            $route=strtolower(get_class($this)).'/view';
        }

        if(!isset($params['uri']))
            $params['uri'] = strtolower(app()->translitFormatter->formatUrl($this->title));

        return $urlManager->createUrl($route, CMap::mergeArray(array('id'=>$this->primaryKey), $params));
    }

    public function getPressUrl()
    {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->title);
        if(IS_BACKEND)
            $urlManager=Yii::app()->frontendUrlManager;
        else
            $urlManager=Yii::app()->urlManager;

        $route='press/view';

        return $urlManager->createUrl($route, CMap::mergeArray(array('id'=>$this->primaryKey), $params));
    }

    public function getVideos(){
        if(!$this->_videos){
            $i = 1;
            $videoCodeProperty = 'video_code'.$i;
            $videoSevice = 'service'.$i;
            while(isset($this->$videoCodeProperty) && !empty($this->$videoCodeProperty)){
                $image = $this->getVideoMiniImage($this->$videoCodeProperty, $this->$videoSevice);
                $this->_videos[] = array(
                    'service' => $this->$videoSevice,
                    'code' => $this->$videoCodeProperty,
//                'htmlCode' => $this->getVideoInsertHtml($this->$videoCodeProperty, $this->$videoSevice),
                    'miniImageHtml' => isset($image['mini'])?$image['mini']:false,
                    'largeImageHtml' => isset($image['large'])?$image['large']:false,
                    'videoLink' => $this->getVideoLink($this->$videoCodeProperty, $this->$videoSevice),
                );
                $i++;
                $videoCodeProperty = 'video_code'.$i;
                $videoSevice = 'service'.$i;
            }
        }

        return $this->_videos;
    }

    public function getCountVideos(){
        return count($this->getVideos());
    }

    public function getVideoInsertHtml($code, $service = 1){
        switch ($service){
            case 1:
                return '<iframe width="420" height="315" src="//www.youtube.com/embed/'.$code.'" frameborder="0" allowfullscreen></iframe>';
            case 2:
                return '<iframe src="//player.vimeo.com/video/'.$code.'" width="500" height="282" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            default:
                return false;
        }
    }

    public function getVideoMiniImage($code, $service = 1){
        switch ($service){
            case 1:
                return array(
                    'mini' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">',
                    'large' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/0.jpg" src="//i1.ytimg.com/vi/'.$code.'/0.jpg" alt="" width="400" data-group-key="thumb-group-0">',
                );
                return '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">';
            case 2:
                $var = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$code.php"));
                if(isset($var[0])){
                    return array(
                        'mini' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_medium']), false, false),
                        'large' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_large'], 'style'=>'width:400px;'), false, false),
                    );
                }
            default:
                return false;
        }
    }

    public function getVideoLink($code, $service = 1){
        switch ($service){
            case 1:
                return 'http://www.youtube.com/embed/'.$code;
            case 2:
                return 'http://player.vimeo.com/video/'.$code.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ff0179';
            default:
                return false;
        }
    }

    public function getSectionList()
    {
        return array(
            self::SECTION_USEFUL_ARTICLE => 'Полезные статьи',
            self::SECTION_PRESS => 'Мы в прессе',
        );
    }

}
