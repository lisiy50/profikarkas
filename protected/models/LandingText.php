<?php

/**
 * This is the model class for table "{{landing_text}}".
 *
 * The followings are the available columns in table '{{landing_text}}':
 * @property integer $id
 * @property integer $landing_id

 * @property string $text_ru
 * @property string $text_ua
 * @property string $name
 * @property string $name_manager
 */
class LandingText extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LandingText the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{landing_text}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('landing_id', 'required'),
            array('landing_id', 'numerical', 'integerOnly'=>true),
            array('name, name_manager', 'length', 'max'=>255),
            array('text_ru, text_ua', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, text_ru, text_ua, name, name_manager, landing_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'landing_id' => 'ID лендинга',
            'text_ru' => 'Текст (русский)',
            'text_ua' => 'Текст (украинский)',
            'name' => 'Название',
            'name_manager' => 'Примечание',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('text_ru',$this->text_ru,true);
        $criteria->compare('text_ua',$this->text_ua,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('name_manager',$this->name_manager,true);
        $criteria->compare('landing_id',$this->landing_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}



