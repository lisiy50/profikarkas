<?php
$config = $_SERVER['SERVER_NAME'];
if (strpos($config, 'www.') !== false)
    $config = str_replace('www.', '', $config);

$config.='.php';

if (!file_exists(dirname(__FILE__) . DS . $config))
    $config = 'production.php';

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'defaultController' => 'landing/index',
        'controllerPath'=>dirname(__FILE__).DS.'..'.DS.'frontend'.DS.'controllers',
        'viewPath'=>dirname(__FILE__).DS.'..'.DS.'frontend'.DS.'views',
        'theme' => 'classic',
        'packageRoutes' => false,

        'onBeginRequest' => function () {
//            $langParam = explode('/', $_SERVER["REQUEST_URI"])[2]; //dev
            $langParam = explode('/', $_SERVER["REQUEST_URI"])[1]; //prod
            if (in_array($langParam, ['uk', 'en', 'de', 'fr'])) {
                Yii::app()->language = $langParam;
            }
        },

        'components' => array(
            'urlManager' => array(
                'urlFormat'=>'path',
                'showScriptName' => false,
                'class' => 'UrlManager',
                'rules' => array(
                    '<lang:(uk)>' => 'landing/index',
                    '' => 'landing/index',

                    '<lang:(uk|en|de|fr)>/hotels' => 'landing/hotels',
                    'hotels' => 'landing/hotels',

                    '<lang:(uk|en|de|fr)>/langs' => 'landing/langs',
                    'iframe' => 'landing/iframe',
                    'iframe2' => 'landing/iframe2',

                    '<lang:en>' => 'landing/en',
                    '<lang:de>' => 'landing/de',
                    '<lang:fr>' => 'landing/fr',

                    'zabory' => 'article/zabory',
                    'sitemap.xml' => 'sitemap/xml',
                    'sitemap.xml.gz' => 'sitemap/gz',
                    '<lang:(uk|en|de|fr)>/construction' => 'portfolio/construction',
                    'construction' => 'portfolio/construction',

                    '<lang:(uk|en|de|fr)>/<controller:\w+>/<id:\d+>-<uri>' => '<controller>/view',
                    '<controller:\w+>/<id:\d+>-<uri>' => '<controller>/view',


                    '<lang:(uk|en|de|fr)>/<controller:\w+>/page_<page:\d+>' => '<controller>/index',
                    '<controller:\w+>/page_<page:\d+>' => '<controller>/index',

                    '<lang:(uk|en|de|fr)>/<controller:\w+>' => '<controller>/index',
                    '<controller:\w+>' => '<controller>/index',

//                    '<lang:(uk|en|de|fr)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',

                    '<lang:(uk|en|de|fr)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

                    '<lang:(uk|en|de|fr)>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),
            ),
            'clientScript' => array(
                'class' => 'ClientScript',
                'coreScriptPosition' => CClientScript::POS_END,
                'defaultScriptFilePosition' => CClientScript::POS_END,
            ),
            'compare' => array(
                'class' => 'CompareComponent',
            ),

            'seo' => array(
                'class' => 'SEOComponent',
            ),

            'cart' => array(
                'class' => 'CartComponent',
            ),

            'widgetFactory' => array(
                'widgets' => array(
                    'CLinkPager' => array(
                        'header' => '',
                        'cssFile' => false,
                        'nextPageLabel' => '&#187;',
                        'prevPageLabel' => '&#171;',
                    ),
                    'CCaptcha' => array(
                        'clickableImage'=>true,
                        'showRefreshButton'=>false,
                        'imageOptions'=>array(
                            'style'=>'cursor:pointer;height:45px',
                        ),
                        'buttonLabel'=>'<i class="icon-refresh"></i>',
                    ),
                )
            ),
        ),
    ),
    require(dirname(__FILE__) . DS . $config)
);
