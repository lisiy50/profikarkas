<?php

return array(
    'components' => array(

//        'db' => array(
//            'connectionString' => 'mysql:host=localhost;dbname=profikar_yii',
//            'username' => 'profikar_db',
//            'password' => 'TopSecret',
//            'enableProfiling'=>true,
//            'enableParamLogging' => true,
//        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=profikarkas',
            'username' => 'valerii',
            'password' => 'Hv1levka-4m',
            'enableProfiling'=>true,
            'enableParamLogging' => true,
        ),

        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'info',
                    'categories'=>'info.*',
                ),
                array(
                    'class' => 'ext.yiidebugtb.XWebDebugRouter',
                    'config' => 'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
                    //'levels'=>'error, warning, trace, profile, info',
                    'levels' => 'error, warning, trace, info',
                    'allowedIPs' => array('127.0.0.1', '::1', '178.137.59.86', '192\.168\.1[0-5]\.[0-9]{3}'),
                ),
            ),
        ),
    ),
);
