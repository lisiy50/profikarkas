<?php
$config = $_SERVER['SERVER_NAME'];
if (strpos($config, 'www.') !== false)
    $config = str_replace('www.', '', $config);

$config.='.php';

if (!file_exists(dirname(__FILE__) . DS . $config))
    $config = 'production.php';

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'controllerPath'=>dirname(__FILE__).DS.'..'.DS.'backend'.DS.'controllers',
        'viewPath'=>dirname(__FILE__).DS.'..'.DS.'backend'.DS.'views',

        'name'=>'Admin panel',
        'language' => 'ru',

        'components'=>array(
            'urlManager'=>array(
                'urlFormat'=>CUrlManager::PATH_FORMAT,
                'rules'=>array(
                    '<controller:\w+>/<id:\d+>'=>'<controller>/update',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),

            'frontendUrlManager'=>array(
                'class'=>'FrontendUrlManager',
            ),

            'errorHandler'=>array(
                'errorAction'=>'site/error',
            ),
            'yexcel' => array(
                'class' => 'ext.yexcel.Yexcel'
            ),
        ),

        'modules'=>array(
            'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'password',
                'generatorPaths'=>array(
                    'application.gii',
                ),
                'ipFilters' => array('127.0.0.1','::1', '178.137.222.230'),
            ),
        ),
    ),
    require(dirname(__FILE__) . DS . $config)
);