<?php
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Magaziller',
    'language' => 'ru',

    'import'=>array(
        'application.models.*',
        'application.base.*',
        'application.widgets.*',
        'application.widgets.colorpicker.*',
        'application.components.*',
        'application.components.helpers.*',
        'application.components.behaviors.*',
        'application.components.handlers.*',
        'application.lib.ChromePhp',
        'application.lib.MobileDetect',
        'application.helpers.ZohoConnector',
        'application.helpers.ZohoHelper',
    ),

//    'aliases' => array(
//        'xupload' => 'ext.xupload'
//    ),

    // preloading 'log' component
    'preload'=>array('log'),

    // application components
    'components'=>array(
        'priceFormatter'=>array(
            'class'=>'PriceFormatter',
        ),

        'translitFormatter'=>array(
            'class'=>'TranslitFormatter',
        ),

        'config'=>array(
            'class'=>'ConfigComponent',
        ),

        'swiftMailer'=>array(
            'class'=>'SwiftMailer',
        ),

        'image'=>array(
            'class'=>'ext.image.ImageComponent',
            'driver'=>'GD',
        ),

        'currency'=>array(
            'class'=>'CurrencyComponent',
        ),

        'twig'=>array(
            'class'=>'TwigComponent',
        ),

        'user'=>array(
            'class'=>'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),

        'authManager' => array(
            'class' => 'PhpAuthManager',
        ),

        'assetManager' => array(
            'forceCopy' => true,
        ),

        'clientScript' => array(
            'class' => 'CClientScript',
            'packages' => include dirname(dirname(__FILE__)).DS.'data'.DS.'js_packages.php',
        ),

        'db'=>array(
            'emulatePrepare' => true,
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),

        'z500ruDb' => array(
            'connectionString' => 'mysql:host=localhost;dbname=basename',
            'username' => 'login',
            'password' => 'password',
        ),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
    ),

    'packages'=>array(
        /*array(
            'id'=>'crud',
            'modelClass'=>'Review',
            'label'=>'Отзывы клиентов',
            'nameAttribute'=>'title',
            'order'=>6,
            'searchAttributes'=>array(
                'title',
            ),
            'gridColumns'=>array(
                'id',
                'title',
                'create_time',
            )
        ),*/
        /*array(
            'id'=>'crud',
            'modelClass'=>'BuildInEurope',
            'label'=>'Так строят в Европе',
            'nameAttribute'=>'title',
            'order'=>7,
            'searchAttributes'=>array(
                'title',
            ),
            'gridColumns'=>array(
                'id',
                'title',
                'create_time',
            )
        ),*/

        'sliderItem',
        'article',
        'news',
//        'order',
//        'product',
        'gallery',
        'portfolio',
        'review',
//        'discount',
        //'market',
        //'scrap',
        'menuItem',
//        'enterprise1c',
        'textContent',
        'emailTemplate',
    ),

    'params'=>array(),
);
