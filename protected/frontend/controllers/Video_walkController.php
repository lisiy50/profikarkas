<?php

class Video_walkController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 29;

    public $highlight = 29;

    public function actionIndex(){
        if (isset($_GET['page'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = new VideoWalk('search');
        if(isset($_GET['VideoWalk']))
            $model->attributes = $_GET['VideoWalk'];

        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    public function actionCatalog()
    {
        $this->menu_id = 31;
        $this->highlight = 31;
        $this->actionIndex();
    }

    /**
     * @param $id
     * @return VideoWalk model class
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=VideoWalk::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
