<?php

class HomekitController extends FrontendController
{
	public $layout='//layouts/main';

    public function actionIndex()
    {
        Yii::app()->seo->setDefault('title', '{shop_name} - Домокомплекты');


        $this->render('index');
    }

    public function actionKit1() {
        $this->renderPartial('kit_1');

    }

    public function actionKit2() {
        $this->renderPartial('kit_2');

    }

    public function actionKit3() {
        $this->renderPartial('kit_3');

    }

    public function actionKit4() {
        $this->renderPartial('kit_4');

    }

    public function actionKit5() {
        $this->renderPartial('kit_5');

    }
}
