<?php
class TempController extends FrontendController {

    public $layout='//layouts/main';

    function actionProjects()
    {
        echo '<table border="1">';
        echo '<tr>';
        echo "<td>название</td>";
        echo "<td>этажность</td>";
        echo "<td>общая площадь</td>";
        echo '</tr>';
        /** @var Project $project */
        foreach(Project::model()->findAll() as $project) {
            echo '<tr>';
            echo "<td>{$project->getName()}</td>";
            echo "<td>{$project->floors}</td>";
            echo "<td>{$project->net_area}</td>";
            echo '</tr>';
        }
        echo '</table>';
    }


    public function actionWinter1()
    {
        cs()->registerScript('project-view', 'var SNOW_Picture = "https://uguide.ru/js/script/img_sneg.png";
    var SNOW_Width = "55px";
    var SNOW_Height = "55px";
    var SNOW_no = 99;', CClientScript::POS_HEAD);
        cs()->registerScriptFile('https://uguide.ru/js/script/skript_sneg_webanfarwol_ru.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

    public function actionWinter8()
    {
        cs()->registerScriptFile('https://uguide.ru/js/script/snow11.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

    public function actionWinter9()
    {
        cs()->registerScriptFile('https://uguide.ru/js/script/snowfall.min.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

    public function actionWinter10()
    {
        cs()->registerScriptFile('https://uguide.ru/js/script/snowcursor.min.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

    public function actionWinter11()
    {
        cs()->registerCss('witer11', 'body, a:hover {cursor: url(https://uguide.ru/js/script/snowcurser.cur), url(https://uguide.ru/js/script/snowcurser.png), auto !important;}');
        cs()->registerScriptFile('https://uguide.ru/js/script/snowcursor.min.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

    public function actionWinterZ500()
    {
        cs()->registerScript('project-view', '
        imageDir = "http://mvcreative.ru/example/6/2/snow/";
    sflakesMax = 65;
    sflakesMaxActive = 65;
    svMaxX = 2;
    svMaxY = 6;
    ssnowStick = 1;
    ssnowCollect = 0;
    sfollowMouse = 1;
    sflakeBottom = 0;
    susePNG = 1;
    sflakeTypes = 5;
    sflakeWidth = 15;
    sflakeHeight = 15;', CClientScript::POS_HEAD);
        cs()->registerScriptFile('http://mvcreative.ru/example/6/2/snow.js', CClientScript::POS_BEGIN);

        $this->render('/site/index');
    }

}
