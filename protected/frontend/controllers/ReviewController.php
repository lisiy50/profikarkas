<?php

class ReviewController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 29;
    public $highlight = 29;

    public function actionView($id){
        $this->highlight = 43;
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = $this->loadModel($id);
        if(!isset($_GET['uri']))
            $this->redirect($model->getUrl(), true, 301);

        Yii::app()->seo->setParam('title', $model->title);

        $this->render('view', array('model'=>$model));
    }

    /*
     * @return Review model class
     * */
    public function loadModel($id)
    {
        $model=Review::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex(){
        if (isset($_GET['page'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }

        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = new Review('search');
        $model->unsetAttributes();
        if(isset($_GET['Review']))
            $model->attributes = $_GET['Review'];

        $model->status = Review::STATUS_ENABLED;

        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }
}
