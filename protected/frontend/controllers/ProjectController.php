<?php

class ProjectController extends FrontendController{

    public $layout = '//layouts/main';

    public $highlight = 30;

    public function actionIndex(){
        if (isset($_GET['page']) || isset($_GET['Project_sort'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        } elseif ($_GET) {
            $this->additionalHeaderTags = '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }

        $projectModel = new Project('search');
        $projectModel->unsetAttributes();
        $sessionSearchData = user()->getState('projectSearch');
        if(isset($_GET['Project'])){
            user()->setState('projectSearch', $_GET['Project']);
            $sessionSearchData = $_GET['Project'];
        }
        if(isset($_GET['resetSearch'])){
            user()->setState('projectSearch', array());
            $sessionSearchData = array();
            $this->redirect(array('project/index'));
        }
        $projectModel->attributes = $sessionSearchData;

        $dataProvider=$projectModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));
        $dataProvider->sort = array('multiSort'=>true,);

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        }
    }

    public function actionDachi(){
        $_GET['Project']['net_area']['from'] = 30;
        $_GET['Project']['net_area']['till'] = 100;
        if (isset($_GET['page']) || isset($_GET['Project_sort'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        } elseif ($_GET) {
            $this->additionalHeaderTags = '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }

        $projectModel = new Project('search');
        $projectModel->unsetAttributes();
        if(isset($_GET['Project'])){
            user()->setState('projectSearch', $_GET['Project']);
        }
        if(isset($_GET['resetSearch'])){
            user()->setState('projectSearch', array());
            $this->redirect(array('project/index'));
        }
        $projectModel->attributes = user()->getState('projectSearch');
        $projectModel->net_area = array('from'=>30,'till'=>100);

        $dataProvider=$projectModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));
        $dataProvider->sort = array('multiSort'=>true,);

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        }
    }

    public function actionZagorodnie(){
        $_GET['Project']['net_area']['from'] = 100;
        $_GET['Project']['net_area']['till'] = 250;
        if (isset($_GET['page']) || isset($_GET['Project_sort'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        } elseif ($_GET) {
            $this->additionalHeaderTags = '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }

        $projectModel = new Project('search');
        $projectModel->unsetAttributes();
        if(isset($_GET['Project'])){
            user()->setState('projectSearch', $_GET['Project']);
        }
        if(isset($_GET['resetSearch'])){
            user()->setState('projectSearch', array());
            $this->redirect(array('project/index'));
        }
        $projectModel->attributes = user()->getState('projectSearch');
        $projectModel->net_area = array('from'=>100,'till'=>250);

        $dataProvider=$projectModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));
        $dataProvider->sort = array('multiSort'=>true,);

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
                'projectModel' => $projectModel,
            ));
        }
    }

    public function actionView($id){
        $model = $this->loadModel($id);

        if (Yii::app()->request->requestUri != $model->getUrl($_GET)) {
            $this->redirect($model->getUrl(), true, 301);
        }

//        dump($model->getImagesByType('visualization'));
//        foreach($model->getImagesByType('visualization') as $image){
//            var_dump( $image->getImageUrl('thumb') );
//        }

        Yii::app()->seo->setParam('symbol', $model->getName());

        $this->render('view', array(
            'project'=>$model,
			'homeKits'=>HomeKit::model()->findAll(),
//			'packagingHomeKits'=>PackagingHomeKit::model()->order('category')->findAll(),
			'packagingHomeKits'=>PackagingHomeKit::model()->rooted()->order('category')->findAll(),
        ));
    }

    public function actionRecomend($id){
//            throw new CHttpException(404,'The requested page does not exist.');
        $model=new RecomendProjectForm();
        $projectModel = $this->loadModel($id);

        if(app()->request->isAjaxRequest){
            $this->layout = false;
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"><link rel="canonical" href="'.app()->request->hostInfo.$projectModel->getUrl().'" />';
        }

        if(isset($_POST['RecomendProjectForm']))
        {
            $model->attributes=$_POST['RecomendProjectForm'];
            if($model->validate())
            {
                $swift=Yii::app()->swiftMailer;
                $subject=t('Ваш знакомый порекомендовал Вам проект');

                $message=$swift->newMessage($model->email, $subject, $model->body, array('contentType'=>'text/html'));
                $swift->send($message);

                if($model->copy){
                    $subject=t('Вы порекомендовали проект своему знакомому со следующим текстом');

                    $message=$swift->newMessage($model->senderEmail, $subject, $model->body, array('contentType'=>'text/html'));
                    $swift->send($message);
                }

                Yii::app()->user->setFlash('contact','Сообщение было отправлено.');
            }
        }

        $this->render('recomendProject',array('model'=>$model, 'projectModel'=>$projectModel));
    }

    public function actionSendPrice()
    {
        if(!app()->request->isAjaxRequest)
            throw new CHttpException(404,'The requested page does not exist.');
        $model = new ProjectCostForm();
        $message = null;
        if(isset($_POST['ProjectCostForm'])){
            $model->attributes = $_POST['ProjectCostForm'];
            if($model->validate()){
                $model->sendToUser();
                $model->sendToManager();

                $message = 'Ваш запрос принят, мы постараемся выполнить его в ближайшее время';
                $model->email = null;
            }
        }
        $this->renderPartial('_send_cost_form', array('model'=>$model, 'message'=>$message));
    }

    /*
     * @return Project model class
     * */
    public function loadModel($id)
    {
        $model=Project::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
