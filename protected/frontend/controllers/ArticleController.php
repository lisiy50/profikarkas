<?php

class ArticleController extends FrontendController
{
	public $layout='//layouts/column1';

	public function actionView($id)
	{
	    if ($id == 23)
	        $this->redirect(array('/article/zabory'), true, 301);

        $article=$this->loadModel($id);

        if (Yii::app()->request->requestUri != $article->getUrl()) {
            $this->redirect($article->getUrl(), true, 301);
        }

        if($article->menu_id)
            $this->highlightMenu($article->menu_id);
        $this->breadcrumbs=array(
            $article->title
        );

        if($article->getShowSideMenu()){
            $this->layout='//layouts/column2';
            $this->menu_id = $article->menu->id;
        } else {
            $this->layout='//layouts/column1';
        }

        Yii::app()->seo->setParam('title', $article->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

		$this->render(($article->view ? $article->view : 'view'),array(
			'article'=>$article,
		));
	}

	public function actionIndex()
	{
        $this->breadcrumbs=array(
            'Статьи'
        );

        $article=new Article('search');
        $article->unsetAttributes();
        if(isset($_GET['Article']))
            $article->attributes=$_GET['Article'];

        $dataProvider=$article->search();

        Yii::app()->seo->setDefault('title', '{shop_name} - Каталог статей');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionZabory()
    {
        $article=$this->loadModel(23);

        if($article->menu_id)
            $this->highlightMenu($article->menu_id);
        $this->breadcrumbs=array(
            $article->title
        );

        if($article->getShowSideMenu()){
            $this->layout='//layouts/column2';
            $this->menu_id = $article->menu->id;
        } else {
            $this->layout='//layouts/column1';
        }

        Yii::app()->seo->setParam('title', $article->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->render(($article->view ? $article->view : 'view'),array(
            'article'=>$article,
        ));
    }

	public function loadModel($id)
	{
		$model=Article::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
