<?php

class StaffController extends FrontendController
{
	public $layout='//layouts/main';

    public $menu_id = 41;

    public $highlight = 41;

	public function actionIndex()
	{
        if($this->menu_id){
            $this->layout='//layouts/column2';
        }
        $staff=new Staff('search');
        $staff->unsetAttributes();
        if(isset($_GET['Staff']))
            $staff->attributes=$_GET['Staff'];

        $dataProvider=$staff->search();
        $dataProvider->pagination = array(
            'pageSize' => 200,
        );

        Yii::app()->seo->setDefault('title', '{shop_name} - Персонал');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function loadModel($id)
	{
		$model=Staff::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
