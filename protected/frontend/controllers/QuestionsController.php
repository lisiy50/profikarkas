<?php

class QuestionsController extends FrontendController
{
	public $layout='//layouts/main';

    public function actionIndex()
    {
        Yii::app()->seo->setDefault('title', '{shop_name} - Часто задаваемые вопросы');

        $faq = FaqItem::model()->rooted()->findAll();

        $faqItems = $faq[0]->children;
        $iteration = 0;

        $tpl = "<ul id='faqs' class='faq-page__parent'>" . $this->accordion($faqItems, $faq[0], $iteration);

        $this->render('index', array('tpl' => $tpl));
    }

    protected function accordion($items, $parents, $iteration)
    {
        $iteration++;
        $className = '';
        $subClassName = '';
        switch ($iteration) {
            case 1:
                $className = 'parent';
                $subClassName = 'children';
                break;
            case 2:
                $className = 'children';
                $subClassName = 'grandchildren';
                break;
            case 3:
                $className = 'grandchildren';
                $subClassName = 'great-grandchildren';
                break;
            case 4:
                $className = 'great-grandchildren';
                $subClassName = 'great-great-grandchildren';
                break;

        }
        $tpl = '';
        if ($items != null) {
            foreach ($items as $key => $item) {
                $tpl .= "<li class='faq-page__{$className}-item'>";
                $tpl .= "<a class='faq-page__{$className}-item-link'>";
                $tpl .= Yii::app()->language == 'uk' ? $item->name_uk : $item->name_ru;
                $tpl .= "</a>";

                if ($item->hasChildren) {
                    $tpl .= "<ul class='faq-page__{$subClassName}'>";

                    $children = $item->children;
                    $tpl .= $this->accordion($children, $item, $iteration);
                } else {
                    $tpl .= "<ul class='faq-page__{$subClassName}'>";
                    $tpl .= $this->accordion($children = null, $item, $iteration);
                }

                $tpl .= "</li>";
            }
        }
        if (strip_tags($parents->content_ru)) {
            $tpl .= "<li class='faq-page__{$className}-content'>";
            $tpl .= Yii::app()->language == 'uk' ? $parents->content_uk : $parents->content_ru;
            $tpl .= "</li>";
        }
        $tpl .= "</ul>";
        return $tpl;
    }
}

