<?php

class SiteController extends FrontendController
{
    public $layout='//layouts/main';
    public $menu_id;

    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function actionIndex()
    {
        $this->layout='//layouts/main';
        Yii::app()->seo->setDefault('title', '{shop_name}');
        $this->render('index');
    }

    public function actionError()
    {
        if ($error=Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->breadcrumbs=array('Страницы не существует');
                Yii::app()->seo->setDefault('title', '{shop_name} - Страницы не существует');
                $this->render('error', $error);
            }
        }
    }

    public function actionContact()
    {
        $this->menu_id = 41;
        $this->layout = '//layouts/column2';

        Yii::app()->seo->setDefault('title', '{shop_name} - Контакты');
        $this->render('contact');
    }

    public function actionCurrency($id)
    {
        $model = Currency::model()->findByPk((int)$id);
        if ($model === null) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        Yii::app()->currency->setActive($model->id);
        $this->redirect(isset($_GET['returnUrl']) ? $_GET['returnUrl'] : array('site/index'));
    }

    public function actionLogin()
    {
        if (! (strstr(app()->request->urlReferrer, 'site/login') || strstr(app()->request->urlReferrer, 'site/ulogin'))) {
            user()->setState('returnUrl', app()->request->urlReferrer);
        }

        $login = new LoginForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($login);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $login->attributes=$_POST['LoginForm'];

            if ($login->validate() && $login->login()) {
                if (user()->hasState('returnUrl')) {
                    $this->redirect(user()->getState('returnUrl'));
                } else {
                    $this->redirect(app()->user->returnUrl);
                }
            }
        }

        if (! user()->isGuest) {
            if (user()->hasState('returnUrl')) {
                $this->redirect(user()->getState('returnUrl'));
            } else {
                $this->redirect(app()->user->returnUrl);
            }
        }

        $this->breadcrumbs=array('Авторизация');
        Yii::app()->seo->setDefault('title', '{shop_name} - Авторизация');
        $this->render('login',array('login'=>$login,));
    }

    public function actionUlogin()
    {
        if (! (strstr(app()->request->urlReferrer, 'site/login') || strstr(app()->request->urlReferrer, 'site/ulogin'))) {
            user()->setState('returnUrl', app()->request->urlReferrer);
        }

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] .
            '&host=' . $_SERVER['HTTP_HOST']);
        $response = json_decode($s, true);

        $nickname = $response['nickname'];
        $email = $response['email'];
        $network = $response['network']; // social network
        $last_name = $response['last_name'];
        $first_name = $response['first_name'];
        $identity = $response['identity']; // ?? same as profile
        $profile = $response['profile']; // user link of social network
        $verified_email = $response['verified_email']; // ??
        $uid = $response['uid']; // social network user ID
        $password = $network.$uid;

        $model = User::model()->find('username = :email OR email = :email', array(':email' => $email));
        if (!$model) {
            $model = new User();
            $model->setScenario('register');

            $model->username = $email;
            $model->role = User::ROLE_CLIENT;
            $model->email = $email;
            $model->fio = $first_name . ' ' . $last_name;
        } else {
            $model->setScenario('ulogin');
        }
        $model->password = $password;
        $model->rPassword = $password;
        if ($model->save()) {
            Yii::app()->user->setFlash('success', t('Пользователь добавлен'));
            $this->onUserCreate(new CEvent($model));

            $login=new LoginForm();
            $login->username = $model->username;
            $login->password = $password;
            $login->rememberMe = 1;

            if($login->validate() && $login->login()) {
                if (user()->hasState('returnUrl')) {
                    $this->redirect(user()->getState('returnUrl'));
                } else {
                    $this->redirect(app()->user->returnUrl);
                }
            }
        }
        if (user()->hasState('returnUrl')) {
            $this->redirect(user()->getState('returnUrl'));
        } else {
            $this->redirect(app()->user->returnUrl);
        }

    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionMap()
    {
        Yii::app()->seo->setDefault('title', '{shop_name} - Карта сайта');
        $this->render('map');
    }

    public function actionRecall()
    {
        $model = new RecallForm();

        if (isset($_POST['RecallForm'])) {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];

            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['RecallForm'];

            if ($model->validate() && $checkCaptcha) {
                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} заказал обратный звонок";
                $data = ZohoHelper::sendData($model);

                Yii::app()->user->setFlash('success', 'Спасибо за обращение в нашу компанию, наш менеджер свяжется с вами как можно скорее.');
            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('recall', array('model' => $model));
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"/>';
            $this->render('recall', array('model' => $model));
        }
    }

    public function actionSocialpopup()
    {
        if (isset($_COOKIE['debug'])) {
            return;
        }
        $this->renderPartial('social_popup');
    }

    public function actionAdvantagepopup()
    {
        $this->renderPartial('advantage_popup');
    }

    public function actionSignUpForViewing()
    {
        $model = new SignUpForViewing();

        if (isset($_POST['SignUpForViewing'])) {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['SignUpForViewing'];
            if ($model->validate() && $checkCaptcha) {
                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} записался на просмотр";
                $data = ZohoHelper::sendData($model);

                Yii::app()->user->setFlash('success','Спасибо за обращение в нашу компанию, наш менеджер свяжется с вами как можно скорее.');
            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('sign_up_for_viewing', array('model' => $model));
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"/>';
            $this->render('sign_up_for_viewing', array('model' => $model));
        }
    }

    public function actionSignUpForConsultation()
    {
        $model = new SignUpForConsultation();

        if (isset($_POST['SignUpForConsultation'])) {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];

            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['SignUpForConsultation'];
            if ($model->validate() && $checkCaptcha) {
                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} записался на бесплатную консультацию";
                $data = ZohoHelper::sendData($model);

                Yii::app()->user->setFlash('success','Спасибо за обращение в нашу компанию, наш менеджер свяжется с вами как можно скорее.');
            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('sign_up_for_consultation', array('model'=>$model));
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"/>';
            $this->render('sign_up_for_consultation', array('model'=>$model));
        }
    }

    public function actionSignUpForDemoHouse()
    {
        $model = new SignUpForDemoHouse();

        if (isset($_POST['SignUpForDemoHouse'])) {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['SignUpForDemoHouse'];
            if ($model->validate() && $checkCaptcha) {
                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} отправил сообщение из: {$model->subject}";
                $data = ZohoHelper::sendData($model);

                Yii::app()->user->setFlash('success', '<span>' . $model->name . '</span>, <br> cпасибо за обращение в нашу компанию,<br> наш менеджер свяжется с вами как можно скорее.');

            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('sign_up_for_demo_house', array('model' => $model));
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"/>';
            $this->render('sign_up_for_demo_house', array('model' => $model));
        }
    }

    public function actionCostCalculation()
    {
        $model = new CostCalculationForm();

        if (isset($_POST['CostCalculationForm'])) {
            $model->attributes = $_POST['CostCalculationForm'];
            if ($model->validate()) {
//                $swift = Yii::app()->swiftMailer;
//                $subject = t('Заявка на расчет стоимости');
//
//                $body = 'От: '.$model->name.'<br/>';
//                $body .= 'Телефон: '.$model->phone.'<br/>';
//                $body .= 'Email: '.$model->email.'<br/>';
//                $body .= 'Со страницы: '.Yii::app()->request->urlReferrer.'<br/>';
//
//                $message = $swift->newMessage(config('valeriisydorov@gmail.com'), $subject, $body, array('contentType'=>'text/html'));
//                /* @var Swift_Message $message */
//
//                $file = CUploadedFile::getInstance($model, 'file');
//                if ($file) {
//                    $FilePath = Yii::app()->basePath . '/../storage/tmp/';
//                    $file->saveAs($FilePath . $file->name);
//                    $message->attach(Swift_Attachment::fromPath($FilePath . $file->name));
//                }
//
//                $swift->send($message);

                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} отправил заявку на расчет стоимости";
                $data = ZohoHelper::sendData($model);

//                if ($file) {
//                    unlink($FilePath . $file->name);
//                }

                Yii::app()->user->setFlash('success', "Спасибо за обращение в нашу компанию, наш менеджер свяжется с вами как можно скорее.");
            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('_cost_calculation', array('model' => $model));
        } else {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, nofollow"/>';
            $this->render('_cost_calculation',array('model'=>$model));
        }
    }

    /**
     * @param CEvent $event
     * @throws CException
     * rise MailEvent::onUserCreate() event
     */
    public function onUserCreate(CEvent $event)
    {
        $this->raiseEvent('onUserCreate', $event);
    }
}
