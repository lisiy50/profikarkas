<?php

class UserController extends FrontendController
{
	public $layout='//layouts/main';

	public function actionView($id)
	{
        $user=$this->loadModel($id);

        Yii::app()->seo->setDefault('title', t('Личный кабинет пользователя').' '.$user->username);

		$this->render('view', array(
			'user'=>$user,
		));
	}

    public function actionCreate(){
        if (!user()->isGuest)
            $this->redirect(['/site/index']);

        $model = new User('register');

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['User'];
            $model->email = $model->username;
            $model->role = User::ROLE_CLIENT;
            if ($model->save() && $checkCaptcha) {
                Yii::app()->user->setFlash('success', t('Пользователь добавлен'));
                $this->onUserCreate(new CEvent($model));
            } else {
                Yii::app()->user->setFlash('error', t('Пользователь не добавлен'));
            }
        } else if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }

        Yii::app()->seo->setDefault('title', '{shop_name} - '.t('Регистрация'));

        if(Yii::app()->request->isAjaxRequest){
            $this->renderPartial('create_form', array(
                'model' => $model,
            ));
        } else {
            $this->render('create_form', array(
                'model' => $model,
            ));
        }
    }

    public function actionProfile(){
        $user = $this->loadModel(user()->id);
        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            if ($user->save()) {
                Yii::app()->user->setFlash('success', t('Данные сохранены'));
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', t('Данные не сохранены'));
            }
        } else if (isset($_GET['User'])) {
            $user->attributes = $_GET['User'];
        }
        app()->seo->setDefault('title', '{shop_name} - '.t('Личный кабинет пользоватея') . ' - ' . t('Настройки профиля'));
        $this->render('profile', array('user'=>$user));
    }

    public function actionChangePassword(){
        $user = $this->loadModel(user()->id);
        $user->setScenario('changePassword');
        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->save()) {
                Yii::app()->user->setFlash('success', t('Пароль успешно изменен'));
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', t('Пароль не изменен'));
            }
        }
        app()->seo->setDefault('title', '{shop_name} - '.t('Личный кабинет пользоватея') . ' - ' . t('Изменить пароль'));
        $this->render('change_password', array('user'=>$user));
    }

    public function actionRemindPassword(){
        $remindPassword = new RemindPasswordForm();

        if(isset($_POST['RemindPasswordForm'])){
            $remindPassword->attributes = $_POST['RemindPasswordForm'];
            if($remindPassword->validate()){
                $this->onRemindPassword(new CEvent($remindPassword));
                user()->setFlash('success', t('Ссылка для изменения пароля была выслана на Вашу почту.'));
            }
        }
        app()->seo->setDefault('title', '{shop_name} - '.t('Восстановление пароля'));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('remind_password', array('remindPassword' => $remindPassword));
        }
        else {
            $this->render('remind_password', array('remindPassword' => $remindPassword));
        }
    }

    public function actionRestorePassword($key)
    {
        $user = User::getUserByKey($key);
        if($user){
            $user->setScenario('restorePassword');
            if(isset($_POST['User'])){
                $user->attributes = $_POST['User'];
                if($user->save()){
                    user()->setFlash('success', t('Пароль восстановлен.'));
                }
            }
        } else {
            user()->setFlash('error', t('Ссылка восстановления пароля не действительна'));
        }
        $this->render('restore_password', array('user'=>$user));
    }

    /**
     * @param $id
     * @return User
     * @throws CHttpException
     */
	public function loadModel($id)
	{
		$model=User::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === get_class($model).'-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param CEvent $event
     * @throws CException
     * rise MailEvent::onRemindPassword() event
     */
    public function onRemindPassword(CEvent $event) {
        $this->raiseEvent('onRemindPassword', $event);
    }


    /**
     * @param CEvent $event
     * @throws CException
     * rise MailEvent::onUserCreate() event
     */
    public function onUserCreate(CEvent $event) {
        $this->raiseEvent('onUserCreate', $event);
    }
}
