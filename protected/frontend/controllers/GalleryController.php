<?php

class GalleryController extends FrontendController
{
    public $layout='//layouts/main';
    public $highlight = 31;

    public function actionView($id)
    {
        $model=$this->loadModel($id);
        if(!isset($_GET['uri']))
            $this->redirect($model->getUrl(), true, 301);
        $this->menu_id = $model->menu_id;

        if($this->menu_id){
            $this->layout='//layouts/column2';
        }

        Yii::app()->seo->setParam('title', $model->name);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->render('view',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=Gallery::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
