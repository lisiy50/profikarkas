<?php

class PhotogalleryController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 88;

    public $highlight = 88;

    public function actionIndex(){
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $photoGalleryModel = new PhotoGallery('search');
        if(isset($_GET['PhotoGallery']))
            $photoGalleryModel->attributes = $_GET['PhotoGallery'];

        $dataProvider=$photoGalleryModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    public function actionView($id){
        $model = $this->loadModel($id);
        if (Yii::app()->request->requestUri != $model->getUrl()) {
            $this->redirect($model->getUrl(), true, 301);
        }

        Yii::app()->seo->setParam('project_name', $model->project_name);

        $this->render('view', array('model'=>$model));
    }

    /*
     * @return PhotoGallery model class
     * */
    public function loadModel($id)
    {
        $model=PhotoGallery::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
