<?php
class NewsController extends FrontendController {

    public $layout='//layouts/column2';

    public $menu_id = 41;
    public $highlight = 84;

    public function actionView($id) {
        $news=$this->loadModel($id);
        if(!isset($_GET['uri']))
            $this->redirect($news->getUrl(), true, 301);

        Yii::app()->seo->setParams(array(
            'title' => $news->title,
        ));

        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->render('view',array(
            'news'=>$news,
		));
    }

    public function actionArchive() {
        $news=new News('search');
        $news->unsetAttributes();
        if(isset($_GET['News']))
            $news->attributes=$_GET['News'];

        $dataProvider=$news->search();
        $criteria=$dataProvider->getCriteria();
        $criteria->addCondition('visibility='.News::VISIBILITY_ARCHIVE);

        $dataProvider->setCriteria($criteria);
        $dataProvider->setPagination(array(
            'pageSize'=>Yii::app()->config['news_catalog_limit'],
        ));
        $dataProvider->setSort(array(
            'sortVar'=>'sort',
        ));

        Yii::app()->seo->setDefault('title', '{shop_name} - Архив новостей');

        $this->render('archive',array(
            'dataProvider'=>$dataProvider,
		));
    }

    public function actionRight(){
        $subscriber = new Subscriber();

        if(isset($_POST['Subscriber'])){
            $subscriber->attributes = $_POST['Subscriber'];
            if($subscriber->save()){
                $swift=Yii::app()->swiftMailer;
                $subject=t('Вы подписались на рассылку новостей на сайте Z500proekty.ru');
                $confirmUrl = Yii::app()->createAbsoluteUrl('news/confirm', array('hash'=>base64_encode($subscriber->email)));
                $body = '
                <p>Поздравляем!</p>
                <p>Вы успешно оформили подписку на новости компании "Профикаркас" на сайте&nbsp;<a href="http://z500proekty.ru/">profikarkas.com.ua</a></p>
                <p>Чтобы активировать подписку перейдите, пожалуйста, по ссылке</p>
                <p><a href="'.$confirmUrl.'">'.$confirmUrl.'</a></p>
                <p><b>Команда Профикаркас</b></p>
                <p>Для получения дополнительной информации звоните '.config('contact_phone').' или пишите&nbsp;<a href="mailto:info@profikarkas.com.ua">info@profikarkas.com.ua</a></p>
                <p>Skype: profikarkas — мы всегда вам рады!</p>
                <p>Если вы не оформляли подписку на новости, пожалуйста, сообщите нам об этом и не переходите по указанной ссылке.</p>
                ';
                $message=$swift->newMessage($subscriber->email, $subject, $body, array('contentType'=>'text/html'));
                $swift->send($message);

                user()->setFlash('success', 'Вы успешно подписались на рассылку новостей.<br>Для активации рассылки перейдите по ссылке, отправленной на указанный e-mail.');
            }
        }

        $this->renderPartial('//news/_right', array('subscriber'=>$subscriber));
    }

    public function actionConfirm($hash){
        $email = trim(base64_decode($hash));
        $subscriber = Subscriber::model()->find("email LIKE \"%{$email}%\"");
        if(!$subscriber)
            throw new CHttpException(404,'The requested page does not exist.');
        $subscriber->status = Subscriber::STATUS_ENABLED;
        $subscriber->save();
        $this->render('confirm');
    }

    public function loadModel($id)
   	{
   		$model=News::model()->findByPk((int)$id);
   		if($model===null)
   			throw new CHttpException(404,'The requested page does not exist.');
   		return $model;
   	}
}