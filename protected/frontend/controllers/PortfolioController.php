<?php

class PortfolioController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 29;

    public $highlight = 29;

    public function actionIndex(){
        if (isset($_GET['page'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $portfolioModel = new Portfolio('search');
        if(isset($_GET['Portfolio']))
            $portfolioModel->attributes = $_GET['Portfolio'];
        $portfolioModel->construction = Portfolio::STATUS_DISABLED;

        $dataProvider=$portfolioModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    public function actionConstruction(){
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $portfolioModel = new Portfolio('search');
        if(isset($_GET['Portfolio']))
            $portfolioModel->attributes = $_GET['Portfolio'];
        $portfolioModel->construction = Portfolio::STATUS_ENABLED;

        $dataProvider=$portfolioModel->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    public function actionView($id){
        $model = $this->loadModel($id);
        if (Yii::app()->request->requestUri != $model->getUrl()) {
            $this->redirect($model->getUrl(), true, 301);
        }

        Yii::app()->seo->setParam('project_name', $model->project_name);

        $this->render('view', array('model'=>$model));
    }

    /**
     * @param $id
     * @return Portfolio model class
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Portfolio::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
