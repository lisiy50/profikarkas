<?php

class UsefularticleController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 31;
    public $highlight = 31;

    public function actionView($id){
        $this->highlight = 31;
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = $this->loadModel($id);
        if(!isset($_GET['uri']))
            $this->redirect($model->getUrl(), true, 301);

        Yii::app()->seo->setParam('title', $model->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->render('view', array('model'=>$model));
    }

    /*
     * @return UsefulArticle model class
     * */
    public function loadModel($id)
    {
        $model=UsefulArticle::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex(){
        if (isset($_GET['page'])) {
            $this->additionalHeaderTags = '<meta name="robots" content="noindex, follow"/>';
            $this->additionalHeaderTags .= '<link rel="canonical" href="'.app()->createAbsoluteUrl(app()->request->pathInfo).'" />';
        }
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = new UsefulArticle('search');
        if(isset($_GET['UsefulArticle']))
            $model->attributes = $_GET['UsefulArticle'];

        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        $criteria = $dataProvider->getCriteria();
        $criteria->scopes = 'usefulArticles';

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }
}
