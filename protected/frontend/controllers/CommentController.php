<?php
class CommentController extends FrontendController {

    public function actionAddComment() {
        $comment=new Comment();

        $comment->attributes=$_POST['Comment'];
        $comment->file1 = CUploadedFile::getInstance($comment,'file1');
        $comment->file2 = CUploadedFile::getInstance($comment,'file2');
        $comment->file3 = CUploadedFile::getInstance($comment,'file3');
        $comment->user_id = user()->id;

        if ($comment->validate()) {
            if ($comment->file1 instanceof CUploadedFile) {
                $fileName = time().'_file1_'.Yii::app()->translitFormatter->formatFileName($comment->file1->getName()).'.'.$comment->file1->extensionName;
                $comment->file1->saveAs(Yii::app()->basePath.'/../storage/comment_files/'.$fileName);
                $comment->file1 = $fileName;
            }
            if ($comment->file2 instanceof CUploadedFile) {
                $fileName = time().'_file1_'.Yii::app()->translitFormatter->formatFileName($comment->file2->getName()).'.'.$comment->file2->extensionName;
                $comment->file2->saveAs(Yii::app()->basePath.'/../storage/comment_files/'.$fileName);
                $comment->file2 = $fileName;
            }
            if ($comment->file3 instanceof CUploadedFile) {
                $fileName = time().'_file1_'.Yii::app()->translitFormatter->formatFileName($comment->file3->getName()).'.'.$comment->file3->extensionName;
                $comment->file3->saveAs(Yii::app()->basePath.'/../storage/comment_files/'.$fileName);
                $comment->file3 = $fileName;
            }

            if($comment->save()) {
                $this->onCreateComment(new CEvent($comment));

                if ($comment->parent_id) {
                    $this->onCommentReply(new CEvent($comment));
                }

                user()->setFlash('error', 'Комментарий успешно добавлен');
                $comment->unsetAttributes(array('body'));
            } else {
                user()->setFlash('error', 'Комментарий не добавлен');
            }
        } else {
            user()->setFlash('error', 'Комментарий не добавлен');
        }
        $this->widget('CommentsWidget', array('owner' => $comment->ownerModel, 'comment' => $comment));
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if (Yii::app()->request->isAjaxRequest && (user()->getId() == $model->user_id || user()->getRole() == User::ROLE_CONTENT || user()->getRole() == User::ROLE_ADMIN)) {
            foreach ($model->children as $child) {
                $child->delete();
            }
            $model->delete();
            return true;
        }
        throw new CHttpException('The requested page does not exist.');
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if (Yii::app()->request->isAjaxRequest && (user()->getId() == $model->user_id || user()->getRole() == User::ROLE_CONTENT || user()->getRole() == User::ROLE_ADMIN)) {
            if (array_key_exists('body', $_POST)) {
                $model->body = $_POST['body'];
                $model->save();
            }
            $this->render('_update_form', array('model' => $model));
        }
    }

    /**
     * @param $id
     * @return Comment
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Comment::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function onCreateComment(CEvent $event) {
        $this->raiseEvent('onCreateComment', $event);
    }

    public function onCommentReply(CEvent $event) {
        $this->raiseEvent('onCommentReply', $event);
    }
}
