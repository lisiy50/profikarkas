<?php

/**
 * Class OneCController
 */
class OneCController extends FrontendController {

    public function actionImportHomeKitPrice()
    {
        $data = file_get_contents("php://input");

//        $dump='$_GET = '.CVarDumper::dumpAsString($_GET)."\n";
//        $dump.='$_POST = '.CVarDumper::dumpAsString($_POST)."\n";
//        $dump.='$_FILES = '.CVarDumper::dumpAsString($_FILES)."\n";
//        $dump.='php input'.$data;
//        Yii::log($dump, CLogger::LEVEL_INFO, 'info.oneC.importHomeKitPrice');

        $model = new HomeKitPrice();
        $model->setData($data);

        $sql = 'UPDATE tbl_config SET price_notice_default_1=:price_notice_default_1 WHERE id=:id';
        $parameters = array(':price_notice_default_1' => date('d.m.Y'), ':id' => 1);
        Yii::app()->db->createCommand($sql)->execute($parameters);

        if ($this->actionSetHomeKitPrice()) {
            echo json_encode(array('success' => true, 'message' => 'Success'));
        }
    }

    public function actionSetHomeKitPrice()
    {
        $model = new HomeKitPrice();
        $data = $model->getData();

        foreach ($data as $item) {
            $home_kit = $item[$model::ONE_C_HOME_KIT];
            $work_scope = $item[$model::ONE_C_WORK_SCOPE];
            $name = strtolower(trim($item[$model::ONE_C_NAME]) . trim($item[$model::ONE_C_VARIANT]));
            $price = $item[$model::ONE_C_PRICE];

            if (array_key_exists($home_kit, $model::getHomeKitsIDs())
                && array_key_exists($work_scope, $model::getWorkScopesIDs())) {
                $home_kit_id = $model::getHomeKitsIDs()[$home_kit];
                $work_scope_id = $model::getWorkScopesIDs()[$work_scope];
            } else {
                continue;
            }

            if ($model::isProject($name)) {
                $this->updateOrCreateRecord($home_kit_id, $work_scope_id, $model::getProjectId($name), $price);
            }
        }
    }

    private function getRecord($home_kit_id, $work_scope_id, $project_id)
    {
        $rec = WorkScopePrice::model()->findByPk(array(
            'home_kit_id' => $home_kit_id,
            'work_scope_id' => $work_scope_id,
            'project_id' => $project_id
        ));
        return $rec;
    }

    private function updateOrCreateRecord($home_kit_id, $work_scope_id, $project_id, $price)
    {
        $rec = $this->getRecord($home_kit_id, $work_scope_id, $project_id);

        if ($rec) {
            $rec->price = $price;
            $rec->save();
        } else {
            $row = new WorkScopePrice;

            $row->home_kit_id = $home_kit_id;
            $row->work_scope_id = $work_scope_id;
            $row->project_id = $project_id;
            $row->price = $price;
            $row->save();
        }
    }
}
