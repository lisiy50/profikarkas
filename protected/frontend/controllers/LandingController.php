<?php
class LandingController extends CController {

    public $layout = false;

    public function actionIndex()
    {
        Yii::app()->language = isset($_GET['lang']) ? $_GET['lang'] : 'ru';

        $this->layout = false;

        Yii::app()->seo->setDefault('title', t('Строительство каркасных домов под ключ в Киеве ProfiKarkas'));
        Yii::app()->seo->setDefault('description', t('Каркасное строительство домов по немецкой технологии - строительная компания ProfiKarkas'));

        $reviews = Review::model()->order('position ASC')->findAll('show_on_landing = 1 AND status = 1');

//        app()->clientScript->registerPackage('landing');

        $this->render('index', array('reviews' => $reviews));
    }

    public function actionEn()
    {
        $this->layout = false;
        $projects = Project::model()->findAllByAttributes(['id' => [105, 141, 129, 145, 111]]);

        $landingId = 4;
        $baseInformation = Landing::model()->findByPk($landingId);
        $rawTextInformation = LandingText::model()->findAll('landing_id=:landing_id', [':landing_id' => $landingId]);
        $textInformation = [];
        foreach ($rawTextInformation as $rawTextInformationItem) {
            $textInformation[$rawTextInformationItem->id] = ['text_ru' => $rawTextInformationItem->text_ru];
        }
        Yii::app()->seo->setDefault('description', $baseInformation->meta_description_ru);

        $this->render('international_en', [
            'projects' => $projects,
            'baseInformation' => $baseInformation,
            'textInformation' => $textInformation,
        ]);
    }

    public function actionDe()
    {
        $this->layout = false;

        $landingId = 6;
        $baseInformation = Landing::model()->findByPk($landingId);
        $rawTextInformation = LandingText::model()->findAll('landing_id=:landing_id', [':landing_id' => $landingId]);
        $textInformation = [];
        foreach ($rawTextInformation as $rawTextInformationItem) {
            $textInformation[$rawTextInformationItem->id] = ['text_ru' => $rawTextInformationItem->text_ru];
        }
        Yii::app()->seo->setDefault('description', $baseInformation->meta_description_ru);

        $this->render('international_de', [
            'baseInformation' => $baseInformation,
            'textInformation' => $textInformation,
        ]);
    }

    public function actionFr()
    {
        $this->layout = false;

        $landingId = 5;
        $baseInformation = Landing::model()->findByPk($landingId);
        $rawTextInformation = LandingText::model()->findAll('landing_id=:landing_id', [':landing_id' => $landingId]);
        $textInformation = [];
        foreach ($rawTextInformation as $rawTextInformationItem) {
            $textInformation[$rawTextInformationItem->id] = ['text_ru' => $rawTextInformationItem->text_ru];
        }
        Yii::app()->seo->setDefault('description', $baseInformation->meta_description_ru);

        $this->render('international_fr', [
            'baseInformation' => $baseInformation,
            'textInformation' => $textInformation,
        ]);
    }

    public function actionHotels()
    {
        $this->layout = 'landing';
        $landingId = 1;
        $baseInformation = Landing::model()->findByPk($landingId);
        $rawTextInformation = LandingText::model()->findAll('landing_id=:landing_id', [':landing_id' => $landingId]);
        $textInformation = [];
        foreach ($rawTextInformation as $rawTextInformationItem) {
            $textInformation[$rawTextInformationItem->id] = ['text_ru' => $rawTextInformationItem->text_ru, 'text_ua' => $rawTextInformationItem->text_ua];
        }
        Yii::app()->seo->setDefault('title', $baseInformation->meta_title_ru);
        Yii::app()->seo->setDefault('description', $baseInformation->meta_description_ru);
        $this->render('hotels', [
            'baseInformation' => $baseInformation,
            'textInformation' => $textInformation,
        ]);
    }

    public function actionHotelsForm() {
        if (! app()->request->isAjaxRequest) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model = new LandingHotelsForm();

        if(isset($_POST['LandingHotelsForm'])) {
            $model->attributes = $_POST['LandingHotelsForm'];
//            $model->subject = "На сайте profikarkas.com.ua (English landing page) пользователь {$model->name}, email: {$model->email}, страна: {$model->country}, город: {$model->city} отправил сообщение из формы.";

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success == true) {
                    $checkCaptcha = true;
                } else {
                    $checkCaptcha = false;
                }
            }

            if ($model->validate() && $checkCaptcha) {

                $swift = Yii::app()->swiftMailer;
                $subject = t('Страница коммерческой недвижимости profikarkas.com.ua');

                $body = 'From: '.$model->name . '<br/>';
                $body .= 'Phone: '.$model->phone . '<br/>';
                $body .= 'Email: '.$model->email.'<br/>';
                $body .= 'From page: '.Yii::app()->request->urlReferrer.'<br/>';
                $body .= 'Message: ' . $model->text . '<br/>';

                $message = $swift->newMessage(config('contact_email'), $subject, $body, ['contentType'=>'text/html']);
                /* @var Swift_Message $message */

                $file = CUploadedFile::getInstance($model, 'file');
                if ($file) {
                    $FilePath = Yii::app()->basePath . '/../storage/tmp/';
                    $document = $FilePath . $file->name;
                    $file->saveAs($document);
                    $message->attach(Swift_Attachment::fromPath($FilePath . $file->name));
                    $this->sendAjaxFormData($model->name, $model->email, $model->phone, $model->text, '', '', 'https://profikarkas.com.ua/hotels', $document);
                } else {
                    $this->sendAjaxFormData($model->name, $model->email, $model->phone, $model->text, '', '', 'https://profikarkas.com.ua/hotels');
                }
                $swift->send($message);
                if($file) {
                    unlink($FilePath . $file->name);
                }

                Yii::app()->user->setFlash('success', 'Спасибо за обращение. Мендежеры свяжутся с Вами в ближайшее время.');
            }
        }

        if (app()->request->isAjaxRequest) {
            $this->renderPartial('_form_hotels', ['model' => $model]);
        }
    }

    public function actionAjaxEnForm() {
        if (! app()->request->isAjaxRequest) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model = new LandingEnForm();

        if(isset($_POST['LandingEnForm'])) {
            $model->attributes = $_POST['LandingEnForm'];
//            $model->subject = "На сайте profikarkas.com.ua (English landing page) пользователь {$model->name}, email: {$model->email}, страна: {$model->country}, город: {$model->city} отправил сообщение из формы.";

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            if ($model->validate() && $checkCaptcha) {


                $swift=Yii::app()->swiftMailer;
                $subject=t('English landing page profikarkas.com.ua');

                $body = 'From: '.$model->name.'<br/>';
                $body .= 'Country: '.$model->country.'<br/>';
                $body .= 'City: '.$model->city.'<br/>';
                $body .= 'Email: '.$model->email.'<br/>';
                $body .= 'From page: '.Yii::app()->request->urlReferrer.'<br/>';
                $body .= 'Message: ' . $model->text . '<br/>';

                $message=$swift->newMessage(config('contact_email'), $subject, $body, array('contentType'=>'text/html'));
                /* @var Swift_Message $message */

                $file = CUploadedFile::getInstance($model, 'file');
                if($file) {
                    $FilePath = Yii::app()->basePath.'/../storage/tmp/';
                    $document = $FilePath . $file->name;
                    $file->saveAs($document);
                    $message->attach(Swift_Attachment::fromPath($FilePath.$file->name));
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/en', $document);
                } else {
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/en');
                }
                $swift->send($message);
                if($file) {
                    unlink($FilePath . $file->name);
                }

//                $data = ZohoHelper::sendData($model);
                Yii::app()->user->setFlash('success','Thank You. We\'ll contact you shortly.');
            }
        }

        if(app()->request->isAjaxRequest){
            $this->renderPartial('_form_en', array('model' => $model));
        }
    }

    public function actionAjaxDeForm() {
        if(!app()->request->isAjaxRequest){
            throw new CHttpException(404,'The requested page does not exist.');
        }

        $model=new LandingDeForm();

        if(isset($_POST['LandingDeForm']))
        {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes=$_POST['LandingDeForm'];

            if($model->validate() && $checkCaptcha)
            {

                $swift=Yii::app()->swiftMailer;
                $subject=t('German landing page profikarkas.com.ua');

                $body = 'From: '.$model->name.'<br/>';
                $body .= 'Country: '.$model->country.'<br/>';
                $body .= 'City: '.$model->city.'<br/>';
                $body .= 'Email: '.$model->email.'<br/>';
                $body .= 'From page: '.Yii::app()->request->urlReferrer.'<br/>';
                $body .= 'Message: ' . $model->text . '<br/>';

                $message=$swift->newMessage(config('contact_email'), $subject, $body, array('contentType'=>'text/html'));
                /* @var Swift_Message $message */

                $file = CUploadedFile::getInstance($model, 'file');
                if($file) {
                    $FilePath = Yii::app()->basePath.'/../storage/tmp/';
                    $document = $FilePath . $file->name;
                    $file->saveAs($document);
                    $message->attach(Swift_Attachment::fromPath($FilePath.$file->name));
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/de', $document);
                } else {
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/de');
                }

                $swift->send($message);
                if($file) {
                    unlink($FilePath . $file->name);
                }

                Yii::app()->user->setFlash('success','Vielen Dank für die Anfrage! In nächster Zeit werden wir uns mit Ihnen in Kontakt setzen.');
            }
        }

        if(app()->request->isAjaxRequest){
            $this->renderPartial('_form_de',array('model'=>$model));
        }
    }

    public function actionAjaxFrForm() {
        if(!app()->request->isAjaxRequest){
            throw new CHttpException(404,'The requested page does not exist.');
        }

        $model=new LandingFrForm();

        if(isset($_POST['LandingFrForm']))
        {
            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes=$_POST['LandingFrForm'];
            if($model->validate() && $checkCaptcha)
            {

                $swift=Yii::app()->swiftMailer;
                $subject=t('French landing page profikarkas.com.ua');

                $body = 'From: '.$model->name.'<br/>';
                $body .= 'Country: '.$model->country.'<br/>';
                $body .= 'City: '.$model->city.'<br/>';
                $body .= 'Email: '.$model->email.'<br/>';
                $body .= 'From page: '.Yii::app()->request->urlReferrer.'<br/>';
                $body .= 'Message: ' . $model->text . '<br/>';

                $message=$swift->newMessage(config('contact_email'), $subject, $body, array('contentType'=>'text/html'));
                /* @var Swift_Message $message */

                $file = CUploadedFile::getInstance($model, 'file');
                if($file) {
                    $FilePath = Yii::app()->basePath.'/../storage/tmp/';
                    $document = $FilePath . $file->name;
                    $file->saveAs($document);
                    $message->attach(Swift_Attachment::fromPath($FilePath.$file->name));
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/fr', $document);
                } else {
                    $this->sendAjaxFormData($model->name, $model->email, '', $model->text, $model->country, $model->city, 'https://profikarkas.com.ua/fr');
                }

                $swift->send($message);
                if($file) {
                    unlink($FilePath . $file->name);
                }

                Yii::app()->user->setFlash('success','Merci de nous contacter! Nous vous contacterons bientôt.');
            }
        }

        if(app()->request->isAjaxRequest){
            $this->renderPartial('_form_fr', array('model' => $model));
        }
    }

    public function actionAjaxProjects()
    {
        $model = new Project('search');
        $model->unsetAttributes();
        if (isset($_GET['Project'])) {
            $model->attributes = $_GET['Project'];
        }
        $model->show_on_landing = 1;
        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 100,
            'pageVar' => 'page',
        ));
        $sort = $dataProvider->getSort();
        $sort->defaultOrder = array('priority' => CSort::SORT_ASC);
        $dataProvider->setSort($sort);

        $itemWidth = isset($_GET['item_width']) ? $_GET['item_width'] : null;

        $this->renderPartial('ajax-projects', array(
            'dataProvider' => $dataProvider,
            'model' => $model,
            'itemWidth' => $itemWidth,
        ));
    }

    public function actionUpdateCss()
    {
        if (isset($_POST['css'])) {
            file_put_contents(Yii::app()->basePath.DS.'..'.DS.'main.css', $_POST['css']);
            file_put_contents(Yii::app()->basePath.DS.'..'.DS.'main_backup.css', $_POST['css']);
            $this->refresh();
        }
        $css = file_get_contents(Yii::app()->basePath.DS.'..'.DS.'main.css');
        $this->render('update-css', array('css' => $css));
    }

    public function actionRecall()
    {
        $model = new RecallForm;

        if (isset($_POST['RecallForm'])) {

            if (isset($_POST['g-recaptcha-response'])) {
                $captcha = $_POST['g-recaptcha-response'];
            } else {
                $captcha = false;
            }

            if ($captcha) {
                $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                $response = file_get_contents(
                    "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
                );
                $response = json_decode($response);

                if ($response->success === false) {
                    $checkCaptcha = false;
                } else {
                    $checkCaptcha = true;
                }
            }

            $model->attributes = $_POST['RecallForm'];

            if ($model->validate() && $checkCaptcha)
            {
                $model->subject = "На сайте profikarkas.com.ua пользователь {$model->name} заказал обратный звонок";
                ZohoHelper::sendData($model);

                Yii::app()->user->setFlash('success', 'Спасибо за обращение в нашу компанию, наш менеджер свяжется с вами как можно скорее.');
            }
        }

        $this->renderPartial('recall', array('model' => $model), false, false);

    }

    public function actionAjaxContact()
    {
        $msg = 'Заполните поля формы';
        $result = array('error' => true, 'message' => $msg);
        $model = new RecallForm;

        if (Yii::app()->request->isAjaxRequest) {
            if (! empty($_POST)) {
                if (isset($_POST['g-recaptcha-response'])) {
                    $captcha = $_POST['g-recaptcha-response'];
                } else {
                    $captcha = false;
                }

                if ($captcha) {
                    $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
                    $response = json_decode($response);

                    if ($response->success === false) {
                        $checkCaptcha = false;
                    } else {
                        $checkCaptcha = true;
                    }
                }
                $model->attributes = $_POST;
                $model->subject = "На сайте profikarkas.com.ua:главная пользователь {$model->name} отправил сообщение из формы {$model->form} телефон: {$model->phone}, e-mail: {$model->email}";

                if ($model->validate() && $checkCaptcha) {
                    if (ZohoHelper::sendData($model)) {
                        if ($model->form == 'Запрос на поездку в демо-дом. Главная') {
                            $msg = 'Спасибо, что приняли приглашение! Мы свяжемся с вами в ближайшее время для согласования деталей.';
                        } else {
                            $msg = 'Cообщение отправлено';
                        }

                        $result = array('success' => true, 'message' => $msg);
                    }

                    $msg = 'Спасибо, что приняли приглашение! Мы свяжемся с вами в ближайшее время для согласования деталей.';

                } else {
                    $error = array_pop($model->errors);
                    $result = array('error' => true, 'message' => $error[0]);
                }

            }

        } else {
            throw new CHttpException('Запрашиваемая страница не существует.');
        }

        echo json_encode($result);
    }

    public function actionHotelsContact()
    {
        $msg = 'Заполните поля формы';
        $result = ['error' => true, 'message' => $msg];
        $model = new RecallForm;

        if (Yii::app()->request->isAjaxRequest) {
            if (! empty($_POST)) {
                if (isset($_POST['g-recaptcha-response'])) {
                    $captcha = $_POST['g-recaptcha-response'];
                } else {
                    $captcha = false;
                }

                if ($captcha) {
                    $secret   = '6LepOLcZAAAAAHkJTM9eKBNcLhkRNtldEXN9jBaH';
                    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
                    $response = json_decode($response);

                    if ($response->success == true) {
                        $checkCaptcha = true;
                    } else {
                        $checkCaptcha = false;
                    }
                }
                $model->attributes = $_POST;
                $model->subject = "На сайте profikarkas.com.ua {$model->name} отправил сообщение из формы {$model->form} телефон: {$model->phone}, e-mail: {$model->email}";

                if ($model->validate() && $checkCaptcha) {
                    if (ZohoHelper::sendData($model)) {
                        $msg = 'Cообщение отправлено';

                        $result = ['success' => true, 'message' => $msg];
                    }

                } else {
                    $error = array_pop($model->errors);
                    $result = ['error' => true, 'message' => isset($model->errors[0]) ? $model->errors[0] : 'Заполните поля формы.'];
                }
            }

        } else {
            throw new CHttpException('Запрашиваемая страница не существует.');
        }

        echo json_encode($result);
    }

    private function sendAjaxFormData($name, $email, $phone, $subject, $country, $city, $lang, $filePath = null)
    {
        if ($filePath) {
            $uploadFileMimeType = mime_content_type($filePath);
            $uploadFile = new CURLFile($filePath, $uploadFileMimeType,'document');
        } else {
            $uploadFile = null;
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://profikarkas.com.ua/zohocrm/application.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 1000,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'subject' => $subject,
                'country' => $country,
                'city' => $city,
                'landing_lang' => $lang,
                'document' => $uploadFile
            ],
            CURLOPT_HTTPHEADER => [
                'Content-Type: multipart/form-data'
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function actionLangs()
    {
        $this->layout = false;

        $this->render('langs');
    }

    public function actionIframe()
    {
        $this->layout = false;

        $this->render('iframe');
    }

    public function actionIframe2()
    {
        $this->layout = false;

        $this->render('iframe2');
    }
}
