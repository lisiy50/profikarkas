<?php

class BuildInEuropeController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 29;

    public $highlight = 29;

    public function actionIndex(){
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = new BuildInEurope('search');
        if(isset($_GET['BuildInEurope']))
            $model->attributes = $_GET['BuildInEurope'];

        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    public function actionView($id){
        $this->highlight = 44;
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = $this->loadModel($id);

        if(!isset($_GET['uri']))
            $this->redirect($model->getUrl(), true, 301);

        Yii::app()->seo->setParam('title', $model->title);

        $this->render('view', array('model'=>$model));
    }

    /*
     * @return BuildInEurope model class
     * */
    public function loadModel($id)
    {
        $model=BuildInEurope::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
