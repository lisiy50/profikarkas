<?php

class EquiphousesController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 31;
    public $highlight = 31;

    /**
     * @param $id
     * @return EquipHouses model class
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=EquipHouses::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex(){
        $firstItem = EquipHouses::model()->find();
        $this->redirect($firstItem->getUrl());
//        $this->render('index', array(
//            'models' => EquipHouses::model()->findAll(),
//        ));
    }

    public function actionView($id)
    {
        $equipHouses=$this->loadModel($id);

        if (Yii::app()->request->requestUri != $equipHouses->getUrl()) {
            $this->redirect($equipHouses->getUrl(), true, 301);
        }

        Yii::app()->seo->setParam('title', $equipHouses->name);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->render('view',array(
            'models' => EquipHouses::model()->findAll(),
            'equipHouses'=>$equipHouses,
        ));
    }
}
