<?php
class SitemapController extends FrontendController {

    public $layout=false;

    protected function createUrlElement($dom, $loc, $lastmod, $changefreq='monthly', $priority=0.5) {
        $url=$dom->createElement('url');
        $url->appendChild($dom->createElement('loc', Yii::app()->request->getHostInfo() . $loc));
        $url->appendChild($dom->createElement('lastmod', $lastmod));
        $url->appendChild($dom->createElement('changefreq', $changefreq));
        $url->appendChild($dom->createElement('priority', $priority));
        return $url;
    }

    private function getXml($return = false){
        $dom=new DOMDocument('1.0', 'UTF-8');

        $urlset=$dom->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        $urlset->appendChild($this->createUrlElement($dom, Yii::app()->createUrl('site/index'), date('Y-m-d'), 'monthly', 0.9));

        foreach(MenuItem::model()->findAll(array('condition'=>'parent_id = 1')) as $menuItem){
            $urlset->appendChild(
                $this->createUrlElement($dom, $menuItem->url, date('Y-m-d', strtotime('-1 day')), 'daily')
            );
            if($menuItem->children){
                foreach($menuItem->children as $child){
                    if($menuItem->url == $child->url)
                        continue;
                    $urlset->appendChild(
                        $this->createUrlElement($dom, $child->url, date('Y-m-d', strtotime('-1 day')), 'daily')
                    );
                }
            }
        }

        foreach(Project::model()->findAll() as $project)
            $urlset->appendChild(
                $this->createUrlElement($dom, $project->url, date('Y-m-d', $project->update_time?$project->update_time:$project->create_time))
            );

        foreach(Portfolio::model()->findAll() as $portfolio)
            $urlset->appendChild(
                $this->createUrlElement($dom, $portfolio->url, date('Y-m-d', $portfolio->update_time?$portfolio->update_time:$portfolio->create_time))
            );

        foreach(Review::model()->findAll() as $review)
            $urlset->appendChild(
                $this->createUrlElement($dom, $review->url, date('Y-m-d', $review->update_time?$review->update_time:$review->create_time))
            );

        foreach(BuildInEurope::model()->findAll() as $buildInEurope)
            $urlset->appendChild(
                $this->createUrlElement($dom, $buildInEurope->url, date('Y-m-d', $buildInEurope->update_time?$buildInEurope->update_time:$buildInEurope->create_time))
            );

        foreach(News::model()->findAll() as $news_item)
            $urlset->appendChild(
                $this->createUrlElement($dom, $news_item->url, date('Y-m-d', $project->update_time?$project->update_time:$project->create_time))
            );



        $dom->appendChild($urlset);
        if($return)
            return $dom->saveXML();
        else
            echo $dom->saveXML();
    }

    public function actionXml() {
        header('Content-Type: text/xml; charset=UTF-8');
        $this->getXml();
    }

    public function actionGz(){
        $xmlFilePath = app()->basePath.DS.'..'.DS.'_sitemap.xml';
        $gzFilePath = app()->basePath.DS.'..'.DS.'_sitemap.xml.gz';
        $fp = gzopen ($gzFilePath, 'w9');
        file_put_contents($xmlFilePath, $this->getXml(true));
        gzwrite ($fp, file_get_contents($xmlFilePath));
        gzclose($fp);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=sitemap.xml.gz');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo file_get_contents($gzFilePath);
    }
}
