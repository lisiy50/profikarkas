<?php
class SearchController extends FrontendController
{
    public $layout='//layouts/main';

    public function actionResult($query = null)
    {
        $product = new Product('search');
        $product->unsetAttributes();
        if(isset($_GET['Product']))
            $product->attributes=$_GET['Product'];
        $product->search_query = $query;

        $dataProvider = $product->search();

        $dataProvider->setSort(array(
            'defaultOrder' => Yii::app()->params['product_search_order'],
            'sortVar' => 'sort',
        ));
        $dataProvider->setPagination(array(
            'pageSize' => Yii::app()->config['product_search_limit'],
        ));

        $this->breadcrumbs = array(
            "Поиск \"{$query}\""
        );
        Yii::app()->seo->setParam('query', $query);
        Yii::app()->seo->setDefault('title', '{shop_name} - Результаты поиска: {query}');

        $this->render('result', array(
            'dataProvider' => $dataProvider,
            'query' => $query,
        ));
    }

    public function actionIndex($q)
    {
        if (mb_strlen($q) > 1) {
            /** @var Project[] $projects */
            $projects = Project::model()->findAll('concat(`symbol`, " ", `variation_symbol`, " ", `title`, " ", `description`) LIKE :q', array(':q' => "%{$q}%"));
            /** @var Portfolio[] $portfolioItems */
            $portfolioItems = Portfolio::model()->findAll('concat(`project_name`, " ", `construction_place`, " ", `workload`, " ", `description`) LIKE :q', array(':q' => "%{$q}%"));
        } else {
            $projects = [];
        }
        $this->render('index', ['projects' => $projects, 'portfolioItems' => $portfolioItems]);
    }

}