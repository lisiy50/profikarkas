<?php

class PressController extends FrontendController{

    public $layout = '//layouts/main';

    public $menu_id = 41;
    public $highlight = 97;

    public function actionView($id){

        $model = $this->loadModel($id);
        if(!isset($_GET['uri']))
            $this->redirect($model->getUrl(array('route'=>'press/view',)), true, 301);

        Yii::app()->seo->setRoute('usefulArticle/view');

        Yii::app()->seo->setParam('title', $model->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->breadcrumbs = array(
            'Мы в прессе' => array('press/index'),
            $model->title,
        );

        Yii::setPathOfAlias('Deepzoom', Yii::getPathOfAlias('application.lib.deepzoom.Deepzoom'));

        foreach ($model->images as $image) {
            if(is_dir($image->getImagePath('original').'_files'))
                continue;
            $deep = new \Deepzoom\ImageCreator(new \Deepzoom\StreamWrapper\File(),new \Deepzoom\Descriptor(new \Deepzoom\StreamWrapper\File()),new \Deepzoom\ImageAdapter\Imagick());
            $deep->create($image->getImagePath('original'), $image->getImagePath('original').'.dzi');
        }

        $this->render('view', array('model'=>$model));
    }

    public function actionTest($id) {
        $model = $this->loadModel($id);

        Yii::app()->seo->setRoute('usefulArticle/view');

        Yii::app()->seo->setParam('title', $model->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->breadcrumbs = array(
            'Мы в прессе' => array('press/index'),
            $model->title,
        );

        Yii::setPathOfAlias('Deepzoom', Yii::getPathOfAlias('application.lib.deepzoom.Deepzoom'));

        $deep = new \Deepzoom\ImageCreator(new \Deepzoom\StreamWrapper\File(),new \Deepzoom\Descriptor(new \Deepzoom\StreamWrapper\File()),new \Deepzoom\ImageAdapter\Imagick());
        foreach ($model->images as $image) {
            if(is_dir($image->getImagePath('original').'_files'))
                continue;
            $deep->create($image->getImagePath('original'), $image->getImagePath('original').'.dzi');
        }

        $this->render('test', array('model'=>$model));
    }

    /**
     * @param $id
     * @return UsefulArticle
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=UsefulArticle::model()->findByPk((int)$id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex(){
        if($this->menu_id)
            $this->layout = '//layouts/column2';

        $model = new UsefulArticle('search');
        if(isset($_GET['UsefulArticle']))
            $model->attributes = $_GET['UsefulArticle'];

        $dataProvider=$model->search();
        $dataProvider->setPagination(array(
            'pageSize' => 6,
            'pageVar' => 'page',
        ));

        $criteria = $dataProvider->getCriteria();
        $criteria->scopes = 'press';

        if(app()->request->isAjaxRequest){
            $this->renderPartial('ajaxIndex', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }
    }

    function actionEz($id=45) {
//        if($this->menu_id)
//            $this->layout = '//layouts/column2';

        $model = $this->loadModel($id);

        Yii::app()->seo->setRoute('usefulArticle/view');

        Yii::app()->seo->setParam('title', $model->title);
        Yii::app()->seo->setDefault('title', '{shop_name} - {title}');

        $this->breadcrumbs = array(
            'Мы в прессе' => array('press/index'),
            $model->title,
        );

        $this->render('ez', array('model'=>$model));
    }
}
