var socialPoupTimeout = null, socialPopupStart = null;
(socialPopupStart = function () {
    if (!$.cookie("social-poup-dontshow")) {
        var o = 18e4;
        $.cookie("social-popup-first-enter-time") || $.cookie("social-popup-first-enter-time", (new Date).getTime(), {
            expires: 1,
            path: "/"
        });
        var e = o, i = (new Date).getTime() - $.cookie("social-popup-first-enter-time");
        e = o < i ? 1 : o - i, socialPoupTimeout = setTimeout(function () {
            $.get("site/socialpopup", function (o) {
                $("body").append(o), $(".social-poup").animate({opacity: 1}, 1e3, function () {
                    $("#social-poup-wrapper").show().animate({opacity: 1})
                }), ga("create", "UA-52477514-1", "auto"), ga("send", "event", "Social", "Click", "Show", {nonInteraction: TRUE})
            }), $.cookie("social-popup-first-enter-time", null)
        }, e)
    }
    $("body").on("click", "#social-poup-close, .social-poup-subscribed span", function (o) {
        return $.cookie("social-poup-dontshow", !0, {
            expires: 21,
            path: "/"
        }), $("#social-poup-wrapper, .social-poup").remove(), !1
    })
})();
