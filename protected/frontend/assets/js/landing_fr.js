/**
 * Created by lisiy50 on 9/14/15.
 */
$(document).ready(function(){

    $.post('landing/ajaxFrForm', function(html){
        $('#form-container').html(html);
    });

    $('#form-container').on('submit', '#LandingFrForm-form', function () {
        $('#LandingFrForm-form .form-container-send').attr('disabled', 'disabled');

        var fileField = $(this).find('#LandingFrForm_file');
        var fd = new FormData(this);

        fd.append('file', fileField.prop('files')[0]);

        $.ajax({
            url: $(this).attr('action'),
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (html) {
                $('#form-container').html(html);
            }
        });
        return false;
    });
});
