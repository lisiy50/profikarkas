if (!$.cookie('sign-up-for-viewing-dont-show') && !$.cookie('sign-up-for-viewing-closed')) {
    setTimeout(function(){
        $.post('site/signUpForViewing', function(html){
            $('body').append(html);
        });
        clearTimeout(socialPoupTimeout);
    }, 1000 * 120);

    $('body').on('submit', '#SignUpForViewing-form', function () {
        $('.sign-up-for-viewing-form-loader').show();
        $.post($.createUrl('site/signUpForViewing'), $(this).serialize(), function (html) {
            $('#sign-up-for-viewing').remove();
            $('body').append(html);
            $.cookie('sign-up-for-viewing-dont-show', true, {
                expires: 180,
                path: '/'
            });
        });
        return false;
    });

    $('body').on('click', '.sign-up-for-viewing-close, .sign-up-for-consultation-send[type="reset"]', function(){
        $('#sign-up-for-viewing').remove();
        $.cookie('sign-up-for-viewing-closed', true, {
            expires: 3,
            path: '/'
        });
        socialPopupStart();
        return false;
    });
}
