var socialPoupTimeout = null;
var socialPopupStart = null;
var advantagePopupTimeout = null;
var advantagePopupStart = null;

jQuery(function($){

    $(document).on('click', '[rel="compare"]', function(){
        var product_id=$(this).data('id');
        var el=$(this);
        $.get($.createUrl('compare/put'), {id:product_id}, function(){
            el.addClass('in-compare').unbind('click').text('В сравнении');
        });

        return el.hasClass('in-compare');
    });

    $(document).on('click', '[rel="cart"]', function(){
        var product_id=$(this).data('id');
        var quantity=$(this).data('quantity');

        if(/^[0-9]+$/.test(quantity)==false) {
            quantity=$(quantity).val();
        }
        quantity=Math.max(parseInt(quantity), 1) || 1;

        $.get($.createUrl('order/put'), {
            id:product_id,
            quantity:quantity
        }, function(html){
            $('#mz-cart-box').html(html);
        })

        return false;
    });

    addCrossLinksSpace();

    $(function(){
        var positionLeftFixed;
        var positionLeftAbsolute;
        var positionTopFixed;
        var positionTopAbsolute;
        var paddingTop;

        if( $(window).width() < 1025 ) {
            positionTopFixed = '25px';
            positionTopAbsolute = '0';
            positionLeftFixed = '10px';
            positionLeftAbsolute = '-5px';
            paddingTop = '90px';
        } else {
            positionTopFixed = '25px';
            positionTopAbsolute = '0';
            positionLeftFixed = 'calc((100% - 1030px) / 2 - 40px)';
            positionLeftAbsolute = '-55px';
            paddingTop = '0';
        }

        $(document).scroll(function(){
            if ($(window).scrollTop() > 200) {
                $('.block_social_networks').css({
                    position: 'fixed',
                    top: positionTopFixed,
                    left: positionLeftFixed,
                    paddingTop: paddingTop
                });
            } else if ($(window).scrollTop() <= 200) {
                $('.block_social_networks').css({
                    position: 'absolute',
                    top: positionTopAbsolute,
                    left: positionLeftAbsolute,
                    paddingTop: paddingTop
                });
            }
        });
    });




    // $('#recall').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Call', {nonInteraction: FALSE});
    // });
    // $('#recall_for_crm').on('submit', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Call', {nonInteraction: FALSE});
    // });
    // $('#request-price').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Actual price', {nonInteraction: FALSE});
    // });
    // $('#send-price-from').on('submit', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Actual price', {nonInteraction: FALSE});
    // });
    // $('#recomend-project').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Recommend', {nonInteraction: FALSE});
    // });
    // $('.recomend-project-send-btn').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Recommend', {nonInteraction: FALSE});
    // });
    // $('#CostCalculationForm-form').on('submit', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Submit', 'Ferma', {nonInteraction: FALSE});
    // });
    // $('#project-view-page img').on('click', function(){
    //     var img = $(this).attr('src');
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Img', 'Click', img, {nonInteraction: FALSE});
    // });
    // $('.a[href^="mailto:"]').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'E-mail', {nonInteraction: FALSE});
    // });
    $('.a[href^="skype:"]').on('click', function(){
        ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Contacts', 'Click', 'Skype', {nonInteraction: true});
    });
    // $('.a[href="#project-tab4"]').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'Price', {nonInteraction: FALSE});
    // });
    // $('.a[href="#project-tab3"]').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'Interiors', {nonInteraction: FALSE});
    // });
    // $('.a[href="#project-tab1"]').on('click', function(){
    //     ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Tab', 'Click', 'General', {nonInteraction: FALSE});
    // });

    $('#link-1').click(function() {
        $(window).scrollTo('#link-area-1', 1600, {
            offset: -20
        });
        return false;
    });

    $('#link-2').click(function() {
        $(window).scrollTo('#link-area-2', 1700, {
            offset: -20
        });
        return false;
    });

    $('#link-3').click(function() {
        $(window).scrollTo('#link-area-3', 1800, {
            offset: -20
        });
        return false;
    });

    $('#link-4').click(function() {
        $(window).scrollTo('#link-area-4', 1900, {
            offset: -20
        });
        return false;
    });

    $('#link-5').click(function() {
        $(window).scrollTo('#link-area-5', 2000, {
            offset: -20
        });
        return false;
    });

    $('.spoiler-link').click(function (e) {
        e.preventDefault();
        if ($(this).find('span').text() == '+ ') {
            $(this).find('span').text('– ');
        } else {
            $(this).find('span').text('+ ');
        }
        $(this).siblings('.spoiler-body').slideToggle();
        return false;
    });

    $('#iqEnergy').parent().css('padding', '0');

    $('#historyLink, #historyLinkMobile').click(function(){
        parent.history.back();
        return false;
    });
});

function addCrossLinksSpace()
{
    $('#project-catalog .whats_good').each(function(i, el){
        var length = $(el).find('>div').length;
        if(length<=2){
            $(el).css('padding-top','80px');
        } else if(length==3){
            $(el).css('padding-top','56px');
        } else if(length==4){
            $(el).css('padding-top','32px');
        } else if(length==5){
            $(el).css('padding-top','8px');
        }
    });

    $('#portfolio-catalog .portfolio_cross_link').each(function(i, el){
        var length = $(el).find('>div').length;
        if(length<=2){
            $(el).css('padding-top','75px');
        } else if(length==3){
            $(el).css('padding-top','50px');
        } else if(length==4){
            $(el).css('padding-top','30px');
        } else if(length==5){
            $(el).css('padding-top','10px');
        }
    });

    socialPopupStart = function () {
        if(!$.cookie('social-poup-dontshow')) {

            var timeoutGeneral = 1000 * 180;
            // var timeoutGeneral = 1000 * 30;
            if (!$.cookie('social-popup-first-enter-time')) {
                $.cookie('social-popup-first-enter-time', new Date().getTime(), {
                    expires: 1,
                    path: '/'
                });
            }
            var timeout = timeoutGeneral;
            var timeLeft = new Date().getTime() - $.cookie('social-popup-first-enter-time');
            if (timeLeft > timeoutGeneral) {
                timeout = 1;
            } else {
                timeout = timeoutGeneral - timeLeft;
            }

            socialPoupTimeout = setTimeout(function () {
                $.get('site/socialpopup', function (html) {
                    $('body').append(html);
                    $('.social-poup').animate({opacity: 1}, 1000, function () {
                        $('#social-poup-wrapper').show().animate({opacity: 1});
                    });
                    ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Social', 'Click', 'Show', {nonInteraction: true});
                });
                $.cookie('social-popup-first-enter-time', null);
            }, timeout);
        }

        $('body').on('click', '#social-poup-close, .social-poup-subscribed span', function(el){
            $.cookie('social-poup-dontshow', true, {
                expires: 21,
                path: '/'
            });
            $('#social-poup-wrapper, .social-poup').remove();
            return false;
        });
    }
    socialPopupStart();

    advantagePopupStart = function () {
        if (document.referrer.substring(0, 18) == 'http://z500.com.ua') {
            $.cookie('advantage-popup-show', true, {
                expires: 21,
                path: '/'
            });
        }

        if ($.cookie('advantage-popup-show')) {

            advantagePopupTimeout = setTimeout(function () {
                $.get('site/advantagepopup', function (html) {
                    $('body').append(html);
                    $('.advantage-popup').animate({opacity: 1}, 1000, function () {
                        $('#advantage-popup-wrapper').show().animate({opacity: 1});
                    });
                    ga('create', 'UA-52477514-1', 'auto'); ga('send', 'event', 'Advantage', 'Click', 'Show', {nonInteraction: true});
                });
            }, 1000 * 15);
        }

        $('body').on('click', '.advantage-popup-close', function(el) {
            $.cookie('advantage-popup-show', null, {
                path: '/'
            });
            $('#advantage-popup-wrapper, .advantage-popup').remove();
            return false;
        });

        $('body').on('click', '.advantage-popup-link', function(el) {
            $.cookie('advantage-popup-show', null, {
                path: '/'
            });
        });
    }
    advantagePopupStart();

    if( $(window).width() >= 823 ) {
        $('.cost-btn, .cost-title-icon-link').fancybox({
            padding: 0,
            arrows: false,
            helpers: {
                title: null
            }
        });
    }

    $('.show_slider_5').click(function (e) {
        e.preventDefault();
        $('#slider_5').addClass('kit-slider-wrap-open');
        $('html').css('overflow', 'hidden');
        return false;
    });

    $('.show_slider_4').click(function (e) {
        e.preventDefault();
        $('#slider_4').addClass('kit-slider-wrap-open');
        $('html').css('overflow', 'hidden');
        return false;
    });

    $('.show_slider_3').click(function (e) {
        e.preventDefault();
        $('#slider_3').addClass('kit-slider-wrap-open');
        $('html').css('overflow', 'hidden');
        return false;
    });

    $('.show_slider_2').click(function (e) {
        e.preventDefault();
        $('#slider_2').addClass('kit-slider-wrap-open');
        $('html').css('overflow', 'hidden');
        return false;
    });

    $('.show_slider_1').click(function (e) {
        e.preventDefault();
        $('#slider_1').addClass('kit-slider-wrap-open');
        $('html').css('overflow', 'hidden');
        return false;
    });

    $('.kit-slider-close, .kit-slider-overlay').click(function (e) {
        e.preventDefault();
        $('.kit-slider-wrap').removeClass('kit-slider-wrap-open');
        $('html').css('overflow', 'initial');
        return false;
    });

    $('#kit_slider_5').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        infinite: true,
        speed: 400,
        autoplay: false,
        arrows: true
    });

    $('#kit_slider_4').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        infinite: true,
        speed: 400,
        autoplay: false,
        arrows: true
    });

    $('#kit_slider_3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        infinite: true,
        speed: 400,
        autoplay: false,
        arrows: true
    });

    $('#kit_slider_2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        infinite: true,
        speed: 400,
        autoplay: false,
        arrows: true
    });

    $('#kit_slider_1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        infinite: true,
        speed: 400,
        autoplay: false,
        arrows: true
    });

    $('#kit_1, #kit_2, #kit_3, #kit_4, #kit_5').tab();

    $('.cost-var-head').click(function () {
        if( $(window).width() <= 823 ) {
            $(this).toggleClass('opened');
        } else {
            $(this).toggleClass('closed');
        }

        $(this).next().slideToggle();
        $(this).next().toggleClass('overflow');
        return false;
    });

    if( $(window).width() <= 823 ) {
        $('.cost-feature-link').click(function () {
            $('.cost-feature-link').not(this).text('?');
            $('.cost-feature-link').not(this).prev().prev().removeClass('opened');
            $('.cost-feature-link').not(this).prev().prev().prev().removeClass('opened');
            if ($(this).text() === '×') {
                $(this).prev().prev().removeClass('opened');
                $(this).prev().prev().prev().removeClass('opened');
                $(this).text('?');
            } else {
                $(this).prev().prev().addClass('opened');
                $(this).prev().prev().prev().addClass('opened');
                $(this).text('×');
            }
            return false;
        });
    }

    $('.kit-hamburger-number').click(function () {
        $('.kit-hamburger-number').not(this).next().removeClass('opened');
        if ($(this).next().hasClass('opened') == true) {
            $(this).next().removeClass('opened');
        } else {
            $(this).next().addClass('opened');
        }
        return false;
    });

    $(function(){
        var costTableoffsetTop = $('.cost-table').offset().top;
        var costHeadHeight = $('.cost-head').height();
        var costHeadNumHeight = $('.cost-head-num-mobile').height();
        var costTableHeight = $('.cost-table').height();

        $(window).on('resize', function(){
            costHeadHeight = $('.cost-head').height();
        });

        $(document).scroll(function() {
            if ( $(window).width() > 823 ) {
                if ($(this).scrollTop() > costTableoffsetTop && $(this).scrollTop() < costTableHeight + costTableoffsetTop - costHeadHeight) {
                    $('.cost-table').css("paddingTop", costHeadHeight);
                    $('.cost-head').addClass('fixed');
                } else {
                    $('.cost-table').css("paddingTop", 0);
                    $('.cost-head').removeClass('fixed');
                }
            }
        });
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 6500;
        var sumElements = 5;

        $('.cost-btn-desktop-1').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-desktop-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-desktop-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-desktop-1').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-desktop-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-desktop-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 6500;
        var sumElements = 5;

        $('.cost-btn-desktop-2').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-desktop-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-desktop-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-desktop-2').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-desktop-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-desktop-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 6500;
        var sumElements = 5;

        $('.cost-btn-desktop-3').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-desktop-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-desktop-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-desktop-3').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-desktop-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-desktop-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 7500;
        var sumElements = 5;

        $('.cost-btn-mobile-1').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-mobile-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-mobile-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-mobile-1').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-mobile-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-mobile-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 7500;
        var sumElements = 5;

        $('.cost-btn-mobile-2').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-mobile-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-mobile-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-mobile-2').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-mobile-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-mobile-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(function(){
        var sumTimer = 0;
        var delayTimer = 7500;
        var sumElements = 5;

        $('.cost-btn-mobile-3').each(function(index, element) {
            sumTimer += delayTimer;
            setTimeout(function() {
                $(element).addClass('cost-btn-mobile-animate');
            }, sumTimer - delayTimer);
            setTimeout(function() {
                $(element).removeClass('cost-btn-mobile-animate');
            }, sumTimer);

        });

        var timerId = setInterval(function() {
            var sumTimer = 0;

            $('.cost-btn-mobile-3').each(function(index, element) {
                sumTimer += delayTimer;
                setTimeout(function() {
                    $(element).addClass('cost-btn-mobile-animate');
                }, sumTimer - delayTimer);
                setTimeout(function() {
                    $(element).removeClass('cost-btn-mobile-animate');
                }, sumTimer);

            });

        }, sumElements * delayTimer);
    });

    $(document).on('click', '.invitation-confirm-close', function(e) {
        e.preventDefault();
        $('.invitation-confirm-wrapper').fadeOut('slow');
    });
}

function init() {
    var lazyElements = document.querySelectorAll("iframe[data-src], img[data-src]");
    for (var i = 0; i < lazyElements.length; i++) {
        if(lazyElements[i].getAttribute('data-src')) {
            lazyElements[i].setAttribute('src', lazyElements[i].getAttribute('data-src'));
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    window.setTimeout('init()', 4000);
});
