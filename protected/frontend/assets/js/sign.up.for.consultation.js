/**
 * Created by lisiy50 on 9/14/15.
 */
$(document).ready(function(){

    if (!$.cookie('sign-up-for-consultation-dont-show') && !$.cookie('sign-up-for-consultation-closed')) {
        setTimeout(function(){
            $.post('site/signUpForConsultation', function(html){
                $('body').append(html);
                clearTimeout(socialPoupTimeout);
                ga('create', 'UA-52477514-1', 'auto');ga('send', 'event', 'Form', 'Click', 'Free consultation', {nonInteraction: TRUE});
            });
        }, 1000 * 120);

        $('body').on('submit', '#SignUpForConsultation-form', function(){
            $('.sign-up-for-consultation-form-loader').show();
            $.post($.createUrl('site/signUpForConsultation'), $(this).serialize(), function(html){
                $('#sign-up-for-consultation').remove();
                $('body').append(html);
                $.cookie('sign-up-for-consultation-dont-show', true, {
                    expires: 180,
                    path: '/'
                });
            });
            return false;
        });

        $('body').on('click', '.sign-up-for-consultation-close, .sign-up-for-consultation-send[type="reset"]', function(){
            $('#sign-up-for-consultation').remove();
            $.cookie('sign-up-for-consultation-closed', true, {
                expires: 3,
                path: '/'
            });
            socialPopupStart();
            return false;
        });
    }
});
