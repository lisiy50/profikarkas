/**
 * Created by lisiy50 on 9/14/15.
 */
$(document).ready(function(){

    $.post('/site/costCalculation', function(html){
        $('#cost-calculation').html(html);
    });

    $('#cost-calculation').on('submit', '#CostCalculationForm-form', function () {
        $('#CostCalculationForm-form .cost-calculation-send').attr('disabled', 'disabled');

        var fileField = $(this).find('#CostCalculationForm_file');
        var fd = new FormData(this);

        fd.append('file', fileField.prop('files')[0]);

        $.ajax({
            url: $(this).attr('action'),
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (html) {
                $('#cost-calculation').html(html);
            }
        });
        return false;
    });
});
