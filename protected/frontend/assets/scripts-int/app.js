$(".header__nav-btn").on("click", function() {
    $(".header__nav-inner").slideToggle();
    $(this).toggleClass("header__nav-btn--active");
});

$(".mod-header__nav-btn").on("click", function() {
    $(".mod-header__nav-inner").slideToggle();
    $(this).toggleClass("mod-header__nav-btn--active");
    $(".mod-header__nav").toggleClass("mod-header__nav--active");
});

$(".mod-header__btn").on("click", function () {
    $(this).toggleClass("mod-header__btn--open");
    $(".mod-header__menu").toggle();
    $(".mod-header__menu .container").prepend($(".mod-header__email").show(), $(".mod-header__contacts").show());
});

$(".mod-header__menu-link").on("click", function () {
    if ($(window).width() <= 991) {
        $(".mod-header__btn").click();
    }
});

$('#projectsGallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 6000,
    asNavFor: '#projectsThumbs'
});

$('#projectsThumbs').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '#projectsGallery',
    prevArrow: '<span class="slider__arrow2 slider__arrow2--prev"></span>',
    nextArrow: '<span class="slider__arrow2 slider__arrow2--next"></span>',
    dots: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 6
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 5
            }
        },
        {
            breakpoint: 415,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});

$('#housesGallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 6000,
    asNavFor: '#housesThumbs'
});

$('#housesThumbs').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '#housesGallery',
    prevArrow: '<span class="slider__arrow1 slider__arrow1--prev"></span>',
    nextArrow: '<span class="slider__arrow1 slider__arrow1--next"></span>',
    dots: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 6
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 5
            }
        },
        {
            breakpoint: 415,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});

$('#projectsCarousel').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    autoplay: false,
    speed: 900,
    cssEase: "cubic-bezier(.165, .84, .44, 1)",
    nextArrow: ".projects__carousel-arrow--next",
    prevArrow: ".projects__carousel-arrow--prev",
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

function init() {
    var lazyElements = document.querySelectorAll("iframe[data-src], img[data-src]");
    for (var i = 0; i < lazyElements.length; i++) {
        if(lazyElements[i].getAttribute('data-src')) {
            lazyElements[i].setAttribute('src', lazyElements[i].getAttribute('data-src'));
        }
    }
}
document.addEventListener('DOMContentLoaded', function () {
    window.setTimeout('init()', 2000);
});
