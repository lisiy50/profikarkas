var gulp = require('gulp');
var watch = require('gulp-watch');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('cssnano');
var rename = require('gulp-rename');
var atImport = require("postcss-import");
var nested = require('postcss-nested');
var cssvariables = require('postcss-css-variables');

gulp.task('postcss-hotels', function() {
    return gulp.src('./postcss-hotels/*.pcss')
        .pipe(sourcemaps.init())
        .pipe(postcss([
            atImport(),
            nested(),
            cssvariables()
        ]))
        .pipe(postcss([cssnano]))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(rename('styles-hotels-3.min.css'))
        .pipe(sourcemaps.write({includeContent: false, sourceRoot: './postcss-hotels/'}))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch-hotels', function() {
    gulp.watch('./postcss-hotels/**/*.pcss', gulp.series('postcss-hotels'));
});

gulp.task('scripts-hotels', function(){
    return gulp.src([
        './scripts-hotels/vendor/jquery-1.11.3.js',
        './scripts-hotels/vendor/slick.min.js',
        './scripts-hotels/app.js'
    ])
        .pipe(concat('scripts-hotels-1.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js'));
});

gulp.task('postcss-int', function() {
    return gulp.src('./postcss-int/*.pcss')
        .pipe(sourcemaps.init())
        .pipe(postcss([
            atImport(),
            nested(),
            cssvariables()
        ]))
        .pipe(postcss([cssnano]))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(rename('styles-int-3.min.css'))
        .pipe(sourcemaps.write({includeContent: false, sourceRoot: './postcss-int/'}))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch-int', function() {
    gulp.watch('./postcss-int/**/*.pcss', gulp.series('postcss-int'));
});

gulp.task('scripts-int', function(){
    return gulp.src([
        './scripts-int/vendor/jquery-1.11.3.js',
        './scripts-int/vendor/slick.min.js',
        './scripts-int/app.js'
    ])
        .pipe(concat('scripts-int-2.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js'));
});

gulp.task('postcss', function() {
    return gulp.src('./postcss/*.pcss')
        .pipe(sourcemaps.init())
        .pipe(postcss([
            atImport(),
            nested(),
            cssvariables()
        ]))
        .pipe(postcss([cssnano]))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(rename('styles1.min.css'))
        .pipe(sourcemaps.write({includeContent: false, sourceRoot: './postcss/'}))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', function() {
    gulp.watch('./postcss/**/*.pcss', gulp.series('postcss'));
});

