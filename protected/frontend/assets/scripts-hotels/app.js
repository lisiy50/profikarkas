$(document).ready(function() {
    onloadPage();

    $(window).on("scroll", function() {
        if ($(window).scrollTop() === 0) {
            $(".navigation").removeClass("navigation--up");
        } else {
            $(".navigation").addClass("navigation--up");
        }
    });

    var windowWidthValue = (window.innerWidth > 0) ? window.innerWidth : screen.width;

    var clickedMenuCallback = false,
        canScroll;
    $("#headerCallBack").click(function() {
        $("#order1.popup").fadeIn(400);
        $("#overlay").show().css({"opacity": "0.5", "display": "block"});
        canScroll = false;
    });

    $(".popup .close_popup").click(function() {
        $("#order1.popup").fadeOut(400);
        $("#overlay").show().css({"opacity": "0", "display": "none"});
        canScroll = true;
        clickedMenuCallback = false;
    });

    $("#callBackBtnMobile").click(function() {
        $(".popup .close_popup").click();
        setTimeout(function() {
            clickedMenuCallback = true;
            $("#order1.popup").fadeIn(200);
            canScroll = false;
        },1400);
    });

    $(".menu").click(function() {
        if (! clickedMenuCallback) {
            setTimeout(function() {
                $("#overlay").show().css({"opacity": "0.5", "display" : "block"});
            },300);
            $(this).toggleClass("menu--open");
            if (windowWidthValue > 767) {
                if ($(this).hasClass("menu--open")) {
                    setTimeout(function() {
                        $(this).css({"height": $(".menu ul").height()});
                    },200);
                } else {
                    $(this).attr("style", "");
                    setTimeout(function() {
                        $("#overlay").show().css({"opacity": "0", "display" : "none"});
                    },400);
                }
            } else {
                $(this).attr("style", "");
                setTimeout(function(){
                    $("#overlay").hide().css({"opacity": "0", "display" : "none"});
                },400);
            }
        }
    });

    $(document).on("focus", "input, textarea", function () {
        $(this).data("placeholder", $(this).attr("placeholder")).attr("placeholder", "");
    }).on("blur", "input, textarea", function () {
        $(this).attr("placeholder", $(this).data('placeholder'));
    });

    $(".close").on("click", function() {
        $(".containerp").stop().removeClass("active");
    });

    $("#headerLangBtn").on("click", function() {
        $(".header__lang-inner").slideToggle();
        $(this).toggleClass("header__lang-btn--active");
    });

    $(document).mouseup(function (e) {
        if ($(".header__lang").has(e.target).length === 0){
            $(".header__lang-inner").slideUp();
            $("#headerLangBtn").removeClass("header__lang-btn--active");
        }
    });

    $(document).on("click", "#landing_recall .button_popup", function() {
        var form = $("#landing_recall");
        $.ajax({
            url: "landing/recall",
            type: "post",
            data: form.serialize(),
            success: function(data) {
                $('#order1 .card').html(data);
            },
            error: function(data) {
                alert('bad request');
            }
        });
        return false;
    });

    $(".form").submit(function(e) {
        e.preventDefault();
        console.log('submit');
        var form = $(this);
        form.find("button").prop("disabled", true);
        $.ajax({
            type: "post",
            url: "landing/hotelsContact",
            data: form.serialize(),
            dataType: "json",
            success: function(data) {
                console.log('ok');
                alert(data.message);
                if (data.success) {
                    form.find(".reset").val("");
                }
                form.find("button").prop("disabled", false);
            },
            error: function(data) {
                console.log('bad');
                alert(data.message);
                form.find('button').prop("disabled", false);
            }
        });
        return false;
    });

    function onloadPage() {
        if ($(window).scrollTop() > 0) {
            $(".navigation").addClass("navigation--up");
        }
    }

    $("#stageGallery").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        speed: 800,
        infinite: true,
        autoplaySpeed: 6000
    });

    $("#stageThumbs").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: "#stageGallery",
        arrows: false,
        speed: 1000,
        infinite: true,
        cssEase: "cubic-bezier(.165, .84, .44, 1)",
        dots: false,
        focusOnSelect: true
    });

    $("#partnerGallery").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 1000,
        cssEase: "cubic-bezier(.165, .84, .44, 1)",
        infinite: true,
        autoplaySpeed: 6000,
        arrows: true,
        nextArrow: ".footer__data-arrow_next",
        prevArrow: ".footer__data-arrow_prev",
        dots: false,
    });

    $.post('landing/hotelsForm', function(html){
        $('#formContainer').html(html);
    });

    $('#formContainer').on('submit', '#hotelsForm', function () {
        $('#hotelsFormSend').attr('disabled', 'disabled');

        var fileField = $(this).find('#LandingHotelsForm_file'),
            fd = new FormData(this);

        fd.append('file', fileField.prop('files')[0]);

        $.ajax({
            url: $(this).attr('action'),
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (html) {
                $('#formContainer').html(html);
            }
        });
        return false;
    });

});
