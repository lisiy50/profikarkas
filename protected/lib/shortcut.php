<?php
/**
 * This file contains constants and shortcut functions that are commonly used.
 * Please only include functions are most widely used because this file
 * is included for every request. Functions are less often used are better
 * encapsulated as static methods in helper classes that are loaded on demand.
 */

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

/**
 * Displays a variable.
 * This method achieves the similar functionality as var_dump and print_r
 * but is more robust when handling complex objects such as Yii controllers.
 * @param mixed variable to be dumped
 * @param integer maximum depth that the dumper should go into the variable. Defaults to 10.
 * @param boolean whether the result should be syntax-highlighted
 */
function dump($target, $depth=10, $highlight = true)
{
    echo CVarDumper::dumpAsString($target, $depth, $highlight);
}

/**
 * This is the shortcut to Yii::t().
 * Note that the category parameter is removed from the function.
 * It defaults to 'application'. If a different category needs to be specified,
 * it should be put as a prefix in the message, separated by '|'.
 * For example, t('backend|this is a test').
 */
function t($message, $params = array(), $source = null, $language = null)
{
    if (($pos = strpos($message, '|')) !== false)
    {
        $category = substr($message, 0, $pos);
        $message = substr($message, $pos + 1);
    }
    else
        $category = 'application';
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return app()->getUrlManager()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to Yii::app()->user.
 * @return WebUser
 */
function user()
{
    return app()->user;
}

/**
 * This is the shortcut to Yii::app()
 * @return CWebApplication
 */
function app()
{
    return Yii::app();
}

/**
 * This is the shortcut to Yii::app()->clientScript
 * @return CClientScript
 */
function cs()
{
    return app()->clientScript;
}

/**
 * Shortcut to Yii::app()->priceFormatter->templateFormat
 */
function price($arg1, $arg2=false, $arg3=false) {
    if(is_numeric($arg1)) {
        $template='{prefix}{price}{suffix}';
        $price=(float)$arg1;
        $params=$arg2;
    } else {
        $template=$arg1;
        $price=(float)$arg2;
        $params=$arg3;
    }
    return app()->priceFormatter->templateFormat($template, $price, $params);
}

/**
 * Register package
 */
function package($names) {
    foreach(explode(',',$names) as $name)
        cs()->registerPackage($name);
}

/**
 * Format date
 */
function format_date($format, $time=null) {
    if($time) {
        return app()->dateFormatter->format($format, $time);
    } else {
        return app()->dateFormatter->format('dd MMMM y', $format);
    }
}

/**
 * Return currency
 */
function currency($id=false) {
    $suffix=app()->currency->get($id, 'suffix');
    $prefix=app()->currency->get($id, 'prefix');
    return $suffix?$suffix:$prefix;
}

/**
 * Shortcut to config
 */
function config($param) {
    return Yii::app()->config[$param];
}

/**
 * This is the shortcut to Yii::app()->clientScript->registerCssFile
 */
function regCssFile($files, $url = 'css') {
    if (!is_array($files))
        $files = array($files);

    $baseUrl=app()->theme?app()->theme->baseUrl:app()->baseUrl;

    foreach ($files as $file)
        cs()->registerCssFile($baseUrl . '/' . $url . '/' . $file . '.css');
}

/**
 * This is the shotcut to Yii::app()->clientScript->registerCoreScript
 */
function regCoreFile($files)
{
    if (!is_array($files))
        $files = array($files);
    foreach ($files as $file)
        cs()->registerCoreScript($file);
}

/**
 * This is the shortcut to Yii::app()->clientScript->registerScriptFile
 */
function regJsFile($files, $url = 'js', $pos = CClientScript::POS_HEAD)
{
    if (!is_array($files))
        $files = array($files);

    $baseUrl=app()->theme?app()->theme->baseUrl:app()->baseUrl;

    foreach ($files as $file)
        cs()->registerScriptFile($baseUrl . '/' . $url . '/' . $file . '.js', $pos);
}

if(!function_exists('lcfirst')){
    function lcfirst($string){
        $string{0} = strtolower($string{0});
        return $string;
    }
}