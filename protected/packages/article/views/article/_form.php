<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));
    /* @var BootActiveForm $form */

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'title_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    ?>

    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php

    echo $form->widgetFieldRow($model, 'content_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'content', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_de', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_fr', 'Redactor');

    if(Lookup::hasItems('ArticleLayout')) {
        echo $form->dropDownListRow($model, 'layout', Lookup::items('ArticleLayout'), array(
            'class' => 'input-xlarge',
            'prompt' => 'По умолчанию',
        ));
    }

    if(Lookup::hasItems('ArticleView')) {
        echo $form->dropDownListRow($model, 'view', Lookup::items('ArticleView'), array(
            'class' => 'input-xlarge',
            'prompt' => 'По умолчанию',
        ));
    }

    if ($menu = MenuItem::model()->article()->findAll()) {
        echo $form->widgetFieldRow($model, 'menu_id', 'McDropdown', array(
            'data' => $menu,
            'htmlOptions' => array(
                'class' => 'input-xxlarge',
            ),
        ));
    }

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
