<?php

/**
 * This is the model class for table "{{article}}".
 *
 * The followings are the available columns in table '{{article}}':
 * @property integer $id
 * @property integer $menu_id
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $title_de
 * @property string $title_fr
 * @property string $content
 * @property string $content_uk
 * @property string $content_en
 * @property string $content_de
 * @property string $content_fr
 * @property integer $status
 * @property integer $menu_item_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 * @property string $layout
 * @property string $view
 */
class Article extends LibraryRecord
{

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'article/view',
            ),
            'imageResizeBehavior' => array(
                'class' => 'ImageResizeBehavior',
                'fields' => array('content'),
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Article the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{article}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, title_uk, title_en, title_de, title_fr', 'filter', 'filter'=>'strip_tags'),
            array('title, status', 'required'),
            array('menu_id, status, h1_size, h2_size, h3_size, h4_size', 'numerical', 'integerOnly'=>true),
            array('status', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('title, title_uk, title_en, title_de, title_fr', 'length', 'max'=>255),
            array('content, content_uk, content_en, content_de, content_fr', 'safe'),
            array('layout, view', 'length', 'max'=>32),
            array('layout, view', 'match', 'pattern' => '#^[a-z0-9_]+$#i', 'message' => 'Можно использовать только латинские буквы и цифры'),
            array('layout, view', 'match', 'pattern' => '#^[a-z][a-z0-9_]+$#i', 'message' => 'Первой должна идти латинская буква'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, title_uk, title_en, title_de, title_fr, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'menu'=>array(self::BELONGS_TO, 'MenuItem', 'menu_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок (Русский)',
            'title_uk' => 'Заголовок (Украинский)',
            'title_en' => 'Заголовок (Английский)',
            'title_de' => 'Заголовок (Немецкий)',
            'title_fr' => 'Заголовок (Французский)',
            'content' => 'Содержание (Русский)',
            'content_uk' => 'Содержание (Украинский)',
            'content_en' => 'Содержание (Английский)',
            'content_de' => 'Содержание (Немецкий)',
            'content_fr' => 'Содержание (Французский)',
            'status' => 'Опубликована',
            'create_time' => 'Добавлена',
            'update_time' => 'Изменена',
            'url' => 'URL адрес',
            'menu_id' => 'Используется меню',
            'layout' => 'Шаблон',
            'view' => 'Отображение',
            'h1_size' => 'размер h1(28)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',
        );
    }

    public function getMenuList() {
        return CHtml::listData(MenuItem::model()->findAll('parent_id=?', array(MenuItem::ARTICLE)), 'id', 'name');
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('title_uk',$this->title_uk,true);
        $criteria->compare('title_en',$this->title_en,true);
        $criteria->compare('title_de',$this->title_de,true);
        $criteria->compare('title_fr',$this->title_fr,true);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'update_time DESC',
            ),
        ));
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = strtolower(app()->translitFormatter->formatUrl($this->title));
        return parent::getUrl($params);
    }

    public function getShowSideMenu(){
        return $this->menu && $this->menu->countChildren;
    }

}
