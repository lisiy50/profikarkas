<?php
class ArticlePackage extends BasePackage {

    public $import=array(
        'article.models.Article'
    );

    public function getTopMenu() {
        return array(
            'Контент'=>array('order'=>2, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Статьи'=>array('order'=>1, 'url'=>array('article/index')),
            ))
        );
    }

}