<?php

class PortfolioController extends BackendController
{
    public $layout = '//layouts/column1';

    public $icon = 'picture';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Портфолио добавлено',
                'errorMessage' => 'Портфолио не добавлено',
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Портфолио &quot;{name}&quot; удалено',
            ),
        );
    }

    public function actionIndex()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes();
        if (isset($_GET['Portfolio']))
            $model->attributes = $_GET['Portfolio'];

        $dataProvider = $model->search();
        $dataProvider->pagination->pageSize = 500;
        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSaveOrder()
    {
        if (isset($_POST['Portfolio_id'])) {
            $portfolios = Portfolio::model()->findAllByAttributes(array('id' => $_POST['Portfolio_id']));
            foreach ($portfolios as $portfolio) {
                $portfolio->position = array_search($portfolio->id, $_POST['Portfolio_id']);
                $portfolio->save();
            }
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

}
