<?php

/**
 * This is the model class for table "{{portfolio}}".
 *
 * The followings are the available columns in table '{{portfolio}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $project_name
 * @property string $project_name_uk
 * @property string $project_name_en
 * @property string $project_name_fr
 * @property string $project_name_de
 * @property string $construction_place
 * @property string $construction_place_uk
 * @property string $construction_place_en
 * @property string $construction_place_fr
 * @property string $construction_place_de
 * @property integer $gross_area
 * @property string $workload
 * @property string $workload_uk
 * @property string $workload_en
 * @property string $workload_fr
 * @property string $workload_de
 * @property string $turnaround_time
 * @property string $period_of_works
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_fr
 * @property string $description_de
 *
 * @property string $video_code1
 * @property string $video_code2
 * @property string $video_code3
 * @property string $video_code4
 * @property string $video_code5
 * @property string $video_code6
 * @property integer $service1
 * @property integer $service2
 * @property integer $service3
 * @property integer $service4
 * @property integer $service5
 * @property integer $service6
 * @property string $image
 * @property integer $status
 * @property string $video_walk_url
 * @property integer $watermark
 * @property integer $construction
 * @property integer $show_albom
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 * @property string $create_time
 * @property string $update_time
 *
 * @property Project $project
 */
class Portfolio extends LibraryRecord
{

    private $_videos = array();
    private $_nextProjectModel = false;
    private $_prevProjectModel = false;

    public $watermark = self::STATUS_ENABLED;

    public $construction = self::STATUS_DISABLED;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{portfolio}}';
	}

    public function behaviors(){
  	    return array(
  		    'CTimestampBehavior' => array(
  		  	    'class' => 'zii.behaviors.CTimestampBehavior',
  		  	    'createAttribute' => 'create_time',
  			    'updateAttribute' => 'update_time',
  		    ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'portfolio/view',
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'project_name',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/portfolio/cover', 389, 204, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'small_'),
                ),
            ),
  	    );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('project_name, project_name_uk, project_name_en, project_name_fr, project_name_de, construction_place, turnaround_time, period_of_works, workload, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'strip_tags'),
            array('project_name, construction_place, construction_place_uk, construction_place_en, construction_place_fr, construction_place_de, turnaround_time, period_of_works, workload, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'trim'),
            array('gross_area, project_id, h1_size,h2_size,h3_size,h4_size', 'numerical', 'integerOnly'=>true),
            array('description, description_uk, description_en, description_fr, description_de','safe'),
			array('project_name', 'required'),
			array('project_name, construction_place, turnaround_time, period_of_works, workload, workload_uk, workload_en, workload_fr, workload_de', 'length', 'max'=>255),
			array('status, watermark, construction, service1, service2, service3, service4, service5, service6, show_albom', 'numerical', 'integerOnly'=>true),
            array('status, watermark, construction, show_albom', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('image', 'unsafe'),
            array('video_walk_url', 'url', 'allowEmpty' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, project_name, construction_place, status, watermark, construction, show_albom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'firstImage'=>array(self::HAS_ONE, 'PortfolioImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'images'=>array(self::HAS_MANY, 'PortfolioImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'PortfolioImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'project'=>array(self::BELONGS_TO, 'Project', 'project_id', ),
        );
	}

    public function defaultScope(){
        return array(
            'order' => 'position, create_time DESC',
        );
    }

    public function scopes(){
        return array(
            'directOrder' => array(
                'order' => 'create_time DESC',
            ),
            'reverseOrder' => array(
                'order' => 'create_time',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'project_id' => 'Проект',
			'project_name' => 'Название проекта (Русский)',
			'project_name_uk' => 'Название проекта (Украинский)',
			'project_name_en' => 'Название проекта (Английский)',
			'project_name_fr' => 'Название проекта (Французский)',
			'project_name_de' => 'Название проекта (Немецкий)',
			'construction_place' => 'Место строительства (Русский)',
			'construction_place_uk' => 'Место строительства (Украинский)',
			'construction_place_en' => 'Место строительства (Английский)',
			'construction_place_fr' => 'Место строительства (Французский)',
			'construction_place_de' => 'Место строительства (Немецкий)',
			'gross_area' => 'Общая площадь (m2)',
			'workload' => 'Заказанный объем работ (Русский) ',
			'workload_uk' => 'Заказанный объем работ (Украинский) ',
			'workload_en' => 'Заказанный объем работ (Английский) ',
			'workload_fr' => 'Заказанный объем работ (Французский) ',
			'workload_de' => 'Заказанный объем работ (Немецкий) ',
			'turnaround_time' => 'Сроки выполнения работ',
			'period_of_works' => 'Период выполнения работ',
			'description' => 'Описание (Русский)',
			'description_uk' => 'Описание (Украинский)',
			'description_en' => 'Описание (Английский)',
			'description_fr' => 'Описание (Французский)',
			'description_de' => 'Описание (Немецкий)',

//            'video_code1' => '',
//            'service1' => '',
//            'video_code2' => '',
//            'service2' => '',
//            'video_code3' => '',
//            'service3' => '',
//            'video_code4' => '',
//            'service4' => '',
//            'video_code5' => '',
//            'service5' => '',
//            'video_code6' => '',
//            'service6' => '',

            'image' => 'Изображение',
			'status' => 'Включена',
			'video_walk_url' => 'Ссылка на видео прогулку',
			'watermark' => 'Водяной знак',
			'show_albom' => 'Показывать альбом',
			'construction' => 'Сейчас строятся',
            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',
			'create_time' => 'Добавлена',
			'update_time' => 'Изменена',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('project_id', $this->project_id);
		$criteria->compare('project_name',$this->project_name,true);
		$criteria->compare('construction_place',$this->construction_place,true);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('show_albom',$this->show_albom);
        $criteria->compare('construction',$this->construction);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    public function getNextProject(){
        if($this->_nextProjectModel === false)
            $this->_nextProjectModel = self::model()->find('create_time < ?', array($this->create_time));
        return $this->_nextProjectModel;
    }

    public function getPrevProject(){
        if($this->_prevProjectModel === false)
            $this->_prevProjectModel = self::model()->resetScope()->reverseOrder()->find('create_time > ?', array($this->create_time));
        return $this->_prevProjectModel;
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = strtolower(app()->translitFormatter->formatUrl($this->project_name));
        return parent::getUrl($params);
    }

    public function getVideos(){
        if(!$this->_videos){
            $i = 1;
            $videoCodeProperty = 'video_code'.$i;
            $videoSevice = 'service'.$i;
            while(isset($this->$videoCodeProperty) && !empty($this->$videoCodeProperty)){
                $image = $this->getVideoMiniImage($this->$videoCodeProperty, $this->$videoSevice);
                $this->_videos[] = array(
                    'service' => $this->$videoSevice,
                    'code' => $this->$videoCodeProperty,
//                'htmlCode' => $this->getVideoInsertHtml($this->$videoCodeProperty, $this->$videoSevice),
                    'miniImageHtml' => isset($image['mini'])?$image['mini']:false,
//                    'largeImageHtml' => isset($image['large'])?$image['large']:false,
                    'videoLink' => $this->getVideoLink($this->$videoCodeProperty, $this->$videoSevice),
                );
                $i++;
                $videoCodeProperty = 'video_code'.$i;
                $videoSevice = 'service'.$i;
            }
        }

        return $this->_videos;
    }

    public function getCountVideos(){
        return count($this->getVideos());
    }

    public function getVideoInsertHtml($code, $service = 1){
        switch ($service){
            case 1:
                return '<iframe width="420" height="315" src="//www.youtube.com/embed/'.$code.'" frameborder="0" allowfullscreen></iframe>';
            case 2:
                return '<iframe src="//player.vimeo.com/video/'.$code.'" width="500" height="282" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            default:
                return false;
        }
    }

    public function getVideoMiniImage($code, $service = 1){
        switch ($service){
            case 1:
                return array(
                    'mini' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">',
                    'large' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/0.jpg" src="//i1.ytimg.com/vi/'.$code.'/0.jpg" alt="" width="480" data-group-key="thumb-group-0">',
                );
                return '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">';
            case 2:
                $var = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$code.php"));
                if(isset($var[0])){
                    return array(
                        'mini' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_medium']), false, false),
                        'large' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_large'], 'style'=>'width:480px;'), false, false),
                    );
                }
            default:
                return false;
        }
    }

    public function getVideoLink($code, $service = 1){
        switch ($service){
            case 1:
                return 'https://youtube.googleapis.com/v/'.$code.'%26hl=pl%26fs=1%26rel=0%26loop=1';
            case 2:
                return 'http://player.vimeo.com/video/'.$code.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ff0179';
            default:
                return false;
        }
    }

    public function getProjectUrl(){
        $projectModel = Project::model()->findByAttributes(array('symbol'=>$this->project_name));
        if($projectModel)
            return $projectModel->getUrl();
        return false;
    }

}
