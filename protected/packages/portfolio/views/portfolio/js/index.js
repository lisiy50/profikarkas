jQuery(function(){

    $('#Portfolio-grid tbody').sortable({
        axis: 'y',
        containment:'parent',
        tolerance:'pointer',
        handle:'.icon-resize-vertical',
        'update':function( event, ui ){
            $.post('portfolio/saveOrder', $('#Portfolio-grid tbody').sortable('serialize'), function(){
                displayMessage('Порядок сохранен', 'success');
            });
        }
    });
    //$('#save-category').click(function(){
    //    $.post('portfolio/saveOrder', $('#Portfolio-grid tbody').sortable('serialize'), function(){
    //        displayMessage('Порядок сохранен', 'success');
    //    });
    //    return false;
    //});

});