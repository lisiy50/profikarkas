<?php
/**
 * @var $dataProvider CActiveDataProvider
 * @var $this PortfolioController
 * @var $model Portfolio
 * @var $portfolio Portfolio
 */
?>
<?php
$this->breadcrumbs = array(
    'Портфолио',
);
?>

<div class="row">

    <div class="span8">
        <?php
        $this->widget('BootMenu', array(
            'activeIconClass' => 'icon-white',
            'items' => array(
                array('label' => 'Все', 'url' => array('index'), 'active' => empty($_GET)),
                array('label' => 'Включенные', 'icon' => 'eye-open', 'url' => array('index', 'Portfolio' => array('status' => Portfolio::STATUS_ENABLED)), 'active' => $model->status == Portfolio::STATUS_ENABLED),
                array('label' => 'Отключенные', 'icon' => 'eye-close', 'url' => array('index', 'Portfolio' => array('status' => Portfolio::STATUS_DISABLED)), 'active' => $model->status == Portfolio::STATUS_DISABLED),
            ),
            'htmlOptions' => array('class' => 'nav-pills')
        ));
        ?>
    </div>

    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
    </div>

</div>

<?php $this->renderPartial('_search', array(
    'model' => $model,
)); ?>



<?php /*$this->widget('BootGridView', array(
    'id' => get_class($model) . '-grid',
    'dataProvider' => $dataProvider,
    'template' => "{items}\n{pager}",
    'columns' => array(
        array(
            'type' => 'html',
            'value' => "'<i class=\"icon-resize-vertical\" style=\"cursor: row-resize;\"></i>'",
            'htmlOptions' => array('style' => 'width: 30px'),
        ),
        array(
            'name' => 'status',
            'header' => '',
            'type' => 'html',
            'value' => '$data->statusIcon',
            'htmlOptions' => array('style' => 'width: 16px'),
        ),
        'project_name',
        array(
            'class' => 'application.widgets.grid.BootButtonColumn',
            'htmlOptions' => array('style' => 'width: 50px'),
            'buttons' => array(
                'view' => array(
                    'visible' => '$data->status==Portfolio::STATUS_ENABLED',
                    'url' => '$data->url',
                    'options' => array(
                        'target' => '_blank'
                    )
                ),
            ),
        ),
    ),
));*/ ?>

<div id="Portfolio-grid" class="grid-view">
    <table class="table table-striped">
        <thead>
        <tbody>
        <?php foreach($dataProvider->data as $portfolio):?>
            <tr class="even" id="Portfolio_id-<?php echo $portfolio->id;?>">
                <td style="width: 30px">
                    <i class="icon-resize-vertical" style="cursor: row-resize;"></i>
                </td>
                <td style="width: 16px">
                    <i class="<?php echo $portfolio->status==Portfolio::STATUS_ENABLED?'icon-eye-open':'icon-eye-close'?>"></i>
                </td>
                <td>
                    <?php echo $portfolio->project_name?>
                </td>
                <td style="width: 50px">
                    <?php if($portfolio->status==Portfolio::STATUS_ENABLED):?>
                    <a target="_blank" title="Просмотреть" rel="tooltip" href="<?php echo $portfolio->getUrl()?>"><i class="icon-zoom-in"></i></a>
                    <?php endif;?>
                    <a class="update" title="Редактировать" rel="tooltip" href="<?php echo app()->createUrl('portfolio/update', array('id'=>$portfolio->id))?>"><i class="icon-pencil"></i></a>
<!--                    <a class="delete" title="Удалить" rel="tooltip" href="--><?php //echo app()->createUrl('portfolio/delete', array('id'=>$portfolio->id))?><!--"><i class="icon-trash"></i></a>-->
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>