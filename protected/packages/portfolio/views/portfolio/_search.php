<?php $form=$this->beginWidget('BootActiveForm', array(
    'id'=>'search-form',
    'type'=>BootActiveForm::TYPE_INLINE,
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

    <?php echo $form->hiddenField($model,'status'); ?>
    <?php echo $form->textFieldRow($model,'id',array('maxlength'=>11,'class'=>'span1')); ?>
    <?php echo $form->textFieldRow($model,'project_name',array('maxlength'=>255,'class'=>'span6')); ?>
    <?php echo $form->dropDownList($model,'construction',array(1=>'Сейчас строится', 2=>'Построино'),array('class'=>'span2', 'prompt'=>'тип записи')); ?>

    <?php echo CHtml::htmlButton('<i class="icon-search"></i> Поиск', array('class'=>'btn pull-right','type'=>'submit')); ?>

<?php $this->endWidget(); ?>