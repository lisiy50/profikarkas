<div id="pjax"  data-url="//extraImage/_image_view" data-id="<?=$model->id?>" data-name="<?=get_class($model)?>">
    <table id="sortable-images" class="table table-striped">
        <thead>
        <th></th>
        <th style="text-align: center;">В проекте</th>
        <th></th>
        <th></th>
        <th></th>
        </thead>
        <tbody>
        <tr id="empty-images" <?php if ($model->hasImages): ?>style="display: none;"<?php endif; ?> >
            <td colspan="4">У портфолио нету изображений</td>
        </tr>
        <?php foreach ($model->images as $image): ?>
            <?php $this->renderPartial('//extraImage/_image_view', array('model' => $image)); ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="pull-left" style="width: 170px;margin-left: 20px;">
        <a class="btn btn-success btn-small" id="save-images" href="#">Сохранить порядок/текст</a>
    </div>
    <div class="pull-left">
<!--        --><?php //$this->widget('Uplodify', array(
//        'name' => 'Image',
//        'uploadUrl' => Yii::app()->createUrl('extraImage/upload', array('id' => $model->id, 'owner' => get_class($model))),
//        'options' => array(
//            'onUploadSuccess' => 'js:function(event, data){ $("#sortable-images tbody").append(data); $("#empty-images").hide(); }',
//            'multi' => true,
//            'fileTypeExts' => '*.jpg;*.gif;*.jpeg;*.png;',
//            'fileTypeDesc' => 'Изображение',
//            'progressData' => 'all',
//            'queueID' => 'progress',
//        ),
//    )); ?>
        <input type="file" multiple="multiple" accept=".txt,image/*">
        <a href="#" class="upload_files button"
           data-url="<?= Yii::app()->createUrl("image/load")?>"
           data-id="<?=$model->id?>"
           data-name="<?=get_class($model)?>"
        >Загрузить файлы</a>
        <div class="ajax-reply"></div>
    </div>
</div>

<div class="row">
    <div class="span6" id="progress"></div>
</div>