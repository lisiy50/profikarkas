<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'project_name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'project_name_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'project_name_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'project_name_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'project_name_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    $array = array();
    $projects = Project::model()->order('symbol_sort_letter,symbol_sort_number')->findAll();
    foreach ($projects as $project) {
        $array[$project->id] = $project->symbol . ' ' . $project->variation_symbol;
    }

    echo $form->dropDownListRow($model, 'project_id', $array, array('prompt'=>''));

    echo $form->imageFileRow($model, 'image');

    echo $form->textFieldRow($model, 'construction_place', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'construction_place_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'construction_place_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'construction_place_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'construction_place_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'gross_area', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'turnaround_time', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge',
    ));

    echo $form->textFieldRow($model, 'period_of_works', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    ?>

    <div class="row">
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code1', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service1', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code2', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service2', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code3', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service3', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code4', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service4', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code5', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service5', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code6', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service6', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
    </div>

    <?php
    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'show_albom', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'construction', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    echo $form->textFieldRow($model, 'video_walk_url', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    ?>

    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php
    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');

    echo $form->widgetFieldRow($model, 'description_en', 'Redactor');

    echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');

    echo $form->widgetFieldRow($model, 'description_de', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'seo' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            )
        ),
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
