<?php
class Enterprise1cPackage extends BasePackage {

    public function getTopMenu() {
        return array(
            'Контент' => array('order'=>3, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Интеграция с 1C' => array('url'=>array('enterprise1c/index')),
            )),
        );
    }
}