<div class="row">
    <div class="span6">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Интеграция с 1С
        </h4>
    </div>
</div>

<hr>

<div class="row">

    <div class="span9">

    <h4>Настройка интеграции характеристик товаров с 1С.</h4>

    <div class="form">

        <?php $form=$this->beginWidget('BootActiveForm'); ?>

        <table class="table table-striped">
        <tr>
            <th>Название на сайте</th>
            <th>Название в 1С</th>
        </tr>
        <?php foreach($items as $i=>$item): ?>
        <tr>
            <td><?php echo $item->pack->name; ?>: <?php echo $item->name; ?> <?php if($item->unit) echo '('.$item->unit.')'; ?></td>
            <td>
                <?php echo $form->textField($item, "[$i]id_1c",array('class'=>'input-xlarge','maxlength'=>255));?>
                <?php echo $form->error($item,'id_1c'); ?>
            </td>
        </tr>
        <?php endforeach; ?>
        </table>

        <button type="submit" class="btn btn-success btn-small"><i class="icon-ok icon-white"></i> Сохранить</button>

        <?php $this->endWidget(); ?>

    </div>


    </div>

    <div class="span3">
        <?php $this->widget('BootMenu', array(
        'activeIconClass'=>'icon-white',
        'items'=>$this->menu,
        'htmlOptions'=>array(
            'class'=>'nav-list'
        )
    )); ?>
    </div>

</div>
<h5 class="muted">Интеграция с «1С: Управлением торговлей» реализована через формат <a target="_blank" href="http://www.1c.ru/rus/products/1c/integration/cml.htm">CommerceML</a>.</h5>