<div class="row">
    <div class="span6">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Интеграция с 1С
        </h4>
    </div>
</div>

<hr>

<div class="row">

    <div class="span9">
        <div class="form">

            <h4>Автоматический обмен данными</h4>

            <?php $form=$this->beginWidget('BootActiveForm', array(
                'id'=>'config-form',
                'enableAjaxValidation'=>true,
            )); ?>
            <div class="control-group">
                <label for="url_export">Адрес скрипта синхронизации</label>
                <input type="text" style="cursor:text" id="url_export" value="<?php echo $this->createAbsoluteUrl('exchange'); ?>" readonly="readonly" class="input-xxlarge" onclick="this.select();">
            </div>
            <?php echo $form->textFieldRow($model, 'enterprise_1c_login', array(
                'maxlength'=>64,
                'class'=>'input-xlarge'
            )); ?>

            <?php echo $form->textFieldRow($model, 'enterprise_1c_password', array(
                'maxlength'=>64,
                'class'=>'input-xlarge'
            )); ?>

            <button type="submit" class="btn btn-success btn-small"><i class="icon-ok icon-white"></i> Сохранить</button>

            <?php $this->endWidget(); ?>

            <h4>Обмен данными через файл</h4>

            <?php echo CHtml::beginForm('export', 'get'); ?>

            <div class="control-group">
                <label class="radio">
                    <?php echo CHtml::radioButton('size', true, array('value'=>'new'))?> Только измененные после даты последнего экспорта
                </label>
                <label class="radio">
                    <?php echo CHtml::radioButton('size', false, array('value'=>'all'))?> Все заказы
                </label>
                <label class="radio" onclick="$('#amount').select();">
                    <?php echo CHtml::radioButton('size', false, array('value'=>'amount'))?> Последние <input type="text" style="height: 10px;" id="amount" class="input-mini nomargin" name="amount" value="50" size="4"> шт.
                </label>
            </div>

            <div class="control-group">
                <label for="download" class="checkbox inline">
                    <?php echo CHtml::checkBox('download', true)?> скачать файл
                </label>
            </div>

            <button type="submit" class="btn btn-primary btn-small"><i class="icon-download icon-white"></i> Экспортировать в XML-файл</button>

            <?php echo CHtml::endForm(); ?>

        </div>
    </div>

    <div class="span3">
        <?php $this->widget('BootMenu', array(
        'activeIconClass'=>'icon-white',
        'items'=>$this->menu,
        'htmlOptions'=>array(
            'class'=>'nav-list'
        )
    )); ?>
    </div>

</div>
<h5 class="muted">Интеграция с «1С: Управлением торговлей» реализована через формат <a target="_blank" href="http://www.1c.ru/rus/products/1c/integration/cml.htm">CommerceML</a>.</h5>






