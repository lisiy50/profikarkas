<?php

class ReviewController extends BackendController
{
    public $layout = '//layouts/column1';

    public $icon = 'picture';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
//            'index' => array(
//                'class' => 'application.backend.actions.ListAction',
//            ),
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Отзыв добавлен',
                'errorMessage' => 'Отзыв не добавлен',
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Отзыв &quot;{name}&quot; удален',
            ),
        );
    }

    public function actionIndex()
    {
        $model = new Review('search');
        $model->unsetAttributes();
        if (isset($_GET['Review']))
            $model->attributes = $_GET['Review'];

        $dataProvider = $model->search();
        $dataProvider->pagination->pageSize = 500;
        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSaveOrder()
    {
        if (isset($_POST['Review_id'])) {
            $reviews = Review::model()->findAllByAttributes(array('id' => $_POST['Review_id']));
            foreach ($reviews as $review) {
                $review->position = array_search($review->id, $_POST['Review_id']);
                $review->update(array('position'));
            }
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

}
