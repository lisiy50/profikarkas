jQuery(function() {
    $('#Review-grid tbody').sortable({
        axis: 'y',
        containment: 'parent',
        tolerance: 'pointer',
        handle: '.icon-resize-vertical',
        'update': function (event, ui) {
            console.log($('#Review-grid tbody').sortable('serialize'));
            $.post('review/saveOrder', $('#Review-grid tbody').sortable('serialize'), function () {
                displayMessage('Порядок сохранен', 'success');
            });
        }
    });
});

