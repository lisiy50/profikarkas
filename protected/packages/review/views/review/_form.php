<div class="form">

    <?php
    /** @var BootActiveForm $form */
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'title', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'title_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    ?>

    <fieldset class="well">
        <legend>Клиент</legend>

        <i>* Для отображения на главной необходимо что бы было заполнено название проекта выше</i>
        <br>
        <br>


        <div class="row">
            <div class="span5">

                <?php echo $form->textFieldRow($model, 'client_name', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_name_uk', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_name_en', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_name_fr', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_name_de', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
            </div>
            <div class="span5">
                <?php echo $form->textFieldRow($model, 'client_post', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_post_uk', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_post_en', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_post_fr', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
                <?php echo $form->textFieldRow($model, 'client_post_de', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>
            </div>
            <div class="span5">

                <?php echo $form->imageFileRow($model, 'client_image'); ?>
                <?php echo $form->textFieldRow($model, 'client_facebook', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge'
                )); ?>

                <?php echo $form->checkBoxRow($model, 'show_on_landing', array('uncheckValue' => LibraryRecord::STATUS_DISABLED)); ?>
            </div>
        </div>
    </fieldset>

    <fieldset class="well">
        <legend>Информация о проекте</legend>
        <div class="row">
            <div class="span5">
                <?php echo $form->textFieldRow($model, 'project_name', array(
                    'maxlength' => 255,
                    'class' => 'input-xlarge',
                    'append' => '',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_name_uk', array(
                    'maxlength' => 255,
                    'class' => 'input-xlarge',
                    'append' => '',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_name_en', array(
                    'maxlength' => 255,
                    'class' => 'input-xlarge',
                    'append' => '',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_name_fr', array(
                    'maxlength' => 255,
                    'class' => 'input-xlarge',
                    'append' => '',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_name_de', array(
                    'maxlength' => 255,
                    'class' => 'input-xlarge',
                    'append' => '',
                )); ?>
            </div>
        </div>
        <div class="row">
            <div class="span5">
                <?php echo $form->textFieldRow($model, 'project_area', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => 'm<sup>2</sup>',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_volume', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => 'm<sup>3</sup>',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_roof_angle', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => '°',
                )); ?>
            </div>
            <div class="span5">
                <?php echo $form->textFieldRow($model, 'project_built_area', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => 'm<sup>2</sup>',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_height', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => 'm',
                )); ?>
                <?php echo $form->textFieldRow($model, 'project_roof_area', array(
                    'maxlength' => 64,
                    'class' => 'input-xlarge',
                    'append' => 'm<sup>2</sup>',
                )); ?>
            </div>
        </div>
    </fieldset>


    <div class="row">
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code1', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service1', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code2', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service2', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code3', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service3', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code4', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service4', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code5', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service5', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code6', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service6', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
    </div>

    <?php
    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    ?>



    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php
    echo $form->widgetFieldRow($model, 'description', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_de', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'seo' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            )
        ),
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
