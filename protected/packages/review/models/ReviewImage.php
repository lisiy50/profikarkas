<?php
class ReviewImage extends ExtraImage {

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'review_'),
            'mini' => array('storage/review', 102, 60, 'resize'=>'fill', 'prefix'=>'mini_'),
            'small' => array('storage/review', 389, 204, 'prefix'=>'small_', 'resize'=>'fill'),
            'more' => array('storage/review', 1000, 550, 'resize'=>'max', 'prefix'=>'more_'),
            'large' => array('storage/review', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
            'original' => array('storage/review', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->getOwner()->watermark == Portfolio::STATUS_ENABLED){
            $images['more'] = array('storage/review', 1000, 550, 'resize'=>'max', 'prefix'=>'more_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['large'] = array('storage/review', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }
        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> $images,
            ),
        );
    }

}