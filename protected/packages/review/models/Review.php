<?php

/**
 * This is the model class for table "{{review}}".
 *
 * The followings are the available columns in table '{{review}}':
 * @property integer $id
 * @property string $title
 * @property string $title_uk
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_de
 * @property string $project_name
 * @property string $project_name_uk
 * @property string $project_name_en
 * @property string $project_name_fr
 * @property string $project_name_de
 * @property string $project_area
 * @property string $project_built_area
 * @property string $project_volume
 * @property string $project_height
 * @property string $project_roof_angle
 * @property string $project_roof_area
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_fr
 * @property string $description_de
 *
 * @property string $video_code1
 * @property string $video_code2
 * @property string $video_code3
 * @property string $video_code4
 * @property string $video_code5
 * @property string $video_code6
 * @property integer $service1
 * @property integer $service2
 * @property integer $service3
 * @property integer $service4
 * @property integer $service5
 * @property integer $service6
 *
 * @property integer $status
 * @property integer $show_on_landing
 * @property integer $watermark
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 * @property string $client_name
 * @property string $client_name_uk
 * @property string $client_name_en
 * @property string $client_name_fr
 * @property string $client_name_de
 * @property string $client_post
 * @property string $client_post_uk
 * @property string $client_post_en
 * @property string $client_post_fr
 * @property string $client_post_de
 * @property string $client_image
 * @property string $client_facebook
 * @property int $position
 * @property string $create_time
 * @property string $update_time
 */
class Review extends LibraryRecord
{
    public $watermark = self::STATUS_ENABLED;

    private $_videos = array();
    private $_nextProjetcModel = false;
    private $_prevProjetcModel = false;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{review}}';
	}

    public function behaviors(){
  	    return array(
  		    'CTimestampBehavior' => array(
  		  	    'class' => 'zii.behaviors.CTimestampBehavior',
  		  	    'createAttribute' => 'create_time',
  			    'updateAttribute' => 'update_time',
  		    ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'review/view',
            ),
            'imageResizeBehavior' => array(
                'class' => 'ImageResizeBehavior',
                'fields' => array('description'),
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'client_image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 83, 83, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'large' => array('storage/review', 770, 770, 'resize'=>'max', 'prefix' => 'large_'),
                ),
            ),
  	    );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, title_uk, title_en, title_fr, title_de, project_name, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'strip_tags'),
            array('title, project_name, project_name_uk, project_name_en, project_name_fr, project_name_de, video_code1, video_code2, video_code3, video_code4, video_code5, video_code6', 'filter', 'filter'=>'trim'),
//            array('description','filter','filter'=>array($obj=new EHtmlPurifier(),'purify')),
			array('title', 'required'),
			array('title, project_name, client_facebook', 'length', 'max'=>255),
			array('client_name, client_name_uk, client_name_en, client_name_fr, client_name_de, client_post, client_post_uk, client_post_en, client_post_fr, client_post_de', 'length', 'max'=>32),
			array('status, watermark, service1, service2, service3, service4, service5, service6, h1_size,h2_size,h3_size,h4_size,show_on_landing, project_area, project_built_area, project_volume, project_height, project_roof_angle, project_roof_area, position', 'numerical'),
            array('status, watermark,show_on_landing', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('client_image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('client_image', 'unsafe'),
            array('description, description_uk, description_en, description_fr, description_de', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, project_name, status, show_on_landing, project_area, project_built_area, project_volume, project_height, project_roof_angle, project_roof_area', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'firstImage'=>array(self::HAS_ONE, 'ReviewImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'images'=>array(self::HAS_MANY, 'ReviewImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'ReviewImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
        );
	}

    public function defaultScope(){
        return array(
            'order' => 'position ASC',
        );
    }

    public function scopes(){
        return array(
            'directOrder' => array(
                'order' => 'position ASC',
            ),
            'reverseOrder' => array(
                'order' => 'position DESC',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заглавие (Русский)',
			'title_uk' => 'Заглавие (Украинский)',
			'title_en' => 'Заглавие (Английский)',
			'title_fr' => 'Заглавие (Французский)',
			'title_de' => 'Заглавие (Немецкий)',
			'project_name' => 'Название проекта (Русский)',
			'project_name_uk' => 'Название проекта (Украинский)',
			'project_name_en' => 'Название проекта (Английский)',
			'project_name_fr' => 'Название проекта (Французский)',
			'project_name_de' => 'Название проекта (Немецкий)',
			'description' => 'Текст (Русский)',
			'description_uk' => 'Текст (Украинский)',
			'description_en' => 'Текст (Английский)',
			'description_fr' => 'Текст (Французский)',
			'description_de' => 'Текст (Немецкий)',

//            'video_code1' => '',
//            'service1' => '',
//            'video_code2' => '',
//            'service2' => '',
//            'video_code3' => '',
//            'service3' => '',
//            'video_code4' => '',
//            'service4' => '',
//            'video_code5' => '',
//            'service5' => '',
//            'video_code6' => '',
//            'service6' => '',

            'watermark' => 'Водяной знак',
			'status' => 'Включена',
			'show_on_landing' => 'Показать на главной',
			'create_time' => 'Добавлена',
			'update_time' => 'Изменена',
            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(18)',
            'client_name' => 'Имя клиента (Русский)',
            'client_name_uk' => 'Имя клиента (Украинский)',
            'client_name_en' => 'Имя клиента (Английский)',
            'client_name_fr' => 'Имя клиента (Французский)',
            'client_name_de' => 'Имя клиента (Немецкий)',
            'client_post' => 'Должность клиента (Русский)',
            'client_post_uk' => 'Должность клиента (Украинский)',
            'client_post_en' => 'Должность клиента (Английский)',
            'client_post_fr' => 'Должность клиента (Французский)',
            'client_post_de' => 'Должность клиента (Немецкий)',
            'client_image' => 'Фото клиента',
            'client_facebook' => 'Ссылка на профиль в фейсбуке',
            'project_area' => 'Площадь дома',
            'project_built_area' => 'Площадь застройки',
            'project_volume' => 'Кубатура',
            'project_height' => 'Высота',
            'project_roof_angle' => 'Угол наклона крыши',
            'project_roof_area' => 'Площадь крыши',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('show_on_landing',$this->show_on_landing);
        $criteria->compare('project_area',$this->project_area);
        $criteria->compare('project_built_area',$this->project_built_area);
        $criteria->compare('project_volume',$this->project_volume);
        $criteria->compare('project_height',$this->project_height);
        $criteria->compare('project_roof_angle',$this->project_roof_angle);
        $criteria->compare('project_roof_area',$this->project_roof_area);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'position ASC',
            ),
		));
	}

    public function getNextProject(){
        if($this->_nextProjetcModel === false)
            $this->_nextProjetcModel = self::model()->find('create_time < ?', array($this->create_time));
        return $this->_nextProjetcModel;
    }

    public function getPrevProject(){
        if($this->_prevProjetcModel === false)
            $this->_prevProjetcModel = self::model()->resetScope()->reverseOrder()->find('create_time > ?', array($this->create_time));
        return $this->_prevProjetcModel;
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->title);
        return parent::getUrl($params);
    }

    public function getVideos(){
        if(!$this->_videos){
            $i = 1;
            $videoCodeProperty = 'video_code'.$i;
            $videoSevice = 'service'.$i;
            while(isset($this->$videoCodeProperty) && !empty($this->$videoCodeProperty)){
                $image = $this->getVideoMiniImage($this->$videoCodeProperty, $this->$videoSevice);
                $this->_videos[] = array(
                    'service' => $this->$videoSevice,
                    'code' => $this->$videoCodeProperty,
//                'htmlCode' => $this->getVideoInsertHtml($this->$videoCodeProperty, $this->$videoSevice),
                    'miniImageHtml' => isset($image['mini'])?$image['mini']:false,
                    'largeImageHtml' => isset($image['large'])?$image['large']:false,
                    'videoLink' => $this->getVideoLink($this->$videoCodeProperty, $this->$videoSevice),
                );
                $i++;
                $videoCodeProperty = 'video_code'.$i;
                $videoSevice = 'service'.$i;
            }
        }

        return $this->_videos;
    }

    public function getCountVideos(){
        return count($this->getVideos());
    }

    public function getVideoInsertHtml($code, $service = 1){
        switch ($service){
            case 1:
                return '<iframe width="420" height="315" src="//www.youtube.com/embed/'.$code.'" frameborder="0" allowfullscreen></iframe>';
            case 2:
                return '<iframe src="//player.vimeo.com/video/'.$code.'" width="500" height="282" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            default:
                return false;
        }
    }

    public function getVideoMiniImage($code, $service = 1){
        switch ($service){
            case 1:
                return array(
                    'mini' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">',
                    'large' => '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/0.jpg" src="//i1.ytimg.com/vi/'.$code.'/0.jpg" alt="" width="400" data-group-key="thumb-group-0">',
                );
                return '<img data-thumb="//i1.ytimg.com/vi/'.$code.'/default.jpg" src="//i1.ytimg.com/vi/'.$code.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">';
            case 2:
                $var = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$code.php"));
                if(isset($var[0])){
                    return array(
                        'mini' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_medium']), false, false),
                        'large' => CHtml::tag('img', array('src'=>$var[0]['thumbnail_large'], 'style'=>'width:400px;'), false, false),
                    );
                }
            default:
                return false;
        }
    }

    public function getVideoLink($code, $service = 1){
        switch ($service){
            case 1:
                return 'https://www.youtube.com/embed/'.$code;
            case 2:
                return 'http://player.vimeo.com/video/'.$code.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ff0179';
            default:
                return false;
        }
    }

    private $_project;

    /**
     * @return Project
     */
    public function getProject()
    {
        if (!$this->_project) {
            $this->_project = Project::model()->findByAttributes(array('symbol' => $this->project_name));
        }
        return $this->_project;
    }

    /**
     * используется на лендинге
     * @param int $symbols
     * @return string
     */
    public function getShortDescription($symbols = 500)
    {
        $text = trim(strip_tags($this->description));
        $text = wordwrap($text, $symbols, '|||');
        $textArr = explode('|||', $text);
        $text = trim($textArr[0]);
        return $text;
    }

    public function getShortDescriptionUk($symbols = 500)
    {
        $text = trim(strip_tags($this->description_uk));
        $text = wordwrap($text, $symbols, '|||');
        $textArr = explode('|||', $text);
        $text = trim($textArr[0]);
        return $text;
    }

}
