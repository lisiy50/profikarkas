<?php
class SliderItemPackage extends BasePackage {

    public $import=array(
        'sliderItem.models.SliderItem'
    );

    public function getTopMenu() {
        return array(
            'Контент'=>array('order'=>2, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Слайдер на главной'=>array('order'=>1, 'url'=>array('sliderItem/index')),
            ))
        );
    }

}