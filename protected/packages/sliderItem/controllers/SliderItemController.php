<?php
class SliderItemController extends BackendController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    public $icon = 'page';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Слайд удален',
            ),
        );
    }

    public function actionIndex() {
        $sliderItems=SliderItem::model()->findAll();

        if(isset($_POST['SliderItem'])) {
            $valid=true;

            foreach($sliderItems as $sliderItem)
            {
                $i=$sliderItem->id;

                if(isset($_POST['SliderItem'][$i]))
                    $sliderItem->attributes=$_POST['SliderItem'][$i];

                $sliderItem->position=array_search($i, array_keys($_POST['SliderItem']));

                $fileImage=CUploadedFile::getInstanceByName("SliderItem[$i][image]");

                if($fileImage)
                    $sliderItem->setImageFile($fileImage);

                $valid=$sliderItem->save() && $valid;
            }

            if($valid) {
                Yii::app()->user->setFlash('success', "Изменения сохранены");
                $sliderItems=SliderItem::model()->findAll();
            } else {
                Yii::app()->user->setFlash('error', "Изменения не сохранены");
            }
        }

        $newModel=new SliderItem();

        if(isset($_POST['create']) && isset($_POST['ItemCreate'])) {
            $newModel->attributes = $_POST['ItemCreate'];

            $fileImage=CUploadedFile::getInstanceByName("ItemCreate[image]");

            if($fileImage)
                $newModel->setImageFile($fileImage);

            if ($newModel->save()) {
                Yii::app()->user->setFlash('success', 'Слайд добавлен');
                $this->refresh();
            }
        }

        $this->render('index', array(
            'newModel'=>$newModel,
            'sliderItems'=>$sliderItems,
        ));
    }
}