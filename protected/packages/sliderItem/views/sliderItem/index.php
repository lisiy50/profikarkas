<?php
$this->breadcrumbs=array(
	'Слайдер на главной',
);
?>

<div class="form">
    <?php $form=$this->beginWidget('BootActiveForm', array(
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <div class="row" style="height: 40px;">
        <div class="pull-right">
            <button class="btn btn-small btn-success" type="submit"><i class="icon-ok icon-white"></i> Сохранить</button>
        </div>
    </div>

    <table class="table">
        <colgroup>
            <col style="width: 30px;">
            <col/>
            <col/>
            <col style="width: 30px;">
        </colgroup>
        <tbody>
        <?php foreach($sliderItems as $sliderItem): $i=$sliderItem->id;?>
            <tr style="background: #ffffff">
                <td><i class="icon-resize-vertical" style="margin-top: 6px; cursor: row-resize;"></i></td>
                <td><img class="img-polaroid" style="width: 300px;" src="<?php echo $sliderItem->getImageUrl('thumb'); ?>"></td>
                <td>
                    <?php echo $form->textField($sliderItem, "[$i]url", array('placeholder'=>$sliderItem->getAttributeLabel('url'), 'class'=>'span5')); ?>
                    <?php echo $form->error($sliderItem, "[$i]url"); ?>

                    <?php echo $form->labelEx($sliderItem, "[$i]image"); ?>
                    <?php echo $form->fileField($sliderItem,"[$i]image"); ?>
                    <?php echo $form->error($sliderItem, "[$i]image"); ?>

                    <?php $this->widget('Redactor', array(
                        'model'=>$sliderItem,
                        'attribute'=>"[$i]content",
                        'options'=>array(
                            'minHeight'=>'55',
                        ),
                    )); ?>
                </td>
                <td>
                    <?php echo CHtml::link('<i class="icon-trash" style="margin-top: 6px; cursor: pointer;"></i>', '#', array('submit'=>array('delete','id'=>$sliderItem->id),'confirm'=>'Вы уверены, что хотите удалить слайд?'));?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td></td>
            <td><img class="img-polaroid" style="width: 300px;" src="<?php echo $newModel->getImageUrl('thumb'); ?>"></td>
            <td>
                <?php echo $form->textField($newModel, 'url', array('placeholder'=>$newModel->getAttributeLabel('url'), 'class'=>'span5', 'name'=>'ItemCreate[url]')); ?>
                <?php echo $form->error($newModel, 'url'); ?>

                <?php echo $form->labelEx($newModel, 'image'); ?>
                <?php echo $form->fileField($newModel, 'image', array('name'=>'ItemCreate[image]')); ?>
                <?php echo $form->error($newModel, 'image'); ?>

                <?php $this->widget('Redactor', array(
                    'name'=>'ItemCreate[content]',
                    'value'=>$newModel->content,
                    'options'=>array(
                        'minHeight'=>'55',
                    ),
                )); ?>
            </td>
            <td></td>
        </tr>
        </tfoot>
    </table>

    <button class="btn btn-success" style="margin-left: 40px;" name="create" type="submit"><i class="icon-plus icon-white"></i> Добавить слайд</button>

    <?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
    jQuery(function(){
        $('.form table.table tbody').sortable({
            axis: 'y',
            containment:'parent',
            tolerance:'pointer',
            handle:'.icon-resize-vertical'
        });
    })
</script>