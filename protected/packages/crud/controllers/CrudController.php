<?php
class CrudController extends BackendController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    public $icon = 'database';

    protected $form_attributes;
    protected $search_attributes;
    protected $_grid_columns;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        $modelClass=$this->package->modelClass;
        $nameAttribute=$this->package->nameAttribute;

        return array(
            'index' => array(
                'class' => 'application.backend.actions.ListAction',
                'modelClass' => $modelClass
            ),
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Запись успешно добавлена',
                'errorMessage' => 'Запись не добавлена',
                'modelClass' => $modelClass
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
                'modelClass' => $modelClass
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Запись &quot;{'.$nameAttribute.'}&quot; удалена',
                'modelClass' => $modelClass
            ),
        );
    }

    public function getViewPath()
    {
        if(($package=$this->getPackage())===null)
            $package=Yii::app();
        return $package->getViewPath().DIRECTORY_SEPARATOR.'crud';
    }

    public function getFormAttributes($model) {
        if($this->form_attributes===null) {
            if($this->package->formAttributes) {
                $attributes=$this->normalizeAttributes($model, $this->package->formAttributes);
            } else {
                $attributes=array();

                $columns=$model->getMetaData()->columns;

                $safeAttributeNames=array_flip(array_intersect_key(array_flip($model->attributeNames()), array_flip($model->getSafeAttributeNames())));

                foreach($safeAttributeNames as $name) {
                    if(isset($attributes[$name]))
                        continue;

                    $options=array('type'=>'text');
                    $column=$columns[$name];

                    if($name=='status')
                    {
                        $options['type']='checkBox';
                        $options['uncheckValue']=LibraryRecord::STATUS_DISABLED;
                    }
                    else if(strpos($column->dbType, 'varchar')===0)
                    {
                        $options['maxlength']=$column->size;
                        if($column->size>32)
                            $options['class']='span6';
                        else
                            $options['class']='span3';
                    }
                    else if(strpos($column->dbType, 'int')===0)
                    {
                        $name=$column->name;
                        if(strpos($name, '_id')!==false)
                            $name=substr($name, 0, -3);

                        $func='get'.$this->pluralize(ucfirst($name)).'List';

                        if(method_exists($model, $func)) {
                            $options['type']='dropDownList';
                            $options['data']=$model->$func();

                            if($column->allowNull)
                                $options['prompt']='';

                            $options['class']='span6';
                        } else {
                            $options['class']='span3';
                        }
                    }
                    else if($column->dbType=='text')
                    {
                        $options['type']='widget';
                        $options['className']='Redactor';
                    }
                    else if($column->dbType=='date')
                    {
                        $options=CMap::mergeArray($options, array(
                            'type'=>'widget',
                            'className'=>'zii.widgets.jui.CJuiDatePicker',
                            'language'=>'ru',
                            'options' => array(
                                'dateFormat' => 'yy-mm-dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'input-small'
                            ),
                        ));
                    }

                    $attributes[$name]=$options;
                }

                foreach($model->behaviors() as $behavior) {
                    if($behavior['class']!='ImageUploadBehavior') {
                        continue;
                    }
                    $name=$behavior['fileAttribute'];
                    $attributes[$name]['type']='image';
                }
            }
            $this->form_attributes=$attributes;
        }

        return $this->form_attributes;
    }

    public function getSearchAttributes($model) {
        if($this->search_attributes===null) {
            $packageAttributes=$this->package->searchAttributes;
            if(empty($packageAttributes))
                return array();

            $columns=$model->getMetaData()->columns;
            $attributes=array();
            $spanClass='span'.max(floor(10/count($packageAttributes)), 1);

            foreach($packageAttributes as $name=>$options) {
                if(is_int($name)) {
                    $name=$options;
                    $options=array();
                }

                $dbType='';
                if(isset($columns[$name])) {
                    $dbType=$columns[$name]->dbType;
                }

                if(empty($options['type']) && strpos($dbType, 'int')===0 && strpos($name, '_id')!==false)
                {
                    $options['type']='dropDownList';
                    if(!isset($options['class']))
                        $options['class']=$spanClass;
                }
                else if(empty($options['type']) && $dbType=='date') {
                    $options=CMap::mergeArray($options, array(
                        'type'=>'widget',
                        'className'=>'zii.widgets.jui.CJuiDatePicker',
                        'language'=>'ru',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                        ),
                        'htmlOptions' => array(
                            'class' => 'input-small',
                            'placeholder'=>$model->getAttributeLabel($name)
                        ),
                    ));
                }
                else {
                    $options['type']='text';
                    if(!isset($options['class']))
                        $options['class']=$spanClass;
                }

                if(!empty($options['dataExpression'])) {
                    $options['data']=$this->evaluateExpression($options['dataExpression'],array('data'=>$model));
                    $options['prompt']='';
                    unset($options['dataExpression']);
                }

                if($options['type']=='dropDownList' && empty($options['data']) && strpos($name, '_id')!==false) {
                    $list='get'.$this->pluralize(ucfirst(substr($name, 0, -3))).'List';

                    if(method_exists($model, $list)) {
                        $options['data']=$model->$list();
                        $options['prompt']='';
                    } else {
                        $options['data']=array();
                    }
                }

                $attributes[$name]=$options;
            }
            $this->search_attributes=$attributes;
        }

        return $this->search_attributes;
    }

    public function getGridColumns($model) {
        if($this->_grid_columns===null) {
            $columns=$this->package->gridColumns;

            if($columns===null)
                $columns=array($this->package->nameAttribute);

            if($model instanceof LibraryRecord) {
                array_unshift($columns, array(
                    'name' => 'status',
                    'header'=>'',
                    'type' => 'html',
                    'value' => '$data->statusIcon',
                    'htmlOptions'=>array('style'=>'width: 16px'),
                ));

                array_push($columns, array(
                    'class'=>'application.widgets.grid.BootButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 50px'),
                    'buttons'=>array(
                        'view'=>array(
                            'visible'=>'$data->status==LibraryRecord::STATUS_ENABLED',
                            'url'=>'$data->url',
                        ),
                    ),
                ));
            } else {
                array_push($columns, array(
                    'class'=>'application.widgets.grid.BootButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 32px'),
                    'template'=>'{update} {delete}',
                ));
            }
            $this->_grid_columns=$columns;
        }

        return $this->_grid_columns;
    }

    protected function normalizeAttributes($model, $attributes) {
        $result=array();

        foreach($attributes as $name=>$options) {
            if(is_int($name)) {
                $name=$options;
                $options=array();
            }

            if(empty($options['type']))
                $options['type']='text';

            if(!empty($options['dataExpression'])) {
                $options['data']=$this->evaluateExpression($options['dataExpression'],array('data'=>$model));
                unset($options['dataExpression']);
            }

            $result[$name]=$options;
        }

        return $result;
    }

    public function pluralize($name)
    {
        $rules=array(
            '/(m)ove$/i' => '\1oves',
            '/(f)oot$/i' => '\1eet',
            '/(c)hild$/i' => '\1hildren',
            '/(h)uman$/i' => '\1umans',
            '/(m)an$/i' => '\1en',
            '/(t)ooth$/i' => '\1eeth',
            '/(p)erson$/i' => '\1eople',
            '/([m|l])ouse$/i' => '\1ice',
            '/(x|ch|ss|sh|us|as|is|os)$/i' => '\1es',
            '/([^aeiouy]|qu)y$/i' => '\1ies',
            '/(?:([^f])fe|([lr])f)$/i' => '\1\2ves',
            '/(shea|lea|loa|thie)f$/i' => '\1ves',
            '/([ti])um$/i' => '\1a',
            '/(tomat|potat|ech|her|vet)o$/i' => '\1oes',
            '/(bu)s$/i' => '\1ses',
            '/(ax|test)is$/i' => '\1es',
            '/s$/' => 's',
        );
        foreach($rules as $rule=>$replacement)
        {
            if(preg_match($rule,$name))
                return preg_replace($rule,$replacement,$name);
        }
        return $name.'s';
    }

}