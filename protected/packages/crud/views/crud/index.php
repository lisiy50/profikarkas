<?php
$this->breadcrumbs=array(
    $this->package->label,
);
?>

<div class="row" style="height: 40px;">

<?php if($model instanceof LibraryRecord): ?>
<div class="span8">
<?php
$this->widget('BootMenu', array(
    'activeIconClass'=>'icon-white',
    'items'=>array(
        array('label'=>'Все', 'icon'=>'th-list', 'url'=>array('index'), 'active'=>empty($_GET)),
        array('label'=>'Активные', 'icon'=>'eye-open', 'url'=>array('index', get_class($model)=>array('status'=>LibraryRecord::STATUS_ENABLED)),'active'=>$model->status==LibraryRecord::STATUS_ENABLED),
        array('label'=>'Скрытые', 'icon'=>'eye-close', 'url'=>array('index', get_class($model)=>array('status'=>LibraryRecord::STATUS_DISABLED)),'active'=>$model->status==LibraryRecord::STATUS_DISABLED),
    ),
    'htmlOptions'=>array('class'=>'nav-pills')
));
?>
</div>
<?php endif; ?>

<div class="pull-right">
    <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
</div>

</div>

<?php if($this->getSearchAttributes($model)) {
    $this->renderPartial('_search',array(
        'model'=>$model,
    ));
} ?>

<?php $this->widget('BootGridView', array(
    'id'=> get_class($model) . '-grid',
	'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'columns'=>$this->getGridColumns($model),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>