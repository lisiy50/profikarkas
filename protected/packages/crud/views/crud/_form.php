<div class="form">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    foreach($this->getFormAttributes($model) as $attribute=>$options) {
        switch($options['type'])
        {
            case 'text':
                unset($options['type']);
                echo $form->textFieldRow($model, $attribute, $options) . "\n";
                break;
            case 'dropDownList':
                $data=$options['data'];
                unset($options['type'], $options['data']);
                echo $form->dropDownListRow($model,$attribute,$data,$options). "\n";
                break;
            case 'widget':
                $className=$options['className'];
                unset($options['type'],$options['className']);
                echo $form->widgetFieldRow($model,$attribute,$className,$options). "\n";
                break;
            case 'image':
                unset($options['type']);
                echo $form->imageFileRow($model, $attribute);
                break;
            case 'checkBox':
                unset($options['type']);
                echo $form->checkBoxRow($model, $attribute, $options);
                break;
        }
    }

    $this->endClip();

    $tabs['basic']=array(
        'title' => 'Основные',
        'content' => $this->clips['basic'],
    );

    if(!$model->isNewRecord && array_key_exists('images', $model->relations())) {
        $tabs['images'] = array(
            'title' => 'Изображения',
            'view' => '_images',
        );
    }

    if($model->asa('SEOBehavior')) {
        $content='';
        foreach(array('metaTitle', 'metaKeywords', 'metaDescription') as $attribute) {
            $content.=$form->textFieldRow($model, $attribute, array('class'=>'span6')) . "\n";
        }
        $tabs['seo']=array(
            'title' => 'SEO информация',
            'content' => $content
        );
    }

    if(count($tabs)>1)
        $this->widget('BootTabView', array(
            'viewData' => array(
                'model' => $model,
                'form' => $form,
            ),
            'tabs' => $tabs,
        ));
    else
        echo $this->clips['basic'];

    $this->endWidget();
    ?>

</div><!-- form -->