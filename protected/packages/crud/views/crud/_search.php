<?php
$form = $this->beginWidget('BootActiveForm', array(
    'id' => 'search-form',
    'type' => BootActiveForm::TYPE_INLINE,
    'action' => array($this->route),
    'method' => 'get',
    'htmlOptions' => array('class' => 'well'),
));

foreach($this->getSearchAttributes($model) as $attribute=>$options) {
    if($options['type']=='dropDownList') {
        $data=$options['data'];
        unset($options['type'], $options['data']);
        echo $form->dropDownList($model,$attribute,$data,$options). "\n";
    } else if($options['type']=='widget') {
        $className=$options['className'];
        unset($options['type'],$options['className']);
        echo $form->widgetField($model,$attribute,$className,$options). "\n";
    } else {
        unset($options['type']);
        echo $form->textFieldRow($model, $attribute, $options) . "\n";
    }
}

echo CHtml::htmlButton('<i class="icon-search"></i> Поиск', array('class' => 'btn pull-right', 'type' => 'submit'));

$this->endWidget();
?>