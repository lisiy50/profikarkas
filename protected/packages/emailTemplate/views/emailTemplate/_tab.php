<h4><?php echo $header; ?></h4>

<?php echo CHtml::activeTextField($model, "[$id]subject", array('placeholder'=>$model->getAttributeLabel('subject'), 'class'=>'span7')); ?>
<?php echo CHtml::error($model, "subject"); ?>

<?php
$this->widget('CodeMirror', array(
    'model'=>$model,
    'attribute'=>"[$id]body"
));
?>