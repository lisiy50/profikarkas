<?php
class EmailTemplateController extends BackendController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='/layouts/column1';

    public $icon = 'mailing_list';

    public $list = array(
        'test'=>'Тестовый шаблон',
    );

    private $_models;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles'=>array('content')),
            array('deny', 'users'=>array('*')),
        );
    }

    public function actionIndex() {
        if(isset($_POST['EmailTemplate'])) {
            $valid=true;

            foreach($this->list as $id=>$title) {
                $model=$this->loadModel($id);

                if(isset($_POST['EmailTemplate'][$id]))
                    $model->attributes=$_POST['EmailTemplate'][$id];

                $valid=$model->validate() && $valid;
            }
            if($valid) {
                foreach($this->list as $id=>$title) {
                    $this->loadModel($id)->save();
                }

                Yii::app()->user->setFlash('success', 'Изменения сохранены');
            } else {
                Yii::app()->user->setFlash('error', 'Изменения не сохранены');
            }
        }

        $this->render('index');
    }

    public function loadModel($id) {
        if($this->_models==null)
            $this->loadModels();

        if(!isset($this->_models[$id])) {
            $model=new EmailTemplate();
            $model->id=$id;
            $model->save(false);
            $this->_models[$id]=$model;
        }

        return $this->_models[$id];
    }

    public function loadModels() {
        $criteria=new CDbCriteria();
        $criteria->addNotInCondition('id', array_keys($this->list));
        EmailTemplate::model()->deleteAll($criteria);

        foreach(EmailTemplate::model()->findAll() as $model)
            $this->_models[$model->id]=$model;

        return true;
    }
}