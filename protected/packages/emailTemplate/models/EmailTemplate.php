<?php

/**
 * This is the model class for table "{{text_content}}".
 *
 * The followings are the available columns in table '{{text_content}}':
 * @property integer $id
 * @property string $content
 */
class EmailTemplate extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SliderItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{email_template}}';
	}

    public function behaviors(){
        return array(
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('subject', 'filter', 'filter'=>'strip_tags'),
            array('subject,body', 'required'),
            array('subject', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'Тема письма',
			'body' => 'Тело письма',
		);
	}

}