<?php
class EmailTemplatePackage extends BasePackage {

    public $import=array(
        'emailTemplate.models.EmailTemplate'
    );

    public function getTopMenu() {
        return array(
            'Настройки'=>array('order'=>7, 'url'=>'#', 'icon'=>'cog', 'items'=>array(
                'Шаблоны писем'=>array('order'=>1, 'url'=>array('emailTemplate/index')),
            ))
        );
    }

}