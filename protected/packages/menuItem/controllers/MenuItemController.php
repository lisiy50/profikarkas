<?php

class MenuItemController extends BackendController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $icon = 'menu_item';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Пункт меню &quot;{name}&quot; удален',
            ),
        );
    }

    public function actionIndex()
    {
        $this->render('index', array(
            'menuItems' => MenuItem::model()->rooted()->findAll(),
        ));
    }

    public function actionCreate($root_id)
    {
        $model = new MenuItem;
        $rooted = $this->loadModel($root_id);

        $this->performAjaxValidation($model);

        if (isset($_POST['MenuItem'])) {
            $model->attributes = $_POST['MenuItem'];

            if (empty($model->parent_id))
                $model->parent_id = $rooted->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Пункт меню добавлен");
                $this->redirect(array('update', 'id' => $model->id));
            } else {
                Yii::app()->user->setFlash('error', "Пункт меню не добавлен");
            }
        } else if (isset($_GET['MenuItem'])) {
            $model->attributes = $_GET['MenuItem'];
        }

        $this->render('create', array(
            'model' => $model,
            'rooted' => $rooted,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $rooted = $model->getRooted();

        $this->performAjaxValidation($model);

        if (isset($_POST['MenuItem'])) {
            $model->attributes = $_POST['MenuItem'];

            if (empty($model->parent_id))
                $model->parent_id = $rooted->id;

            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Изменения сохранены");
                $this->redirect(array('update', 'id' => $model->id));
            } else {
                Yii::app()->user->setFlash('error', "Изменения не сохранены");
            }
        }

        $this->render('update', array(
            'model' => $model,
            'rooted' => $rooted,
        ));
    }

    public function actionSaveOrder()
    {
        if (isset($_POST['MenuItem_id'])) {
            $menuItems = MenuItem::model()->findAllByAttributes(array('id' => $_POST['MenuItem_id']));
            foreach ($menuItems as $menuItem) {
                $menuItem->position = array_search($menuItem->id, $_POST['MenuItem_id']);
                $menuItem->save();
            }
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function loadModel($id)
    {
        $model = MenuItem::model()->findByPk((int)$id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === get_class($model) . '-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
