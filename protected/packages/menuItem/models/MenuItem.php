<?php

/**
 * This is the model class for table "{{menu_item}}".
 *
 * The followings are the available columns in table '{{menu_item}}':
 * @property integer $id
 * @property string $name_ru
 * @property string $name_uk
 * @property string $name_de
 * @property string $name_fr
 * @property string $name_en
 * @property string $uri
 * @property integer $parent_id
 * @property integer $type
 * @property integer $level
 * @property integer $position
 * @property string $link_attributes
 *
 *
 * @property string $name
 */
class MenuItem extends ActiveRecord
{
    const ARTICLE = 1;
    const GALLERY = 1;

    public $multyLangFields = array(
        'name',
    );

    /**
     * Returns the static model of the specified AR class.
     * @return MenuItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{menu_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('uri, name_ru, name_en, name_uk, name_de, name_fr', 'filter', 'filter' => 'strip_tags'),
            array('name_ru', 'required'),
            array('parent_id, level, position', 'numerical', 'integerOnly' => true),
            array('name_ru, name_en, name_uk, name_de, name_fr, uri, link_attributes', 'length', 'max' => 255),
        );
    }

    public function scopes()
    {
        return array(
            'rooted' => array(
                'condition' => 'parent_id IS NULL'
            ),
            'article' => array(
                'condition' => 'parent_id='.self::ARTICLE
            ),
        );
    }

    public function defaultScope()
    {
        return array(
            'order' => 'position',
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'countChildren' => array(self::STAT, 'MenuItem', 'parent_id'),
            'children' => array(self::HAS_MANY, 'MenuItem', 'parent_id'),
            'parent' => array(self::BELONGS_TO, 'MenuItem', 'parent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name_ru' => 'Название (Русский)',
            'name_en' => 'Название (Английский)',
            'name_uk' => 'Название (Украинский)',
            'name_de' => 'Название (Немецкий)',
            'name_fr' => 'Название (Французский)',
            'uri' => 'URL адрес',
            'parent_id' => 'Размещение',
            'level' => 'Вложенность',
            'position' => 'Порядок',
            'link_attributes' => 'Дополнительные атрибуты ссылки',
        );
    }

    public function getText()
    {
        return CHtml::link(CHtml::encode($this->name), array('update', 'id' => $this->id));
    }

    protected function beforeDelete()
    {
        if (parent::beforeDelete()) {
            self::model()->deleteAll('parent_id=?', array($this->id));
            return true;
        } else {
            return false;
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->parent === null || $this->parent->isNewRecord) {
                $this->level = 0;
            } else {
                $this->level = $this->parent->level + 1;
            }
            if ($this->isNewRecord) {
                $criteria = new CDbCriteria;
                $criteria->select = 'position';
                $criteria->order = 'position DESC';
                if ($this->parent_id) {
                    $criteria->condition = 'parent_id=:parent_id';
                    $criteria->params = array(':parent_id' => $this->parent_id);
                } else {
                    $criteria->condition = 'parent_id IS NULL';
                }
                $last = self::model()->find($criteria);
                $this->position = $last ? $last->position + 1 : 0;
            }
            return true;
        } else
            return false;
    }

    private $_neighbors;

    public function getNeighbors()
    {
        if ($this->_neighbors == null) {
            if ($this->isRooted) {
                $this->_neighbors = MenuItem::model()->rooted()->findAll();
            } else {
                $this->_neighbors = MenuItem::model()->findAllByAttributes(array(
                    'parent_id' => $this->parent_id,
                ));
            }
        }
        return $this->_neighbors;
    }

    public function getIsRooted()
    {
        return empty($this->parent_id);
    }

    public function getRooted()
    {
        $parent = $this;
        while ($parent->parent) {
            $parent = $parent->parent;
        }
        return $parent;
    }

    public function getUrl()
    {
        if (strpos($this->uri, 'www.') === 0 || strpos($this->uri, 'http://') === 0) {
            return $this->uri;
        }

        $urlManager = (IS_BACKEND) ? Yii::app()->frontendUrlManager : Yii::app()->urlManager;
        if (empty($this->uri)) {
            return $urlManager->baseUrl;
        }

        $pars=parse_url($this->uri);

        if(empty($pars['path']) && !empty($pars['fragment'])) {
            return '#'.$pars['fragment'];
        }

        $route=empty($pars['path'])?'site/index':$pars['path'];
        $params=array();
        if(!empty($pars['query'])) {
            foreach(explode('&', $pars['query']) as $str) {
                list($key, $val)=explode('=', $str);
                $params[$key]=$val;
            }
        }
        if(!empty($pars['fragment'])) {
            $params['#']=$pars['fragment'];
        }

        return $urlManager->createUrl($route, $params);
    }
}
