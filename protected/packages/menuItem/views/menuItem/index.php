<?php
$this->breadcrumbs = array(
    'Пункты меню',
);
?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Пункты меню
        </h4>
    </div>
</div>
<hr>
<?php
$tabs = array();
foreach ($menuItems as $menuItem) {
    $tabs['tab' . $menuItem->id] = array(
        'title' => $menuItem->name,
        'view' => '_view',
        'data' => array(
            'model' => $menuItem,
        )
    );
}
?>

<?php $this->widget('BootTabView', array(
    'tabs' => $tabs,
)); ?>