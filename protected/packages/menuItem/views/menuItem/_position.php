<table id="sortable-menu-item" class="table table-striped">
    <colgroup>
        <col width="18"/>
        <col/>
        <col/>
    </colgroup>
    <?php foreach ($menuItems as $menuItem): ?>
    <tr id="MenuItem_id-<?php echo $menuItem->id; ?>" <?php if($model->id==$menuItem->id) echo 'class="current-row"'; ?>>
        <td><i class="icon-resize-vertical" style="cursor: row-resize;"></i></td>
        <td><?php echo $menuItem->name; ?></td>
        <td><?php echo $menuItem->url; ?></td>
    </tr>
    <?php endforeach; ?>

    <?php if (count($menuItems)==0): ?>
       <tr>
           <td colspan="3">Невозможно настроить порядок</td>
       </tr>
    <?php endif; ?>
</table>

<div class="row">
    <div class="span2">
        <a class="btn btn-success" id="save-menu-item" href="#">Сохранить порядок</a>
    </div>
</div>