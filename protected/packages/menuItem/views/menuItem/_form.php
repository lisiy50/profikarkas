<div class="form">
    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));

    echo $form->errorSummary($model);
    $this->beginClip('basic');
    echo $form->widgetFieldRow($model, 'parent_id', 'McDropdown', array(
        'data' => $rooted->children,
        'without' => $model->id,
        'options' => array(
            'select' => "js:function(id){
                $('#tab2').html('Сначала нужно сохранить пункт меню');
            }"
        ),
        'htmlOptions' => array(
            'class' => 'input-xxlarge'
        )
    ));
    echo $form->textFieldRow($model, 'name_ru', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    echo $form->textFieldRow($model, 'name_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    echo $form->textFieldRow($model, 'name_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    echo $form->textFieldRow($model, 'name_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    echo $form->textFieldRow($model, 'name_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'uri', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    echo $form->textFieldRow($model, 'link_attributes', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array('model' => $model),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'Настроить порядок',
                'view' => '_position',
                'data' => array(
                    'menuItems' => $model->neighbors,
                ),
                'visible' => !$model->isNewRecord && $model->hasNeighbors,
            )
        ),
    ));
    $this->endWidget();
    ?>

</div><!-- form -->
