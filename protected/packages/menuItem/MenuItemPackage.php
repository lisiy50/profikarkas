<?php
class MenuItemPackage extends BasePackage {

    public $import=array(
        'menuItem.models.MenuItem',
    );

    public function getTopMenu() {
        return array(
            'Контент' => array('order'=>3, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Пункты меню' => array('url'=>array('menuItem/index')),
            )),
        );
    }

}