<?php
class NewsImage extends ExtraImage {

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'news_'),
            'mini' => array('storage/news', 102, 60, 'resize'=>'fill', 'prefix'=>'mini_'),
            'more' => array('storage/news', 770, 500, 'resize'=>'max', 'prefix'=>'more_'),
            'project' => array('storage/news', 907, 550, 'resize'=>'max', 'prefix'=>'project_'),
            'large' => array('storage/news', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
            'original' => array('storage/news', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->getOwner()->watermark == LibraryRecord::STATUS_ENABLED){
            $images['more'] = array('storage/news', 770, 500, 'resize'=>'max', 'prefix'=>'more_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['project'] = array('storage/news', 907, 550, 'resize'=>'max', 'prefix'=>'project_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['large'] = array('storage/news', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }

        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> $images,
            ),
        );
    }

}