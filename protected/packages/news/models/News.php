<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property integer $project_id
 * @property string $title
 * @property string $title_uk
 * @property string $annotation
 * @property string $annotation_uk
 * @property string $content
 * @property string $content_uk
 * @property string $tags
 * @property integer $status
 * @property integer $watermark
 * @property integer $visibility
 * @property boolean $sent_by_mail
 * @property string $publish_date
 * @property string $create_time
 * @property string $update_time
 */
class News extends LibraryRecord
{

    const VISIBILITY_DIRECT_ONLY = 1;
    const VISIBILITY_ARCHIVE = 2;

    const SENT_BY_MAIL_NO = 0;
    const SENT_BY_MAIL_YES = 1;

    public $send_by_mail = 0;

    public $visibility = self::VISIBILITY_ARCHIVE;

    private $_oldTags;

    public $search_tag;

    public $watermark = self::STATUS_ENABLED;

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ),
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'image',
                'nameAttribute' => 'title',
                'images' => array(
                    'thumb' => array('storage/.tmb', 45, 45, 'resize' => 'fill', 'required' => 'thumb.jpg'),
                    'small' => array('storage/news', 212, 159, 'required' => 'default.png', 'resize'=>'fill', 'prefix' => 'small_'),
                    'large' => array('storage/news', 770, 770, 'resize'=>'max', 'prefix' => 'large_' /*'watermark'=>array('watermark.jpg', -10, -10, 'opacity'=>50)*/),
                ),
            ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'news/view',
            ),
            'imageResizeBehavior' => array(
                'class' => 'ImageResizeBehavior',
                'fields' => array('annotation', 'content'),
            ),
        );
    }

    public function init()
    {
        $this->publish_date = date('Y-m-d');
    }

    /**
     * Returns the static model of the specified AR class.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{news}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tags, title, title_uk', 'filter', 'filter' => 'strip_tags'),
//            array('annotation, content', 'filter', 'filter' => array($obj = new EHtmlPurifier(), 'purify')),
            array('visibility, title, annotation, content, status, watermark, publish_date', 'required'),
            array('visibility, status, watermark, project_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('annotation_uk, content_uk', 'safe'),
            array('image', 'file', 'types' => 'jpg, gif, png, jpeg', 'allowEmpty' => true),
            array('status, watermark', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
            array('sent_by_mail, send_by_mail', 'in', 'range'=>array(self::SENT_BY_MAIL_NO,self::SENT_BY_MAIL_YES)),
            array('image', 'unsafe'),
            array('tags', 'length', 'max' => 1024),
            array('tags', 'match', 'pattern' => '/^[a-zа-я\s,0-9-_]+$/iu', 'message' => 'Теги могут содержать только символы и цифры.'),
            array('tags', 'normalizeTags'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, search_tag, status, watermark, sent_by_mail, publish_date, create_time, update_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'images'=>array(self::HAS_MANY, 'NewsImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'NewsImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'project'=>array(self::BELONGS_TO, 'Project', 'project_id', ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'project_id' => 'Проект',
            'title' => 'Заглавие (Русский)',
            'title_uk' => 'Заглавие (Украинский)',
            'annotation' => 'Аннотация (Русский)',
            'annotation_uk' => 'Аннотация (Украинский)',
            'content' => 'Содержание (Русский)',
            'content_uk' => 'Содержание (Украинский)',
            'status' => 'Опубликована',
            'watermark' => 'Водяной знак',
            'sent_by_mail' => 'Рассылка произошла (если убрать галочку и сохранить, то рассылка произойдет повторно)',
            'send_by_mail' => 'Произвести рассылку',
            'visibility' => 'Показывать',
            'image' => 'Изображение',
            'tags' => 'Теги (через запятую)',
            'publish_date' => 'Дата публикации',
            'create_time' => 'Добавлена',
            'update_time' => 'Изменена',
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        $scopes = array(
            'order' => "$alias.publish_date DESC",
        );

        return $scopes;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('project_id', $this->project_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('watermark', $this->watermark);
        $criteria->compare('sent_by_mail', $this->sent_by_mail);

        if (is_array($this->publish_date) && isset($this->publish_date['from']) && isset($this->publish_date['till'])) {
            $criteria->compare('publish_date', '>=' . $this->publish_date['from']);
            $criteria->compare('publish_date', '<=' . $this->publish_date['till']);
        } else {
            $criteria->compare('publish_date', $this->publish_date, true);
        }

        if($this->search_tag)
            $criteria->addSearchCondition('tags', $this->search_tag);

        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function last($limit = NULL)
    {
        $criteria['order'] = 'publish_date DESC';
        if ($limit)
            $criteria['limit'] = $limit;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function normalizeTags($attribute, $params)
    {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags = $this->tags;
    }

    protected function afterSave()
    {
        parent::afterSave();
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);

        if($this->send_by_mail){
            $model = self::model()->findByPk($this->id);
            $url = app()->request->hostInfo.app()->baseUrl.$model->getUrl();
            $body = 'Перейти к новости Вы можете по '.CHtml::link('ссылке', $url, array('target'=>'_blank'));

            $emails = CHtml::listData(Subscriber::model()->findAllByAttributes(array('status' => 1)), 'name', 'email');
//            $emails = array('Oleg'=>'lisiy50@gmail.com');
            foreach($emails as $email){
                $message = Yii::app()->swiftMailer->newMessage($email, 'На сайте profikarkas.com.ua была опубликована новость', $body, array('contentType'=>'text/html'));
                Yii::app()->swiftMailer->send($message);
            }
            $model->sent_by_mail = self::SENT_BY_MAIL_YES;
            $model->update('sent_by_mail');
        }
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        Tag::model()->updateFrequency($this->tags, '');
    }

    public function containTag($tag)
    {
        if (empty($tag))
            return $this;
        $criteria = new CDbCriteria;
        $criteria->addSearchCondition('tags', $tag);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function containTags(array $tags)
    {
        if (empty($tags))
            return $this;

        $criteria = new CDbCriteria;
        foreach ($tags as $tag)
            $criteria->addSearchCondition('tags', $tag, true, 'OR');
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->title);
        return parent::getUrl($params);
    }
}
