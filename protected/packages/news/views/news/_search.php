<?php $form=$this->beginWidget('BootActiveForm', array(
    'id'=>'search-form',
    'type'=>BootActiveForm::TYPE_INLINE,
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

    <?php echo $form->hiddenField($model,'status'); ?>
    <?php echo $form->textFieldRow($model,'id',array('maxlength'=>11,'class'=>'span1')); ?>
    <?php echo $form->textFieldRow($model,'title',array('maxlength'=>255,'class'=>'span6')); ?>

    <strong>дата</strong>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'publish_date[from]',
        'language'=>'ru',
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
        ),
        'htmlOptions'=>array(
            'class'=>'input-small',
            'placeholder'=>'от',
        ),
    )); ?>
    <strong>-</strong>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,
        'attribute'=>'publish_date[till]',
        'language'=>'ru',
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
        ),
        'htmlOptions'=>array(
            'class'=>'input-small',
            'placeholder'=>'до',
        ),
    )); ?>

    <?php echo CHtml::htmlButton('<i class="icon-search"></i> Поиск', array('class'=>'btn pull-right','type'=>'submit')); ?>

<?php $this->endWidget(); ?>