<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>


    <?php echo $form->errorSummary($model); ?>

    <?php $this->beginClip('basic'); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title', array(
            'maxlength' => 255,
            'class' => 'span8'
        )); ?>
        </div>
        <div class="span4">
            <?php echo $form->widgetFieldRow($model, 'publish_date', 'zii.widgets.jui.CJuiDatePicker', array(
            'language' => 'ru',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
            ),
            'htmlOptions' => array(
                'class' => 'input-small'
            ),
        )); ?>
        </div>
    </div>
    <div class="row">
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title_uk', array(
                'maxlength' => 255,
                'class' => 'span8'
            )); ?>
        </div>
    </div>

    <?php
    echo $form->dropDownListRow($model, 'project_id', CHtml::listData(Project::model()->order('symbol_sort_letter,symbol_sort_number')->findAll(), 'id', 'symbol'), array('prompt'=>''));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    echo $form->checkBoxRow($model, 'send_by_mail', array('uncheckValue' => News::SENT_BY_MAIL_NO));

    echo '<div style="display:none;">';
    echo $form->dropDownListRow($model, 'visibility', Lookup::items('NewsVisibility'));
    echo '</div>';

    echo $form->widgetFieldRow($model, 'annotation', 'Redactor');
    echo $form->widgetFieldRow($model, 'annotation_uk', 'Redactor');

    echo $form->imageFileRow($model, 'image');

    echo $form->widgetFieldRow($model, 'content', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_uk', 'Redactor');

    echo '<div style="display:none;">';
    echo $form->widgetFieldRow($model, 'tags', 'zii.widgets.jui.CJuiAutoComplete', array(
        'sourceUrl' => $this->createUrl('tag/autoComplete'),
        'options' => array(
            'select' => 'js:function(event,ui){
                var terms = this.value.split(/,\s*/);
                terms.pop();
                terms.push(ui.item.value);
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }'
        ),
        'htmlOptions' => array(
            'class' => 'input-xxlarge',
            'maxlength' => 255,
        )
    ));
    echo '</div>';

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
