<?php

class NewsController extends BackendController
{

    public $layout = '//layouts/column1';
    public $icon = 'newspaper';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'index' => array(
                'class' => 'application.backend.actions.ListAction',
            ),
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Новость добавлена',
                'errorMessage' => 'Новость не добавлена',
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Новость &quot;{title}&quot; удалена',
            ),
            'autoComplete' => array(
                'class' => 'application.backend.actions.AutoCompleteAction',
                'queryAttribute' => 'title',
                'valueAttribute' => 'title',
                'labelAttribute' => 'title',
            ),
        );
    }

}
