<?php
$this->breadcrumbs=array(
	'Галереи изображений'=>array('index'),
	'Добавление',
);
$this->icon='picture_add';

?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Добавление галереи
        </h4>
    </div>
    <div class="pull-right">
        <button class="btn btn-small btn-success" type="button" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-plus icon-white"></i> Добавить</button>
    </div>
</div>
<hr>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>