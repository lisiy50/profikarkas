<?php
$this->breadcrumbs = array(
    'Галереи изображений',
);
?>

<div class="row">

    <div class="span8">
        <?php
        $this->widget('BootMenu', array(
            'activeIconClass' => 'icon-white',
            'items' => array(
                array('label' => 'Все галереи', 'url' => array('index'), 'active' => empty($_GET)),
                array('label' => 'Включенные', 'icon' => 'eye-open', 'url' => array('index', 'Gallery' => array('status' => Gallery::STATUS_ENABLED)), 'active' => $model->status == Gallery::STATUS_ENABLED),
                array('label' => 'Отключенные', 'icon' => 'eye-close', 'url' => array('index', 'Gallery' => array('status' => Gallery::STATUS_DISABLED)), 'active' => $model->status == Gallery::STATUS_DISABLED),
            ),
            'htmlOptions' => array('class' => 'nav-pills')
        ));
        ?>
    </div>

    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
    </div>

</div>

<?php $this->renderPartial('_search', array(
    'model' => $model,
)); ?>

<?php $this->widget('BootGridView', array(
    'id' => get_class($model) . '-grid',
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}",
    'columns' => array(
        array(
            'name' => 'status',
            'header' => '',
            'type' => 'html',
            'value' => '$data->statusIcon',
            'htmlOptions' => array('style' => 'width: 16px'),
        ),
        'name',
        array(
            'class' => 'application.widgets.grid.BootButtonColumn',
            'htmlOptions' => array('style' => 'width: 50px'),
            'buttons' => array(
                'view' => array(
                    'visible' => '$data->status==Gallery::STATUS_ENABLED',
                    'url' => '$data->url',
                    'options' => array(
                        'target' => '_blank'
                    )
                ),
            ),
        ),
    ),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>