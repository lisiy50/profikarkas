<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    echo $form->widgetFieldRow($model, 'description', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_de', 'Redactor');

    if ($menu = MenuItem::model()->article()->findAll()) {
        echo $form->widgetFieldRow($model, 'menu_id', 'McDropdown', array(
            'data' => $menu,
            'htmlOptions' => array(
                'class' => 'input-xxlarge',
            ),
        ));
    }

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'seo' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            )
        ),
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
