<div id="pjax"  data-url="//extraImage/_image_view" data-id="<?=$model->id?>" data-name="<?=get_class($model)?>">
    <table id="sortable-images" class="table table-striped">
        <tbody>
        <tr id="empty-images" <?php if ($model->hasImages): ?>style="display: none;"<?php endif; ?> >
            <td colspan="4">У этой галереи нету изображений</td>
        </tr>
        <?php foreach ($model->images as $image): ?>
            <?php $this->renderPartial('//extraImage/_image_view', array('model' => $image)); ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="pull-left" style="width: 170px;margin-left: 20px;">
        <a class="btn btn-success btn-small" id="save-images" href="#">Сохранить порядок/текст</a>
    </div>

    <div class="pull-left">
        <!--        --><?php //$this->widget('Uplodify', array(
        //        'name' => 'Image',
        //        'uploadUrl' => Yii::app()->createUrl('extraImage/upload', array('id' => $model->id, 'owner' => get_class($model))),
        //        'options' => array(
        //            'onUploadSuccess' => 'js:function(event, data){ $("#sortable-images tbody").append(data); $("#empty-images").hide(); }',
        //            'multi' => true,
        //            'fileTypeExts' => '*.jpg;*.gif;*.jpeg;*.png;',
        //            'fileTypeDesc' => 'Изображение',
        //            'progressData' => 'all',
        //            'queueID' => 'progress',
        //        ),
        //    )); ?>
        <input type="file" multiple="multiple" accept=".txt,image/*">
        <a href="#" class="upload_files button"
           data-url="<?= Yii::app()->createUrl("image/load")?>"
           data-id="<?=$model->id?>"
           data-name="<?=get_class($model)?>"
        >Загрузить файлы</a>
        <div class="ajax-reply"></div>
    </div>
</div>

<div class="row">
    <div class="span6" id="progress"></div>
</div>

<!--<script>-->
<!--    var files; // переменная. будет содержать данные файлов-->
<!---->
<!--    // заполняем переменную данными, при изменении значения поля file-->
<!--    $('input[type=file]').on('change', function () {-->
<!--        files = this.files;-->
<!--    });-->
<!---->
<!--    // обработка и отправка AJAX запроса при клике на кнопку upload_files-->
<!--    $('.upload_files').on('click', function (event) {-->
<!--        event.stopPropagation(); // остановка всех текущих JS событий-->
<!--        event.preventDefault();  // остановка дефолтного события для текущего элемента - клик для <a> тега-->
<!---->
<!--        // ничего не делаем если files пустой-->
<!--        if (typeof files == 'undefined') return;-->
<!---->
<!--        // создадим объект данных формы-->
<!--        var data = new FormData();-->
<!---->
<!--        // заполняем объект данных файлами в подходящем для отправки формате-->
<!--        $.each(files, function (key, value) {-->
<!--            data.append(key, value);-->
<!--        });-->
<!---->
<!--        // добавим переменную для идентификации запроса-->
<!--        data.append('my_file_upload', 1);-->
<!--        // добавить user_id и название модели где происходи загрузка-->
<!--        data.append('model_id', $(this).data("id"));-->
<!--        data.append('model_name', $(this).data("name"));-->
<!---->
<!--        // AJAX запрос-->
<!--        $.ajax({-->
<!--            url: 'image/load',-->
<!--            // url: $('.upload_files').data('url'),-->
<!--            type: 'POST', // важно!-->
<!--            data: data,-->
<!--            cache: false,-->
<!--            dataType: 'json',-->
<!--            // отключаем обработку передаваемых данных, пусть передаются как есть-->
<!--            processData: false,-->
<!--            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос-->
<!--            contentType: false,-->
<!--            // функция успешного ответа сервера-->
<!--            success: function(respond, status, jqXHR){-->
<!--                // перезагрузка страницы-->
<!--                $.ajax({-->
<!--                    url: 'image/img',-->
<!--                    type: 'POST',-->
<!--                    dataType : 'html',-->
<!--                    data:-->
<!--                    {-->
<!--                        id: $('#pjax').data('id'),-->
<!--                        name: $('#pjax').data('name'),-->
<!--                        url: $('#pjax').data('url')-->
<!--                    },-->
<!--                    success: function(html){-->
<!--                        $('#pjax').html(html);-->
<!--                        $("html,body").animate({scrollTop: $(".ajax-reply").offset().top}, 1000);-->
<!---->
<!--                    },-->
<!--                    error: function (error) {-->
<!--                    }-->
<!--                });-->
<!---->
<!--                if( typeof respond.error === 'undefined' ){-->
<!--                    // console.log("ok");-->
<!--                    // выведем пути загруженных файлов в блок '.ajax-reply'-->
<!--                    // var files_path = respond.files;-->
<!--                    // var html = '';-->
<!--                    // $.each( files_path, function( key, val ){-->
<!--                    //     html += val +'<br>';-->
<!--                    // } )-->
<!--                    //-->
<!--                    // $('.ajax-reply').html( html );-->
<!--                }-->
<!--                // ошибка-->
<!--                else {-->
<!--                    console.log('ОШИБКА: ' + respond.data );-->
<!--                }-->
<!--            },-->
<!--            // функция ошибки ответа сервера-->
<!--            error: function (jqXHR, status, errorThrown) {-->
<!--                console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);-->
<!--            }-->
<!---->
<!--        });-->
<!---->
<!--    });-->
<!--</script>-->