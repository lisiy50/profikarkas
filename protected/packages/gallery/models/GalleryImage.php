<?php
class GalleryImage extends ExtraImage {

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors(){
        $images = array(
            'thumb' => array('storage/.tmb', 150, 150, 'resize'=>'fill', 'prefix'=>'gallery_'),
            'mini' => array('storage/portfolio', 102, 60, 'resize'=>'fill', 'prefix'=>'mini_'),
            'more' => array('storage/gallery', 800, 490, 'resize'=>'fill', 'prefix'=>'more_'),
            'large' => array('storage/gallery', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_'),
            'original' => array('storage/news', 0, 0, 'resize'=>false, 'prefix'=>'original_'),
        );
        if($this->getOwner()->watermark == Gallery::STATUS_ENABLED){
            $images['more'] = array('storage/gallery', 800, 490, 'resize'=>'fill', 'prefix'=>'more_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
            $images['large'] = array('storage/gallery', 1280, 1024, 'resize'=>'max', 'prefix'=>'large_', 'watermark'=>array('storage/watermark.png', 'center', 'center'));
        }

        return array(
            'ImageUploadBehavior' => array(
                'class' => 'ImageUploadBehavior',
                'fileAttribute' => 'filename',
                'images'=> $images,
            ),
        );
    }

}