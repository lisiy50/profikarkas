<?php

/**
 * This is the model class for table "{{gallery}}".
 *
 * The followings are the available columns in table '{{gallery}}':
 * @property integer $id
 * @property integer $menu_id
 * @property string $name
 * @property string $name_uk
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_de
 * @property string $description
 * @property string $description_uk
 * @property string $description_en
 * @property string $description_fr
 * @property string $description_de
 * @property integer $status
 * @property integer $watermark
 * @property string $create_time
 * @property string $update_time
 */
class Gallery extends LibraryRecord
{

    public $watermark = self::STATUS_ENABLED;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gallery}}';
	}

    public function behaviors(){
  	    return array(
  		    'CTimestampBehavior' => array(
  		  	    'class' => 'zii.behaviors.CTimestampBehavior',
  		  	    'createAttribute' => 'create_time',
  			    'updateAttribute' => 'update_time',
  		    ),
            'SEOBehavior' => array(
                'class' => 'SEOBehavior',
                'route' => 'gallery/view',
            )
  	    );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('name', 'filter', 'filter'=>'strip_tags'),
            array('description, description_uk, description_en, description_fr, description_de','filter','filter'=>array($obj=new EHtmlPurifier(),'purify')),
			array('name', 'required'),
			array('name, name_uk, name_en, name_fr, name_de', 'length', 'max'=>255),
			array('status, menu_id, watermark', 'numerical', 'integerOnly'=>true),
            array('status, watermark', 'in', 'range'=>array(self::STATUS_ENABLED,self::STATUS_DISABLED)),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, status, watermark', 'safe', 'on'=>'search'),
		);
	}

    public function getMenuList() {
        return CHtml::listData(MenuItem::model()->findAll('parent_id=?', array(MenuItem::GALLERY)), 'id', 'name');
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'images'=>array(self::HAS_MANY, 'GalleryImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'countImages'=>array(self::STAT, 'GalleryImage', 'owner_id', 'condition'=>'owner_model=:om', 'params'=>array(':om'=>get_class($this))),
            'menu'=>array(self::BELONGS_TO, 'MenuItem', 'menu_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название (Русский)',
			'name_uk' => 'Название (Украинский)',
			'name_en' => 'Название (Английский)',
			'name_fr' => 'Название (Французский)',
			'name_de' => 'Название (Немецкий)',
			'description' => 'Описание (Русский)',
			'description_uk' => 'Описание (Украинский)',
			'description_en' => 'Описание (Английский)',
			'description_fr' => 'Описание (Французский)',
			'description_de' => 'Описание (Немецкий)',
			'status' => 'Включена',
			'watermark' => 'Водяной знак',
			'create_time' => 'Добавлена',
			'update_time' => 'Изменена',
            'menu_id' => 'Используется меню',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('watermark',$this->watermark);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    public function getUrl($params=array()) {
        if(!isset($params['uri']))
            $params['uri'] = app()->translitFormatter->formatUrl($this->name);
        return parent::getUrl($params);
    }

}
