<?php
class GalleryPackage extends BasePackage {

    public $import=array(
        'gallery.models.Gallery',
        'gallery.models.GalleryImage',
    );

    public function getTopMenu() {
        return array(
            'Контент' => array('order'=>3, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Галереи изображений' => array('url'=>array('gallery/index')),
            )),
        );
    }

}