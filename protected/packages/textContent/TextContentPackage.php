<?php
class TextContentPackage extends BasePackage {

    public $import=array(
        'textContent.models.TextContent'
    );

    public function getTopMenu() {
        return array(
            'Контент'=>array('order'=>2, 'url'=>'#', 'icon'=>'book', 'items'=>array(
                'Текстовый контент'=>array('order'=>1, 'url'=>array('textContent/index')),
            ))
        );
    }

}