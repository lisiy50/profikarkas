<?php
class TextContentController extends BackendController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='/layouts/column1';

    public $icon = 'text_content';

    public $list = array(
        'site-index'=>'На главной странице',
        'review-index'=>'Отзывы пользователей',
        'walks-index'=>'Видеопрогулки',
    );



    private $_models;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles'=>array('content')),
            array('deny', 'users'=>array('*')),
        );
    }

    public function actionIndex() {
        if(isset($_POST['TextContent'])) {
            foreach($this->list as $id=>$title) {
                $model=$this->loadModel($id);

                if(isset($_POST['TextContent'][$id]))
                    $model->attributes=$_POST['TextContent'][$id];

                $model->save();
            }
            Yii::app()->user->setFlash('success', 'Изменения сохранены');
        }

        $this->render('index');
    }

    public function loadModel($id) {
        if($this->_models==null)
            $this->loadModels();

        if(!isset($this->_models[$id])) {
            $model=new TextContent();
            $model->id=$id;
            $model->save(false);
            $this->_models[$id]=$model;
        }

        return $this->_models[$id];
    }

    public function loadModels() {
        $criteria=new CDbCriteria();
        $criteria->addNotInCondition('id', array_keys($this->list));
        TextContent::model()->deleteAll($criteria);

        foreach(TextContent::model()->findAll() as $model)
            $this->_models[$model->id]=$model;

        return true;
    }
}