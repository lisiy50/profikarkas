<?php

/**
 * This is the model class for table "{{text_content}}".
 *
 * The followings are the available columns in table '{{text_content}}':
 * @property integer $id
 * @property string $content
 * @property string $content_uk
 * @property string $content_en
 * @property string $content_fr
 * @property string $content_de
 * @property integer $h1_size
 * @property integer $h2_size
 * @property integer $h3_size
 * @property integer $h4_size
 */
class TextContent extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SliderItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{text_content}}';
	}

    public function behaviors(){
        return array(
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('content', 'filter', 'filter'=>array($obj=new EHtmlPurifier(),'purify')),
			array('content, content_uk, content_en, content_fr, content_de', 'safe'),
			array('h1_size,h2_size,h3_size,h4_size', 'numerical', 'integerOnly' => true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Текст (Русский)',
			'content_uk' => 'Текст (Украинский)',
			'content_en' => 'Текст (Английский)',
			'content_fr' => 'Текст (Французский)',
			'content_de' => 'Текст (Немецкий)',
            'h1_size' => 'размер h1(36)',
            'h2_size' => 'размер h2(30)',
            'h3_size' => 'размер h3(24)',
            'h4_size' => 'размер h4(24)',
		);
	}

    public static function get($id) {
        $model=self::model()->findByPk($id);

        if($model)
            return $model->content . $model->getStyles();

        return '';
    }

    public static function getUk($id) {
        $model=self::model()->findByPk($id);

        if($model)
            return $model->content_uk . $model->getStyles();

        return '';
    }

    public function getStyles()
    {
        $css = '<style>';
        if ($this->h1_size) {
            $css .= ".review_article h1, .review_article h1 span {font-size: {$this->h1_size}px !important;}";
        }
        if ($this->h2_size) {
            $css .= ".review_article h2, .review_article h2 span {font-size: {$this->h2_size}px !important;}";
        }
        if ($this->h3_size) {
            $css .= ".review_article h3, .review_article h3 span {font-size: {$this->h3_size}px !important;}";
        }
        if ($this->h4_size) {
            $css .= ".review_article h4, .review_article h4 span {font-size: {$this->h4_size}px !important;}";
        }
        $css .= '</style>';
        return $css;
    }

}
