<h4><?php echo $header . ' (Русский)'; ?></h4>
<?php
$this->widget('Redactor', array(
    'model'=>$model,
    'attribute'=>"[$id]content",
    'htmlOptions'=>array('style'=>'max-height:500px;'),
));
?>

<h4><?php echo $header . ' (Украинский)'; ?></h4>
<?php
$this->widget('Redactor', array(
    'model'=>$model,
    'attribute'=>"[$id]content_uk",
    'htmlOptions'=>array('style'=>'max-height:500px;'),
));
?>

<h4><?php echo $header . ' (Английский)'; ?></h4>
<?php
$this->widget('Redactor', array(
    'model'=>$model,
    'attribute'=>"[$id]content_en",
    'htmlOptions'=>array('style'=>'max-height:500px;'),
));
?>

<h4><?php echo $header . ' (Французский)'; ?></h4>
<?php
$this->widget('Redactor', array(
    'model'=>$model,
    'attribute'=>"[$id]content_fr",
    'htmlOptions'=>array('style'=>'max-height:500px;'),
));
?>

<h4><?php echo $header . ' (Немецкий)'; ?></h4>
<?php
$this->widget('Redactor', array(
    'model'=>$model,
    'attribute'=>"[$id]content_de",
    'htmlOptions'=>array('style'=>'max-height:500px;'),
));
?>
<div class="row">
    <div class="span3">
        <?php
        echo $form->textFieldRow($model, "[{$id}]h1_size", array(
            'class'=>'span2',
            'type' => 'number',
            'min'=>1,
            'max'=>128,
            'append'=>'px',
        ));?>
    </div>
    <div class="span3">
        <?php
        echo $form->textFieldRow($model, "[{$id}]h2_size", array(
            'class'=>'span2',
            'type' => 'number',
            'min'=>1,
            'max'=>128,
            'append'=>'px',
        ));?>
    </div>
    <div class="span3">
        <?php
        echo $form->textFieldRow($model, "[{$id}]h3_size", array(
            'class'=>'span2',
            'type' => 'number',
            'min'=>1,
            'max'=>128,
            'append'=>'px',
        ));?>
    </div>
    <div class="span3">
        <?php
        echo $form->textFieldRow($model, "[{$id}]h4_size", array(
            'class'=>'span2',
            'type' => 'number',
            'min'=>1,
            'max'=>128,
            'append'=>'px',
        ));?>
    </div>
</div>
