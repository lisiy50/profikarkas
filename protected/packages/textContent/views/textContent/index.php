<?php
$this->layout='column1';
$this->breadcrumbs=array(
    'Текстовый контент'
);
?>

<div class="row">
    <div class="span8">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Текстовый контент
        </h4>
    </div>
    <div class="pull-right">
        <button class="btn btn-small btn-success" type="button" onclick="$('#list-form').submit();"><i class="icon-plus icon-white"></i> Сохранить</button>
    </div>
</div>
<hr>

<?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => 'list-form',
)); ?>

<div class="tabbable tabs-left">
<?php
$tabs=array();
foreach($this->list as $id=>$title) {
    $tabs[$id]=array(
        'title' => $title,
        'view' => '_tab',
        'data' => array(
            'form' => $form,
            'model'=>$this->loadModel($id),
            'header'=>$title,
            'id'=>$id,
        ),
    );
}

$this->widget('BootTabView', array(
    'tabs' => $tabs,
));
?>
</div>

<?php $this->endWidget(); ?>
