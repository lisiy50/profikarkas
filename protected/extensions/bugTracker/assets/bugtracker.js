jQuery(function () {

    var start = {x:0, y:0}, end;
    var toolbar=$('<a class="bug-tracker-toolbar"></a>');
    var overlay = $('<div class="bug-tracker-overlay">&nbsp;</div>');
    var select = $('<div class="bug-tracker-selecter"><div></div></div>');
    var comment = $('<div class="bug-tracker-comment">ss</div>');

    $('body')
        .append(toolbar)
        .append(overlay)
        .append(select)
        .append(comment);

    select.on('resize', function () {
        select.show().css({
            left:Math.min(start.x, end.x) + 'px',
            top:Math.min(start.y, end.y) + 'px',
            width:Math.abs(start.x - end.x) + 'px',
            height:Math.abs(start.y - end.y) + 'px'
        });
    });

    var mousedown = function (e) {
        end = start = {x:e.pageX, y:e.pageY};

        select.trigger('resize');

        comment.hide();
        $(document)
            .off('mousemove')
            .off('mouseup')
            .on('mousemove', function (e) {
                end = {x:e.pageX, y:e.pageY};
                select.trigger('resize');
            })
            .on('mouseup', function (e) {
                $(document).off('mousemove');
                end = {x:e.pageX, y:e.pageY};
                if (start.x == end.x && start.y == end.y) {
                    return;
                }
                select.trigger('resize');
                comment.css({
                    left:Math.min(start.x, end.x) + 'px',
                    top:Math.max(start.y, end.y) + 10 + 'px',
                    width:Math.max(Math.abs(start.x - end.x), 250) + 'px'
                }).show('blind');
                comment.find('textarea').css({width:Math.max(Math.abs(start.x - end.x) - 7, 243) + 'px'})
            })
    }

    toolbar.on('click', function () {
        $(document).on('mousedown', mousedown);
        overlay.show();
    })

    $(document).on('endSelect', function () {
        select.hide();
        overlay.hide();
        comment.hide();
        $(document).off('mousedown').off('mouseup').off('mousemove');
    })

    overlay.on('click', function (e) {
        if (start.x == e.pageX && start.y == e.pageY) {
            select.hide();
        }
    });

    /*

    $('#bug-tracker-send').on('click', function () {
        $.post('http://brainstorm.com.ua/brainstorm/admin.php/task/createFromSite', {
            Task:{
                content:$('#bug-tracker-message').val(),
                project_id:$('#bug-tracker-project').val(),
                name:window.location + '',
                location:{
                    top:start.y,
                    center:start.x - $(document).width() / 2,
                    width:Math.abs(start.x - end.x),
                    height:Math.abs(start.y - end.y),
                    url:window.location + ''
                }
            }

        }, function () {
            $(document).trigger('endSelect');
            alert('Сообщение об ошибке отправлено. Спасибо.');
        })
    })

    $('#bug-tracker-cancel').on('click', function () {
        $(document).trigger('endSelect');
    })

    $('.bug-tracker-comment').on('mousedown', function (event) {
        event.stopPropagation();
    })

    $('.bug-tracker-comment').on('click', function (event) {
        event.stopPropagation();
    })

    $('.bug-tracker-comment').on('mouseup', function (event) {
        event.stopPropagation();
    })

    $('#bugTrackerToolbar').on('click', function () {
        $(document).on('mousedown', mousedown);
        overlay.show();
    })*/
})

function BugTrackerRun(params) {
    $('.bug-tracker-overlay').show();
    $('.bug-tracker-selecter').show().css({
        left:$(document).width() / 2 + new Number(params.center) + 'px',
        top:params.top + 'px',
        width:params.width + 'px',
        height:params.height + 'px'
    });
    $('.bug-tracker-comment').html(params.content).show().css({
        left:($(document).width() / 2 + new Number(params.center)) + 'px',
        top:new Number(params.top) + new Number(params.height) + 10 + 'px',
        width:Math.max(new Number(params.width), 250) + 'px',
        height:'auto'
    });
}