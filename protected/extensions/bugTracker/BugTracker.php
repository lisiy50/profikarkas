<?php
class BugTracker extends CApplicationComponent
{
    const PACKAGE_ID='bugTracker';

    public $project_id;

    public function init()
    {
        if(Yii::app()->request->isAjaxRequest || !defined('YII_DEBUG') || YII_DEBUG === false)
            return;

        Yii::app()->attachEventHandler('onEndRequest', array($this, 'endRequest'));
        Yii::app()->attachEventHandler('onBeginRequest', array($this, 'beginRequest'));
    }

    public function beginRequest()
    {
        $cs = Yii::app()->clientScript;
        if (!isset($cs->packages[self::PACKAGE_ID])) {
            $cs->packages[self::PACKAGE_ID] = array(
                'basePath' => 'ext.bugTracker.assets',
                'js' => array('bugtracker.js'),
                'css' => array('bugtracker.css'),
                'depends' => array('jquery'),
            );
        }
        $cs->registerPackage(self::PACKAGE_ID);
        /*if(isset($_GET['BugTracker'])) {
            $params=CJavaScript::encode($_GET['BugTracker']);
            $cs->registerScript('BugTrackerRun', "BugTrackerRun($params)");
        }*/
    }

    public function endRequest()
    {
        /*echo '<a id="bugTrackerToolbar"></a>';
        echo '<div class="bug-tracker-comment">' .
            '<textarea id="bug-tracker-message"></textarea><br>' .
            '<input id="bug-tracker-project" type="hidden" value="'.$this->project_id.'">' .
            '<input id="bug-tracker-send" type="button" value="Отправить">' .
            '<input id="bug-tracker-cancel" type="button" value="Отмена">' .
            '</div>';
        echo '<div class="bug-tracker-overlay">&nbsp;</div>';
        echo '<div class="bug-tracker-selecter"><div></div></div>';*/
    }
}