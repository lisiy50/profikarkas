<?php
/**
 * Bootstrap vertical form input widget.
 * @since 0.9.8
 */

Yii::import('application.widgets.input.BootInputInline');

class BootInputSearch extends BootInputInline
{
	/**
	 * Renders a text field.
	 * @return string the rendered content
	 */
	protected function textField()
	{
		if (isset($this->htmlOptions['class']))
			$this->htmlOptions['class'] .= ' search-query';
		else
			$this->htmlOptions['class'] = 'search-query';

		$this->htmlOptions['placeholder'] = $this->model->getAttributeLabel($this->attribute);
		echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
		echo $this->getError().$this->getHint();
	}
}
