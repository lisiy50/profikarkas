<?php
class ProductMultiSelect extends CWidget {

    public $model;

    public $attribute;

    public $htmlOptions=array();

    public $items=array();

    protected $_template;

    public function run() {
        if(empty($this->htmlOptions['id']))
            $this->htmlOptions['id']=$this->getId();

        if(empty($this->htmlOptions['class']))
            $this->htmlOptions['class']='span9';

        $this->htmlOptions['type']='text';

        $this->registerScripts();

        echo '<div class="input-append">'."\n";
        echo "\t".CHtml::tag('input',$this->htmlOptions)."\n";
        echo "\t".'<span class="add-on"><i class="icon-search"></i></span>'."\n";
        echo '</div>'."\n";

        echo CHtml::openTag('table', array('id'=>$this->resultId, 'class'=>'table'))."\n";

        foreach($this->items as $item) {
            echo '<tr>'."\n";
            echo "\t".'<td style="width: 13px;">'.CHtml::checkBox($this->resolveName().'[]', true, array('value'=>$item->id, 'id'=>$this->resolveId().'_'.$item->id)).'</td>'."\n";
            echo "\t".'<td>'.CHtml::tag('label', array('for'=>$this->resolveId().'_'.$item->id), $item->brand->name . ' ' . $item->name).'</td>'."\n";
            echo "\t".'<td>'.CHtml::image($item->getImageUrl('thumb'), '', array('style'=>'max-width: 70px;')).'</td>'."\n";
            echo '</tr>'."\n";
        }

        echo '</table>'."\n";
    }

    protected function registerScripts() {
        $id=$this->htmlOptions['id'];

        $cs = Yii::app()->getClientScript();
        $cs->registerPackage('multiSelect');

        $options= CJavaScript::encode(array(
            'source'=>Yii::app()->createUrl('product/autoComplete'),
            'resultElement'=>'#'.$this->resultId,
            'template'=>$this->getTemplate(),
            'requestData'=>"js:function(){var ids=[]; $('#$this->resultId :checked').each(function(){ids.push($(this).val())}); return {without:ids}; }",
        ));

        $cs->registerScript(__CLASS__.'#'.$id, "$('#$id').multiSelect($options);");
    }

    protected function getTemplate() {
        if($this->_template===null) {
            $this->_template='<tr>';
            $this->_template.='<td style="width: 13px;"><input value="{id}" id="'.$this->resolveId().'_{id}" type="checkbox" name="'.$this->resolveName().'[]"></td>';
            $this->_template.='<td><label for="'.$this->resolveId().'_{id}">{label}</label></td>';
            $this->_template.='<td><img style="max-width: 70px;" src="{icon}"></td>';
            $this->_template.='</tr>';
        }

        return $this->_template;
    }

    public function resolveName() {
        return CHtml::activeName($this->model,$this->attribute);
    }

    public function resolveId() {
        return CHtml::getIdByName($this->resolveName());
    }

    public function getResultId() {
        return $this->getId().'-result';
    }
}