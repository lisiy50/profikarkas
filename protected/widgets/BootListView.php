<?php
Yii::import('zii.widgets.CListView');

class BootListView extends CListView {

    public $pagerCssClass='pagination';

    public $pager=array('class'=>'BootPager');

    public $template="{sorter}\n{items}\n{pager}";

    public $itemView='_view';


}