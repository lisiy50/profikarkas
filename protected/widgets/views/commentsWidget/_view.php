<?php
/**
 * @var Comment $model
 */
?>

<div class="comment_view" id="c<?php echo $model->id ?>" data-id="<?php echo $model->id ?>">

    <?php if(user()->getRole() == User::ROLE_ADMIN): ?>
        <div class="comment_icon">
            <img class="img-responsive img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/profikarkas_admin_icon.png" alt="profikarkas-admin-icon">
        </div>
    <?php endif ?>

    <div class="comment_view_inner">
        <div class="comments-header">
            <span class="name"><?php echo $model->user->fio; ?></span>
            <?php echo $model->getFormatedDate() ?>

            <?php if(user()->getId() == $model->user_id || user()->getRole() == User::ROLE_ADMIN || user()->getRole() == User::ROLE_CONTENT):?>
                <div class="comment-controls pull-right">
                    <?php echo CHtml::link('<i class="glyphicon glyphicon-pencil"></i>', array('/comment/update', 'id'=>$model->id), array('class' => 'comment-update fancybox.ajax')) ?>
                    <?php echo CHtml::link('<i class="glyphicon glyphicon-trash"></i>', array('/comment/delete','id'=>$model->id), array('class' => 'comment-delete')); ?>
                </div>
            <?php endif ?>

        </div>
        <div class="comments-body">
            <pre class="comments-text"><?php echo $model->body ?></pre>

            <?php if($model->file1): ?>
                <div class="comments-thumbs">
                    <a class="fancybox" href="<?php echo Yii::app()->getBaseUrl(); ?>/storage/comment_files/<?php echo $model->file1; ?>" download>
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/clip_icon.png" alt="">файл 1
                    </a>
                    <?php if($model->file2): ?>
                        <a class="fancybox" href="<?php echo Yii::app()->getBaseUrl(); ?>/storage/comment_files/<?php echo $model->file2; ?>" download>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/clip_icon.png" alt="">файл 2
                        </a>
                    <?php endif ?>
                    <?php if($model->file3): ?>
                        <a class="fancybox" href="<?php echo Yii::app()->getBaseUrl(); ?>/storage/comment_files/<?php echo $model->file3; ?>" download>
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/clip_icon.png" alt="">файл 3
                        </a>
                    <?php endif ?>
                </div>
            <?php endif ?>

            <?php if($model->parent_id == null):?>
                <div class="comments-link"><a class="replay">ответить</a></div>
            <?php endif ?>
        </div>

        <?php if($model->children): ?>
            <?php foreach ($model->children as $child): ?>
                <?php $this->render('commentsWidget/_view', array('model' => $child))?>
            <?php endforeach ?>
        <?php endif ?>

    </div>

</div>
