<?php
/**
 * @var Comment $model
 * @var CActiveRecord $owner
 */
?>

<?php /* @var BootActiveForm $form */ ?>
<?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => $model->getClassId() . '-form',
    'action' => array('/comment/addComment'),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->error($model, 'user_id') ?>

<?php echo $form->hiddenField($model, 'owner'); ?>
<?php echo $form->hiddenField($model, 'owner_id'); ?>
<?php echo $form->hiddenField($model, 'parent_id'); ?>
<div class="control-group">
    <label for="Comment_body" class="required">А каково ваше мнение? Оставьте комментарий</label>
    <?php echo $form->textArea($model, 'body'); ?>
    <?php echo $form->error($model, 'body') ?>
</div>

<div class="control-group">
<?php echo $form->fileFieldRow($model, 'file1'); ?>
</div>
<div class="control-group" style="display: none;">
<?php echo $form->fileFieldRow($model, 'file2'); ?>
</div>
<div class="control-group" style="display: none;">
<?php echo $form->fileFieldRow($model, 'file3'); ?>
</div>

<?php if(user()->isGuest): ?>
Вы должны <?php echo CHtml::link('войти', array('/site/login'))?>, чтобы  добавить комментарий или <?php echo CHtml::link('зарегистрироваться', array('/user/create'))?> если у вас пока нет аккаунта на Profikarkas.
<?php else: ?>
<?php echo CHtml::submitButton('Отправить')?>
<?php endif ?>

<?php $this->endWidget();?>

