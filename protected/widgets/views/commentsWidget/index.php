<?php
/**
 * @var Comment[] $comments
 * @var CWidget $this
 */
?>

<?php
foreach ($comments as $comment) {
    $this->render('commentsWidget/_view', array('model' => $comment));
}
?>
