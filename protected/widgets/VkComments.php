<?php
class VkComments extends CWidget {

    public $width=496;
    public $attach='*';
    public $limit=10;

    public function run() {
        $id=$this->getId();
        $api_id=Yii::app()->config['vkontakte_api_id'];

        if(YII_DEBUG === true && empty($api_id))
            throw new CException('Необходимо заполнить API ключ Вконтакте');
        if(empty($api_id))
            return false;

        $options=CJavaScript::encode(array(
            'limit'=>$this->limit,
            'width'=>$this->width,
            'attach'=>$this->attach,
        ));
        echo CHtml::tag('div', array('id'=>$id), '');

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('http://userapi.com/js/api/openapi.js?47');
        $cs->registerScript('VK.init', "VK.init({apiId: '$api_id', onlyWidgets: true});");
        $cs->registerScript(__CLASS__.'#'.$id, "VK.Widgets.Comments('$id', $options);");
    }
}