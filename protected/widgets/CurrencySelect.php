<?php
class CurrencySelect extends CWidget {

    protected $data;

    const TYPE_SELECT='select';
    const TYPE_LINKS='links';

    public $type=self::TYPE_SELECT;
    public $htmlOptions=array();
    public $current;

    public function init() {
        $this->data=Currency::model()->findAll();
        $this->current=Yii::app()->currency->active['id'];
    }

    public function run() {
        switch($this->type) {
            case self::TYPE_SELECT:
                $this->htmlOptions['onchange']='this.form.submit();';
                echo CHtml::beginForm(array('site/currency'), 'get');
                echo CHtml::dropDownList('id', Yii::app()->currency->active['id'], CHtml::listData($this->data, 'id', 'name'), $this->htmlOptions);
                echo CHtml::hiddenField('returnUrl', Yii::app()->request->url);
                echo CHtml::endForm();
                break;
            case self::TYPE_LINKS:
                echo CHtml::openTag('ul', $this->htmlOptions);
                foreach($this->data as $currency) {
                    echo '<li>';
                    echo CHtml::link($currency->name, array('site/currency', 'id'=>$currency->id, 'returnUrl'=>Yii::app()->request->url));
                    echo '</li>';
                }
                echo '</ul>';
                break;
        }
    }

}