<?php
class SocialLikes extends CWidget {

    public $buttons=array();
    public $htmlOptions=array();

    public function run()
    {
        if(empty($this->htmlOptions['id']))
            $this->htmlOptions['id']=$this->id;

        echo CHtml::openTag('ul', $this->htmlOptions);

        foreach($this->buttons as $button=>$options) {
            if(is_string($options)) {
                $button=$options;
                $options=array();
            }
            echo CHtml::openTag('li', array('class'=>$button));
            $this->renderButton($button, $options);
            echo '</li>';
        }

        echo '</ul>';
    }

    public function renderButton($button, $options=array()) {
        switch($button) {
//            case 'vk':
//                $this->renderVk($options);
//                break;
            case 'gplus':
                $this->renderGplus($options);
                break;
            case 'twitter':
                $this->renderTwitter($options);
                break;
            case 'facebook':
                $this->renderFacebook($options);
                break;
        }
    }

    public function renderVk($options) {
        $id='VK_' . $this->id;
        $api_id=Yii::app()->config['vkontakte_api_id'];

        if(empty($api_id))
            throw new CException('Необходимо заполнить API ключ Вконтакте');

        $options=array_merge(array(
            'type'=>'button',
        ), $options);

        $options=CJavaScript::encode($options);

        echo CHtml::tag('div', array('id'=>$id));

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('http://userapi.com/js/api/openapi.js?47');
        $cs->registerScript('VK.init', "VK.init({apiId: '$api_id', onlyWidgets: true});");
        $cs->registerScript(__CLASS__ . '#' . $id, "VK.Widgets.Like('$id', $options);");
    }

    public function renderGplus($options) {
        foreach($options as $i=>$value) {
            $options['data-' . $i]= $value;
        }

        $options=array_merge(array(
            'class'=>'g-plusone',
        ), $options);

        echo CHtml::tag('div', $options, '');

        $cs=Yii::app()->getClientScript();
        $cs->registerScriptFile('https://apis.google.com/js/plusone.js');
        $cs->registerScript(__CLASS__.'#Gplus', "window.___gcfg = {lang: 'ru'};", CClientScript::POS_HEAD);
    }

    public function renderTwitter($options) {
        foreach($options as $i=>$value) {
            $options['data-' . $i]=$value;
        }

        $options=array_merge(array(
            'data-lang'=>'ru',
            'href'=>'https://twitter.com/share',
            'class'=>'twitter-share-button',
        ), $options);

        echo CHtml::link('Твитнуть', 'https://twitter.com/share', $options);

        Yii::app()->clientScript->registerScriptFile('//platform.twitter.com/widgets.js');
    }

    public function renderFacebook($options) {
        foreach($options as $i=>$value) {
            $options['data-' . $i]=$value;
        }

        $options=array_merge(array(
            'data-show-faces'=>'true',
            'data-width'=>'450',
            'data-layout'=>'button_count',
            'data-send'=>'false',
            'class'=>'fb-like',
        ), $options);

        echo CHtml::tag('div', $options, '');

//        Yii::app()->clientScript->registerScriptFile('//connect.facebook.net/ru_RU/all.js#xfbml=1');
    }

}