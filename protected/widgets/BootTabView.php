<?php
class BootTabView extends CWidget {

    public $activeTab;

    public $viewData;

    public $tabs=array();

    public function init() {
        $this->tabs=$this->normalizeTabs($this->tabs);
    }

    public function run() {
        if(empty($this->tabs))
            return;

        if($this->activeTab===null || !isset($this->tabs[$this->activeTab]))
        {
            reset($this->tabs);
            list($this->activeTab, )=each($this->tabs);
        }

        $this->renderHeader();
        $this->renderBody();

        Yii::app()->getClientScript()->registerScript(__CLASS__, "
            // Javascript to enable link to tab
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
            }

            // Change hash for page-reload
            //$('.nav-tabs a').on('shown', function (e) {
            //    window.location.hash = e.target.hash;
            //})
        ");
    }

    protected function renderHeader() {
        echo "<ul class=\"nav nav-tabs\">\n";
        foreach($this->tabs as $id=>$tab)
        {
            $class='';
            if($id==$this->activeTab)
                $class=' class="active"';

            if(!isset($tab['content']) && !isset($tab['view']))
                echo "<li{$class}><a href=\"{$tab['url']}\">{$tab['title']}</a></li>\n";
            else
                echo "<li{$class}><a href=\"{$tab['url']}\" data-toggle=\"tab\">{$tab['title']}</a></li>\n";
        }
        echo "</ul>\n";
    }

    protected function renderBody() {
        echo "<div class=\"tab-content\">\n";
        foreach($this->tabs as $id=>$tab)
        {
            if($id==$this->activeTab)
                $class='tab-pane active';
            else
                $class='tab-pane';

            echo CHtml::openTag('div', array('class'=>$class, 'id'=>$id))."\n";

            if(isset($tab['content']))
                echo $tab['content'];
            else if(isset($tab['view']))
            {
                if(isset($tab['data']))
                {
                    if(is_array($this->viewData))
                        $data=array_merge($this->viewData, $tab['data']);
                    else
                        $data=$tab['data'];
                }
                else
                    $data=$this->viewData;
                $this->getController()->renderPartial($tab['view'], $data);
            }
            echo "</div><!-- {$id} -->\n";
        }
        echo "</div>";
    }

    protected function normalizeTabs($tabs) {
        foreach($tabs as $id=>$tab)
        {
            if(isset($tab['visible']) && !$tab['visible'])
            {
                unset($tabs[$id]);
                continue;
            }

            if(!isset($tab['title']))
                $tabs[$id]['title']='undefined';

            if(!isset($tab['url']))
                $tabs[$id]['url']='#'.$id;
            else if(is_array($tab['url']))
                $tabs[$id]['url']=CHtml::normalizeUrl($tab['url']);
        }
        return $tabs;
    }

}