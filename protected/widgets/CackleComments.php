<?php
class CackleComments extends CWidget {

    public $channel;

    public function run() {
        $site_id=Yii::app()->config['cackle_site_id'];

        if(empty($site_id))
            throw new CException('Необходимо заполнить ID сайта CACKLE');

        echo '<div id="mc-container"></div>';

        $js="window.mcSite = '$site_id';";
        if($this->channel)
            $js.="\n window.mcChannel = '$this->channel';";

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('http://cackle.me/mc.widget-min.js', CClientScript::POS_END);
        $cs->registerScript(__CLASS__.'#', $js);
    }
}