<?php
class CommentsWidget extends CWidget {

    /** @var ActiveRecord */
    public $owner;
    /** @var Comment */
    public $comment;

    public function run()
    {
        if ($this->owner instanceof СActiveRecord) {
            throw new CHttpException('owner должен быть экземпляром класса СActiveRecord');
        }
        if (!$this->comment) {
            throw new CException('CommentsWidget::$comment does not set');
        }

        $this->comment->owner = get_class($this->owner);
        $this->comment->owner_id = $this->owner->id;

        echo '<div id="'.$this->id.'">';
        $this->renderForm();
        $this->renderComments();
        echo '</div>';

        $id = __CLASS__;
        $this->registerScripts($id);
    }

    protected function registerScripts($id) {

        /** @var CClientScript $cs */
        $cs = Yii::app()->clientScript;
        $cs->registerCoreScript('jquery');

$js = <<<JS
$('#comment-container').on('submit', '#{$this->comment->getClassId()}-form', function(){
    $.fancybox.showLoading();
    var fd = new FormData(document.querySelector('#{$this->comment->getClassId()}-form'));
    $.ajax({
        url: $(this).attr('action'),
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (html) {
            $('#comment-container').html(html);
        },
        error: function () {
            window.alert('Ошибка загрузки данных');
        }
    });

    $.fancybox.hideLoading();
    return false;
});
$('#comment-container').on('click', '.replay', function(){
var parentId = $(this).closest('.comment_view').data('id');
$('#Comment_parent_id').val(parentId);
$('#Comment_body').focus();
$('body').animate({
        scrollTop: $("#comment-form").offset().top
    }, 500);
});

$("#Comment_file1").on('change', function () {
    if ($(this).val()) {
        $('#Comment_file2').closest('.control-group').show();
    }
});
// Продублировал код)
$("#Comment_file2").on('change', function () {
    if ($(this).val()) {
        $('#Comment_file3').closest('.control-group').show();
    }
});

$('#comment-container').on('click', '.comment-delete', function() {
  if (window.confirm('Вы уверены, что хотите удалить комментарий?')) {
      var link = $(this);
      $.get(link.attr('href'), function() {
        link.closest('.comment_view').remove();
      })
  }
  return false;
});

$('.comment-update').fancybox();

JS;
        $cs->registerScript($id, $js);

    }

    protected function renderComments()
    {
        $this->render('commentsWidget/index', array('comments' => $this->owner->comments));
    }

    protected function renderForm()
    {
        $this->render('commentsWidget/_form', array('owner' => $this->owner, 'model' => $this->comment));
    }
}
