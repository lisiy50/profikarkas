<?php
class DisqusComments extends CWidget
{

    public function run()
    {
        $shortname = Yii::app()->config['disqus_site_shortname'];

        if(empty($shortname))
            throw new CException('Необходимо заполнить название сайта DISQUS');

        echo '<div id="disqus_thread"></div>';

        Yii::app()->getClientScript()->registerScriptFile('http://' . $shortname . '.disqus.com/embed.js', CClientScript::POS_END);
    }
}