<?php

class Redactor extends CInputWidget
{
    /**
     * Assets package ID.
     */
    const PACKAGE_ID = 'redactor';

    /**
     * @var array {@link http://imperavi.com/redactor/docs/ redactor options}.
     */
    public $options = array();

    /**
     * Init widget.
     */
    public function init()
    {
        parent::init();

        list($name, $id) = $this->resolveNameID();

        if (empty($this->htmlOptions['id']))
            $this->htmlOptions['id'] = $id;

        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];
        else
            $this->htmlOptions['name'] = $name;

        if ($this->hasModel())
            echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        else
            echo CHtml::textArea($name, $this->value, $this->htmlOptions);

        $this->registerClientScript();
    }

    /**
     * Register CSS and Script.
     */
    protected function registerClientScript()
    {
        $cs = Yii::app()->clientScript;
        if (!isset($cs->packages[self::PACKAGE_ID])) {
            $cs->packages[self::PACKAGE_ID] = array(
                'basePath' => 'application.assets.redactor',
                'js' => array('redactor' . (YII_DEBUG ? '' : '.min') . '.js', 'lang/ru.js'),
                'css' => array('redactor.css'),
                'depends' => array('jquery'),
            );
        }
        $cs->registerPackage(self::PACKAGE_ID);

        if(!isset($this->options['fileUpload']))
            $this->options['fileUpload'] = Yii::app()->createUrl('redactor/fileUpload');

        if(!isset($this->options['imageUpload']))
            $this->options['imageUpload'] = Yii::app()->createUrl('redactor/imageUpload');

        if(!isset($this->options['imageGetJson']))
            $this->options['imageGetJson'] = Yii::app()->createUrl('redactor/imageGetJson');

        if(!isset($this->options['minHeight']))
            $this->options['minHeight'] = 80;

        if(!isset($this->options['lang']))
            $this->options['lang'] = 'ru';

        $options = CJavaScript::encode($this->options);
        $id = $this->htmlOptions['id'];
        $cs->registerScript(__CLASS__ . '#' . $id, "$('#$id').redactor($options);", CClientScript::POS_READY);
    }
}
