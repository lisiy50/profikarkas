<?php
class Menu extends CWidget {

    public $htmlOptions;

    public $template='<a href="{url}">{name}</a>';

    public $nesting = 1;

    public $items;

    public $criteria;

    protected $requestUrl;

    public function init() {
        if(isset($this->htmlOptions['id']))
            $this->htmlOptions['id'];
        else
            $this->htmlOptions['id']=$this->getId();

        $this->requestUrl=current(explode('?', Yii::app()->request->url));

        echo CHtml::openTag('ul',$this->htmlOptions)."\n";
        echo $this->saveAsHtml($this->items, 1);
    }

    public function run()
    {
        echo "</ul>";
    }

    protected function saveAsHtml($items, $level) {
        $html='';

        foreach($items as $item) {
            if(isset($item['visible']) && $item['visible']===false)
                continue;

            $css='';

            if($this->nesting>$level && isset($item['hasChildren']) && $item['hasChildren']) {
                if($css!=='')
                    $css.=' ';
                $css.='hasChildren';
            }

            if(empty($item['url']))
                $item['url']='#';

            if(is_array($item['url']))
                $item['url']=CHtml::normalizeUrl($item['url']);

            $current=false;
            if(isset($item['current'])) {
                $current=$item['current'];
            } elseif($this->requestUrl==current(explode('?', $item['url']))) {
                $current=true;
            }

            if($current) {
                if($css!=='')
                    $css.=' ';
                $css.='current';
            }

            if($css!=='')
                $css=' class="'.$css.'"';

            $html.='<li'.$css.'>'.$this->renderTemplate($item, $level);

            if($this->nesting>$level && isset($item['hasChildren']) && $item['hasChildren']) {
                if($this->criteria && is_object($item) && $item instanceof CActiveRecord) {
                    if(is_string($this->criteria))
                        $this->criteria=array('condition'=>$this->criteria);

                    $children=$item->children($this->criteria);
                } else
                    $children=$item['children'];

                $html.="\n<ul>\n";
                $html.=$this->saveAsHtml($children, $level+1);
                $html.="</ul>\n";
            }
            $html.="</li>\n";
        }
        return $html;
    }

    protected function renderTemplate($item, $level) {
        $template=$this->getTemplate($level);
        preg_match_all('#{([a-z]+)}#i', $template, $matches);
        foreach($matches[1] as $param) {
            if(isset($item[$param]) && is_string($item[$param])) {
                $template = str_replace('{'.$param.'}', CHtml::encode($item[$param]), $template);
            }
        }
        return $template;
    }

    protected function getTemplate($level) {
        if(is_string($this->template))
            return $this->template;

        if(array_key_exists($level, $this->template))
            return $this->template[$level];

        if(array_key_exists('default', $this->template))
            return $this->template['default'];

        return reset($this->template);
    }
}