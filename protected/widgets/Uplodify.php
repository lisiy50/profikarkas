<?php
class Uplodify extends CWidget  {

    public $name;
	public $options=array();
    public $uploadUrl;
    public $htmlOptions=array();

    public function run() {
	    if(empty($this->uploadUrl))
			throw new CException(Yii::t('yii','В классе Uplodify должно быть определено свойство uploadUrl'));

        if(empty($this->htmlOptions['id']))
            $this->htmlOptions['id']=$this->id;

        $this->registerClientScript();

	    echo CHtml::fileField('Uplodify_'.$this->id,'',$this->htmlOptions);
    }

    public function registerClientScript() {
        $id=$this->htmlOptions['id'];

        $dir = Yii::getPathOfAlias('application.assets.uploadify');
        $baseUrl=Yii::app()->assetManager->publish($dir);

        $this->options=array_merge(array(
            'checkExisting'=>false,
            //'buttonText'=>'<i class="icon-upload icon-white"></i> Обзор...',
            'buttonImage'=>$baseUrl.'/images/uploadify-start.png',
            'width'=>89,
            'height'=>28,
            'auto'=>true,

        ), $this->options);

        $this->options['fileObjName']=empty($this->name)?'file':$this->name;

        if(empty($this->options['postData']))
            $this->options['postData']=array();

        $this->options['postData']['PHPSESSID']=Yii::app()->session->sessionID;
        $this->options['postData']['ajax']=1;

        $this->options['swf']=$baseUrl.'/swf/uploadify.swf';
        $this->options['cancelImage']=$baseUrl.'/images/uploadify-cancel.png';
        $this->options['uploader']=$this->uploadUrl;
        $options=CJavaScript::encode($this->options);

        $cs=Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($baseUrl.'/js/jquery.uploadify.min.js');
        $cs->registerCssFile($baseUrl.'/css/uploadify.css');
		$cs->registerScript('Yii.Uplodify#'.$id,"$(\"#{$id}\").uploadify({$options});");
    }

}