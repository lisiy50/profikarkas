<?php
class BootMenu extends CWidget {

   	public $items=array();

   	public $itemTemplate;

   	public $encodeLabel=true;

   	public $activeCssClass='active';

   	public $activateItems=true;

   	public $activateParents=false;

   	public $hideEmptyItems=true;

   	public $htmlOptions=array();

   	public $submenuHtmlOptions=array();

   	public $linkLabelWrapper;

   	public $firstItemCssClass;

   	public $lastItemCssClass;

   	public $itemCssClass;

    public $activeIconClass;

   	/**
   	 * Initializes the menu widget.
   	 * This method mainly normalizes the {@link items} property.
   	 * If this method is overridden, make sure the parent implementation is invoked.
   	 */
    public function init()
   	{
        if(empty($this->htmlOptions['class'])) {
            $this->htmlOptions['class']='nav';
        } else {
            $this->htmlOptions['class'].=' nav';
        }
        $this->htmlOptions['id']=$this->getId();
        $route=$this->getController()->getRoute();
        $this->items=$this->normalizeItems($this->items,$route,$hasActiveChild);
   	}

    public function run()
   	{
   		$this->renderMenu($this->items);
   	}

    protected function renderMenu($items)
   	{
   		if(count($items))
   		{
   			echo CHtml::openTag('ul',$this->htmlOptions)."\n";
   			$this->renderMenuRecursive($items);
   			echo CHtml::closeTag('ul');
   		}
   	}

    protected function renderMenuRecursive($items)
   	{
   		$count=0;
   		$n=count($items);
   		foreach($items as $item)
   		{
            if(isset($item['divider'])) {
                echo '<li class="divider"></li>';
                continue;
            }

   			$count++;
   			$options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
   			$class=array();
   			if($item['active'] && $this->activeCssClass!='')
   				$class[]=$this->activeCssClass;
   			if($count===1 && $this->firstItemCssClass!==null)
   				$class[]=$this->firstItemCssClass;
   			if($count===$n && $this->lastItemCssClass!==null)
   				$class[]=$this->lastItemCssClass;
   			if($this->itemCssClass!==null)
   				$class[]=$this->itemCssClass;
            if(isset($item['items']) && count($item['items']))
                $class[]='dropdown';
            if(!isset($item['url']))
                $class[]='nav-header';
   			if($class!==array())
   			{
   				if(empty($options['class']))
   					$options['class']=implode(' ',$class);
   				else
   					$options['class'].=' '.implode(' ',$class);
   			}

   			echo CHtml::openTag('li', $options);

   			$menu=$this->renderMenuItem($item);
   			if(isset($this->itemTemplate) || isset($item['template']))
   			{
   				$template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
   				echo strtr($template,array('{menu}'=>$menu));
   			}
   			else
   				echo $menu;

   			if(isset($item['items']) && count($item['items']))
   			{
                $submenuOptions=isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions;

                if(isset($submenuOptions['class']))
                    $submenuOptions['class'].=' dropdown-menu';
                else
                    $submenuOptions['class']='dropdown-menu';

   				echo "\n".CHtml::openTag('ul',$submenuOptions)."\n";
   				$this->renderMenuRecursive($item['items']);
   				echo CHtml::closeTag('ul')."\n";
   			}

   			echo CHtml::closeTag('li')."\n";
   		}
   	}

    protected function renderMenuItem($item)
   	{
        if(isset($item['icon']))
        {
            $class='';
            if (strpos($item['icon'], 'icon') === false)
            {
                $pieces = explode(' ', $item['icon']);
                $class = 'icon-'.implode(' icon-', $pieces);
            }
            if($this->activeIconClass!==null && $item['active'])
                $class.=($class)?' '.$this->activeIconClass:$this->activeIconClass;
            $item['label'] = '<i class="'.$class.'"></i> '.$item['label'];
        }
        if(isset($item['items']) && count($item['items'])) {
            if (!isset($item['linkOptions']))
          	    $item['linkOptions'] = array();

            if (isset($item['linkOptions']['class']))
                $item['linkOptions']['class'] .= ' dropdown-toggle';
            else
                $item['linkOptions']['class'] = 'dropdown-toggle';

            $item['linkOptions']['data-toggle'] = 'dropdown';
            $item['label'].=' <span class="caret"></span>';
        }
   		if(isset($item['url']))
   		{
   			$label=$this->linkLabelWrapper===null ? $item['label'] : '<'.$this->linkLabelWrapper.'>'.$item['label'].'</'.$this->linkLabelWrapper.'>';
   			return CHtml::link($label,$item['url'],isset($item['linkOptions']) ? $item['linkOptions'] : array());
   		}
   		else
   			return $item['label'];
   	}

    protected function normalizeItems($items,$route,&$active)
   	{
   		foreach($items as $i=>$item)
   		{
            if(!is_array($item)) {
                $items[$i]=array('divider'=>true);
                continue;
            }

   			if(isset($item['visible']) && !$item['visible'])
   			{
   				unset($items[$i]);
   				continue;
   			}
   			if(!isset($item['label']))
   				$item['label']='';
   			if($this->encodeLabel)
   				$items[$i]['label']=CHtml::encode($item['label']);
   			$hasActiveChild=false;
   			if(isset($item['items']) && is_array($item['items']))
   			{
   				$items[$i]['items']=$this->normalizeItems($item['items'],$route,$hasActiveChild);
   				if(empty($items[$i]['items']) && $this->hideEmptyItems)
   				{
   					unset($items[$i]['items']);
   					if(!isset($item['url']))
   					{
   						unset($items[$i]);
   						continue;
   					}
   				}
   			}
   			if(!isset($item['active']))
   			{
   				if($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item,$route))
   					$active=$items[$i]['active']=true;
   				else
   					$items[$i]['active']=false;
   			}
   			else if($item['active'])
   				$active=true;
   		}
   		return array_values($items);
   	}

    protected function isItemActive($item,$route)
   	{
   		if(isset($item['url']) && is_array($item['url']) && !strcasecmp(trim($item['url'][0],'/'),$route))
   		{
   			if(count($item['url'])>1)
   			{
   				foreach(array_splice($item['url'],1) as $name=>$value)
   				{
   					if(!isset($_GET[$name]) || $_GET[$name]!=$value)
   						return false;
   				}
   			}
   			return true;
   		}
   		return false;
   	}

}