<?php
return array(
    'openseadragon'=>array(
        'basePath'=>'application.assets.openseadragon',
        'js'=>array('openseadragon.js', 'openseadragon.min.js.map'),
    ),
    'waitforimages'=>array(
        'basePath'=>'application.assets.waitforimages',
        'js'=>array('jquery.waitforimages.js'),
        'depends'=>array('jquery'),
    ),
    'bootstrap'=>array(
        'basePath'=>'application.assets.bootstrap',
        'js'=>array('js/bootstrap.min.js'),
        'css'=>array('css/bootstrap.css'),
        'depends'=>array('jquery'),
    ),
    'bootstrap3'=>array(
        'basePath'=>'application.assets.bootstrap310',
        'js'=>array('js/bootstrap.min.js'),
//        'css'=>array('css/bootstrap.min.css'),
        'depends'=>array('jquery'),
    ),
    'knockout'=>array(
        'basePath'=>'application.assets.knockout',
        'js'=>array('knockout-2.1.0.js'),
    ),
    'fancybox'=>array(
        'basePath'=>'application.assets.fancybox',
        'js'=>array('jquery.mousewheel-3.0.6.pack.js', 'jquery.fancybox.pack.js', 'helpers/jquery.fancybox-buttons.js'),
        'css'=>array('jquery.fancybox.css', 'helpers/jquery.fancybox-buttons.css'),
        'depends'=>array('jquery'),
    ),
    'fancyboxHelpers'=>array(
        'basePath'=>'application.assets.fancybox.helpers',
        'js'=>array('jquery.fancybox-media.js'),
        'depends'=>array('fancybox'),
    ),
    'carouFredSel'=>array(
        'basePath'=>'application.assets.carouFredSel',
        'js'=>array('jquery.carouFredSel-6.1.0-packed.js'),
        'depends'=>array('jquery'),
    ),
    'jgrowl'=>array(
        'basePath'=>'application.assets.jgrowl',
        'js'=>array('jquery.jgrowl.js'),
        'css'=>array('jquery.jgrowl.css', 'reset.css'),
        'depends'=>array('jquery'),
    ),
    'redactor'=>array(
        'basePath' => 'application.assets.redactor',
        'js' => array('redactor' . (YII_DEBUG ? '' : '.min') . '.js', 'lang/ru.js'),
        'css' => array('redactor.css'),
        'depends' => array('jquery'),
    ),
    'mcDropdown'=>array(
        'basePath' => 'application.assets.mcDropdown',
        'js' => array('js/jquery.mcdropdown' . (YII_DEBUG ? '' : '.min') . '.js'),
        'css' => array('css/jquery.mcdropdown' . (YII_DEBUG ? '' : '.min') . '.css'),
        'depends' => array('jquery'),
    ),
    'elevateZoom'=>array(
        'basePath' => 'application.assets.elevateZoom',
        'js' => array('jquery.elevateZoom-2.5.5.min.js'),
        'depends' => array('jquery'),
    ),
    'elevateZoom3'=>array(
        'basePath' => 'application.assets.elevatezoom-master',
        'js' => array('jquery.elevateZoom-3.0.8.min.js'),
        'depends' => array('jquery'),
    ),
    'formStyler'=>array(
        'basePath' => 'application.assets.formStyler',
        'js' => array('jquery.formstyler.min.js'),
        'css'=>array('jquery.formstyler.css'),
        'depends' => array('jquery'),
    ),
    'modernizr'=>array(
        'basePath' => 'application.assets.modernizr',
        'js' => array('modernizr.custom.js'),
    ),
    'multiSelect'=>array(
        'basePath' => 'application.assets.multiSelect',
        'js' => array('jquery.multiSelect.js'),
        'depends' => array('jquery'),
    ),
    'cookieWithoutJquery'=>array(
        'js'=>array('jquery.cookie.js'),
    ),
    'landing'=>array(
        'basePath' => 'application.frontend.assets',
        'js'=>array('js/landing.js'),
        'css'=>array('css/landing.css'),
        'depends' => array('cookieWithoutJquery'),
    ),
);
