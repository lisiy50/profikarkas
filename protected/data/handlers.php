<?php
return array(
    'onOrder'=>array(
        array('MailEvent', 'newOrder'),
        array('SmsEvent', 'newOrder'),
    ),
    'onCreateComment'=>array(
        array('MailEvent', 'onCreateComment'),
    ),
    'onCommentReply'=>array(
        array('MailEvent', 'onCommentReply'),
    ),
    'onRemindPassword'=>array(
        array('MailEvent', 'onRemindPassword'),
    ),
);