<?php

/**
 * Class ZohoConnector
 */

class ZohoConnector
{
    /**
     * @param string $xml
     * @return mixed
     */
    public static function sendXml($xml)
    {
        $xmlData = urlencode($xml);
        $url = "https://desk.zoho.com/api/xml/requests/addrecords?authtoken=aba29d702efaa8f6e43dfa1b89a557d0&portal=z500profi&department=Profikarkas&xml={$xmlData}";
//        $url = "https://desk.zoho.com/api/xml/requests/addrecords";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt( $ch, CURLOPT_POSTFIELDS, "authtoken=aba29d702efaa8f6e43dfa1b89a557d0&portal=z500profi&department=z500&xml={$xmlData}" );
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * @param string $subject
     * @param string|null $contactName
     * @param string|null $email
     * @param string|null $phone
     * @param string|null $description
     * @return string
     */
    public static function generateXml($subject, $contactName = null, $email = null, $phone = null, $description = null)
    {
        $dom = new \DOMDocument("1.0", "utf-8");
        $requests = $dom->createElement("requests");
        $row = $dom->createElement('row');
        $row->setAttribute('no', 1);

        $fl = $dom->createElement('fl', $subject);
        $fl->setAttribute('val', 'Subject');
        $row->appendChild($fl);

        $fl = $dom->createElement('fl', $contactName);
        $fl->setAttribute('val', 'Contact Name');
        $row->appendChild($fl);

        $fl = $dom->createElement('fl', $email);
        $fl->setAttribute('val', 'Email');
        $row->appendChild($fl);

        $fl = $dom->createElement('fl', $phone);
        $fl->setAttribute('val', 'Phone');
        $row->appendChild($fl);

        $fl = $dom->createElement('fl', $description);
        $fl->setAttribute('val', 'Description');
        $row->appendChild($fl);

        $requests->appendChild($row);

        $dom->appendChild($requests);

        return $dom->saveXML();
    }
}

ZohoConnector::sendXml(ZohoConnector::generateXml('subject', 'contact name', 'email', 'phone', 'description'));
