<?php

/**
 * Class ZohoHelper
 */

class ZohoHelper
{
    public static function sendData($model)
    {

        $ch = curl_init();

        $data = array(
            'name' => $model->name,
            'phone' => $model->phone ? $model->phone : '',
            'subject' => $model->subject ? $model->subject : '',
            'email' => $model->email ? $model->email : '',
            'comment' => $model->comment ? $model->comment : '',
        );

        curl_setopt($ch, CURLOPT_URL, "https://profikarkas.com.ua/zohocrm/application.php");
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type' => 'application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return true;
    }
}

