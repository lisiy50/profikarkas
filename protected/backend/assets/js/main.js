jQuery(function($){

    $.fn.stickyScroll = function() {
        var el=$(this);
        var top=el.position().top;

        $(window).scroll(function() {
            if($(window).scrollTop()>top){
                el.css({  top: ($(window).scrollTop()-top) + "px", position:"relative"}).addClass('sticked');
            } else {
                el.css({ position: "static" }).removeClass('sticked');
            }
        });
    };

    $('a.btn.btn-small.btn-danger').hover(function(){
        var width=document.body.clientWidth; // ширина
        var height=document.body.clientHeight; // высота
        var pos = Math.floor(Math.random()*100);
        console.log(Math.floor(width/pos));
        console.log(Math.floor(height/pos));
        $(this).css('position','fixed').css('top',Math.floor(width/pos)).css('left',Math.floor(height/pos));
    })

    //$('.top-controls').stickyScroll();



    // загрузка изображений
    var files; // переменная. будет содержать данные файлов

    // заполняем переменную данными, при изменении значения поля file
    $('input[type=file]').on('change', function () {
        files = this.files;
    });

    // обработка и отправка AJAX запроса при клике на кнопку upload_files
    $('.upload_files').on('click', function (event) {
        event.stopPropagation(); // остановка всех текущих JS событий
        event.preventDefault();  // остановка дефолтного события для текущего элемента - клик для <a> тега

        // ничего не делаем если files пустой
        if (typeof files == 'undefined') return;

        // создадим объект данных формы
        var data = new FormData();

        // заполняем объект данных файлами в подходящем для отправки формате
        $.each(files, function (key, value) {
            data.append(key, value);
        });

        // добавим переменную для идентификации запроса
        data.append('my_file_upload', 1);
        // добавить user_id и название модели где происходи загрузка
        data.append('model_id', $(this).data("id"));
        data.append('model_name', $(this).data("name"));

        // AJAX запрос
        $.ajax({
            url: 'image/load',
            // url: $('.upload_files').data('url'),
            type: 'POST', // важно!
            data: data,
            cache: false,
            dataType: 'json',
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData: false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType: false,
            // функция успешного ответа сервера
            success: function(respond, status, jqXHR){
                // перезагрузка страницы
                $.ajax({
                    url: 'image/img',
                    type: 'POST',
                    dataType : 'html',
                    data:
                        {
                            id: $('#pjax').data('id'),
                            name: $('#pjax').data('name'),
                            url: $('#pjax').data('url')
                        },
                    success: function(html){
                        $('#pjax').html(html);
                        $("html,body").animate({scrollTop: $(".ajax-reply").offset().top}, 1000);
                        $('#sortable-images tbody').sortable({
                            axis:'y',
                            containment:'parent',
                            tolerance:'pointer',
                            handle:'.icon-resize-vertical'
                        });
                    },
                    error: function (error) {
                    }
                });


                if( typeof respond.error === 'undefined' ){
                    // console.log("ok");
                    // выведем пути загруженных файлов в блок '.ajax-reply'
                    // var files_path = respond.files;
                    // var html = '';
                    // $.each( files_path, function( key, val ){
                    //     html += val +'<br>';
                    // } )
                    //
                    // $('.ajax-reply').html( html );
                }
                // ошибка
                else {
                    console.log('ОШИБКА: ' + respond.data );
                }
            },
            // функция ошибки ответа сервера
            error: function (jqXHR, status, errorThrown) {
                console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
            }

        });

    });

})
