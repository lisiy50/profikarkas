<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->dropDownListRow($model, 'category', Lookup::items('PackagingHomeKitCategory'), array('prompt'=>'', 'class'=>'span12'));

    echo $form->dropDownListRow($model, 'parent_id', CHtml::listData(PackagingHomeKit::model()->rooted()->findAll(), 'id', 'name'), array('prompt'=>'', 'class'=>'span12'));

    echo $form->textFieldRow($model, 'name', array('class' => 'span12'));

    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    $this->endWidget();
    ?>

</div><!-- form -->