<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => get_class($model) . '-form',
    'enableAjaxValidation' => true,
)); ?>

        <h4>Найтройка проекта</h4>

        <?php
        echo $form->checkBoxRow($model, 'project_house_equipment_link_visibility');
        echo $form->checkBoxRow($model, 'project_faq_link_visibility');
        echo $form->checkBoxRow($model, 'project_mounting_video_link_visibility');
        echo $form->checkBoxRow($model, 'project_building_steps_link_visibility');
        echo $form->checkBoxRow($model, 'show_actual_cost');

        ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->