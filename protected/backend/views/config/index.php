<?php $this->beginContent('/layouts/main'); ?>

    <div class="row">
        <div class="span6">
            <h4 class="nomargin">
                <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
                <?php echo CHtml::encode($this->header); ?>
            </h4>
        </div>
        <div class="pull-right">
            <button class="btn btn-success btn-small" type="submit" onclick="$('#Config-form').submit();"><i class="icon-ok icon-white"></i> Сохранить</button>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="span9"><?php echo $content; ?></div>
        <div class="span3">
        <?php $this->widget('BootMenu', array(
            'activeIconClass'=>'icon-white',
            'items'=>$this->menu,
            'htmlOptions'=>array(
                'class'=>'nav-list'
            )
        )); ?>
        </div>
    </div>
<?php $this->endContent(); ?>