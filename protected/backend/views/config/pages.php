<?php
/**
 * @var Config $model
 */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));
    /* @var BootActiveForm $from*/
    ?>

<!--    --><?php //$this->beginClip('uk'); ?>

    <h4>Страница портфолио</h4>

    <?php echo $form->widgetFieldRow($model, 'portfolio_text_uk', 'Redactor'); ?>

    <h4>Страница проектов</h4>

    <?php echo $form->widgetFieldRow($model, 'product_text_uk', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_dachi_uk', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_zagorodnie_uk', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'project_price_tab_text_uk', 'Redactor'); ?>

    <h4>Главная страница</h4>

    <?php echo $form->widgetFieldRow($model, 'main_text_uk', 'Redactor'); ?>

    <h4>Страница контактов</h4>

    <?php echo $form->widgetFieldRow($model, 'contact_text_uk', 'Redactor'); ?>

    <h4>Страница стоимости и вкладка стоимость</h4>

    <?php echo $form->textFieldRow($model, 'price_notice_default_2_uk', array('class' => 'span8')); ?>

<!--    --><?php //$this->endClip(); ?>


<!--    --><?php //$this->beginClip('ru'); ?>

    <h4>Страница портфолио</h4>

    <?php echo $form->widgetFieldRow($model, 'portfolio_text', 'Redactor'); ?>

    <h4>Страница проектов</h4>

    <?php echo $form->widgetFieldRow($model, 'product_text', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_dachi', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_zagorodnie', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'project_price_tab_text', 'Redactor'); ?>

    <h4>Главная страница</h4>

    <?php echo $form->widgetFieldRow($model, 'main_text', 'Redactor'); ?>

    <h4>Страница контактов</h4>

    <?php echo $form->widgetFieldRow($model, 'contact_text', 'Redactor'); ?>

<!--    --><?php //$this->endClip(); ?>


<!--    --><?php //$this->beginClip('en'); ?>

    <h4>Страница портфолио</h4>

    <?php echo $form->widgetFieldRow($model, 'portfolio_text_en', 'Redactor'); ?>

    <h4>Страница проектов</h4>

    <?php echo $form->widgetFieldRow($model, 'product_text_en', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_dachi_en', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_zagorodnie_en', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'project_price_tab_text_en', 'Redactor'); ?>

    <h4>Главная страница</h4>

    <?php echo $form->widgetFieldRow($model, 'main_text_en', 'Redactor'); ?>

    <h4>Страница контактов</h4>

    <?php echo $form->widgetFieldRow($model, 'contact_text_en', 'Redactor'); ?>

<!--    --><?php //$this->endClip(); ?>


<!--    --><?php //$this->beginClip('de'); ?>

    <h4>Страница портфолио</h4>

    <?php echo $form->widgetFieldRow($model, 'portfolio_text_de', 'Redactor'); ?>

    <h4>Страница проектов</h4>

    <?php echo $form->widgetFieldRow($model, 'product_text_de', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_dachi_de', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_zagorodnie_de', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'project_price_tab_text_de', 'Redactor'); ?>

    <h4>Главная страница</h4>

    <?php echo $form->widgetFieldRow($model, 'main_text_de', 'Redactor'); ?>

    <h4>Страница контактов</h4>

    <?php echo $form->widgetFieldRow($model, 'contact_text_de', 'Redactor'); ?>

<!--    --><?php //$this->endClip(); ?>


<!--    --><?php //$this->beginClip('fr'); ?>

    <h4>Страница портфолио</h4>

    <?php echo $form->widgetFieldRow($model, 'portfolio_text_fr', 'Redactor'); ?>

    <h4>Страница проектов</h4>

    <?php echo $form->widgetFieldRow($model, 'product_text_fr', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_dachi_fr', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'product_categiry_zagorodnie_fr', 'Redactor'); ?>

    <?php echo $form->widgetFieldRow($model, 'project_price_tab_text_fr', 'Redactor'); ?>

    <h4>Главная страница</h4>

    <?php echo $form->widgetFieldRow($model, 'main_text_fr', 'Redactor'); ?>

    <h4>Страница контактов</h4>

    <?php echo $form->widgetFieldRow($model, 'contact_text_fr', 'Redactor'); ?>

<!--    --><?php //$this->endClip(); ?>


    <?php
//    $this->beginClip('basic');
    echo $form->textFieldRow($model, 'price_notice_default_1', array('class' => 'span8'));
    echo $form->textFieldRow($model, 'contact_map', array('class' => 'span8'));
    echo $form->checkBoxRow($model, 'contact_use_captcha', array('value' => 1));
//    $this->endClip();
    ?>

    <?php $this->endWidget(); ?>

    <?php

//    $this->widget(
//        'BootTabView',
//        array(
//            'activeTab' => 'uk',
//            'viewData' => array(
//                'model' => $model,
//                'form' => $form,
//            ),
//            'tabs' => array(
//                'uk' => array(
//                    'title' => 'Украинский',
//                    'content' => $this->clips['uk'],
//                ),
//                'ru' => array(
//                    'title' => 'Русский',
//                    'content' => $this->clips['ru'],
//                ),
//                'en' => array(
//                    'title' => 'Английский',
//                    'content' => $this->clips['en'],
//                ),
//                'de' => array(
//                    'title' => 'Немецкий',
//                    'content' => $this->clips['de'],
//                ),
//                'fr' => array(
//                    'title' => 'Французский',
//                    'content' => $this->clips['fr'],
//                ),
//                'basic' => array(
//                    'title' => 'Общее',
//                    'content' => $this->clips['basic'],
//                ),
//            )
//        )
//    );

    ?>

</div>
