<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => get_class($model) . '-form',
    'enableAjaxValidation' => true,
)); ?>

        <h4>Найтройка оповещения на ел.почту</h4>

        <?php echo CHtml::errorSummary($model); ?>description

        <?php
        echo $form->checkBoxRow($model, 'mailing_new_order_to_admin');

        echo $form->checkBoxRow($model, 'mailing_new_order_to_user');

        echo CHtml::openTag('div', array('class'=>'mail-pattern'));
        echo $form->textFieldRow($model, 'mailing_new_order_subject', array('class'=>'input-xxlarge'));
        echo $form->widgetFieldRow($model, 'mailing_new_order_pattern', 'CodeMirror');
        echo '</div>';
        ?>

        <h4>Найтройка оповещения через sms</h4>

        <?php
        echo $form->checkBoxRow($model, 'smsing_new_order_to_admin');


        echo CHtml::openTag('div', array('class'=>'order-phones'));
        echo $form->textFieldRow($model, 'smsing_new_order_phones', array('hint' => 'Например: (050)333-33-33, (068)333-33-33', 'class'=>'input-xxlarge'));
        echo '</div>';
        ?>

        <h4>Оформление заказа</h4>

        <?php
        echo $form->checkBoxRow($model, 'payment_required');

        echo $form->checkBoxRow($model, 'delivery_required');
        ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$cs = Yii::app()->clientScript;
$cs->registerScript('use_mailing_to_user', "
        $('#Config_mailing_new_order_to_user').change(function(){
            if($(this).is(':checked'))
                $('div.mail-pattern').show();
            else
                $('div.mail-pattern').hide();
        }).change();
    ");
$cs->registerScript('use_smsing_to_user', "
        $('#Config_smsing_new_order_to_admin').change(function(){
            if($(this).is(':checked'))
                $('div.order-phones').show();
            else
                $('div.order-phones').hide();
        }).change();
    ");
?>