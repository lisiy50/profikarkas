<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    )); ?>

    <div class="row">
        <div class="span4">
            <h4>Товары</h4>
            <?php
            echo $form->textFieldRow($model, 'product_catalog_limit', array('maxlength' => 3));

            echo $form->dropDownListRow($model, 'product_catalog_order', array(
                'priority DESC' => 'По приоритету',
                'price' => 'Дешевые сначала',
                'price DESC' => 'Дорогие сначала',
                'name' => 'По названию',
                'hit DESC' => 'Спец предложения сначала',
                'browse DESC' => 'Популярные сначала',
            ));

            echo $form->dropDownListRow($model, 'similar_price_accuracy', array(
                '0.01' => 'Разница в цене 1%',
                '0.02' => 'Разница в цене 2%',
                '0.03' => 'Разница в цене 3%',
                '0.05' => 'Разница в цене 5%',
                '0.10' => 'Разница в цене 10%',
                '0.15' => 'Разница в цене 15%',
                '0.20' => 'Разница в цене 20%',
                '0.30' => 'Разница в цене 30%',
            ));
            ?>
        </div>
        <div class="span4">
            <h4>Цены</h4>
            <?php
            echo $form->dropDownListRow($model, 'price_accuracy', array('Целое число', '1 цифра после запятой', '2 цифры после запятой', '3 цифры после запятой'));

            echo $form->dropDownListRow($model, 'currency_default', $model->getCurrencyList());

            echo $form->dropDownListRow($model, 'currency_basic', $model->getCurrencyList());
            ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <h4>Поиск</h4>
            <?php echo $form->textFieldRow($model, 'product_search_limit', array('maxlength' => 3)); ?>
            <?php
            echo $form->dropDownListRow($model, 'product_search_order', array(
                'price' => 'Дешевые сначала',
                'price DESC' => 'Дорогие сначала',
                'name' => 'По названию',
                'hit DESC' => 'Спец предложения сначала',
                'browse DESC' => 'Популярные сначала',
            ));
            ?>
        </div>
        <div class="span4">
            <h4>Новости</h4>
            <?php echo $form->textFieldRow($model, 'news_catalog_limit', array('maxlength' => 3)); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->