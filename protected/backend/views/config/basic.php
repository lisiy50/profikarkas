<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => get_class($model) . '-form',
    'enableAjaxValidation' => true,
)); ?>
    <div class="row">
        <div class="span4">
            <?php
            echo $form->textFieldRow($model, 'shop_name', array('maxlength' => 64, 'class' => 'span4'));
            ?>
        </div>
        <div class="span4">
            <?php
            echo $form->textFieldRow($model, 'company', array('maxlength' => 64, 'class' => 'span4'));
            ?>
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <?php
            echo $form->textFieldRow($model, 'admin_email', array('maxlength' => 255, 'class' => 'span4'));
            ?>
        </div>
        <div class="span4">
            <?php
            echo $form->textFieldRow($model, 'contact_email', array('maxlength' => 255, 'class' => 'span4'));
            ?>
        </div>
    </div>



    <?php
    echo $form->textFieldRow($model, 'contact_phone', array('maxlength' => 255, 'class' => 'span6'));
    ?>

    <?php
    echo $form->textFieldRow($model, 'contact_phone_int', array('maxlength' => 32, 'class' => 'span6'));
    ?>

    <?php
    echo $form->textFieldRow($model, 'contact_address_uk', array('maxlength' => 500, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'contact_address', array('maxlength' => 500, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'contact_address_en', array('maxlength' => 500, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'contact_address_de', array('maxlength' => 500, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'contact_address_fr', array('maxlength' => 500, 'class' => 'span8'));
    ?>

    <?php
    echo $form->widgetFieldRow($model, 'counters', 'CodeMirror', array(
        'htmlOptions' => array('style' => 'width:680px;')
    ));
    ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->
