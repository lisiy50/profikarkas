<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    )); ?>

    <h4>Интеграция с Вконтакте</h4>

    <?php
    echo $form->textFieldRow($model, 'vkontakte_api_id', array('maxlength' => 11));

    echo $form->textFieldRow($model, 'vkontakte_poll_id', array('maxlength' => 32));

    echo $form->textFieldRow($model, 'vkontakte_group_id', array('maxlength' => 11));
    ?>

    <a href="http://vk.com/developers.php?oid=-1&p=Poll" target="_blank"><i class="icon-info-sign"></i> Настройка и редактирование голосований</a>

    <h4>Интеграция с системой комментирования <b style="color:#4FA3DA">CACKL</b><b style="color:#F65077">E</b></h4>

    <?php
    echo $form->textFieldRow($model, 'cackle_site_id', array('maxlength' => 11));
    ?>

    <a href="http://ru.cackle.me/site/comments" target="_blank"><i class="icon-info-sign"></i> Информация и администрирование</a>


    <h4>Интеграция с системой комментирования <b style="color:#5B88A6">DISQ</b><b style="color:#FAA21B">US</b></h4>

    <?php
    echo $form->textFieldRow($model, 'disqus_site_shortname', array('maxlength' => 50));
    ?>

    <a href="http://disqus.com/admin/moderate" target="_blank"><i class="icon-info-sign"></i> Информация и администрирование</a>


    <?php $this->endWidget(); ?>

</div><!-- form -->