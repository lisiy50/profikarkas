<?php
$this->breadcrumbs=array(
    'Валюты'=>array('index'),
    'Добавление',
);
$this->icon = 'money_add';
?>

<div class="row">
    <div class="span8">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Добавление валюты
        </h4>
    </div>

    <div class="pull-right">
        <button class="btn btn-success btn-small" type="button" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-plus icon-white"></i> Добавить</button>
    </div>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>