<?php
$this->breadcrumbs=array(
	'Валюты',
);
?>

<div class="row">

    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Валюты
        </h4>
    </div>
    <div class="pull-right">
        <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-small" ><i class="icon-plus"></i> Добавить</a>
    </div>

</div>

<hr>

<div class="form">
<?php $form=$this->beginWidget('BootActiveForm', array(
	'id'=>'currency-batch-form',
)); ?>

<?php echo $form->errorSummary($currencies); ?>

<table class="table table-striped">
  <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Название</th>
        <th>Префикс</th>
        <th>Суффикс</th>
        <th>Код ISO</th>
        <th>Курс от</th>
        <th>Курс к</th>
        <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
<?php foreach($currencies as $currency): $i=$currency->id;?>
    <tr>
        <td><i class="icon-resize-vertical" style="margin-top: 6px; cursor: row-resize;"></i></td>
        <td><?php echo $form->textField($currency, "[$i]name", array('style'=>'width:400px')); ?></td>
        <td><?php echo $form->textField($currency, "[$i]prefix", array('class'=>'input-mini')); ?></td>
        <td><?php echo $form->textField($currency, "[$i]suffix", array('class'=>'input-mini')); ?></td>
        <td><?php echo $form->textField($currency, "[$i]code", array('class'=>'input-mini')); ?></td>
        <td><?php echo $form->textField($currency, "[$i]ratio_from", array('class'=>'input-mini')); ?></td>
        <td><?php echo $form->textField($currency, "[$i]ratio_to", array('class'=>'input-mini')); ?></td>
        <td>
            <?php echo CHtml::link('<i class="icon-trash" style="margin-top: 6px; cursor: pointer;"></i>', '#', array('submit'=>array('delete','id'=>$currency->id),'confirm'=>'Вы уверены, что хотите удалить валюту?'));?>
        </td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
<?php $this->endWidget(); ?>
</div><!-- form -->

<script type="text/javascript">
    jQuery(function(){
        $('#currency-batch-form tbody').sortable({
            axis: 'y',
            containment:'parent',
            tolerance:'pointer',
            handle:'.icon-resize-vertical'
        });
    })
</script>
