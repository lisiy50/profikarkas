<div class="form">

    <hr>

    <?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => get_class($model) . '-form',
    'enableClientValidation' => true,
)); ?>

    <?php
    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array('maxlength' => 20));

    echo $form->textFieldRow($model, 'prefix', array('maxlength' => 20));

    echo $form->textFieldRow($model, 'suffix', array('maxlength' => 20));

    echo $form->textFieldRow($model, 'code', array('maxlength' => 3));

    echo $form->textFieldRow($model, 'ratio_from', array('maxlength' => 7));

    echo $form->textFieldRow($model, 'ratio_to', array('maxlength' => 7));
    ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->