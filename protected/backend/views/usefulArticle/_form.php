<?php
/**
 * @var $form BootActiveForm
 * @var $model UsefulArticle
 */
?>
<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    echo $form->errorSummary($model);

    $this->beginClip('basic');
    ?>

    <div class="row">
        <div class="span-8">
            <?php echo $form->textFieldRow($model, 'title_uk', array(
                'maxlength' => 255,
                'class' => 'input-xxlarge'
            ));?>
            <?php echo $form->textFieldRow($model, 'title', array(
                'maxlength' => 255,
                'class' => 'input-xxlarge'
            ));?>
            <?php echo $form->textFieldRow($model, 'title_en', array(
                'maxlength' => 255,
                'class' => 'input-xxlarge'
            ));?>
            <?php echo $form->textFieldRow($model, 'title_de', array(
                'maxlength' => 255,
                'class' => 'input-xxlarge'
            ));?>
            <?php echo $form->textFieldRow($model, 'title_fr', array(
                'maxlength' => 255,
                'class' => 'input-xxlarge'
            ));?>
        </div>
        <div class="span-4">

            <?php echo $form->labelEx($model, 'title_color')?>
            <?php
            $this->widget('ColorPicker', array(
                'model' => $model,
                'attribute' => 'title_color',
                'options' => array( // Optional
                    'pickerDefault' => "333333", // Configuration Object for JS
                    'colors' => array("#ffffff","#000000","#eeece1","#1f497d","#4f81bd","#c0504d","#9bbb59","#8064a2","#4bacc6","#f79646","#ffff00","#f2f2f2","#7f7f7f","#ddd9c3","#c6d9f0","#dbe5f1","#f2dcdb","#ebf1dd","#e5e0ec","#dbeef3","#fdeada","#fff2ca","#d8d8d8","#595959","#c4bd97","#8db3e2","#b8cce4","#e5b9b7","#d7e3bc","#ccc1d9","#b7dde8","#fbd5b5","#ffe694","#bfbfbf","#3f3f3f","#938953","#548dd4","#95b3d7","#d99694","#c3d69b","#b2a2c7","#b7dde8","#fac08f","#f2c314","#a5a5a5","#262626","#494429","#17365d","#366092","#953734","#76923c","#5f497a","#92cddc","#e36c09","#c09100","#7f7f7f","#0c0c0c","#1d1b10","#0f243e","#244061","#632423","#4f6128","#3f3151","#31859b","#974806","#7f6000","#ba3133","#333333","#747474"),
                ),
            ));
            ?>
        </div>
    </div>

    <?php echo $form->textFieldRow($model, 'project_name_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    )); ?>

    <?php echo $form->textFieldRow($model, 'project_name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    )); ?>

    <?php echo $form->textFieldRow($model, 'project_name_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    )); ?>

    <?php echo $form->textFieldRow($model, 'project_name_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    )); ?>

    <?php echo $form->textFieldRow($model, 'project_name_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    )); ?>

    <?php echo $form->imageFileRow($model, 'image');?>

    <div class="row">
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code1', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service1', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code2', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service2', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code3', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service3', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code4', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service4', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code5', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service5', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code6', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service6', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
    </div>

    <?php
    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'status_main', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->dropDownListRow($model, 'section', $model->getSectionList(), array('prompt'=>''));


    ?>

    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php

    echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'description', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_de', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'seo' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            )
        ),
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
