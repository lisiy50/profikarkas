<?php
/* @var $this LandingtextController */

/* @var $model LandingText */
/* @var $form CActiveForm */
/* @var $count */
?>


<?php $formText = $this->beginWidget(
    'BootActiveForm',
    array(
        'id' => "landing-text-form",
        'enableAjaxValidation' => false,
    )
); ?>

<?= $formText->hiddenField($model, 'id', array('value' => $model->id)) ?>
<?= $formText->hiddenField($model, 'landing_id', array('value' => $model->landing_id)) ?>

<div class="row row-title">
    <div class="span12 span12-landing">
        <?php echo $formText->labelEx($model, 'name'); ?>
        <?php echo $formText->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
    </div>
</div>

<div class="row row-notice" style="margin-bottom: 5px;">
    <div class="span12 span12-landing">
        <?php echo $formText->labelEx($model, 'name_manager'); ?>
        <?php echo $formText->textField($model, 'name_manager', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div class="span6 span6-landing">
        <?= $formText->widgetFieldRow($model, 'text_ru', 'Redactor', ['id' => 'ru' . $count]); ?>
    </div>
    <div class="span6 span6-landing">
        <?= $formText->widgetFieldRow($model, 'text_ua', 'Redactor', ['id' => 'ua' . $count]); ?>
    </div>
</div>

<div class="row">
    <div class="span12 span12-landing">
        <?= CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => "btn-new btn-blue button_change{$count}"]); ?>
    </div>
</div>

<?php $this->endWidget(); ?>



