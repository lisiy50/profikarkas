<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->beginClip('basic'); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>

        <div class="span4">
            <?php echo $form->widgetFieldRow($model, 'publish_date', 'zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'input-small'
                ),
            )); ?>
        </div>

        <div class="span8">
            <?php echo $form->textFieldRow($model, 'name', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'name_uk', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'name_en', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'name_fr', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'name_de', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>

    </div>

    <?php

    echo $form->imageFileRow($model, 'image');

    echo $form->textAreaRow($model, 'content', array('maxlength' => 255, 'class' => 'span8', 'style' => 'resize: vertical', 'rows' => 5,));
    echo $form->textAreaRow($model, 'content_uk', array('maxlength' => 255, 'class' => 'span8', 'style' => 'resize: vertical', 'rows' => 5,));
    echo $form->textAreaRow($model, 'content_en', array('maxlength' => 255, 'class' => 'span8', 'style' => 'resize: vertical', 'rows' => 5,));
    echo $form->textAreaRow($model, 'content_fr', array('maxlength' => 255, 'class' => 'span8', 'style' => 'resize: vertical', 'rows' => 5,));
    echo $form->textAreaRow($model, 'content_de', array('maxlength' => 255, 'class' => 'span8', 'style' => 'resize: vertical', 'rows' => 5,));

    echo $form->textFieldRow($model, 'product', array('maxlength' => 255, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'product_uk', array('maxlength' => 255, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'product_en', array('maxlength' => 255, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'product_fr', array('maxlength' => 255, 'class' => 'span8'));
    echo $form->textFieldRow($model, 'product_de', array('maxlength' => 255, 'class' => 'span8'));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Basic',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO information',
                'view' => '//seo/_form',
            ),
        )
    ));

    $this->endWidget();

    ?>

</div>
