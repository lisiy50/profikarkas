<?php
$this->breadcrumbs=array(
	'Отзывы поставщиков'=>array('index'),
	$model->title
);
?>
<div class="row">

    <div class="span8">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            <?php echo empty($model->title)?'#'.$model->id:$model->title; ?>
        </h4>
    </div>
<div class="pull-right">
    <?php echo CHtml::link('<i class="icon-trash icon-white"></i> Удалить', '#', array(
        'submit'=>array('delete','id'=>$model->id),
        'confirm'=>'Are you sure you want to delete "'.$model->title.'"?',
        'class'=>'btn btn-danger btn-small',
    ));?>

    <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-small"><i class="icon-plus"></i> Добавить</a>

    <a href="#" class="btn btn-small btn-success" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-ok icon-white"></i> Сохранить</a>

</div>

</div>
<hr>
<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
)); ?>