<div class="row">
    <?php foreach($homeKitRelationModels as $homeKitRelationModel):?>
        <?php $key = $homeKitRelationModel->home_kit_id.'_'.$homeKitRelationModel->packaging_home_kit_id;?>
        <?php $oldCategory; if($homeKitRelationModel->packagingHomeKit->category != $oldCategory):?>
            <h4 class="span12" style="color: #ff0000;"><?php echo Lookup::item('PackagingHomeKitCategory', $homeKitRelationModel->packagingHomeKit->category);?></h4>
        <?php $oldCategory = $homeKitRelationModel->packagingHomeKit->category; endif;?>
        <div class="well span5" style="height: 230px;">
            <h5><?php echo $homeKitRelationModel->packagingHomeKit->name;?></h5>
            <div class="row">
                <div class="span4">
                    <?php echo $form->textFieldRow($homeKitRelationModel, '['.$key.']value');?>
                </div>
                <div class="span4">
                    <?php echo $form->textFieldRow($homeKitRelationModel, '['.$key.']price');?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>