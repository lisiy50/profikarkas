<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'name', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'price', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'Комплектация',
                'view' => '_packaging',
                'visible' => !$model->isNewRecord,
                'data' => array(
                    'homeKitRelationModels' => $homeKitRelationModels,
                ),
            ),
            'tab3' => array(
                'title' => 'Цены',
                'view' => '_pricing',
                'visible' => !$model->isNewRecord,
                'data' => array(
                    'homeKitWorkScopeModels' => $homeKitWorkScopeModels,
                ),
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->