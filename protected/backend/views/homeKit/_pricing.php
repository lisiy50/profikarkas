<?php
/**
 * @var HomeKitWorkScope[] $homeKitWorkScopeModels
 */
?>
<div class="row">
    <?php foreach($homeKitWorkScopeModels as $homeKitWorkScopeModel):?>
        <?php $key = $homeKitWorkScopeModel->home_kit_id.'_'.$homeKitWorkScopeModel->work_scope_id;?>
        <div class="well span5" style="height: 120px;">
            <h5><?php echo $homeKitWorkScopeModel->workScope->name;?></h5>
            <div class="row">
                <div class="span4">
                    <?php echo $form->textFieldRow($homeKitWorkScopeModel, '['.$key.']price');?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>