<?php
$this->layout='column1';
$this->breadcrumbs=array(
    'SEO информация'
);
?>

<div class="row">
    <div class="span8">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            SEO информация
        </h4>
    </div>
    <div class="pull-right">
        <button class="btn btn-small btn-success" type="button" onclick="$('#seo-list-form').submit();"><i class="icon-plus icon-white"></i> Сохранить</button>
    </div>
</div>
<hr>

<?php $form = $this->beginWidget('BootActiveForm', array(
    'id' => 'seo-list-form',
)); ?>

<div class="tabbable tabs-left">
<?php
$tabs=array();
foreach($this->routes as $route=>$title) {
    $key=str_replace('/','-',$route);
    $tabs[$key]=array(
        'title' => $title,
        'view' => '_tab',
        'data' => array(
            'model'=>$this->getByRoute($route),
            'header'=>$title,
            'key'=>$key,
        ),
    );
}

$this->widget('BootTabView', array(
    'tabs' => $tabs,
));
?>
</div>

<?php $this->endWidget(); ?>
