<h4><?php echo $header; ?></h4>

<?php
echo CHtml::activeLabel($model, "[$key]metaTitle");
echo CHtml::activeTextField($model, "[$key]metaTitle", array(
    'maxlength' => 255,
    'class' => 'input-xxlarge'
));
echo CHtml::activeLabel($model, "[$key]metaDescription");
echo CHtml::activeTextField($model, "[$key]metaDescription", array(
    'maxlength' => 255,
    'class' => 'input-xxlarge'
));
echo CHtml::activeLabel($model, "[$key]metaKeywords");
echo CHtml::activeTextField($model, "[$key]metaKeywords", array(
    'maxlength' => 255,
    'class' => 'input-xxlarge'
));
?>