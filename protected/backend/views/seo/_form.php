<?php
echo $form->textFieldRow($model, 'metaTitle_uk', array(
    'maxlength' => 500,
    'class' => 'input-xxlarge'
));
echo $form->textAreaRow($model, 'metaDescription_uk', array(
    'class' => 'input-xxlarge',
    'rows' => 4
));
echo $form->textFieldRow($model, 'metaTitle', array(
    'maxlength' => 500,
    'class' => 'input-xxlarge'
));
echo $form->textAreaRow($model, 'metaDescription', array(
    'class' => 'input-xxlarge',
    'rows' => 4
));
echo $form->textFieldRow($model, 'metaTitle_en', array(
    'maxlength' => 500,
    'class' => 'input-xxlarge'
));
echo $form->textAreaRow($model, 'metaDescription_en', array(
    'class' => 'input-xxlarge',
    'rows' => 4
));
echo $form->textFieldRow($model, 'metaTitle_de', array(
    'maxlength' => 500,
    'class' => 'input-xxlarge'
));
echo $form->textAreaRow($model, 'metaDescription_de', array(
    'class' => 'input-xxlarge',
    'rows' => 4
));
echo $form->textFieldRow($model, 'metaTitle_fr', array(
    'maxlength' => 500,
    'class' => 'input-xxlarge'
));
echo $form->textAreaRow($model, 'metaDescription_fr', array(
    'class' => 'input-xxlarge',
    'rows' => 4
));
?>
