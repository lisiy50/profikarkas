<div class="form">

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->beginClip('basic'); ?>

    <div class="row">
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title_uk', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title_en', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title_fr', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'title_de', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>

        <div class="span4">
            <?php echo $form->widgetFieldRow($model, 'publish_date', 'zii.widgets.jui.CJuiDatePicker', array(
                'language' => 'ru',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'input-small'
                ),
            )); ?>
        </div>

        <div class="span8">
            <?php echo $form->textFieldRow($model, 'slogan', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'slogan_uk', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'slogan_en', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'slogan_fr', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>
        <div class="span8">
            <?php echo $form->textFieldRow($model, 'slogan_de', array('maxlength' => 255, 'class' => 'span8')); ?>
        </div>

    </div>

    <?php

    echo $form->imageFileRow($model, 'image');

//    echo '<div class="row">'."\n";
//    echo '<div class="span1">'."\n";
//    echo CHtml::image($model->Image2UploadBehavior->getImageUrl('thumb'), '', array('class'=>'img-polaroid', 'width'=>'45'))."\n";
//    echo '</div>'."\n";
//    echo '<div class="pull-left">'."\n";
//    if($model->image_main){
//        echo CHtml::link('&times;', array('deleteImage', 'id'=>$model->id, 'image_attr'=> 'image_main', ), array(
//            'class'=>'pull-right close',
//            'confirm'=>"Are you sure you want to remove the image of the article?",
//        ));
//    }
//    echo $form->inputRow(BootInput::TYPE_FILE, $model, 'image_main')."\n";
//    echo '</div>'."\n";
//    echo '</div>';

    echo $form->widgetFieldRow($model, 'annotation', 'Redactor');
    echo $form->widgetFieldRow($model, 'annotation_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'annotation_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'annotation_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'annotation_de', 'Redactor');

    echo $form->widgetFieldRow($model, 'content', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'content_de', 'Redactor');

    echo $form->labelEx($model, 'position');

    echo $form->dropDownList($model, 'position', $model->getPosition());

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Basic',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO information',
                'view' => '//seo/_form',
            ),
        )
    ));

    $this->endWidget();

    ?>

</div>
