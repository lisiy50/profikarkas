<?php
/* @var $this LandingtextController */

/* @var $model LandingText */
/* @var $form CActiveForm */
/* @var $count */
?>


<?php $formText = $this->beginWidget(
    'BootActiveForm',
    array(
        'id' => "landing-gallery-form",
        'enableAjaxValidation' => true,
    )
); ?>

<?= $formText->hiddenField($model, 'id', array('value' => $model->id)) ?>
<?= $formText->hiddenField($model, 'landing_id', array('value' => $model->landing_id)) ?>

<div class="row row-title">
    <div class="span12 span12-landing">
        <?php echo $formText->labelEx($model, 'name'); ?>
        <?php echo $formText->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
    </div>
</div>

<div class="row row-notice" style="margin-bottom: 10px;">
    <div class="span12 span12-landing">
        <?php echo $formText->labelEx($model, 'name_manager'); ?>
        <?php echo $formText->textField($model, 'name_manager', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
    </div>
</div>

<div class="row">
    <div class="span12 span12-landing" style="margin-bottom: 5px; font-size: 13px; font-weight: bold;">Размеры первого изображения</div>
    <div class="span12 span12-landing" style="margin-bottom: 10px;">
        <div class="row">
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_1_width_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_width_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_1_height_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_height_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_1_width_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_width_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_1_height_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_height_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_1_width_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_width_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_1_height_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_1_height_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="span12 span12-landing" style="margin-bottom: 5px; font-size: 13px; font-weight: bold;">Размеры второго изображения (для мобильных)</div>
    <div class="span12 span12-landing" style="margin-bottom: 10px;">
        <div class="row">
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_2_width_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_width_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_2_height_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_height_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_2_width_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_width_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_2_height_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_height_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
            <div class="span1 span1-landing with-delimiter">
                <?php echo $formText->labelEx($model, 'image_2_width_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_width_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                <span class="size-delimiter">x</span>
            </div>
            <div class="span1 span1-landing">
                <?php echo $formText->labelEx($model, 'image_2_height_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                <?php echo $formText->textField($model, 'image_2_height_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="span12 span12-landing">
        <?= CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => "btn-new btn-blue button_change_{$count}"]); ?>
    </div>
</div>

<?php $this->endWidget(); ?>



