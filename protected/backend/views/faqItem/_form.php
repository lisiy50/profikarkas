<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));

    $this->beginClip('basic');

    echo $form->widgetFieldRow($model, 'parent_id', 'McDropdown', array(
        'data' => $rooted->children,
        'without' => $model->id,
        'options' => array(
            'select' => "js:function(id){
                $('#tab2').html('Сначала нужно сохранить вопрос');
            }"
        ),
        'htmlOptions' => array(
            'class' => 'input span12'
        )
    ));

    echo $form->textFieldRow($model, 'name_ru', array(
        'class' => 'input span12'
    ));

    echo $form->textFieldRow($model, 'name_uk', array(
        'class' => 'input span12'
    ));

    echo $form->widgetFieldRow($model, 'content_ru', 'Redactor');

    echo $form->widgetFieldRow($model, 'content_uk', 'Redactor');

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array('model' => $model),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'Настроить порядок',
                'view' => '_position',
                'data' => array(
                    'faqItems' => $model->neighbors,
                ),
                'visible' => !$model->isNewRecord && $model->hasNeighbors,
            )
        ),
    ));
    $this->endWidget();

    ?>

</div>
