<?php

$this->breadcrumbs = array(
    'Часто задаваемые вопросы' => array('index'),
    'Изменить вопрос',
);

?>

<div class="row">
    <div class="span8">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            <?php echo empty($model->name_ru)?'#'.$model->id:CHtml::encode($model->name_ru); ?>
        </h4>
    </div>
    <div class="pull-right">
        <?php echo CHtml::link('<i class="icon-trash icon-white"></i> Удалить', '#', array(
        'submit' => array('delete', 'id'=>$model->id),
        'confirm' => 'Вы уверены, что хотите удалить вопрос?',
        'class' => 'btn btn-small btn-danger',
    ));?>

        <a href="<?php echo $this->createUrl('create', array('root_id' => $model->parent_id)); ?>" class="btn btn-small"><i class="icon-plus"></i> Добавить</a>

        <a href="#" class="btn btn-success btn-small" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-ok icon-white"></i> Сохранить</a>
    </div>
</div>
<hr>
<?php echo $this->renderPartial('_form', array(
    'model' => $model,
    'rooted' => $rooted
)); ?>

<script type="text/javascript">

    $('#sortable-faq-item tbody').sortable({
        axis:'y',
        containment:'parent',
        tolerance:'pointer',
        handle:'.icon-resize-vertical'
    });

    $('#save-faq-item').on('click', function () {
        $.post('faqItem/saveOrder', $('#sortable-faq-item tbody').sortable('serialize'), function () {
            displayMessage('Порядок сохранен', 'success');
        });
    });

</script>
