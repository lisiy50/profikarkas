<table id="sortable-faq-item" class="table table-striped">
    <colgroup>
        <col width="18"/>
        <col/>
        <col/>
    </colgroup>
    <?php foreach ($faqItems as $faqItem): ?>
    <tr id="FaqItem_id-<?php echo $faqItem->id; ?>" <?php if($model->id==$faqItem->id) echo 'class="current-row"'; ?>>
        <td><i class="icon-resize-vertical" style="cursor: row-resize;"></i></td>
        <td><?php echo $faqItem->name_ru; ?></td>
    </tr>
    <?php endforeach; ?>

    <?php if (count($faqItems)==0): ?>
       <tr>
           <td colspan="3">Невозможно настроить порядок</td>
       </tr>
    <?php endif; ?>
</table>

<div class="row">
    <div class="span3">
        <a class="btn btn-success" id="save-faq-item" href="#">Сохранить порядок</a>
    </div>
</div>
