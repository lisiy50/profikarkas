<div class="row">
    <div class="span6">
        <?php if ($model->hasChildren==false): ?>
        Часто задаваемых вопросов нет
        <?php endif;?>

        <?php if ($model->id ==  FaqItem::ARTICLE): ?>
            <?php foreach ($model->children as $child): ?>
            <strong>
                <a href="<?php echo $this->createUrl('update', array('id' => $child->id)); ?>"><?php echo $child->name_ru;?></a>
            </strong>
            <?php $this->widget('CTreeView', array(
                'data' => $child->children,
            )); ?>
            <?php endforeach; ?>
        <?php else: ?>
            <?php $this->widget('CTreeView', array(
                'data' => $model->children,
            )); ?>
        <?php endif; ?>
    </div>

    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('create', array('root_id' => $model->id)); ?>"><i class="icon-plus"></i> Добавить</a>
    </div>
</div>
