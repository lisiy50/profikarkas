<?php

$this->breadcrumbs = array('Часто задаваемые вопросы');

?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>"
                 title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Часто задаваемые вопросы
        </h4>
    </div>
</div>

<hr>

<?php

foreach ($faqItems as $faqItem) {
    echo $this->renderPartial('_view', array('model' => $faqItem));
}

?>

