<tr id="Image_id-<?php echo $model->id; ?>">
    <td style="width: 20px;"><i class="icon-resize-vertical" style="margin-top: 32px; cursor: row-resize;"></i></td>
    <td style="text-align: center;vertical-align: middle;"><?php echo CHtml::checkBox("Image[$model->id][additional_option]", $model->additional_option==ExtraImage::ADDITIONAL_OPTION_ENABLE?true:false, array('uncheckValue'=>ExtraImage::ADDITIONAL_OPTION_DISABLE))?></td>
    <td><img width="70" class="img-polaroid" src="<?php echo $model->getImageUrl('thumb'); ?>"></td>
    <td><?php echo CHtml::textArea("Image[$model->id][description]", $model->description, array('style'=>'width:661px;height:70px;'))?></td>
    <td style="width: 20px;"><i class="icon-trash" style="margin-top: 32px; cursor: pointer;"></i></td>
</tr>