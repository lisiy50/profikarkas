<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'contract', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'date_day', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'date_moth', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'date_year', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'client_name', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'passport_series', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'passport_number', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'passport_issued', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'passport_date', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'address', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'project', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'phone', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'price', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'price_text', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'signature', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'email', array('class' => 'input-xxlarge'));
    echo $form->textFieldRow($model, 'cover_up_date', array('class' => 'input-xxlarge'));

    $this->endWidget();
    ?>

</div><!-- form -->