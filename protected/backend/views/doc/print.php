<?
/**
 * @var $model Doc
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="ru">
    <style type="text/css">
        body {
            background-color: #6e6e6e;
            margin: 0;
        }
        .page {
            background-color: #ffffff;
            width: 794px;
            margin: 0 auto;
        }
        .text {
            width: 700px;
            margin: 0 auto;
        }
        table {
            border-collapse: collapse;
        }
        table td {
            width: 50%;
            padding: 4px;
            vertical-align: top;
            border: 1px solid #000000;
        }
        table p, .list p {
            margin: 0;
        }
    </style>
</head>
<body>

<div class="page">

    <div class="text">

    <img style="position: absolute;" src="<?php echo $this->assetsUrl.'/img/z500_doc_logo.png'?>"/>

        <p style="text-align: center;margin: 0;">ДОГОВОР <span><?php echo $model->contract?></span></p>

        <p style="text-align: center; center;margin: 0;">купли - продажи типового проекта</p>

        <p style="text-align: center; center;margin: 0;">жилого дома № <?php echo $model->project?></p>
        <div style="width: 100%;float: left;">
            <p style="float: left;">г. Москва</p>
            <p style="float: right;"><?php echo $model->date_day?> <?php echo $model->date_moth?> <?php echo $model->date_year?> г.</p>
        </div>
        <p>Физическое лицо: <?php echo $model->client_name?>;
            паспорт серия <?php echo $model->passport_series?> номер <?php echo $model->passport_number?><span>, выдан </span>тп №49 ОУФМС России по
            Санкт-Петербургу и Ленинградской области в Московском р-не <?php echo $model->passport_date?><span> года &nbsp;</span>&nbsp;(в дальнейшем - "Покупатель, с одной
            стороны, и ООО «Зет пятьсот»&nbsp; (в дальнейшем - "Продавец"),
            в лице директора Козлова Никиты Вячеславовича,
            &nbsp;с другой стороны, в дальнейшем вместе
            именуемые "Стороны", а каждый в отдельности - "Сторона",
            заключили настоящий Договор купли – продажи типового проекта жилого дома (в
            дальнейшем именуемый "Договор") о нижеследующем:</p>
        <p>1. ОБЩИЕ ПОЛОЖЕНИЯ</p>

        <p>1.1.
            На условиях настоящего договора Продавец обязуется передать в собственность
            Покупателю, а Покупатель обязуется принять и оплатить проект индивидуального
            жилого дома (в дальнейшем именуется "Проект"), разработанный архитектурным
            бюро Studio Z500.</p>

        <p>1.2. Покупатель приобретает Проект с целью
            его одноразового использования исключительно путем построения одного частного
            дома.</p>

        <p>1.3.
            Содержание проекта, его составных частей приводится в отдельном приложении к
            настоящему договору, которое является неотъемлемой частью данного Договора.</p>
        <p>2. ОБЯЗАННОСТИ СТОРОН</p>

        <p>2.1.
            Продавец обязан:</p>

        <p>2.1.1.
            Передать в собственность Покупателя проект в сроки и на условиях,
            предусмотренных данным договором.</p>

        <p>2.1.2.
            В случае выявления полиграфических или типографских недостатков Проекта, при
            условии соблюдения Покупателем п. 2.2.5 настоящего договора, отправить
            покупателю проект надлежащего качества в течение 10 (десяти) рабочих дней со
            дня получения претензии Продавцом.</p>

        <p>2.1.3
            В случае выявления ошибок Проекта, обоснованных&nbsp;
            соответствующими строительными нормами или законодательными актами РФ,
            отправить покупателю Проект с внесенными правками в течение 10 (десяти) рабочих
            дней со дня получения претензии Продавцом.</p>

        <p>2.2.
            Покупатель обязан:</p>

        <p>2.2.1.
            Оплатить стоимость Проекта в сроки, предусмотренные данным договором.</p>

        <p>2.2.2.
            Принять проект в порядке, предусмотренном настоящим договором.</p>

        <p>2.2.3.
            Использовать приобретенный Проект только один раз, с целью построения одного
            частного дома.</p>

        <p>2.2.4.
            Не передавать более чем одному третьему лицу, которое непосредственно будет
            осуществлять строительство частного дома, по данному проекту, его составные
            части и\или ксерокопии.</p>

        <p>2.2.5.
            Не дополнять, корректировать и/или изменять Проект.</p>
        <p>3. ЦЕНА ДОГОВОРА И ПОРЯДОК РАСЧЕТОВ</p>

        <p>3.1.
            Цена Проекта, являющегося предметом данного Договора, составляет <?php echo price('{price}', $model->price, array('price_accuracy'=>0))?>
            (<?php echo $model->price_text?>)
            рублей.</p>

        <p>3.2.
            Расчеты производятся в национальной валюте Российской Федерации.</p>

        <p>3.3.
            Оплата за Проект осуществляется в полном размере на расчётный счёт Продавца.</p>
        <p>4. ПОРЯДОК ЗАКАЗА И ПЕРЕДАЧИ ПРОЕКТОВ</p>

        <p>4.1.Покупатель
            информирует Продавца (путем подачи Заявки, электронным письмом, либо в устной
            форме) о своем намерении приобрести Проект определенного частного дома, выбрав
            его среди предложенных проектов зданий, содержащихся на сайте <u><a href="http://www.studio-z500.ru/"><span>www.</span></a>z500proekty.ru</u> , подробно указав
            сам Проект&nbsp; и &nbsp;желаемые изменения в проекте .</p>

        <p>4.2.После
            достижения договоренности относительно всех существенных условий, относительно
            предмета данного договора, Стороны подписывают соответствующее приложение к Договору.</p>

        <p>4.3.Передача
            Проекта оформляется сторонами Актом приема-передачи, который подписывается
            уполномоченными представителями Сторон.</p>

        <p>4.4.Риск
            случайного уничтожения Проекта или его повреждения переходит к покупателю с
            момента подписания Акта приема-передачи.</p>

        <p>4.5.Если
            возникли претензии по комплектности и качеству Проекта во время их приема,
            Стороны заключают соответствующий Акт.</p>
        <p>5. ОТВЕТСТВЕННОСТЬ СТОРОН</p>

        <p>5.1.В
            случае нарушения требований настоящего Договора виновная Сторона несет
            ответственность в соответствии с настоящим договором и действующим
            законодательством Российской
            Федерации, по месту нахождения стороны, подающей иск.</p>

        <p>5.1.1.
            Нарушением Договора является его невыполнение или ненадлежащее выполнение, то
            есть исполнение с нарушением условий, определенных содержанием настоящего
            Договора.</p>

        <p>5.1.2.
            Сторона не несет ответственности за нарушение Договора, если оно произошло не
            по ее вине (умыслу или неосторожности).</p>

        <p>5.1.3.
            Сторона считается невиновной и не несет ответственности за нарушение Договора,
            если она докажет, что приняла все зависящие от нее меры по надлежащему
            исполнению этого Договора.</p>

        <p>5.2.Сторона,
            которая нарушила этот Договор, обязана возместить убытки, причиненные таким
            нарушением, независимо от принятия другой стороной каких-либо мер по
            предотвращению убытков или их уменьшению, кроме случаев, когда последняя своим
            виновными (умышленными или неосторожными) деяниями (действием или бездействием)
            способствовала наступлению или увеличению убытков.</p>

        <p>5.3.За
            копирование и распространение Проекта с нарушением требований настоящего
            договора Покупатель несет ответственность в виде штрафа, который равен 100
            000,00 (сто тысяч) рублей за каждый скопированный, проданный, переданный или
            иным образом распространенный экземпляр Проекта.</p>

        <p>5.4.
            За просрочки осуществления расчета за Проект Покупатель обязан по требованию
            Продавца выплатить последнему пеню в размере 1% от неуплаченной суммы за каждый
            день просрочки платежа (но не более двойной учетной ставки ЦБРФ, которая
            действовала в период, за который уплачивается пеня).</p>

        <p>5.5.
            Оплата Стороной определенных настоящим договором и (или) действующим
            законодательством Российской Федерации штрафных санкций (неустойки, штрафа,
            пени) не освобождает ее от обязанности возместить по требованию другой стороны
            убытки, причиненные нарушением Договора (реальные убытки и (или) упущенную
            выгоду) в полном объеме, а возмещение убытков не освобождает ее от обязанности
            уплатить по требованию другой стороны штрафные санкции в полном объеме.</p>

        <p>5.6.
            Оплата Стороной и (или) возмещение убытков, причиненных нарушением Договора, не
            освобождает ее от обязанности выполнить этот Договор в натуре, если иное прямо
            не предусмотрено действующим законодательством Российской Федерации.</p>
        <p>6. Урегулирование возможных споров</p>

        <p>6.1.Все
            споры и разногласия, которые возникают или могут возникнуть во время действия
            настоящего Договора, решаются путем переговоров между Сторонами.</p>

        <p>6.2.Если
            Стороны не могут прийти к согласию путем переговоров, данное расхождение
            передается на рассмотрение Арбитрижного Суда в соответствии с действующим
            законодательством Российской Федерации.</p>
        <p>7. Форс-мажор</p>

        <p>7.1.
            Ни одна из сторон не несет ответственности за полное или частичное
            неисполнение, задержку в выполнении или ненадлежащее исполнение своих
            обязательств по настоящему договору, если такое неисполнение или задержка
            выполнения непосредственно или косвенно вызваны обстоятельствами форс-мажора.
            Сторона, которая ссылается на положения этого пункта, должна не позднее, чем
            через 3 (три) дня, предоставить письменное уведомление другой стороне о природе
            наступления и ожидаемой продолжительности обстоятельств форс-мажора, и приложить
            все усилия для ограничения влияния этих обстоятельств, с целью более
            эффективного выполнения своих обязательств &nbsp;по этому договору.</p>

        <p>7.2.
            Для выполнения условий данного Договора обстоятельствами форс-мажора считаются
            любые обстоятельства, которые влияют на выполнение данного Договора, что является
            следствием или вызваны действиями, событиями, удержанием от действий или
            случаями, находящимися вне контроля Сторон, и, не ограничивая общности
            вышеупомянутого, включают стихийные бедствия, восстания, военные действия,
            забастовки, пожары, &nbsp;наводнения, другие
            стихийные бедствия или сезонные природные явления, эпидемии и иные случаи и
            обстоятельства, признанные таковыми в хозяйственной практике (форс-мажорные
            обстоятельства).</p>

        <p>7.3.
            В этом случае выполнение условий данного Договора может быть отложено на срок
            действия этих обстоятельств.</p>

        <p>7.4.
            Свидетельство, выданное Торгово-Промышленной Палатой Российской Федерации или
            другим компетентным органом, будет достаточным доказательством существования
            форс-мажорных обстоятельств и их продолжительности.</p>

        <p>7.5.Если
            форс-мажорные обстоятельства продолжаются в течение 28 (двадцати восьми)
            календарных дней или очевидно, что они будут продолжаться в течение такого
            периода или более длительного промежутка времени, данный Договор может быть
            прекращен по письменному согласию сторон, путем предоставления письменного
            уведомления за 10 (десять) дней до его прекращения.</p>
        <p>8. Действие договора</p>

        <p>8.1. Настоящий Договор считается заключенным и вступает в силу с
            момента его подписания Сторонами и скрепления печатями Сторон.<br>
            8.2. Срок этого Договора начинает свой ход в момент,
            определенный в п. 8.1 настоящего Договора, и заканчивается 30.12.2014.&nbsp; .&nbsp;
            <br>
            8.3. Окончание срока этого Договора не освобождает Стороны
            от обязанности выполнить взятые на себя обязательства и от ответственности за
            его нарушение, которое имело место во время действия этого Договора.&nbsp; .<br>
            8.4. Если иное прямо не предусмотрено настоящим Договором
            или действующим законодательством Российской Федерации, изменения в настоящий
            Договор могут быть внесены только по договоренности Сторон, которая оформляется
            дополнительным соглашением к настоящему Договору.&nbsp; .<br>
            8.5. Изменения в настоящий Договор вступают с момента
            надлежащего оформления Сторонами соответствующего дополнительного соглашения к
            настоящему Договору, если иное не установлено в самом дополнительном
            соглашении, настоящем Договоре или в действующем законодательстве Российской
            Федерации.<br>
            8.6. Если иное прямо не предусмотрено настоящим Договором
            или действующим законодательством Российской Федерации, настоящий Договор может
            быть разорван только по договоренности Сторон, которая оформляется
            дополнительным соглашением к настоящему Договору.&nbsp; .<br>
            8.7. Настоящий Договор считается расторгнутым с момента
            надлежащего оформления Сторонами соответствующего дополнительного соглашения к
            настоящему Договору, если иное не установлено в самом дополнительном соглашении,
            настоящем Договоре или в действующем законодательстве Российской Федерации.</p>

        <p><br>
            9. Заключительные
            положения</p>

        <p>9.1. Все правоотношения, возникающие из настоящего Договора или
            связанные с ним, в том числе связанные с действительностью, заключением,
            исполнением, изменением и прекращением настоящего Договора, толкованием его
            условий, определением последствий недействительности или нарушения Договора,
            регламентируются настоящим Договором и соответствующими нормами действующего
            законодательства Российской Федерации, а также обычаями делового оборота,
            применимыми к таким правоотношениям на основании принципов добросовестности,
            разумности и справедливости.<br>
            9.2. После подписания этого Договора все предварительные
            переговоры по нему, переписка, предварительные договоры, протоколы о намерениях
            и любые другие устные или письменные договоренности Сторон по вопросам, которые
            так или иначе касаются этого Договора, теряют юридическую силу, но могут
            учитываться при толковании условий этого Договора.&nbsp; .<br>
            9.3. Стороны несут полную ответственность за правильность
            указанных ею в этом Договоре реквизитов и обязуется своевременно в письменной
            форме уведомлять другую Сторону об их изменении, а в случае несообщения - несет
            риск наступления связанных с ним неблагоприятных последствий.&nbsp; .<br>
            9.4. Передача прав и обязанностей по Договору одной из
            Сторон к третьим лицам допускается исключительно при условии письменного
            согласования этого с другой Стороной.<br>
            9.5. Дополнительные соглашения и приложения к настоящему
            Договору являются его неотъемлемой частью и имеют юридическую силу в случае,
            если они совершены в письменной форме, подписаны Сторонами и скреплены их
            печатями.<br>
            6.6. Все исправления за текстом настоящего Договора имеют
            юридическую силу и могут учитываться только при условии, что они в каждом
            отдельном случае датированы, удостоверены подписями Сторон и скреплены их
            печатями.<br>
            9.7. Настоящий Договор составлен при полном понимании
            Сторонами его условий и терминологии русским языком в двух экземплярах, имеющих
            одинаковую юридическую силу, по одному для каждой из Сторон.</p>

        <p>10. Местонахождение и реквизиты сторон</p>

        <table>
            <tbody><tr>
                <td>
                    <p style="text-align: center;">ПРОДАВЕЦ</p>
                    <p>ООО «Зет пятьсот»</p>
                    <h4>Юридически адрес: 107045, г. Москва, Луков пер, д.4, офис 8 </h4>
                    <p>Фактический адрес: 107045, г. Москва, Луков пер, д.4, офис
                        8&nbsp;</p>
                    <p>ОГРН (рег.номер) –1127746029564</p>
                    <p>ИНН/КПП: 7708754551/770801001</p>
                    <p>Р/С: 40702810901300023035</p>
                    <p>Банк: "БАНК24.РУ"(ОАО)г.ЕКАТЕРИНБУРГ</p>
                    <p>БИК: 046577859</p>
                    <p>К/С: 30101810600000000859</p>

                    <br/>

                    <p>Козлов Н.В.
                        ____________________</p>
                    <p>&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; (м.п.)</p>

                </td>
                <td>
                    <p style="text-align: center;">ПОКУПАТЕЛЬ</p>
                    <p><?php echo $model->client_name?></p>

                    <p>Адрес: <span style="text-decoration: underline"><?php echo $model->address?></span></p>
                    <p>паспорт серия <?php echo $model->passport_series?> номер <?php echo $model->passport_number?><span>,</span></p>
                    <p><span>выдан </span><?php echo $model->passport_issued?> <?php echo $model->passport_date?><span> года</span>&nbsp; </p>
                    <p>телефон: <?php echo $model->phone?></p>
                    <p>e-mail: <?php echo $model->phone?></p>


                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>


                    <p><?php echo $model->signature?>
                        _____________________&nbsp; &nbsp;</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;(подпись) </p>
                </td>
            </tr>
            </tbody></table>

    <br/>
        <p style="text-align: center;">Приложение № 1</p>

        <p style="text-align: center;">к договору купли - продажи</p>

        <p style="text-align: center;">проекта жилого дома № <?php echo $model->project?>&nbsp; от «<?php echo $model->date_day?>» <?php echo $model->date_moth?> <?php echo $model->date_year?> г.</p>

        <p>&nbsp;
        </p>

        <p>Физическое лицо: <?php echo $model->client_name?>;
            паспорт серия <?php echo $model->passport_series?> номер <?php echo $model->passport_number?><span>,</span></p>

        <p><span>выдан </span><?php echo $model->passport_issued?> <?php echo $model->passport_date?><span> года </span>&nbsp;(в дальнейшем - "Покупатель, с одной
            стороны, и ООО «Зет пятьсот»&nbsp; (в дальнейшем - "Продавец"),
            в лице директора Козлова Никиты
            Вячеславовича, с другой стороны, которые в дальнейшем вместе именуемые
            "Стороны", а каждый в отдельности - "Сторона", заключили
            настоящий Договор купли-продажи проекта жилого дома (в дальнейшем именуемый
            "Договор") о нижеследующем:</p>
        <p>Согласно
            условиям Договора Продавец передает Покупателю русскоязычный Проект "<?php echo $model->project;?>",
            состоящий из следующих разделов и передающийся в двух экземплярах:</p>
        <p>Описательная часть:</p>

        <p>Принципы
            использования готового проекта</p>

        <ol>
            <li>Общая часть</li>
            <li>Архитектурно - планировочное решения</li>
            <li>Конструктивные решения</li>
            <li>Инженерное обеспечение</li>
            <li>Противопожарные мероприятия</li>
            <li>Энергосбережение</li>
            <li>Охрана труда</li>
            <li>Охрана окружающей среды</li>
        </ol>

        <p>Часть, состоящая из чертежей</p>

        <div class="list">
            <p>1.Архитектурный
                раздел</p>

            <p>1.1.
                Планы этажей с экспликацией помещений</p>

            <p>1.2.
                План крыши</p>

            <p>1.3.
                Разрезы и состав ограждающих конструкций</p>

            <p>1.4.
                Фасады с таблицей отделки </p>

            <p>1.5.
                Спецификация элементов заполнения проемов</p>

            <p>1.6.
                Спецификация и сечения перемычек</p>

            <p>1.7.
                План и экспликация полов</p>

            <p>1.8.
                Архитектурные узлы в конструкциях</p>

            <p>2.Конструктивный
                раздел</p>

            <p>2.1.
                План монолитных фундаментов</p>

            <p>2.2.
                Узлы армирования и сечения</p>

            <p>2.3.
                Чертежи отдельных фундаментов</p>

            <p>2.4.
                План сборных фундаментов</p>

            <p>2.5.
                Схема расположения элементов каркаса</p>

            <p>2.6.
                Схема расположения элементов перекрытия (покрытия)</p>

            <p>2.7.
                Узлы армирования или опоры перекрытия на стены</p>

            <p>2.8.
                Схема расположения элементов кровли</p>

            <p>2.9.
                Узлы крепления, сечения, а также чертежи отдельных элементов</p>

            <p>2.10.
                Ведомость расходов стали и бетона</p>

            <p>3.Електротехнические
                решения</p>

            <p>3.1.Общие
                данные с условными обозначениями</p>

            <p>&nbsp;3.2. Принципиальная схема ВРУ</p>

            <p>&nbsp;3.3.План сетей освещения</p>

            <p>&nbsp;3.4.План силовых сетей</p>

            <p>&nbsp;3.5. Схема системы уравнения потенциалов</p>

            <p>&nbsp;3.6.Схема повторного контура заземления</p>

            <p>&nbsp;3.7.Спецификация материалов</p>

            <p>4.
                Водоснабжение и канализация</p>

            <p>4.1.Общие
                данные с условными обозначениями</p>

            <p>4.2.Система
                водоснабжения и канализации</p>

            <p>4.3.Узлы</p>

            <p>4.4.Спецификация
                материалов</p>

            <p>5.Отопление,
                вентиляция и кондиционирование</p>

            <p>5.1.Общие
                данные с условными обозначениями</p>

            <p>5.2.Система
                отопления</p>

            <p>5.3.Система
                вентиляции</p>

            <p>5.4.Спецификация
                материалов</p>
        </div>


        <p><br>
        </p><p>Продавец
            передает Проект в срок до 30 (тридцати) рабочих дней после 100% оплаты
            счета-фактуры.</p>

        <p>Стоимость
            Проекта составляет: Общая стоимость Проекта составляет <?php echo price('{price}', $model->price, array('price_accuracy'=>0))?>
            (<?php echo $model->price_text?>)
            рублей.</p>

        <p>Покупатель
            оплачивает указанную сумму в срок до <?php echo $model->cover_up_date?> г. путем перечисления на реквизиты
            указанные в счете-фактуре.</p>

        <p>Данное
            приложение является неотъемлемой частью Договора No.241/18022014.</p>
        <table>
            <tbody><tr>
                <td>
                    <p style="text-align: center;">ПРОДАВЕЦ</p>
                    <p>ООО «Зет пятьсот»</p>
                    <h4>Юридически адрес: 107045, г. Москва, Луков пер, д.4, офис 8 </h4>
                    <p>Фактический адрес: 107045, г. Москва, Луков пер, д.4, офис
                        8&nbsp;</p>
                    <p>ОГРН (рег.номер) –1127746029564</p>
                    <p>ИНН/КПП: 7708754551/770801001</p>
                    <p>Р/С: 40702810901300023035</p>
                    <p>Банк: "БАНК24.РУ"(ОАО) Г.ЕКАТЕРИНБУРГ</p>
                    <p>БИК: 046577859</p>
                    <p>К/С: 30101810600000000859</p>

                    <br/>
                    <br/>


                    <p>Козлов
                        Н.В.________________________</p>
                    <p>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;(м.п.) </p>
                </td>
                <td>
                    <p style="text-align: center;">ПОКУПАТЕЛЬ</p>

                    <p><?php echo $model->client_name?></p>

                    <p>Адрес: <?php echo $model->address?></p>
                    <p>паспорт серия <?php echo $model->passport_series?> номер <?php echo $model->passport_number?><span>,</span></p>
                    <p><span>выдан </span><?php echo $model->passport_issued?> <?php echo $model->passport_date?><span> года</span>&nbsp; </p>
                    <p>телефон: <?php echo $model->phone?></p>
                    <p>e-mail: <?php echo $model->email?></p>


                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>


                    <p><?php echo $model->signature?>&nbsp; __________________ </p>
                    <p>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                        (подпись)</p>
                </td>
            </tr>
            </tbody></table>
        <br></p>

    </div>

</div>

</body>
</html>

<script type="text/javascript">
    window.print();
</script>