<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'post_text', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->dropDownListRow($model, 'post', $model->getPosts(), array(
        'class' => 'input-xxlarge'
    ));

    echo $form->dropDownListRow($model, 'image_size', $model->getImageSizes(), array(
        'class' => 'input-xxlarge'
    ));

    echo $form->imageFileRow($model, 'image');

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endWidget();
    ?>

</div><!-- form -->