<table id="sortable-workScope" class="table table-striped">
    <colgroup>
        <col width="18"/>
        <col/>
    </colgroup>
<?php foreach(WorkScope::model()->findAll() as $workScope): ?>
    <tr id="WorkScope_id-<?php echo $workScope->id; ?>" <?php if($model->id==$workScope->id) echo 'class="current-row"'; ?>>
        <td width="30"><i class="icon-resize-vertical" style="cursor: row-resize;"></i></td>
        <td width="670"><?php echo $workScope->name; ?></td>
    </tr>
<?php endforeach; ?>
</table>

<a class="btn btn-success btn-small" id="save-workScope" href="#">Сохранить порядок</a>