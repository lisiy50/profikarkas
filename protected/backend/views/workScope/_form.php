<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'name', array('class' => 'span12'));

    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'position' => array(
                'title' => 'Настроить порядок',
                'view' => '_position',
                'data' => array(
                ),
                'visible' => !$model->isNewRecord,
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->