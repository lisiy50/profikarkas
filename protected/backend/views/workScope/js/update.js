jQuery(function(){

    $('#sortable-workScope tbody').sortable({
        axis: 'y',
        containment:'parent',
        tolerance:'pointer',
        handle:'.icon-resize-vertical'
    });
    $('#save-workScope').click(function(){
        $.post('workScope/saveOrder', $('#sortable-workScope tbody').sortable('serialize'), function(){
            displayMessage('Порядок сохранен', 'success');
        });
        return false;
    });

});