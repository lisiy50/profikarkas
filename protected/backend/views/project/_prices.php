<?php $homeKits = HomeKit::model()->findAll();?>
<table class="table table-striped">
    <colgroup>
        <col/>
        <col/>
        <col/>
        <col/>
        <col/>
    </colgroup>
    <thead>
    <tr>
        <td>Объем работ</td>
        <?php foreach($homeKits as $homeKit):?>
        <td><?php echo $homeKit->name;?></td>
        <?php endforeach;?>
    </tr>
    </thead>
    <?php foreach(WorkScope::model()->findAll() as $workScope): ?>
        <tr>
            <td><?php echo $workScope->name;?></td>
            <?php foreach($homeKits as $homeKit):?>
                <?php $workScopePrice = WorkScopePrice::model()->findByPk(array('project_id'=>$model->id,'work_scope_id'=>$workScope->id,'home_kit_id'=>$homeKit->id));?>
                <td>
                    <?php echo CHtml::textField("Project[workScopePrice][$model->id-$workScope->id-$homeKit->id][price]", $workScopePrice->price, array('class'=>'input-small'));?>
                    <?php echo CHtml::hiddenField("Project[workScopePrice][$model->id-$workScope->id-$homeKit->id][work_scope_id]", $workScope->id);?>
                    <?php echo CHtml::hiddenField("Project[workScopePrice][$model->id-$workScope->id-$homeKit->id][home_kit_id]", $homeKit->id);?>
                </td>
            <?php endforeach;?>
        </tr>
    <?php endforeach; ?>
</table>