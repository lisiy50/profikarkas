<?php
/**
 * @var ProjectImage $model
 */
?>

<tr id="Image_id-<?php echo $model->id; ?>">
    <td style="width: 20px;"><i class="icon-resize-vertical" style="margin-top: 32px; cursor: row-resize;"></i></td>

    <td>
        <img width="70" class="img-polaroid" src="<?php echo $model->getImageUrl('thumb'); ?>">
    </td>

    <td><?php echo CHtml::dropDownList("Image[$model->id][type]", $model->type, $model->getTypeList(), array('prompt'=>'')); ?></td>

    <td style="width: 20px;"><i class="icon-trash" style="margin-top: 32px; cursor: pointer;"></i></td>
</tr>
