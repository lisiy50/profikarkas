jQuery(function () {

    $('#sortable-images tbody').sortable({
        axis:'y',
        containment:'parent',
        tolerance:'pointer',
        handle:'.icon-resize-vertical'
    });

    $('#save-images').on('click', function () {
        var data = $('#sortable-images tbody').sortable('serialize') + '&' + $('#sortable-images textarea, #sortable-images input, #sortable-images select').serialize();
        var url = $.createUrl('projectImage/saveOrder?ajax=1');
        $.post(url, data, function () {
            displayMessage('Изображения сохранены', 'success');
        });
        return false;
    });
    $(document).on('click', '#sortable-images .icon-trash', function () {
        if (confirm('Вы уверены, что хотите удалить изображение?') == false) return;
        var image = $(this).parent().parent();
        var id = image.attr('id').match(/Image_id-([0-9]+)/)[1];
        var url = $.createUrl('projectImage/delete?ajax=1&id=' + id);
        $.post(url, function () {
            displayMessage('Изображение удалено', 'success');
            image.remove();
            if ($('#sortable-images tbody tr').length == 1) {
                $('#empty-images').show();
            }
        });
        return false;
    });

})