<?php
/**
 * @var Project $model
 */

?>

<div class="row">

    <div class="span12">
        <?php echo $form->textFieldRow($model, 'z500_url_key', array('class' => 'span12',));?>
    </div>
</div>

<?php if($model->z500_url_key && $z500Images = $model->getZ500Images()): ?>

<table class="table table-bordered">
    <thead>
    <tr>
        <td>type</td>
        <td>img</td>
        <td>Текущее изображение</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($z500Images as $image): ?>
        <tr>
            <td>
                <?php echo $image->type?>
            </td>
            <td>
                <img src="<?php echo $image->getUrl()?>" width="200" alt="">
            </td>
            <td>
                <?php
                $pi = ProjectImage::model()->find("type={$image->type} AND filename LIKE '{$image->getFilename()}' AND project_id={$model->id}");
                if ($pi) {
                    echo CHtml::image($pi->getImageUrl('origin'), '', array('width' => 200));
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php echo CHtml::link('<i class="icon-download"></i> Импортировать все', '#', array(
        'submit'=>array('importImages','projectId'=>$model->id),
        'confirm'=>'Вы уверены, что хотите импортировать все изображения?',
        'class'=>'btn btn-default',
    ));?>


<?php else: ?>
<div class="alert alert-danger">
    Необходимо указать код проекта, к примеру http://z500ua/projekt/<span class="label label-important">zx63_b</span>.html <strong>и нажать кнопку "Сохранить" вверху страницы</strong>.
</div>
<?php endif; ?>
