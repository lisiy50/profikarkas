<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
//        'htmlOptions'=>array(
//            'enctype'=>'multipart/form-data',
//        ),
    ));

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    ?>

    <div class="row">
        <div class="span-12">
            папка для проекта (не должна содержать пробелы):
            <strong><?php echo Yii::app()->translitFormatter->formatFileName($model->symbol.str_replace('+', 'plus', $model->variation_symbol)) ?></strong>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="span2">
            <?php echo $form->label($model, 'z500_id_primary');?>
            <span class="label label-important"><?php echo $model->z500_id_primary;?></span>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model, 'symbol', array('class' => 'span2',));?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model, 'variation_symbol', array('class' => 'span2',));?>
        </div>
        <div class="span2">
            <br>
            <?php echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
        </div>
        <div class="span3">
            <br>
            <?php echo $form->checkBoxRow($model, 'show_on_landing', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
        </div>
        <div class="span12">
            <?php echo $form->textFieldRow($model, 'title_uk', array('class' => 'span12',));?>
        </div>
        <div class="span12">
            <?php echo $form->textFieldRow($model, 'title', array('class' => 'span12',));?>
        </div>
        <div class="span12">
            <?php echo $form->textFieldRow($model, 'title_en', array('class' => 'span12',));?>
        </div>
        <div class="span12">
            <?php echo $form->textFieldRow($model, 'title_de', array('class' => 'span12',));?>
        </div>
        <div class="span12">
            <?php echo $form->textFieldRow($model, 'title_fr', array('class' => 'span12',));?>
        </div>
    </div>


    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');?>
    <?php echo $form->widgetFieldRow($model, 'description', 'Redactor');?>
    <?php echo $form->widgetFieldRow($model, 'description_en', 'Redactor');?>
    <?php echo $form->widgetFieldRow($model, 'description_de', 'Redactor');?>
    <?php echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');?>

    <?php echo $form->textFieldRow($model, 'house_height', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'living_room_height', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'cubage', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'minimum_building_area', array('class'=>''));?>

    <?php echo $form->textFieldRow($model, 'floors', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'rooms', array('class'=>''));?>
    <?php echo $form->checkBoxRow($model, 'attic_floor', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
    <?php echo $form->checkBoxRow($model, 'first_floor_room', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>

    <?php echo $form->checkBoxRow($model, 'roof_2_slope', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
    <?php echo $form->checkBoxRow($model, 'roof_4_slope', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
    <?php echo $form->checkBoxRow($model, 'roof_many_slope', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>
    <?php echo $form->checkBoxRow($model, 'roof_flat', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));?>

    <?php echo $form->textFieldRow($model, 'roof_angle', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'roof_angle1', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'roof_area', array('class'=>''));?>

    <?php echo $form->textFieldRow($model, 'roof_canopy1', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'roof_canopy2', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'roof_canopy3', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'roof_canopy4', array('class'=>''));?>

    <?php echo $form->textFieldRow($model, 'net_area', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'usable_area', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'usable_area_by_order', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'external_dimensions_area', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'basement_area', array('class'=>''));?>

    <div class="row">
        <div class="span2">
            <?php echo $form->textFieldRow($model, 'garage_area', array('class'=>'span2'));?>
        </div>
        <div class="span2">
            <?php echo $form->dropDownListRow($model, 'cars_in_garage', $model->getCarsInGarage(), array('class'=>'span2'));?>
        </div>
    </div>

    <?php echo $form->textFieldRow($model, 'loft_area', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'plot_width', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'plot_length', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'facade_width', array('class'=>''));?>
    <?php echo $form->textFieldRow($model, 'facade_height', array('class'=>''));?>

    <?php echo $form->textFieldRow($model, 'video_ids', array('class'=>'input-xxlarge'));?>
    <?php echo $form->textFieldRow($model, 'video1_ids', array('class'=>'input-xxlarge'));?>
    <?php echo $form->textFieldRow($model, 'price', array('class'=>'input-xxlarge'));?>


    <div class="control-group">

            <label><?php echo $model->getAttributeLabel('priority'); ?>: <span id="priority-value"><?php echo $model->priority; ?></span></label>

            <div class="">
                <?php
                echo $form->hiddenField($model, 'priority');
                $this->widget('zii.widgets.jui.CJuiSlider', array(
                    'value' => $model->priority,
                    'options' => array(
                        'min' => 0,
                        'max' => 99,
                        'slide' => "js:function(event, ui){ $('#Project_priority').val(ui.value); $('#priority-value').text(ui.value); }",
                    ),
                    'htmlOptions' => array(
                        'style' => 'width:300px;',
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'priority'); ?>
            </div>
            </div>



    <?php

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'prices' => array(
                'title' => 'Цены',
                'view' => '_prices',
                'data' => array(),
                'visible' => !$model->isNewRecord,
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            ),
            'z500_images' => array(
                'title' => 'Импорт с Z500',
                'view' => '_z500_images',
                'visible' => !$model->isNewRecord
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
