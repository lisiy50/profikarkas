<?php
$form = $this->beginWidget('BootActiveForm', array(
    'id' => get_class($model) . '-form',
    'enableAjaxValidation' => false,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
));
?>

<div class="row">
    <div class="span4">
        <?php echo $form->fileFieldRow($model, 'csvFile');?>
    </div>
</div>
<?php echo CHtml::submitButton('Загрузить');?>

<?php $this->endWidget();?>