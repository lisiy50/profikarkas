<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'title', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->imageFileRow($model, 'image');
    echo CHtml::link('Удалить изображение', array('deleteImage', 'id'=>$model->id));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));


    ?>

    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php

    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    $this->endWidget();
    ?>

</div><!-- form -->