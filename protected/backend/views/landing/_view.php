<?php
/* @var $this LandingController */
/* @var $data Landing */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->title_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title_ua')); ?>:</b>
	<?php echo CHtml::encode($data->title_ua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_ru')); ?>:</b>
	<?php echo CHtml::encode($data->text_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_ua')); ?>:</b>
	<?php echo CHtml::encode($data->text_ua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ru); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title_ua')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title_ua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description_ua')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description_ua); ?>
	<br />

	*/ ?>

</div>