<?php
/* @var $this LandingController */
/* @var $model Landing */
/* @var $model_text LandingText */
/* @var $model_gallery LandingGallery */

$this->breadcrumbs=array(
	'Лендинги'=>array('admin'),
	'Добавить',
);

$this->menu=array(
	array('label'=>'List Landing', 'url'=>array('index')),
	array('label'=>'Manage Landing', 'url'=>array('admin')),
);
?>

<h1 style="margin-bottom: 20px;">Добавить лендинг</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'model_text'=>$model_text,'model_gallery'=>$model_gallery)); ?>
