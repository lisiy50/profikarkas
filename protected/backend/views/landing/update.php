<?php
/* @var $this LandingController */
/* @var $model Landing */
/* @var $model_text LandingText */
/* @var $model_gallery LandingGallery */

$this->breadcrumbs=array(
	'Лендинги'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Landing', 'url'=>array('index')),
	array('label'=>'Create Landing', 'url'=>array('create')),
	array('label'=>'View Landing', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Landing', 'url'=>array('admin')),
);
?>

<h1 style="margin-bottom: 20px;">Изменить лендинг</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'model_text'=>$model_text,'model_gallery'=>$model_gallery)); ?>
