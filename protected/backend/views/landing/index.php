<?php
/* @var $this LandingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Лендинги',
);

$this->menu=array(
	array('label'=>'Create Landing', 'url'=>array('create')),
	array('label'=>'Manage Landing', 'url'=>array('admin')),
);
?>

<div class="row">
    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>">
            <i class="icon-plus"></i> Добавить
        </a>
    </div>
</div>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
