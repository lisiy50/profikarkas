<?php
/* @var $this LandingController */
/* @var $model Landing */

$this->breadcrumbs=array(
	'Лендинги',
);

$this->menu=array(
	array('label'=>'List Landing', 'url'=>array('index')),
	array('label'=>'Create Landing', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#landing-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Лендинги</h1>

<p style="margin-top: 1.5rem; margin-bottom: 0;">
    <a href="<?= $this->createUrl('create') ?>" class="btn-new btn-green">Добавить лендинг</a>
</p>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'landing-grid',
	'dataProvider'=>$model->search(),
//	'filter'=>$model,
	'columns'=>array(
        'id',
		'name',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{update}',
		),
	),
)); ?>
