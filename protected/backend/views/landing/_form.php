<?php
/* @var $this LandingController */
/* @var $model Landing */
/* @var $model_text LandingText */
/* @var $model_gallery LandingGallery */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php $this->beginClip('basic'); ?>
    <?php $form = $this->beginWidget(
        'BootActiveForm',
        array(
            'id' => 'landing-form',
            'enableAjaxValidation' => false,
        )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField(
                $model,
                'name',
                array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'title_ru'); ?>
            <?php echo $form->textField(
                $model,
                'title_ru',
                array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'title_ru'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'title_ua'); ?>
            <?php echo $form->textField(
                $model,
                'title_ua',
                array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'title_ua'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="span12">
            <?php echo $form->widgetFieldRow($model, 'text_ru', 'Redactor'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
        <div class="span12">
            <?php echo $form->widgetFieldRow($model, 'text_ua', 'Redactor'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'meta_title_ru'); ?>
            <?php echo $form->textField(
                $model,
                'meta_title_ru',
                array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'meta_title_ru'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'meta_title_ua'); ?>
            <?php echo $form->textField(
                $model,
                'meta_title_ua',
                array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'meta_title_ua'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'meta_description_ru'); ?>
            <?php echo $form->textArea(
                $model,
                'meta_description_ru',
                array('rows' => 4, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'meta_description_ru'); ?>
        </div>
    </div>

    <div class="row" style="margin-bottom: 10px;">
        <div class="span12">
            <?php echo $form->labelEx($model, 'meta_description_ua'); ?>
            <?php echo $form->textArea(
                $model,
                'meta_description_ua',
                array('rows' => 4, 'class' => 'input-xxxlarge')
            ); ?>
            <?php echo $form->error($model, 'meta_description_ua'); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="span12">
            <?php echo CHtml::submitButton(
                $model->isNewRecord ? 'Добавить' : 'Сохранить',
                ['class' => 'btn-new btn-blue']
            ); ?>
        </div>
    </div>

    <?php
    $this->endWidget();
    $this->endClip();
    $this->beginClip('text');

    $count = 1;
    ?>

    <?php foreach ($model_text as $item): ?>

        <?php
            if (!$item) {
                continue;
            }
            $count = $item->id;
        ?>
        <div class="landing-text-form item" id="<?= $count ?>">
            <?php $formText = $this->beginWidget(
                'BootActiveForm',
                array(
                    'id' => "landing-text-form",
                    'enableAjaxValidation' => false,
                )
            ); ?>

            <?= $formText->hiddenField($item, 'id', array('value' => $item->id)) ?>
            <?= $formText->hiddenField($item, 'landing_id', array('value' => $model->id)) ?>

            <div class="row">
                <div class="span12 span12-landing span12-title">
                    <?php
                    $paddingLeft = 34 + 8 * strlen($item->id);
                    ?>
                    <?= $formText->textField($item, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge input-label input-title', 'style' => "padding-left: {$paddingLeft}px")); ?>
                    <span class="title-id">Id: <?= $item->id ?>.</span>
                </div>
            </div>

            <div class="row" style="margin-bottom: 5px;">
                <div class="span12 span12-landing span12-notice">
                    <?php echo $formText->textField($item, 'name_manager', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge input-label input-notice')); ?>
                    <span class="notice-marker">*</span>
                </div>
            </div>

            <div class="row" style="margin-bottom: 10px;">
                <div class="span6 span6-landing">
                    <?= $formText->widgetFieldRow($item, 'text_ru', 'Redactor', ['id' => 'ru' . $count]); ?>
                </div>
                <div class="span6 span6-landing">
                    <?= $formText->widgetFieldRow($item, 'text_ua', 'Redactor', ['id' => 'ua' . $count]); ?>
                </div>
            </div>

            <div class="row">
                <div class="span12 span12-landing">
                    <?php echo CHtml::submitButton($item->isNewRecord ? 'Создать' : 'Сохранить', ['class'=>"btn-new btn-blue button_change{$count}"]); ?>
                </div>
            </div>

            <?php
            $this->endWidget();
            $count++;
            ?>
        </div>
    <?php endforeach; ?>

    <div class="landing-text-form landing-text-form-empty item" id="<?= $count ?>"></div>

    <div class="row">
        <div class="span12">
            <div id="add_button" style="margin-top: -5px; padding-top: 15px; padding-bottom: 15px; padding-left: 15px; background-color: #dddddd;">
                <a href="#" class="add_button btn-new btn-green" data-count="<?= $count ?>" data-id="<?= $model->id ?>">
                    Добавить текстовый блок
                </a>
            </div>
        </div>
    </div>

    <?php
        $this->endClip();
        $this->beginClip('gallery');

        $count = 1;
    ?>

    <?php foreach ($model_gallery as $item): ?>

        <?php
        if (!$item) {
            continue;
        }
        $count = $item->id;
        ?>
        <div class="landing-text-form item" id="<?= $count ?>">
            <?php $formGallery = $this->beginWidget(
                'BootActiveForm',
                array(
                    'id' => "landing-gallery-form",
                    'enableAjaxValidation' => false,
                )
            ); ?>

            <?= $formGallery->hiddenField($item, 'id', array('value' => $item->id)) ?>
            <?= $formGallery->hiddenField($item, 'landing_id', array('value' => $model->id)) ?>

            <div class="row">
                <div class="span12 span12-landing span12-title">
                    <?php
                    $paddingLeft = 34 + 8 * strlen($item->id);
                    ?>
                    <?= $formGallery->textField($item, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge input-label input-title', 'style' => "padding-left: {$paddingLeft}px")); ?>
                    <span class="title-id">Id: <?= $item->id ?>.</span>
                </div>
            </div>

            <div class="row" style="margin-bottom: 10px;">
                <div class="span12 span12-landing span12-notice">
                    <?php echo $formGallery->textField($item, 'name_manager', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge input-label input-notice')); ?>
                    <span class="notice-marker">*</span>
                </div>
            </div>

            <div class="row">
                <div class="span12 span12-landing" style="margin-bottom: 5px; font-size: 13px; font-weight: bold;">Размеры первого изображения</div>
                <div class="span12 span12-landing" style="margin-bottom: 10px;">
                    <div class="row">
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_1_width_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_width_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_1_height_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_height_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_1_width_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_width_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_1_height_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_height_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_1_width_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_width_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_1_height_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_1_height_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12 span12-landing" style="margin-bottom: 5px; font-size: 13px; font-weight: bold;">Размеры второго изображения (для мобильных)</div>
                <div class="span12 span12-landing" style="margin-bottom: 10px;">
                    <div class="row">
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_2_width_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_width_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_2_height_lg', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_height_lg', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_2_width_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_width_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_2_height_md', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_height_md', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                        <div class="span1 span1-landing with-delimiter">
                            <?php echo $formGallery->labelEx($item, 'image_2_width_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_width_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                            <span class="size-delimiter">x</span>
                        </div>
                        <div class="span1 span1-landing">
                            <?php echo $formGallery->labelEx($item, 'image_2_height_sm', ['style' => 'font-size: 13px; font-style: italic;']); ?>
                            <?php echo $formGallery->textField($item, 'image_2_height_sm', array('size' => 60, 'maxlength' => 255, 'class' => 'input-xxxlarge')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12 span12-landing">
                    <?php echo CHtml::submitButton($item->isNewRecord ? 'Создать' : 'Сохранить', ['class'=>"btn-new btn-blue button_change_{$count}"]); ?>
                    <?php echo CHtml::link(
                            $item->isNewRecord ? 'Добавить элементы' : 'Изменить элементы',
                            ['landinggalleryitem/view', 'id' => $item->id],
                            ['class'=>"btn-new btn-green button_change_{$count}", 'data-id' => $item->id]);
                    ?>
                </div>
            </div>


            <?php
            $this->endWidget();
            $count++;
            ?>
        </div>
    <?php endforeach; ?>

    <div class="landing-text-form landing-text-form-empty item" id="gallery<?= $count ?>"></div>

    <div class="row">
        <div class="span12">
            <div id="add_gallery_button" style="margin-top: -5px; padding-top: 15px; padding-bottom: 15px; padding-left: 15px; background-color: #dddddd;">
                <a href="#" class="add_gallery_button btn-new btn-green" data-count="<?= $count ?>" data-id="<?= $model->id ?>">
                    Добавить галерею
                </a>
            </div>
        </div>
    </div>

    <?php

    $this->endClip();

    $this->widget(
        'BootTabView',
        array(
            'activeTab' => 'basic',
            'viewData' => array(
                'model' => $model,
                'model_text' => $model_text,
                'model_gallery' => $model_gallery,
                'form' => $form,
                'formText' => $formText,
                'formGallery' => $formGallery,
            ),
            'tabs' => array(
                'basic' => array(
                    'title' => 'Основные данные',
                    'content' => $this->clips['basic'],
                ),
                'text' => array(
                    'title' => 'Текстовый контент',
                    'content' => $this->clips['text'],
                ),
                'gallery' => array(
                    'title' => 'Список галерей',
//                'view' => '//seo/_form',
                    'content' => $this->clips['gallery'],
                ),
            )
        )
    );

    ?>

</div>

<div id="galleryWindow" class="gallery-window-wrap">
    <div class="gallery-window">
        <div class="gallery-window-inner" id="galleryWindowItems"></div>
    </div>
</div>
<div id="galleryWindowOverlay" class="gallery-window-overlay"></div>

<style>
    body {
        position: relative;
    }
</style>

<script>
    $(document).on("click", 'a[class*="button_change_"]', function (ev) {
        showHideGalleryWindow();
        var id = $(this).data('id'),
            title = $(this).closest(".form-vertical").find(".input-title").val();
        $.ajax({
            url: "<?= $this->createUrl('landinggalleryitem/get') ?>",
            type: 'POST',
            dataType : 'html',
            data:
                {
                    id: id,
                },
            success: function(html){
                $("#galleryWindowItems").html(html).prepend("<h3>" + title + "</h3>");
                fileInputWidget();

                var textareas = document.querySelectorAll("[name='text_ru'], [name='text_ua']");
                for (var i = 0 ; i < textareas.length; i++) {
                    textareas[i].setAttribute('id', 'textarea' + i);
                }
                $("[name='text_ru'], [name='text_ua']").each(function(index, value) {
                    $("#textarea" + index).redactor();
                });
            },
            error: function (error) {
            }
        });

        return false;
    });

    $(document).on("click", "#galleryWindowOverlay", function () {
        showHideGalleryWindow(false);
    });

    function showHideGalleryWindow(show = true) {
        var window = $("#galleryWindow"),
            overlay = $("#galleryWindowOverlay"),
            body = $("html, body");
        if (show) {
            overlay.show();
            setTimeout(function () {
                window.css({"width": "940px"});
            }, 400);
            body.css({"overflow": "hidden"});
        } else {
            window.css({"width": "0"});
            setTimeout(function () {
                overlay.hide();
                body.css({"overflow": "initial"});
            }, 800);
        }

    }

    $(document).on('submit', 'form', function (event) {
       if ($(this).attr('id') == "landing-text-form") {
           event.preventDefault();
           var data = decodeURI($(this).serialize()),
               id = $(this).parent(),
               count = $(this).parent().attr('id'),
               link = '.button_change' + count;
           $.ajax({
               url: "<?= $this->createUrl('landingtext/save') ?>",
               type: 'POST',
               dataType : 'html',
               data:
                   {
                       data:data,
                   },
               success: function(html){
                   $(id).find('#LandingText_id').val(html);
                   $(link).val('Сохранить');
                   var title = $(link).closest(".form-vertical").find(".row-title").children(".span12-landing").addClass("span12-title"),
                       notice = $(link).closest(".form-vertical").find(".row-notice").children(".span12-landing").addClass("span12-notice"),
                       paddingLeft = 34 + 8 * count.length;
                   title.children("label").remove();
                   title.children("input").addClass("input-label input-title").css({"padding-left": paddingLeft + 'px'}).after('<span class="title-id">Id: ' + count + '.</span>');
                   notice.children("label").remove();
                   notice.children("input").addClass("input-label input-notice").after('<span class="notice-marker">*</span>');
               },
               error: function (error) {
               }
           });
       }
    });

    $(document).on('click', '.add_button', function (event) {
        event.stopPropagation();
        event.preventDefault();
        var count = $(this).data("count"),
            id = $(this).data("id"),
            link = "#" + $(this).data("count"),
            count_next = count + 1,
            action = $('#landing-text-form').attr('action'),
            ru = "#ru"+count,
            ua = "#ua"+count;
        $.ajax({
            url: "<?= $this->createUrl('landingtext/add') ?>",
            type: 'POST',
            dataType: 'html',
            data:
                {
                    count: count,
                    id: id,
                },
            success: function (html) {
                $(link).html(html);
                $(ua).redactor({'minHeight':'80'});
                $(ru).redactor({'minHeight':'80'});
                $('#add_button').parent().parent().before(function () {
                    return "<div class='landing-text-form landing-text-form-empty item' id='" + count_next + "'></div>";
                });
                $("#" + count).removeClass("landing-text-form-empty");
                $("html,body").animate({scrollTop: $("#add_button").offset().top}, 1000);
                $(".form-vertical").each(function( index, element ) {
                    $(this).attr('action', action);
                });

                $('.add_button').data('count', count_next);
                $(link).children('#LandingGallery_id').val(count);
            },
            error: function (error) {
            }
        });
    });

    //gallery
    $(document).on('submit', 'form', function (event) {
        if ($(this).attr('id') == "landing-gallery-form") {
            event.preventDefault();
            var data = decodeURI($(this).serialize()),
                id = $(this).parent(),
                count = $(this).parent().attr('id'),
                link = '.button_change_' + count;
            console.log($(id).find('#LandingGallery_id').val());
            $.ajax({
                url: "<?= $this->createUrl('landinggallery/save') ?>",
                type: 'POST',
                dataType : 'html',
                data:
                    {
                        data:data,
                    },
                success: function(html){
                    id.attr('id',html);
                    $(id).find('#LandingGallery_id').val(html);
                    $(link).val('Сохранить');
                    var title = $(link).closest(".form-vertical").find(".row-title").children(".span12-landing").addClass("span12-title"),
                        notice = $(link).closest(".form-vertical").find(".row-notice").children(".span12-landing").addClass("span12-notice"),
                        paddingLeft = 34 + 8 * count.length;
                    title.children("label").remove();
                    title.children("input").addClass("input-label input-title").css({"padding-left": paddingLeft + 'px'}).after('<span class="title-id">Id: ' + html + '.</span>');
                    notice.children("label").remove();
                    notice.children("input").addClass("input-label input-notice").after('<span class="notice-marker">*</span>');
                },
                error: function (error) {
                }
            });
        }
    });

    $(document).on('click', '.add_gallery_button', function (event) {
        event.stopPropagation();
        event.preventDefault();
        var count = $(this).data("count"),
            id = $(this).data("id"),
            link = "#gallery" + $(this).data("count"),
            count_next = count + 1,
            action = $('#landing-text-form').attr('action');
        $.ajax({
            url: "<?= $this->createUrl('landinggallery/add') ?>",
            type: 'POST',
            dataType: 'html',
            data:
                {
                    count: count,
                    id: id,
                },
            success: function (html) {
                $(link).html(html);
                $('#add_gallery_button').parent().parent().before(function () {
                    return "<div class='landing-text-form landing-text-form-empty item' id='gallery" + count_next + "'></div>";
                });
                $("#gallery" + count).removeClass("landing-text-form-empty");
                $("html,body").animate({scrollTop: $("#add_gallery_button").offset().top}, 1000);
                $(".form-vertical").each(function( index, element ) {
                    $(this).attr('action', action);
                });

                $('.add_gallery_button').data('count', count_next);

                $(link).find('#LandingGallery_id').val(count);
                $(link).attr('id',count); //это последнее!!!
            },
            error: function (error) {
            }
        });
    });

    function fileInputWidget() {
        const fileInputs = document.querySelectorAll("[type='file']");
        for (let i = 0 ; i < fileInputs.length; i++) {
            fileInputs[i].addEventListener('change', function () {
                const file = this.files[0],
                    img = this.closest('.file-input-wrap').querySelector('img');

                if (['image/jpeg', 'image/png', 'image/gif'].includes(file.type)) {
                    const reader = new FileReader();
                    reader.addEventListener('load', function () {
                        img.src = reader.result;
                    }, false);

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }
            }) ;
        }
    }

    $(document).on('click', '.buttonSave', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var id = $(this).data('id'),
            form = $('#landing-gallery-item-form-' + id),
            landing_gallery_id = form.find('[name=landing_gallery_id]').val(),
            position = form.find('[name=position]').val(),
            video = form.find('[name=video]').val(),
            title_ru = form.find('[name=title_ru]').val(),
            title_ua = form.find('[name=title_ua]').val(),
            text_ru = form.find('[name=text_ru]').val(),
            text_ua = form.find('[name=text_ua]').val(),
            status = form.find('[name=status]').val(),
            file = document.getElementById('image_1_url-' + id).files[0],
            file2 = document.getElementById('image_2_url-' + id).files[0];

        var data = new FormData();

        if (file) {
            data.append("image_1_url", file);
            console.log(file);
        }
        if (file2) {
            data.append("image_2_url", file2);
            console.log(file2);
        }

        data.append("id", id);
        data.append("landing_gallery_id", landing_gallery_id);
        data.append("position", position);
        data.append("video", video);
        data.append("title_ru", title_ru);
        data.append("title_ua", title_ua);
        data.append("text_ru", text_ru);
        data.append("text_ua", text_ua);
        data.append("status", status);

        $.ajax({
            url: "<?= $this->createUrl('landinggalleryitem/load') ?>",
            type: 'POST',
            dataType: 'html',
            data: data,
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
                // console.log(error)
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $(document).on("click", ".add_gallery_image_button", function (event) {
        event.stopPropagation();
        event.preventDefault();
        var landing_gallery_id = $(this).data("landing_gallery_id"),
            count = $(this).data("count"),
            target = $("#galleryItem" + $(this).data("count")),
            count_next = count + 1;
        $.ajax({
            url: "<?= $this->createUrl('landinggalleryitem/add') ?>",
            type: 'POST',
            dataType: 'html',
            data:
                {
                    landing_gallery_id: landing_gallery_id,
                    count: count,
                },
            success: function (html) {
                target.html(html).removeClass("landing_gallery_item_empty");

                var textareasQuantity = $("[name='text_ru'], [name='text_ua']").size();

                // инициализируем редактор на двух последних textarea
                $("#textarea" + (textareasQuantity - 2)).redactor();
                $("#textarea" + (textareasQuantity - 1)).redactor();

                $('#add_gallery_image_button').parent().parent().before(function () {
                    return "<div class='landing_gallery_item landing_gallery_item_empty' id='galleryItem" + count_next + "'></div>";
                });

                setTimeout(function () {
                    $(".gallery-window").animate({scrollTop: $("#galleryWindowItems").height()}, 1000);
                }, 100);

                $(".add_gallery_image_button").data("count", count_next);

                fileInputWidget();
            },
            error: function (error) {
            }
        });
    });

    $(document).on("change", 'input[type="checkbox"]', function () {
        this.value = (Number(this.checked));
    });

    $(document).on("click", ".buttonDelete", function(event) {
        event.stopPropagation();
        event.preventDefault();

        var id = $(this).data("id"),
            elem = $(this).closest(".landing_gallery_item");

        if (confirm("Вы уверены, что хотите удалить этот слайд?")) {
            $.ajax({
                url: "<?= $this->createUrl('landinggalleryitem/del') ?>",
                type: "POST",
                dataType: "html",
                data: {
                    id: id
                },
                success: function (data) {
                    elem.remove();
                },
                error: function (error) {
                    console.log(error)
                },
            });
        }

    });
</script>
