<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'email', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endWidget();
    ?>

</div><!-- form -->