<?php
$this->breadcrumbs=array(
	'Подписчики',
);
?>

<div class="row">

<div class="span9">
<?php
$this->widget('BootMenu', array(
    'activeIconClass'=>'icon-white',
    'items'=>array(
        array('label'=>'Все', 'icon'=>'th-list', 'url'=>array('index'), 'active'=>empty($_GET)),
        array('label'=>'Вклеченные', 'icon'=>'eye-open', 'url'=>array('index', 'Subscriber'=>array('status'=>Subscriber::STATUS_ENABLED)),'active'=>$model->status==Subscriber::STATUS_ENABLED),
        array('label'=>'Отключенные', 'icon'=>'eye-close', 'url'=>array('index', 'Subscriber'=>array('status'=>Subscriber::STATUS_DISABLED)),'active'=>$model->status==Subscriber::STATUS_DISABLED),
        array('label'=>'Все', 'icon'=>'download', 'url'=>array('exportCsv')),
        array('label'=>'Активные', 'icon'=>'download', 'url'=>array('exportCsv', 'status'=>Subscriber::STATUS_ENABLED)),
        array('label'=>'Не активные', 'icon'=>'download', 'url'=>array('exportCsv', 'status'=>Subscriber::STATUS_DISABLED)),
    ),
    'htmlOptions'=>array('class'=>'nav-pills')
));
?>
</div>

<div class="pull-right">
    <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
</div>

</div>

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>

<?php $this->widget('BootGridView', array(
    'id'=> get_class($model) . '-grid',
	'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        array(
            'name' => 'status',
            'header'=>'',
            'type' => 'html',
            'value' => '$data->statusIcon',
            'htmlOptions'=>array('style'=>'width: 16px'),
        ),
        'name',
        'email',
        array(
            'class'=>'application.widgets.grid.BootButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
            'buttons'=>array(
                'view'=>array(
                    'visible'=>'false',
                    'url'=>'$data->url',
                ),
            ),
        ),
   	),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>