<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->assetsUrl; ?>/css/main.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<?php
if ($model->hasErrors())
    Yii::app()->getClientScript()->registerScript('shake', "$('.hero-unit').effect('shake', {distance:6,times:2}, 100);");
?>
<div class="hero-unit" align="center" style="width: 300px;">

    <h1>Magaziller</h1>

    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => 'login-form',
        'type' => BootActiveForm::TYPE_INLINE,
    )); ?>

    <p>
    <div class="control-group">
        <?php echo $form->textField($model, 'username', array('placeholder' => $model->getAttributeLabel('username'))); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->passwordField($model, 'password', array('placeholder' => $model->getAttributeLabel('password'), 'value'=>'')); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="control-group">
        <label for="LoginForm_rememberMe" class="checkbox inline">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $model->getAttributeLabel('rememberMe'); ?>
        </label>
    </div>

    <input type="submit" value="Войти" class="btn btn-large btn-primary span2">
    </p>

    <?php $this->endWidget(); ?>

</div>

<script type="text/javascript">

    $(window).resize(function () {
        $('.hero-unit').css({
            position:'absolute',
            left:($(window).width() - $('.hero-unit').outerWidth()) / 2 + 'px',
            top:($(window).height() - $('.hero-unit').outerHeight()) / 2 + 'px'
        })
    }).resize();

</script>

</body>
</html>