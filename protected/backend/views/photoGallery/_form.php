<div class="form">

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));
    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'project_name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'construction_place', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'gross_area', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'workload', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'turnaround_time', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge',
    ));

    echo $form->textFieldRow($model, 'period_of_works', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));
    ?>

    <div class="row">
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code1', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service1', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code2', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service2', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code3', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service3', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code4', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service4', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code5', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service5', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
        <div class="span3"><?php echo $form->textFieldRow($model, 'video_code6', array('class'=>'span3'))?></div>
        <div class="span1"><?php echo $form->dropDownListRow($model, 'service6', Lookup::items('VideoServiceType'), array('class'=>'span1'));?></div>
    </div>

    <?php
    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'show_albom', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    echo $form->widgetFieldRow($model, 'description', 'Redactor');

    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'basic' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'seo' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
            'images' => array(
                'title' => 'Изображения',
                'view' => '_images',
                'visible' => !$model->isNewRecord
            )
        ),
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
