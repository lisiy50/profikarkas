<?php
$this->breadcrumbs=array(
	'Галереи изображений'=>array('index'),
	$model->project_name
);
?>
<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            <?php echo empty($model->project_name)?'#'.$model->id:CHtml::encode($model->project_name); ?>
        </h4>
    </div>
    <div class="pull-right">
        <?php /*echo CHtml::link('<i class="icon-trash icon-white"></i> Удалить полностью', '#', array(
            'submit'=>array('delete','id'=>$model->id),
            'confirm'=>'Вы уверены, что хотите удалить галерею "'.CHtml::encode($model->project_name).'"?',
            'class'=>'btn btn-small btn-danger',
        ));*/?>

        <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-small"><i class="icon-plus"></i> Добавить</a>

        <a href="#" class="btn btn-success btn-small" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-ok icon-white"></i> Сохранить</a>
    </div>
</div>
<hr>
<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
)); ?>