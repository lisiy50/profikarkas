<?php
/**
 * @var VideoWalkController $this
 * @var VideoWalk $model
 */
?>
<?php
$this->breadcrumbs=array(
	'Видеопрогулки'=>array('index'),
	'Добавление',
);
$this->icon='page_add';
?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Видеопрогулки
        </h4>
    </div>
    <div class="pull-right">
        <button class="btn btn-small btn-success" type="button" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-plus icon-white"></i> Добавить</button>
    </div>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>