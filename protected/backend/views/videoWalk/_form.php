<?php
/**
 * @var VideoWalkController $this
 * @var VideoWalk $model
 */
?>
<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    /** @var BootActiveForm $form */
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_uk', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_en', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_fr', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_de', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'projects', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'path', array(
        'class' => 'input-xxlarge'
    ));

    echo $form->imageFileRow($model, 'image');
    echo CHtml::link('Удалить изображение', array('VideoWalk/deleteImage', 'id'=>$model->id));


    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    $this->endWidget();
    ?>

</div><!-- form -->
