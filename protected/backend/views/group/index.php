<?php
$this->breadcrumbs=array(
	'Группы',
);
?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Группы
        </h4>
    </div>
    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
    </div>
</div>

<hr>

<div class="row">

<?php foreach($letters as $letter=>$groups): ?>
    <div class="item-view item-view3">
        <strong class="title"><?php echo $letter; ?></strong><br>
        <?php $links=array(); foreach($groups as $group) $links[]=CHtml::link($group->name, array('update', 'id'=>$group->id)); echo implode(' | ', $links); ?>
    </div>
<?php endforeach; ?>

</div>