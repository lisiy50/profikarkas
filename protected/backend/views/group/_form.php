<div class="form">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    $this->endWidget();
    ?>

</div><!-- form -->