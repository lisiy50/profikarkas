<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    echo $form->textFieldRow($model, 'name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->widgetFieldRow($model, 'content', 'Redactor');

    echo $form->widgetFieldRow($model, 'parent_id', 'McDropdown', array(
        'without' => $model->id,
        'data' => Faq::model()->rooted()->findAll(),
        'options' => array(
            'select' => 'js:function(value){
                $("#position").html("Сперва нужно сохранить запись.");
            }',
        ),
        'htmlOptions' => array('class' => 'span5'),
    ));

    $this->endWidget();
?>

</div><!-- form -->