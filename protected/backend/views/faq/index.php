<?php
$this->breadcrumbs=array(
	'Оснащение дома',
);
?>

<div class="row">

<div class="pull-right">
    <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
</div>

</div>

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>

<?php $this->widget('BootGridView', array(
    'id'=> get_class($model) . '-grid',
	'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'columns'=>array(
//        array(
//            'name' => 'status',
//            'header'=>'',
//            'type' => 'html',
//            'value' => '$data->statusIcon',
//            'htmlOptions'=>array('style'=>'width: 16px'),
//        ),
        'name',
        array(
            'name' => 'parent_id',
            'value' => '$data->parent->name',
        ),
        array(
            'class'=>'application.widgets.grid.BootButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
            'buttons'=>array(
                'view'=>array(
                    'visible'=>'false',
                ),
            ),
        ),
   	),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>