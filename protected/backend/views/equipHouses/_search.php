<?php
$form = $this->beginWidget('BootActiveForm', array(
    'id' => 'search-form',
    'type' => BootActiveForm::TYPE_INLINE,
    'action' => array($this->route),
    'method' => 'get',
    'htmlOptions' => array('class' => 'well'),
));

echo $form->hiddenField($model, 'status') . "\n";

echo $form->textFieldRow($model, 'id', array('maxlength' => 11, 'class' => 'span1')) . "\n";

echo $form->textFieldRow($model, 'name', array('maxlength' => 255, 'class' => 'span9')) . "\n";

echo CHtml::htmlButton('<i class="icon-search"></i> Поиск', array('class' => 'btn pull-right', 'type' => 'submit'));

$this->endWidget();
?>