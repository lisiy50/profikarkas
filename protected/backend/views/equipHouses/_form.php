<div class="form" xmlns="http://www.w3.org/1999/html">

    <hr>

    <?php
    $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
        'htmlOptions'=>array(
            'enctype'=>'multipart/form-data',
        ),
    ));

    echo $form->errorSummary($model);

    $this->beginClip('basic');

    echo $form->textFieldRow($model, 'name', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_uk', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_en', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_fr', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->textFieldRow($model, 'name_de', array(
        'maxlength' => 255,
        'class' => 'input-xxlarge'
    ));

    echo $form->imageFileRow($model, 'image');
    echo $form->checkBoxRow($model, 'status', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));
    echo $form->checkBoxRow($model, 'watermark', array('uncheckValue' => LibraryRecord::STATUS_DISABLED));

    echo $form->widgetFieldRow($model, 'right_description', 'Redactor');
    echo $form->widgetFieldRow($model, 'right_description_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'right_description_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'right_description_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'right_description_de', 'Redactor');

    ?>

    <div class="row">
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h1_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h2_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h3_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
        <div class="span3">
            <?php
            echo $form->textFieldRow($model, 'h4_size', array(
                'class'=>'span2',
                'type' => 'number',
                'min'=>1,
                'max'=>128,
                'append'=>'px',
            ));?>
        </div>
    </div>

    <?php


    echo $form->widgetFieldRow($model, 'description', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_uk', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_en', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_fr', 'Redactor');
    echo $form->widgetFieldRow($model, 'description_de', 'Redactor');


    $this->endClip();

    $this->widget('BootTabView', array(
        'viewData' => array(
            'model' => $model,
            'form' => $form,
        ),
        'tabs' => array(
            'tab1' => array(
                'title' => 'Основные',
                'content' => $this->clips['basic'],
            ),
            'tab2' => array(
                'title' => 'SEO информация',
                'view' => '//seo/_form',
            ),
        )
    ));

    $this->endWidget();
    ?>

</div><!-- form -->
