<?php
$this->breadcrumbs=array(
	'Оснащение дома',
);
?>

<div class="row">

<div class="span8">
<?php
$this->widget('BootMenu', array(
    'activeIconClass'=>'icon-white',
    'items'=>array(
        array('label'=>'Все Оснащения дома', 'icon'=>'th-list', 'url'=>array('index'), 'active'=>empty($_GET)),
        array('label'=>'Опубликованные', 'icon'=>'eye-open', 'url'=>array('index', 'EquipHouses'=>array('status'=>EquipHouses::STATUS_ENABLED)),'active'=>$model->status==EquipHouses::STATUS_ENABLED),
        array('label'=>'Черновики', 'icon'=>'eye-close', 'url'=>array('index', 'EquipHouses'=>array('status'=>EquipHouses::STATUS_DISABLED)),'active'=>$model->status==EquipHouses::STATUS_DISABLED),
    ),
    'htmlOptions'=>array('class'=>'nav-pills')
));
?>
</div>

<div class="pull-right">
    <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i> Добавить</a>
</div>

</div>

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>

<?php $this->widget('BootGridView', array(
    'id'=> get_class($model) . '-grid',
	'dataProvider'=>$model->search(),
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        array(
            'name' => 'status',
            'header'=>'',
            'type' => 'html',
            'value' => '$data->statusIcon',
            'htmlOptions'=>array('style'=>'width: 16px'),
        ),
        'name',
        array(
            'header'=>'Адрес',
            'value' => 'CHtml::encode(Yii::app()->request->getHostInfo().$data->url)',
        ),
        array(
            'class'=>'application.widgets.grid.BootButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
            'buttons'=>array(
                'view'=>array(
                    'visible'=>'$data->status==EquipHouses::STATUS_ENABLED',
                    'url'=>'$data->url',
                ),
            ),
        ),
   	),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>