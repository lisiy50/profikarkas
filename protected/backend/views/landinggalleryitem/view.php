<?php

/* @var $this LandinggalleryitemController */
/* @var $items LandingGalleryItem */
/* @var $id */

$count = 1; // взять последний id со всех LandingGalleryItem

?>

<?php foreach ($items as $item): ?>
    <div class="landing_gallery_item" id="galleryItem<?= $count ?>">
    <form action="" id="landing-gallery-item-form-<?= $item->id ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $item->id ?>">
        <input type="hidden" name="landing_gallery_id" value="<?= $item->landing_gallery_id ?>">

        <div class="row" style="margin-bottom: 10px;">
            <div class="span6">
                <label for="image_1_url-<?= $item->id ?>">Изображение</label>
                <div class="file-input-wrap" style="display: flex; align-items: center">
                    <div class="file-input-pic">
                        <?php if ($item->image_1_url): ?>
                            <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/gallery_item/large/large_' . $item->image_1_url; ?>" alt="">
                        <?php else: ?>
                            <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/image-preview.png'; ?>" alt="">
                        <?php endif; ?>
                    </div>
                    <input type="file" id="image_1_url-<?= $item->id ?>" name="image_1_url" data-id="<?= $item->id ?>">
                </div>
            </div>
            <div class="span6">
                <label for="image_2_url-<?= $item->id ?>">Изображение (для мобильных)</label>
                <div class="file-input-wrap" style="display: flex; align-items: center">
                    <div class="file-input-pic">
                        <?php if ($item->image_2_url): ?>
                            <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/gallery_item_2/large/large_' . $item->image_2_url; ?>" alt="">
                        <?php else: ?>
                            <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/image-preview.png'; ?>" alt="">
                        <?php endif; ?>
                    </div>
                    <input type="file" id="image_2_url-<?= $item->id ?>" name="image_2_url" data-id="<?= $item->id ?>">
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 10px;">
            <div class="span6">
                <label for="">Заголовок (русский)</label>
                <input class="input-xxxlarge" type="text" name="title_ru" value="<?= $item->title_ru ?>">
                <label for="">Текст (русский)</label>
                <textarea name="text_ru"><?= $item->text_ru ?></textarea>
            </div>
            <div class="span6">
                <label for="">Заголовок (украинский)</label>
                <input class="input-xxxlarge" type="text" name="title_ua" value="<?= $item->title_ua ?>">
                <label for="">Текст (украинский)</label>
                <textarea name="text_ua"><?= $item->text_ua ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="span12" style="width: 853px;">
                <label for="">Youtube код (или SVG код)</label>
                <textarea class="input-xxxlarge" type="text" name="video" style="resize: vertical">
                    <?= $item->video ?>
                </textarea>
            </div>
        </div>
        <div class="row">
            <div class="span1">
                <label for="">Позиция</label>
                <input class="input-xxxlarge" type="number" name="position" value="<?= $item->position ?>">
            </div>
        </div>
        <div style="margin-bottom: 10px;">
            <label>
                <input type="checkbox" name="status" value="<?= $item->status ?>" <?= $item->status ? 'checked' : ''; ?>>
                Показывать на сайте
            </label>
        </div>
        <div class="row">
            <div class="span12 span12-landing">
                <?= CHtml::submitButton($item->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => "btn-new btn-blue buttonSave", 'data-id' => $item->id]); ?>
                <?php if (!$item->isNewRecord): ?>
                    <?= CHtml::link('Удалить', '#', ['class' => "btn-new btn-red buttonDelete", 'data-id' => $item->id]); ?>
                <?php endif; ?>
            </div>
        </div>
    </form>
    </div>

    <?php

    $count++; // взять последний id со всех

    ?>

<?php endforeach; ?>
<!-- взять последний id со всех LandingGalleryItem -->
<div class="landing_gallery_item landing_gallery_item_empty" id="galleryItem<?= $count ?>"></div>

<div class="row">
    <div class="span12" style="width: 886px;">
        <div id="add_gallery_image_button" style="margin-top: -5px; padding-top: 15px; padding-bottom: 15px; padding-left: 15px; background-color: #dddddd;">
            <!-- взять последний id со всех LandingGalleryItem -->
            <a href="#"
               class="add_gallery_image_button btn-new btn-green"
               data-count="<?= $count ?>"
               data-landing_gallery_id="<?= $id ?>">
                Добавить элемент
            </a>
        </div>
    </div>
</div>

