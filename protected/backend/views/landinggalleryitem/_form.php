<?php
/* @var $this LandinggalleryitemController */

/* @var $model LandingGalleryItem */
/* @var $count */
?>

<form action="" id="landing-gallery-item-form-<?= $model->id ?>" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?= $model->id ?>">
    <input type="hidden" name="landing_gallery_id" value="<?= $model->landing_gallery_id ?>">

    <div class="row" style="margin-bottom: 10px;">
        <div class="span6">
            <label for="">Изображение</label>
            <div class="file-input-wrap" style="display: flex; align-items: center">
                <div class="file-input-pic">
                    <?php if ($model->image_1_url): ?>
                        <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/gallery_item/large/large_' . $model->image_1_url; ?>" alt="">
                    <?php else: ?>
                        <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/image-preview.png'; ?>" alt="">
                    <?php endif; ?>
                </div>
                <input type="file" id="image_1_url-<?= $model->id ?>" name="image_1_url" data-id="<?= $model->id ?>">
            </div>
        </div>
        <div class="span6">
            <label for="">Изображение (для мобильных)</label>
            <div class="file-input-wrap" style="display: flex; align-items: center">
                <div class="file-input-pic">
                    <?php if ($model->image_2_url): ?>
                        <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/gallery_item_2/large/large_' . $model->image_2_url; ?>" alt="">
                    <?php else: ?>
                        <img style="max-width: 100px; height: auto; max-height: 100px;" src="<?= Yii::app()->baseUrl . '/storage/image-preview.png'; ?>" alt="">
                    <?php endif; ?>
                </div>
                <input type="file" id="image_2_url-<?= $model->id ?>" name="image_2_url" data-id="<?= $model->id ?>">
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="span6">
            <label for="">Заголовок (русский)</label>
            <input class="input-xxxlarge" type="text" name="title_ru" value="<?= $model->title_ru ?>">
            <label for="">Текст (русский)</label>
            <textarea name="text_ru" id="textarea<?= $count * 2 - 2 ?>"><?= $model->text_ru ?></textarea>
        </div>
        <div class="span6">
            <label for="">Заголовок (украинский)</label>
            <input class="input-xxxlarge" type="text" name="title_ua" value="<?= $model->title_ua ?>">
            <label for="">Текст (украинский)</label>
            <textarea name="text_ua" id="textarea<?= $count * 2 - 1 ?>"><?= $model->text_ua ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="span12" style="width: 853px;">
            <label for="">Youtube код (или SVG код)</label>
            <textarea class="input-xxxlarge" type="text" name="video" style="resize: vertical">
                <?= $model->video ?>
            </textarea>
        </div>
    </div>
    <div class="row">
        <div class="span1">
            <label for="">Позиция</label>
            <input class="input-xxxlarge" type="number" name="position" value="<?= $model->position ?>">
        </div>
    </div>
    <div style="margin-bottom: 10px;">
        <label>
            <input type="checkbox" name="status" value="<?= $model->status ?>">
            Показывать на сайте
        </label>
    </div>
    <div class="row">
        <div class="span12 span12-landing">
            <?= CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => "btn-new btn-blue buttonSave", 'data-id' => $model->id]); ?>
        </div>
    </div>
</form>
