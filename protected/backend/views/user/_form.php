<?php
Yii::app()->clientScript->registerScript('changeRole', "
    $('#User_role').change(function(){
        if($(this).val()=='" . User::ROLE_CLIENT . "') {
            $('.control-group:has(#User_group_id), .control-group:has(#User_address), .control-group:has(#User_comment)').show();
        } else {
            $('.control-group:has(#User_group_id), .control-group:has(#User_address), .control-group:has(#User_comment)').hide();
        }
    }).change();
");
?>
<div class="form">
    <?php $form = $this->beginWidget('BootActiveForm', array(
        'id' => get_class($model) . '-form',
        'enableAjaxValidation' => true,
    )); ?>


    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model, 'username', array(
        'maxlength' => 255,
        'class' => 'input-large',
        'disabled' => !$model->isNewRecord
    )); ?>

    <div class="control-group">
        <label for="User_status" class="checkbox inline">
            <?php echo $form->checkBox($model, 'status', array('uncheckValue' => User::STATUS_DISABLED)); ?>
            <?php echo $model->getAttributeLabel('status'); ?>
        </label>
        <?php if (!$model->isNewRecord): ?>
        <label for="changePassword" class="checkbox inline">
            <?php echo CHtml::checkBox('changePassword', $model->scenario == 'changePassword'); ?>
            Изменить пароль
        </label>
        <?php endif; ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <?php echo $form->passwordFieldRow($model, 'oldPassword', array(
        'maxlength' => 128,
        'class' => 'input-xlarge',
        'value' => ''
    )); ?>

    <?php echo $form->passwordFieldRow($model, 'password', array(
        'maxlength' => 128,
        'class' => 'input-xlarge',
        'value' => ''
    )); ?>

    <?php echo $form->passwordFieldRow($model, 'rPassword', array(
        'maxlength' => 128,
        'class' => 'input-xlarge',
        'value' => ''
    )); ?>

    <?php echo $form->dropDownListRow($model, 'role', Lookup::items('UserRole')); ?>

    <?php echo $form->textFieldRow($model, 'fio', array(
        'maxlength' => 255,
        'class' => 'input-large',
    )); ?>

    <?php echo $form->textFieldRow($model, 'email', array(
        'maxlength' => 255,
        'class' => 'input-large',
    )); ?>

    <?php echo $form->textFieldRow($model, 'phone', array(
        'maxlength' => 255,
        'class' => 'input-large',
    )); ?>

    <?php echo $form->dropDownListRow($model, 'group_id', $model->getGroupList(), array(
        'prompt' => '',
        'class' => 'input-large',
    )); ?>

    <?php echo $form->textAreaRow($model, 'address', array(
        'class' => 'input-xxlarge',
        'rows' => 2,
    )); ?>

    <?php echo $form->textAreaRow($model, 'comment', array(
        'class' => 'input-xxlarge',
        'rows' => 2,
    )); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->