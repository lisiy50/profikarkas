<?php
$this->breadcrumbs = array(
    'Пользователи' => array('index'),
    'Добавление',
);
$this->icon = 'user_add';
?>

<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            Добавление пользователя
        </h4>
    </div>
    <div class="pull-right">
        <a class="btn btn-small" href="<?php echo $this->createUrl('index'); ?>"><i class="icon-th-large"></i> Каталог</a>
        <button class="btn btn-small btn-success" type="button" onclick="$('#<?php echo get_class($model); ?>-form').submit();">
            <i class="icon-plus icon-white"></i> Добавить
        </button>
    </div>
</div>
<hr>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>