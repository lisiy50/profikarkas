<?php $form=$this->beginWidget('BootActiveForm', array(
    'id'=>'search-form',
    'type'=>BootActiveForm::TYPE_INLINE,
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

    <?php echo $form->hiddenField($model,'role'); ?>
    <?php echo $form->textFieldRow($model,'id',array('maxlength'=>11,'class'=>'span1')); ?>
    <?php echo $form->textFieldRow($model,'username',array('maxlength'=>255,'class'=>'span2')); ?>
    <?php echo $form->textFieldRow($model,'email',array('maxlength'=>255,'class'=>'span3')); ?>
    <?php echo $form->textFieldRow($model,'phone',array('maxlength'=>255,'class'=>'span3')); ?>
    <?php echo $form->dropDownList($model,'group_id', CHtml::listData(Group::model()->findAll(), 'id', 'name'), array('prompt'=>'','class'=>'span2')); ?>

    <?php echo CHtml::htmlButton('<i class="icon-search"></i> Поиск', array('class'=>'btn pull-right','type'=>'submit')); ?>

<?php $this->endWidget(); ?>










