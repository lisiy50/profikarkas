<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('index'),
    Lookup::item('UserRole', $model->role)=>array('index', 'User'=>array('role'=>$model->role)),
	$model->username
);
switch($model->role) {
    case User::ROLE_ADMIN:
        $this->icon = 'administrator';
        break;
    case User::ROLE_CLIENT:
        $this->icon = 'user_green';
        break;
}
?>
<div class="row">
    <div class="span7">
        <h4 class="nomargin">
            <img src="<?php echo $this->assetsUrl . '/icon/' . $this->icon . '.png'; ?>" title="<?php echo CHtml::encode($this->pageTitle); ?>">
            <?php echo empty($model->username)?'#'.$model->id:$model->username; ?>
        </h4>
    </div>
    <div class="pull-right">
        <?php /*echo CHtml::link('<i class="icon-trash icon-white"></i> Удалить полностью', '#', array(
            'submit'=>array('delete','id'=>$model->id),
            'confirm'=>'Вы уверены, что хотите удалить пользователя "'.CHtml::encode($model->username).'"?',
            'class'=>'btn btn-small btn-danger',
        ));*/?>
        <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-small"><i class="icon-plus"></i> Добавить</a>

        <a href="#" class="btn btn-success btn-small" onclick="$('#<?php echo get_class($model); ?>-form').submit();"><i class="icon-ok icon-white"></i> Сохранить</a>
    </div>

</div>
<hr>
<?php echo $this->renderPartial('_form', array(
    'model'=>$model,
)); ?>

<script type="text/javascript">

    $('#changePassword').change(function(){
        if($(this).is(':checked'))
            $('.control-group:has(#User_password), .control-group:has(#User_rPassword), .control-group:has(#User_oldPassword)').show();
        else
            $('.control-group:has(#User_password), .control-group:has(#User_rPassword), .control-group:has(#User_oldPassword)').hide();
    }).change();

</script>