<?php
$this->breadcrumbs = array(
    'Пользователи',
);

switch($model->role) {
    case User::ROLE_ADMIN:
        $this->icon = 'administrator';
        break;
    case User::ROLE_CLIENT:
        $this->icon = 'user_green';
        break;
}
?>

<div class="row">

    <div class="span9">
        <?php
        $this->widget('BootMenu', array(
            'activeIconClass' => 'icon-white',
            'items' => array(
                array('label' => 'Все', 'url' => array('user/index'), 'active' => empty($_GET)),
                array('label' => 'Клиенты', 'url' => array('user/index', 'User' => array('role' => User::ROLE_CLIENT)), 'active' => $model->role == User::ROLE_CLIENT),
                array('label' => 'Менеджеры', 'url' => array('user/index', 'User' => array('role' => User::ROLE_MANAGER)), 'active' => $model->role == User::ROLE_MANAGER),
                array('label' => 'Контент менеджеры', 'url' => array('user/index', 'User' => array('role' => User::ROLE_CONTENT)), 'active' => $model->role == User::ROLE_CONTENT),
                array('label' => 'Администраторы', 'url' => array('user/index', 'User' => array('role' => User::ROLE_ADMIN)), 'active' => $model->role == User::ROLE_ADMIN),
                array('label' => 'Заблокированные', 'url' => array('user/index', 'User' => array('status' => User::STATUS_DISABLED)), 'active' => $model->status == User::STATUS_DISABLED),
            ),
            'htmlOptions' => array('class' => 'nav nav-pills')
        ));
        ?>
    </div>

    <div class="span3">
        <div class="pull-right">
            <a class="btn btn-small" href="<?php echo $this->createUrl('create'); ?>"><i class="icon-plus"></i>Добавить</a>
            <a class="btn btn-small" href="<?php echo $this->createUrl('csv', $_GET); ?>"><i class="icon-download-alt"></i>Скачать</a>
        </div>
    </div>

</div>

<?php $this->renderPartial('_search', array(
    'model' => $model,
)); ?>

<?php $this->widget('BootGridView', array(
    'id' => get_class($model) . '-grid',
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}",
    'columns' => CMap::mergeArray(array(
        array(
            'name' => 'status',
            'header' => '',
            'type' => 'html',
            'value' => '$data->statusIcon',
            'htmlOptions' => array('style' => 'width: 16px'),
        ),
    ), ($model->role == User::ROLE_CLIENT) ? array(
        'username',
        'email',
        'phone',
        array('name' => 'group.name', 'header' => 'Группа'),
    ) : array(
        'username',
        'email',
    ), array(
        array(
            'class' => 'application.widgets.grid.BootButtonColumn',
            'template' => '{update} {delete}',
            'htmlOptions' => array('style' => 'width: 32px'),
        ),
    )),
)); ?>

<script type="text/javascript">

    $('#search-form').on('submit', function() {
        $.fn.yiiGridView.update('<?php echo get_class($model); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });

</script>