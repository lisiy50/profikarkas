<?php $this->beginContent('//layouts/main'); ?>
<div class="row show-grid">
<div class="span9">
    <?php echo $content; ?>
</div>
<div class="span3">
    <div class="well" style="padding: 8px 0;">
    <?php $this->widget('BootMenu', array(
        'activeIconClass'=>'icon-white',
        'items'=>$this->menu,
        'htmlOptions'=>array(
            'class'=>'nav-list'
        )
    )); ?>
    </div>
</div>
</div>
<?php $this->endContent(); ?>