<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->assetsUrl; ?>/css/main.css"/>
</head>

<body>
<div class="container" id="page">

    <div class="navbar" style="margin-top: 5px;">
        <div class="navbar-inner">
            <div class="container">
                <?php
                $this->widget('BootMenu', array(
                    'items' => $this->getTopMenu()
                ));
                ?>
                <?php
                $this->widget('BootMenu', array(
                    'items' => array(
                        array(
                            'label' => mb_strimwidth(Yii::app()->user->name, 0, 12, "..."),
                            'url' => '#',
                            'items' => array(
                                array('label' => 'Профиль', 'url' => array('user/update', 'id' => Yii::app()->user->id)),
                                array('label' => 'Выход', 'url' => array('site/logout')),
                            )
                        )
                    ),
                    'htmlOptions' => array('class' => 'pull-right')
                ));
                ?>
            </div>
        </div>
    </div>

    <?php $this->widget('BootBreadcrumbs', array(
        'homeLink' => array('label' => 'Домой', 'url' => Yii::app()->homeUrl),
        'links' => $this->breadcrumbs,
    )); ?>

    <?php echo $content; ?>

    <hr>

    <footer>
        <p>Profikarkas | Copyright &copy; <?php echo date('Y'); ?> Powered by Yii.</p>
    </footer>
</div>

</body>
</html>
