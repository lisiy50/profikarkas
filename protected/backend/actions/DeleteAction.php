<?php
Yii::import('application.backend.actions.GridAction');

class DeleteAction extends GridAction
{
    public $successMessage = 'Запись удалена';
    public $returnUrl=array('index');

    public function run()
    {
        if (empty($_GET['id']))
            throw new CHttpException(404, 'The requested page does not exist.');

        $controller = $this->getController();

        if (Yii::app()->request->isPostRequest) {
            $model = CActiveRecord::model($this->modelClass)->findByPk((int)$_GET['id']);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            $message = $this->getMessage($model);
            $model->delete();
            if (!isset($_GET['ajax'])) {
                if ($message)
                    Yii::app()->user->setFlash('success', $message);
                $controller->redirect(isset($_REQUEST['returnUrl']) ? $_REQUEST['returnUrl'] : $this->returnUrl);
            } else {
                if ($message)
                    echo CJSON::encode(array('status' => 'success', 'message' => $message));
                exit;
            }
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function getMessage($model)
    {
        $message = $this->successMessage;

        if (empty($message))
            return false;

        preg_match_all('#{([a-z]+)}#i', $message, $matches);

        if (isset($matches[1]) && is_array($matches[1])) {
            foreach ($matches[1] as $param) {
                if ($model->hasAttribute($param))
                    $message = str_replace('{' . $param . '}', CHtml::encode($model->getAttribute($param)), $message);
            }
        }

        return $message;
    }

}