<?php
Yii::import('application.backend.actions.GridAction');

class UpdateAction extends GridAction
{

    public $successMessage = 'Изменения сохранены';

    public $errorMessage = 'Изменения не сохранены';

    public $ajaxValidation = true;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws CHttpException if the view is invalid
     */
    public function run()
    {
        $this->onBeforeRender($event = new CEvent($this));
        if (!$event->handled) {
            if (empty($_GET['id']))
                throw new CHttpException(404, 'The requested page does not exist.');

            $controller = $this->getController();
            $modelClass = $this->modelClass;

            $model = CActiveRecord::model($modelClass)->findByPk((int)$_GET['id']);

            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            $this->performAjaxValidation($model);

            if (isset($_POST[$modelClass])) {
                $model->attributes = $_POST[$modelClass];
                if ($model->save()) {
                    if ($this->successMessage)
                        Yii::app()->user->setFlash('success', $this->successMessage);

                    if (isset($_REQUEST['returnUrl']))
                        $controller->redirect($_REQUEST['returnUrl']);
                    else
                        $controller->refresh();
                } else if ($this->errorMessage) {
                    Yii::app()->user->setFlash('error', $this->errorMessage);
                }
            } else if (isset($_GET[$modelClass])) {
                $model->attributes = $_GET[$modelClass];
            }

            $controller->render('update', array(
                'model' => $model,
            ));

            $this->onAfterRender(new CEvent($this));
        }
    }

    protected function performAjaxValidation($model)
    {
        if ($this->ajaxValidation && isset($_POST['ajax']) && $_POST['ajax'] === $this->modelClass . '-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}