<?php
class AutoCompleteAction extends CAction
{
    public $queryAttribute;
    public $valueAttribute;
    public $labelAttribute;
    public $limit=10;

    private $_model;

    public function run()
    {
        if(empty($this->queryAttribute) || empty($this->valueAttribute) || empty($this->labelAttribute))
            throw new CException('queryAttribute, valueAttribute, labelAttribute обязательны для заполнения');

        if(!isset($_GET['term']))
            throw new CHttpException(404, 'The requested page does not exist.');

        $term=$_GET['term'];

        $criteria = new CDbCriteria;

        if (strpos($term, ',') === false) {
            $criteria->addSearchCondition($this->queryAttribute, $term);
        } else {
            $terms = array_map("trim", explode(',', $term));
            $criteria->addSearchCondition($this->queryAttribute, array_pop($terms));
            if (count($terms))
                $criteria->addNotInCondition($this->queryAttribute, $terms);
        }

        $criteria->limit = $this->limit;

        $class=$this->model;
        $models = CActiveRecord::model($class)->findAll($criteria);
        $array = array();
        foreach ($models as $model) {
            array_push($array, array(
                'id' => $model->primaryKey,
                'value' => $model->getAttribute($this->valueAttribute),
                'label' => $model->getAttribute($this->labelAttribute),
            ));
        }
        echo function_exists('json_encode') ? json_encode($array) : CJSON::encode($array);
        Yii::app()->end();
    }

    public function setModel($class)
    {
        $this->_model = $class;
    }

    public function getModel()
    {
        if ($this->_model === null) {
            $this->_model = substr(get_class($this->controller), 0, -10);
        }
        return $this->_model;
    }
}