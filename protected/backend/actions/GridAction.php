<?php
abstract class GridAction extends CAction {

    private $_model_class;
    /**
     * Raised right before the action invokes the render method.
     * Event handlers can set the {@link CEvent::handled} property
     * to be true to stop further view rendering.
     * @param CEvent $event event parameter
     */
    public function onBeforeRender($event)
    {
        $this->raiseEvent('onBeforeRender', $event);
    }

    /**
     * Raised right after the action invokes the render method.
     * @param CEvent $event event parameter
     */
    public function onAfterRender($event)
    {
        $this->raiseEvent('onAfterRender', $event);
    }

    public function setModelClass($class) {
        $this->_model_class=$class;
    }

    public function getModelClass() {
        if($this->_model_class===null) {
            $this->_model_class=substr(get_class($this->controller),0,-10);
        }
        return $this->_model_class;
    }
}