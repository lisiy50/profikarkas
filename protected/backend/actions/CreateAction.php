<?php
Yii::import('application.backend.actions.GridAction');

class CreateAction extends GridAction
{
    public $successMessage = 'Запись добавлена';

    public $errorMessage = 'Запись не добавлена';

    public $ajaxValidation = true;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws CHttpException if the view is invalid
     */
    public function run()
    {
        $this->onBeforeRender($event = new CEvent($this));
        if (!$event->handled) {
            $controller = $this->getController();
            $modelClass = $this->modelClass;

            $model = new $modelClass;

            $this->performAjaxValidation($model);

//            if (isset($_POST[$modelClass])) {
//                $model->attributes = $_POST[$modelClass];
//                if ($model->save()) {
//                    if ($this->successMessage)
//                        Yii::app()->user->setFlash('success', $this->successMessage);
//                    if (isset($_REQUEST['returnUrl']))
//                        $controller->redirect($_REQUEST['returnUrl']);
//                    else
//                        $controller->redirect(array('update', 'id' => $model->primaryKey));
//                } else if ($this->errorMessage) {
//                    Yii::app()->user->setFlash('error', $this->errorMessage);
//                }
//            } else if (isset($_GET[$modelClass])) {
//                $model->attributes = $_GET[$modelClass];
//            }

            $controller->render('create', array(
                'model' => $model,
            ));

            $this->onAfterRender(new CEvent($this));
        }
    }

    protected function performAjaxValidation($model)
    {
        if ($this->ajaxValidation && isset($_POST['ajax']) && $_POST['ajax'] === $this->modelClass . '-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}