<?php
Yii::import('application.backend.actions.GridAction');

class ListAction extends GridAction
{

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws CHttpException if the view is invalid
     */
    public function run()
    {
        $controller = $this->getController();

        $this->onBeforeRender($event = new CEvent($this));
        if (!$event->handled) {
            $modelClass = $this->modelClass;

            $model = new $modelClass('search');
            $model->unsetAttributes();
            if (isset($_GET[$modelClass]))
                $model->attributes = $_GET[$modelClass];
            $controller->render('index', array(
                'model' => $model,
            ));

            $this->onAfterRender(new CEvent($this));
        }
    }


}