<?php
class ProjectImageController extends BackendController
{
    public $layout = false;

    public function actionUpload($id)
    {
        $project = Project::model()->findByPk($id);
        if ($project === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        $image = new ProjectImage();
        $image->project_id = $id;

        $prevImage = ProjectImage::model()->order('position DESC')->findByAttributes(array('project_id' => $id));
        if ($prevImage != null) {
            $image->position = $prevImage->position + 1;
        } else {
            $image->position = 1;
        }

        $file = CUploadedFile::getInstanceByName('Image');
        $image->setImageFile($file);
        $image->save();

        $this->renderPartial('/project/_image_view', array(
            'model' => $image,
        ));
        die;
    }

    public function actionSaveOrder($owner = null)
    {
        $class = $this->getImageClass($owner);
        if (isset($_POST['Image_id'])) {
            $images = CActiveRecord::model($class)->findAllByAttributes(array('id' => $_POST['Image_id']));
            foreach ($images as $image) {
                if (isset($_POST['Image'][$image->id]))
                    $image->attributes = $_POST['Image'][$image->id];
                $image->position = array_search($image->id, $_POST['Image_id']);
                $image->save(false);
            }
            Yii::app()->end();
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDelete($id, $owner = null)
    {
        $class = $this->getImageClass($owner);
        $model = CActiveRecord::model($class)->findByPk((int)$id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        $model->delete();
        Yii::app()->end();
    }

    public function getImageClass($owner)
    {
        $class = 'ProjectImage';
        if ($owner && @class_exists($owner . 'Image'))
            $class = $owner . 'Image';

        return $class;
    }
}