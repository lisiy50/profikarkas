<?php

class AdvantageController extends BackendController
{

    public $layout = '//layouts/column1';
    public $icon = 'newspaper';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'index' => array(
                'class' => 'application.backend.actions.ListAction',
            ),
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Преимущество добавлено',
                'errorMessage' => 'Преимущество не добавлено',
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Преимущество &quot;{title}&quot; удалено',
            ),
            'deleteImage' => array(
                'class' => 'application.backend.actions.DeleteImageAction',
            ),
            'autoComplete' => array(
                'class' => 'application.backend.actions.AutoCompleteAction',
                'queryAttribute' => 'title',
                'valueAttribute' => 'title',
                'labelAttribute' => 'title',
            ),
        );
    }

}
