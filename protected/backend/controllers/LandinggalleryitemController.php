<?php


class LandinggalleryitemController extends BackendController
{
    public function actionView($id)
    {
        $items = LandingGalleryItem::model()->findAll(
            'landing_gallery_id=:landing_gallery_id',
            array(':landing_gallery_id' => $id)
        );

        $this->render(
            'view',
            array(
                'items' => $items,
            )
        );
    }

    public function actionLoad()
    {
        if (isset($_POST)) {
            $model = LandingGalleryItem::model()->find('id=:id', [':id' => $_POST['id']]);

            if (! $model) {
                $model = new LandingGalleryItem();
            }

            $model->landing_gallery_id = $_POST['landing_gallery_id'];
            $model->video = $_POST['video'];
            $model->title_ru = $_POST['title_ru'];
            $model->title_ua = $_POST['title_ua'];
            $model->text_ru = $_POST['text_ru'];
            $model->text_ua = $_POST['text_ua'];
            $model->position = $_POST['position'];
            $model->status = $_POST['status'];

            if (isset($_FILES)) {
                $galleryModel = LandingGallery::model()->find('id=:id', [':id' => $model->landing_gallery_id]);
            }

            if (isset($_FILES['image_1_url'])) {
                $img = CUploadedFile::getInstanceByName('image_1_url');
                if ($img) {
                    $imgOptionsArr = [];

                    if ($galleryModel->image_1_width_lg && $galleryModel->image_1_height_lg) {
                        $imgOptionsArr['large'] = ['storage/gallery_item/large', $galleryModel->image_1_width_lg, $galleryModel->image_1_height_lg, 'prefix'=>'large_'];
                    }
                    if ($galleryModel->image_1_width_md && $galleryModel->image_1_height_md) {
                        $imgOptionsArr['medium'] = ['storage/gallery_item/medium', $galleryModel->image_1_width_md, $galleryModel->image_1_height_md, 'prefix'=>'medium_'];
                    }
                    if ($galleryModel->image_1_width_sm && $galleryModel->image_1_height_sm) {
                        $imgOptionsArr['small'] = ['storage/gallery_item/small', $galleryModel->image_1_width_sm, $galleryModel->image_1_height_sm, 'prefix'=>'small_'];
                    }

                    $model->attachBehavior('ImageUploadBehavior', [
                        'class' => 'ImageUploadBehavior',
                        'fileAttribute' => 'image_1_url',
                        'images'=> $imgOptionsArr,
                    ]);
                    $model->setImageFile($img);
                }
            }

            if (isset($_FILES['image_2_url'])) {
                $img2 = CUploadedFile::getInstanceByName('image_2_url');
                if ($img2) {
                    $imgOptionsArr2 = [];

                    if ($galleryModel->image_2_width_lg && $galleryModel->image_2_height_lg) {
                        $imgOptionsArr2['large'] = ['storage/gallery_item_2/large', $galleryModel->image_2_width_lg, $galleryModel->image_2_height_lg, 'prefix'=>'large_'];
                    }
                    if ($galleryModel->image_2_width_md && $galleryModel->image_2_height_md) {
                        $imgOptionsArr2['medium'] = ['storage/gallery_item_2/medium', $galleryModel->image_2_width_md, $galleryModel->image_2_height_md, 'prefix'=>'medium_'];
                    }
                    if ($galleryModel->image_2_width_sm && $galleryModel->image_2_height_sm) {
                        $imgOptionsArr2['small'] = ['storage/gallery_item_2/small', $galleryModel->image_2_width_sm, $galleryModel->image_2_height_sm, 'prefix'=>'small_'];
                    }

                    $model->attachBehavior('ImageUploadBehavior', [
                        'class' => 'ImageUploadBehavior',
                        'fileAttribute' => 'image_2_url',
                        'images'=> $imgOptionsArr2,
                    ]);
                    $model->setImageFile($img2);
                }
            }

            $model->save();
        }
    }

    public function actionAdd()
    {
//        $this->layout = false;
        $landing_gallery_id = $_POST['landing_gallery_id'];
        $count = $_POST['count'];
        $model = new LandingGalleryItem();
        $model->landing_gallery_id = $landing_gallery_id;
        $model->save();


        $this->renderPartial('_form',array(
//        $this->render('_form',array(
            'model'=>$model,
            'count'=>$count,
        ));
    }

    public function actionGet()
    {
//        $this->layout = false;

        $id = $_POST['id'];
        $criteria = new CDbCriteria;
        $criteria->compare('landing_gallery_id', $id);
        $criteria->order = 'position DESC';
        $items = LandingGalleryItem::model()->findAll($criteria);

//        $items = LandingGalleryItem::model()->findAll(
//            'landing_gallery_id=:landing_gallery_id',
//            array(':landing_gallery_id' => $id)
//        );

        $this->renderPartial(
            'view',
            array(
                'items' => $items,
                'id' => $id,
            )
        );
    }

    public function actionDel()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = LandingGalleryItem::model()->find('id=:id', [':id' => $_POST['id']]);
            $model->delete();
        }
    }
}
