<?php


class LandingtextController extends BackendController
{
    public function actionAdd()
    {
//        $this->layout = false;
        $count = $_POST['count'];
        $landing_id = $_POST['id'];
        $model = new LandingText();
        $model->landing_id = $landing_id;
        $model->save();

        $this->renderPartial('_form',array(
            'model'=>$model,
            'count'=>$count,
        ));
    }

    public function actionSave()
    {
        parse_str($_POST['data'], $output);

        $model_id = $output['LandingText']['id'];

        $model = LandingText::model()->find("id=:id", array(":id"=>$model_id));
        if(!$model){
            $model = new LandingText();
        }
        $model->landing_id = $output['LandingText']['landing_id'];
        $model->text_ru = $output['LandingText']['text_ru'];
        $model->text_ua = $output['LandingText']['text_ua'];
        $model->name = $output['LandingText']['name'];
        $model->name_manager = $output['LandingText']['name_manager'];
        $model->save();
        echo $model->id;
    }


    public function actionDel()
    {
        parse_str($_POST['data'], $output);

        $model_id = $output['LandingText']['id'];

        $model = LandingText::model()->find("id=:id", array(":id"=>$model_id));

        $model->delete();
    }
}