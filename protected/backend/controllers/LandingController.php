<?php

class LandingController extends BackendController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Landing;
//		$model_text[] = new LandingText;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Landing']))
		{
			$model->attributes=$_POST['Landing'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
//			'model_text'=>$model_text,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $model_text_all = LandingText::model()->findAll('landing_id=:landing_id', array(':landing_id'=>$id));
        $model_text = [];
        if($model_text_all){
            foreach ($model_text_all as $item){
                $model_text[] = $item;
            }
        }
//        $model_text[] = new LandingText;

        $model_gallery_all = LandingGallery::model()->findAll('landing_id=:landing_id', array(':landing_id'=>$id));
        $model_gallery = [];
        if($model_gallery_all){
            foreach ($model_gallery_all as $item){
                $model_gallery[] = $item;
            }
        }
//        $model_gallery[] = new LandingGallery;

        // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Landing']))
		{
			$model->attributes=$_POST['Landing'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}
//		if(isset($_POST['LandingText']))
//		{
//            $model_text = LandingText::model()->find("id=:id", array(":id"=>$_POST['LandingText']['id']));
//            if(!$model_text ){
//                $model_text = new LandingText;
//            }
//            $model_text->attributes=$_POST['LandingText'];
//
//			if($model_text->save())
//				$this->redirect(array('update','id'=>$model->id));
//		}

		$this->render('update',array(
			'model'=>$model,
            'model_text'=>$model_text,
            'model_gallery'=>$model_gallery,
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id);

        $model_text_all = LandingText::model()->findAll('landing_id=:landing_id', array(':landing_id'=>$id));
        if($model_text_all){
            foreach ($model_text_all as $item){
                $item->delete();
            }
        }

        $model_gallery_all = LandingGallery::model()->findAll('landing_id=:landing_id', array(':landing_id'=>$id));
        if($model_gallery_all){
            foreach ($model_gallery_all as $item){
                $item->delete();
            }
        }

        $this->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Landing');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Landing('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Landing']))
			$model->attributes=$_GET['Landing'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Landing the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Landing::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Landing $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='landing-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
