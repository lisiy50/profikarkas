<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 3/1/16
 * Time: 5:27 PM
 */

require_once(Yii::getPathofAlias('application.lib.phpQuery.phpQuery').'/phpQuery.php');

class ParseVserabotnikiController extends BackendController {

    public $layout = '//layouts/column1';

    public $icon = 'page';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }


    public function actionGetPriceIndex()
    {
        set_time_limit(0);

        $html = file_get_contents('http://www.vserabotniki.com/ua/price/');

        $dom = phpQuery::newDocument($html);

        $rootCategories = $dom->find('#price .prow');

        foreach ($rootCategories as $rootCategory) {
            $pq = pq($rootCategory);

            $rootCategoryName = $pq->find('>h2 a')->text();

            $categories = $pq->find('ul.pods >li >a');

            foreach ($categories as $el) {
                $pq = pq($el);
                $name = $pq->text();
                $href = $pq->attr('href');
                $parsingCategory = ParsingCategory::model()->findByAttributes(array('link' => $href));
                if (!$parsingCategory) {
                    $parsingCategory = new ParsingCategory();
                    $parsingCategory->parent_name = $rootCategoryName;
                    $parsingCategory->link = $href;
                    $parsingCategory->name = $name;
                    $parsingCategory->save();
                }
            }
        }

        $this->render('getPriceIndex');


    }

    public function actionGetNumberOfPages()
    {
        set_time_limit(0);

        foreach (ParsingCategory::model()->findAll() as $parsingCategory) {
            /* @var ParsingCategory $parsingCategory */

            $html = file_get_contents($parsingCategory->getSiteLink());

            $dom = phpQuery::newDocument($html);

            $dom->find('div.pagination a:last')->remove();
            $pagecount = $dom->find('div.pagination a:last');
            foreach ($pagecount as $pp) {
                $pq = pq($pp);
                preg_match('#\d+$#', $pq->attr('href'), $matches);
                if (!empty($matches)) {
                    $parsingCategory->number_of_pages = $matches[0];
                    $parsingCategory->save();
                }
            }
        }

        $this->render('getNumberOfPages');
    }

    public function actionGetPagesHtml()
    {
        set_time_limit(0);

        foreach (ParsingCategory::model()->findAll() as $parsingCategory) {
            /* @var ParsingCategory $parsingCategory */

            echo $parsingCategory->name . ' ' . $parsingCategory->getSiteLink();


            $i = 1;
            do {
                $pageLink = $parsingCategory->getSiteLink().'?page='.$i;
                echo '-'.$i.'-';
                $html = file_get_contents($pageLink);

                $parsingPagesHtml = ParsingPagesHtml::model()->findByAttributes(array('category_id' => $parsingCategory->id, 'page' => $i));
                if (!$parsingPagesHtml) {
                    $parsingPagesHtml = new ParsingPagesHtml();
                    $parsingPagesHtml->category_id = $parsingCategory->id;
                    $parsingPagesHtml->page = $i;

                }
                $parsingPagesHtml->html = $html;
                $parsingPagesHtml->save();

                flush();
                ob_flush();

                $i++;
            } while ($i <= $parsingCategory->number_of_pages);


            echo '<br>';
        }
    }

    public function actionParseData()
    {
        set_time_limit(0);

        foreach (ParsingCategory::model()->findAll() as $parsingCategory) {
            /* @var ParsingCategory $parsingCategory */

            while ($parsingPagesHtml = ParsingPagesHtml::model()->findByAttributes(array('parsed'=>0))) {
                /* @var ParsingPagesHtml $parsingPagesHtml */

                echo $parsingPagesHtml->id;

                $html = $parsingPagesHtml->html;

                $dom = phpQuery::newDocument($html);

                $items = $dom->find('ul.pricelist >li');

                foreach ($items as $item) {
                    $pq = pq($item);
                    $pq->find('div.inf a.pro')->remove();

                    $pro = $pq->attr('class');

                    $name = $pq->find('div.inf h3 a')->text();

                    preg_match('#^\S+#', $pq->find('div.inf h3 a')->text(), $matches);
                    $type = $matches[0];
                    $link = $pq->find('div.inf h3 a')->attr('href');

                    preg_match('#\d+$#', $pq->find('div.rating')->attr('class'), $matches);
                    $rating = empty($matches) ? null : $matches[0];

                    $pq->find('div.contact >div')->remove();
                    $city = trim(str_replace('г.', '', $pq->find('div.contact')->text()));

                    if ($pq->find('div.mean')->length) {
                        $priceType = 'normal';
                        $priceComment = $pq->find('div.mean small')->text();
                        $pq->find('div.mean small')->remove();
                        if ($pq->find('div.mean div')->length) {
                            $price = 0;
                        } else {
                            $price = trim($pq->find('div.mean')->text());
                        }
                    } else/*if ($pq->find('div.pmean'))*/ {
                        $priceType = 'grey';
                        $priceComment = $pq->find('div.pmean small')->text();
                        $pq->find('div.pmean small')->remove();
                        if ($pq->find('div.pmean div')->length) {
                            $price = 0;
                        } else {
                            $price = trim($pq->find('div.pmean')->text());
                        }
                    }

                    $parsingPrice = ParsingPrice::model()->findByAttributes(['link' => $link, 'category_id' => $parsingPagesHtml->category_id]);
                    if (!$parsingPrice) {
                        $parsingPrice = new ParsingPrice();
                    }
                    $parsingPrice->page_id = $parsingPagesHtml->id;
                    $parsingPrice->category_id = $parsingPagesHtml->category_id;
                    $parsingPrice->type = $type;
                    $parsingPrice->name = $name;
                    $parsingPrice->pro = $pro;
                    $parsingPrice->rating = $rating;
                    $parsingPrice->city = $city;
                    $parsingPrice->price = $price;
                    $parsingPrice->price_type = $priceType;
                    $parsingPrice->price_comment = $priceComment;
                    $parsingPrice->date = null; //пока не понял что это такое.
                    $parsingPrice->link = $link;
                    if (!$parsingPrice->save()) {
                        echo '<span style="color: #f00;">';
                        echo $parsingPrice->link;
                        echo ' - ';
                        var_dump($parsingPrice->errors);
                        echo '</span>';
                    }
                }

                $parsingPagesHtml->parsed = 1;
                $parsingPagesHtml->save();

                flush();
                ob_flush();

                echo '<br>';
//                exit;
            }

        }
    }

    public function actionExportCsv()
    {
        set_time_limit(0);

        $FileName = 'storage/export-'.date('Y.m.d-H.i.s').'.csv';

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($FileName));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        //header('Content-Length: ' . filesize($FileName));

        echo iconv ('utf-8', 'cp1251', "категория;тип;название;Про;рейтинг;город;цена;тип цены;коммент к цене;ссылка;дата\r\n");

        $offset = 0;
        $result = false;
        do {
            $result = Yii::app()->db->createCommand()->select('pc.name as category_name, pp.type, pp.name, pp.pro, pp.rating, pp.city, pp.price, pp.price_type, pp.price_comment, pp.link, pp.update_time')->join('{{parsing_category}} pc', 'pp.category_id = pc.id')->from('{{parsing_price}} pp')->order('id')->limit(1000)->offset($offset)->queryAll();


            foreach ($result as $row){
                echo iconv ('utf-8', 'cp1251', $row['category_name'].';'.$row['type'].';'.$row['name'].';'.$row['pro'].';'.$row['rating'].';'.$row['city'].';'.$row['price'].';'.$row['price_type'].';'.$row['price_comment'].';http://www.vserabotniki.com'.$row['link'].';'.date('dd/mm/Y', $row['update_time'])."\r\n");
            }

            $offset += 1000;
        } while ($result);


    }

}
