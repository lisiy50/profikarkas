<?php
class RedactorController extends BackendController {

    public $layout = false;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('moderator')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actionFileUpload() {
        $dir = Yii::app()->basePath.'/../storage/files/';
        $file=CUploadedFile::getInstanceByName('file');

        if($file) {
            $name=md5(date('YmdHis')).'.'.$file->extensionName;
            $file->saveAs($dir.$name);

            $array = array(
                'filelink' => Yii::app()->baseUrl.'/storage/files/'.$name,
                'filename' => $_FILES['file']['name']
            );

            echo CJSON::encode($array);
        }
        exit;
    }

    public function actionImageUpload() {
        $dir = Yii::app()->basePath.'/../storage/images/';
        $file=CUploadedFile::getInstanceByName('file');

        if($file) {
            $name=md5(date('YmdHis')).'.'.$file->extensionName;
//            $file->saveAs($dir.$name);

            /*add watermark*/
            /* @var Image $image */
            $image=Yii::app()->image->load($file->tempName);
            if($image->width > 1000)
                $image->resize(1000, 3000, Image::AUTO);
            $watermarkPath = Yii::app()->basePath.'/../storage/watermark.png';
            $watermark=Yii::app()->image->load($watermarkPath);
            $watermark->resize(ceil($image->width*0.95), ceil($image->height*0.95), Image::AUTO);
            $offsetY = -round($image->height/2-$watermark->height/2);
            $offsetX = -round($image->width/2-$watermark->width/2);
            $image->watermark($watermark, $offsetX, $offsetY);
            $image->save($dir.$name, 100);
            /* end add watermark*/

            $array = array(
                'filelink' => Yii::app()->baseUrl.'/storage/images/'.$name
            );

            echo CJSON::encode($array);
        }
        exit;
    }

    public function actionImageGetJson() {
        $array=array();
        $files=scandir(Yii::app()->basePath.'/../storage/images/');
        foreach($files as $file) {
            if($file=='.' || $file=='..') {
                continue;
            }

            array_push($array, array(
                'thumb'=>Yii::app()->baseUrl.'/storage/images/'.$file,
                'image'=>Yii::app()->baseUrl.'/storage/images/'.$file,
            ));
        }
        echo CJSON::encode($array);
        exit;
    }

}