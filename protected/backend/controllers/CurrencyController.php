<?php

class CurrencyController extends BackendController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column1';
    public $icon='money';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('manager')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actionCreate() {
        $model = new Currency;

        if (isset($_POST['Currency'])) {
            $model->attributes = $_POST['Currency'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Валюта добавлена');
                $this->redirect(array('index'));
            } else if ($this->errorMessage) {
                Yii::app()->user->setFlash('error', 'Валюта не добавлена');
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            if (Yii::app()->config['currency_default'] == $model->id || Yii::app()->config['currency_basic'] == $model->id) {
                Yii::app()->user->setFlash('error', "Валюта &quot;{$model->name}&quot; не может быть удалена");
            } else {
                Yii::app()->user->setFlash('success', "Валюта &quot;{$model->name}&quot; удалена");
                $model->delete();
            }

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $currencies = Currency::model()->findAll();
        if (isset($_POST['Currency'])) {
            $valid = true;
            foreach ($currencies as $currency) {
                $i = $currency->id;
                if (isset($_POST['Currency'][$i])) {
                    $currency->position = array_search($i, array_keys($_POST['Currency']));
                    $currency->attributes = $_POST['Currency'][$i];
                }
                $valid = $currency->validate() && $valid;
            }
            if ($valid) {
                foreach ($currencies as $currency)
                    $currency->save();
                Yii::app()->user->setFlash('success', "Изменения сохранены");
                $this->refresh();
            } else {
                Yii::app()->user->setFlash('error', "Изменения не сохранены");
            }
        }
        $this->render('batchUpdate', array(
            'currencies' => $currencies,
        ));
    }

    public function loadModel($id)
    {
        $model = Currency::model()->findByPk((int)$id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}
