<?php

class PhotoGalleryController extends BackendController
{
    public $layout = '//layouts/column1';

    public $icon = 'picture';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('content')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'index' => array(
                'class' => 'application.backend.actions.ListAction',
            ),
            'create' => array(
                'class' => 'application.backend.actions.CreateAction',
                'successMessage' => 'Галерея изображений добавлена',
                'errorMessage' => 'Галерея изображений добавлена',
            ),
            'update' => array(
                'class' => 'application.backend.actions.UpdateAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Галерея изображений &quot;{name}&quot; удалена',
            ),
        );
    }

}
