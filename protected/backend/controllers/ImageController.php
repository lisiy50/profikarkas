<?php

class ImageController extends BackendController
{

    public function actionLoad()
    {
        $model_id = $_POST['model_id'];
        $model_name = $_POST['model_name'];

        if (isset($_POST['my_file_upload'])) {
            $files = $_FILES; // полученные файлы

            $done_files = array();

            foreach ($files as $key => $file) {
                $file_name = $file['name'];
                // тестовая загрузка
//                if (move_uploaded_file($file['tmp_name'], Yii::app()->basePath.'/gallery/'.$file_name)) {
//                    $done_files[] = realpath(Yii::app()->basePath.'/gallery/'.$file_name);
//                }
                $done_files[] = realpath(Yii::app()->basePath.'/gallery/'.$file_name);// со временем убрать

                $image_model_name = $model_name."Image"; // правильная модель картинок
                $image_model = new $image_model_name();

                $img = CUploadedFile::getInstanceByName($key); // полученные файлы
                $image_model->setImageFile($img);
                if($model_name=="Project"){
                    $image_model->project_id = $model_id;
                    $image_model->type = 0;
                    $position = Yii::app()->db->createCommand()
                    ->select('position')
                    ->from("tbl_project_image")
                    ->where('project_id=:id', array(':id'=>$model_id))
                    ->order('position  desc')
                    ->queryRow();
                }else{
                    $image_model->owner_model = $model_name;
                    $image_model->owner_id = $model_id;
                    $position = Yii::app()->db->createCommand()
                    ->select('position')
                    ->from("tbl_extra_image")
                    ->where('owner_id=:id', array(':id'=>$model_id))
                    ->order('position  desc')
                    ->queryRow();
                }
                $image_model->position = $position['position']+1;
                $image_model->save();

            }

            $data = $done_files ? array('files' => $done_files) : array('error' => 'Ошибка загрузки файлов.');

            die(json_encode($data));
        }
    }

    public function actionImg()
    {
        $model_id = $_POST['id'];
        $model_name = $_POST['name'];
        $model_url = $_POST['url'];
        $model = $model_name::model()->findAll("id=:id", array(":id"=>$model_id));

        echo'<table id="sortable-images" class="table table-striped"><tbody>';
        foreach ($model[0]->images as $image){
            $this->renderPartial("$model_url", array('model' => $image));
        }

        echo" </tbody></table>";
    }

    // кнопка
//<div class='row'>
//<div class='pull-left' style='width: 170px;margin-left: 20px;'>
//<a class='btn btn-success btn-small' id='save-images' href='#'>Сохранить порядок/текст</a>
//</div>
//</div>
}