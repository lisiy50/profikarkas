<?php
class ExtraImageController extends BackendController
{
    public $layout = false;

    public function actionUpload($id, $owner)
    {
        $owner_model = CActiveRecord::model($owner)->findByPk($id);
        if ($owner_model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        $class = $this->getImageClass($owner);
        $image = new $class;
        $image->owner_id=$id;
        $image->owner_model=$owner;

        $prevImage = ExtraImage::model()->order('position DESC')->findByAttributes(array('owner_id' => $id, 'owner_model' => $owner));
        if ($prevImage != null) {
            $image->position = $prevImage->position + 1;
        }

        $image->detachBehavior('ImageUploadBehavior');
        $imageUploadBehavior = $image->behaviors();
        $imageUploadBehavior = $imageUploadBehavior['ImageUploadBehavior'];
        $image->attachBehavior('ImageUploadBehavior', $imageUploadBehavior);

        $image->setImageFile(CUploadedFile::getInstanceByName('Image'));
        $image->save();

        $this->renderPartial('_image_view', array(
            'model' => $image,
        ));
        die;
    }

    public function actionSaveOrder($owner = null)
    {
        $class = $this->getImageClass($owner);
        if (isset($_POST['Image_id'])) {
            $images = CActiveRecord::model($class)->findAllByAttributes(array('id' => $_POST['Image_id']));
            foreach ($images as $image) {
                if (isset($_POST['Image'][$image->id]))
                    $image->attributes = $_POST['Image'][$image->id];
                $image->position = array_search($image->id, $_POST['Image_id']);
                $image->save(false);
            }
            Yii::app()->end();
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionDelete($id, $owner = null)
    {
        $class = $this->getImageClass($owner);
        $model = CActiveRecord::model($class)->findByPk((int)$id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        $model->delete();
        Yii::app()->end();
    }

    public function getImageClass($owner)
    {
        $class = 'ExtraImage';
        if ($owner && @class_exists($owner . 'Image'))
            $class = $owner . 'Image';

        return $class;
    }
}