<?php
class SeoController extends BackendController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='/layouts/column1';

    public $icon = 'chart_up_color';

    public $routes = array(
        'site/index'=>'Главная страница',
        'portfolio/index'=>'Портфолио',
        'portfolio/view'=>'Портфолио подробнее',
        'portfolio/construction'=>'Сейчас строятся',
        'review/index'=>'Отзывы пользователей',
        'review/view'=>'Отзывы пользователей подробнее',
        'buildInEurope/index'=>'Так строят в Европе',
        'buildInEurope/view'=>'Так строят в Европе подробнее',
        'equipHouses/index' => 'Оснащение дома',
        'project/index' => 'Каталог проектов',
        'project/view' => 'Подробнее проекта',

        'project/dachi'=>'Категория дачи',
        'project/zagorodnie'=>'Категория загородные дома',

        'faq/index' => 'Частозадаваемые вопросы',
        'usefulArticle/index' => 'Полезные статьи',
        'press/index' => 'Мы в прессе',
        'usefulArticle/view' => 'Полезные статьи подробнее',
        'homeKit/index' => 'Стоимость домокоплектов',
        'photoGallery/index' => 'Фотогалерея',
        'photoGallery/view' => 'Фотогалерея подробнее',
        'staff/index' => 'Персонал',

        'gallery/view'=>'Страница галереи',
        'news/view'=>'Страница новости',
        'article/view'=>'Статья',
        'news/archive'=>'Архив новостей',
        'site/contact'=>'Контакты',
    );

    private $_list;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles'=>array('content')),
            array('deny', 'users'=>array('*')),
        );
    }

    public function actionIndex() {
        if(isset($_POST['SEO'])) {
            foreach($this->routes as $route=>$title) {
                $key=str_replace('/','-',$route);
                $model=$this->getByRoute($route);

                if(empty($_POST['SEO'][$key])) {
                    if(!$model->isNewRecord) {
                        $model->delete();
                    }
                    continue;
                }
                $data=$_POST['SEO'][$key];

                $model->attributes=$data;

                if(empty($data['metaTitle']) && empty($data['metaKeywords']) && empty($data['metaDescription']) && !$model->isNewRecord) {
                    $model->delete();
                } else {
                    $model->save();
                }
            }
            Yii::app()->user->setFlash('success', 'Изменение сохранены');
        }

        $this->render('index');
    }

    public function getByRoute($route) {
        if($this->_list==null)
            $this->loadModels();

        if(!isset($this->_list[$route])) {
            $model=new SEO;
            $model->route=$route;
            $this->_list[$route]=$model;
        }

        return $this->_list[$route];
    }

    public function loadModels() {
        foreach(SEO::model()->findAll('entity=0') as $model)
            $this->_list[$model->route]=$model;

        return $this->_list;
    }
}