<?php


class LandinggalleryController extends BackendController
{
    public function actionAdd()
    {
//        $this->layout = false;
        $count = $_POST['count'];
        $landing_id = $_POST['id'];
        $model = new LandingGallery();
        $model->landing_id = $landing_id;
        $model->save();

        $this->renderPartial('_form',array(
            'model'=>$model,
            'count'=>$count,
        ));
    }

    public function actionSave()
    {
        parse_str($_POST['data'], $output);

        $model_id = $output['LandingGallery']['id'];

        $model = LandingGallery::model()->find("id=:id", array(":id"=>$model_id));
        if(!$model){
            $model = new LandingGallery();
        }
        $model->landing_id = $output['LandingGallery']['landing_id'];
        $model->name = $output['LandingGallery']['name'];
        $model->name_manager = $output['LandingGallery']['name_manager'];
        $model->image_1_width_lg = (int) $output['LandingGallery']['image_1_width_lg'];
        $model->image_1_height_lg = (int) $output['LandingGallery']['image_1_height_lg'];
        $model->image_1_width_md = (int) $output['LandingGallery']['image_1_width_md'];
        $model->image_1_height_md = (int) $output['LandingGallery']['image_1_height_md'];
        $model->image_1_width_sm = (int) $output['LandingGallery']['image_1_width_sm'];
        $model->image_1_height_sm = (int) $output['LandingGallery']['image_1_height_sm'];
        $model->image_2_width_lg = (int) $output['LandingGallery']['image_2_width_lg'];
        $model->image_2_height_lg = (int) $output['LandingGallery']['image_2_height_lg'];
        $model->image_2_width_md = (int) $output['LandingGallery']['image_2_width_md'];
        $model->image_2_height_md = (int) $output['LandingGallery']['image_2_height_md'];
        $model->image_2_width_sm = (int) $output['LandingGallery']['image_2_width_sm'];
        $model->image_2_height_sm = (int) $output['LandingGallery']['image_2_height_sm'];
        $model->save();
        echo $model->id;
    }
//
//
//    public function actionDel()
//    {
//        parse_str($_POST['data'], $output);
//
//        $model_id = $output['LandingText']['id'];
//
//        $model = LandingText::model()->find("id=:id", array(":id"=>$model_id));
//
//        $model->delete();
//    }
}
