<?php

class UserController extends BackendController
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column1';
    public $icon = 'user';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('admin')),
            array('deny', 'users' => array('*')),
        );
    }

    public function actions()
    {
        return array(
            'index' => array(
                'class' => 'application.backend.actions.ListAction',
            ),
            'delete' => array(
                'class' => 'application.backend.actions.DeleteAction',
                'successMessage' => 'Пользователь &quot;{username}&quot; удален',
            ),
        );
    }

    public function actionCreate() {
        $model = new User('register');

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Пользователь добавлен');
                $this->redirect(array('update', 'id' => $model->id));
            } else {
                Yii::app()->user->setFlash('error', 'Пользователь не добавлен');
            }
        } else if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (!empty($_POST['changePassword']))
            $model->scenario = 'changePassword';

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Изменения сохранены");
                $this->redirect(array('update', 'id' => $model->id));
            } else {
                Yii::app()->user->setFlash('error', "Изменения не сохранены");
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCsv()
    {
        set_time_limit(0);

        $FileName = 'storage/export-users-'.date('Y.m.d-H.i.s').'.csv';

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream; charset=cp1251');
        header('Content-Disposition: attachment; filename=' . basename($FileName));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        //header('Content-Length: ' . filesize($FileName));

        echo iconv ('utf-8', 'cp1251', "Имя пользователя;Роль;ФИО;Email;Телефон;Адрес;Коммент;Включен\r\n");

        $request = Yii::app()->db->createCommand()->from('{{user}}')->order('id DESC');
        if (isset($_GET['User']['role'])) {
            $request->where('role = :role', array(':role' => $_GET['User']['role']));
        }
        $result = $request->queryAll();
        foreach ($result as $row){
            echo iconv ('utf-8', 'cp1251', $row['username'].';'.$row['role'].';'.$row['fio'].';'.$row['email'].';'.$row['phone'].';'.$row['address'].';'.$row['comment'].';'.$row['status']."\r\n");
        }
    }

    public function loadModel($id)
    {
        $model = User::model()->findByPk((int)$id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === get_class($model).'-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
