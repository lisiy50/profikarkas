<?php
return array(
//    'Контент' => array('url'=>'#', 'icon'=>'user', 'items'=>array(
//    )),

    'Дополнения' => array('url'=>'#', 'icon'=>'book', 'items'=>array(
        'Оснащение дома' => array('url'=>array('equipHouses/index')),
        'Персонал' => array('url'=>array('staff/index')),
        'Вопросы и ответы' => array('url'=>array('faq/index')),
        'Часто задаваемые вопросы' => array('url'=>array('faqItem/index')),
        'Крос ссылки' => array('url'=>array('crossLink/index')),
        'Так строят в Европе' => array('url'=>array('buildInEurope/index')),
        'Полезные статьи / Мы в прессе' => array('url'=>array('usefulArticle/index')),
        'Преимущества' => array('url'=>array('advantage/index')),
        'Отзывы поставщиков' => array('url'=>array('reviewSupplier/index')),
        'Галереи изображений' => array('url'=>array('photoGallery/index')),
        'Галереи изображений (наше производство)' => array('url'=>array('gallery/index')),
        'Видеопрогулки' => array('url'=>array('videoWalk/index')),
    )),

    'Лендинги' => ['url' => ['landing/admin'], 'icon' => 'file'],

    'Проекты' => array('url'=>array('project/index'), 'icon'=>'home', 'items'=>array(
        'Проекты' => array('url'=>array('project/index')),
        'Домокомплект' => array('url'=>array('homeKit/index')),
        'Комплектация домокомплекта' => array('url'=>array('packagingHomeKit/index')),
        'Объемы работ' => array('url'=>array('workScope/index')),
        'Импорт цен' => array('url'=>array('project/importPrices')),
    )),

    'Пользователи' => array('url'=>'#', 'icon'=>'user', 'items'=>array(
        'Клиенты' => array('url'=>array('user/index', 'User'=>array('role'=>User::ROLE_CLIENT))),
        'Менеджеры' => array('url'=>array('user/index', 'User'=>array('role'=>User::ROLE_MANAGER))),
        'Контент менеджеры' => array('url'=>array('user/index', 'User'=>array('role'=>User::ROLE_CONTENT))),
        'Администраторы' => array('url'=>array('user/index', 'User'=>array('role'=>User::ROLE_ADMIN))),
        'Группы' => array('url'=>array('group/index')),
    )),

    'Настройки' => array('order'=>7, 'url'=>'#', 'icon'=>'cog',  'items'=>array(
        'Основные' => array('url'=>array('config/index', 'section'=>'basic')),
        'Контент' => array('url'=>array('config/index', 'section'=>'content')),
        'Страницы' => array('url'=>array('config/index', 'section'=>'pages')),
        'SEO информация' => array('url'=>array('seo/index')),
        'Интеграция' => array('url'=>array('config/index', 'section'=>'integration')),
        'Проекты' => array('url'=>array('config/index', 'section'=>'project')),
        'Валюта' => array('url'=>array('currency/index')),
    )),

//    'Doc' => array('url'=>array('doc/index'), 'icon'=>'book'),
);
