<?php
class EHtmlPurifier extends CHtmlPurifier {

    public $options=array(
        'HTML.AllowedElements'=>array("div", "thead","table","tr","td","th","hr","strike","p","ul","ol","li","h1","h2","h3","h4","h5","h6","img","a","b","i","s","u","blockquote","sup","sub","pre","br","iframe","span"),
        'HTML.AllowedAttributes'=>array("*.id","*.src","img.alt","a.href","*.title","a.target","*.width","*.height","iframe.frameborder","iframe.allowfullscreen","iframe.src","*.style","*.class"),
        'Attr.AllowedFrameTargets'=>array("_blank"),
        'HTML.SafeIframe'=>true,
        'URI.SafeIframeRegexp'=>'%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%',
        'Attr.EnableID'=>true,
    );

}