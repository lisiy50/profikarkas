<?php 
/**
 * Swift Mailer wrapper class.
 *
 * @author Martin Nilsson <martin.nilsson@haxtech.se>
 * @link http://www.haxtech.se
 * @copyright Copyright 2010 Haxtech
 * @license GNU GPL
 */
class SwiftMailer extends CApplicationComponent {

	public function init() {
        $path=Yii::getPathOfAlias('application.lib.swift.lib');
        define('SWIFT_REQUIRED_LOADED', true);
        require_once $path.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'Swift.php';
        Swift::$initPath=null;
        Yii::registerAutoloader(array('Swift', 'autoload'), false);
        require_once $path.DIRECTORY_SEPARATOR.'swift_init.php';
	}

    public function newMessage($to, $subject, $message, $options=array()) {
        if(empty($options['from']))
            $options['from']=isset($_SERVER['HTTP_HOST'])?'noreply@'.$_SERVER['HTTP_HOST']:'noreply@'.$_SERVER['SERVER_NAME'];

        if(empty($options['contentType']))
            $options['contentType']='text/plain';

        return Swift_Message::newInstance($subject)
            ->setFrom($options['from'])
            ->setTo($to)
            ->setBody($message, $options['contentType']);
    }

    public function newTemplateMessage($to, $template, $data, $options=array()) {
        $model=EmailTemplate::model()->findByPk($template);
        if($model===null)
            throw new CException('Шаблон письма "'.$template.'" отсутствует');

        $subject=Yii::app()->twig->render($model->subject, $data);
        $message=Yii::app()->twig->render($model->body, $data);

        if(empty($options['contentType']))
            $options['contentType']='text/html';

        return $this->newMessage($to, $subject, $message, $options);
    }

    public function send(Swift_Message $message) {
        $transport=Swift_MailTransport::newInstance();
        $mailer=Swift_Mailer::newInstance($transport);

        $mailer->send($message);
    }
	
}