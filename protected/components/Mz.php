<?php
class Mz {

    const PACKAGE_CAROUFREDSEL='carouFredSel';
    const PACKAGE_FANCYBOX='fancybox';
    const PACKAGE_KNOCKOUT='knockout';
    const PACKAGE_ELEVATE_ZOOM='elevateZoom';

    public static function currency($id, $column=false) {
        if($column=='short') {
            $suffix=Yii::app()->currency->get($id, 'suffix');
            $prefix=Yii::app()->currency->get($id, 'prefix');
            return $suffix?$suffix:$prefix;
        } else {
            return Yii::app()->currency->get($id, $column);
        }
    }

    public static function date($format, $time=null) {
        if($time) {
            return Yii::app()->dateFormatter->format($format, $time);
        } else {
            return Yii::app()->dateFormatter->format('dd MMMM y', $format);
        }
    }

    public static function price($arg1, $arg2=false, $arg3=false) {
        if(is_numeric($arg1)) {
            $template='{prefix}{price}{suffix}';
            $price=(float)$arg1;
            $params=$arg2;
        } else {
            $template=$arg1;
            $price=(float)$arg2;
            $params=$arg3;
        }
        return Yii::app()->priceFormatter->templateFormat($template, $price, $params);
    }

    public static function package($name, $js=null) {
        Yii::app()->getClientScript()->registerPackage($name);
        if($js) self::script($js);
    }

    public static function url($route,$params=array(),$ampersand='&') {
        return Yii::app()->createUrl($route,$params,$ampersand);
    }

    public static function styleFile($name) {
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl.'/css/'.$name);
    }

    public static function script($js) {
        $id=md5($js);
        Yii::app()->getClientScript()->registerScript($id, $js);
    }

    public static function scriptFile($name) {
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/'.$name);
    }

}