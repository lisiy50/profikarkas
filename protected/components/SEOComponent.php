<?php
class SEOComponent extends CComponent {

    public $tableName='{{seo}}';

    protected $_params=array();
    protected $_metaTitle;
    protected $_metaTitle_uk;
    protected $_metaTitle_en;
    protected $_metaTitle_de;
    protected $_metaTitle_fr;
    protected $_metaKeywords;
    protected $_metaDescription;
    protected $_metaDescription_uk;
    protected $_metaDescription_en;
    protected $_metaDescription_de;
    protected $_metaDescription_fr;
    protected $_default=array(
        'title'=>'',
        'title_uk'=>'',
        'title_en'=>'',
        'title_de'=>'',
        'title_fr'=>'',
        'description'=>'',
        'description_uk'=>'',
        'description_en'=>'',
        'description_de'=>'',
        'description_fr'=>'',
        'keywords'=>'',
    );

    protected $_route;

    public function init() {
        if (!$this->_route)
            $this->_route = Yii::app()->controller->route;
        $entity=array_key_exists('id', $_GET)?$_GET['id']:0;
        $row=Yii::app()->db->createCommand()
            ->select('metaTitle,metaTitle_uk,metaTitle_en,metaTitle_de,metaTitle_fr,metaKeywords,metaDescription,metaDescription_uk,metaDescription_en,metaDescription_de,metaDescription_fr')
            ->from($this->tableName)
            ->where('route=? AND (entity=? OR entity=0)', array($this->_route, $entity))
            ->order('entity DESC')
            ->queryRow();


        $default=Yii::app()->db->createCommand()
            ->select('metaTitle,metaTitle_uk,metaTitle_en,metaTitle_de,metaTitle_fr,metaKeywords,metaDescription,metaDescription_uk,metaDescription_en,metaDescription_de,metaDescription_fr')
            ->from($this->tableName)
            ->where('route=? AND (entity=? OR entity=0)', array($this->_route, 0))
            ->order('entity DESC')
            ->queryRow();

        if($this->_metaTitle===null)
            $this->_metaTitle=$row['metaTitle'] ? $row['metaTitle'] : $default['metaTitle'];

        if($this->_metaTitle_uk===null)
            $this->_metaTitle_uk=$row['metaTitle_uk'] ? $row['metaTitle_uk'] : $default['metaTitle_uk'];

        if($this->_metaTitle_en===null)
            $this->_metaTitle_en=$row['metaTitle_en'] ? $row['metaTitle_en'] : $default['metaTitle_en'];

        if($this->_metaTitle_de===null)
            $this->_metaTitle_de=$row['metaTitle_de'] ? $row['metaTitle_de'] : $default['metaTitle_de'];

        if($this->_metaTitle_fr===null)
            $this->_metaTitle_fr=$row['metaTitle_fr'] ? $row['metaTitle_fr'] : $default['metaTitle_fr'];

        if($this->_metaKeywords===null)
            $this->_metaKeywords=$row['metaKeywords'] ? $row['metaKeywords'] : $default['metaKeywords'];

        if($this->_metaDescription===null)
            $this->_metaDescription=$row['metaDescription'] ? $row['metaDescription'] : $default['metaDescription'];

        if($this->_metaDescription_uk===null)
            $this->_metaDescription_uk=$row['metaDescription_uk'] ? $row['metaDescription_uk'] : $default['metaDescription_uk'];

        if($this->_metaDescription_en===null)
            $this->_metaDescription_en=$row['metaDescription_en'] ? $row['metaDescription_en'] : $default['metaDescription_en'];

        if($this->_metaDescription_de===null)
            $this->_metaDescription_de=$row['metaDescription_de'] ? $row['metaDescription_de'] : $default['metaDescription_de'];

        if($this->_metaDescription_fr===null)
            $this->_metaDescription_fr=$row['metaDescription_fr'] ? $row['metaDescription_fr'] : $default['metaDescription_fr'];
    }

    public function getMetaTitle() {
        $title=empty($this->_metaTitle)?$this->_default['title']:$this->_metaTitle;
        return CHtml::encode(strtr($title, $this->Params));
    }

    public function getMetaTitle_uk() {
        $title=empty($this->_metaTitle_uk)?$this->_default['title_uk']:$this->_metaTitle_uk;
        return CHtml::encode(strtr($title, $this->Params));
    }

    public function getMetaTitle_en() {
        $title=empty($this->_metaTitle_en)?$this->_default['title_en']:$this->_metaTitle_en;
        return CHtml::encode(strtr($title, $this->Params));
    }

    public function getMetaTitle_de() {
        $title=empty($this->_metaTitle_de)?$this->_default['title_de']:$this->_metaTitle_de;
        return CHtml::encode(strtr($title, $this->Params));
    }

    public function getMetaTitle_fr() {
        $title=empty($this->_metaTitle_fr)?$this->_default['title_fr']:$this->_metaTitle_fr;
        return CHtml::encode(strtr($title, $this->Params));
    }

    public function getMetaKeywords() {
        $keywords=empty($this->_metaKeywords)?$this->_default['keywords']:$this->_metaKeywords;
        return CHtml::encode(strtr($keywords, $this->Params));
    }

    public function getMetaDescription() {
        $description=empty($this->_metaDescription)?$this->_default['description']:$this->_metaDescription;
        return CHtml::encode(strtr($description, $this->Params));
    }

    public function getMetaDescription_uk() {
        $description=empty($this->_metaDescription_uk)?$this->_default['description_uk']:$this->_metaDescription_uk;
        return CHtml::encode(strtr($description, $this->Params));
    }

    public function getMetaDescription_en() {
        $description=empty($this->_metaDescription_en)?$this->_default['description_en']:$this->_metaDescription_en;
        return CHtml::encode(strtr($description, $this->Params));
    }

    public function getMetaDescription_de() {
        $description=empty($this->_metaDescription_de)?$this->_default['description_de']:$this->_metaDescription_de;
        return CHtml::encode(strtr($description, $this->Params));
    }

    public function getMetaDescription_fr() {
        $description=empty($this->_metaDescription_fr)?$this->_default['description_fr']:$this->_metaDescription_fr;
        return CHtml::encode(strtr($description, $this->Params));
    }

    public function setMetaTitle($val) {
        $this->_metaTitle=$val;
    }

    public function setMetaTitle_uk($val) {
        $this->_metaTitle_uk=$val;
    }

    public function setMetaTitle_en($val) {
        $this->_metaTitle_en=$val;
    }

    public function setMetaTitle_de($val) {
        $this->_metaTitle_de=$val;
    }

    public function setMetaTitle_fr($val) {
        $this->_metaTitle_fr=$val;
    }

    public function setMetaKeywords($val) {
        $this->_metaKeywords=$val;
    }

    public function setMetaDescription($val) {
        $this->_metaDescription=$val;
    }

    public function setMetaDescription_uk($val) {
        $this->_metaDescription_uk=$val;
    }

    public function setMetaDescription_en($val) {
        $this->_metaDescription_en=$val;
    }

    public function setMetaDescription_de($val) {
        $this->_metaDescription_de=$val;
    }

    public function setMetaDescription_fr($val) {
        $this->_metaDescription_fr=$val;
    }

    public function getParams() {
        return CMap::mergeArray($this->_params, array(
            '{shop_name}'=>Yii::app()->config['shop_name'],
        ));
    }

    public function setParam($key, $val=null) {
        if(strpos($key, '{')!==0) {
            $key='{'.$key.'}';
        }
        $this->_params[$key]=$val;
    }

    public function setParams(array $params) {
        foreach($params as $name=>$val) {
            $this->setParam($name, $val);
        }
    }

    public function setDefault($key, $val) {
        if(array_key_exists($key, $this->_default)==false) {
            throw new CException('Ключ должен быть title, keywords или description');
        }
        $this->_default[$key]=$val;
    }

    public function setRoute($val)
    {
        $this->_route = $val;
        $this->init();
    }

}
