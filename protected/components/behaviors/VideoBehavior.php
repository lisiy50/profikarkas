<?php

class VideoBehavior extends CActiveRecordBehavior
{
    private $_videos = array();

    public $video_ids_attr = 'video_ids';

    public function getVideos(){
        if(!$this->_videos){
            if(trim($this->owner->{$this->video_ids_attr})){
                $videoIds = explode(';', trim($this->owner->{$this->video_ids_attr}));
                foreach($videoIds as $videoId){
                    $this->_videos[] = new ProjectVideoItem($videoId);
                }
            }
        }
        return $this->_videos;
    }
}

class ProjectVideoItem {
    const VIDEO_SEVICE_YOUTUBE = 'youtube';
    const VIDEO_SEVICE_VIMEO = 'vimeo';
    /*
     * @param string image file name
     * @param string type of image
     * */
    private $_videoId;
    private $_service;
    private $_thumbImage;

    /*
     * @param string - video Id
     * */
    public function __construct($videoId){
        $this->_videoId = $videoId;
    }

    /*
     * @return string
     * */
    public function getVideoService(){
        if(!$this->_service){
            if($this->_videoId == intVal($this->_videoId).'')
                $this->_service = self::VIDEO_SEVICE_VIMEO;
            else
                $this->_service = self::VIDEO_SEVICE_YOUTUBE;
        }
        return $this->_service;
    }

    public function getVideoInsertHtml(){
        switch ($this->getVideoService()){
            case self::VIDEO_SEVICE_YOUTUBE:
                return '<iframe width="907" height="550" src="//www.youtube.com/embed/'.$this->_videoId.'" frameborder="0" allowfullscreen></iframe>';
            case self::VIDEO_SEVICE_VIMEO:
                return '<iframe src="//player.vimeo.com/video/'.$this->_videoId.'" width="907" height="550" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            default:
                return false;
        }
    }

    /*
     * @return string
     * */
    public function getVideoThumb(){
        if(!$this->_thumbImage){
            switch ($this->getVideoService()){
                case self::VIDEO_SEVICE_YOUTUBE:
                    $this->_thumbImage = '<img data-thumb="//i1.ytimg.com/vi/'.$this->_videoId.'/default.jpg" src="//i1.ytimg.com/vi/'.$this->_videoId.'/default.jpg" alt="" width="120" data-group-key="thumb-group-0">';
                    break;
                case self::VIDEO_SEVICE_VIMEO:
                    $var = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$this->_videoId.php"));
                    if(isset($var[0])){
                        $this->_thumbImage = CHtml::tag('img', array('src'=>$var[0]['thumbnail_medium']), false, false);
                    }
                    break;
                default:
                    return false;
            }
        }
        return $this->_thumbImage;
    }

}