<?php
class ImageResizeBehavior extends CActiveRecordBehavior {

    /* @var array */
	public $fields;
    public $soursFolder = 'storage/images/';
    public $tempFolder = 'storage/images/temp/';
    public $options = array(
        'quality' => 95,
    );

    /**
     * @return string
     */
    protected function getBasePath() {
        return Yii::app()->basePath.DS.'..'.DS;
    }

    public function afterFind($event)
    {
        if (!IS_FRONTEND)
            return;

        require_once(Yii::getPathofAlias('application.lib.phpQuery.phpQuery').'/phpQuery.php');

        foreach ($this->fields as $field) {
            $content = phpQuery::newDocument($this->owner->{$field});
            $feldImages = $content->find('img');

            foreach ($feldImages as $feldImage) {
                $pq = pq($feldImage);

                $fileName = end(explode('/', $pq->attr('src')));
                $style = $pq->attr('style');

                if ($sizes = $this->parseSize($style)) {
                    $pq->attr('src', $this->prepareImage($fileName, $sizes['width'], $sizes['height']));
                }
            }

            $this->owner->{$field} = $content->html();
        }
    }

    /**
     * @param string $style
     * @return array|bool
     */
    private function parseSize($style)
    {
        $width = false;
        $height = false;
        preg_match('#[^\-]*width:([^p]+)#', $style, $matches);
        if (array_key_exists(1, $matches)) {
            $width = round(trim($matches[1]));
        }
        preg_match('#[^\-]*height:([^p]+)#', $style, $matches);
        if (array_key_exists(1, $matches)) {
            $height = round(trim($matches[1]));
        }

        if (!$width and !$height) {
            return false;
        }
        return array('width' => $width, 'height' => $height);
    }

    /**
     * @param $fileName
     * @param $width
     * @param $height
     * @return string - imageUrl
     */
    private function prepareImage($fileName, $width, $height)
    {
        $originalFilePath = $this->getBasePath().$this->soursFolder.$fileName;
        $tempFileName = $width.'x'.$height.'-'.$fileName;
        $tempFilePath = $this->getBasePath().$this->tempFolder.$tempFileName;

        if ( !is_dir($this->getBasePath().$this->tempFolder) )
            mkdir($this->getBasePath().$this->tempFolder, 0775, true);

        if (!file_exists($tempFilePath)) {
            $image = Yii::app()->image->load($originalFilePath);

            if ($image->width > $width || $image->height > $height)
                $image->resize($width, $height, Image::AUTO);

            $image->save($tempFilePath, array_key_exists('quality', $this->options) ? $this->options['quality'] : 100);
        }

        return Yii::app()->baseUrl.'/'.$this->tempFolder.$tempFileName;
    }
}
