<?php
class SEOBehavior extends CActiveRecordBehavior {

    public $route;

    public $tableName='{{seo}}';

    public $params=array();

    protected $data;

    private function rules() {
		return array(
            array('metaTitle, metaTitle_uk, metaTitle_en, metaTitle_de, metaTitle_fr, metaKeywords, metaDescription, metaDescription_uk, metaDescription_en, metaDescription_de, metaDescription_fr', 'filter', 'filter'=>'strip_tags'),
			array('metaTitle, metaTitle_uk, metaTitle_en, metaTitle_de, metaTitle_fr, metaKeywords', 'length', 'max'=>500),
            array('metaDescription, metaDescription_uk, metaDescription_en, metaDescription_de, metaDescription_fr', 'safe')
		);
    }

    public function attach($owner) {
        $validators=$owner->getValidatorList();
        foreach($this->rules() as $rule)
            $validators->add(CValidator::createValidator($rule[1],$owner,$rule[0],array_slice($rule,2)));

        parent::attach($owner);
    }

    public function setMetaTitle($metaTitle) {
        $this->load();
        $this->data['metaTitle']=$metaTitle;
    }

    public function setMetaTitle_uk($metaTitle) {
        $this->load();
        $this->data['metaTitle_uk']=$metaTitle;
    }

    public function setMetaTitle_en($metaTitle) {
        $this->load();
        $this->data['metaTitle_en']=$metaTitle;
    }

    public function setMetaTitle_de($metaTitle) {
        $this->load();
        $this->data['metaTitle_de']=$metaTitle;
    }

    public function setMetaTitle_fr($metaTitle) {
        $this->load();
        $this->data['metaTitle_fr']=$metaTitle;
    }

    public function setMetaKeywords($metaKeywords) {
        $this->load();
        $this->data['metaKeywords']=$metaKeywords;
    }

    public function setMetaDescription($metaDescription) {
        $this->load();
        $this->data['metaDescription']=$metaDescription;
    }

    public function setMetaDescription_uk($metaDescription) {
        $this->load();
        $this->data['metaDescription_uk']=$metaDescription;
    }

    public function setMetaDescription_en($metaDescription) {
        $this->load();
        $this->data['metaDescription_en']=$metaDescription;
    }

    public function setMetaDescription_de($metaDescription) {
        $this->load();
        $this->data['metaDescription_de']=$metaDescription;
    }

    public function setMetaDescription_fr($metaDescription) {
        $this->load();
        $this->data['metaDescription_fr']=$metaDescription;
    }

    public function getMetaTitle() {
        $this->load();
        return $this->data['metaTitle'];
    }

    public function getMetaTitle_uk() {
        $this->load();
        return $this->data['metaTitle_uk'];
    }

    public function getMetaTitle_en() {
        $this->load();
        return $this->data['metaTitle_en'];
    }

    public function getMetaTitle_de() {
        $this->load();
        return $this->data['metaTitle_de'];
    }

    public function getMetaTitle_fr() {
        $this->load();
        return $this->data['metaTitle_fr'];
    }

    public function getMetaKeywords() {
        $this->load();
        return $this->data['metaKeywords'];
    }

    public function getMetaDescription() {
        $this->load();
        return $this->data['metaDescription'];
    }

    public function getMetaDescription_uk() {
        $this->load();
        return $this->data['metaDescription_uk'];
    }

    public function getMetaDescription_en() {
        $this->load();
        return $this->data['metaDescription_en'];
    }

    public function getMetaDescription_de() {
        $this->load();
        return $this->data['metaDescription_de'];
    }

    public function getMetaDescription_fr() {
        $this->load();
        return $this->data['metaDescription_fr'];
    }

    public function afterSave($event) {
        if($this->data===null)
            return;

        if($this->isEmpty()) {
            $sql="DELETE FROM $this->tableName WHERE route=:route AND entity=:entity LIMIT 1";
            Yii::app()->db->createCommand($sql)->execute(array(
                ':route'=>$this->route,
                ':entity'=>$this->owner->id,
            ));
        } else {
            $sql="INSERT INTO $this->tableName (route,entity,metaTitle,metaTitle_uk,metaTitle_en,metaTitle_de,metaTitle_fr,metaKeywords,metaDescription,metaDescription_uk,metaDescription_en,metaDescription_de,metaDescription_fr) VALUES (:r,:e,:t,:tu,:te,:td,:tf,:k,:d,:du,:de,:dd,:df) " .
                 "ON DUPLICATE KEY UPDATE metaTitle=:t, metaTitle_uk=:tu, metaTitle_en=:te, metaTitle_de=:td, metaTitle_fr=:tf, metaKeywords=:k, metaDescription=:d, metaDescription_uk=:du, metaDescription_en=:de, metaDescription_de=:dd, metaDescription_fr=:df";
            Yii::app()->db->createCommand($sql)->execute(array(
                ':r'=>$this->route,
                ':e'=>$this->owner->id,
                ':t'=>$this->metaTitle,
                ':tu'=>$this->metaTitle_uk,
                ':te'=>$this->metaTitle_en,
                ':td'=>$this->metaTitle_de,
                ':tf'=>$this->metaTitle_fr,
                ':k'=>$this->metaKeywords,
                ':d'=>$this->metaDescription,
                ':du'=>$this->metaDescription_uk,
                ':de'=>$this->metaDescription_en,
                ':dd'=>$this->metaDescription_de,
                ':df'=>$this->metaDescription_fr,
            ));
        }
    }

    protected function load() {
        if($this->data===null) {
            $meta=Yii::app()->db->createCommand()
                ->select('metaTitle,metaTitle_uk,metaTitle_en,metaTitle_de,metaTitle_fr,metaKeywords,metaDescription,metaDescription_uk,metaDescription_en,metaDescription_de,metaDescription_fr')
                ->from($this->tableName)
                ->where('route=:route AND entity=:entity', array(':route'=>$this->route, ':entity'=>$this->owner->id))
                ->queryRow();
            $this->data=($meta)?$meta:array('metaTitle'=>'', 'metaTitle_uk'=>'', 'metaTitle_en'=>'', 'metaTitle_de'=>'', 'metaTitle_fr'=>'', 'metaKeywords'=>'', 'metaDescription'=>'', 'metaDescription_uk'=>'', 'metaDescription_en'=>'', 'metaDescription_de'=>'', 'metaDescription_fr'=>'');
        }
    }

    protected function isEmpty() {
        if($this->data===null)
            return true;

        $empty=true;
        foreach($this->data as $val)
            $empty=$empty && empty($val);
        return $empty;
    }

}
