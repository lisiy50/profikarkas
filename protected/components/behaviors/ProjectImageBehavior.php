<?php

class ProjectImageBehavior extends CActiveRecordBehavior
{
    const IMAGE_TYPE_FACADE = 'facade';
    const IMAGE_TYPE_INTERIOR = 'interior';
    const IMAGE_TYPE_LAYOUT = 'layout';
    const IMAGE_TYPE_VISUALIZATION = 'visualization';

    private $_images = array();

    /*
     * @return string - base path of images folder
     * */
    public function getImagesBasePath(){
        return app()->basePath.'/../storage/projectData';
    }

    public function getProjectName()
    {
        return Yii::app()->translitFormatter->formatFileName($this->getOwner()->symbol.str_replace('+', 'plus', $this->getOwner()->variation_symbol));
    }

    /*
     * @param string image type
     * use const of ProjectImage
     * @return string path to the image type directory
     * */
    public function getProjectPath($type){
        return $this->getImagesBasePath().DS.$type.DS.$this->getProjectName();
    }

    /*
     * @param string image type
     * use const of ProjectImage
     * @return array of image pathes
     * */
    public function getImagesByType($type, $limit = NULL){
        $projectPath = $this->getProjectPath($type);

        if (is_dir($projectPath)) {
            if(!isset($this->_images[$type]) && $dir_handle = opendir($projectPath)){
                $imageCount = 0;
                $files = array();
                while (($file = readdir($dir_handle)) !== false) {
                    if(in_array($file, array('.', '..'))) continue;
                    $files[] = $file;
                }
                sort($files);
                foreach ($files as $file) {
                    if(preg_match("#jpg$|png$#", $file)){
                        $this->_images[$type][] = new ProjectImageItem($this->getProjectName(), $file, $type);
                        $imageCount++;
                        if($limit!==NULL && $imageCount>=$limit)
                            break;
                    }
                }

            }
        }
        if(!isset($this->_images[$type]))
            return array();
        return $this->_images[$type];
    }
}

class ProjectImageItem {
    /*
     * @param string image file name
     * @param string type of image
     * */
    private $_project;
    private $_fileName;
    private $_type;
    private $_imageUrl;

    private $_imageSizes = array(
        'thumb' => array(
            'width'=>102,
            'height'=>60,
        ),
        'facadeSmall'=>array(
            'width'=>218,
            'height'=>131,
        ),
        'interiorSmall'=>array(
            'width'=>300,
            'height'=>168,
        ),
        'landing'=>array(
            'width'=>318,
            'height'=>241,
        ),
        'small' => array(
            'width'=>490,
            'height'=>257,
        ),
        'large' => array(
            'width'=>907,
            'height'=>550,
        ),
    );

    /*
     * @param string - project symbol
     * @param string - file name
     * @param type - image type
     * */
    public function __construct($project, $fileName, $type){
        $this->_project = $project;
        $this->_fileName = $fileName;
        $this->_type = $type;
    }

    /*
     * @return string - base path of images folder
     * */
    public function getImagesBasePath(){
        return app()->basePath.DS.'..'.DS.'storage'.DS.'projectData';
    }

    /*
     * @param string - image type
     * use const of ProjectImage
     * @return string path to the image type directory
     * */
    public function getProjectPath(){
        return $this->getImagesBasePath().DS.$this->_type.DS.$this->_project;
    }

    /*
     * @param string - image type
     * @return string - path to the image
     * */
    public function getProjectOriginalImagePath(){
        return $this->getImagesBasePath().DS.$this->_type.DS.$this->_project.DS.$this->_fileName;
    }

    /*
     * @return string - original image directory
     * */
    public function getImagesBaseUrl(){
        return app()->request->hostInfo.app()->baseUrl.'/storage/projectData/'.$this->_type.'/'.$this->_project;
    }

    /*
     * @return string - file name
     * */
    public function getFileName(){
        return $this->_fileName;
    }

    public function getImageUrl($size = NULL){
        if(!isset($this->_imageSizes[$size]) && $size !== NULL)
            throw new CException('нет такого размера изображений');

        if($size === NULL || $size === 'original'){
            $this->_imageUrl = $this->getImagesBaseUrl().'/'.$this->_fileName;
        }
        else {
            if(!file_exists($this->getProjectPath().DS.$size.DS.$this->_fileName)){
                $this->createImageFile($size);
            }
            $this->_imageUrl = $this->getImagesBaseUrl().'/'.$size.'/'.$this->_fileName;
        }
        return $this->_imageUrl;
    }

    /*
     * @param string - image size
     * */
    public function createImageFile($size){
        if ( !is_dir($this->getProjectPath().DS.$size) ) {
            mkdir($this->getProjectPath().DS.$size, 0775, true);
        }
        $image = Yii::app()->image->load($this->getProjectOriginalImagePath());
        $image->resize($this->_imageSizes[$size]['width'], $this->_imageSizes[$size]['height'], Image::INVERSE)->crop($this->_imageSizes[$size]['width'], $this->_imageSizes[$size]['height']);

        if($size == 'large'){
            $watermarkPath=Yii::app()->basePath.DS.'..'.DS.'storage'.DS.'project_watermark.png';
            if(!file_exists($watermarkPath))
                throw new CException('Файл водяного знака отсутсвует');
            $watermark=Yii::app()->image->load($watermarkPath);
            $image->watermark($watermark, 843, 15);
        }

        $image->save($this->getProjectPath().DS.$size.DS.$this->_fileName, 75);
    }
}