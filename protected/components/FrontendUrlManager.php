<?php
class FrontendUrlManager extends CUrlManager {

    private $_baseUrl;

    public function __construct() {

    }

    public function init()
   	{
        $frontend=include(Yii::getPathOfAlias('application.config.frontend').'.php');

        $config=$frontend['components']['urlManager'];
        unset($config['class']);
        foreach($config as $key=>$value)
            $this->$key=$value;

        parent::init();
   	}

	public function getBaseUrl()
	{
		if($this->_baseUrl!==null)
			return $this->_baseUrl;
		else
		{
            $baseUrl=Yii::app()->getRequest()->getBaseUrl();
			if($this->showScriptName)
				$this->_baseUrl=$baseUrl.'/index.php';
			else
				$this->_baseUrl=$baseUrl;
			return $this->_baseUrl;
		}
	}

}