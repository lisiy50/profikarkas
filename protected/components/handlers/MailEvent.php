<?php
class MailEvent {

    public static function newOrder($event) {
        if(!($event->sender instanceof Order))
            return;

        $order=$event->sender;
        $config=Yii::app()->config;
        $swift=Yii::app()->swiftMailer;

        if($config['mailing_new_order_to_user']) {
            $subject=Yii::app()->twig->render($config['mailing_new_order_subject'], array(
                'order'=>$order,
            ));

            $body=Yii::app()->twig->render($config['mailing_new_order_pattern'], array(
                'order'=>$order
            ));

            $message=$swift->newMessage($order->email, $subject, $body, array('contentType'=>'text/html'));
            $swift->send($message);
        }

        if($config['mailing_new_order_to_admin']) {

            $subject=$config['company'].' - пришел заказ';

            $body="№ заказа: $order->id<br>";
            $body.="Имя: $order->name<br>";
            $body.="Телефон: $order->phone<br>";
            $body.="Ел.почта: $order->email<br>";
            $body.="Адрес: $order->address<br>";
            $body.=$order->comment;

            $message=$swift->newMessage($config['admin_email'], $subject, $body, array('contentType'=>'text/html'));
            $swift->send($message);
        }
    }

    public static function onCreateComment($event) {
        if(!($event->sender instanceof Comment))
            return;

        /** @var Comment $comment */
        $comment=$event->sender;

        /** @var SwiftMailer $swift */
        $swift=Yii::app()->swiftMailer;
        $subject=config('company').' - Оставлен новый коментарий';
        $body=$comment->body;
        $body.='<br>';
        $body.=CHtml::link('Подробнее', $comment->getUrl());

        /** @var Swift_Message $message */
        $message=$swift->newMessage(/*config('admin_email')*/ 'kocherovskaya@z500.biz', $subject, $body, array('contentType'=>'text/html'));
        $message->setFrom(config('contact_email'));
        if ($comment->file1)
            $message->attach(Swift_Attachment::fromPath($comment->getFile1Path()));
        if ($comment->file2)
            $message->attach(Swift_Attachment::fromPath($comment->getFile2Path()));
        if ($comment->file3)
            $message->attach(Swift_Attachment::fromPath($comment->getFile3Path()));
        $swift->send($message);


        $subject = 'Вы оставили на сайте profikarkas.com.ua сообщение';
        $createDate = app()->dateFormatter->format('M-d-Y', $comment->create_time);
        $createTime = app()->dateFormatter->format('H:m', $comment->create_time);
        $body = "Приветствуем, {$comment->user->fio}! Вы оставили на сайте profikarkas.com.ua {$createDate} в {$createTime} сообщение: <br>";
        $body.='"'.$comment->body.'<br>" — Мы ответим на него в ближайшее время. Как правило, это происходит в течение этого или следующего рабочего дня. Всегда ваша - команда "Профикаркас".<br>';
        $body.=CHtml::link('Подробнее', $comment->getUrl());
        /** @var Swift_Message $message */
        $message=$swift->newMessage($comment->user->email, $subject, $body, array('contentType'=>'text/html'));
        $message->setFrom(config('contact_email'));
        if ($comment->file1)
            $message->attach(Swift_Attachment::fromPath($comment->getFile1Path()));
        if ($comment->file2)
            $message->attach(Swift_Attachment::fromPath($comment->getFile2Path()));
        if ($comment->file3)
            $message->attach(Swift_Attachment::fromPath($comment->getFile3Path()));
        $swift->send($message);


        $subject = "На сайте profikarkas.com.ua пользователь {$comment->user->fio} оставил комментарий";
        $body=$comment->body;
        $body.='<br>';
        $body.=CHtml::link('Подробнее', $comment->getUrl());
        $xml = ZohoConnector::generateXml($subject, $comment->user->fio, $comment->user->email, $comment->user->phone, $body);
        ZohoConnector::sendXml($xml);
    }

    public static function onCommentReply($event) {
        if(!($event->sender instanceof Comment))
            return;

        $comment=$event->sender;

        /** @var SwiftMailer $swift */
        $swift=Yii::app()->swiftMailer;
        $subject=config('company').' - Оставлен ответ на ваш комментарий';
        $body=$comment->body;
        $body .= "<br>\n";
        $body.=CHtml::link('Подробнее', $comment->getUrl());

        /** @var Swift_Message $message */
        $message=$swift->newMessage($comment->parent->user->email, $subject, $body, array('contentType'=>'text/html'));
        $message->setFrom(config('contact_email'));
        if ($comment->file1)
            $message->attach(Swift_Attachment::fromPath($comment->getFile1Path()));
        if ($comment->file2)
            $message->attach(Swift_Attachment::fromPath($comment->getFile2Path()));
        if ($comment->file3)
            $message->attach(Swift_Attachment::fromPath($comment->getFile3Path()));
        $swift->send($message);
    }

    /**
     * @param $event
     * @return void
     * @throws CException
     * UserController::actionRemindPassword() uses this event
     */
    public static function onRemindPassword($event)
    {
        if(!($event->sender instanceof RemindPasswordForm))
            return;

        $remindPassword=$event->sender;
        $swift=Yii::app()->swiftMailer;
        $subject=config('company').' - '.t('Восстановление пароля пользователя');

        $message = Swift_Message::newInstance($subject);
        $body = app()->controller->renderPartial('/event/remind_password_mail', array('key'=>$remindPassword->getUserByEmail()->getKey(), 'message'=>$message), true);
        $message->setBody($body, 'text/html');
        $message->setTo(array($remindPassword->email));
        $message->setFrom(isset($_SERVER['HTTP_HOST'])?'noreply@'.$_SERVER['HTTP_HOST']:'noreply@'.$_SERVER['SERVER_NAME']);

        $swift->send($message);
    }
}