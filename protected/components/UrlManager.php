<?php


class UrlManager extends CUrlManager {

    public function createUrl($route, $params = [], $ampersand = '&')
    {
        if (!isset($params['lang'])) {
            $params['lang'] = Yii::app()->language;
        }

        if (isset($params['lang']) && $params['lang'] === 'ru') {
            unset($params['lang']);
        }

        return parent::createUrl($route, $params, $ampersand = '&');
    }
}
