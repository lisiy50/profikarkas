<?php
/**
 * Created by PhpStorm.
 * User: lisiy50
 * Date: 7/7/15
 * Time: 5:22 PM
 */

class ClientScript extends CClientScript
{

    public $defaultCssFilePosition = self::POS_HEAD;

    public function renderHead(&$output)
    {
        $html='';
        foreach($this->metaTags as $meta)
            $html.=CHtml::metaTag($meta['content'],null,null,$meta)."\n";
        foreach($this->linkTags as $link)
            $html.=CHtml::linkTag(null,null,null,null,$link)."\n";
        if (isset($this->cssFiles[self::POS_HEAD])) {
            foreach($this->cssFiles[self::POS_HEAD] as $url=>$media)
                $html.=CHtml::cssFile($url,$media)."\n";
        }
        foreach($this->css as $css)
            $html.=CHtml::css($css[0],$css[1])."\n";
        if($this->enableJavaScript)
        {
            if(isset($this->scriptFiles[self::POS_HEAD]))
            {
                foreach($this->scriptFiles[self::POS_HEAD] as $scriptFile)
                    $html.=CHtml::scriptFile($scriptFile)."\n";
            }

            if(isset($this->scripts[self::POS_HEAD]))
                $html.=CHtml::script(implode("\n",$this->scripts[self::POS_HEAD]))."\n";
        }

        if($html!=='')
        {
            $count=0;
            $output=preg_replace('/(<title\b[^>]*>|<\\/head\s*>)/is','<###head###>$1',$output,1,$count);
            if($count)
                $output=str_replace('<###head###>',$html,$output);
            else
                $output=$html.$output;
        }
    }

    public function renderBodyEnd(&$output)
    {
        if(!isset($this->scriptFiles[self::POS_END]) && !isset($this->scripts[self::POS_END])
            && !isset($this->scripts[self::POS_READY]) && !isset($this->scripts[self::POS_LOAD]))
            return;

        $fullPage=0;
        $output=preg_replace('/(<\\/body\s*>)/is','<###end###>$1',$output,1,$fullPage);
        $html='';
        if (isset($this->cssFiles[self::POS_END])) {
            foreach($this->cssFiles[self::POS_END] as $url=>$media)
                $html.=CHtml::cssFile($url,$media)."\n";
        }
        if(isset($this->scriptFiles[self::POS_END]))
        {
            foreach($this->scriptFiles[self::POS_END] as $scriptFile)
                $html.=CHtml::scriptFile($scriptFile)."\n";
        }
        $scripts=isset($this->scripts[self::POS_END]) ? $this->scripts[self::POS_END] : array();
        if(isset($this->scripts[self::POS_READY]))
        {
            if($fullPage)
                $scripts[]="jQuery(function($) {\n".implode("\n",$this->scripts[self::POS_READY])."\n});";
            else
                $scripts[]=implode("\n",$this->scripts[self::POS_READY]);
        }
        if(isset($this->scripts[self::POS_LOAD]))
        {
            if($fullPage)
                $scripts[]="jQuery(window).on('load',function() {\n".implode("\n",$this->scripts[self::POS_LOAD])."\n});";
            else
                $scripts[]=implode("\n",$this->scripts[self::POS_LOAD]);
        }
        if(!empty($scripts))
            $html.=CHtml::script(implode("\n",$scripts))."\n";

        if($fullPage)
            $output=str_replace('<###end###>',$html,$output);
        else
            $output=$output.$html;
    }

    public function renderCoreScripts()
    {
        if($this->coreScripts===null)
            return;
        $cssFiles=array();
        $jsFiles=array();
        foreach($this->coreScripts as $name=>$package)
        {
            $baseUrl=$this->getPackageBaseUrl($name);
            if(!empty($package['js']))
            {
                foreach($package['js'] as $js)
                    $jsFiles[$baseUrl.'/'.$js]=$baseUrl.'/'.$js;
            }
            if(!empty($package['css']))
            {
                foreach($package['css'] as $css)
                    $cssFiles[$this->defaultCssFilePosition][$baseUrl.'/'.$css]='';
            }
        }
        // merge in place
        if($cssFiles!==array() && $this->cssFiles!==array())
        {
            foreach($this->cssFiles[$this->defaultCssFilePosition] as $cssFile=>$media)
                $cssFiles[$this->defaultCssFilePosition][$cssFile]=$media;
            $this->cssFiles[$this->defaultCssFilePosition]=$cssFiles[$this->defaultCssFilePosition];
        }
        if($jsFiles!==array())
        {
            if(isset($this->scriptFiles[$this->coreScriptPosition]))
            {
                foreach($this->scriptFiles[$this->coreScriptPosition] as $url)
                    $jsFiles[$url]=$url;
            }
            $this->scriptFiles[$this->coreScriptPosition]=$jsFiles;
        }
    }

    public function registerCssFile($url,$media='', $position = null)
    {
        if($position===null)
            $position=$this->defaultCssFilePosition;
        $this->hasScripts=true;
        $this->cssFiles[$position][$url]=$media;
        $params=func_get_args();
        $this->recordCachingAction('clientScript','registerCssFile',$params);
        return $this;
    }

    public function isCssFileRegistered($url, $position = null)
    {
        if($position===null)
            $position=$this->defaultCssFilePosition;
        return isset($this->cssFiles[$position][$url]);
    }

    protected function remapScripts()
    {
        foreach ($this->cssFiles as $position=>$files) {
            $cssFiles=array();
            foreach($files as $url=>$media)
            {
                $name=basename($url);
                if(isset($this->scriptMap[$name]))
                {
                    if($this->scriptMap[$name]!==false)
                        $cssFiles[$this->scriptMap[$name]]=$media;
                }
                elseif(isset($this->scriptMap['*.css']))
                {
                    if($this->scriptMap['*.css']!==false)
                        $cssFiles[$this->scriptMap['*.css']]=$media;
                }
                else
                    $cssFiles[$url]=$media;
            }
            $this->cssFiles[$position]=$cssFiles;
        }

        $jsFiles=array();
        foreach($this->scriptFiles as $position=>$scripts)
        {
            $jsFiles[$position]=array();
            foreach($scripts as $key=>$script)
            {
                $name=basename($script);
                if(isset($this->scriptMap[$name]))
                {
                    if($this->scriptMap[$name]!==false)
                        $jsFiles[$position][$this->scriptMap[$name]]=$this->scriptMap[$name];
                }
                elseif(isset($this->scriptMap['*.js']))
                {
                    if($this->scriptMap['*.js']!==false)
                        $jsFiles[$position][$this->scriptMap['*.js']]=$this->scriptMap['*.js'];
                }
                else
                    $jsFiles[$position][$key]=$script;
            }
        }
        $this->scriptFiles=$jsFiles;
    }
}
