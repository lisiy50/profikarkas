$(document).ready(function() {
    smoothScroll();
    onloadPage();
    landingSkrollr();
    $(".pre").delay(400).fadeOut(800);
    lineLeft();
    initQuestionSlider();

    $(window).on("scroll", function() {
        var scroll = $(window).scrollTop(),
            neededPosition = $(".new_menu_appear").offset().top;

        if (window.matchMedia("(min-width: 1025px)").matches) {
            if (scroll >= 5000) {
                $(".form3 button").addClass("ready");
            } else {
                $(".form3 button").removeClass("ready");
            }
        }

        if (scroll === 0) {
            $(".wrap_nav").removeClass("back_shadow");
            $(".wrap_content").removeClass("line_hidden");
        } else {
            $(".wrap_nav").addClass("back_shadow");
            $(".wrap_content").addClass("line_hidden");
        }

        if (scroll >= neededPosition) {
            $(".wrap_nav").addClass("loader");
        } else {
            $(".wrap_nav").removeClass("loader");
        }

        if (scroll >= 400) {
            $(".wrap_nav").addClass("removediv");
        } else {
            $(".wrap_nav").removeClass("removediv");
        }
    });

    if (window.matchMedia("(max-width: 1024px)").matches) {
        $(".form3 button").addClass('ready');
    }

    $(window).on("resize", function() {
        if (window.matchMedia("(max-width: 1024px)").matches) {
            $(".form3 button").addClass('ready');
        } else {
            $(".pre").hide();
        }
    });

    var firstSlick = $(".first_slider");
    firstSlick.on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        var slide = $(this).find(".slick-active");
        slide.animate({
            opacity: 0
        }, 600, function() {
            setTimeout(function() {
                slide.css("opacity", "1");
            }, 400);
        });
    });

    var advantageBlock = $(".fourteen1");
    advantageBlock.on("mousemove", function(e) {
        if (window.matchMedia("(min-width: 1025px)").matches) {
            var mx = e.pageX,
                rotateK = 90 / $(window).width(),
                rotateDeg = -(45 - rotateK * mx);

            if (rotateDeg < 0) {
                rotateDeg = rotateDeg - 7;
            }
            $(this).find(".show_item").css({transform: "rotate3d(0, 1, 0, " + -rotateDeg + "deg )"});
        }
    });

    advantageBlock.on("mouseover", function() {
        $(".fourteen1 .show_item").css({transform: "rotate3d(0, 1, 0, 0deg)"});
    });

    $(".read_more").on("click", function(){
        var par = $(this).parent(),
            video = par.find(".video3"),
            text = par.find(".desc_text"),
            height = par.find(".desc_text > div").height() + 2;

        if (height > 285) {
            height = 285;
        }

        if (window.matchMedia("(max-width: 1024px)").matches) {
            if (height > 225) {
                height = 225;
            }
        }

        if (window.matchMedia("(max-width: 760px)").matches) {
            if (height > 260) {
                height = 260;
            }
        }

        if ($(this).hasClass("opened")) {
            $(this).removeClass("opened").html("Читать больше...");
            text.animate({
                height: "121px"
            }, 600, function() {
                video.fadeIn();
            });
        } else {
            $(this).addClass("opened").html("Свернуть");
            video.hide();
            text.animate({
                height: height
            }, 600);
        }
    });

    var questionSlider = $(".container.third");
    questionSlider.find("h5, p").on("click", function() {
        if (window.matchMedia("(max-width: 1024px)").matches) {
            var lines = $(this).parent().find("line"),
                line1 = $(this).parent().find("line:first-child"),
                line2 = $(this).parent().find("line:nth-child(2)");
            $(".container.third line").animate({
                "stroke-dashoffset": 200
            }, function () {
                $(".container.third line:nth-child(2)").hide();
                $(".container.third line:nth-child(1)").show();
            });
            lines.stop().animate({
                "stroke-dashoffset": 0
            }, function () {
                line1.hide();
                line2.show();
            });
        }
    });

    questionSlider.find("h5, p").on("mouseover", function(){
        if (window.matchMedia("(min-width: 1025px)").matches) {
            var lines = $(this).parent().find("line"),
                line1 = $(this).parent().find("line:first-child"),
                line2 = $(this).parent().find("line:nth-child(2)");
            $('.container.third line').stop().animate({
                "stroke-dashoffset": 200
            }, function () {
                $(".container.third line:nth-child(2)").hide();
                $(".container.third line:nth-child(1)").show();
            });
            lines.stop().animate({
                "stroke-dashoffset": 0
            }, function () {
                line1.hide();
                line2.show();
            });
        }
    });

    var sliderIndex = 1;

    $(".eight_big a").on("click", function(e) {
        e.preventDefault();
        var index = $(this).parent().attr("data-index");
        $(".overflow_lb").fadeIn();
        sliderIndex = index;
        lb_click(sliderIndex);
    });

    $(".overflow_lb, .overflow_lb .close_lb").on("click", function() {
        $(".overflow_lb").fadeOut();
        $(".overflow_lb .lb").fadeOut();
    });

    $(".overflow_lb > div").on("click", function(e) {
        e.stopPropagation();
    });

    $(".overflow_lb .lb_prev").on("click", function() {
        sliderIndex = parseInt(sliderIndex) - 1;
        if (sliderIndex < 1) {
            sliderIndex = $(".overflow_lb .lb").length;
        }
        lb_click(sliderIndex);
    });

    $(".overflow_lb .lb_next").on("click", function() {
        sliderIndex = parseInt(sliderIndex) + 1;
        if (sliderIndex > $(".overflow_lb .lb").length) {
            sliderIndex = 1;
        }
        lb_click(sliderIndex);
    });

    var questionSliderLink = $(".small h5, .small p");
    questionSliderLink.on("mouseover", function() {
        if (window.matchMedia("(min-width: 1025px)").matches) {
            $(this).siblings("span").toggleClass("active");
            $(this).closest(".small").siblings(".small").each(function() {
                $(this).find("span").removeClass("active");
            });
            $(this).closest(".outerdiv").siblings(".outerdiv").each(function() {
                $(this).find("span").removeClass("active");
            });

            var imgInner = $(this).siblings(".previewImage").html();
            $(".previewImageShow").fadeOut(300, function() {
                $(".previewImageShow").html(imgInner).fadeIn("slow");
            });
        }
    });

    questionSliderLink.click(function () {
        if (window.matchMedia("(max-width: 1024px)").matches) {
            $(this).siblings("span").toggleClass("active");
            $(this).closest(".small").siblings(".small").each(function() {
                $(this).find("span").removeClass("active");
            });
            $(this).closest(".outerdiv").siblings(".outerdiv").each(function() {
                $(this).find("span").removeClass("active");
            });

            var imgInner = $(this).siblings(".previewImage").html();
            $(".previewImageShow").fadeOut(300, function() {
                $(".previewImageShow").html(imgInner).fadeIn("slow");
            });
        }
    });

    $(".ooo").click(function() {
        var block = $(this).closest(".fourteen1").parent();
        $(this).toggleClass("clicked");
        $(this).siblings("p").toggle("slow");
        $(this).closest(".fourteen1").siblings(".fourteen1").each(function() {
            $(this).find(".ooo").removeClass("clicked").siblings("p").hide("slow");
        });

        if (block.hasClass("pozz5")) {
            $(".pozz7 .fourteen1").each(function() {
                $(this).find(".ooo").removeClass("clicked").siblings("p").hide("slow");
            });
        } else if (block.hasClass("pozz7")) {
            $(".pozz5 .fourteen1").each(function() {
                $(this).find(".ooo").removeClass("clicked").siblings("p").hide("slow");
            });
        }
    });

    var windowWidthValue = (window.innerWidth > 0) ? window.innerWidth : screen.width;

    if (windowWidthValue < 767) {
        $(".small > span").click(function() {
            if ($(this).hasClass("change")) {
                $(this).siblings("p").hide("300");
                setTimeout(function() {
                    $(this).removeClass("change");
                }, 300);
            } else {
                $(this).toggleClass("change");
                setTimeout(function() {
                    $(this).siblings("p").show("300");
                }, 300);
                $(this).closest(".small").siblings(".small").each(function() {
                    $(this).find("p").hide("300").siblings("span").removeClass("change");
                });
                $(this).closest(".outerdiv").siblings(".outerdiv").each(function() {
                    $(this).find("p").hide("300").siblings("span").removeClass("change");
                });
            }
        });
    } else {
        $(".eighteen_scroll").mCustomScrollbar();
    }

    var sly;
    initSly();

    var advantageCard = $(".eight");

    if (windowWidthValue < 765) {
        advantageCard.slick({
        slidesToShow: 1
      });
    }

    $(window).resize(function() {
        var windowWidthValue = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        if (windowWidthValue > 768) {
            if (advantageCard.hasClass('slick-slider')) {
                advantageCard.slick('unslick');
            }
        } else {
            if (! advantageCard.hasClass('slick-slider')) {
                advantageCard.slick({
                  slidesToShow: 1
                });
            }
        }
    });

    firstSlick.slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots:true,
        speed: 1000,
        dotsClass: "first_dots"
    });

    var secondSlick = $(".second_slider");
    secondSlick.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1000,
        dots:true,
        dotsClass: "second_dots"
    });

    secondSlick.on("beforeChange", function(event, slick, currentSlide, nextSlide) {
        var slide = $(this).find('.slick-slide[data-slick-index="' + currentSlide + '"]'),
            slideNext = $(this).find('.slick-slide[data-slick-index="' + nextSlide + '"]');
        slide.animate({
            opacity: 0
        }, 600, function() {
            setTimeout(function() {
                slide.css("opacity", "1");
            }, 400);
        });
        slideNext.css("opacity", "0");
        slideNext.animate({
            opacity: 1
        }, 1000);
    });

    var partnerSlick = $(".footer__data-gallery-inner");
    partnerSlick.slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 1000,
        cssEase: "cubic-bezier(.165, .84, .44, 1)",
        infinite: true,
        autoplaySpeed: 6000,
        arrows: true,
        nextArrow: ".footer__data-arrow_next",
        prevArrow: ".footer__data-arrow_prev",
        dots: false
    });

    var clickedMenuCallback = false,
        canScroll;
    $(".first ul li a.call_back").click(function() {
        $("#order1.popup").fadeIn(400);
        $("#overlay").show().css({"opacity": "0.5", "display": "block"});
        canScroll = false;
    });

    $(".popup .close_popup").click(function() {
        $("#order1.popup").fadeOut(400);
        $("#overlay").show().css({"opacity": "0", "display": "none"});
        canScroll = true;
        clickedMenuCallback = false;
    });

    $(".menu_collapse_768 ul.black li a.call_back").click(function() {
        clickedMenuCallback = true;
        $("#order1.popup").fadeIn(200);
        canScroll = false;
    });

    $(".menu_collapse_768").click(function() {
        if (! clickedMenuCallback) {
            setTimeout(function() {
                $("#overlay").show().css({"opacity": "0.5", "display" : "block"});
            },300);
            $(this).toggleClass("active_nav");
            if (windowWidthValue > 767) {
                if ($(this).hasClass("active_nav")) {
                    setTimeout(function() {
                        $(this).css({"height": $(".menu_collapse_768 ul").height()});
                    },200);
                } else {
                    $(this).attr("style", "");
                    setTimeout(function() {
                        $("#overlay").show().css({"opacity": "0", "display" : "none"});
                    },400);
                }
            } else {
                $(this).attr("style", "");
                setTimeout(function(){
                    $("#overlay").hide().css({"opacity": "0", "display" : "none"});
                },400);
            }
        }
    });

    $(".word").click(function() {
       $(this).toggleClass("opinion");
       $(this).siblings("div").toggle("300");
    });

    $("input, textarea").focus(function() {
        $(this).data("placeholder", $(this).attr("placeholder")).attr("placeholder", "");
    }).blur(function() {
        $(this).attr("placeholder", $(this).data('placeholder'));
    });

    $(".toggle").on("click", function() {
        $(".containerp").stop().addClass("active");
    });

    $('.close').on("click", function() {
        $(".containerp").stop().removeClass("active");
    });

    var firstSlickblockHeight = firstSlick.height() / 2,
        firstSlickArrowHeight = $(".first_slider .slick-next").height() / 2;

    $(".first_slider .slick-arrow").css({
        position: "absolute",
        top: firstSlickblockHeight - firstSlickArrowHeight + "px",
    });

    var item_width = false;

    function loadProjects(z) {
        if (! item_width) {
            item_width = $('#frame .slidee .active.curr').css('width');
        }
        $.get("landing/ajaxProjects", {
            Project_sort: $('#sort-by').val(),
            'Project[net_area][from]': $('#filter-area-from').val(),
            'Project[net_area][till]': $('#filter-area-till').val(),
            item_width: item_width
        }, function (result) {
            $('#frame .slidee').html(result);
            destroySly();
            if (z) {
                $('#discardFiltr').css('display', 'none');
            } else if ( result.length >= 0 ) {
                document.getElementById("discardFiltr").style.display = 'block';
            } else {
                document.getElementById("discardFiltr").style.display = 'none';
            }

            if (result.length <= 9) {
                document.getElementById("showOnNeed").style.display = 'block';
            } else {
                document.getElementById("showOnNeed").style.display = 'none';
            }
        });
    }

    $("#sort-by-price").click(function () {
        $("#sort-by").val("price");
        loadProjects();
        return false;
    });

    $("#sort-by-area").click(function () {
        $("#sort-by").val("net_area");
        loadProjects();
        return false;
    });

    $('#filter-area-from, #filter-area-till').change(function() {
        loadProjects();
    });

    $("#discardFiltr").click(function() {
        $("#filter-area-from").val("");
        $("#filter-area-till").val("");
        loadProjects(1);
    });

    $(".header_lang_btn").on("click", function() {
        $(".header_lang_inner").slideToggle();
        $(this).toggleClass("header_lang_btn_active");
    });

    $(document).on("click", "#landing_recall .button_popup", function() {
        var form = $("#landing_recall");
        $.ajax({
            url: "landing/recall",
            type: "post",
            data: form.serialize(),
            success: function(data) {
                $('#order1 .card').html(data);
            },
            error: function(data) {
                alert('bad request');
            }
        });
        return false;
    });

    $(".form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        form.find("button").prop("disabled", true);
        $.ajax({
            type: "post",
            url: "landing/ajaxContact",
            data: form.serialize(),
            dataType: "json",
            success: function(data) {
                alert(data.message);
                if (data.success) {
                    form.find(".reset").val("");
                }
                form.find("button").prop("disabled", false);
            },
            error: function(data) {
                alert(data.message);
                form.find('button').prop("disabled", false);
            }
        });
        return false;
    });

    function smoothScroll() {
        SmoothScroll({
            stepSize: 80
        });
    }

    function onloadPage() {
        var scroll = $(window).scrollTop(),
            button = $(".form3 button");
        if (scroll > 0) {
            $(".wrap_nav").addClass("back_shadow removediv");
            $('.wrap_content').addClass('line_hidden');
        }
        if (scroll >= 5000) {
            button.addClass("ready");
        }
    }

    function landingSkrollr() {
        var skr;
        setTimeout(function() {
            if (window.matchMedia("(min-width: 1025px)").matches) {
                skr = skrollr.init();
            }
        }, 500);
    }

    function lb_click(sliderIndex) {
        var next = $(".overflow_lb .lb:nth-child(" + sliderIndex + ")"),
            heightLb = next.innerHeight() + 40;
        $(".overflow_lb .lb, .overflow_lb .text_wrap, .overflow_lb .text_content").removeAttr("style");
        if (window.matchMedia("(max-width: 760px)").matches) {
            heightLb = next.innerHeight() + 60;
        }
        if (heightLb < 400) {
            heightLb = 400;
        }
        $(".overflow_lb .lb").fadeOut();
        if (window.matchMedia("(max-width: 760px)").matches) {
            $('.overflow_lb .text_wrap, .overflow_lb .text_content').height(heightLb);
        }
        $('.overflow_lb .lb_content').animate({
            height: heightLb
        }, 400);
        next.animate({
            height: heightLb
        }, 400, function() {
            next.fadeIn();
        });
    }

    function lineLeft() {
        var contOff = $(".container.second").offset().left,
            firstElem = $(".header_wrap .first ul li:last-of-type"),
            pathOff = firstElem.offset().left,
            pathW = firstElem.width();
        if (window.matchMedia("(min-width: 767px)").matches
            && window.matchMedia("(max-width: 992px)").matches) {
            $(".path").css({left: contOff + "px", width: (pathOff + pathW) - contOff + "px"})
        }
    }

    function initQuestionSlider() {
        var elem = $(".small h5");
        elem.siblings("span").toggleClass("active");
        elem.closest(".small").siblings(".small").each(function() {
            elem.find("span").removeClass("active");
        });
        elem.closest(".outerdiv").siblings(".outerdiv").each(function() {
            $(this).find("span").removeClass("active");
        });

        var imgInner = elem.siblings(".previewImage").html();
        $(".previewImageShow").fadeOut(300, function () {
            $(".previewImageShow").html(imgInner).fadeIn("slow");
        });
    }

    function initSly() {
        $("#frame .slidee > div").each(function() {
            var width = $('#frame').innerWidth();
            $(this).css("width", width / 2 + "px");
            if ( width <= 425) {
                $(this).css("width", width + "px");
            }
        });

        var frame = $("#frame");

        sly = new Sly(frame, {
            horizontal: 1,
            itemNav: "basic",
            smart: 1,
            activateOn: "click",
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $(".scrollbar"),
            scrollBy: 1,
            speed: 800,
            elasticBounds: false,
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,
            activeClass: "active",
            prev: frame.find(".prev"),
            next: frame.find(".next")
        });

        $(".slidee .active").addClass("curr");

        sly.on("load moveStart", function() {
            if(window.matchMedia("(min-width: 1025px)").matches) {
                var active = $(".slidee .curr"),
                    oprop = $(".slidee").css("transform");
                oprop = oprop.split(",");
                var oldV = oprop[4];
                if (oldV !== 0) {
                    oldV = oldV * -1;
                }
                setTimeout(function() {
                    var nprop = $(".slidee").css("transform");
                    nprop = nprop.split(",");
                    var newV = nprop[4];
                    if (newV !== 0) {
                        newV = newV * -1;
                    }
                    if (newV >= oldV) {
                        active.stop().animate({
                            opacity: 0
                        }, 600, function() {
                            setTimeout(function() {
                                active.css("opacity", "1");
                            }, 400);
                        });
                        active.next().next().css("opacity", 0).stop().animate({
                            opacity: 1
                        }, 1200);
                    } else {
                        active.next().stop().animate({
                            opacity: 0
                        }, 600, function() {
                            setTimeout(function() {
                                active.next().css("opacity", "1");
                            }, 400);
                        });
                        active.prev().css("opacity", 0).stop().animate({
                            opacity: 1
                        }, 1200);
                    }
                }, 100);
                setTimeout(function() {
                    var prop = $(".slidee").css("transform");
                    prop = prop.split(",");
                    var prevValue = prop[4];
                    active.removeClass("curr");
                    var num = 1;
                    if (prevValue !== 0) {
                        num = (prevValue * -1/540) + 1;
                    }
                    $(".slidee > div:nth-child(" + num + ")").addClass("curr");
                },850);
            }
        });
        sly.init();
    }

    function destroySly() {
        sly.destroy();
        initSly();
        loadImages();
    }

    function loadImages() {
        var lazyElements = document.querySelectorAll("img[data-src]");
        for (var i = 0; i < lazyElements.length; i++) {
            if(lazyElements[i].getAttribute('data-src')) {
                lazyElements[i].setAttribute('src', lazyElements[i].getAttribute('data-src'));
            }
        }
    }
});
