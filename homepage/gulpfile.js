var gulp = require('gulp');
var watch = require('gulp-watch');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('cssnano');
var rename = require('gulp-rename');
var stylelint = require('stylelint');
var reporter = require('postcss-reporter');
var atImport = require("postcss-import");
var nested = require('postcss-nested');
var cssvariables = require('postcss-css-variables');

gulp.task('postcss', function() {
    return gulp.src('./postcss/*.pcss')
        .pipe(sourcemaps.init())
        .pipe(postcss([
            atImport(),
            nested(),
            cssvariables()
        ]))
        .pipe(postcss([cssnano]))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(rename('styles1.min.css'))
        .pipe(sourcemaps.write({includeContent: false, sourceRoot: './postcss/'}))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', function() {
    gulp.watch('./postcss/**/*.pcss', gulp.series('postcss'));
});

gulp.task('scripts', function(){
    return gulp.src([
        './scripts/vendor/jquery-1.11.3.js',
        './scripts/vendor/bootstrap.min.js',
        './scripts/vendor/jquery.mCustomScrollbar.concat.min.js',
        './scripts/vendor/skrollr.min.js',
        './scripts/vendor/slick.min.js',
        './scripts/vendor/SmoothScroll.js',
        './scripts/vendor/sly.js',
        './scripts/app.js'
    ])
        .pipe(concat('scripts-b.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js'));
});

gulp.task('lint-styles', function() {
    return gulp.src('./postcss/**/*.pcss')
        .pipe(postcss([stylelint({
            "rules": {
                "color-no-invalid-hex": true,
                "font-family-no-duplicate-names": true,
                "font-family-no-missing-generic-family-keyword": true,
                "function-calc-no-unspaced-operator": true,
                "function-linear-gradient-no-nonstandard-direction": true,
                "string-no-newline": true,
                "unit-no-unknown": true,
                "property-no-unknown": true,
                "keyframe-declaration-no-important": true,
                "declaration-block-no-duplicate-properties": true,
                "declaration-block-no-shorthand-property-overrides": true,
                "block-no-empty": true,
                "selector-pseudo-class-no-unknown": true,
                "selector-pseudo-element-no-unknown": true,
                "selector-type-no-unknown": true,
                "media-feature-name-no-unknown": true,
                "at-rule-no-unknown": true,
                "comment-no-empty": true,
                "no-descending-specificity": true,
                "no-duplicate-at-import-rules": true,
                "no-duplicate-selectors": true,
                "no-empty-source": true,
                "no-extra-semicolons": true,
                "no-invalid-double-slash-comments": true,
                "color-named": "never",
                "function-url-no-scheme-relative": true,
                "number-max-precision": 8,
                "time-min-milliseconds": 100,
                "shorthand-property-no-redundant-values": true,
                "value-no-vendor-prefix": true,
                "property-no-vendor-prefix": true,
                "declaration-block-no-redundant-longhand-properties": true,
                "declaration-no-important": true,
                "declaration-block-single-line-max-declarations": 0,
                "selector-max-empty-lines": 0,
                "selector-no-qualifying-type": [
                    true,
                    {
                        "ignore": [
                            "attribute"
                        ]
                    }
                ],
                "at-rule-no-vendor-prefix": true,
                "no-unknown-animations": true,
                "color-hex-case": "lower",
                "color-hex-length": "long",
                "font-family-name-quotes": "always-unless-keyword",
                "function-comma-space-after": "always",
                "function-comma-space-before": "never",
                "function-max-empty-lines": 0,
                "function-name-case": "lower",
                "function-parentheses-space-inside": "never",
                "function-url-quotes": "always",
                "function-whitespace-after": "always",
                "number-leading-zero": "always",
                "number-no-trailing-zeros": true,
                "string-quotes": "double",
                "length-zero-no-unit": true,
                "unit-case": "lower",
                "value-keyword-case": "lower",
                "value-list-comma-newline-after": "always-multi-line",
                "value-list-comma-newline-before": "never-multi-line",
                "value-list-comma-space-after": "always",
                "value-list-comma-space-before": "never",
                "value-list-max-empty-lines": 0,
                "custom-property-empty-line-before": "never",
                "property-case": "lower",
                "declaration-bang-space-after": "never",
                "declaration-bang-space-before": "always",
                "declaration-colon-newline-after": "always-multi-line",
                "declaration-colon-space-after": "always",
                "declaration-colon-space-before": "never",
                "declaration-empty-line-before": "never",
                "declaration-block-semicolon-newline-after": "always",
                "declaration-block-semicolon-newline-before": "never-multi-line",
                "declaration-block-semicolon-space-after": "always-single-line",
                "declaration-block-semicolon-space-before": "never",
                "declaration-block-trailing-semicolon": "always",
                "block-closing-brace-empty-line-before": "never",
                "block-closing-brace-newline-after": "always",
                "block-closing-brace-newline-before": "always",
                "block-opening-brace-newline-after": "always",
                "block-opening-brace-space-after": "always",
                "block-opening-brace-space-before": "always",
                "selector-attribute-brackets-space-inside": "never",
                "selector-attribute-operator-space-after": "never",
                "selector-attribute-operator-space-before": "never",
                "selector-attribute-quotes": "always",
                "selector-combinator-space-after": "always",
                "selector-combinator-space-before": "always",
                "selector-descendant-combinator-no-non-space": true,
                "selector-pseudo-class-case": "lower",
                "selector-pseudo-class-parentheses-space-inside": "never",
                "selector-pseudo-element-case": "lower",
                "selector-pseudo-element-colon-notation": "single",
                "selector-type-case": "lower",
                "selector-list-comma-newline-after": "always",
                "selector-list-comma-newline-before": "never-multi-line",
                "selector-list-comma-space-after": "always",
                "selector-list-comma-space-before": "always-single-line",
                "rule-empty-line-before": [
                    "always",
                    {
                        "except": [
                            "after-single-line-comment",
                            "first-nested"
                        ]
                    }
                ],
                "media-feature-colon-space-after": "always",
                "media-feature-colon-space-before": "never",
                "media-feature-name-case": "lower",
                "media-feature-parentheses-space-inside": "never",
                "media-feature-range-operator-space-after": "always",
                "media-feature-range-operator-space-before": "always",
                "media-query-list-comma-newline-after": "always",
                "media-query-list-comma-newline-before": "never-multi-line",
                "media-query-list-comma-space-after": "always",
                "media-query-list-comma-space-before": "never",
                "at-rule-empty-line-before": [
                    "always",
                    {
                        "except": [
                            "blockless-after-same-name-blockless",
                            "first-nested"
                        ],
                        "ignore": [
                            "after-comment"
                        ]
                    }
                ],
                "at-rule-name-case": "lower",
                "at-rule-name-newline-after": "always-multi-line",
                "at-rule-name-space-after": "always",
                "at-rule-semicolon-newline-after": "always",
                "at-rule-semicolon-space-before": "never",
                "comment-empty-line-before":  [
                    "always",
                    {
                        "except": [
                            "first-nested"
                        ]
                    }
                ],
                "comment-whitespace-inside": "always",
                "indentation": "tab",
                "max-empty-lines": 1,
                "no-eol-whitespace": true,
                "no-missing-end-of-source-newline": true
            }
        }),
            reporter({
                clearMessages: true
            })
        ]))
});
