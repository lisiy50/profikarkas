var windowWidthValue = (window.innerWidth > 0) ? window.innerWidth : screen.width;

$(document).ready(function(){

    $('.small h5, .small p').click(function () {
        if (window.matchMedia("(max-width: 1024px)").matches) {
            var $this = $(this);
            $(this).siblings('span').toggleClass('active');
            $this.closest('.small').siblings('.small').each(function () {
                $(this).find('span').removeClass('active');
            });

            $this.closest('.outerdiv').siblings('.outerdiv').each(function () {
                $(this).find('span').removeClass('active');
            });

            var imgInner = $this.siblings('.previewImage').html();
            $('.previewImageShow').fadeOut(300, function () {
                $('.previewImageShow').html(imgInner);
                $('.previewImageShow').fadeIn("slow");
            });
        }
    });

    function sliderFirst(){
        var first = $('.small h5');

        first.siblings('span').toggleClass('active');
        first.closest('.small').siblings('.small').each(function () {
            first.find('span').removeClass('active');
        });

        first.closest('.outerdiv').siblings('.outerdiv').each(function () {
            $(this).find('span').removeClass('active');
        });

        var imgInner = first.siblings('.previewImage').html();
        $('.previewImageShow').fadeOut(300, function () {
            $('.previewImageShow').html(imgInner);
            $('.previewImageShow').fadeIn("slow");
        });
    }
    sliderFirst();

    $('.small h5, .small p').on('mouseover',function () {
        if (window.matchMedia("(min-width: 1025px)").matches) {
            var $this = $(this);
            $(this).siblings('span').toggleClass('active');
            $this.closest('.small').siblings('.small').each(function () {
                $(this).find('span').removeClass('active');
            });

            $this.closest('.outerdiv').siblings('.outerdiv').each(function () {
                $(this).find('span').removeClass('active');
            });

            var imgInner = $this.siblings('.previewImage').html();
            $('.previewImageShow').fadeOut(300, function () {
                $('.previewImageShow').html(imgInner);
                $('.previewImageShow').fadeIn("slow");
            });
        }
    });



$('.ooo').click(function(){

    var block = $(this).closest('.fourteen1').parent();
    //console.log(block);
    $(this).toggleClass('clicked');
    $(this).siblings('p').toggle('slow');

    $(this).closest('.fourteen1').siblings('.fourteen1').each(function(){
        $(this).find('.ooo').removeClass('clicked').siblings('p').hide('slow');
    });
    if(block.hasClass('pozz5')) {
        $('.pozz7 .fourteen1').each(function(){
            $(this).find('.ooo').removeClass('clicked').siblings('p').hide('slow');
        });
    } else if(block.hasClass('pozz7')) {
        $('.pozz5 .fourteen1').each(function(){
            $(this).find('.ooo').removeClass('clicked').siblings('p').hide('slow');
        });
    }
});

$('.let1').find('div.small:first-child > h5').click();

      if (windowWidthValue < 767) {
       $('.small > span').click(function(){
           var that =  $(this);
          if($(this).hasClass('change')){
            that.siblings('p').hide('300');
            setTimeout(function() {
                that.removeClass('change');
            }, 300);

         } else {
           that.toggleClass('change');
            setTimeout(function() {
            that.siblings('p').show('300');
            }, 300);

            that.closest('.small').siblings('.small').each(function() {
              $(this).find('p').hide('300').siblings('span').removeClass('change');
            });

            that.closest('.outerdiv').siblings('.outerdiv').each(function() {
             $(this).find('p').hide('300').siblings('span').removeClass('change');
            });
          }
      });
      }



// screen scroll

    $("a.topLink").click(function() {
      if (windowWidthValue > 1024) {
        $("html, body").animate({
             scrollTop: $($(this).attr("href")).offset().top - 140 + "px"},
            {
             duration: 500
        });
      } else {
         $("html, body").animate({
             scrollTop: $($(this).attr("href")).offset().top - 40 + "px"},
            {
             duration: 500
        });
        // return false;
      }
    });



// slider on mobile

  var $window = $(window)
    , $card = $('.eight')
    , toggleSlick;

    if (windowWidthValue < 765) {
      $card.slick({
        slidesToShow: 1
      });
    }

  $(window).resize(function(){
    var windowWidthValue = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    if (windowWidthValue > 768) {
      if ($card.hasClass('slick-slider')){
        $card.slick('unslick');
      }
    } else{
      if (!$card.hasClass('slick-slider')){
        $card.slick({
          slidesToShow: 1
        });
      }

    }
  });

// ****************************************

  $('.first_slider').slick({

    slidesToShow: 4,
    slidesToScroll: 4,
    dots:true,
    speed: 1000,
    dotsClass:'first_dots'
  });

  $('.second_slider').slick({

    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1000,
    dots:true,
    dotsClass:'second_dots'
  });

  $('.second_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    var slide = $(this).find('.slick-slide[data-slick-index="'+currentSlide+'"]');
    var slideNext = $(this).find('.slick-slide[data-slick-index="'+nextSlide+'"]');
    slide.animate({
         opacity: 0
    }, 600, function() {
      setTimeout(function() {
       slide.css('opacity', '1');
      }, 400);
    });

    // console.log(slideNext);
    slideNext.css('opacity', '0');
    slideNext.animate({
         opacity: 1
    }, 1000);
    // console.log($('.second_slider').slick('slickCurrentSlide'));
  });







var clickedMenuCallback = false;

   // popup заказать звонок

      $('.first ul li a.call_back').click(function() {
          $('#order1.popup').fadeIn(400);
          $("#overlay").show().css({"opacity": "0.5", "display": "block"});
          canScroll = false;
      });

      $('.popup .close_popup').click(function() {
          $('#order1.popup').fadeOut(400);
          $("#overlay").show().css({"opacity": "0", "display": "none"});
          canScroll = true;
      });

      $('.menu_collapse_768 ul.black li a.call_back').click(function() {
         clickedMenuCallback = true;

          $('#order1.popup').fadeIn(200);
          canScroll = false;
      });

      $('.popup .close_popup').click(function() {
          $('#order1.popup').fadeOut(400);
          canScroll = true;
          clickedMenuCallback = false;
      });


  $('.menu_collapse_768').click(function() {
    if (!clickedMenuCallback){
     setTimeout(function(){
     $("#overlay").show().css({"opacity": "0.5", "display" : "block"})
     },300)
    _this =  $(this)
     _this.toggleClass('active_nav');

     if (windowWidthValue > 767) {

      if(_this.hasClass('active_nav')){
        setTimeout(function(){
          _this.css({'height':$('.menu_collapse_768 ul').height()})
        },200)
      }
      else {
        _this.attr('style','')
         setTimeout(function(){
        $("#overlay").show().css({"opacity": "0", "display" : "none"})
        },400)
      }

    } else {
       _this.attr('style','')
         setTimeout(function(){
        $("#overlay").hide().css({"opacity": "0", "display" : "none"})
        },400)
    }
    }

    })





$('.word').click(function(){
   $(this).toggleClass('opinion');
   $(this).siblings('div').toggle('300');
});








//scroll on 11 screen


$(window).on("load",function(){
  if (windowWidthValue > 767) {
    $(".eighteen_scroll").mCustomScrollbar();
  }
});



// fadeout fixed  menu

 $(window).bind("scroll", function() {

   var topPositon = $(window).scrollTop();
   var neededPosition = $('.new_menu_appear').offset().top;
   var newMenu = $('.wrap_nav');

    if(topPositon > 0) {
        $('.wrap_content').addClass('line_hidden');
    } else {
        $('.wrap_content').removeClass('line_hidden');
    }
     if ($(window).scrollTop() >= neededPosition) {
         $(newMenu).addClass("loader");
        }
     else {
        $(newMenu).removeClass("loader");
        };


         if ($(window).scrollTop() >= 400) {
         $(newMenu).addClass("removediv");
        }
     else {
        $(newMenu).removeClass("removediv");
        };

  });



     $(function () {
    $(window).scroll(function () {
        var top_offset = $(window).scrollTop();
        if (top_offset == 0) {
            $('.wrap_nav').removeClass('back_shadow');
        } else {
            $('.wrap_nav').addClass('back_shadow');
        }
    })
});

     $('#frame .slidee > div').each(function() {
      var width = $('#frame').innerWidth();
       $(this).css('width', width/2+'px');
       if( width <= 425) {$(this).css('width', width +'px'); }

     });
    var frame = $('#frame');
    var sly = new Sly(frame,{
      horizontal: 1,
      itemNav: 'basic',
      smart: 1,
      activateOn: 'click',
      mouseDragging: 1,
      touchDragging: 1,
      releaseSwing: 1,
      startAt: 0,
      scrollBar: $('.scrollbar'),
      scrollBy: 1,
      // pagesBar: $wrap.find('.pages'),
      // activatePageOn: 'click',
      speed: 800,
      elasticBounds: false,
      dragHandle: 1,
      dynamicHandle: 1,
      clickBar: 1,

      //classes
      activeClass: 'active',

      // Buttons
      prev: $('#frame').find('.prev'),
      next: $('#frame').find('.next')
    });
    $('.slidee .active').addClass('curr');

    sly.on('load moveStart', function(){
        if(window.matchMedia("(min-width: 1025px)").matches) {
            var active = $('.slidee .curr');
            oprop = $('.slidee').css('transform');
            oprop = oprop.split(',');
            var oldV = oprop[4];
            if(oldV != 0) {
                oldV = oldV*-1;
            }
            setTimeout(function(){
                nprop = $('.slidee').css('transform');
                nprop = nprop.split(',');
                var newV = nprop[4];
                if(newV != 0) {
                    newV = newV*-1;
                }
                if(newV >= oldV) {
                    active.stop().animate({
                        opacity: 0
                    }, 600, function() {
                        setTimeout(function() {
                            active.css('opacity', '1');
                        }, 400);
                    });
                    active.next().next().css('opacity', 0)
                        .stop().animate({
                        opacity: 1
                    }, 1200);
                } else {
                    //console.log('false');
                    active.next().stop().animate({
                        opacity: 0
                    }, 600, function() {
                        setTimeout(function() {
                            active.next().css('opacity', '1');
                        }, 400);
                    });
                    active.prev().css('opacity', 0)
                        .stop().animate({
                        opacity: 1
                    }, 1200);
                }
            }, 100);
            setTimeout(function(){
                prop = $('.slidee').css('transform');
                prop = prop.split(',');
                prevValue = prop[4];
                active.removeClass('curr');
                var num = 1;
                if(prevValue != 0) {
                    num = (prevValue*-1/540) + 1;
                }
                $('.slidee > div:nth-child('+num+')').addClass('curr');
            },850);
        }
    });
    sly.init();

    $('input,textarea').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
         .attr('placeholder','');
         }).blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
    });

        $('.toggle').on('click', function() {
        $('.containerp').stop().addClass('active');
        });

        $('.close').on('click', function() {
        $('.containerp').stop().removeClass('active');
        });


// Form validation


$(function() {

    $('#form').each(function(){

      var form = $(this),
          btn = form.find('button');

      form.find('.valid').addClass('empty_field');
      btn.addClass('disabled');

      // Функция проверки полей формы
      function checkInput(){

        form.find('.valid').each(function(){

           if($(this).hasClass('email')) {
            var mailfield = $(this);
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test(mailfield.val())){
              mailfield.removeClass('empty_field');
            } else {
              mailfield.addClass('empty_field');
            }
          } else if($(this).val() != '') {
            $(this).removeClass('empty_field');
          } else {
            $(this).addClass('empty_field');
          }

        });
      }

      // Функция подсветки незаполненных полей
      function lightEmpty(){
        form.find('.empty_field').addClass('rf_error');
        form.find('.empty_field').closest('.required').addClass('not-valid');
        form.find('.empty_field').parent().find('.rfield_error').fadeIn(300);
        setTimeout(function(){
          form.find('.empty_field').removeClass('rf_error');
          form.find('.empty_field').closest('.required').removeClass('not-valid');
          form.find('.rfield_error').fadeOut(300);
        },2000);
      }

      //  Полсекундная проверка
      setInterval(function(){
        checkInput();
        var sizeEmpty = form.find('.empty_field').length;
        if(sizeEmpty > 0){
          if(btn.hasClass('disabled')){
            return false
          } else {
            btn.addClass('disabled')
          }
        } else {
          btn.removeClass('disabled')
        }
      },500);

      //  Клик по кнопке
      btn.click(function(e){
        if($(this).hasClass('disabled')){
          lightEmpty();
          return false
        } else {
          e.preventDefault(e);
          var form_data = form.serialize(); //собераем все данные из формы
            $.ajax({
                type: "POST", //Метод отправки
                url: "mail.php", //путь до php фаила отправителя
                data: form_data,
                success: function() {
                       //код в этом блоке выполняется при успешной отправке сообщения
                       window.alert("Ваше сообщение успешно отправлено! =)");
                       $('#form')[0].reset();
                }
            });
        }
      });

    });

});

/*----------------------------*/

// load map script


    function initialize() {

        // Create an array of styles.
        var styles =   [
   {
       "featureType": "administrative",
       "elementType": "all",
       "stylers": [
           {
               "saturation": "-100"
           }
       ]
   },
   {
       "featureType": "administrative.province",
       "elementType": "all",
       "stylers": [
           {
               "visibility": "off"
           }
       ]
   },
   {
       "featureType": "landscape",
       "elementType": "all",
       "stylers": [
           {
               "saturation": -100
           },
           {
               "lightness": 65
           },
           {
               "visibility": "on"
           }
       ]
   },
   {
       "featureType": "poi",
       "elementType": "all",
       "stylers": [
           {
               "saturation": -100
           },
           {
               "lightness": "50"
           },
           {
               "visibility": "simplified"
           }
       ]
   },
   {
       "featureType": "road",
       "elementType": "all",
       "stylers": [
           {
               "saturation": "-100"
           }
       ]
   },
   {
       "featureType": "road",
       "elementType": "geometry.fill",
       "stylers": [
           {
               "visibility": "on"
           },
           {
               "color": "#ffde17 "
           }
       ]
   },
   {
       "featureType": "road.highway",
       "elementType": "all",
       "stylers": [
           {
               "visibility": "simplified"
           }
       ]
   },
   {
       "featureType": "road.arterial",
       "elementType": "all",
       "stylers": [
           {
               "lightness": "30"
           }
       ]
   },
   {
       "featureType": "road.local",
       "elementType": "all",
       "stylers": [
           {
               "lightness": "40"
           }
       ]
   },
   {
       "featureType": "transit",
       "elementType": "all",
       "stylers": [
           {
               "saturation": -100
           },
           {
               "visibility": "simplified"
           }
       ]
   },
   {
       "featureType": "water",
       "elementType": "geometry",
       "stylers": [
           {
               "hue": "#ffff00 "
           },
           {
               "lightness": -25
           },
           {
               "saturation": -97
           }
       ]
   },
   {
       "featureType": "water",
       "elementType": "labels",
       "stylers": [
           {
               "lightness": -25
           },
           {
               "saturation": -100
           }
       ]
   }
]



// Create a new StyledMapType object, passing it the array of styles,
        // as well as the name to be displayed on the map type control.
        var styledMap = new google.maps.StyledMapType(styles,
            {name: 'Styled Map'});


        var mapOptions = {
            zoom: 14,
            mapTypeControl: false,
            center: new google.maps.LatLng(50.428542, 30.531771),
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };



        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);


        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        var companyImage = new google.maps.MarkerImage('images/locat.png',
            new google.maps.Size(42,57),
            new google.maps.Point(0,0),
            new google.maps.Point(42,57)
        );


        var companyPos = new google.maps.LatLng(50.428528, 30.531782);

        var companyMarker = new google.maps.Marker({
            position: companyPos,
            map: map,
            icon: companyImage,
            zIndex: 3});


   }


    google.maps.event.addDomListener(window, 'load', initialize);





});

