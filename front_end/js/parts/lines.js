(function($){
    $(document).ready(function(){
        SmoothScroll({ stepSize: 80 });
        setTimeout(function(){
            $('.pre').fadeOut(800);
        }, 6000);
        var s;
        setTimeout(function(){
            if(window.matchMedia("(min-width: 1025px)").matches) {
                s = skrollr.init();
            }
        }, 500);
        onloadPage();
/*============================================= form 3 button ========================================================*/
        var form3button = $('.form3 button');
        $(window).on('scroll', function(){
            var scroll = $(window).scrollTop();
            if(window.matchMedia("(min-width: 1025px)").matches) {
                if (scroll >= 5000) {
                    form3button.addClass('ready');
                } else {
                    form3button.removeClass('ready');
                }
            }
        });
        if(window.matchMedia("(max-width: 1024px)").matches) {
            form3button.addClass('ready');
        }
        $(window).on('resize', function(){
            if(window.matchMedia("(max-width: 1024px)").matches) {
                form3button.addClass('ready');
            } else {
                $('.pre').hide();
            }
        });
/*========================================== slider opacity effect ===================================================*/
        var firstSlider = $('.first_slider');
        firstSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var slide = $(this).find('.slick-active');
            slide.animate({
                opacity: 0
            }, 600, function() {
                setTimeout(function() {
                    slide.css('opacity', '1');
                }, 400);
            });
        });
/*============================================ parallax icons ========================================================*/
        $('.fourteen1').on('mousemove', function(e) {
            if(window.matchMedia("(min-width: 1025px)").matches) {
                var mx = e.pageX;
                var rotateK = 90 / $(window).width();
                //console.log(rotateK);
                //console.log(mx);
                var rotateDeg = -(45 - rotateK * mx);
                if(rotateDeg < 0) {
                    rotateDeg = rotateDeg - 7;
                }
                $(this).find('.show_item').css({transform: 'rotate3d(0, 1, 0, ' + -rotateDeg + 'deg )'});
            }
        });
        $('.fourteen1').on('mouseover', function(){
            $('.fourteen1 .show_item').css({transform: 'rotate3d(0,1,0, 0deg)'});
        });
/*=========================================== block 9 hidden text ====================================================*/
        $('.read_more').on('click', function(){
            var par = $(this).parent(),
                video = par.find('.video3'),
                text = par.find('.desc_text'),
                h = par.find('.desc_text > div').height() + 2;
            if(h > 285) {
                h = 285;
            }
            if(window.matchMedia("(max-width: 1024px)").matches) {
                if(h > 225) {
                    h = 225;
                }
            }
            if(window.matchMedia("(max-width: 760px)").matches) {
                if(h > 260) {
                    h = 260;
                }
            }

            if($(this).hasClass('opened')) {
               $(this).removeClass('opened').html('Читать больше...');
                text.animate({
                    height: '121px'
                }, 600, function(){
                    video.fadeIn();
                });
            } else {
               $(this).addClass('opened').html('Свернуть');
               video.hide();
               text.animate({
                   height: h
               }, 600);
            }
        });
/*=========================================== slider third block =====================================================*/
        var slider = $('.container.third');
        slider.find('h5, p').on('click', function(){
            if (window.matchMedia("(max-width: 1024px)").matches) {
                var lines = $(this).parent().find('line'),
                    line1 = $(this).parent().find('line:first-child'),
                    line2 = $(this).parent().find('line:nth-child(2)');
                $('.container.third line').animate({
                    'stroke-dashoffset': 200
                }, function () {
                    $('.container.third line:nth-child(2)').hide();
                    $('.container.third line:nth-child(1)').show();
                });
                lines.stop().animate({
                    'stroke-dashoffset': 0
                }, function () {
                    line1.hide();
                    line2.show();
                });
            }
        });
        slider.find('h5, p').on('mouseover', function(){
            if (window.matchMedia("(min-width: 1025px)").matches) {
                var lines = $(this).parent().find('line'),
                    line1 = $(this).parent().find('line:first-child'),
                    line2 = $(this).parent().find('line:nth-child(2)');
                $('.container.third line').stop().animate({
                    'stroke-dashoffset': 200
                }, function () {
                    $('.container.third line:nth-child(2)').hide();
                    $('.container.third line:nth-child(1)').show();
                });
                lines.stop().animate({
                    'stroke-dashoffset': 0
                }, function () {
                    line1.hide();
                    line2.show();
                });
            }
        });
    });
})(jQuery);
function onloadPage(){
    var scroll = $(window).scrollTop();
    var form3button = $('.form3 button');
    if (scroll > 0) {
        $('.wrap_nav').addClass('back_shadow removediv');
        $('.wrap_content').addClass('line_hidden');
    }
    if(scroll >= 5000) {
        form3button.addClass('ready');
    }
}
/*============================================== lightbox ===========================================================*/
var sliderIndex = 1;
$('.eight_big a').on('click', function(e){
    var index = $(this).parent().attr('data-index');
    e.preventDefault();
    $('.overflow_lb').fadeIn();
    sliderIndex = index;
    lb_click();
});
$('.overflow_lb, .overflow_lb .close_lb').on('click', function(){
   $('.overflow_lb').fadeOut();
   $('.overflow_lb .lb').fadeOut();
});
$('.overflow_lb>div').on('click', function(e){
    e.stopPropagation();
});
function lb_click() {
    var next = $('.overflow_lb .lb:nth-child('+sliderIndex+')');

    $('.overflow_lb .lb').removeAttr('style');
    $('.overflow_lb .text_wrap').removeAttr('style');
    $('.overflow_lb .text_content').removeAttr('style');
    heightLb = next.innerHeight() + 40;
    if(window.matchMedia("(max-width: 760px)").matches) {
        heightLb = next.innerHeight() + 60;
    }
    if(heightLb < 400) {
        heightLb = 400;
    }
    $('.overflow_lb .lb').fadeOut();
    if(window.matchMedia("(max-width: 760px)").matches) {
        $('.overflow_lb .text_wrap').height(heightLb);
        $('.overflow_lb .text_content').height(heightLb);
    }
    $('.overflow_lb .lb_content').animate({
        height: heightLb
    }, 400);
    next.animate({
        height: heightLb
    }, 400, function(){
        next.fadeIn();
    });
}
$('.overflow_lb .lb_prev').on('click', function(){
    sliderIndex = parseInt(sliderIndex)-1;
    if(sliderIndex < 1) {
        sliderIndex = $('.overflow_lb .lb').length;
    }
    lb_click();
});
$('.overflow_lb .lb_next').on('click', function(){
    sliderIndex = parseInt(sliderIndex)+1;
    if(sliderIndex > $('.overflow_lb .lb').length) {
        sliderIndex = 1;
    }
    lb_click();
});
