<?php

class DeskZohoApi
{

    private $oauth_token = '';


    public function __construct()
    {
        $this->oauth_token = $this->getAccessToken();
    }


    private function getAccessToken()
    {
        $file_token = dirname(__FILE__).'/../token.php';

        if (file_exists($file_token) && ((filemtime($file_token) + 1800) > time())) {
            $token = include($file_token);

            if (empty($token['token'])) {
                $token['token'] = $this->generateAccessFromRefreshToken();

                file_put_contents($file_token, '<?php '.PHP_EOL.'return ' . var_export($token, true) . ';');
            }
        } else {
            $token['token'] = $this->generateAccessFromRefreshToken();
            file_put_contents($file_token, '<?php '.PHP_EOL.'return ' . var_export($token, true) . ';');
        }
        return $token['token'];
    }


    private function generateAccessFromRefreshToken()
    {
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded'
        );

        $post_fields = array(
            'refresh_token' => Config::$params['zoho_refresh_token'],
            'client_id'     => Config::$params['zoho_client_id'],
            'client_secret' => Config::$params['zoho_client_secret'],
            'grant_type'    => 'refresh_token'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://accounts.zoho.com/oauth/v2/token");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_fields));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        curl_close($ch);

        $response_obj = json_decode($response);

        if (array_key_exists("access_token", $response_obj)) {
            $access_token = $response_obj->access_token;
        } else {
            $access_token = '';
        }

        return $access_token;
    }


    public function getDataFromCrm($url, $param, $method)
    {
        $headers = array(
            'Authorization: Zoho-oauthtoken ' . $this->oauth_token,
            'orgId: ' . Config::$params['org_id'],
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$method");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if (($method == "POST") || ($method == "PUT")) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        }

        $result = curl_exec($ch);

        if($result === false) {
            Log::logging('curl', curl_error($ch));
        }

        curl_close($ch);

        return $result;
    }
    
    
    public function addAttachToTicketDesk($id, $attach_file)
    {
        $file_type = $attach_file['type']; 
        $ext = $this->getFileExtension($file_type);        
        
        $url = 'https://desk.zoho.com/api/v1/tickets/'.$id.'/attachments';
        
        $headers = array(
            'Authorization: Zoho-oauthtoken ' . $this->oauth_token,
            'orgId: ' . Config::$params['org_id'],
            'Content-Type: multipart/form-data'
        );

        $param = array(
            'file' => curl_file_create($attach_file['tmp_name'], "$file_type", $attach_file['name'].$ext)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        $result = curl_exec($ch);

        curl_close($ch);
        
        if (isset($result['errorCode'])) {
            Log::logging('attachment error', print_r($result, true));
        }
        
        return $result;    
    }


    /*
     * Search record
     */
    public function searchByPhone($phone, $field_name = '')
    {
        $url = "https://desk.zoho.com/api/v1/contacts/search?limit=1&phone=" . $phone;

        //make request
        $result = $this->getDataFromCrm($url, '', "GET");

        if (isset($result)) {
            $result_array = json_decode($result, true);

            if (isset($result_array['status']) && $result_array['status'] == "error") {
                //log error to file
                Log::logging(' search contact ', $result);

            } else {

                return !empty($field_name) ? $this->getByField($result_array, 'search', $field_name) : $result_array;
            }
        }

        return null;
    }


    /*
     * Create record
     */
    public function createRecord($module_name, $module_data)
    {
        $url = "https://desk.zoho.com/api/v1/" . $module_name;

        //make request
        $result = $this->getDataFromCrm($url, json_encode($module_data), "POST");

        if (isset($result)) {
            $result_array = json_decode($result, true);

            if (isset($result_array['status']) && $result_array['status'] == "error") {
                //log error to file
                Log::logging($module_name, $result);

            } else {

                return isset($result_array['id']) ? $result_array['id'] : '';
            }
        }

        return null;
    }


    /*
     * Get value by field name or full array
     */
    private function getByField($data = array(), $operation = '', $field_name = '')
    {
        if ($operation == 'search') {
            return isset($data['data'][0][$field_name]) ? $data['data'][0][$field_name] : '';
        }
    }
    
    
    private function getFileExtension($type)
    {
        $mime_types = array(
            'txt' => 'text/plain',
            'html' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpeg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        
        
        $mime_types = array_flip($mime_types);
        
        return isset($mime_types[$type]) ? '.'.$mime_types[$type] : '';
    }
}
