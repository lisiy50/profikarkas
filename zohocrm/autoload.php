<?php

$needed_files = array(
    dirname(__FILE__).'/env.php',
    dirname(__FILE__).'/classes/Log.php',
    dirname(__FILE__).'/classes/Request.php',
    dirname(__FILE__).'/classes/ZohoCrmApi.php',
    dirname(__FILE__).'/classes/DeskZohoApi.php'
);

foreach ($needed_files as $file) {
    require_once($file);
}
    
    