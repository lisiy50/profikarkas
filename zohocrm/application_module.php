<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

//header('Access-Control-Allow-Origin: *');

header('Content-Type: text/html; charset=utf-8');

require_once(dirname(__FILE__).'/autoload.php');

class App
{
    private $zohoCrmApi;
    private $deskZohoApi;
    private $request = array();
    private $data = array();


    public function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            die();
        }

        //Create ZohoCrmApi object
        $this->zohoCrmApi = new ZohoCrmApi();
        //Create DeskZohoApi object
        $this->deskZohoApi = new DeskZohoApi();
        //Set data from request
        $this->setData();

        if (!empty($this->data['name']) || !empty($this->data['phone'])) {
            //Execute
            $this->run();
        }

    }


    public function run()
    {
        //search contact and set contact_id
        $this->searchContact();

        //Create contact
        if (empty($this->data['contact_id'])) {
            $this->createContact();
        }


        //Create ticket
        if (!empty($this->data['contact_id'])) {
            $this->createTicket();
        }

        //Create lead
        $this->createLead();

        die();
    }


    public function setData()
    {
        //Get data from GET, POST and COOKIE

        $this->request = new Request();

        $this->data['subject']  = isset($this->request->post['subject']) ? $this->request->post['subject'] : '';
        $this->data['name']  = isset($this->request->post['name']) ? $this->request->post['name'] : '';
        $this->data['email']  = isset($this->request->post['email']) ? $this->request->post['email'] : '';
        $this->data['phone'] = isset($this->request->post['phone']) ? $this->request->cleanPhone($this->request->post['phone']) : '';
        $this->data['time'] = isset($this->request->post['time']) ? $this->request->post['time'] : '';
        $this->data['comment'] = isset($this->request->post['comment']) ? $this->request->post['comment'] : '';
    }


    //Create lead
    public function createLead()
    {
        $lead_data = array(
            "Last_Name"     => (string)$this->data['name'],
            "Phone"         => (string)$this->data['phone'],
            "Email_Address" => (string)$this->data['email'],
        );

        $this->data['lead_id'] = $this->zohoCrmApi->createRecord('Leads', $lead_data, 'id', 'approval, workflow, blueprint');
    }


    public function searchContact()
    {
        if (!empty($this->data['phone'])) {
            $this->data['contact_id'] = $this->deskZohoApi->searchByPhone($this->data['phone'], 'id');
        }
    }


    //Create contact and set id
    public function createContact()
    {
        $contact_data = array(
            "lastName"     => (string)$this->data['name'],
            "phone"        => (string)$this->data['phone'],
            "email"        => (string)$this->data['email']
        );

        $this->data['contact_id'] = $this->deskZohoApi->createRecord('contacts', $contact_data);
    }

    //Create contact and set id
    public function createTicket()
    {
        $ticket_data = array(
            "subject"      => (string)$this->data['subject'],
            "contactId"    => $this->data['contact_id'],
            "phone"        => (string)$this->data['phone'],
            "email"        => (string)$this->data['email'],
            "departmentId" => "117151000000092675",
            "description" => (string)$this->data['subject'] . ' ' . (string)$this->data['comment'] . ' Время для звонка: ' . (string)$this->data['time'],
            "channel"      => "Web",
        );

        $this->data['ticket_id'] = $this->deskZohoApi->createRecord('tickets', $ticket_data);
    }

}

new App();
