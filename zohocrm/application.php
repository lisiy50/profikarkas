<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

header('Access-Control-Allow-Origin: *');

header('Content-Type: application/json; charset=utf-8');

require_once(dirname(__FILE__).'/autoload.php');

class App
{
    private $zohoCrmApi;
    private $deskZohoApi;
    private $request = array();
    private $data = array();


    public function __construct()
    {
        
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            die(json_encode(['status' => 'error']));
        }

        //Create ZohoCrmApi object
        $this->zohoCrmApi = new ZohoCrmApi();
        //Create DeskZohoApi object
        $this->deskZohoApi = new DeskZohoApi();
        //Set data from request
        $this->setData();
        //Execute
        $this->run();
    }


    public function run()
    {
        if (empty($this->data['name']) && empty($this->data['email'])) {
            die(json_encode(['status' => 'error']));
        }

        //search contact and set contact_id
        $this->searchContact();

        //Create contact
        if (empty($this->data['contact_id'])) {
            $this->createContact();
        }

        //Create ticket
        if (!empty($this->data['contact_id'])) {
            $this->createTicket();
            
            if (isset($_FILES['document']) && !empty($this->data['ticket_id'])) {
                $this->addAttachmentToTicket($this->data['ticket_id'], $_FILES['document']);
            }
        }

        //Create lead
        $this->createLead();

        if (!empty($this->data['contact_id']) && !empty($this->data['ticket_id']) && !empty($this->data['lead_id'])) {
            die(json_encode([
                'status' => 'success',
                'data' => json_encode($this->data)
            ]));

        } else {
            die(json_encode(['status' => 'error']));
        }
    }


    public function setData()
    {
        //Get data from GET, POST and COOKIE

        $this->request = new Request();

        $this->data['subject']      = isset($this->request->post['subject']) ? $this->request->post['subject'] : '';
        $this->data['name']         = isset($this->request->post['name']) ? $this->request->post['name'] : '';
        $this->data['email']        = isset($this->request->post['email']) ? $this->request->post['email'] : '';
        $this->data['country']      = isset($this->request->post['country']) ? $this->request->post['country'] : '';
        $this->data['city']         = isset($this->request->post['city']) ? $this->request->post['city'] : '';
        $this->data['landing_lang'] = isset($this->request->post['landing_lang']) ? $this->request->post['landing_lang'] : '';    
        $this->data['phone']        = isset($this->request->post['phone']) ? $this->request->cleanPhone($this->request->post['phone']) : '';  
        
        $this->data['lang_code']    = !empty($this->data['landing_lang']) ? substr($this->data['landing_lang'], -2) : '';
        
        if (!empty($this->data['lang_code'])) {
            $this->data['lang_code'] = in_array($this->data['lang_code'], array('en', 'fr', 'de')) ? strtoupper($this->data['lang_code']) : '';
        }        
    }


    //Create lead
    public function createLead()
    {
        $lead_data = array(
            "Layout"            => "1793916000036629001",
            "Last_Name"         => (string)$this->data['name'],
            "Phone"             => (string)$this->data['phone'],
            "Email"             => (string)$this->data['email'],
            "State"             => (string)$this->data['country'],
            "City"              => (string)$this->data['city'],
            "URL_first"         => (string)$this->data['landing_lang'],
            "field12"           => "Profikarkas",
            "Lead_Status"       => "Еще конь не валялся"
        );

        $this->data['lead_id'] = $this->zohoCrmApi->createRecord('Leads', $lead_data, 'id', 'approval, workflow, blueprint');
    }


    public function searchContact()
    {
        if (!empty($this->data['phone'])) {
            $this->data['contact_id'] = $this->deskZohoApi->searchByPhone($this->data['phone'], 'id');
        }
    }


    //Create contact and set id
    public function createContact()
    {
        $contact_data = array(
            "lastName"     => (string)$this->data['name'],
            "phone"        => (string)$this->data['phone'],
            "email"        => (string)$this->data['email']
        );

        $this->data['contact_id'] = $this->deskZohoApi->createRecord('contacts', $contact_data);
    }

    //Create contact and set id
    public function createTicket()
    {
        $ticket_data = array(
            "subject"      => sprintf('Заявка с евространицы %s от %s', $this->data['lang_code'], $this->data['name']),
            "contactId"    => $this->data['contact_id'],
            "phone"        => (string)$this->data['phone'],
            "email"        => (string)$this->data['email'],
            "departmentId" => "117151000000092675",
            "description"  => (string)$this->data['subject'],
            "channel"      => "Web"
        );

        $this->data['ticket_id'] = $this->deskZohoApi->createRecord('tickets', $ticket_data);
    }
    
    
    public function addAttachmentToTicket($id, $attach_file)
    {
        $this->deskZohoApi->addAttachToTicketDesk($id, $attach_file);
    }

}

new App();
