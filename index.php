<?php
if (isset($_COOKIE['debug'])) {
    define('YII_DEBUG', true);
}
defined('YII_DEBUG') or define('YII_DEBUG', $_SERVER['REMOTE_ADDR'] == '127.0.0.1');
define('YII_VERSION', 'yii-1.1.21');

defined('IS_BACKEND') or define('IS_BACKEND', false);
defined('IS_FRONTEND') or define('IS_FRONTEND', true);

if (YII_DEBUG) {
    define('YII_TRACE_LEVEL', 3);
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
} else
    error_reporting(0);

$base_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'protected' . DIRECTORY_SEPARATOR;
$yii_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . YII_VERSION;

if (!file_exists($yii_path))
    $yii_path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . YII_VERSION;

$yiiBasePath = $yii_path . DIRECTORY_SEPARATOR . (YII_DEBUG ? 'yii.php' : 'yiilite.php');
require_once($yiiBasePath);

require_once($base_path . 'lib' . DIRECTORY_SEPARATOR . 'shortcut.php');
require_once($base_path . 'base' . DIRECTORY_SEPARATOR . 'WebApplication.php');

$config = require_once($base_path . 'config' . DIRECTORY_SEPARATOR . 'frontend.php');

$app = Yii::createApplication('WebApplication', $config);

unset($config, $yii_path, $base_path);

$app->run();
